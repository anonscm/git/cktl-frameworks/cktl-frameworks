/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOPlanComptableNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SwingFinderEOPlanComptableNibCtrl extends SwingFinderEOGenericRecord {
	//NSMutableArray resltat = new NSMutableArray();
	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_FILTRER = "actionFiltrer";
	
	// les tables view
	private JTableViewCocktail resultatTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup resultatDG = new NSMutableArrayDisplayGroup();

	// le nib
	private SwingFinderEOPlanComptableNib monAssistantPlanComptableNib = null;

	// parentControleur
	//NibCtrl parentControleur = null;


//	public void setSender(SwingFinder swingfinder) {
//	this.swingfinder= swingfinder;
//	}


//	public SwingFinder getSender() {
//	return swingfinder;
//	}


//	private SwingFinder swingfinder=null;

//	private boolean modal = false;

//	public boolean isModal() {
//	return modal;
//	}

	public SwingFinderEOPlanComptableNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);
	}

	public void creationFenetre(SwingFinderEOPlanComptableNib leAssistantPlanComptableNib, String title,NibCtrl parentControleur,boolean modal) {
		super.creationFenetre(leAssistantPlanComptableNib, title);
		setMonAssistantPlanComptableNib(leAssistantPlanComptableNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
	}


	public void creationFenetre(SwingFinderEOPlanComptableNib leAssistantPlanComptableNib, String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
		super.creationFenetre(leAssistantPlanComptableNib, title);
		setMonAssistantPlanComptableNib(leAssistantPlanComptableNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleurEONib =  parentControleur;
		bindingAndCustomization();
	}

//	public void afficherFenetre(){
//	super.afficherFenetre();
//	if(isModal()){
//	app.removeFromLesPanelsModal(getMonAssistantPlanComptableNib());
//	app.activerLesGlassPane();
//	}
//	}



	public void setResultat() {
		
		resltat = new NSMutableArray(getResultatTBV().getSelectedObjects());
	}


	private void bindingAndCustomization() {

		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Numero", 100,JLabel.CENTER, EOPlanComptable.PCO_NUM_KEY, "String"));
			lesColumns.addObject(new ColumnData("Libelle", 100,JLabel.CENTER, EOPlanComptable.PCO_LIBELLE_KEY, "String"));

			setResultatTBV(new JTableViewCocktail(lesColumns, getResultatDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonAssistantPlanComptableNib().getJPanelResultat().add(getResultatTBV());

			// multi ou mono selection dans les tbv
			getResultatTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// le cablage des  objets
			getMonAssistantPlanComptableNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonAssistantPlanComptableNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			getMonAssistantPlanComptableNib().getJButtonCocktailFiltrer().addDelegateActionListener(this, METHODE_ACTION_FILTRER);
            getMonAssistantPlanComptableNib().getJTextFieldCocktailCompte().addDelegateKeyListener(this, METHODE_ACTION_FILTRER);
			// les images des objets
			getMonAssistantPlanComptableNib().getJButtonCocktailSelectionner().setIcone("valider16");
			getMonAssistantPlanComptableNib().getJButtonCocktailAnnuler().setIcone("close_view");
			getMonAssistantPlanComptableNib().getJButtonCocktailFiltrer().setIcone("linkto_help");

			// init interface 
			getMonAssistantPlanComptableNib().getJListTypePco().setSelectedIndex(0);
			getMonAssistantPlanComptableNib().getJCheckBoxBudgetaire().setSelected(false);
			getMonAssistantPlanComptableNib().getJTextFieldCocktailCompte().setText("*");

			//sera modal lors d une transaction 
			if (parentControleurEONib == null)
				app.addLesPanelsModal(currentNib);
			else
				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}

		getResultatTBV().refresh();

	}

	private void setPlancomptableDG() {

		getResultatDG().removeAllObjects();
		if (getResultatDG() != null)
			getResultatTBV().refresh();	

		try {

			getResultatDG().addObjectsFromArray(
					findPlanComptable(app,getMonAssistantPlanComptableNib().getJTextFieldCocktailCompte().getText())
			);

			if (getResultatTBV() != null)
				getResultatTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}

	}

	// finder
	public  NSArray findPlanComptable(ApplicationCocktail app,String pco) {
		String entityName =EOPlanComptable.ENTITY_NAME;
		String pcoType ="D";
		String pcoBudgetaire ="N";
		NSArray leSort = null;
		EOQualifier aQualifier =null;

		// le sort 
		leSort =new NSArray(new Object[]{
				EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)
		});

//EG (INPT)
/*
		if (getMonAssistantPlanComptableNib().getJListTypePco().getSelectedValue().equals("Recette"))
			pcoType ="R";
		else
			pcoType ="D";
*/

		if (getMonAssistantPlanComptableNib().getJListTypePco().getSelectedValue().equals(SwingFinderEOPlanComptableNib.CHOIX_RECETTE))
			pcoType ="R";
		else 
		{
			if (getMonAssistantPlanComptableNib().getJListTypePco().getSelectedValue().equals(SwingFinderEOPlanComptableNib.CHOIX_DEPENSE))
				pcoType ="D";
			else
				pcoType ="T";
		}


		if(getMonAssistantPlanComptableNib().getJCheckBoxBudgetaire().isSelected())
			pcoBudgetaire ="O";
		// le qualifier salles
		NSMutableArray args = new NSMutableArray();
		NSMutableArray args2 = new NSMutableArray();
		args2.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY+" caseinsensitivelike %@", new NSArray(pco+"*")));
		args2.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_LIBELLE_KEY+" caseinsensitivelike %@", new NSArray(pco+"*")));
		args.addObject(new EOOrQualifier(args2));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_VALIDITE_KEY+" = %@", new NSArray("VALIDE")));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_BUDGETAIRE_KEY+" caseinsensitivelike %@", new NSArray(pcoBudgetaire+"*")));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NATURE_KEY+" caseinsensitivelike %@", new NSArray(pcoType+"*")));
		
		
//		aQualifier = EOQualifier.qualifierWithQualifierFormat("pcoNum caseinsensitivelike %@ " +
//				"AND pcoValidite = 'VALIDE' " +
//				"AND pcoBudgetaire caseinsensitivelike %@ " +
//				"AND pcoNature caseinsensitivelike %@",
//				new NSArray(new Object[]{pco+"*",pcoBudgetaire+"*",pcoType+"*"}));
		aQualifier = new EOAndQualifier(args);
		System.out.println(" \n aQualifier \n"+aQualifier);
		return Finder.find(app, entityName, leSort, aQualifier);
	}


	// methodes cablage
//	public void actionAnnuler() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray();
//	getMonAssistantPlanComptableNib().getJListTypePco().setSelectedIndex(0);
//	getMonAssistantPlanComptableNib().getJCheckBoxBudgetaire().setSelected(false);
//	getMonAssistantPlanComptableNib().getJTextFieldCocktailCompte().setText("*");

//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantPlanComptableNib());
//	app.retirerLesGlassPane();
//	}

//	masquerFenetre();
//	parentControleur.swingFinderAnnuler(getSender());
//	}

//	// methodes cablage
//	public void actionSelectionner() {
//	trace(METHODE_ACTION_SELECTIONNER);
//	resltat = new NSMutableArray(getResultatTBV().getSelectedObjects());
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantPlanComptableNib());
//	app.retirerLesGlassPane();
//	}
//	masquerFenetre();
//	parentControleur.swingFinderTerminer(getSender());
//	getMonAssistantPlanComptableNib().getJListTypePco().setSelectedIndex(0);
//	getMonAssistantPlanComptableNib().getJCheckBoxBudgetaire().setSelected(false);
//	getMonAssistantPlanComptableNib().getJTextFieldCocktailCompte().setText("*");
//	}

	// methodes cablage
	public void actionFiltrer() {
		//	trace(METHODE_ACTION_FERMER);
		setPlancomptableDG();
	}
	
	/**
	 * Permet d'afficher la fenetre du finder avec le type recette/depense/tiers deja positionne
	 * @param choixType Le type a preselectionne : {@link SwingFinderEOPlanComptableNib#CHOIX_DEPENSE},{@link SwingFinderEOPlanComptableNib#CHOIX_RECETTE},{@link SwingFinderEOPlanComptableNib#CHOIX_TIERS}
	 */
	public void afficherFenetre(String choixType) {
		getMonAssistantPlanComptableNib().setChoixType(choixType);
		super.afficherFenetre();
	}

	public NSArray getResultat(){

		getMonAssistantPlanComptableNib().getJTextFieldCocktailCompte().setText("");
		getResultatDG().removeAllObjects();
		getResultatTBV().refresh();	
		return resltat;
	}


	public SwingFinderEOPlanComptableNib getMonAssistantPlanComptableNib() {
		return monAssistantPlanComptableNib;
	}

	public void setMonAssistantPlanComptableNib(
			SwingFinderEOPlanComptableNib monAssistantPlanComptableNib) {
		this.monAssistantPlanComptableNib = monAssistantPlanComptableNib;
	}

	public NSMutableArrayDisplayGroup getResultatDG() {
		return resultatDG;
	}

	public void setResultatDG(NSMutableArrayDisplayGroup resultatDG) {
		this.resultatDG = resultatDG;
	}

	public JTableViewCocktail getResultatTBV() {
		return resultatTBV;
	}

	public void setResultatTBV(JTableViewCocktail resultatTBV) {
		this.resultatTBV = resultatTBV;
	}


//	public void swingFinderAnnuler(SwingFinder s) {
//	

//	}


//	public void swingFinderTerminer(SwingFinder s) {
//	

//	}
}

