/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.ModalInterfaceCocktail;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOExerciceNib;
import org.cocktail.application.client.swingfinder.nib.SwingFinderGedNib;
import org.cocktail.application.client.tools.CocktailFileChooser;
import org.cocktail.application.client.tools.GedFile;
import org.cocktail.application.client.tools.Zip;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSRange;

import org.cocktail.fwkcktlwebapp.common.util.SystemCtrl;
import java.lang.reflect.Method;

public class SwingFinderGedNibCtrl extends SwingFinderEOGenericRecord implements ModalInterfaceCocktail{
    /**
     * Deleger du controler : public NSArray getDefaultDocumentGed()
     */
    public static final String METHODE_GET_DEFAULT_DOCUMENT_GED="getDefaultDocumentGed";
	private SwingFinderGedNib leFinderGedNib;
	private NSMutableArrayDisplayGroup lesGedFiles;
	private CocktailFileChooser _explorer;
	private NSMutableArray _listFilter;
	private int nbMaxFichier;
	
	protected static final String METHODE_ACTION_AJOUTER= "actionAjouter";
	protected static final String METHODE_ACTION_SUPPRIMER= "actionSupprimer";
	protected static final String METHODE_ACTION_VOIR= "actionVoir";
    protected static final String METHODE_ACTION_SELECTIONNER_LIST="actionSelectionnerDansListe";
    
    private boolean useDepotFile = true;

	public SwingFinderGedNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(true);
		nbMaxFichier=0;
//		lesGedFiles = new NSMutableArrayDisplayGroup();
	}
	
	public void creationFenetre(SwingFinderGedNib leFinderGedNib,String title,NibCtrl parentControleur,boolean modal)
	{
		super.creationFenetre(leFinderGedNib,title);
		setLeFinderGedNib(leFinderGedNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
	}

	public void creationFenetre(SwingFinderGedNib leFinderGedNib,String title,EOInterfaceControllerCocktail parentControleur,boolean modal)
	{
		super.creationFenetre(leFinderGedNib,title);
		setLeFinderGedNib(leFinderGedNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleurEONib =  parentControleur;
		bindingAndCustomization();
	}
	
	private void bindingAndCustomization() {
		System.out.println("SwingFinderEOExerciceNibCtrl.bindingAndCustomization()");
		try {
			// creation de la table view
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData( "Nom Fichier",100, JLabel.CENTER ,GedFile.KEY_NAME,"String"));
			lesColumns.addObject(new ColumnData( "Url Fichier",100, JLabel.CENTER ,GedFile.KEY_URL,"String"));
			JTableViewCocktail tvc = new JTableViewCocktail(lesColumns,getLesGedFiles(),new Dimension(100,100),JTable.AUTO_RESIZE_LAST_COLUMN);
			if(getLeFinderGedNib()!=null && getLeFinderGedNib().getTvcListCourrier()!=null)
			{
				getLeFinderGedNib().getTvcListCourrier().initTableViewCocktail(tvc);
			}
			else
				getLeFinderGedNib().setTvcListCourrier(tvc);
			
			// multi ou mono selection dans les tbv
			getLeFinderGedNib().getTvcListCourrier().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// le cablage des  objets
			getLeFinderGedNib().getBtcFermer().addDelegateActionListener(this,METHODE_ACTION_SELECTIONNER);
			getLeFinderGedNib().getBtcAjouter().addDelegateActionListener(this,METHODE_ACTION_AJOUTER);
			getLeFinderGedNib().getBtcSupprimer().addDelegateActionListener(this,METHODE_ACTION_SUPPRIMER);
			getLeFinderGedNib().getBtcApercu().addDelegateActionListener(this,METHODE_ACTION_VOIR);
			getLeFinderGedNib().getTvcListCourrier().addDelegateSelectionListener(this,METHODE_ACTION_SELECTIONNER_LIST);
			getLeFinderGedNib().getTvcListCourrier().addDelegateKeyListener(this,METHODE_ACTION_SELECTIONNER_LIST);
			// les images des objets
			getLeFinderGedNib().getBtcFermer().setIcone(IconeCocktail.FERMER);
			getLeFinderGedNib().getBtcAjouter().setIcone(IconeCocktail.AJOUTER);
			getLeFinderGedNib().getBtcSupprimer().setIcone(IconeCocktail.SUPPRIMER);
			getLeFinderGedNib().getBtcApercu().setIcone(IconeCocktail.SHOW);

			
			//sera modal lors d une transaction 
//			if (parentControleurEONib == null)
//				app.addLesPanelsModal(currentNib);
//			else
//				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);
		}
		catch(Throwable e) {
			e.printStackTrace();
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);

		}

		getLeFinderGedNib().getTvcListCourrier().refresh();

	}
	
	public void afficherFenetre(NSArray listCouNumero) {
            if(!GedFile.gedDisponible())
            {
            	fenetreDeDialogueInformation("Impossible d'acceder a la Gestion Electronique des Documents. (probleme d'initialisation !)");
            	return ;
            }
            if(listCouNumero!=null)
            {
                getLesGedFiles().removeAllObjects();
                try {
                    for (int i = 0; i < listCouNumero.count(); i++) {
                            GedFile fic = GedFile.getGedFileForNumero((Integer)listCouNumero.objectAtIndex(i),app.getCurrentUtilisateur().persId());
                            if(getLesGedFiles().indexOfObject(fic)==NSArray.NotFound)
                            {
                                getLesGedFiles().addObject(fic);
                            }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
            else
            	getLesGedFiles().removeAllObjects();
            refreshBtAjouter();
            super.afficherFenetre();
	}

        public void afficherFenetre() {
            NSArray listCouNumero=null;
            if(parentControleur!=null)
                listCouNumero = delegateGetDocument(parentControleur);
            else
                listCouNumero = delegateGetDocument(parentControleurEONib);
            afficherFenetre(listCouNumero);
        }
        
     private void refreshBtAjouter(){
    	 if(nbMaxFichier<=0)
    		 getLeFinderGedNib().getBtcAjouter().setEnabled(true);
    	 else
    		 getLeFinderGedNib().getBtcAjouter().setEnabled(getLesGedFiles().count()<nbMaxFichier);
     }
	
	public void actionAjouter(){
		if(_explorer==null)
		{
	        _explorer = new CocktailFileChooser(null, "Selectionner un fichier ou un repertoire");
	        _listFilter = new NSMutableArray();
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.CSV_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.DOC_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.GIF_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.HTML_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.JPG_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.PDF_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.PNG_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.RTF_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.TXT_FORMAT_ID));
	        _listFilter.addObject(CocktailFileChooser.getFileFilterFor(CocktailFileChooser.XLS_FORMAT_ID));
		}
        NSArray currentFile = null;
        currentFile = _explorer.chooseMultiFile(_listFilter, null,false,CocktailFileChooser.MODE_FILES_AND_DIRECTORIES);
        if(currentFile == null || currentFile.count() == 0)
            return;
        app.ceerTransactionLog();
        java.io.InputStream input = null;
        int etape1 = 0;
        int etape2 = 0;
        int etape3 = 0;
        int etape4 = 0;
        int debut = 0;
        int fin = 100;
        int deplacement = fin - debut;
        BigDecimal oldVal = new BigDecimal((double)debut);
        BigDecimal delta = new BigDecimal((double)deplacement / currentFile.count());
        boolean hasError=false;
        for(int i = 0; i < currentFile.count(); i++)
        {
            BigDecimal val = delta.multiply(new BigDecimal((double)(i + 1))).add(new BigDecimal((double)debut));
            BigDecimal delta2 = delta.divide(new BigDecimal(4D), 4);
            etape1 = delta2.multiply(new BigDecimal(0.0D)).add(delta2).add(oldVal).intValue();
            etape2 = delta2.multiply(new BigDecimal(1.0D)).add(delta2).add(oldVal).intValue();
            etape3 = delta2.multiply(new BigDecimal(2D)).add(delta2).add(oldVal).intValue();
            etape4 = delta2.multiply(new BigDecimal(3D)).add(delta2).add(oldVal).intValue();
            File aFile = (File)currentFile.objectAtIndex(i);
            String nameFile="sansNom";
            NSData datas = null;
            if(aFile.isFile())
            {
	            try
	            {
	            	app.afficherUnLogDansTransactionLog("Debut de l'upload du fichier " + aFile.getName() + " ....", etape1);
	                input = new FileInputStream(aFile);
	            }
	            catch(FileNotFoundException e)
	            {
	                e.printStackTrace();
	                app.afficherUnLogDansTransactionLog("Erreur lors de la lecture du fichier !!!", 100);
	                app.finirTransactionLog();
//	                app.retirerLesGlassPane();
	                return;
	            }
	            
	            try
	            {
	            	app.afficherUnLogDansTransactionLog("Lecture du fichier...", etape2);
	                datas = new NSData(input, (int)aFile.length());
	            }
	            catch(IOException e)
	            {
	                e.printStackTrace();
	                app.afficherUnLogDansTransactionLog("Erreur lors de la lecture du fichier !!!", 100);
	                app.finirTransactionLog();
//	                app.retirerLesGlassPane();
	                return;
	            }
	            nameFile = aFile.getName();
	            useDepotFile=true;
            }
            else
            {
            	String nameZip = app.getToolsCocktailSystem().getTemporaryPathFileName(aFile.getName(), "zip");
            	nameFile = aFile.getName()+".zip";
            	try {
					Zip.zipFolder(aFile.getAbsolutePath(),nameZip , aFile.getAbsolutePath().replaceAll(aFile.getName(),""));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					app.afficherUnLogDansTransactionLog("Erreur lors de la compression du repertoire !!!", 100);
                    app.finirTransactionLog();
//                    app.retirerLesGlassPane();
                    return;
				}
				aFile = new File(nameZip);
                try
                {
                	app.afficherUnLogDansTransactionLog("Debut de l'upload du fichier " + aFile.getName() + " ....", etape1);
                    input = new FileInputStream(aFile);
                }
                catch(FileNotFoundException e)
                {
                    e.printStackTrace();
                    app.afficherUnLogDansTransactionLog("Erreur lors de la lecture du fichier !!!", 100);
                    app.finirTransactionLog();
//                    app.retirerLesGlassPane();
                    return;
                }
                try
                {
                	app.afficherUnLogDansTransactionLog("Lecture du fichier...", etape2);
                    datas = new NSData(input, (int)aFile.length());
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                    app.afficherUnLogDansTransactionLog("Erreur lors de la lecture du fichier !!!", 100);
                    app.finirTransactionLog();
//                    app.retirerLesGlassPane();
                    return;
                }
//                app.finirTransactionLog();
//                app.fermerTransactionLog();
//                System.err.println("1");
//                fenetreDeDialogueYESNOCancel("Voulez-vous deposer un repertoire ? (ou seulement un zip du repertoire)",this, "okDepotRep", "okDepotFile");
//                System.err.println("2");
//                app.ceerTransactionLog();
                okDepotFile();
            }
            System.err.println("3");
            if(useDepotFile)
            {
            	System.err.println("3bis");
            	hasError = depotFichier(datas,nameFile,etape3,etape4);
            }
            else
            {
            	System.err.println("3ter");
            	hasError = depotReperetoire(aFile,nameFile,etape3,etape4);
            }
            oldVal = val;
        }
        if(hasError)
        {
        	app.finirTransactionLog();
//        	app.retirerLesGlassPane();
        	return;
        }

        app.afficherUnLogDansTransactionLog("Mise \u2021 jour de l'affichage...", 95);
        app.finirTransactionLog();
    	app.fermerTransactionLog();
    	refreshBtAjouter();
    	getLeFinderGedNib().getTvcListCourrier().refresh();
//        app.retirerLesGlassPane();
//        if(isModal())
//            app.activerLesGlassPane(currentNib);
        }
	
	public void okDepotRep(){
		System.err.println("2bis");
		useDepotFile=false;
	}
	public void okDepotFile(){
		System.err.println("2ter");
		useDepotFile=true;
	}
	
	public void actionSupprimer(){
		fenetreDeDialogueYESCancel("Etes-vous s\u02DAr de vouloir supprimer ce fichier de la ged ?", this, "deleteFile");
	}
	
	public void deleteFile()
    {
        NSArray list = getLeFinderGedNib().getTvcListCourrier().getSelectedObjects();
        if(list==null || list.count()==0)
        	return;
        app.ceerTransactionLog();
        if(list.count() > 1)
        	app.afficherUnLogDansTransactionLog("Suppression des fichiers dans la ged...", 30);
        else
        	app.afficherUnLogDansTransactionLog("Suppression du fichier dans la ged...", 30);
        Boolean ok = null;
        int debut = 30;
        int fin = 95;
        int deplacement = fin - debut;
        BigDecimal delta = new BigDecimal((double)(deplacement / list.count()));
        EOEditingContext ec = new EOEditingContext();
        try {
            for(int i = 0; i < list.count(); i++)
            {
                BigDecimal val = delta.multiply(new BigDecimal((double)(i + 1))).add(new BigDecimal((double)debut));
                GedFile file = (GedFile)list.objectAtIndex(i);
                app.afficherUnLogDansTransactionLog("Suppression de " + file.getUrl(), val.intValue());
                ec.lockObjectStore();
                ok = (Boolean)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", "clientSideRequestSupprimerFicherDansGed", new Class[] {
                    java.lang.Integer.class, java.lang.Number.class
                }, new Object[] {
                    file.getNumero(), app.getCurrentUtilisateur().persId()
                }, true);
                ec.unlockObjectStore();
                app.afficherUnLogDansTransactionLog("-------------" + (ok.booleanValue() ? "ok" : "ko !!!"), val.intValue());
                if(!ok.booleanValue())
                {
                	app.finirTransactionLog();
                    return;
                }
                getLesGedFiles().removeObject(file);
            }
		} catch (Throwable e) {
			ec.unlockObjectStore();
			e.printStackTrace();
	        app.afficherUnLogDansTransactionLog("Erreur...  "+e.getMessage(), 100);
	        app.finirTransactionLog();
	        return;
		}

        app.afficherUnLogDansTransactionLog("Mise \u2021 jour de l'affichage...", 100);
        app.finirTransactionLog();
        app.fermerTransactionLog();
        refreshBtAjouter();
        getLeFinderGedNib().getTvcListCourrier().refresh();
//        app.retirerLesGlassPane();
//        if(isModal())
//            app.activerLesGlassPane(currentNib);
    }
	
	public void actionVoir(){
		if(getLeFinderGedNib().getTvcListCourrier().getSelectedObjects().count()==1)
		{
			GedFile file = (GedFile)getLeFinderGedNib().getTvcListCourrier().getSelectedObjects().lastObject();
			SystemCtrl.openFileInBrowser(file.getUrl());
		}
	}
	
	public void actionSelectionnerDansListe() {
		switch (getLeFinderGedNib().getTvcListCourrier().getSelectedObjects().count()) {
		case 0:
			getLeFinderGedNib().getBtcAjouter().setEnabled(true);
			getLeFinderGedNib().getBtcApercu().setEnabled(false);
			getLeFinderGedNib().getBtcSupprimer().setEnabled(false);
			break;
			
		case 1:
			getLeFinderGedNib().getBtcAjouter().setEnabled(true);
			getLeFinderGedNib().getBtcApercu().setEnabled(true);
			getLeFinderGedNib().getBtcSupprimer().setEnabled(true);
			break;

		default:
			getLeFinderGedNib().getBtcAjouter().setEnabled(true);
			getLeFinderGedNib().getBtcApercu().setEnabled(false);
			getLeFinderGedNib().getBtcSupprimer().setEnabled(true);
			break;
		}
		refreshBtAjouter();
	}

	public SwingFinderGedNib getLeFinderGedNib() {
		return leFinderGedNib;
	}

	public void setLeFinderGedNib(SwingFinderGedNib leFinderGedNib) {
		this.leFinderGedNib = leFinderGedNib;
	}

	public NSMutableArrayDisplayGroup getLesGedFiles() {
            if(lesGedFiles==null)
                lesGedFiles = new NSMutableArrayDisplayGroup();
            return lesGedFiles;
	}

	public void setLesGedFiles(NSMutableArrayDisplayGroup lesGedFiles) {
		this.lesGedFiles.addObjectsFromArray(lesGedFiles);
	}
	
	public NSArray getResultat() {
		if(getLesGedFiles().count()==0)
			return new NSArray();
//		if(nbMaxFichier<=0)
			return (NSArray)getLesGedFiles().valueForKey(GedFile.KEY_NUMERO);
//		return (NSArray)getLesGedFiles().subarrayWithRange(new NSRange(0, nbMaxFichier-1)).valueForKey(GedFile.KEY_NUMERO);
	}

	public void creerGlassPane() {
		getLeFinderGedNib().creerGlassPane();
	}

	public void poserGlassPane() {
		getLeFinderGedNib().poserGlassPane();
	}

	public void retirerGlassPane() {
		getLeFinderGedNib().retirerGlassPane();
	}
        
    private NSArray delegateGetDocument(Object obj){
        System.out.println("delegateGetDocument >> "+obj.getClass().getName());
        try {
            Class c = obj.getClass();
            Method meth = c.getMethod(METHODE_GET_DEFAULT_DOCUMENT_GED);
            System.out.println("invoke : "+meth.toString());
            return (NSArray) meth.invoke(obj);
        } catch (Exception exception) {
        }
        return new NSArray();
    }
    
    private boolean depotReperetoire(File repertoire,String nameFile,int etape3,int etape4){
    	System.out.println("SwingFinderGedNibCtrl.depotReperetoire("+repertoire+", "+nameFile+", "+etape3+", "+etape4+")");
    	String str = listerRepertoire(repertoire);
    	app.afficherUnLogDansTransactionLog("Erreur, fonction non disponible !!!\n\n"+str, 100);
    	return true;
    }
    
    private String listerRepertoire(File repertoire){
    	System.out.println("listerRepertoire"); 
    	String [] listefichiers;
    	StringBuffer str = new StringBuffer();
    	int i;
    	listefichiers=repertoire.list();
	    System.out.println("listefichiers = " + listefichiers.length);
    	for(i=0;i<listefichiers.length;i++){
	    	System.out.println(listefichiers[i]);
	    	str.append(listefichiers[i]+"\n");
	    }
    	return str.toString();
    	} 
    
    private boolean depotFichier(NSData datas,String nameFile,int etape3,int etape4){
    	EOEditingContext ec = new EOEditingContext();
        try
        {
        	app.afficherUnLogDansTransactionLog("Copie du fichier dans la ged...", etape3);
        	ec.lockObjectStore();
//        	app.getAppEditingContext().parentObjectStore().lock();
            Integer couNumero = (Integer)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", "clientSideRequestAjouterFicherDansGed", new Class[] {
                com.webobjects.foundation.NSData.class, java.lang.String.class, java.lang.Number.class
            }, new Object[] {
                datas, nameFile, app.getCurrentUtilisateur().persId()
            }, true);
            app.afficherUnLogDansTransactionLog("Verification du fichier dans la ged...", etape4);
            ec.unlockObjectStore();
//            app.getAppEditingContext().parentObjectStore().unlock();
            if(couNumero==null || couNumero.intValue()==-1)
            {
            	app.afficherUnLogDansTransactionLog("---------->Erreur !!!!!\n"+GedFile.getLastErrorGed(), etape4);
            	return true;
            }
            else
            	getLesGedFiles().addObjectsFromArray(new NSArray(GedFile.getGedFileForNumero(couNumero,app.getCurrentUtilisateur().persId())));
        }
        catch(Exception e)
        {
        	ec.unlockObjectStore();
        	app.afficherUnLogDansTransactionLog("Erreur lors de la sauvegarde du fichier dans la GED !!!", 100);
            e.printStackTrace();
            return true;
        }
        return false;
    }

	public int getNbMaxFichier() {
		return nbMaxFichier;
	}

	public void setNbMaxFichier(int nbMaxFichier) {
		this.nbMaxFichier = nbMaxFichier;
	}

}
