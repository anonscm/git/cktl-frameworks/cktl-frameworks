/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.eof.EOIndividuUlr;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOIndividuUlrNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class SwingFinderEOIndividuUlrNibCtrl extends SwingFinderEOGenericRecord {

	//NSMutableArray resltat = new NSMutableArray();
	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_FILTRER = "actionFiltrer";

	// les tables view
	private JTableViewCocktail individuTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup IndividuDG = new NSMutableArrayDisplayGroup();

	// le nib
	private SwingFinderEOIndividuUlrNib monAssistantRechercheIndividuNib = null;

	public SwingFinderEOIndividuUlrNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);
	}

	public void creationFenetre(SwingFinderEOIndividuUlrNib leAssistantRechercheIndividuNib, String title,NibCtrl parentControleur,boolean modal) {
		super.creationFenetre(leAssistantRechercheIndividuNib, title);
		setMonAssistantRechercheIndividuNib(leAssistantRechercheIndividuNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
	}

	public void creationFenetre(SwingFinderEOIndividuUlrNib leAssistantRechercheIndividuNib, String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
		super.creationFenetre(leAssistantRechercheIndividuNib, title);
		setMonAssistantRechercheIndividuNib(leAssistantRechercheIndividuNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleurEONib =  parentControleur;
		bindingAndCustomization();
	}


	public void setResultat() {
		resltat = new NSMutableArray(individuTBV.getSelectedObjects());
	}

	private void bindingAndCustomization() {

		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Nom Usuel", 100,JLabel.CENTER, EOIndividuUlr.NOM_USUEL_KEY, "String"));
			lesColumns.addObject(new ColumnData("Nom", 100,JLabel.CENTER, EOIndividuUlr.NOM_PATRONYMIQUE_KEY, "String"));
			lesColumns.addObject(new ColumnData("Prenom", 100,JLabel.CENTER, EOIndividuUlr.PRENOM_KEY, "String"));
			lesColumns.addObject(new ColumnData("Naissance", 100,JLabel.CENTER, EOIndividuUlr.D_NAISSANCE_KEY, "String"));

			setIndividuTBV(new JTableViewCocktail(lesColumns, getIndividuDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonAssistantRechercheIndividuNib().getJPanelResultat().add(getIndividuTBV());

			// multi ou mono selection dans les tbv
			getIndividuTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// le cablage des  objets
			getMonAssistantRechercheIndividuNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			getMonAssistantRechercheIndividuNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonAssistantRechercheIndividuNib().getJButtonCocktailFiltrer().addDelegateActionListener(this, METHODE_ACTION_FILTRER);
            getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere1().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
            getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere2().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
			// les images des objets
			getMonAssistantRechercheIndividuNib().getJButtonCocktailSelectionner().setIcone("valider16");
			getMonAssistantRechercheIndividuNib().getJButtonCocktailAnnuler().setIcone("close_view");
			getMonAssistantRechercheIndividuNib().getJButtonCocktailFiltrer().setIcone("linkto_help");

			//sera modal lors d une transaction 
			if (parentControleurEONib == null)
				app.addLesPanelsModal(currentNib);
			else
				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}

		getIndividuTBV().refresh();

	}

	private void setAssistantRechercheIndividuDG() {

		getIndividuDG().removeAllObjects();
		if (getIndividuDG() != null)
			getIndividuTBV().refresh();	

		try {

			getIndividuDG().addObjectsFromArray(findLesIndividus(app));

			if (getIndividuTBV() != null)
				getIndividuTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}

	}

	// methodes cablage
	public void actionFiltrer() {
		setAssistantRechercheIndividuDG();
	}

	public NSArray getResultat(){

		getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere1().setText("");
		getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere2().setText("");
		getIndividuDG().removeAllObjects();
		getIndividuTBV().refresh();	
		return resltat;
	}

	//finder
	public  NSArray findLesIndividus(ApplicationCocktail app) {
		String entityName =EOIndividuUlr.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier prenomQualifier = null;

		NSMutableArray lesQualifiers = new NSMutableArray();

		// le sort 
		leSort =new NSArray(new Object[]{
				EOSortOrdering.sortOrderingWithKey(EOIndividuUlr.NOM_USUEL_KEY, EOSortOrdering.CompareCaseInsensitiveDescending),
				EOSortOrdering.sortOrderingWithKey(EOIndividuUlr.NOM_PATRONYMIQUE_KEY, EOSortOrdering.CompareCaseInsensitiveDescending),
				EOSortOrdering.sortOrderingWithKey(EOIndividuUlr.PRENOM_KEY, EOSortOrdering.CompareCaseInsensitiveDescending)
		});

		// le qualifier NOM
		if(getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere1().getText().length() >= 2){
			NSMutableArray args = new NSMutableArray();
			args.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.NOM_PATRONYMIQUE_KEY+" = %@", new NSArray(getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere1().getText()+"*")));
			args.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.NOM_USUEL_KEY+" caseinsensitivelike %@",
					new NSArray(getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere1().getText()+"*")));
			lesQualifiers.addObject(new EOOrQualifier(args));
		}

		// le qualifier PRENOM
		if(getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere2().getText().length() >= 2){
			prenomQualifier = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.PRENOM_KEY+" caseinsensitivelike %@",
					new NSArray(new Object[]{getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere2().getText()+"*"}));
			lesQualifiers.addObject(prenomQualifier);
		}

		System.out.println("lesQualifiers "+lesQualifiers);

		if (lesQualifiers.count() == 0)
			return null;
//		temValide
		lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.TEM_VALIDE_KEY+" = 'O'",null));
		EOAndQualifier monAND = new EOAndQualifier(lesQualifiers);

		return Finder.find(app, entityName, leSort, monAND);
	}

	// methode pour le parentControleur

	// accesseurs
	public NSMutableArrayDisplayGroup getIndividuDG() {
		return IndividuDG;
	}

	public void setIndividuDG(NSMutableArrayDisplayGroup individuDG) {
		IndividuDG = individuDG;
	}

	public JTableViewCocktail getIndividuTBV() {
		return individuTBV;
	}

	public void setIndividuTBV(JTableViewCocktail individuTBV) {
		this.individuTBV = individuTBV;
	}

	public SwingFinderEOIndividuUlrNib getMonAssistantRechercheIndividuNib() {
		return monAssistantRechercheIndividuNib;
	}

	public void setMonAssistantRechercheIndividuNib(
			SwingFinderEOIndividuUlrNib monAssistantRechercheIndividuNib) {
		this.monAssistantRechercheIndividuNib = monAssistantRechercheIndividuNib;
	}


	public void swingFinderAnnuler(SwingFinder s) {
		

	}


	public void swingFinderTerminer(SwingFinder s) {
		

	}
}



