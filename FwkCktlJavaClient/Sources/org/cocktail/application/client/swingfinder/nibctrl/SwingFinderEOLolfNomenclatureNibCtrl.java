/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.application.client.swingfinder.nibctrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import java.awt.Dimension;

import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOLolfNomenclatureAbstract;
import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOLolfNomenclatureRecette;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOLolfNomenclatureNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import java.awt.event.ActionListener;
/**
 *
 * @author Michael Haller
 */
public class SwingFinderEOLolfNomenclatureNibCtrl extends SwingFinderEOGenericRecord implements ActionListener {
    
    // methodes
    private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
    private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
    private static final String METHODE_ACTION_FILTRER = "actionFiltrer";

    // les tables view

    // les displaygroup
    private NSMutableArrayDisplayGroup lolfDepenseDG = new NSMutableArrayDisplayGroup();
    
    private NSMutableArray lolf;

    
    // le nib
    private SwingFinderEOLolfNomenclatureNib monSwingFinderEOLolfNomenclatureNib = null;

    private boolean useRecette=false;
    
    private String LolfEntityName=EOLolfNomenclatureDepense.ENTITY_NAME;
    
    public SwingFinderEOLolfNomenclatureNibCtrl(ApplicationCocktail ctrl, int alocation_x,
            int alocation_y, int ataille_x, int ataille_y, boolean useRecette) {
        super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
        this.useRecette=useRecette;
        if(this.useRecette)
          LolfEntityName=EOLolfNomenclatureRecette.ENTITY_NAME; 
        setWithLogs(false);
    }
    
        public void creationFenetre(SwingFinderEOLolfNomenclatureNib leSwingFinderEOLolfNomenclatureNib, String title, NibCtrl parentControleur, boolean modal) {
        super.creationFenetre(leSwingFinderEOLolfNomenclatureNib, title);
        setMonSwingFinderEOLolfNomenclatureNib(leSwingFinderEOLolfNomenclatureNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleur = parentControleur;
        bindingAndCustomization();
    }

    public void creationFenetre(SwingFinderEOLolfNomenclatureNib leSwingFinderEOLolfNomenclatureNib, String title, EOInterfaceControllerCocktail parentControleur, boolean modal) {
        super.creationFenetre(leSwingFinderEOLolfNomenclatureNib, title);
        setMonSwingFinderEOLolfNomenclatureNib(leSwingFinderEOLolfNomenclatureNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleurEONib = parentControleur;
        bindingAndCustomization();
    }
    
    private void bindingAndCustomization() {
        try {
            System.out.println("SwingFinderEOLolfNomenclatureNibCtrl.bindingAndCustomization()");
            // creation de la table view des code lolf 
            NSMutableArray lesColumnsDepense = new NSMutableArray();
            lesColumnsDepense.addObject(new ColumnData("Code", 40, JLabel.CENTER, EOLolfNomenclatureAbstract.LOLF_CODE_KEY, "String"));
            lesColumnsDepense.addObject(new ColumnData("Abrev.", 40, JLabel.CENTER, EOLolfNomenclatureAbstract.LOLF_ABREVIATION_KEY, "String"));
            lesColumnsDepense.addObject(new ColumnData("Libelle", 200, JLabel.CENTER, EOLolfNomenclatureAbstract.LOLF_LIBELLE_KEY, "String"));
            lesColumnsDepense.addObject(new ColumnData("Debut", 40, JLabel.CENTER, EOLolfNomenclatureAbstract.LOLF_OUVERTURE_KEY, "Date"));
	    lesColumnsDepense.addObject(new ColumnData("Fin", 40, JLabel.CENTER, EOLolfNomenclatureAbstract.LOLF_FERMETURE_KEY, "Date"));
            lesColumnsDepense.addObject(new ColumnData("Niveau", 40, JLabel.CENTER, EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY, "String"));
            JTableViewCocktail jtvc = new JTableViewCocktail(lesColumnsDepense,getLolfDepenseDG(),new Dimension(100,100), JTable.AUTO_RESIZE_LAST_COLUMN);
            getEOLolfNomenclatureDepenseTBV().initTableViewCocktail(jtvc);


            getEOLolfNomenclatureDepenseTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
  
            
            
            // le cablage des  objets
            getMonSwingFinderEOLolfNomenclatureNib().getJbValider().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
            getMonSwingFinderEOLolfNomenclatureNib().getJbAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
            getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepCode().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
            getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepAbrev().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
            getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepLibelle().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
            //  getMonSwingFinderEOStructureNib().getJTextFieldCocktailCritere2().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
            // les images des objets
            getMonSwingFinderEOLolfNomenclatureNib().getJbValider().setIcone(IconeCocktail.VALIDER);
            getMonSwingFinderEOLolfNomenclatureNib().getJbAnnuler().setIcone(IconeCocktail.FERMER);
            
            //sera modal lors d une transaction 
            if (parentControleurEONib == null) {
                app.addLesPanelsModal(currentNib);
            } else {
                app.addLesPanelsModal((EOInterfaceControllerCocktail) parentControleurEONib);
            }

        } 
        catch (Exception e) {
            e.printStackTrace();
            this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
                    e.getMessage(), true);
        }

    }
    
    private void initTBVLolf(){
        try {
            clean();

            NSMutableArrayDisplayGroup dg =  new NSMutableArrayDisplayGroup();
            NSMutableArray sort = new NSMutableArray();
            sort.addObject(EOSortOrdering.sortOrderingWithKey(EOLolfNomenclatureAbstract.LOLF_CODE_KEY, EOSortOrdering.CompareAscending));
            lolf = new NSMutableArray(app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(app.getAppEditingContext(), LolfEntityName, null, sort) );
            System.out.println("lolf = "+lolf.count());
            getLolfDepenseDG().addObjectsFromArray(lolf);
            getEOLolfNomenclatureDepenseTBV().refresh();
            getEOLolfNomenclatureDepenseTBV().updateUI();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
    
    public void afficherFenetre() {
        viderFiltres();
        initTBVLolf();
        super.afficherFenetre();
    }
    

    
    // methodes cablage
    public void actionSelectionner() {
	//	trace(METHODE_ACTION_FERMER);
	setResultat(new NSMutableArray(getEOLolfNomenclatureDepenseTBV().getSelectedObjects()));
	if(isModal()){
            app.addLesPanelsModal(getMonSwingFinderEOLolfNomenclatureNib());
            app.retirerLesGlassPane();
	}
	masquerFenetre();
	parentControleur.swingFinderTerminer(getSender());
    }
    
    public void actionFiltrer() {
        System.out.println("filtrer");
        String filtre="";
        NSMutableArray filtreArgs = new NSMutableArray();
        
        if(!getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepCode().getText().equals(""))
            filtre+=EOLolfNomenclatureAbstract.LOLF_CODE_KEY+" caseInsensitiveLike '*"+ getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepCode().getText() +"*'";
        
        if(!getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepAbrev().getText().equals(""))
        {
            if(!filtre.equals(""))
                filtre+=" and ";
            filtre+=EOLolfNomenclatureAbstract.LOLF_ABREVIATION_KEY+" caseInsensitiveLike '*"+ getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepAbrev().getText() +"*'";
        }
        
        if(!getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepLibelle().getText().equals(""))
        {
            if(!filtre.equals(""))
                filtre+=" and ";
            filtre+=EOLolfNomenclatureAbstract.LOLF_LIBELLE_KEY+" caseInsensitiveLike '*"+ getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepLibelle().getText() +"*'";
        }
        
        getLolfDepenseDG().removeAllObjects();
        getLolfDepenseDG().addObjectsFromArray(EOQualifier.filteredArrayWithQualifier(lolf, EOQualifier.qualifierWithQualifierFormat(filtre,null)));
        getEOLolfNomenclatureDepenseTBV().refresh();
        getEOLolfNomenclatureDepenseTBV().updateUI();
    }
    
    public void viderFiltres() {
        getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepAbrev().setText("");
        getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepCode().setText("");
        getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepLibelle().setText("");
        
    }
    
    private void clean(){
        lolfDepenseDG.removeAllObjects();
        getEOLolfNomenclatureDepenseTBV().refresh();
    }


    public NSMutableArrayDisplayGroup getLolfDepenseDG() {
        return lolfDepenseDG;
    }

    public void setLolfDepenseDG(NSMutableArrayDisplayGroup lolfDepenseDG) {
        this.lolfDepenseDG = lolfDepenseDG;
    }

    public SwingFinderEOLolfNomenclatureNib getMonSwingFinderEOLolfNomenclatureNib() {
        return monSwingFinderEOLolfNomenclatureNib;
    }

    public void setMonSwingFinderEOLolfNomenclatureNib(SwingFinderEOLolfNomenclatureNib monSwingFinderEOLolfNomenclatureNib) {
        this.monSwingFinderEOLolfNomenclatureNib = monSwingFinderEOLolfNomenclatureNib;
    }

    public NSArray getResultat() {
        //clean();
        return resltat;
    }

    public void setResultat(NSMutableArray resltat) {
        this.resltat = resltat;
    }
    
    public void actionPerformed(ActionEvent e) {
        
       /*if(e.getSource().equals(getMonSwingFinderEOLolfNomenclatureNib().getJfRechercheDepCode()) && e.getModifiers()!=0)
       {
            actionFiltrer();
       }*/
    }
    
    public JTableViewCocktail getEOLolfNomenclatureDepenseTBV() {
        return getMonSwingFinderEOLolfNomenclatureNib().getJtvLolfDepense();
    }

    public void setEOLolfNomenclatureDepenseTBV(JTableViewCocktail lolfDepenseTBV) {
        getMonSwingFinderEOLolfNomenclatureNib().getJtvLolfDepense().initTableViewCocktail(lolfDepenseTBV);
    }
    
}
