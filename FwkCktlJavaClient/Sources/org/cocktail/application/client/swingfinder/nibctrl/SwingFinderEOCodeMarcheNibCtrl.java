/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOCodeMarche;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.client.swingfinder.SwingFinderCodeMarche;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOCodeMarcheNib;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOCodeMarcheNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SwingFinderEOCodeMarcheNibCtrl  extends SwingFinderEOGenericRecord {

	//NSMutableArray resltat = new NSMutableArray();
	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_FILTRER = "actionFiltrer";
	private static final String METHODE_ACTION_OPEN_TREE_MODE = "actionOpenTreeMode";

	// les tables view
	private JTableViewCocktail codeMarcheTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup CodeMarcheDG = new NSMutableArrayDisplayGroup();

	// le nib
	private SwingFinderEOCodeMarcheNib monAssistantRechercheEOCodeMarcheNib = null;
    
    private EOExercice exercice=null;

	// parentControleur
	//NibCtrl parentControleur = null;


//	public void setSender(SwingFinder swingfinder) {
//	this.swingfinder= swingfinder;
//	}


//	public SwingFinder getSender() {
//	return swingfinder;
//	}


	private SwingFinder swingfinder=null;

	private boolean modal = false;

	public boolean isModal() {
		return modal;
	}

	public SwingFinderEOCodeMarcheNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);
	}

	public void creationFenetre(SwingFinderEOCodeMarcheNib leAssistantRechercheIndividuNib, String title,NibCtrl parentControleur,boolean modal) {
		super.creationFenetre(leAssistantRechercheIndividuNib, title);
		setMonAssistantRechercheEOCodeMarcheNib(leAssistantRechercheIndividuNib);
		setNibCtrlLocation(LOCATION_MIDDLE);

		this.modal=modal;
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
		}

	public void creationFenetre(SwingFinderEOCodeMarcheNib leAssistantRechercheIndividuNib, String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
		super.creationFenetre(leAssistantRechercheIndividuNib, title);
		setMonAssistantRechercheEOCodeMarcheNib(leAssistantRechercheIndividuNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		this.modal=modal;
		this.parentControleurEONib =  parentControleur;
		bindingAndCustomization();
	}

	public void setResultat() {
		
		resltat = new NSMutableArray(getCodeMarcheTBV().getSelectedObjects());
	}

	private void bindingAndCustomization() {

		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Code", 100,JLabel.CENTER, EOCodeMarche.CM_CODE_KEY, "String"));
			lesColumns.addObject(new ColumnData("Libelle", 100,JLabel.CENTER, EOCodeMarche.CM_LIB_KEY, "String"));


			setCodeMarcheTBV(new JTableViewCocktail(lesColumns, getCodeMarcheDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			//getMonAssistantRechercheEOCodeMarcheNib().getJPanelResultat().add(getCodeMarcheTBV());
			getMonAssistantRechercheEOCodeMarcheNib().getJTableResultat().initTableViewCocktail(getCodeMarcheTBV());
			// multi ou mono selection dans les tbv
			getCodeMarcheTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// le cablage des  objets
			getMonAssistantRechercheEOCodeMarcheNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			getMonAssistantRechercheEOCodeMarcheNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonAssistantRechercheEOCodeMarcheNib().getJButtonCocktailFiltrer().addDelegateActionListener(this, METHODE_ACTION_FILTRER);
            getMonAssistantRechercheEOCodeMarcheNib().getJTextFieldCocktailCritere1().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
            getMonAssistantRechercheEOCodeMarcheNib().getJTextFieldCocktailCritere1().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
            getMonAssistantRechercheEOCodeMarcheNib().getBtTreeview().addDelegateActionListener(this, METHODE_ACTION_OPEN_TREE_MODE);
			// les images des objets
			getMonAssistantRechercheEOCodeMarcheNib().getJButtonCocktailSelectionner().setIcone("valider16");
			getMonAssistantRechercheEOCodeMarcheNib().getJButtonCocktailAnnuler().setIcone("close_view");
			getMonAssistantRechercheEOCodeMarcheNib().getJButtonCocktailFiltrer().setIcone("linkto_help");
			
			
			getMonAssistantRechercheEOCodeMarcheNib().getBtTableview().setIcone(IconeCocktail.TABLE);
			getMonAssistantRechercheEOCodeMarcheNib().getBtTreeview().setIcone(IconeCocktail.TREEVIEW);
			getMonAssistantRechercheEOCodeMarcheNib().getBtTableview().setText("");
			getMonAssistantRechercheEOCodeMarcheNib().getBtTreeview().setText("");

			//sera modal lors d une transaction 
			if (parentControleurEONib == null)
				app.addLesPanelsModal(currentNib);
			else
				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}

		getCodeMarcheTBV().refresh();

	}

	private void setAssistantRechercheEOCodeMarcheDG() {

		getCodeMarcheDG().removeAllObjects();
		if (getCodeMarcheDG() != null)
			getCodeMarcheTBV().refresh();	

		try {

			getCodeMarcheDG().addObjectsFromArray(findLesCodeMarche(app));

			if (getCodeMarcheTBV() != null)
				getCodeMarcheTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}

	}





//	// methodes cablage
//	public void actionAnnuler() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray();
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantRechercheEOCodeMarcheNib());
//	app.retirerLesGlassPane();
//	}

//	masquerFenetre();
//	parentControleur.swingFinderAnnuler(getSender());
//	}

//	// methodes cablage
//	public void actionSelectionner() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray(getCodeMarcheTBV().getSelectedObjects());
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantRechercheEOCodeMarcheNib());
//	app.retirerLesGlassPane();
//	}
//	masquerFenetre();
//	parentControleur.swingFinderTerminer(getSender());
//	}

	// methodes cablage
	public void actionFiltrer() {
		//	trace(METHODE_ACTION_FERMER);
		setAssistantRechercheEOCodeMarcheDG();
	}

	public NSArray getResultat(){

		getMonAssistantRechercheEOCodeMarcheNib().getJTextFieldCocktailCritere1().setText("");
		getMonAssistantRechercheEOCodeMarcheNib().getJTextFieldCocktailCritere2().setText("");
		getCodeMarcheDG().removeAllObjects();
		getCodeMarcheTBV().refresh();	
		return resltat;
	}

	//finder
	public  NSArray findLesCodeMarche(ApplicationCocktail app) {
		String entityName =EOCodeExer.ENTITY_NAME;
		NSArray leSort = null;

		EOQualifier nomQualifier = null;
		EOQualifier prenomQualifier = null;

		NSMutableArray lesQualifiers = new NSMutableArray();

		// le sort 
		leSort =new NSArray(new Object[]{
				EOSortOrdering.sortOrderingWithKey(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending),
				EOSortOrdering.sortOrderingWithKey(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_LIB_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)
		});

		// le qualifier NOM
		if(getMonAssistantRechercheEOCodeMarcheNib().getJTextFieldCocktailCritere1().getText().length() >= 1){
			nomQualifier = EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY+" caseinsensitivelike %@",
					new NSArray(getMonAssistantRechercheEOCodeMarcheNib().getJTextFieldCocktailCritere1().getText()+"*"));
			lesQualifiers.addObject(nomQualifier);
		}

		// le qualifier PRENOM
		if(getMonAssistantRechercheEOCodeMarcheNib().getJTextFieldCocktailCritere2().getText().length() >= 2){
			prenomQualifier = EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_LIB_KEY+" caseinsensitivelike %@",
					new NSArray(getMonAssistantRechercheEOCodeMarcheNib().getJTextFieldCocktailCritere2().getText()+"*"));
			lesQualifiers.addObject(prenomQualifier);
		}
        
        if(exercice!=null)
        {
            lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.EXERCICE_KEY+"."+EOExercice.EXE_ORDRE_KEY+" = %@",new NSArray(exercice.exeOrdre())));
        }

		System.out.println("lesQualifiers "+lesQualifiers);

		if (lesQualifiers.count() == 0)
			return null;

		EOAndQualifier monAND = new EOAndQualifier(lesQualifiers);
		System.out.println("monAND "+monAND);
		NSArray list = Finder.find(app, entityName, null, monAND);
		System.out.println("list "+list);
        if(list != null)
            return (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(list, leSort).valueForKey(EOCodeExer.CODE_MARCHE_KEY);
        return null;
	}

	// methode pour le parentControleur

	// accesseurs
	public NSMutableArrayDisplayGroup getCodeMarcheDG() {
		return CodeMarcheDG;
	}
    
    public void afficherFenetre() {
        afficherFenetre(app.getCurrentExercice());
    }
    
    public void afficherFenetre(EOExercice exercice){
        this.exercice = exercice;
        resltat = null;
        super.afficherFenetre();
    }

	public void setCodeMarcheDG(NSMutableArrayDisplayGroup CodeMarcheDG) {
		this.CodeMarcheDG = CodeMarcheDG;
	}

	public JTableViewCocktail getCodeMarcheTBV() {
		return codeMarcheTBV;
	}

	public void setCodeMarcheTBV(JTableViewCocktail codeMarcheTBV) {
		this.codeMarcheTBV = codeMarcheTBV;
	}

	public SwingFinderEOCodeMarcheNib getMonAssistantRechercheEOCodeMarcheNib() {
		return monAssistantRechercheEOCodeMarcheNib;
	}

	public void setMonAssistantRechercheEOCodeMarcheNib(
			SwingFinderEOCodeMarcheNib monAssistantRechercheEOCodeMarcheNib) {
		this.monAssistantRechercheEOCodeMarcheNib = monAssistantRechercheEOCodeMarcheNib;
	}

    public void actionOpenTreeMode(){
    	actionAnnuler();
    	((SwingFinderCodeMarche)getSender()).setUseTreeView(true);
		((SwingFinderCodeMarche)getSender()).afficherFenetreFinder(null, exercice);
    }

}
