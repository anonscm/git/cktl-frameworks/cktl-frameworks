
/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOStructureUlr;
import org.cocktail.application.client.eof.EOTypeGroupe;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOStructureNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class SwingFinderEOStructureNibCtrl extends SwingFinderEOGenericRecord implements ActionListener{

    //NSMutableArray resltat = new NSMutableArray();
    // methodes
    private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
    private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
    private static final String METHODE_ACTION_FILTRER = "actionFiltrer";
    private static final String METHODE_ACTION_ADRESSE = "actionFiltrerAdresse";

    // les tables view
//    private JTableViewCocktail structureTBV = null;

    // les displaygroup
    private NSMutableArrayDisplayGroup structureDG = new NSMutableArrayDisplayGroup();
    private NSMutableArrayDisplayGroup adresseDG = new NSMutableArrayDisplayGroup();
    private NSMutableArrayDisplayGroup telDG = new NSMutableArrayDisplayGroup();

    // le nib
    private SwingFinderEOStructureNib monSwingFinderEOStructureNib = null;

    // parentControleur
//	NibCtrl parentControleur = null;
//	public void setSender(SwingFinder swingfinder) {
//	this.swingfinder= swingfinder;
//	}
//	public SwingFinder getSender() {
//	return swingfinder;
//	}
    //private SwingFinder swingfinder=null;

    //private boolean modal = false;

//	public boolean isModal() {
//	return modal;
//	}
    public SwingFinderEOStructureNibCtrl(ApplicationCocktail ctrl, int alocation_x,
            int alocation_y, int ataille_x, int ataille_y) {
        super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

        setWithLogs(false);
    }

    public void creationFenetre(SwingFinderEOStructureNib leSwingFinderEOStructureNib, String title, NibCtrl parentControleur, boolean modal) {
        super.creationFenetre(leSwingFinderEOStructureNib, title);
        setMonEOStructureNib(leSwingFinderEOStructureNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleur = parentControleur;
        bindingAndCustomization();
    }

    public void creationFenetre(SwingFinderEOStructureNib leSwingFinderEOStructureNib, String title, EOInterfaceControllerCocktail parentControleur, boolean modal) {
        super.creationFenetre(leSwingFinderEOStructureNib, title);
        setMonEOStructureNib(leSwingFinderEOStructureNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleurEONib = parentControleur;
        bindingAndCustomization();
    }


//	public void afficherFenetre(){
//	super.afficherFenetre();
//	if(isModal()){
//	app.removeFromLesPanelsModal(getMonAssistantRechercheIndividuNib());
//	app.activerLesGlassPane();
//	}
//	}
    public void setResultat() {
        
        resltat = new NSMutableArray(getStructureTBV().getSelectedObjects());
    }

    private void bindingAndCustomization() {

        try {
            System.out.println("SwingFinderEOStructureNibCtrl.bindingAndCustomization()");
            // creation de la table view des structures
            NSMutableArray lesColumns = new NSMutableArray();
            lesColumns.addObject(new ColumnData("Code", 80, JLabel.CENTER, "cStructure", "String"));
            lesColumns.addObject(new ColumnData("Nom", 200, JLabel.CENTER, "llStructure", "String"));
            lesColumns.addObject(new ColumnData("Abrev.", 80, JLabel.CENTER, "lcStructure", "String"));
            lesColumns.addObject(new ColumnData("Valide", 80, JLabel.CENTER, "temValide", "String"));

            setStructureTBV(new JTableViewCocktail(lesColumns, getStructureDG(), new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
            getStructureTBV().addDelegateSelectionListener(this, METHODE_ACTION_ADRESSE);
            getStructureTBV().addDelegateKeyListener(this, METHODE_ACTION_ADRESSE);
//            getMonSwingFinderEOStructureNib().getJTable().add(getStructureTBV());

            // multi ou mono selection dans les tbv
            getStructureTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

            // creation de la table view des structures
            NSMutableArray lesColumns2 = new NSMutableArray();
            lesColumns2.addObject(new ColumnData("Adresse", 200, JLabel.CENTER, "adresse.afficheAdresse", "String"));
            lesColumns2.addObject(new ColumnData("Email", 100, JLabel.CENTER, "eMail", "String"));
            lesColumns2.addObject(new ColumnData("Principal", 80, JLabel.CENTER, "rpaPrincipal", "String"));
            lesColumns2.addObject(new ColumnData("Type", 80, JLabel.CENTER, "tadrCode", "String"));
            
            JTableViewCocktail jtvc = new JTableViewCocktail(lesColumns2, adresseDG, new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN);
            getMonSwingFinderEOStructureNib().getJtvcAdresse().initTableViewCocktail(jtvc);
            
                        // creation de la table view des structures
            NSMutableArray lesColumns3 = new NSMutableArray();
            lesColumns3.addObject(new ColumnData("Telephone", 200, JLabel.CENTER, "noTelephone", "String"));
            lesColumns3.addObject(new ColumnData("Tel/Fax", 80, JLabel.CENTER, "typeNo", "String"));
            lesColumns3.addObject(new ColumnData("Type", 100, JLabel.CENTER, "typeTelephone", "String"));
            
            JTableViewCocktail jtvc2 = new JTableViewCocktail(lesColumns3, telDG, new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN);
            getMonSwingFinderEOStructureNib().getJtvcTelephone().initTableViewCocktail(jtvc2);
            
            initCBCTypeStr();
            // le cablage des  objets
            getMonSwingFinderEOStructureNib().getJbValider().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
            getMonSwingFinderEOStructureNib().getJbAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
            getMonSwingFinderEOStructureNib().getJbFiltre().addDelegateActionListener(this, METHODE_ACTION_FILTRER);
            getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().addDelegateKeyListener(this, METHODE_ACTION_FILTRER);
//            getMonSwingFinderEOStructureNib().getJTextFieldCocktailCritere2().addDelegateKeyListener(this,METHODE_ACTION_FILTRER);
            // les images des objets
            getMonSwingFinderEOStructureNib().getJbValider().setIcone(IconeCocktail.VALIDER);
            getMonSwingFinderEOStructureNib().getJbAnnuler().setIcone(IconeCocktail.FERMER);
            getMonSwingFinderEOStructureNib().getJbFiltre().setIcone(IconeCocktail.FILTRER);

            //sera modal lors d une transaction 
            if (parentControleurEONib == null) {
                app.addLesPanelsModal(currentNib);
            } else {
                app.addLesPanelsModal((EOInterfaceControllerCocktail) parentControleurEONib);
            }

        } catch (Exception e) {
            e.printStackTrace();
            this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
                    e.getMessage(), true);
        }

        getStructureTBV().refresh();

    }
    
    private void initCBCTypeStr(){
        try {
            getMonSwingFinderEOStructureNib().getJcbcTypeStr().removeAllItems();
            getMonSwingFinderEOStructureNib().getJcbcTypeStr().addItem("Tous");
            NSMutableArrayDisplayGroup dg = new NSMutableArrayDisplayGroup();
//            EOClassDescription.classDescriptionForEntityName("ca_TypeGroupe");
//            EOTypeGroupe tous = new EOTypeGroupe();
//            tous.setTgrpLibelle("Tous");
            dg.addObject(EOTypeGroupe.getTypeGroupeTous());
            String entityName = "ca_TypeGroupe";
            NSMutableArray sort = new NSMutableArray();
            sort.addObject(EOSortOrdering.sortOrderingWithKey("tgrpLibelle", EOSortOrdering.CompareAscending));
            NSArray types = app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(app.getAppEditingContext(), entityName, null, sort);
            System.out.println("types = "+types.count());
            dg.addObjectsFromArray(types);
            getMonSwingFinderEOStructureNib().getJcbcTypeStr().setDisplayGroup(dg);
            getMonSwingFinderEOStructureNib().getJcbcTypeStr().setItemsWithKey("tgrpLibelle");
            getMonSwingFinderEOStructureNib().getJcbcTypeStr().updateUI();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void setAssistantRechercheStructureDG() {

        getStructureDG().removeAllObjects();
        if (getStructureDG() != null) {
            getStructureTBV().refresh();
        }

        try {
            NSArray array = findLesStructure();
            if (array == null || array.count() == 0) {
                System.out.println("----->0");
            } else {
                System.out.println("---->" + array.count());
            }
            getStructureDG().addObjectsFromArray(array);

            if (getStructureTBV() != null) {
                getStructureTBV().refresh();
            } else {
                System.out.println("Rien !!!");
            }
        } catch (Exception e) {
            fenetreDeDialogueInformation("PROBLEME ! \n" + e.toString());
        }

    }

    public void afficherFenetre() {
//        getMonSwingFinderEOStructureNib().getJcbcTypeStr().addActionListener(this);
        clean();
        super.afficherFenetre();
    }
    
    

//	// methodes cablage
//	public void actionAnnuler() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray();
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantRechercheIndividuNib());
//	app.retirerLesGlassPane();
//	}

//	masquerFenetre();
//	parentControleur.swingFinderAnnuler(getSender());
//	}

//	// methodes cablage
//	public void actionSelectionner() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray(individuTBV.getSelectedObjects());
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantRechercheIndividuNib());
//	app.retirerLesGlassPane();
//	}
//	masquerFenetre();
//	parentControleur.swingFinderTerminer(getSender());
//	}

    // methodes cablage
    public void actionFiltrer() {
        //	trace(METHODE_ACTION_FERMER);
        clean();
        getMonSwingFinderEOStructureNib().getJtvcAdresse().refresh();
        getMonSwingFinderEOStructureNib().getJtvcTelephone().refresh();
        setAssistantRechercheStructureDG();
        actionFiltrerAdresse();
    }
    
    private void clean(){
        structureDG.removeAllObjects();
        adresseDG.removeAllObjects();
        telDG.removeAllObjects();
        getMonSwingFinderEOStructureNib().getJtvcStructure().refresh();
        getMonSwingFinderEOStructureNib().getJtvcAdresse().refresh();
        getMonSwingFinderEOStructureNib().getJtvcTelephone().refresh();
    }
    
    public void actionFiltrerAdresse(){
        if(getStructureTBV().getSelectedObjects()==null || getStructureTBV().getSelectedObjects().count()==0)
        {
            return;
        }
        EOStructureUlr str = (EOStructureUlr)getStructureTBV().getSelectedObjects().lastObject();
        adresseDG.setArray(str.tosRepartPersonneAdresses());
        getMonSwingFinderEOStructureNib().getJtvcAdresse().refresh();
        telDG.setArray(str.tosVPersonneTelephones());
        getMonSwingFinderEOStructureNib().getJtvcTelephone().refresh();
    }

    public NSArray getResultat() {

        getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().setText("");
//		getMonAssistantRechercheIndividuNib().getJTextFieldCocktailCritere2().setText("");
        getStructureDG().removeAllObjects();
        getStructureTBV().refresh();
        return resltat;
    }
    
    public NSArray getResultatRepartAdresse(){
        if(getMonSwingFinderEOStructureNib().getJtvcAdresse().getData().count()==1)
            return getMonSwingFinderEOStructureNib().getJtvcAdresse().getData();
        return getMonSwingFinderEOStructureNib().getJtvcAdresse().getSelectedObjects();
    }
    
    public NSArray getResultatTelephone(){
        if(getMonSwingFinderEOStructureNib().getJtvcTelephone().getData().count()==1)
            return getMonSwingFinderEOStructureNib().getJtvcTelephone().getData();
        return getMonSwingFinderEOStructureNib().getJtvcTelephone().getSelectedObjects();
    }

    //finder
    public NSArray findLesStructure() {
        String entityName = "ca_StructureUlr";
        NSArray leSort = null;

        EOQualifier nomQualifier = null;
        EOQualifier prenomQualifier = null;

        NSMutableArray lesQualifiers = new NSMutableArray();
        System.out.println("SwingFinderEOStructureNibCtrl.findLesStructure()");
        try {
            // le sort 
            leSort = new NSArray(new Object[]{
                EOSortOrdering.sortOrderingWithKey("llStructure", EOSortOrdering.CompareCaseInsensitiveAscending),
                EOSortOrdering.sortOrderingWithKey("lcStructure", EOSortOrdering.CompareCaseInsensitiveAscending)
            });

            // le qualifier llStructure
            if (getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().getText().length() >= 2) {
                nomQualifier = EOQualifier.qualifierWithQualifierFormat("llStructure caseInsensitiveLike %@",
                        new NSArray(new Object[]{"*" + getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().getText() + "*"}));
                lesQualifiers.addObject(nomQualifier);
            }
            //
            //		// le qualifier lcStructure
            if (getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().getText().length() >= 2) {
                prenomQualifier = EOQualifier.qualifierWithQualifierFormat("lcStructure caseInsensitiveLike %@",
                        new NSArray(new Object[]{"*" + getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().getText() + "*"}));
                lesQualifiers.addObject(prenomQualifier);
            }

            //le qualifier 
            if (getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().getText().length() >= 2) {
                prenomQualifier = EOQualifier.qualifierWithQualifierFormat("cStructure caseInsensitiveLike %@",
                        new NSArray(new Object[]{"*" + getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().getText() + "*"}));
                lesQualifiers.addObject(prenomQualifier);
            }
            
            if (getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().getText().length() >= 2) {
                prenomQualifier = EOQualifier.qualifierWithQualifierFormat("cStructure caseInsensitiveLike %@",
                        new NSArray(new Object[]{"*" + getMonSwingFinderEOStructureNib().getJTextFieldCocktail1().getText() + "*"}));
                lesQualifiers.addObject(prenomQualifier);
            }

            System.out.println("lesQualifiers " + lesQualifiers);

            if (lesQualifiers.count() == 0) {
                System.out.println("Pas de qualifier !!!");
                return null;
            }

            EOQualifier monOR = new EOOrQualifier(lesQualifiers);
            
            if(getMonSwingFinderEOStructureNib().getJcbcTypeStr().getSelectedEOG()!=null
                    && !EOTypeGroupe.getTypeGroupeTous().equals(getMonSwingFinderEOStructureNib().getJcbcTypeStr().getSelectedEOG()))
            {
                EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("tosRepartTypeGroupes.typeGroupe = %@", new NSArray(getMonSwingFinderEOStructureNib().getJcbcTypeStr().getSelectedEOG()));
                monOR = new EOAndQualifier(new NSArray((new Object[]{monOR,qual})));
            }
            
            NSArray array = app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(app.getAppEditingContext(), entityName, monOR, leSort);
           // NSArray array = Finder.find(app,app.getAppEditingContext(), entityName, leSort, monOR);
            if (array == null || array.count() == 0) {
                System.out.println("--> 0");
            }
            return array;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // methode pour le parentControleur

    // accesseurs
    public NSMutableArrayDisplayGroup getStructureDG() {
        return structureDG;
    }

    public void setStructureDG(NSMutableArrayDisplayGroup leDisplayGroup) {
        structureDG = leDisplayGroup;
    }

    public JTableViewCocktail getStructureTBV() {
        return getMonSwingFinderEOStructureNib().getJtvcStructure();
    }

    public void setStructureTBV(JTableViewCocktail structureTBV) {
        getMonSwingFinderEOStructureNib().getJtvcStructure().initTableViewCocktail(structureTBV);
    }

    public SwingFinderEOStructureNib getMonSwingFinderEOStructureNib() {
        return monSwingFinderEOStructureNib;
    }

    public void setMonEOStructureNib(
            SwingFinderEOStructureNib monSwingFinderEOStructureNib) {
        if (monSwingFinderEOStructureNib == null) {
            System.out.println("insert null !!!!");
        }
        this.monSwingFinderEOStructureNib = monSwingFinderEOStructureNib;
    }

    public void swingFinderAnnuler(SwingFinder s) {
    

    }

    public void swingFinderTerminer(SwingFinder s) {
    

    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(getMonSwingFinderEOStructureNib().getJcbcTypeStr()) && e.getModifiers()!=0)
        {
            actionFiltrer();
        }
    }
}



