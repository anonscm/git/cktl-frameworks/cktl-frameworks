/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 8 oct. 07
 * author mparadot 
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOCodeMarche;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swingfinder.SwingFinderCodeMarche;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOCodeMarcheTreeNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTreeCocktail;
import org.cocktail.application.palette.interfaces.JTreeObjectCocktail;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SwingFinderEOCodeMarcheTreeNibCtrl extends SwingFinderEOGenericRecord {
   
        // methodes
        private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
        private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
        private static final String METHODE_ACTION_OPEN_TABLE_MODE = "actionShowTableMode";
        
        //pour le treeView
        private JTreeObjectCocktail delegate;
        private JTreeCocktail tree;
        
        // le nib
        private SwingFinderEOCodeMarcheTreeNib monSwingFinderEOCodeMarcheTreeNib = null;
        
        //le resultat
//        NSMutableArray resultat = null;
        NSArray listNode = null;
        
        //L'exercice utiliser
        EOExercice exercice;
        
        
        public SwingFinderEOCodeMarcheTreeNibCtrl(ApplicationCocktail ctrl, int alocation_x,
                int alocation_y, int ataille_x, int ataille_y) {
            super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
            setWithLogs(false);
            resltat = new NSMutableArray();
            delegate = new ObjectMarche();
        }
        
        public void creationFenetre(SwingFinderEOCodeMarcheTreeNib leSwingFinderEOCodeMarcheTreeNib,String title,NibCtrl parentControleur,boolean modal) {
            super.creationFenetre(leSwingFinderEOCodeMarcheTreeNib, title);
            setMonSwingFinderEOCodeMarcheTreeNib(leSwingFinderEOCodeMarcheTreeNib);
            setNibCtrlLocation(LOCATION_MIDDLE);
            setModal(modal);
            this.parentControleur =  parentControleur;
            bindingAndCustomization();
        }
        
        public void creationFenetre(SwingFinderEOCodeMarcheTreeNib leSwingFinderEOCodeMarcheTreeNib,String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
            super.creationFenetre(leSwingFinderEOCodeMarcheTreeNib, title);
            setMonSwingFinderEOCodeMarcheTreeNib(leSwingFinderEOCodeMarcheTreeNib);
            setNibCtrlLocation(LOCATION_MIDDLE);
            setModal(modal);
            this.parentControleurEONib =  parentControleur;
            bindingAndCustomization();
        }

        public SwingFinderEOCodeMarcheTreeNib getMonSwingFinderEOCodeMarcheTreeNib() {
            return monSwingFinderEOCodeMarcheTreeNib;
        }

        public void setMonSwingFinderEOCodeMarcheTreeNib(SwingFinderEOCodeMarcheTreeNib monSwingFinderEOCodeMarcheTreeNib) {
            this.monSwingFinderEOCodeMarcheTreeNib = monSwingFinderEOCodeMarcheTreeNib;
        }
        
        public void afficherFenetre(EOExercice unExercice) {
            this.exercice = unExercice;
            resltat = null;
            majRootNode();
            super.afficherFenetre();
        }
        
        public void majRootNode(){
            NSMutableArray args = new NSMutableArray();
            args.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_NIVEAU_KEY+" = 1", null));
            if(exercice!=null)
                args.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.EXERCICE_KEY+"."+EOExercice.EXE_ORDRE_KEY+" = %@", new NSArray(new Integer(exercice.exeOrdre().intValue()))));   
            else
            	args.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.EXERCICE_KEY+"."+EOExercice.EXE_ORDRE_KEY+" = %@", new NSArray(((ApplicationCocktail)EOApplication.sharedApplication()).getCurrentExercice().exeOrdre())));
            NSMutableArray sort = new NSMutableArray();
            sort.addObject(EOSortOrdering.sortOrderingWithKey(EOCodeMarche.CM_CODE_KEY, EOSortOrdering.CompareAscending));
            sort.addObject(EOSortOrdering.sortOrderingWithKey(EOCodeMarche.CM_LIB_KEY, EOSortOrdering.CompareAscending));
            NSMutableArray list = new NSMutableArray();
            list.addObjectsFromArray(app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(EOCodeExer.ENTITY_NAME, new EOAndQualifier(args), null));
            if(list==null||list.count()==0)
            {
            	listNode = new NSArray();
            }
            listNode = EOSortOrdering.sortedArrayUsingKeyOrderArray((NSArray)list.valueForKey(EOCodeExer.CODE_MARCHE_KEY), sort);
        }
        
        public void afficherFenetre() {
            afficherFenetre(null);
        }
        
        public void setResultat() {
            resltat = new NSMutableArray();
            if(tree!=null && tree.getSelectedObject()!=null)
                resltat.addObject(tree.getSelectedObject());
        }
        
        private void bindingAndCustomization() {
            
            try {
                // le cablage des  objets
                getMonSwingFinderEOCodeMarcheTreeNib().getBtAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
                getMonSwingFinderEOCodeMarcheTreeNib().getBtValider().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
                
                getMonSwingFinderEOCodeMarcheTreeNib().getJbtcTable().addDelegateActionListener(this, METHODE_ACTION_OPEN_TABLE_MODE);
                
                // les images des objets
                getMonSwingFinderEOCodeMarcheTreeNib().getBtValider().setIcone(IconeCocktail.VALIDER);
                getMonSwingFinderEOCodeMarcheTreeNib().getBtAnnuler().setIcone(IconeCocktail.FERMER);
                
                getMonSwingFinderEOCodeMarcheTreeNib().getJbtcTable().setIcone(IconeCocktail.TABLE);
                getMonSwingFinderEOCodeMarcheTreeNib().getJbtcTreeview().setIcone(IconeCocktail.TREEVIEW);
                getMonSwingFinderEOCodeMarcheTreeNib().getJbtcTable().setText("");
                getMonSwingFinderEOCodeMarcheTreeNib().getJbtcTreeview().setText("");
                
                //creation du treeView
                tree = new JTreeCocktail(delegate);
                JScrollPane pane = new JScrollPane(tree);//, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

                getMonSwingFinderEOCodeMarcheTreeNib().getTreeView().removeAll();
                getMonSwingFinderEOCodeMarcheTreeNib().getTreeView().setLayout(new BorderLayout());
                getMonSwingFinderEOCodeMarcheTreeNib().getTreeView().add(pane, BorderLayout.CENTER);
                getMonSwingFinderEOCodeMarcheTreeNib().getTreeView().validate();
                getMonSwingFinderEOCodeMarcheTreeNib().getTreeView().setVisible(true);
                
//                getMonSwingFinderEOCodeMarcheTreeNib().jtreeView = tree;
                
                //dans le super
//                //sera modal lors d une transaction
//                if (parentControleurEONib == null)
//                    app.addLesPanelsModal(currentNib);
//                else
//                    app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);
                
                // obseration des changements de selection de lexercice :
//                app.getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");
                
            } catch (Exception e) {
                this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
                        e.getMessage(), true);
            }

        }
//        
//        public NSArray getResultat(){
//            return resltat;
//        }
        
        public void actionShowTableMode(){
        	actionAnnuler();
        	((SwingFinderCodeMarche)getSender()).setUseTreeView(false);
    		((SwingFinderCodeMarche)getSender()).afficherFenetreFinder(null, exercice);
        }
        
        class ObjectMarche implements JTreeObjectCocktail{
            
            public NSArray getChilds(Object ob) {
                if(ob==null)
                    return null;
                return ((EOCodeMarche)ob).codeMarchesFils();
            }

            public NSArray getRootsNode() {
            	if(listNode==null)
            		majRootNode();
            	return listNode;
            }

            public String toString(Object ob) {
            	if(ob==null)
            		return "";
                return ((EOCodeMarche)ob).cmCode()+" "+((EOCodeMarche)ob).cmLib();
            }
            
            public boolean isSelectableObject(Object ob) {
//                NSArray list = (NSArray)app.getMesUtilisateurOrgan().valueForKey("organ");
//                return !((list!=null) && (list.indexOfIdenticalObject(ob)==NSArray.NotFound));
                return true;
            }
        }
//
//		public EOExercice getExercice() {
//			return exercice;
//		}
//
//		public void setExercice(EOExercice exercice) {
//			this.exercice = exercice;
//		}
}
