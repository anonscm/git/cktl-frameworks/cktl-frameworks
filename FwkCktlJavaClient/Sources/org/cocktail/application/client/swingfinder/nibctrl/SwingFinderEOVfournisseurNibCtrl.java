/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOVfournisseurNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.VFournisseur;
import org.cocktail.application.client.inspecteur.SwingInspecteurFournisseur;

public class SwingFinderEOVfournisseurNibCtrl extends SwingFinderEOGenericRecord {
	//NSMutableArray resltat = new NSMutableArray();
	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_FILTRER = "actionFiltrer";
        private static final String METHODE_AFFICHE_INSPECTEUR_FOUR = "afficheInspecteurFournisseur";

	// les tables view
	private JTableViewCocktail resultatTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup resultatDG = new NSMutableArrayDisplayGroup();

	// le nib
	private SwingFinderEOVfournisseurNib monSwingFinderEOVfournisseurNib = null;
        
        private SwingInspecteurFournisseur monSwingInspecteurFournisseur = null;

	// parentControleur
	//NibCtrl parentControleur = null;


//	public void setSender(SwingFinder swingfinder) {
//	this.swingfinder= swingfinder;
//	}


//	public SwingFinder getSender() {
//	return swingfinder;
//	}


//	private SwingFinder swingfinder=null;

//	private boolean modal = false;

//	public boolean isModal() {
//	return modal;
//	}

	public SwingFinderEOVfournisseurNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);
	}


	public void creationFenetre(SwingFinderEOVfournisseurNib leSwingFinderEOVfournisseurNib, String title,NibCtrl parentControleur,boolean modal) {
		super.creationFenetre(leSwingFinderEOVfournisseurNib, title);
		setMonSwingFinderEOVfournisseurNib(leSwingFinderEOVfournisseurNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
	}


	public void creationFenetre(SwingFinderEOVfournisseurNib leSwingFinderEOVfournisseurNib, String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
		super.creationFenetre(leSwingFinderEOVfournisseurNib, title);
		setMonSwingFinderEOVfournisseurNib(leSwingFinderEOVfournisseurNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleurEONib =  parentControleur;
		bindingAndCustomization();
	}

//	public void afficherFenetre(){
//	super.afficherFenetre();
//	if(isModal()){
//	app.removeFromLesPanelsModal(getMonSwingFinderEOVfournisseurNib());
//	app.activerLesGlassPane();
//	}
//	}


	public SwingFinderEOVfournisseurNib getMonSwingFinderEOVfournisseurNib() {
		return monSwingFinderEOVfournisseurNib;
	}

	public void setMonSwingFinderEOVfournisseurNib(
			SwingFinderEOVfournisseurNib monSwingFinderEOVfournisseurNib) {
		this.monSwingFinderEOVfournisseurNib = monSwingFinderEOVfournisseurNib;
	}

	public NSMutableArrayDisplayGroup getResultatDG() {
		return resultatDG;
	}

	public void setResultatDG(NSMutableArrayDisplayGroup resultatDG) {
		this.resultatDG = resultatDG;
	}

	public JTableViewCocktail getResultatTBV() {
		return resultatTBV;
	}

	public void setResultatTBV(JTableViewCocktail resultatTBV) {
		this.resultatTBV = resultatTBV;
	}

	public void setResultat() {
		
		resltat = new NSMutableArray(getResultatTBV().getSelectedObjects());
	}

	private void bindingAndCustomization() {

		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Code", 50,JLabel.CENTER, "fouCode", "String"));
			lesColumns.addObject(new ColumnData("Nom", 100,JLabel.CENTER, "adrNom", "String"));
			lesColumns.addObject(new ColumnData("Prenom", 100,JLabel.CENTER, "adrPrenom", "String"));
			lesColumns.addObject(new ColumnData("Ville", 100,JLabel.CENTER, "adrVille", "String"));
			lesColumns.addObject(new ColumnData("CP", 50,JLabel.CENTER, "adrCp", "String"));


			setResultatTBV(new JTableViewCocktail(lesColumns, getResultatDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonSwingFinderEOVfournisseurNib().getJPanelTbv().add(getResultatTBV());
			//setVFournisseurDG();

			// multi ou mono selection dans les tbv
			getResultatTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			// le cablage des  objets
			getMonSwingFinderEOVfournisseurNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonSwingFinderEOVfournisseurNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			getMonSwingFinderEOVfournisseurNib().getJButtonCocktailFiltrer().addDelegateActionListener(this, METHODE_ACTION_FILTRER);
			getMonSwingFinderEOVfournisseurNib().getJTextFieldCocktailCode().addDelegateKeyListener(this, METHODE_ACTION_FILTRER);
            getMonSwingFinderEOVfournisseurNib().getJTextFieldCocktailNom().addDelegateKeyListener(this, METHODE_ACTION_FILTRER);
            // les images des objets
			getMonSwingFinderEOVfournisseurNib().getJButtonCocktailSelectionner().setIcone("valider16");
			getMonSwingFinderEOVfournisseurNib().getJButtonCocktailAnnuler().setIcone("close_view");
			getMonSwingFinderEOVfournisseurNib().getJButtonCocktailFiltrer().setIcone("linkto_help");
                        
                        
                        //pour l'inspecteur des fournisseurs
                        getMonSwingFinderEOVfournisseurNib().getJbtInfo().setIcone("linkto_help");
                        getMonSwingFinderEOVfournisseurNib().getJbtInfo().addDelegateActionListener(this, METHODE_AFFICHE_INSPECTEUR_FOUR);
                        monSwingInspecteurFournisseur = new SwingInspecteurFournisseur(app, this, 0, 0, 553, 351, isModal());
                        
            //les coches ....
            getMonSwingFinderEOVfournisseurNib().getJCheckBoxClient().addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    actionFiltrer();
                    }
                    });
            getMonSwingFinderEOVfournisseurNib().getJCheckBoxFournisseur().addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    actionFiltrer();
                    }
                    });
            getMonSwingFinderEOVfournisseurNib().getJCheckBoxMorale().addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    actionFiltrer();
                    }
                    });
            getMonSwingFinderEOVfournisseurNib().getJCheckBoxPhysique().addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    actionFiltrer();
                    }
                    });

			//sera modal lors d une transaction 
			//sera modal lors d une transaction 
			if (parentControleurEONib == null)
				app.addLesPanelsModal(currentNib);
			else
				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);
			netoyerInterface();


		} catch (Exception e) {
                    e.printStackTrace();
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}

		getResultatTBV().refresh();

	}




	private void setVFournisseurDG() {

		getResultatDG().removeAllObjects();
		if (getResultatDG() != null)
			getResultatTBV().refresh();	

		try {

			getResultatDG().addObjectsFromArray(
					findVFournisseur(app)
			);

			if (getResultatTBV() != null)
				getResultatTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}

	}
	// finder
	public  NSArray findVFournisseur(ApplicationCocktail app) {
		String entityName ="ca_VFournisseur";
		String personne ="";
		String clientfour ="F";
		NSArray leSort = null;
		EOQualifier aQualifier =null;

		// le sort 
		leSort =new NSArray(new Object[]{
				EOSortOrdering.sortOrderingWithKey("adrNom", EOSortOrdering.CompareCaseInsensitiveAscending)
		});


		// le qualifier personne physique ou morale
		if (getMonSwingFinderEOVfournisseurNib().getJCheckBoxMorale().isSelected())
			personne ="AND adrCivilite = 'STR' ";
		else
			personne ="AND adrCivilite != 'STR' ";


		if (getMonSwingFinderEOVfournisseurNib().getJCheckBoxMorale().isSelected() &&
				getMonSwingFinderEOVfournisseurNib().getJCheckBoxPhysique().isSelected())
			personne ="";

		// le qualifier fournisseur client les 2
		if (getMonSwingFinderEOVfournisseurNib().getJCheckBoxFournisseur().isSelected())
			clientfour ="AND fouType = 'F' ";
		else
			clientfour ="AND fouType = 'C' ";


		if (getMonSwingFinderEOVfournisseurNib().getJCheckBoxFournisseur().isSelected() &&
				getMonSwingFinderEOVfournisseurNib().getJCheckBoxClient().isSelected())
			clientfour ="";



		if(getMonSwingFinderEOVfournisseurNib().getJTextFieldCocktailCode().getText().length() <= 1 &&
				getMonSwingFinderEOVfournisseurNib().getJTextFieldCocktailNom().getText().length() <= 2)
			return null;

		aQualifier = EOQualifier.qualifierWithQualifierFormat("fouValide ='O'"+
				"AND fouCode caseinsensitivelike %@ " +
				"AND adrNom  caseinsensitivelike %@ " +
				personne+clientfour,
				new NSArray(new Object[]{
						"*"+getMonSwingFinderEOVfournisseurNib().getJTextFieldCocktailCode().getText()+"*",
						"*"+getMonSwingFinderEOVfournisseurNib().getJTextFieldCocktailNom().getText()+"*"
				}));

		System.out.println(" \n aQualifier \n"+aQualifier);
		return Finder.find(app, entityName, leSort, aQualifier);
	}
	// methodes cablage
//	public void actionAnnuler() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray();
//	netoyerInterface();
//	if(isModal()){
//	app.addLesPanelsModal(getMonSwingFinderEOVfournisseurNib());
//	app.retirerLesGlassPane();
//	}

//	masquerFenetre();
//	parentControleur.swingFinderAnnuler(getSender());
//	}

//	// methodes cablage
//	public void actionSelectionner() {
//	trace(METHODE_ACTION_SELECTIONNER);
//	resltat = new NSMutableArray(getResultatTBV().getSelectedObjects());
//	if(isModal()){
//	app.addLesPanelsModal(getMonSwingFinderEOVfournisseurNib());
//	app.retirerLesGlassPane();
//	}
//	masquerFenetre();
//	parentControleur.swingFinderTerminer(getSender());
//	netoyerInterface();
//	}


	private void netoyerInterface(){
		getMonSwingFinderEOVfournisseurNib().getJTextFieldCocktailCode().setText("");
		getMonSwingFinderEOVfournisseurNib().getJTextFieldCocktailNom().setText("");
		getMonSwingFinderEOVfournisseurNib().getJCheckBoxClient().setSelected(true);
		getMonSwingFinderEOVfournisseurNib().getJCheckBoxFournisseur().setSelected(true);
		getMonSwingFinderEOVfournisseurNib().getJCheckBoxMorale().setSelected(true);
		getMonSwingFinderEOVfournisseurNib().getJCheckBoxPhysique().setSelected(true);
	}

	// methodes cablage
	public void actionFiltrer() {
		//	trace(METHODE_ACTION_FERMER);
		setVFournisseurDG();
	}
        
        public void afficheInspecteurFournisseur(){
            monSwingInspecteurFournisseur.getMonInspecteurFournisseurNibCtrl().setDisplayedFournisseur((VFournisseur)getResultatTBV().getSelectedObjects().lastObject());
            monSwingInspecteurFournisseur.getMonInspecteurFournisseurNibCtrl().afficherFenetre();
            //afficherFenetreFinder(getMonSwingFinderEOVfournisseurNib().getJbtInfo(), (VFournisseur)getResultatTBV().getSelectedObjects().lastObject());
        }

	public NSArray getResultat(){

		netoyerInterface();
		getResultatDG().removeAllObjects();
		getResultatTBV().refresh();	
		return resltat;
	}




}
