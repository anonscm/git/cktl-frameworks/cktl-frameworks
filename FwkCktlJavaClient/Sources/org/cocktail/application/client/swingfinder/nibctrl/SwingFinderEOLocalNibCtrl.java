/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.eof.EOLocal;
import org.cocktail.application.client.eof.EOSalles;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOLocalNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SwingFinderEOLocalNibCtrl extends SwingFinderEOGenericRecord {

	//NSMutableArray resltat = new NSMutableArray();
	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_CHANGEMENT_SELECTION_BATIMENT = "changementBatiment";

	// les tables view
	private JTableViewCocktail lesSallesTBV = null;
	private JTableViewCocktail lesBatimentsTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup lesSallesDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup lesBatimentsDG = new NSMutableArrayDisplayGroup();

	// le nib
	private SwingFinderEOLocalNib monAssistantLocalisationNib = null;


	// parentControleur
	//NibCtrl parentControleur = null;


//	public void setSender(SwingFinder swingfinder) {
//	this.swingfinder= swingfinder;
//	}


//	public SwingFinder getSender() {
//	return swingfinder;
//	}


//	private SwingFinder swingfinder=null;

//	private boolean modal = false;

//	public boolean isModal() {
//	return modal;
//	}

	public SwingFinderEOLocalNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);
	}

	public void creationFenetre(SwingFinderEOLocalNib leAssistantLocalisationNib, String title,NibCtrl parentControleur,boolean modal) {
		super.creationFenetre(leAssistantLocalisationNib, title);
		setMonAssistantLocalisationNib(leAssistantLocalisationNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
	}


	public void creationFenetre(SwingFinderEOLocalNib leAssistantLocalisationNib, String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
		super.creationFenetre(leAssistantLocalisationNib, title);
		setMonAssistantLocalisationNib(leAssistantLocalisationNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleurEONib =  parentControleur;		
		bindingAndCustomization();
	}



//	public void afficherFenetre(){
//	super.afficherFenetre();
//	if(isModal()){
//	app.removeFromLesPanelsModal(getMonAssistantLocalisationNib());
//	app.activerLesGlassPane();
//	}
//	}

	public void setResultat() {
		
		resltat = new NSMutableArray(getLesSallesTBV().getSelectedObjects());
	}


	private void bindingAndCustomization() {

		try {
			// creation de la table view des batiments
			NSMutableArray lesColumnsBat = new NSMutableArray();
			lesColumnsBat.addObject(new ColumnData("Batiment", 100,JLabel.CENTER, EOLocal.APPELLATION_KEY, "String"));


			//setAssistantLocalisationDG();
			setLesBatimentsTBV(new JTableViewCocktail(lesColumnsBat, getLesBatimentsDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonAssistantLocalisationNib().getJPanelBatiment().add(getLesBatimentsTBV());
			setBatimentDG();

			// creation de la table view des salles
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Etage", 20,JLabel.CENTER, EOSalles.SAL_ETAGE_KEY, "String"));
			lesColumns.addObject(new ColumnData("Porte", 100,JLabel.CENTER, EOSalles.SAL_PORTE_KEY, "String"));
			lesColumns.addObject(new ColumnData("Descriptif", 200,JLabel.CENTER, EOSalles.SAL_DESCRIPTIF_KEY, "String"));

			setLesSallesTBV(new JTableViewCocktail(lesColumns, getLesSallesDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonAssistantLocalisationNib().getJPanelResultat().add(getLesSallesTBV());

			// multi ou mono selection dans les tbv
			getLesSallesTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// multi ou mono selection dans les tbv
			getLesBatimentsTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			getLesSallesTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// les listeners
			getLesBatimentsTBV().addDelegateSelectionListener(this,METHODE_CHANGEMENT_SELECTION_BATIMENT);
			getLesBatimentsTBV().addDelegateKeyListener(this,METHODE_CHANGEMENT_SELECTION_BATIMENT);


			// le cablage des  objets
			getMonAssistantLocalisationNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			getMonAssistantLocalisationNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);

			// les images des objets
			getMonAssistantLocalisationNib().getJButtonCocktailSelectionner().setIcone("valider16");
			getMonAssistantLocalisationNib().getJButtonCocktailAnnuler().setIcone("close_view");

			//sera modal lors d une transaction 
			if (parentControleurEONib == null)
				app.addLesPanelsModal(currentNib);
			else
				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);

			// obseration des changements de selection de lexercice :
			app.getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}

		getLesSallesTBV().refresh();

	}

	private void setBatimentDG() {
		getLesBatimentsDG().removeAllObjects();
		if (getLesBatimentsDG() != null)
			getLesBatimentsTBV().refresh();	

		try {

			getLesBatimentsDG().addObjectsFromArray(findLesBatiments(app));

			if (getLesBatimentsTBV() != null)
				getLesBatimentsTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}

	}

	private void setLesSallesDG() {

		getLesSallesDG().removeAllObjects();
		if (getLesSallesDG() != null)
			getLesSallesTBV().refresh();	

		try {

			getLesSallesDG().addObjectsFromArray(findLesSalles(app,(EOLocal)getLesBatimentsTBV().getSelectedObjects().objectAtIndex(0)));

			if (getLesSallesTBV() != null)
				getLesSallesTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}

	}

//	// methodes cablage
//	public void actionAnnuler() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray();
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantLocalisationNib());
//	app.retirerLesGlassPane();
//	}
//	masquerFenetre();
//	parentControleur.swingFinderAnnuler(getSender());
//	}

//	// methodes cablage
//	public void actionSelectionner() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray(getLesSallesTBV().getSelectedObjects());
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantLocalisationNib());
//	app.retirerLesGlassPane();
//	}
//	masquerFenetre();
//	parentControleur.swingFinderTerminer(getSender());
//	}


	public NSArray getResultat(){
		getLesSallesDG().removeAllObjects();
		getLesSallesTBV().refresh();	
		return resltat;
	}

	public void changementBatiment(){
		setLesSallesDG();
	}

	//finder
	public  NSArray findLesBatiments(ApplicationCocktail app) {
		String entityName = EOLocal.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier =null;

		// le sort 
		leSort =new NSArray(new Object[]{
				EOSortOrdering.sortOrderingWithKey(EOLocal.APPELLATION_KEY, EOSortOrdering.CompareCaseInsensitiveDescending)
		});

		return Finder.find(app, entityName, leSort, aQualifier);
	}

	public  NSArray findLesSalles(ApplicationCocktail app,EOLocal local) {
		String entityName = EOSalles.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier =null;

		// le sort 
		leSort =new NSArray(new Object[]{
				EOSortOrdering.sortOrderingWithKey(EOSalles.SAL_ETAGE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending),
				EOSortOrdering.sortOrderingWithKey(EOSalles.SAL_PORTE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)
		});

		// le qualifier salles

		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOSalles.LOCAL_KEY+" = %@",
				new NSArray(new Object[]{local}));

		return Finder.find(app, entityName, leSort, aQualifier);
	}

//	// methode pour le parentControleur
//	private void rechercheAnnuler(){
//	parentControleur.assistantsAnnuler(this);
//	}

//	private void rechercheTerminer(){
//	parentControleur.assistantsTerminer(this);
//	}



	public NSMutableArrayDisplayGroup getLesSallesDG() {
		return lesSallesDG;
	}

	public void setLesSallesDG(NSMutableArrayDisplayGroup lesSallesDG) {
		this.lesSallesDG = lesSallesDG;
	}

	public JTableViewCocktail getLesSallesTBV() {
		return lesSallesTBV;
	}

	public void setLesSallesTBV(JTableViewCocktail lesSallesTBV) {
		this.lesSallesTBV = lesSallesTBV;
	}

	public SwingFinderEOLocalNib getMonAssistantLocalisationNib() {
		return monAssistantLocalisationNib;
	}

	public void setMonAssistantLocalisationNib(
			SwingFinderEOLocalNib monAssistantLocalisationNib) {
		this.monAssistantLocalisationNib = monAssistantLocalisationNib;
	}

	public NSMutableArrayDisplayGroup getLesBatimentsDG() {
		return lesBatimentsDG;
	}

	public void setLesBatimentsDG(NSMutableArrayDisplayGroup lesBatimentsDG) {
		this.lesBatimentsDG = lesBatimentsDG;
	}

	public JTableViewCocktail getLesBatimentsTBV() {
		return lesBatimentsTBV;
	}

	public void setLesBatimentsTBV(JTableViewCocktail lesBatimentsTBV) {
		this.lesBatimentsTBV = lesBatimentsTBV;
	}


	public void swingFinderAnnuler(SwingFinder s) {
		

	}


	public void swingFinderTerminer(SwingFinder s) {
		

	}
}


