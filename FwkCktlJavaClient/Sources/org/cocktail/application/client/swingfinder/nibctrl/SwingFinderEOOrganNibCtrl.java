/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;
import java.util.TimeZone;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOUtilisateurOrgan;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOOrganNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eoapplication.EOXMLUnarchiver;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class SwingFinderEOOrganNibCtrl extends SwingFinderEOGenericRecord {
	//NSMutableArray resltat = new NSMutableArray();
	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_FILTRER = "actionFiltrer";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
    
    private EOExercice exercice;


	// les tables view
	private JTableViewCocktail resultatTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup resultatDG = new NSMutableArrayDisplayGroup();

	// le nib
	private SwingFinderEOOrganNib monSwingFinderEOOrganNib = null;


	// parentControleur
	//NibCtrl parentControleur = null;


//	public void setSender(SwingFinder swingfinder) {
//	this.swingfinder= swingfinder;
//	}


//	public SwingFinder getSender() {
//	return swingfinder;
//	}


//	private SwingFinder swingfinder=null;

//	private boolean modal = false;

//	public boolean isModal() {
//	return modal;
//	}

	public SwingFinderEOOrganNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);
	}



	public void creationFenetre(SwingFinderEOOrganNib leSwingFinderEOOrganNib,String title,NibCtrl parentControleur,boolean modal) {
		super.creationFenetre(leSwingFinderEOOrganNib, title);
		setMonSwingFinderEOOrganNib(leSwingFinderEOOrganNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
	}


	public void creationFenetre(SwingFinderEOOrganNib leSwingFinderEOOrganNib,String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
		super.creationFenetre(leSwingFinderEOOrganNib, title);
		setMonSwingFinderEOOrganNib(leSwingFinderEOOrganNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleurEONib =  parentControleur;
		bindingAndCustomization();
	}

	public void afficherFenetre(EOExercice unExercice) {
	    this.exercice = unExercice;
        super.afficherFenetre();
	}
    
    public void afficherFenetre() {
        afficherFenetre(null);
    }



	public void setResultat() {
		
		resltat = new NSMutableArray(getResultatTBV().getSelectedObjects());
	}

	private void bindingAndCustomization() {

		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Ouverture", 100,JLabel.CENTER,EOOrgan.ORG_DATE_OUVERTURE_KEY, "String"));
			lesColumns.addObject(new ColumnData("Cloture", 100,JLabel.CENTER,EOOrgan.ORG_DATE_CLOTURE_KEY, "String"));
			lesColumns.addObject(new ColumnData("Ufr", 100,JLabel.CENTER,EOOrgan.ORG_UB_KEY, "String"));
			lesColumns.addObject(new ColumnData("Cr", 100,JLabel.CENTER,EOOrgan.ORG_CR_KEY, "String"));
			lesColumns.addObject(new ColumnData("Sous Cr", 100,JLabel.CENTER,EOOrgan.ORG_SOUSCR_KEY, "String"));

			//setEOOrganDG();
			setResultatTBV(new JTableViewCocktail(lesColumns, getResultatDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonSwingFinderEOOrganNib().getJPanelTbv().add(getResultatTBV());

			// multi ou mono selection dans les tbv
			getResultatTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			// le cablage des  objets
			getMonSwingFinderEOOrganNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonSwingFinderEOOrganNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			getMonSwingFinderEOOrganNib().getJButtonCocktailFiltrer().addDelegateActionListener(this, METHODE_ACTION_FILTRER);
			// les images des objets
			getMonSwingFinderEOOrganNib().getJButtonCocktailSelectionner().setIcone("valider16");
			getMonSwingFinderEOOrganNib().getJButtonCocktailAnnuler().setIcone("close_view");
			getMonSwingFinderEOOrganNib().getJButtonCocktailFiltrer().setIcone("linkto_help");

			//sera modal lors d une transaction 
			if (parentControleurEONib == null)
				app.addLesPanelsModal(currentNib);
			else
				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);

			// obseration des changements de selection de lexercice :
			app.getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

			// init pour les prefs
			findOrganInit(app);
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}
		getResultatTBV().refresh();
	}

	public NSArray getResultat(){

		netoyerInterface();
		getResultatDG().removeAllObjects();
		getResultatTBV().refresh();	
		return resltat;
	}

	private void setEOOrganDG() {

		getResultatDG().removeAllObjects();
		if (getResultatDG() != null)
			getResultatTBV().refresh();	

		try {
			getResultatDG().addObjectsFromArray(findOrgan(app));
			if (getResultatTBV() != null)
				getResultatTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	private NSArray findOrganInit(ApplicationCocktail app) {
		String entityName = EOOrgan.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier =null;

		return app.getMesOrgans();
		//return Finder.find(app, entityName, leSort, aQualifier);

	}
    
//    private NSTimestamp dateDebutForExercice(){
//        //le 01/01/de l'anne de l'exercice
//        return new NSTimestamp(exercice.exeOrdre().intValue(),1,1,0,0,0,TimeZone.getTimeZone(app.getApplicationParametre("GRHUM_TIMEZONE")));
//    }
//    
//    private NSTimestamp dateFinForExercice(){
//        //lle 01/01/de l'anne suivant celle de l'exercice
//        return new NSTimestamp(exercice.exeOrdre().intValue()+1,1,1,0,0,0,TimeZone.getTimeZone(app.getApplicationParametre("GRHUM_TIMEZONE")));
//    }


	/**
	 * Filtrage des organs
	 */
	private NSArray findOrgan(ApplicationCocktail app) {
		long start = System.currentTimeMillis();
		NSMutableArray result = new NSMutableArray();

		NSArray lesOrgans = app.getMesOrgans();

		try {
			if (lesOrgans != null && exercice != null) {
				System.out.println("Avant : " + lesOrgans.count());

				String pathKeyOuverture = EOOrgan.ORG_DATE_OUVERTURE_KEY;
				String pathKeyCloture = EOOrgan.ORG_DATE_CLOTURE_KEY;
				EOQualifier qual = app.getToolsCocktailEOF().getQualifierForPeriodeAndExercice(pathKeyOuverture, pathKeyCloture, exercice);
				System.out.println("qual = " + qual);
				lesOrgans = EOQualifier.filteredArrayWithQualifier(lesOrgans, qual);
				System.out.println("Apres : " + lesOrgans.count());
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

		for (int i = 0; i < lesOrgans.count(); i++) {
			int orgNiveau = ((EOOrgan) lesOrgans.objectAtIndex(i)).orgNiveau().intValue();
			boolean isComposante = getMonSwingFinderEOOrganNib().getJCheckBoxComposante().isSelected() && orgNiveau == 2;
			boolean isCr = getMonSwingFinderEOOrganNib().getJCheckBoxCr().isSelected() && orgNiveau == 3;
			boolean isSousCr = getMonSwingFinderEOOrganNib().getJCheckBoxSousCr().isSelected() && orgNiveau == 4;
			if (isComposante || isCr || isSousCr) {
				result.addObject(lesOrgans.objectAtIndex(i));
			}
		}
		System.out.println("Filtrage organs : " + (System.currentTimeMillis() - start) + "ms");

		return result;
	}

	//	private NSArray findOrgan(ApplicationCocktail app) {
	//		long start = System.currentTimeMillis();
	//		NSMutableArray result = new NSMutableArray();
	//		NSArray lesUtlOrgan = app.getMesUtilisateurOrgan();
	//		try {
	//			if (lesUtlOrgan != null && exercice != null) {
	//				System.out.println("Avant : " + lesUtlOrgan.count());
	//				
	//				String pathKeyOuverture = EOUtilisateurOrgan.ORGAN_KEY + "." + EOOrgan.ORG_DATE_OUVERTURE_KEY;
	//				String pathKeyCloture = EOUtilisateurOrgan.ORGAN_KEY + "." + EOOrgan.ORG_DATE_CLOTURE_KEY;
	//				EOQualifier qual = app.getToolsCocktailEOF().getQualifierForPeriodeAndExercice(pathKeyOuverture, pathKeyCloture, exercice);
	//				System.out.println("qual = " + qual);
	//				lesUtlOrgan = EOQualifier.filteredArrayWithQualifier(lesUtlOrgan, qual);
	//				System.out.println("Apres : " + lesUtlOrgan.count());
	//			}
	//		} catch (Throwable e) {
	//			e.printStackTrace();
	//		}
	//		
	//		for (int i = 0; i < lesUtlOrgan.count(); i++) {
	//			int orgNiveau = ((EOUtilisateurOrgan) lesUtlOrgan.objectAtIndex(i)).organ().orgNiveau().intValue();
	//			boolean isComposante = getMonSwingFinderEOOrganNib().getJCheckBoxComposante().isSelected() && orgNiveau == 2;
	//			boolean isCr = getMonSwingFinderEOOrganNib().getJCheckBoxCr().isSelected() && orgNiveau == 3;
	//			boolean isSousCr = getMonSwingFinderEOOrganNib().getJCheckBoxSousCr().isSelected() && orgNiveau == 4;
	//			if (isComposante || isCr || isSousCr) {
	//				result.addObject(((EOUtilisateurOrgan) lesUtlOrgan.objectAtIndex(i)).organ());
	//			}
	//		}
	//		System.out.println("Filtrage organs : " + (System.currentTimeMillis() - start) + "ms");
	//		
	//		return result;
	//	}


	// methodes cablage
	public void actionFermer() {
		//	trace(METHODE_ACTION_FERMER);
		//masquerFenetre();
        actionAnnuler();
	}
	// methodes cablage
//	public void actionAnnuler() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray();
//	netoyerInterface();
//	if(isModal()){
//	app.addLesPanelsModal(getMonSwingFinderEOOrganNib());
//	app.retirerLesGlassPane();
//	}

//	masquerFenetre();
//	parentControleur.swingFinderAnnuler(getSender());
//	}

//	// methodes cablage
//	public void actionSelectionner() {
//	trace(METHODE_ACTION_SELECTIONNER);
//	resltat = new NSMutableArray(getResultatTBV().getSelectedObjects());
//	if(isModal()){
//	app.addLesPanelsModal(getMonSwingFinderEOOrganNib());
//	app.retirerLesGlassPane();
//	}
//	masquerFenetre();
//	parentControleur.swingFinderTerminer(getSender());
//	netoyerInterface();
//	}

	public void changementExercice() {
		setEOOrganDG();

	}

	// methodes cablage
	public void actionFiltrer() {
		//	trace(METHODE_ACTION_FERMER);
		setEOOrganDG();
	}

	private void netoyerInterface(){
		getMonSwingFinderEOOrganNib().getJCheckBoxComposante().setSelected(false);
		getMonSwingFinderEOOrganNib().getJCheckBoxCr().setSelected(false);
		getMonSwingFinderEOOrganNib().getJCheckBoxSousCr().setSelected(false);
	}

	public SwingFinderEOOrganNib getMonSwingFinderEOOrganNib() {
		return monSwingFinderEOOrganNib;
	}

	public void setMonSwingFinderEOOrganNib(
			SwingFinderEOOrganNib monSwingFinderEOOrganNib) {
		this.monSwingFinderEOOrganNib = monSwingFinderEOOrganNib;
	}

	public NSMutableArrayDisplayGroup getResultatDG() {
		return resultatDG;
	}

	public void setResultatDG(NSMutableArrayDisplayGroup resultatDG) {
		this.resultatDG = resultatDG;
	}

	public JTableViewCocktail getResultatTBV() {
		return resultatTBV;
	}

	public void setResultatTBV(JTableViewCocktail resultatTBV) {
		this.resultatTBV = resultatTBV;
	}


	public NibCtrl getParentControleur() {
		return parentControleur;
	}


	public void setParentControleur(NibCtrl parentControleur) {
		this.parentControleur = parentControleur;
	}


//	public SwingFinder getSwingfinder() {
//	return swingfinder;
//	}


//	public void setSwingfinder(SwingFinder swingfinder) {
//	this.swingfinder = swingfinder;
//	}


//	public void setModal(boolean modal) {
//	this.modal = modal;
//	}


//	public void swingFinderAnnuler(SwingFinder s) {
//	

//	}


//	public void swingFinderTerminer(SwingFinder s) {
//	

//	}
}
