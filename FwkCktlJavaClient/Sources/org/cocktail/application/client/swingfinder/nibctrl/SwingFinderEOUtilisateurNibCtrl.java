/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOUtilisateurNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SwingFinderEOUtilisateurNibCtrl extends SwingFinderEOGenericRecord {

	//NSMutableArray resltat = new NSMutableArray();
	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_FILTRER = "actionFiltrer";

	// les tables view
	private JTableViewCocktail utilisateurTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup utilisateurDG = new NSMutableArrayDisplayGroup();

	// le nib
	private SwingFinderEOUtilisateurNib monAssistantRechercheUtilisateurNib = null;

	// parentControleur
	//NibCtrl parentControleur = null;


//	public void setSender(SwingFinder swingfinder) {
//	this.swingfinder= swingfinder;
//	}


//	public SwingFinder getSender() {
//	return swingfinder;
//	}


//	private SwingFinder swingfinder=null;

//	private boolean modal = false;

//	public boolean isModal() {
//	return modal;
//	}

	public SwingFinderEOUtilisateurNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);
	}

	public void creationFenetre(SwingFinderEOUtilisateurNib leAssistantRechercheIndividuNib, String title,NibCtrl parentControleur,boolean modal) {
		super.creationFenetre(leAssistantRechercheIndividuNib, title);
		setMonAssistantRechercheUtilisateurNib(leAssistantRechercheIndividuNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
	}

	public void creationFenetre(SwingFinderEOUtilisateurNib leAssistantRechercheIndividuNib, String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
		super.creationFenetre(leAssistantRechercheIndividuNib, title);
		setMonAssistantRechercheUtilisateurNib(leAssistantRechercheIndividuNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleurEONib =  parentControleur;
		bindingAndCustomization();
	}


//	public void afficherFenetre(){
//	super.afficherFenetre();
//	if(isModal()){
//	app.removeFromLesPanelsModal(getMonAssistantRechercheUtilisateurNib());
//	app.activerLesGlassPane();
//	}
//	}

	public void setResultat() {
		
		resltat = new NSMutableArray(utilisateurTBV.getSelectedObjects());
	}

	private void bindingAndCustomization() {

		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Nom", 100,JLabel.CENTER, "individu.nomUsuel", "String"));
			lesColumns.addObject(new ColumnData("Prenom", 100,JLabel.CENTER, "individu.prenom", "String"));

			setUtilisateurTBV(new JTableViewCocktail(lesColumns, getUtilisateurDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonAssistantRechercheUtilisateurNib().getJPanelResultat().add(getUtilisateurTBV());

			// multi ou mono selection dans les tbv
			getUtilisateurTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// le cablage des  objets
			getMonAssistantRechercheUtilisateurNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			getMonAssistantRechercheUtilisateurNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonAssistantRechercheUtilisateurNib().getJButtonCocktailFiltrer().addDelegateActionListener(this, METHODE_ACTION_FILTRER);
            getMonAssistantRechercheUtilisateurNib().getJTextFieldCocktailCritere1().addDelegateKeyListener(this, METHODE_ACTION_FILTRER);
            getMonAssistantRechercheUtilisateurNib().getJTextFieldCocktailCritere2().addDelegateKeyListener(this, METHODE_ACTION_FILTRER);
            // les images des objets
			getMonAssistantRechercheUtilisateurNib().getJButtonCocktailSelectionner().setIcone("valider16");
			getMonAssistantRechercheUtilisateurNib().getJButtonCocktailAnnuler().setIcone("close_view");
			getMonAssistantRechercheUtilisateurNib().getJButtonCocktailFiltrer().setIcone("linkto_help");

			//sera modal lors d une transaction 
			if (parentControleurEONib == null)
				app.addLesPanelsModal(currentNib);
			else
				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}

		getUtilisateurTBV().refresh();

	}

	private void setAssistantRechercheUtilisateurDG() {

		getUtilisateurDG().removeAllObjects();
		if (getUtilisateurTBV() != null)
			getUtilisateurTBV().refresh();	

		try {

			getUtilisateurDG().addObjectsFromArray(findLesUtilisateurs(app));
			if (getUtilisateurTBV() != null)
				getUtilisateurTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}

	}

	// methodes cablage
//	public void actionAnnuler() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray();
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantRechercheUtilisateurNib());
//	app.retirerLesGlassPane();
//	}

//	masquerFenetre();
//	parentControleur.swingFinderAnnuler(getSender());
//	}

//	// methodes cablage
//	public void actionSelectionner() {
//	//	trace(METHODE_ACTION_FERMER);
//	resltat = new NSMutableArray(utilisateurTBV.getSelectedObjects());
//	if(isModal()){
//	app.addLesPanelsModal(getMonAssistantRechercheUtilisateurNib());
//	app.retirerLesGlassPane();
//	}
//	masquerFenetre();
//	parentControleur.swingFinderTerminer(getSender());
//	}

	// methodes cablage
	public void actionFiltrer() {
		//	trace(METHODE_ACTION_FERMER);
		setAssistantRechercheUtilisateurDG();
	}

	public NSArray getResultat(){

		getMonAssistantRechercheUtilisateurNib().getJTextFieldCocktailCritere1().setText("");
		getMonAssistantRechercheUtilisateurNib().getJTextFieldCocktailCritere2().setText("");
		getUtilisateurDG().removeAllObjects();
		getUtilisateurTBV().refresh();	
		return resltat;
	}

	//finder
	public  NSArray findLesUtilisateurs(ApplicationCocktail app) {
		String entityName ="ca_Utilisateur";
		NSArray leSort = null;

		EOQualifier nomQualifier = null;
		EOQualifier prenomQualifier = null;

		NSMutableArray lesQualifiers = new NSMutableArray();

		// le sort 
//		leSort =new NSArray(new Object[]{
//		EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareCaseInsensitiveDescending),
//		EOSortOrdering.sortOrderingWithKey("individu.prenom", EOSortOrdering.CompareCaseInsensitiveDescending)
//		});

		// le qualifier NOM
		if(getMonAssistantRechercheUtilisateurNib().getJTextFieldCocktailCritere1().getText().length() >= 2){
			nomQualifier = EOQualifier.qualifierWithQualifierFormat("individu.nomUsuel caseinsensitivelike %@",
					new NSArray(new Object[]{getMonAssistantRechercheUtilisateurNib().getJTextFieldCocktailCritere1().getText()+"*"}));
			lesQualifiers.addObject(nomQualifier);
		}

		// le qualifier PRENOM
		if(getMonAssistantRechercheUtilisateurNib().getJTextFieldCocktailCritere2().getText().length() >= 2){
			prenomQualifier = EOQualifier.qualifierWithQualifierFormat("individu.prenom caseinsensitivelike %@",
					new NSArray(new Object[]{getMonAssistantRechercheUtilisateurNib().getJTextFieldCocktailCritere2().getText()+"*"}));
			lesQualifiers.addObject(prenomQualifier);
		}

		System.out.println("lesQualifiers "+lesQualifiers);

		if (lesQualifiers.count() == 0)
			return null;

		EOAndQualifier monAND = new EOAndQualifier(lesQualifiers);

		return Finder.find(app, entityName, leSort, monAND);
	}

	// methode pour le parentControleur

	// accesseurs
	public NSMutableArrayDisplayGroup getUtilisateurDG() {
		return utilisateurDG;
	}

	public void setUtilisateurDG(NSMutableArrayDisplayGroup utilisateurDG) {
		this.utilisateurDG = utilisateurDG;
	}

	public JTableViewCocktail getUtilisateurTBV() {
		return utilisateurTBV;
	}

	public void setUtilisateurTBV(JTableViewCocktail utilisateurTBV) {
		this.utilisateurTBV = utilisateurTBV;
	}

	public SwingFinderEOUtilisateurNib getMonAssistantRechercheUtilisateurNib() {
		return monAssistantRechercheUtilisateurNib;
	}

	public void setMonAssistantRechercheUtilisateurNib(
			SwingFinderEOUtilisateurNib monAssistantRechercheIndividuNib) {
		this.monAssistantRechercheUtilisateurNib = monAssistantRechercheIndividuNib;
	}


//	public void swingFinderAnnuler(SwingFinder s) {
//	

//	}


//	public void swingFinderTerminer(SwingFinder s) {
//	

//	}

}
