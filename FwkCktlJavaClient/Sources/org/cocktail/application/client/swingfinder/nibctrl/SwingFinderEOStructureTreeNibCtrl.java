/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 22 oct. 07
 * author mparadot 
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOStructureUlr;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOStructureNib;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOStructureTreeNib;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOUtilisateurNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.JTreeCocktail;
import org.cocktail.application.palette.interfaces.JTreeObjectCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SwingFinderEOStructureTreeNibCtrl extends SwingFinderEOGenericRecord{

    //NSMutableArray resltat = new NSMutableArray();
    // methodes
    private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
    private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
//    private static final String METHODE_ACTION_FILTRER = "actionFiltrer";
    
    private final static String TypeDeStructureEtablissement = "E,ES";

    // les tables view
    private JTableViewCocktail structureTBV = null;

    // les displaygroup
    private NSMutableArrayDisplayGroup structureDG = new NSMutableArrayDisplayGroup();

    // le nib
    private SwingFinderEOStructureTreeNib monSwingFinderEOStructureTreeNib = null;
    
    //pour le treeView
    private JTreeObjectCocktail delegate;
    private JTreeCocktail tree;

    public SwingFinderEOStructureTreeNibCtrl(ApplicationCocktail ctrl, int alocation_x,
            int alocation_y, int ataille_x, int ataille_y) {
        super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
        delegate = new ObjectStructure();
        setWithLogs(false);
    }

    public void creationFenetre(SwingFinderEOStructureTreeNib leSwingFinderEOStructureNib, String title,NibCtrl parentControleur,boolean modal) {
        super.creationFenetre(leSwingFinderEOStructureNib, title);
        setMonEOStructureTreeNib(leSwingFinderEOStructureNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleur =  parentControleur;
    }

    public void creationFenetre(SwingFinderEOStructureTreeNib leSwingFinderEOStructureNib, String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
        super.creationFenetre(leSwingFinderEOStructureNib, title);
        setMonEOStructureTreeNib(leSwingFinderEOStructureNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleurEONib =  parentControleur;
    }


	public void afficherFenetre(){
		bindingAndCustomization();
		super.afficherFenetre();
	}

    private void bindingAndCustomization() {
        try {
//            System.out.println("SwingFinderEOStructureNibCtrl.bindingAndCustomization()");
            // le cablage des  objets
            getMonSwingFinderEOStructureTreeNib().getJButtonCocktailValider().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
            getMonSwingFinderEOStructureTreeNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
            getMonSwingFinderEOStructureTreeNib().getJButtonCocktailValider().setIcone(IconeCocktail.VALIDER);
            getMonSwingFinderEOStructureTreeNib().getJButtonCocktailAnnuler().setIcone(IconeCocktail.FERMER);
            
            //creation du treeView
            tree = new JTreeCocktail(delegate);
            JScrollPane pane = new JScrollPane(tree);//, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

            getMonSwingFinderEOStructureTreeNib().getJPanel1().removeAll();
            getMonSwingFinderEOStructureTreeNib().getJPanel1().setLayout(new BorderLayout());
            getMonSwingFinderEOStructureTreeNib().getJPanel1().add(pane, BorderLayout.CENTER);
            getMonSwingFinderEOStructureTreeNib().getJPanel1().validate();
            getMonSwingFinderEOStructureTreeNib().getJPanel1().setVisible(true);

            //sera modal lors d une transaction 
//            if (parentControleurEONib == null)
//                app.addLesPanelsModal(currentNib);
//            else
//                app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);
        } catch (Exception e) {
            e.printStackTrace();
            this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
                    e.getMessage(), true);
        }
    }
    
    public void actionSelectionner() {
    	getResultat();
    	super.actionSelectionner();
	}

    public NSArray getResultat(){
    	if(tree.getSelectedObject()==null)
    		return new NSArray();
    	return new NSArray(tree.getSelectedObject());
    }

    public NSMutableArrayDisplayGroup getStructureDG() {
        return structureDG;
    }

    public void setStructureDG(NSMutableArrayDisplayGroup leDisplayGroup) {
        structureDG = leDisplayGroup;
    }

    public JTableViewCocktail getStructureTBV() {
        return structureTBV;
    }

    public void setStructureTBV(JTableViewCocktail structureTBV) {
        this.structureTBV = structureTBV;
    }

    public SwingFinderEOStructureTreeNib getMonSwingFinderEOStructureTreeNib() {
        return monSwingFinderEOStructureTreeNib;
    }

    public void setMonEOStructureTreeNib(
            SwingFinderEOStructureTreeNib monSwingFinderEOStructureNib) {
        if(monSwingFinderEOStructureNib==null)
            System.out.println("insert null !!!!");
        this.monSwingFinderEOStructureTreeNib = monSwingFinderEOStructureNib;
    }

    public void swingFinderAnnuler(SwingFinder s) {
        

    }

    public void swingFinderTerminer(SwingFinder s) {
        
    }
    
    /**
    * Specification des structures de type etablissement (utilisation de cTypeStructure).
    * @param avecNonValides Mettre <code>true</code> pour ne pas ecarter les structures non valides (temValide)
    * @return Le qualifer correspondant.
    */
    public static final EOQualifier QualifierTypeEtablissement(final boolean avecNonValides) {
    StringBuffer buf;
    if (avecNonValides) buf = new StringBuffer("cTypeStructure='"+TypeDeStructureEtablissement+"'");
    else buf = new StringBuffer("temValide = 'O' and cTypeStructure='");
    buf.append(TypeDeStructureEtablissement.replaceAll(",", "' or cTypeStructure='"));// c_type_structure='E' or c_type_structure='ES'
    buf.append("')");
    return EOQualifier.qualifierWithQualifierFormat(buf.toString(), null);
    }
    
    class ObjectStructure implements JTreeObjectCocktail{

        public NSArray getChilds(Object ob) {
            if(ob==null)
                return new NSArray();
            return (NSArray)((EOGenericRecord)ob).valueForKey("ca_StructureUlrs");
        }

        public NSArray getRootsNode() {
            NSMutableArray args = new NSMutableArray();
            args.addObject(QualifierTypeEtablissement(false));
            NSMutableArray sort = new NSMutableArray();
            sort.addObject(EOSortOrdering.sortOrderingWithKey("llStructure", EOSortOrdering.CompareAscending));
            return app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort("ca_StructureUlr", new EOAndQualifier(args), sort);
        }

        public boolean isSelectableObject(Object ob) {
            
            return true;
        }

        public String toString(Object ob) {
            if(ob==null)
                return null;
            if("O".equals(((EOStructureUlr)ob).temValide()))
                return ((EOStructureUlr)ob).llStructure();
            return "<html><body><s>"+((EOStructureUlr)ob).llStructure()+"</s></body></html>";
        }
        
    }
}
