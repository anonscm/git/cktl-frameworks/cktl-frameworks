/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nibctrl;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.ModalInterfaceCocktail;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.nibctrl.NibCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SwingFinderEOGenericRecord extends NibCtrl {

	private boolean modal = false;
	NSMutableArray resltat = new NSMutableArray();

	// methodes
	protected static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	protected static final String METHODE_ACTION_ANNULER = "actionAnnuler";

	private SwingFinder swingfinder=null;


	public void setSender(SwingFinder swingfinder) {
		this.swingfinder= swingfinder;
	}

	public SwingFinder getSender() {
		return swingfinder;
	}

	public SwingFinderEOGenericRecord(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		
	}

	public void afficherFenetre(){
            super.afficherFenetre();
//		System.out.println("SwingFinderEOGenericRecord.afficherFenetre("+currentNib.getClass().getName()+")");
            if(isModal()){
                    try {
                            app.activerLesGlassPane(this.currentNib);
//				System.out.println("Activer les glass pane depuis : "+currentNib.getClass().getName());
                    } catch (Throwable e) {
                            e.printStackTrace();
                    }
            }
	}

	// les actions
	public void actionAnnuler(){
//		System.out.println("SwingFinderEOGenericRecord.actionAnnuler("+currentNib.getClass().getName()+")");
		trace(METHODE_ACTION_ANNULER);
		if(isModal()){
			app.retirerLesGlassPane();
			if ( parentControleurEONib != null)
				app.activerLesGlassPane(parentControleurEONib);
    		else
    		{
    			try {
    				if(parentControleur!=null && parentControleur.isModal())
    					app.activerLesGlassPane(parentControleur.currentNib);
				} catch (Throwable e) {
					e.printStackTrace();
				}
    		}
		}
		masquerFenetre();
		if ( parentControleurEONib == null )
			parentControleur.swingFinderAnnuler(getSender());
		else
			parentControleurEONib.swingFinderAnnuler(getSender());
	}

	public void actionSelectionner(){
//		System.out.println("SwingFinderEOGenericRecord.actionSelectionner("+currentNib.getClass().getName()+")");
		trace(METHODE_ACTION_SELECTIONNER);
		setResultat(); 
		if(isModal()){
			app.retirerLesGlassPane();
    		if ( parentControleurEONib != null )
    			app.activerLesGlassPane(parentControleurEONib);
    		else
    		{
    			try {
    				if(parentControleur!=null && parentControleur.isModal())
    					app.activerLesGlassPane(parentControleur.currentNib);
				} catch (Throwable e) {
					e.printStackTrace();
				}
    		}
    				
		}
		masquerFenetre();
		if ( parentControleurEONib == null )
			parentControleur.swingFinderTerminer(getSender());
		else
			parentControleurEONib.swingFinderTerminer(getSender());
	}

	public void setResultat() {
	}

	public NSArray getResultat(){
		return resltat;
	}
}