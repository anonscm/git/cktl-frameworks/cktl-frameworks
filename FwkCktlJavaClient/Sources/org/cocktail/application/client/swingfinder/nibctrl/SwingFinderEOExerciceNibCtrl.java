/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.finder.FinderGrhum;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOExerciceNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.foundation.NSMutableArray;

public class SwingFinderEOExerciceNibCtrl extends SwingFinderEOGenericRecord{

	// les tables view
	private JTableViewCocktail mesExercicesTBV=null;

	// les displaygroup
	private NSMutableArrayDisplayGroup mesExercicesDG = new NSMutableArrayDisplayGroup();

	// le nib
	private SwingFinderEOExerciceNib monExerciceNib =null;


	public SwingFinderEOExerciceNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(true);
	}


	public void creationFenetre(SwingFinderEOExerciceNib leExerciceNib,String title,NibCtrl parentControleur,boolean modal)
	{
		super.creationFenetre( leExerciceNib,title);
		setMonExerciceNib(leExerciceNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleur =  parentControleur;
		bindingAndCustomization();
	}

	public void creationFenetre(SwingFinderEOExerciceNib leExerciceNib,String title,EOInterfaceControllerCocktail parentControleurEONib,boolean modal)
	{
		super.creationFenetre( leExerciceNib,title);
		setMonExerciceNib(leExerciceNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		setModal(modal);
		this.parentControleurEONib =  parentControleurEONib;
		bindingAndCustomization();
	}



	public void setResultat() {
		
		resltat = new NSMutableArray((EOExercice)getMesExercicesTBV().getSelectedObjects().objectAtIndex(0));
	}


	private void bindingAndCustomization() {
		System.out.println("SwingFinderEOExerciceNibCtrl.bindingAndCustomization()");
		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Exercice",100, JLabel.CENTER ,EOExercice.EXE_EXERCICE_KEY,"String"));
			setExerciceDG();
			setMesExercicesTBV(new JTableViewCocktail(lesColumns,getMesExercicesDG(),new Dimension(100,100),JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonExerciceNib().getJScrollPaneExercice().add(getMesExercicesTBV());


			// multi ou mono selection dans les tbv
			getMesExercicesTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			// le cablage des  objets
			getMonExerciceNib().getJButtonCocktailAnnuler().addDelegateActionListener(this,METHODE_ACTION_ANNULER);
			getMonExerciceNib().getJButtonCocktailExerciceOk().addDelegateActionListener(this,METHODE_ACTION_SELECTIONNER);

			// les images des objets
			getMonExerciceNib().getJButtonCocktailExerciceOk().setIcone("valider16");
			getMonExerciceNib().getJButtonCocktailAnnuler().setIcone("delete_obj");

			//sera modal lors d une transaction 
			if (parentControleurEONib == null)
				app.addLesPanelsModal(currentNib);
			else
				app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);
		}
		catch(Throwable e) {
			e.printStackTrace();
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);

		}

		getMesExercicesTBV().refresh();

	}

	// charger les Exercices 
	void setExerciceDG(){
		setMesExercicesDG(FinderGrhum.findLesExercices(app));	
	}


	public JTableViewCocktail getMesExercicesTBV() {
		return mesExercicesTBV;
	}


	void setMesExercicesTBV(JTableViewCocktail mesExercicesTBV) {
		this.mesExercicesTBV = mesExercicesTBV;
	}


	NSMutableArrayDisplayGroup getMesExercicesDG() {
		return mesExercicesDG;
	}


	void setMesExercicesDG(NSMutableArrayDisplayGroup mesExercicesDG) {
		this.mesExercicesDG = mesExercicesDG;
	}


	SwingFinderEOExerciceNib getMonExerciceNib() {
		return monExerciceNib;
	}


	void setMonExerciceNib(SwingFinderEOExerciceNib monExerciceNib) {
		this.monExerciceNib = monExerciceNib;
	}





}
