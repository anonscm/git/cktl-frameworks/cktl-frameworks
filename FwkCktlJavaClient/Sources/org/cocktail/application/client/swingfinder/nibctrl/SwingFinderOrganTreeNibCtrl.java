/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.application.client.swingfinder.nibctrl;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.swingfinder.nib.SwingFinderOrganTreeNib;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTreeCocktail;
import org.cocktail.application.palette.interfaces.JTreeObjectCocktail;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 *
 * @author mparadot
 */
public class SwingFinderOrganTreeNibCtrl extends SwingFinderEOGenericRecord{
    // methodes
    private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
    private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
    
    //pour le treeView
    private JTreeObjectCocktail delegate;
    private JTreeCocktail tree;
    
    // le nib
    private SwingFinderOrganTreeNib monSwingFinderOrganTreeNib = null;
    
    //le resultat
    NSMutableArray resultat = null;
    
    //L'exercice utiliser
    EOExercice exercice;
    
    
    public SwingFinderOrganTreeNibCtrl(ApplicationCocktail ctrl, int alocation_x,
            int alocation_y, int ataille_x, int ataille_y) {
        super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
        setWithLogs(false);
        resultat = new NSMutableArray();
        delegate = new ObjectOrgan();
    }
    
    
    
    public void creationFenetre(SwingFinderOrganTreeNib leSwingFinderOrganTreeNib,String title,NibCtrl parentControleur,boolean modal) {
        super.creationFenetre(leSwingFinderOrganTreeNib, title);
        setMonSwingFinderOrganTreeNib(leSwingFinderOrganTreeNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleur =  parentControleur;
        bindingAndCustomization();
    }
    
    
    public void creationFenetre(SwingFinderOrganTreeNib leSwingFinderOrganTreeNib,String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
        super.creationFenetre(leSwingFinderOrganTreeNib, title);
        setMonSwingFinderOrganTreeNib(leSwingFinderOrganTreeNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleurEONib =  parentControleur;
        bindingAndCustomization();
    }
    
    public void setMonSwingFinderOrganTreeNib(SwingFinderOrganTreeNib leSwingFinderOrganTreeNib) {
        monSwingFinderOrganTreeNib = leSwingFinderOrganTreeNib;
    }
    
    public SwingFinderOrganTreeNib getMonSwingFinderOrganTreeNib() {
        return monSwingFinderOrganTreeNib;
    }
    
    public void afficherFenetre(EOExercice unExercice) {
        this.exercice = unExercice;
        super.afficherFenetre();
    }
    
    public void afficherFenetre() {
        afficherFenetre(null);
    }
    public NSArray lesOrgansFils(EOOrgan organ){
        NSArray list = organ.organFils();
        if(exercice==null)
            return list;
        EOQualifier qual = app.getToolsCocktailEOF().getQualifierForPeriodeAndExercice("orgDateOuverture", "orgDateCloture", exercice);
        return EOQualifier.filteredArrayWithQualifier(list,qual);
    }
    
    
    
    public void setResultat() {
        resultat = new NSMutableArray();
        if(tree.getSelectedObject()!=null)
            resultat.addObject(tree.getSelectedObject());
    }
    
    private void bindingAndCustomization() {
        
        try {
            
            // le cablage des  objets
            getMonSwingFinderOrganTreeNib().btAnnuler.addDelegateActionListener(this, METHODE_ACTION_ANNULER);
            getMonSwingFinderOrganTreeNib().btValider.addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
            
            // les images des objets
            getMonSwingFinderOrganTreeNib().btValider.setIcone(IconeCocktail.VALIDER);
            getMonSwingFinderOrganTreeNib().btAnnuler.setIcone(IconeCocktail.FERMER);
            
            //creation du treeView
            tree = new JTreeCocktail(delegate);
            JScrollPane pane = new JScrollPane(tree);//, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

            getMonSwingFinderOrganTreeNib().treeView.removeAll();
            getMonSwingFinderOrganTreeNib().treeView.setLayout(new BorderLayout());
            getMonSwingFinderOrganTreeNib().treeView.add(pane, BorderLayout.CENTER);
            getMonSwingFinderOrganTreeNib().treeView.validate();
            getMonSwingFinderOrganTreeNib().treeView.setVisible(true);
            
            //sera modal lors d une transaction
            if (parentControleurEONib == null)
                app.addLesPanelsModal(currentNib);
            else
                app.addLesPanelsModal((EOInterfaceControllerCocktail)parentControleurEONib);
            
            // obseration des changements de selection de lexercice :
//            app.getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");
            
        } catch (Exception e) {
            this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
                    e.getMessage(), true);
        }

    }
    
    public NSArray getResultat(){
        return resultat;
    }
    
    
    class ObjectOrgan implements JTreeObjectCocktail{
        
        public NSArray getChilds(Object ob) {
            return lesOrgansFils((EOOrgan)ob);
        }

        public NSArray getRootsNode() {
            NSMutableArray args = new NSMutableArray();
            args.addObject(EOQualifier.qualifierWithQualifierFormat("orgNiveau = 0", null));
            NSMutableArray sort = new NSMutableArray();
            sort.addObject(EOSortOrdering.sortOrderingWithKey("orgLibelle", EOSortOrdering.CompareAscending));
            NSArray list = app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort("ca_Organ",new EOAndQualifier(args), sort);
            list = EOQualifier.filteredArrayWithQualifier(list, new EOAndQualifier(args));
            return EOSortOrdering.sortedArrayUsingKeyOrderArray(list, sort);
        }

        public String toString(Object ob) {
            //Le libelle change en fonction des niveaux
            switch (((EOOrgan)ob).orgNiveau().intValue()) {
            case 1:
                return ((EOOrgan)ob).orgEtab();
            case 2 :
                return ((EOOrgan)ob).orgUb();
            case 3 :
                return ((EOOrgan)ob).orgCr();
            case 4 :
                return ((EOOrgan)ob).orgSouscr();
            default:
                break;
            }
            return ((EOOrgan)ob).orgUniv();
        }
        
        public boolean isSelectableObject(Object ob) {
			//            NSArray list = (NSArray)app.getMesUtilisateurOrgan().valueForKey("organ");
			NSArray list = (NSArray) app.getMesOrgans();
            return !((list!=null) && (list.indexOfIdenticalObject(ob)==NSArray.NotFound));
        }
        
    }
}
