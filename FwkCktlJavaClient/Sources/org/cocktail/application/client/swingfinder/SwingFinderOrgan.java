/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOOrganNib;
import org.cocktail.application.client.swingfinder.nib.SwingFinderOrganTreeNib;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOOrganNibCtrl;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderOrganTreeNibCtrl;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.application.palette.ToolsSwing;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.foundation.NSArray;

public class SwingFinderOrgan extends SwingFinder{

	private static final String FENETRE_DE_SELECTION_Organ="Fenetre de selection de ligne budgetaire";

	private SwingFinderEOOrganNib monSwingFinderEOOrganNib = null;
    private SwingFinderOrganTreeNib monSwingFinderOrganTreeNib = null;
	private SwingFinderEOOrganNibCtrl monSwingFinderEOOrganNibCtrl = null;
    private SwingFinderOrganTreeNibCtrl monSwingFinderOrganTreeNibCtrl = null;
    private boolean useTreeView;
    
    private ApplicationCocktail app = (ApplicationCocktail)EOApplication.sharedApplication();

	private static final String METHODE_AFFICHER_FENETRE = "afficherFenetre";

    public SwingFinderOrgan(ApplicationCocktail ctrl,NibCtrl responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal) {
        this(ctrl, responder, alocation_x, alocation_y, ataille_x, ataille_y, modal, false);
    }
    
    public SwingFinderOrgan(ApplicationCocktail ctrl,NibCtrl responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useTreeView) {
    	this(ctrl, responder, null, null, alocation_x, alocation_y, ataille_x, ataille_y, modal, useTreeView);
    }
    
	public SwingFinderOrgan(ApplicationCocktail ctrl,NibCtrl responder,JDialog parentD,JFrame parentF, int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useTreeView) {
		super();
		this.useTreeView = useTreeView;
		// creation du nib Organ
        if(useTreeView)
        {
            setMonSwingFinderOrganTreeNibCtrl(new SwingFinderOrganTreeNibCtrl(ctrl,alocation_x , alocation_y, ataille_x , ataille_y));
            setMonSwingFinderOrganTreeNib( new SwingFinderOrganTreeNib());   
//            getMonSwingFinderOrganTreeNibCtrl().parentDialog = parentD;
//            getMonSwingFinderOrganTreeNibCtrl().parentFrame = parentF;
            getMonSwingFinderOrganTreeNibCtrl().creationFenetre(
                    getMonSwingFinderOrganTreeNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_Organ),
                    responder,
                    modal
            );
            getMonSwingFinderOrganTreeNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
            );
            getMonSwingFinderOrganTreeNib().setSize(ataille_x,ataille_y);     
            getMonSwingFinderOrganTreeNibCtrl().setSender(this);
        }
        else
        {
    		setMonSwingFinderEOOrganNibCtrl(new SwingFinderEOOrganNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
    		setMonSwingFinderEOOrganNib( new SwingFinderEOOrganNib());
//            getMonSwingFinderEOOrganNibCtrl().parentDialog = parentD;
//            getMonSwingFinderEOOrganNibCtrl().parentFrame = parentF;
    		getMonSwingFinderEOOrganNibCtrl().creationFenetre(
    				getMonSwingFinderEOOrganNib(),
    				ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_Organ),
    				responder,
    				modal
    		);
    		getMonSwingFinderEOOrganNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
    		);
    		getMonSwingFinderEOOrganNib().setSize(ataille_x,ataille_y);		
    		getMonSwingFinderEOOrganNibCtrl().setSender(this);
        }
	}

    public SwingFinderOrgan(ApplicationCocktail ctrl,EOInterfaceControllerCocktail responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal) {
        this(ctrl, responder, alocation_x, alocation_y, ataille_x, ataille_y, modal, false);
    }
    
    public SwingFinderOrgan(ApplicationCocktail ctrl,EOInterfaceControllerCocktail responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useTreeView) {
    	this(ctrl, responder, null, null, alocation_x, alocation_y, ataille_x, ataille_y, modal, useTreeView);
    }
    
	public SwingFinderOrgan(ApplicationCocktail ctrl,EOInterfaceControllerCocktail responder,JDialog parentD,JFrame parentF,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useTreeView) {
		super();
        this.useTreeView = useTreeView;
		// creation du nib Organ
        if(useTreeView)
        {
            setMonSwingFinderOrganTreeNibCtrl(new SwingFinderOrganTreeNibCtrl(ctrl,alocation_x , alocation_y, ataille_x , ataille_y));
            setMonSwingFinderOrganTreeNib( new SwingFinderOrganTreeNib());  
//            getMonSwingFinderOrganTreeNibCtrl().parentDialog = parentD;
//            getMonSwingFinderOrganTreeNibCtrl().parentFrame = parentF;
            getMonSwingFinderOrganTreeNibCtrl().creationFenetre(
                    getMonSwingFinderOrganTreeNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_Organ),
                    responder,
                    modal
            );
            getMonSwingFinderOrganTreeNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
            );
            getMonSwingFinderOrganTreeNib().setSize(ataille_x,ataille_y);     
            getMonSwingFinderOrganTreeNibCtrl().setSender(this);
        }
        else
        {
            setMonSwingFinderEOOrganNibCtrl(new SwingFinderEOOrganNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
            setMonSwingFinderEOOrganNib( new SwingFinderEOOrganNib());
//            getMonSwingFinderEOOrganNibCtrl().parentDialog = parentD;
//            getMonSwingFinderEOOrganNibCtrl().parentFrame = parentF;
            getMonSwingFinderEOOrganNibCtrl().creationFenetre(
                    getMonSwingFinderEOOrganNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_Organ),
                    responder,
                    modal
            );
            getMonSwingFinderEOOrganNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
            );
            getMonSwingFinderEOOrganNib().setSize(ataille_x,ataille_y);     
            getMonSwingFinderEOOrganNibCtrl().setSender(this);
        }

	}

	public void afficherFenetreFinder(JButton bt) {
        if(useTreeView)
            getMonSwingFinderOrganTreeNibCtrl().afficherFenetre();
        else
            getMonSwingFinderEOOrganNibCtrl().afficherFenetre();	
	}
    
    public void afficherFenetreFinder(JButton bt,EOExercice exercice) {
        if(useTreeView)
            getMonSwingFinderOrganTreeNibCtrl().afficherFenetre(exercice);
        else
            getMonSwingFinderEOOrganNibCtrl().afficherFenetre(exercice);  
    }


	public void relierBouton(JButtonCocktail bt) {
        if(useTreeView)
            bt.addDelegateActionListener(getMonSwingFinderOrganTreeNibCtrl(),METHODE_AFFICHER_FENETRE);
        else
            bt.addDelegateActionListener(getMonSwingFinderEOOrganNibCtrl(),METHODE_AFFICHER_FENETRE);	
	}

	public NSArray getResultat() {
        if(useTreeView)
            return  getMonSwingFinderOrganTreeNibCtrl().getResultat();
        else
            return	getMonSwingFinderEOOrganNibCtrl().getResultat();	
	}

	public JPanelCocktail getPanel(){
        if(useTreeView)
            return getMonSwingFinderOrganTreeNib();   
        else
            return getMonSwingFinderEOOrganNib();	
	}

	public SwingFinderEOOrganNib getMonSwingFinderEOOrganNib() {
		return monSwingFinderEOOrganNib;
	}

	public void setMonSwingFinderEOOrganNib(
			SwingFinderEOOrganNib monSwingFinderEOOrganNib) {
		this.monSwingFinderEOOrganNib = monSwingFinderEOOrganNib;
	}
    
    public void setMonSwingFinderOrganTreeNib(
            SwingFinderOrganTreeNib monSwingFinderOrganTreeNib) {
        this.monSwingFinderOrganTreeNib = monSwingFinderOrganTreeNib;
    }

	public SwingFinderEOOrganNibCtrl getMonSwingFinderEOOrganNibCtrl() {
		return monSwingFinderEOOrganNibCtrl;
	}

	public void setMonSwingFinderEOOrganNibCtrl(
			SwingFinderEOOrganNibCtrl monSwingFinderEOOrganNibCtrl) {
		this.monSwingFinderEOOrganNibCtrl = monSwingFinderEOOrganNibCtrl;
	}
    
    public void setMonSwingFinderOrganTreeNibCtrl(
            SwingFinderOrganTreeNibCtrl monSwingFinderOrganTreeNibCtrl) {
        this.monSwingFinderOrganTreeNibCtrl = monSwingFinderOrganTreeNibCtrl;
    }
    public SwingFinderOrganTreeNib getMonSwingFinderOrganTreeNib() {
        return monSwingFinderOrganTreeNib;
    }
    public SwingFinderOrganTreeNibCtrl getMonSwingFinderOrganTreeNibCtrl() {
        return monSwingFinderOrganTreeNibCtrl;
    }
}
