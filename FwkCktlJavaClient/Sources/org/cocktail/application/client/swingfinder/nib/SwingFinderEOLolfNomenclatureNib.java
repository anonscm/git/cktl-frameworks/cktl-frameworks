/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * SwingFinderEOLolfNomenclatureNib.java
 *
 * Created on 22 avril 2008, 14:46
 */

package org.cocktail.application.client.swingfinder.nib;

import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;

import org.cocktail.application.palette.JPanelCocktail;

/**
 *
 * @author  Michael Haller
 */
public class SwingFinderEOLolfNomenclatureNib extends JPanelCocktail {
    
    /** Creates new form SwingFinderEOLolfNomenclatureNib */
    public SwingFinderEOLolfNomenclatureNib() {
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jbAnnuler = new org.cocktail.application.palette.JButtonCocktail();
        jbValider = new org.cocktail.application.palette.JButtonCocktail();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jtvLolfDepense = new org.cocktail.application.palette.JTableViewCocktail();
        jfRechercheDepCode = new org.cocktail.application.palette.JTextFieldCocktail();
        jfRechercheDepAbrev = new org.cocktail.application.palette.JTextFieldCocktail();
        jfRechercheDepLibelle = new org.cocktail.application.palette.JTextFieldCocktail();
        jLabelCocktail1 = new org.cocktail.application.palette.JLabelCocktail();
        jLabelCocktail2 = new org.cocktail.application.palette.JLabelCocktail();
        jLabelCocktail3 = new org.cocktail.application.palette.JLabelCocktail();

        jbAnnuler.setText("Annuler");

        jbValider.setText("Selectionner");

        jfRechercheDepCode.setText("jTextFieldCocktail1");
        jfRechercheDepCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jfRechercheDepCodeActionPerformed(evt);
            }
        });

        jfRechercheDepAbrev.setText("jTextFieldCocktail1");

        jfRechercheDepLibelle.setText("jTextFieldCocktail1");

        jLabelCocktail1.setText("Code :");

        jLabelCocktail2.setText("Abreviation :");

        jLabelCocktail3.setText("Libelle :");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jtvLolfDepense, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 646, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabelCocktail1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jfRechercheDepCode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 77, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jfRechercheDepAbrev, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 109, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jfRechercheDepLibelle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 440, Short.MAX_VALUE)
                            .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jfRechercheDepCode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jfRechercheDepAbrev, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jfRechercheDepLibelle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jtvLolfDepense, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 280, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Actions Lolf", jPanel1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jbAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jbValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jTabbedPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 707, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(jTabbedPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jbValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jbAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jfRechercheDepCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jfRechercheDepCodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jfRechercheDepCodeActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.cocktail.application.palette.JLabelCocktail jLabelCocktail1;
    private org.cocktail.application.palette.JLabelCocktail jLabelCocktail2;
    private org.cocktail.application.palette.JLabelCocktail jLabelCocktail3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private org.cocktail.application.palette.JButtonCocktail jbAnnuler;
    private org.cocktail.application.palette.JButtonCocktail jbValider;
    private org.cocktail.application.palette.JTextFieldCocktail jfRechercheDepAbrev;
    private org.cocktail.application.palette.JTextFieldCocktail jfRechercheDepCode;
    private org.cocktail.application.palette.JTextFieldCocktail jfRechercheDepLibelle;
    private org.cocktail.application.palette.JTableViewCocktail jtvLolfDepense;
    // End of variables declaration//GEN-END:variables
    
    public JTextFieldCocktail getJfRechercheDepAbrev() {
        return jfRechercheDepAbrev;
    }

    public void setJfRechercheDepAbrev(JTextFieldCocktail jfRechercheDepAbrev) {
        this.jfRechercheDepAbrev = jfRechercheDepAbrev;
    }

    public JTextFieldCocktail getJfRechercheDepCode() {
        return jfRechercheDepCode;
    }

    public void setJfRechercheDepCode(JTextFieldCocktail jfRechercheDepCode) {
        this.jfRechercheDepCode = jfRechercheDepCode;
    }

    public JTextFieldCocktail getJfRechercheDepLibelle() {
        return jfRechercheDepLibelle;
    }

    public void setJfRechercheDepLibelle(JTextFieldCocktail jfRechercheDepLibelle) {
        this.jfRechercheDepLibelle = jfRechercheDepLibelle;
    }


    public JTableViewCocktail getJtvLolfDepense() {
        return jtvLolfDepense;
    }

    public void setJtvLolfDepense(JTableViewCocktail jtvLolfDepense) {
        this.jtvLolfDepense = jtvLolfDepense;
    }

    public JButtonCocktail getJbAnnuler() {
        return jbAnnuler;
    }

    public void setJbAnnuler(JButtonCocktail jbAnnuler) {
        this.jbAnnuler = jbAnnuler;
    }

    public JButtonCocktail getJbValider() {
        return jbValider;
    }

    public void setJbValider(JButtonCocktail jbValider) {
        this.jbValider = jbValider;
    }
    
}
