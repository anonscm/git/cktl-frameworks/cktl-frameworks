/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.application.client.swingfinder.nib;

import org.cocktail.application.palette.JPanelCocktail;

/**
 *
 * @author  mparadot
 */
public class SwingFinderGedNib extends JPanelCocktail {
    
    /** Creates new form SwingFinderGedNib */
    public SwingFinderGedNib() {
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonCocktail1 = new org.cocktail.application.palette.JButtonCocktail();
        tvcListCourrier = new org.cocktail.application.palette.JTableViewCocktail();
        panelBottom = new javax.swing.JPanel();
        btcFermer = new org.cocktail.application.palette.JButtonCocktail();
        panelLateral = new javax.swing.JPanel();
        btcAjouter = new org.cocktail.application.palette.JButtonCocktail();
        btcSupprimer = new org.cocktail.application.palette.JButtonCocktail();
        btcApercu = new org.cocktail.application.palette.JButtonCocktail();

        jButtonCocktail1.setText("jButtonCocktail1");

        btcFermer.setText("Fermer");

        org.jdesktop.layout.GroupLayout panelBottomLayout = new org.jdesktop.layout.GroupLayout(panelBottom);
        panelBottom.setLayout(panelBottomLayout);
        panelBottomLayout.setHorizontalGroup(
            panelBottomLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, panelBottomLayout.createSequentialGroup()
                .addContainerGap(311, Short.MAX_VALUE)
                .add(btcFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelBottomLayout.setVerticalGroup(
            panelBottomLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelBottomLayout.createSequentialGroup()
                .add(btcFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btcAjouter.setText("Ajouter");
        btcAjouter.setPreferredSize(new java.awt.Dimension(22, 22));

        btcSupprimer.setText("Supprimer");
        btcSupprimer.setPreferredSize(new java.awt.Dimension(22, 22));

        btcApercu.setText("Apercu");
        btcApercu.setPreferredSize(new java.awt.Dimension(22, 22));

        org.jdesktop.layout.GroupLayout panelLateralLayout = new org.jdesktop.layout.GroupLayout(panelLateral);
        panelLateral.setLayout(panelLateralLayout);
        panelLateralLayout.setHorizontalGroup(
            panelLateralLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelLateralLayout.createSequentialGroup()
                .addContainerGap()
                .add(panelLateralLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btcApercu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btcSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, btcAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelLateralLayout.setVerticalGroup(
            panelLateralLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelLateralLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(btcAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btcSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btcApercu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(174, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelBottom, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(tvcListCourrier, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelLateral, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(panelLateral, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(20, 20, 20)
                        .add(tvcListCourrier, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                        .add(11, 11, 11)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelBottom, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.cocktail.application.palette.JButtonCocktail btcAjouter;
    private org.cocktail.application.palette.JButtonCocktail btcApercu;
    private org.cocktail.application.palette.JButtonCocktail btcFermer;
    private org.cocktail.application.palette.JButtonCocktail btcSupprimer;
    private org.cocktail.application.palette.JButtonCocktail jButtonCocktail1;
    private javax.swing.JPanel panelBottom;
    private javax.swing.JPanel panelLateral;
    private org.cocktail.application.palette.JTableViewCocktail tvcListCourrier;
    // End of variables declaration//GEN-END:variables

    public org.cocktail.application.palette.JButtonCocktail getBtcAjouter() {
        return btcAjouter;
    }

    public void setBtcAjouter(org.cocktail.application.palette.JButtonCocktail btcAjouter) {
        this.btcAjouter = btcAjouter;
    }

    public org.cocktail.application.palette.JButtonCocktail getBtcApercu() {
        return btcApercu;
    }

    public void setBtcApercu(org.cocktail.application.palette.JButtonCocktail btcApercu) {
        this.btcApercu = btcApercu;
    }

    public org.cocktail.application.palette.JButtonCocktail getBtcFermer() {
        return btcFermer;
    }

    public void setBtcFermer(org.cocktail.application.palette.JButtonCocktail btcFermer) {
        this.btcFermer = btcFermer;
    }

    public org.cocktail.application.palette.JButtonCocktail getBtcSupprimer() {
        return btcSupprimer;
    }

    public void setBtcSupprimer(org.cocktail.application.palette.JButtonCocktail btcSupprimer) {
        this.btcSupprimer = btcSupprimer;
    }

    public org.cocktail.application.palette.JButtonCocktail getJButtonCocktail1() {
        return jButtonCocktail1;
    }

    public void setJButtonCocktail1(org.cocktail.application.palette.JButtonCocktail jButtonCocktail1) {
        this.jButtonCocktail1 = jButtonCocktail1;
    }

    public org.cocktail.application.palette.JTableViewCocktail getTvcListCourrier() {
        return tvcListCourrier;
    }

    public void setTvcListCourrier(org.cocktail.application.palette.JTableViewCocktail tvcListCourrier) {
        this.tvcListCourrier = tvcListCourrier;//.initTableViewCocktail(tvcListCourrier.getColumns(), tvcListCourrier.getData(), tvcListCourrier.getTable().getPreferredScrollableViewportSize(), tvcListCourrier.getTable().getAutoResizeMode());
    }
    
}
