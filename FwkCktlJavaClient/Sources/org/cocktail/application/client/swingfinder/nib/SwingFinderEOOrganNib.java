/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nib;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;

import javax.swing.WindowConstants;
import org.cocktail.application.palette.JButtonCocktail;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class SwingFinderEOOrganNib extends org.cocktail.application.palette.JPanelCocktail {
	private JButtonCocktail jButtonCocktailFiltrer;
	private JButtonCocktail jButtonCocktailSelectionner;
	private JCheckBox jCheckBoxCr;
	private JCheckBox jCheckBoxSousCr;
	private JCheckBox jCheckBoxComposante;
	private JPanel jPanelTbv;
	private JButtonCocktail jButtonCocktailAnnuler;

	/**
	* Auto-generated main method to display this 
	* JPanel inside a new JFrame.
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new SwingFinderEOOrganNib());
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	public SwingFinderEOOrganNib() {
		super();
		initGUI();
	}
	
	private void initGUI() {
		try {
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
			thisLayout.rowHeights = new int[] {7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7};
			thisLayout.columnWeights = new double[] {0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0};
			thisLayout.columnWidths = new int[] {7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7};
			this.setLayout(thisLayout);
			this.setPreferredSize(new java.awt.Dimension(544, 359));
			{
				jButtonCocktailFiltrer = new JButtonCocktail();
				this.add(jButtonCocktailFiltrer, new GridBagConstraints(9, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailFiltrer.setText("Filtrer");
			}
			{
				jButtonCocktailSelectionner = new JButtonCocktail();
				this.add(jButtonCocktailSelectionner, new GridBagConstraints(9, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailSelectionner.setText("Selectionner");
			}
			{
				jButtonCocktailAnnuler = new JButtonCocktail();
				this.add(jButtonCocktailAnnuler, new GridBagConstraints(7, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailAnnuler.setText("Annuler");
			}
			{
				jPanelTbv = new JPanel();
				this.add(jPanelTbv, new GridBagConstraints(1, 2, 9, 15, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
				BoxLayout jPanelTbvLayout = new BoxLayout(
					jPanelTbv,
					javax.swing.BoxLayout.X_AXIS);
				jPanelTbv.setLayout(jPanelTbvLayout);
			}
			{
				jCheckBoxComposante = new JCheckBox();
				this.add(getJCheckBoxComposante(), new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jCheckBoxComposante.setText("UFR");
			}
			{
				jCheckBoxCr = new JCheckBox();
				this.add(getJCheckBoxCr(), new GridBagConstraints(4, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jCheckBoxCr.setText("CR");
			}
			{
				jCheckBoxSousCr = new JCheckBox();
				this.add(jCheckBoxSousCr, new GridBagConstraints(7, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jCheckBoxSousCr.setText("SousCr");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JCheckBox getJCheckBoxComposante() {
		return jCheckBoxComposante;
	}
	
	public JCheckBox getJCheckBoxCr() {
		return jCheckBoxCr;
	}

	public JButtonCocktail getJButtonCocktailAnnuler() {
		return jButtonCocktailAnnuler;
	}

	public void setJButtonCocktailAnnuler(JButtonCocktail buttonCocktailAnnuler) {
		jButtonCocktailAnnuler = buttonCocktailAnnuler;
	}

	public JButtonCocktail getJButtonCocktailFiltrer() {
		return jButtonCocktailFiltrer;
	}

	public void setJButtonCocktailFiltrer(JButtonCocktail buttonCocktailFiltrer) {
		jButtonCocktailFiltrer = buttonCocktailFiltrer;
	}

	public JButtonCocktail getJButtonCocktailSelectionner() {
		return jButtonCocktailSelectionner;
	}

	public void setJButtonCocktailSelectionner(
			JButtonCocktail buttonCocktailSelectionner) {
		jButtonCocktailSelectionner = buttonCocktailSelectionner;
	}

	public JCheckBox getJCheckBoxSousCr() {
		return jCheckBoxSousCr;
	}

	public void setJCheckBoxSousCr(JCheckBox checkBoxSousCr) {
		jCheckBoxSousCr = checkBoxSousCr;
	}

	public JPanel getJPanelTbv() {
		return jPanelTbv;
	}

	public void setJPanelTbv(JPanel panelTbv) {
		jPanelTbv = panelTbv;
	}

	public void setJCheckBoxComposante(JCheckBox checkBoxComposante) {
		jCheckBoxComposante = checkBoxComposante;
	}

	public void setJCheckBoxCr(JCheckBox checkBoxCr) {
		jCheckBoxCr = checkBoxCr;
	}

}
