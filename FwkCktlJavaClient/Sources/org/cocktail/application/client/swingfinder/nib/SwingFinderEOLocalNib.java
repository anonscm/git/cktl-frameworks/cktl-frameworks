/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nib;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.JFrame;

import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JLabelCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class SwingFinderEOLocalNib extends org.cocktail.application.palette.JPanelCocktail {
	private JPanel jPanelResultat;
	private JPanel jPanelBatiment;
	private JButtonCocktail jButtonCocktailAnnuler;
	private JButtonCocktail jButtonCocktailSelectionner;

	/**
	* Auto-generated main method to display this 
	* JPanel inside a new JFrame.
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new SwingFinderEOIndividuUlrNib());
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	public SwingFinderEOLocalNib() {
		super();
		initGUI();
	}
	
	private void initGUI() {
		try {
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.1, 0.1, 0.0, 0.1};
			thisLayout.rowHeights = new int[] {26, 25, 20, 7, 39, 63, 63, 7, 20, 58, 7};
			thisLayout.columnWeights = new double[] {0.0, 0.0, 0.1, 0.1, 0.1, 0.1, 0.0, 0.0, 0.1, 0.0};
			thisLayout.columnWidths = new int[] {7, 20, 7, 7, 7, 7, 71, 49, 7, 7};
			this.setLayout(thisLayout);
			this.setPreferredSize(new java.awt.Dimension(400, 380));
			{
				jPanelResultat = new JPanel();
				BoxLayout jPanelResultatLayout = new BoxLayout(
					jPanelResultat,
					javax.swing.BoxLayout.X_AXIS);
				jPanelResultat.setLayout(jPanelResultatLayout);
				this.add(jPanelResultat, new GridBagConstraints(1, 6, 8, 4, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));
			}
			{
				jButtonCocktailSelectionner = new JButtonCocktail();
				this.add(jButtonCocktailSelectionner, new GridBagConstraints(7, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailSelectionner.setText("Selectionner");
			}
			{
				jButtonCocktailAnnuler = new JButtonCocktail();
				this.add(jButtonCocktailAnnuler, new GridBagConstraints(5, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailAnnuler.setText("Annuler");
			}
			{
				jPanelBatiment = new JPanel();
				BoxLayout jPanelBatimentLayout = new BoxLayout(
					jPanelBatiment,
					javax.swing.BoxLayout.X_AXIS);
				jPanelBatiment.setLayout(jPanelBatimentLayout);
				this.add(jPanelBatiment, new GridBagConstraints(1, 1, 8, 5, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JPanel getJPanelResultat() {
		return jPanelResultat;
	}
	
	public JButtonCocktail getJButtonCocktailSelectionner() {
		return jButtonCocktailSelectionner;
	}
	
	public JButtonCocktail getJButtonCocktailAnnuler() {
		return jButtonCocktailAnnuler;
	}
	

	
	public JPanel getJPanelBatiment() {
		return jPanelBatiment;
	}

}
