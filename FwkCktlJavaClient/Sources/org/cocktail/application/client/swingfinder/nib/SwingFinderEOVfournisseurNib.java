/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder.nib;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;

import javax.swing.WindowConstants;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JLabelCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class SwingFinderEOVfournisseurNib extends org.cocktail.application.palette.JPanelCocktail {
	private JButtonCocktail jButtonCocktailFiltrer;
	private JButtonCocktail jButtonCocktailAnnuler;
	private JButtonCocktail jButtonCocktailSelectionner;
	private JLabelCocktail jLabelCocktail1;
	private JButtonCocktail jbtInfo;
	private JCheckBox jCheckBoxPhysique;
	private JPanel jPanelTbv;
	private JCheckBox jCheckBoxMorale;
	private JCheckBox jCheckBoxClient;
	private JCheckBox jCheckBoxFournisseur;
	private JTextFieldCocktail jTextFieldCocktailNom;
	private JLabelCocktail jLabelCocktailNom;
	private JTextFieldCocktail jTextFieldCocktailCode;

	/**
	* Auto-generated main method to display this 
	* JPanel inside a new JFrame.
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new SwingFinderEOVfournisseurNib());
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	public SwingFinderEOVfournisseurNib() {
		super();
		initGUI();
	}
	
	private void initGUI() {
		try {
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0};
			thisLayout.rowHeights = new int[] {7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7};
			thisLayout.columnWeights = new double[] {0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.0};
			thisLayout.columnWidths = new int[] {7, 7, 7, 7, 7, 7, 7, 7, 114, 7, 7};
			this.setLayout(thisLayout);
			this.setPreferredSize(new java.awt.Dimension(495, 393));
			{
				jButtonCocktailFiltrer = new JButtonCocktail();
				this.add(jButtonCocktailFiltrer, new GridBagConstraints(9, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailFiltrer.setText("Filtrer");
			}
			{
				jButtonCocktailAnnuler = new JButtonCocktail();
				this.add(jButtonCocktailAnnuler, new GridBagConstraints(8, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailAnnuler.setText("Annuler");
			}
			{
				jButtonCocktailSelectionner = new JButtonCocktail();
				this.add(jButtonCocktailSelectionner, new GridBagConstraints(9, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailSelectionner.setText("Selectionner");
			}
			{
				jLabelCocktail1 = new JLabelCocktail();
				this.add(jLabelCocktail1, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jLabelCocktail1.setText("Code ?");
			}
			{
				jTextFieldCocktailCode = new JTextFieldCocktail();
				this.add(jTextFieldCocktailCode, new GridBagConstraints(4, 2, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
			}
			{
				jLabelCocktailNom = new JLabelCocktail();
				this.add(jLabelCocktailNom, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jLabelCocktailNom.setText("Nom ?");
			}
			{
				jTextFieldCocktailNom = new JTextFieldCocktail();
				this.add(jTextFieldCocktailNom, new GridBagConstraints(4, 3, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
			}
			{
				jCheckBoxFournisseur = new JCheckBox();
				this.add(jCheckBoxFournisseur, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jCheckBoxFournisseur.setText("Fournisseur");
			}
			{
				jCheckBoxClient = new JCheckBox();
				this.add(getJCheckBoxClient(), new GridBagConstraints(7, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jCheckBoxClient.setText("Client");
			}
			{
				jCheckBoxMorale = new JCheckBox();
				this.add(jCheckBoxMorale, new GridBagConstraints(1, 5, 6, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jCheckBoxMorale.setText("Pers. Morale");
			}
			{
				jCheckBoxPhysique = new JCheckBox();
				this.add(jCheckBoxPhysique, new GridBagConstraints(7, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jCheckBoxPhysique.setText("Pers. Physique");
			}
			{
				jbtInfo = new JButtonCocktail();
				this.add(getJbtInfo(), new GridBagConstraints(9, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
			}
			{
				jPanelTbv = new JPanel();
				BoxLayout jPanelTbvLayout = new BoxLayout(
					jPanelTbv,
					javax.swing.BoxLayout.X_AXIS);
				jPanelTbv.setLayout(jPanelTbvLayout);
				this.add(getJPanelTbv(), new GridBagConstraints(1, 6, 9, 13, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JCheckBox getJCheckBoxFournisseur() {
		return jCheckBoxFournisseur;
	}
	
	public JCheckBox getJCheckBoxClient() {
		return jCheckBoxClient;
	}
	
	public JPanel getJPanelTbv() {
		return jPanelTbv;
	}

	public JButtonCocktail getJButtonCocktailAnnuler() {
		return jButtonCocktailAnnuler;
	}

	public void setJButtonCocktailAnnuler(JButtonCocktail buttonCocktailAnnuler) {
		jButtonCocktailAnnuler = buttonCocktailAnnuler;
	}

	public JButtonCocktail getJButtonCocktailFiltrer() {
		return jButtonCocktailFiltrer;
	}

	public void setJButtonCocktailFiltrer(JButtonCocktail buttonCocktailFiltrer) {
		jButtonCocktailFiltrer = buttonCocktailFiltrer;
	}

	public JButtonCocktail getJButtonCocktailSelectionner() {
		return jButtonCocktailSelectionner;
	}

	public void setJButtonCocktailSelectionner(
			JButtonCocktail buttonCocktailSelectionner) {
		jButtonCocktailSelectionner = buttonCocktailSelectionner;
	}

	public JCheckBox getJCheckBoxMorale() {
		return jCheckBoxMorale;
	}

	public void setJCheckBoxMorale(JCheckBox checkBoxMorale) {
		jCheckBoxMorale = checkBoxMorale;
	}

	public JCheckBox getJCheckBoxPhysique() {
		return jCheckBoxPhysique;
	}

	public void setJCheckBoxPhysique(JCheckBox checkBoxPhysique) {
		jCheckBoxPhysique = checkBoxPhysique;
	}

	public JTextFieldCocktail getJTextFieldCocktailCode() {
		return jTextFieldCocktailCode;
	}

	public void setJTextFieldCocktailCode(JTextFieldCocktail textFieldCocktailCode) {
		jTextFieldCocktailCode = textFieldCocktailCode;
	}

	public JTextFieldCocktail getJTextFieldCocktailNom() {
		return jTextFieldCocktailNom;
	}

	public void setJTextFieldCocktailNom(JTextFieldCocktail textFieldCocktailNom) {
		jTextFieldCocktailNom = textFieldCocktailNom;
	}

	public void setJCheckBoxClient(JCheckBox checkBoxClient) {
		jCheckBoxClient = checkBoxClient;
	}

	public void setJCheckBoxFournisseur(JCheckBox checkBoxFournisseur) {
		jCheckBoxFournisseur = checkBoxFournisseur;
	}

	public void setJPanelTbv(JPanel panelTbv) {
		jPanelTbv = panelTbv;
	}
	/**
	* This method should return an instance of this class which does 
	* NOT initialize it's GUI elements. This method is ONLY required by
	* Jigloo if the superclass of this class is abstract or non-public. It 
	* is not needed in any other situation.
	 */
	public static Object getGUIBuilderInstance() {
		return new SwingFinderEOVfournisseurNib(Boolean.FALSE);
	}
	
	/**
	 * This constructor is used by the getGUIBuilderInstance method to
	 * provide an instance of this class which has not had it's GUI elements
	 * initialized (ie, initGUI is not called in this constructor).
	 */
	public SwingFinderEOVfournisseurNib(Boolean initGUI) {
		super();
	}
	
	public JButtonCocktail getJbtInfo() {
		return jbtInfo;
	}

}
