/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 */

package org.cocktail.application.client.swingfinder;

import javax.swing.JButton;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOLolfNomenclatureNib;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOLolfNomenclatureNibCtrl;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.application.palette.ToolsSwing;

import com.webobjects.foundation.NSArray;
/**
 *
 * @author Michael Haller
 */
public class SwingFinderLolfNomenclature extends SwingFinder{

    private static final String FENETRE_DE_SELECTION_DES_NOMENCLATURES_LOLF="Fenetre de selection des actions lolf";

    private SwingFinderEOLolfNomenclatureNib monSwingFinderEOLolfNomenclatureNib= null;
    private SwingFinderEOLolfNomenclatureNibCtrl monSwingFinderEOLolfNomenclatureNibCtrl= null;
    
    private static final String METHODE_AFFICHER_FENETRE = "afficherFenetre";
    
    private boolean useRecette=false;
    private String titleExtensionName="(Depenses)";
    
    public SwingFinderLolfNomenclature(ApplicationCocktail ctrl,NibCtrl responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useRecette) {
        super();
        this.useRecette = useRecette;
        if(this.useRecette)
            titleExtensionName="(Recettes)";

            if(alocation_x<640)
                alocation_x = 640;
            if(alocation_y<480)
                alocation_y = 480;
            if(ataille_x<740)
                ataille_x = 740;
            if(ataille_y<460)
                ataille_y = 460;
            setMonSwingFinderEOLolfNomenclatureNibCtrl(new SwingFinderEOLolfNomenclatureNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y,useRecette));
            setMonSwingFinderEOLolfNomenclatureNib(new SwingFinderEOLolfNomenclatureNib());
            
            getMonSwingFinderEOLolfNomenclatureNibCtrl().creationFenetre(
                    getMonSwingFinderEOLolfNomenclatureNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_DES_NOMENCLATURES_LOLF+" "+titleExtensionName),
                    responder,
                    modal
            );
            getMonSwingFinderEOLolfNomenclatureNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y));
            getMonSwingFinderEOLolfNomenclatureNib().setSize(ataille_x,ataille_y);       
            getMonSwingFinderEOLolfNomenclatureNibCtrl().setSender(this);
        
    }


    public SwingFinderLolfNomenclature(ApplicationCocktail ctrl,EOInterfaceControllerCocktail responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useRecette) {
        super();
        this.useRecette = useRecette;
 
            setMonSwingFinderEOLolfNomenclatureNibCtrl(new SwingFinderEOLolfNomenclatureNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y,useRecette));
            getMonSwingFinderEOLolfNomenclatureNibCtrl().creationFenetre(
                    getMonSwingFinderEOLolfNomenclatureNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_DES_NOMENCLATURES_LOLF+" "+titleExtensionName),
                    responder,
                    modal
            );
            getMonSwingFinderEOLolfNomenclatureNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
            );
            getMonSwingFinderEOLolfNomenclatureNib().setSize(ataille_x,ataille_y);       
            getMonSwingFinderEOLolfNomenclatureNibCtrl().setSender(this);
        
    }

    public void afficherFenetreFinder(JButton bt) {
            getMonSwingFinderEOLolfNomenclatureNibCtrl().afficherFenetre();  
    }


    public void relierBouton(JButtonCocktail bt) {
            bt.addDelegateActionListener(getMonSwingFinderEOLolfNomenclatureNibCtrl(),METHODE_AFFICHER_FENETRE); 
    }

    public NSArray getResultat() {
        return  getMonSwingFinderEOLolfNomenclatureNibCtrl().getResultat();  
    }

    public JPanelCocktail getPanel(){
        return getMonSwingFinderEOLolfNomenclatureNib(); 
    }

    public SwingFinderEOLolfNomenclatureNib getMonSwingFinderEOLolfNomenclatureNib() {
        return monSwingFinderEOLolfNomenclatureNib;
    }

    public void setMonSwingFinderEOLolfNomenclatureNib(SwingFinderEOLolfNomenclatureNib monSwingFinderEOLolfNomenclatureNib) {
        this.monSwingFinderEOLolfNomenclatureNib = monSwingFinderEOLolfNomenclatureNib;
    }

    public SwingFinderEOLolfNomenclatureNibCtrl getMonSwingFinderEOLolfNomenclatureNibCtrl() {
        return monSwingFinderEOLolfNomenclatureNibCtrl;
    }

    public void setMonSwingFinderEOLolfNomenclatureNibCtrl(SwingFinderEOLolfNomenclatureNibCtrl monSwingFinderEOLolfNomenclatureNibCtrl) {
        this.monSwingFinderEOLolfNomenclatureNibCtrl = monSwingFinderEOLolfNomenclatureNibCtrl;
    }

    public boolean isUseRecette() {
        return useRecette;
    }

    public void setUseRecette(boolean useRecette) {
        this.useRecette = useRecette;
    }    
    

}
