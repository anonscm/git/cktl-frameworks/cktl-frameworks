/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.tools;

import java.lang.reflect.Method;

import javax.swing.JComponent;

import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.foundation.NSMutableArray;

public class ToolsCocktailJefyAdminPrivileges {

	
	private ApplicationCocktail app =null;
	private NSMutableArray mesObjetsAvecPrivileges=null;
	private NSMutableArray mesPrivileges=null;

	
	public ToolsCocktailJefyAdminPrivileges(ApplicationCocktail app, NSMutableArray mesPrivileges) {
		super();
		this.app = app;
		this.mesObjetsAvecPrivileges = new NSMutableArray();
		this.mesPrivileges = mesPrivileges;
		
		//System.out.println("ToolsCocktailJefyAdmin..");
	}

	public void appliquerLesPrivilegesUI(){
		//System.out.println("fireIUUpdate.. callMethodeSetDisabled");
		// on diseabled tous les objects	
		for (int i = 0; i < getMesObjetsAvecPrivileges().count(); i++) {
            if(((ObjetsAvecPrivileges)getMesObjetsAvecPrivileges().objectAtIndex(i)).getKey()!=null)
				callMethodeSetDisabled(((ObjetsAvecPrivileges)getMesObjetsAvecPrivileges().objectAtIndex(i)).getObj());
		}
		//System.out.println("fireIUUpdate.. callMethodeSetEnabled");
		// on enabled les objects qui sont autorises
		for (int i = 0; i < mesPrivileges.count(); i++) {
			ToolsCocktailPrivileges  tmpPriv = (ToolsCocktailPrivileges)mesPrivileges.objectAtIndex(i);
			
			for (int j = 0; j < mesObjetsAvecPrivileges.count(); j++) {
				if (((ObjetsAvecPrivileges)getMesObjetsAvecPrivileges().objectAtIndex(j)).getKey().equals(tmpPriv.getKey()))
					callMethodeSetEnabled(((ObjetsAvecPrivileges)getMesObjetsAvecPrivileges().objectAtIndex(j)).getObj());
			}
			
		}
		
		//System.out.println("fireIUUpdate..");

	}
    
    public void enableAll(){
        //System.out.println("fireIUUpdate.. callMethodeSetDisabled");
        // on diseabled tous les objects    
        for (int i = 0; i < getMesObjetsAvecPrivileges().count(); i++) {
            if(((ObjetsAvecPrivileges)getMesObjetsAvecPrivileges().objectAtIndex(i)).getKey()!=null)
                callMethodeSetEnabled(((ObjetsAvecPrivileges)getMesObjetsAvecPrivileges().objectAtIndex(i)).getObj());
        }
    }
	
	public void ajouterObjetPourPrivileges(Object obj,String priv){
		mesObjetsAvecPrivileges.addObject(new ObjetsAvecPrivileges(obj,priv));
	}
	
	public void retirerObjetPourPrivileges(Object obj,String key){
		
	}
	

	private void callMethodeSetEnabled(Object object)
	{
		// try du call
		// try du call
		try {
			Class c;
			c = object.getClass();
			// Recupereation de la methode getXxxxx :
			Method call = c.getMethod("setEnabledFromPrivileges");
			// appel de la methode
			call.invoke(object);

		} catch (Throwable ex) {
            try {
                ((JComponent)object).setEnabled(true);
            } catch (Exception e) {
                // TODO: handle exception
            }
			//	System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
		}

	}
	

	private void callMethodeSetDisabled(Object object)
	{
		// try du call
		try {
			Class c;
			c = object.getClass();
			// Recupereation de la methode getXxxxx :
			Method call = c.getMethod("setDisabledFromPrivileges");
			// appel de la methode
			call.invoke(object);

		} catch (Throwable ex) {
            try {
                ((JComponent)object).setEnabled(false);
            } catch (Exception e) {
                // TODO: handle exception
            }
			//	System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
		}

	}
    
    
	
	public NSMutableArray getMesObjetsAvecPrivileges() {
		return mesObjetsAvecPrivileges;
	}


	public void setMesObjetsAvecPrivileges(NSMutableArray mesObjetsAvecPrivileges) {
		this.mesObjetsAvecPrivileges = mesObjetsAvecPrivileges;
	}



}
