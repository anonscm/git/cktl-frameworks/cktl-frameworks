/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 25 sept. 07
 * author mparadot 
 */
package org.cocktail.application.client.tools;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class UserInfoCocktail extends NSMutableDictionary {

    public UserInfoCocktail() {
        super();
    }
    
    public UserInfoCocktail(NSDictionary dico){
        super();
        if(dico==null)
        {
            System.out.println("UserInfoCocktail.UserInfoCocktail()--->null");
            return;
        }
        setEmail((String) dico.valueForKey("email"));
        setNoIndividu((Number) dico.valueForKey("noIndividu"));
        setUserName((String) dico.valueForKey("userName"));
        setNoCompte((Number) dico.valueForKey("noCompte"));
        setNom((String) dico.valueForKey("nom"));
        setPrenom((String) dico.valueForKey("prenom"));
        setVLan((String) dico.valueForKey("vLan"));
        setLogin((String) dico.valueForKey("login"));
        setPersId((Number) dico.valueForKey("persId"));
    }
    
    public Number persId(){
        return (Number) valueForKey("persId");
    }
    
    public void setPersId(Number persId){
        if(persId!=null)
            takeValueForKey(persId, "persId");
    }
    
    public Number noIndividu(){
        return (Number) valueForKey("noIndividu");
    }
    
    public void setNoIndividu(Number noIndividu){
        if(noIndividu!=null)
            takeValueForKey(noIndividu, "noIndividu");
    }
    
    public String userName(){
        return (String) valueForKey("userName");
    }
    
    public void setUserName(String userName){
        if(userName!=null)
            takeValueForKey(userName, "userName");
    }
    
    public Number noCompte(){
        return (Number) valueForKey("noCompte");
    }
    
    public void setNoCompte(Number noCompte){
        if(noCompte!=null)
            takeValueForKey(noCompte, "noCompte");
    }
    
    public String nom(){
        return (String) valueForKey("nom");
    }
    
    public void setNom(String nom){
        if(nom!=null)
            takeValueForKey(nom, "nom");
    }
    
    public String prenom(){
        return (String) valueForKey("prenom");
    }
    
    public void setPrenom(String prenom){
        if(prenom!=null)
         takeValueForKey(prenom, "prenom");
    }
    
    public String email(){
        return (String) valueForKey("email");
    }
    
    public void setEmail(String email){
        if(email!=null)
            takeValueForKey(email, "email");
    }
    
    public String vLan(){
        return (String) valueForKey("vLan");
    }
    
    public void setVLan(String vLan){
        if(vLan!=null)
            takeValueForKey(vLan, "vLan");
    }
    
    public String login(){
        return (String) valueForKey("login");
    }
    
    public void setLogin(String login){
        if(login!=null)
            takeValueForKey(login, "login");
    }
    
    public String lastError(){
        String str = (String) valueForKey("lastError");
        if(str==null || "vide".equals(str))
            return null;
        return (String) valueForKey("lastError");
    }
    
    public void setLastError(String err){
        if(err!=null && !"".equals(err))
            takeValueForKey(err, "lastError");
        else
            takeValueForKey("vide", "lastError");
    }
    
    public boolean hasError(){
        return lastError()!=null && !"vide".equals(lastError());
    }
    
    public Object valueForKey(String key) {
        return objectForKey(key);
    }
    
    public void takeValueForKey(Object value, String key) {
        super.setObjectForKey(value, key);
    }
}
