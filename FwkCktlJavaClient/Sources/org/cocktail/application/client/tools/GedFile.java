/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.application.client.tools;

import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSKeyValueCodingAdditions;


public class GedFile
    implements NSKeyValueCodingAdditions
{

    private GedFile(Integer numero, String url, String name)
    {
    	System.out.println("GedFile.GedFile(numero, url, name)" + numero+" , "+ url+" , "+ name);
    	_numero = numero;
        _url = url;
        _name = name;
    }

    public static GedFile getGedFileForNumero(Integer i,Number persId)
    {
    	System.out.println("GedFile.getGedFileForNumero(i, persId)" + i +" , "+ persId);
        if(i == null)
            return null;
        if(!gedDisponible())
        	return null;
        EOEditingContext ec = new EOEditingContext();
        NSArray list = (NSArray)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", "clientSideRequestUrlFicherDansGed", new Class[] {
            java.lang.Integer.class,java.lang.Number.class
        }, new Object[] {
            i,persId
        }, true);
        NSArray list2 = (NSArray)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", "clientSideRequestNameFicherDansGed", new Class[] {
            java.lang.Integer.class,java.lang.Number.class
        }, new Object[] {
            i,persId
        }, true);
        GedFile file = new GedFile(i, (String)list.lastObject(), (String)list2.lastObject());
        if(file.fileIsEmpty())
            return null;
        else
            return file;
    }
    
    public static boolean gedDisponible(){
    	System.out.println("GedFile.gedDisponible()");
    	EOEditingContext ec = new EOEditingContext();
    	return ((Integer)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", "clientSideRequestGedDisponible",null,null, true)).intValue()==1;
    }

    public Integer getNumero()
    {
    	System.out.println("GedFile.getNumero()");
    	return _numero;
    }

    public String getUrl()
    {
    	System.out.println("GedFile.getUrl()");
        return _url;
    }

    public String getName()
    {
    	System.out.println("GedFile.getName()");
        return _name;
    }

    public String toString()
    {
    	System.out.println("GedFile.toString()");
        return _url;
    }

    private boolean fileIsEmpty()
    {
    	System.out.println("GedFile.fileIsEmpty()");
        return (_name == null || "".equals(_name)) && (_url == null || "".equals(_url));
    }

    public void takeValueForKey(Object obj, String s)
    {
    }

    public Object valueForKey(String arg0)
    {
    	System.out.println("GedFile.valueForKey(arg0)" + arg0);
        if(KEY_NUMERO.equals(arg0))
            return _numero;
        if(KEY_URL.equals(arg0))
            return _url;
        if(KEY_NAME.equals(arg0))
            return _name;
        else
            return null;
    }
    
    public static String getLastErrorGed(){
    	System.out.println("GedFile.getLastErrorGed()");
    	if(!gedDisponible())
    		return "Impossible d'acceder a la Gestion Electronique des Documents. (probleme d'initialisation !)";
    	EOEditingContext ec = new EOEditingContext();
    	return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", "clientSideRequestGedLastError", null, null, true);
    }

    private Integer _numero;
    private String _url;
    private String _name;
    private static ApplicationCocktail app = (ApplicationCocktail)EOApplication.sharedApplication();
    
    public static final String KEY_NUMERO="numero";
    public static final String KEY_URL="url";
    public static final String KEY_NAME="name";
    public void takeValueForKeyPath(Object arg0, String arg1) {
            

    }

    public Object valueForKeyPath(String arg0) {
    	System.out.println("GedFile.valueForKeyPath(arg0)" + arg0);
            return valueForKey(arg0);
    }

    public boolean equals(Object obj) {
    	System.out.println("GedFile.equals(obj)" + obj);
        if(obj==null || !(obj instanceof GedFile))
            return false;
        return getNumero().equals(((GedFile)obj).getNumero());
    }

	@Override
	public int hashCode() {
		return _numero;
	}

}
