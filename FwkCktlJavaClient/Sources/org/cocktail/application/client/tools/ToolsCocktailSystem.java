/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)
package org.cocktail.application.client.tools;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.fwkcktlwebapp.common.util.CktlMailMessage;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

public class ToolsCocktailSystem {

    private static ApplicationCocktail app = (ApplicationCocktail) EOApplication.sharedApplication();

    // Chaines correspondant a System.getProperties().getProperty(os.name)
    public static final String MAC_OS_X_OS_NAME = "Mac OS X";

    public static final String WINDOWSXP_OS_NAME = "Windows XP";

    public static final String WINDOWS2000_OS_NAME = "Windows 2000";

    public static final String WINDOWS_OS_NAME = "Win";

    public static final String WINDOWSNT_OS_NAME = "Windows NT";

    public static final String LINUX_OS_NAME = "Linux";

    // Chaines correspondant aux commandes permettant de lancer une commande sur les différents systemes
    public static final String MAC_OS_X_EXEC = "open ";

    public static final String WINDOWSNT_EXEC = "cmd /C start ";

    public static final String WINDOWSXP_EXEC = "cmd /C start ";

    public static final String WINDOWS2000_EXEC = "start ";

    private static final String WINDOWS_EXEC_DLL = "url.dll,FileProtocolHandler";

    private static final String WINDOWS_EXEC_RUNDLL32 = "rundll32";

    public static final String LINUX_EXEC = "xdg-open ";
    
    private static final EOClientResourceBundle leBundle = new EOClientResourceBundle();

    // Envoi d'un mail "Courrier"
    public boolean envoyerMail(NSDictionary leObjet) {
        /*
         * smtpHost destinataire expediteur sujet texte
         */
        String from, to, text, subject;
        CktlMailMessage mailMessage;

        // Creation d'un objet CktlMailMessage a chaque envoi
        try {
            mailMessage = new CktlMailMessage(app.getMandatoryParameterForKey("HOST_MAIL"));
        } catch (Exception exception) {
            getApp().getToolsCocktailLogs().addLogMessage(this, "Exception", "Envoi de mail : " + exception.getMessage(), getApp().withLogs());
            return false;
        }
       
        from = (String) leObjet.objectForKey("expediteur");
        to = (String) leObjet.objectForKey("destinataire");
        if (from.equals("") || to.equals("")) {
            getApp().getToolsCocktailLogs().addLogMessage(this, "Erreur", "Envoi mail() : Expediteur ou destinataire manquant", getApp().withLogs());
            return false;
        }

        subject = (String) leObjet.objectForKey("sujet");
        text = (String) leObjet.objectForKey("texte");
        // Envoi du message
        try {
            mailMessage.initMessage(from, to, subject, text);
            mailMessage.send();
        } catch (Exception exception) {
            getApp().getToolsCocktailLogs().addLogMessage(this, "Exception", "Envoi mail : " + exception.getMessage(), getApp().withLogs());
            return false;
        }

        return true;
    }

    public String saveToUserHomeDir(NSData datas, final String fileNameWithoutExt, final int typeFormat) throws java.io.IOException {
        String extension;
        switch (typeFormat) {
        case /* PDF_FORMAT_ID */0:
            extension = new String(".pdf");
            break;
        case /* XLS_FORMAT_ID */1:
            extension = new String(".xls");
            break;
        case /* CSV_FORMAT_ID */2:
            extension = new String(".csv");
            break;
        case /* HTML_FORMAT_ID */3:
            extension = new String(".html");
            break;

        default:
            extension = new String(".pdf");
            break;
        }
        String OUT_FILE_NAME = fileNameWithoutExt;

        // répertoire cible par défaut :
        String targetDir = System.getProperty("java.io.tmpdir");
        if (!targetDir.endsWith(java.io.File.separator)) {
            targetDir = targetDir + java.io.File.separator;
        }
        java.io.File f = new java.io.File(targetDir + OUT_FILE_NAME + extension);
        // on verifie si le fichier cible existe deja :
        if (f.exists()) {
            // on va renomer l'existant en utilisant sa date de dernière modif :
            java.util.Calendar calLastModif = java.util.Calendar.getInstance();
            calLastModif.setTime(new java.util.Date(f.lastModified()));
            String lastModif = "-" + calLastModif.get(java.util.Calendar.DAY_OF_MONTH) + "." + (calLastModif.get(java.util.Calendar.MONTH)+1) + "." + calLastModif.get(java.util.Calendar.YEAR) + "-"
                    + calLastModif.get(java.util.Calendar.HOUR_OF_DAY) + "h" + calLastModif.get(java.util.Calendar.MINUTE) + "m" + calLastModif.get(java.util.Calendar.SECOND);
            String newName = targetDir + OUT_FILE_NAME + lastModif + extension;
            f.renameTo(new java.io.File(newName));
            // System.out.println("Le fichier "+targetDir+OUT_FILE_NAME+extension+" existait deja, il a ete renomme en "+newName+".");
        }
        // enregistrement du fichier sur le disque :
        if (f.createNewFile()) {
            java.io.FileOutputStream fileOutputStream = new java.io.FileOutputStream(f);
            datas.writeToStream(fileOutputStream);
            fileOutputStream.close();
        }
        return f.getAbsolutePath();
    }

    public void openFile(String filePath) {
        try {
            final String platform = System.getProperties().getProperty("os.name");
            final File aFile = new File(filePath);
            final Runtime runtime = Runtime.getRuntime();
            // ZLogger.debug("Ouverture du fichier " + aFile);
            if (platform.startsWith(WINDOWS_OS_NAME)) {
                if (WINDOWSNT_OS_NAME.equals(platform)) {
                    // ZLogger.verbose("Plateforme "+platform+", utilisation de
                    // cmd.exe /c");
                    runtime.exec(WINDOWSNT_EXEC + aFile);
                } else {
                    runtime.exec(new String[] { WINDOWS_EXEC_RUNDLL32, WINDOWS_EXEC_DLL, "\"" + aFile + "\"" });
                }
            } else if (platform.startsWith(MAC_OS_X_OS_NAME)) {
                Runtime.getRuntime().exec(MAC_OS_X_EXEC + aFile);
            } else if (platform.startsWith(LINUX_OS_NAME)) {
                if (System.getProperties().getProperty("os.name").startsWith(LINUX_OS_NAME)) {

                    System.out.println("OUVERTURE DU FICHIER : " + filePath);
                 
                    try {
                            Runtime.getRuntime().exec(LINUX_EXEC + filePath);
                        } catch (Exception exception) {
                            EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : " + aFile.getPath()
                                    + "\nMESSAGE : " + exception.getMessage());
                            exception.printStackTrace();
                        }

                }

            } else {
                // ZLogger.warning(PLATEFORME_NON_SUPPORTEE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
     
    public NSData getFileFromWebServeurRessources(String fileName){
        return (NSData)((EODistributedObjectStore)app.getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(app.getAppEditingContext(),
                "session.remoteCallDelagate",
                "clientSideRequestGetFileFromWebServeurResources",
                new Class[]{String.class},new Object[]{fileName},true);
    }
    
    public  Object getObjectFromWebServeurRessources(String fileName){
    	try {
    		return leBundle.getObject(fileName);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
    }

    
    public String systemClientJVM() {
        return System.getProperty("java.vm.version");
    }

    public ToolsCocktailSystem(ApplicationCocktail app) {
        super();
        // this.setApp(app);
    }

    void setApp(ApplicationCocktail app) {
        this.app = app;
    }

    ApplicationCocktail getApp() {
        return app;
    }

    public boolean envoyerMail(NSDictionary leObjet, String pathfile) {
        ThreadEmail  th = new ThreadEmail(leObjet,pathfile);
        th.start();
        return true;
    }
    
    public boolean envoyerMail(NSDictionary leObjet, NSData file,String fileName) {
      ThreadEmail  th = new ThreadEmail(leObjet,file,fileName);
      th.start();
      return true;
  }

    private JFrame frameEcran() {
        if (app.screenshot == null) {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            // On récupère la taille de l'écran par défaut :
            Dimension dim = toolkit.getScreenSize();
            JFrame frame = new JFrame();
            frame.setBounds(0, 0, dim.width, dim.height);
            app.screenshot = frame;
        }
        return app.screenshot;
    }
    
    public void playSound(String fileName){
        try{
            sound player = new sound(fileName);
            InputStream stream = new ByteArrayInputStream(player.getSamples());
            player.play(stream); 
        }catch(Throwable e){e.printStackTrace();}
    }

    class sound {

        private AudioFormat format;

        private byte[] samples;

        public sound(String filename) {
            try {
                NSData fichier = getFileFromWebServeurRessources(filename);
                System.out.println("file = "+fichier.length());
                AudioInputStream stream = AudioSystem.getAudioInputStream(fichier.stream());
                format = stream.getFormat();
                samples = getSamples(stream);
            } catch (UnsupportedAudioFileException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public byte[] getSamples() {
            return samples;
        }

        public byte[] getSamples(AudioInputStream stream) {
            int length = (int) (stream.getFrameLength() * format.getFrameSize());
            byte[] samples = new byte[length];
            DataInputStream in = new DataInputStream(stream);
            try {
                in.readFully(samples);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return samples;
        }

        public void play(InputStream source) {
            // 100 ms buffer for real time change to the sound stream
            int bufferSize = format.getFrameSize() * Math.round(format.getSampleRate() / 10);
            byte[] buffer = new byte[bufferSize];
            SourceDataLine line;
            try {
                DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
                line = (SourceDataLine) AudioSystem.getLine(info);
                line.open(format, bufferSize);
            } catch (LineUnavailableException e) {
                e.printStackTrace();
                return;
            }
            line.start();
            try {
                int numBytesRead = 0;
                while (numBytesRead != -1) {
                    numBytesRead = source.read(buffer, 0, buffer.length);
                    if (numBytesRead != -1)
                        line.write(buffer, 0, numBytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            line.drain();
            line.close();
        }
    }
    
    public String getTemporaryPath(){
    	String path = System.getProperty("java.io.tmpdir");
        if (!path.endsWith(java.io.File.separator)) {
            path = path + java.io.File.separator;
        }
        return path;
    }
    
    public String getTemporaryPathFileName(String fileName, String ext){
        String path = getTemporaryPath()+getTemporaryFileName(fileName, ext);
        return path;
    }
    
    public String getTemporaryFileName(String fileName, String ext){
        java.util.Calendar calLastModif = java.util.Calendar.getInstance();
        calLastModif.setTime(new java.util.Date());
        String lastModif = "-" + calLastModif.get(java.util.Calendar.DAY_OF_MONTH) + "." + (calLastModif.get(java.util.Calendar.MONTH)+1) + "." + calLastModif.get(java.util.Calendar.YEAR) + "-"
                + calLastModif.get(java.util.Calendar.HOUR_OF_DAY) + "h" + calLastModif.get(java.util.Calendar.MINUTE) + "m" + calLastModif.get(java.util.Calendar.SECOND);
        return fileName+lastModif+ext;
    }
    
    
    public class ThreadEmail extends Thread {
        /** Un attribut propre à chaque thread */
        private NSDictionary leObjet;
        private String pathfile;
        private String fileName;
        private NSData data;

        /** Création et démarrage automatique du thread */
        public ThreadEmail(NSDictionary leObjet, String pathfile) {
            this.leObjet=leObjet;
            this.pathfile=pathfile;
        }
        
        public ThreadEmail(NSDictionary leObjet, NSData data, String fileName) {
            this.leObjet=leObjet;
            this.data=data;
            this.fileName=fileName;
        }
        
        public void run() {
            try {
                sleep(2000);
                this.envoiEmail();
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }

        public final void envoiEmail() throws Exception {
            // appel de la procedure sur la session
            if(data==null)
                ((EODistributedObjectStore) getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate",
                    "clientSideRequestEnvoyerMail", new Class[] { NSDictionary.class, String.class }, new Object[] { leObjet, pathfile }, false);
            else
                ((EODistributedObjectStore) getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate",
                        "clientSideRequestEnvoyerMail", new Class[] { NSDictionary.class, NSData.class, String.class }, new Object[] { leObjet, data,fileName }, false);
        }

    }
}
