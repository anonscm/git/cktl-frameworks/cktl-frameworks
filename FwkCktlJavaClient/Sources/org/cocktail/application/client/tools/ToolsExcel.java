/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.tools;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/**
 *
 * Name: Excel.java
 *
 * Purpose: To demonstrate how to use ODBC and Excel to create
 * a table, insert data into it, and select it back out.
 *
 * Version: Developed using JDK 1.3, but also works with JDK
 * 1.2.2
 *
 * Instructions:
 *
 * 1) Create a new Excel spreadsheet
 *
 * 2) Create a new ODBC data source that points to this
 * spreadsheet
 *
 * a) Go to Control Panel
 * b) Open "ODBC Data sources (32-bit) (wording may be
 * slightly different for different platforms)
 * c) Under "User DSN" tab, press "Add" button
 * d) Select the "Microsoft Excel Driver (*.xls)" and
 * press "Finish" button
 * e) Enter "Data Source Name" of "TestExcel"
 * f) Press "Select Workbook" button
 * g) Locate and select the spreadsheet you created in
 * Step 1
 * h) Unselect the "Read Only" checkbox
 * i) Press "Ok" button
 *
 * 3) Compile and run Excel.java
 *
 * 4) Open Excel spreadsheet and you will find a newly
 * created sheet, GOOD_DAY, with three rows of data.
 *
 * Notes:
 * If you want to select data from a spreadsheet that was
 * NOT created via JDBC-ODBC (i.e. you entered data manually
 * into a spreadsheet and want to select it out), you must
 * reference the sheet name as "[sheetname$]".
 *
 * When you create the table and insert the data using
 * Java, you must reference the sheet name as "sheetname".
 *
 * Also, do not have the spreadsheet open when you are
 * running the program. You can get locking conflicts.
 *
 *
 */

public class ToolsExcel{
    private static ApplicationCocktail app = (ApplicationCocktail) EOApplication.sharedApplication();
	public static String TYPE_VARCHAR="VARCHAR";
	public static String TYPE_INTEGER="INT";
	public static String TYPE_FLOAT="FLOAT";
	public static String TYPE_DATE="DATE";


	private final static int TIMEOUT_REPORT=3000;
	
	public static String TYPE_EOGENERICRECORD="EO";
	public static String TYPE_NSDICTIONARY="DICO";
	
	public ToolsExcel(ApplicationCocktail app){
		setDefaults("ExportExcel","donnees");
	}

	private static void message(String pMessage){
		System.out.println(pMessage);
	}

	ApplicationCocktail getApp() {
		return app;
	}

	
	private void setDefaults(String setDataSource,String setTableName){
		setDriver("sun.jdbc.odbc.JdbcOdbcDriver");
		setUrl("jdbc:odbc");


//		ODBC data source named "TestExcel" defined from Control Panel
//		FRED setDataSource("TestExcel");
//		FRED setTableName("GREAT_CARS");
		setDataSource(setDataSource);
		setTableName(setTableName);
	}

	public void openDatabase(String file){
//		FRED String file = "c://Excel.xls";

//		use this without setting an ODBC Data Source
		String lConnectStr = "jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ=" + file + ";DriverID=22;READONLY=false";

//		use this in the case of an existing DSN
//		String lConnectStr = getUrl()+":"+getDataSource();
		try {
			Class.forName(getDriver());
			gConnection = DriverManager.getConnection(lConnectStr);
		}catch (Exception e) {
			message("Error connecting to DB: " + e.getMessage());
		}
	}

	private void closeDatabase(){
		try{
			getConnection().close();
		}catch (Exception e){
			message("closeDatabase(): "+e.getMessage());
		}
	}

	private void createTable(NSArray colName,NSArray colType){
		message("createTable() begin");

		Statement lStat = null;

		try {
			lStat = getConnection().createStatement();
			String createTable= "CREATE TABLE "+getTableName()+" (";
			String createTableCol="";
			createTableCol = colName.objectAtIndex(0)+" "+colType.objectAtIndex(0);
			for (int i = 1; i < colName.count(); i++) {
				createTableCol = createTableCol+" , "+colName.objectAtIndex(i)+" "+colType.objectAtIndex(i);
			}
			createTable =createTable+createTableCol+")";
			lStat.execute(createTable);
		}catch (Exception e){
			message("createTable(): "+e.getMessage());
		}

		message("createTable() end");
	}

	private void doInsertEO(NSArray colType,NSArray lesDatas,NSArray lesEOs){
		message("doInsert() begin");
		String stringStatement ="";
		Statement lStat = null;
		try {
			for (int i = 0; i < lesEOs.count(); i++) {
				EOGenericRecord tmp = (EOGenericRecord)lesEOs.objectAtIndex(i);
				lStat = getConnection().createStatement();
				stringStatement ="INSERT INTO " +getTableName()+" VALUES (";
				String 	stringStatementDatas = "'"+
				(tmp.storedValueForKey((String)lesDatas.objectAtIndex(0))== null?" '":tmp.storedValueForKey((String)lesDatas.objectAtIndex(0)).toString().replace('\'',' '))+"'";
				for (int j = 1; j < lesDatas.count(); j++) {
					stringStatementDatas = stringStatementDatas+" , "+"'"+
					(tmp.storedValueForKey((String)lesDatas.objectAtIndex(j)) == null ? " ":tmp.storedValueForKey((String)lesDatas.objectAtIndex(j)).toString().replace('\'',' ') )+
					"'";
				}
				lStat.executeUpdate(stringStatement+stringStatementDatas+")");
				lStat.close();
				
			}

		}catch(Exception e){
			message("doInsert(): "+stringStatement+" "+e.getMessage());
		}

		message("doInsert() end");
	}
	
	private void doInsertDico(NSArray colName,NSArray lesDatas){
		message("doInsert() begin");
		String stringStatement ="";
		String 	stringStatementDatas ="";
		Statement lStat = null;
		try {
			for (int i = 1; i < lesDatas.count(); i++) {
				NSArray tmp = (NSArray)lesDatas.objectAtIndex(i);
				lStat = getConnection().createStatement();
				stringStatement ="INSERT INTO " +getTableName()+" VALUES (";
				stringStatementDatas = "'"+
				(tmp.objectAtIndex(0)== null?" '":tmp.objectAtIndex(0).toString().replace('\'',' '))+"'";
				for (int j = 1; j < tmp.count(); j++) {
					stringStatementDatas = stringStatementDatas+" , "+"'"+
					(tmp.objectAtIndex(j) == null ? " ":tmp.objectAtIndex(j).toString().replace('\'',' ') )+
					"'";
				}
				lStat.executeUpdate(stringStatement+stringStatementDatas+")");
				lStat.close();
				
			}

		}catch(Exception e){
			message("doInsert(): "+stringStatement+stringStatementDatas+" "+e.getMessage());
		}

		message("doInsert() end");
	}

	private void dropTable(){
		message("dropTable() begin");

		Statement lStat = null;
		try {
			lStat = getConnection().createStatement();
			lStat.execute("DROP TABLE " + getTableName());
			lStat.close();
		}catch(Exception e){
			message("dropTable(): "+e.getMessage());
		}

		message("dropTable() end");
	}

	private void doQuery(){
		message("doQuery() begin");

		try {
			Statement lStat = getConnection().createStatement();
			ResultSet lRes = lStat.executeQuery("SELECT * FROM "+getTableName());
			ResultSetMetaData lMeta = lRes.getMetaData();

//			print out the column headers separated by commas
			for (int i = 1; i <= lMeta.getColumnCount(); ++i){
				if (i > 1){
					System.out.print(", ");
				}
				String lValue = lMeta.getColumnName(i);
				System.out.print(lValue);
			}
			System.out.println("");

//			print out the data separated by commas
			while (lRes.next()){
				for (int i=1; i<=lMeta.getColumnCount(); ++i){
					if (i > 1){
						System.out.print(", ");
					}

					String lValue = lRes.getString(i);
					System.out.print(lValue);
				}
				System.out.println("");
			}

			lRes.close();
			lStat.close();
		}catch (Exception e){
			message("doQuery(): "+e.getMessage());
		}
		message("doQuery() end");
	}

	private String milisecondeToString(int nbsec){
	    if(nbsec==0)
	        return "0 seconde";
	    nbsec = nbsec / 1000;
	    int minutes = nbsec / 60;
	    if(minutes==0)
	        return nbsec+" secondes";
	    nbsec = nbsec%60;
	    if(nbsec!=0)
	        return minutes+" minute(s) et "+nbsec+" seconde(s)";
	    return minutes+" minute(s)";
	}

	
	public void exportTo(String pathFile,NSArray colName,NSArray colType,NSArray lesProperties,NSArray lesObjects,String type){
		openDatabase(pathFile);
		dropTable();
		createTable( colName, colType);
		if (type.equals(ToolsExcel.TYPE_EOGENERICRECORD))
		doInsertEO(colType,lesProperties, lesObjects);
		
		if (type.equals(ToolsExcel.TYPE_NSDICTIONARY))
		doInsertDico(lesProperties,lesObjects);
		
		closeDatabase();
	}
	
	public String exportWithJxlsSansOuvrir(String xlstemplate, String sql,String fileName) throws Exception{
		app.ceerTransactionLog();
		app.afficherUnLogDansTransactionLog("DEBUT....",10);
		NSData tmp = null;
		String filePath = null;

		try	{
			app.afficherUnLogDansTransactionLog("Cr\u00E9ation  de l'extraction sur le serveur",40);
			((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(),
					"session.remoteCallDelagate",
					"clientSideRequestExportWithJxlsLong",
					new Class[] {String.class,String.class},
					new Object[] {xlstemplate,sql},true);
		} catch (Exception e) {
			app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
			app.finirTransactionLog();
			throw new Exception(e.getMessage());
		} 
        
        NSDictionary returnDico = null;
        int timeout = 0;
        int temps = TIMEOUT_REPORT*app.nbTimeOut();
        String messageOld = "\n Information : Delai maximal de "+milisecondeToString(temps)+".";
        String messageNew = messageOld;
        app.afficherUnLogDansTransactionLog(messageOld,45);
        while (true){
            Thread.sleep(TIMEOUT_REPORT);
            timeout = timeout+TIMEOUT_REPORT;
            temps = temps-TIMEOUT_REPORT;
            messageOld = messageNew;
            messageNew = "\n Information : Expiration dans "+milisecondeToString(temps)+".";
       
			try	{
				returnDico = (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestReturnValuesForLastJxlsParametresLong", null, null, false);
                System.out.println("returnDico = " + returnDico);
			} catch (Exception e) {
				app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
				app.finirTransactionLog();
				throw new Exception(e.getMessage());
			} 
			
			if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE)
			        || returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)
			        || returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)) {
			    break;
			}
			
            app.rafficherUnLogDansTransactionLog(messageOld,messageNew,45);
            if (timeout == TIMEOUT_REPORT*(app.nbTimeOut()+1))
                break; 
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)){
			app.afficherUnLogDansTransactionLog("D\u00E9lai expir\u00E9 !!"+"\n Impossible de finir l'extraction Excel.",100);
			app.finirTransactionLog();
			return filePath;
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)){
			app.afficherUnLogDansTransactionLog("EDITION VIDE !!",100);
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
			return filePath;
		}
		
		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE)) {
			tmp = (NSData) returnDico.valueForKey("DATAS");
		}
		    
		if (tmp != null) {
			try {
				app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'extraction sur votre poste", 80);
			    filePath = getApp().getToolsCocktailSystem().saveToUserHomeDir(tmp, fileName, 1);
			} catch (Exception e){
				e.printStackTrace();

				app.afficherUnLogDansTransactionLog("probl\u00E8me !!" + e.getMessage(),0);
				app.finirTransactionLog();
			}
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
			app.fermerTransactionLog();
		} else {
			app.afficherUnLogDansTransactionLog("EXTRACTION VIDE !!",100);
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
		}
		
		return filePath;
	}
	
    public ExcelExportResult exportWithJxlsAndFullResult(String xlstemplate, String sql,String fileName) throws Exception {
        app.ceerTransactionLog();
        app.afficherUnLogDansTransactionLog("DEBUT....",10);
        NSData tmp = null;
        String filePath = null;
        String fileServerPath = null;

        try {
            app.afficherUnLogDansTransactionLog("Cr\u00E9ation  de l'extraction sur le serveur",40);
            ((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(),
                    "session.remoteCallDelagate",
                    "clientSideRequestExportWithJxlsLong",
                    new Class[] {String.class,String.class},
                    new Object[] {xlstemplate,sql},true);
        } catch (Exception e) {
            app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
            app.finirTransactionLog();
            throw new Exception(e.getMessage());
        } 
        
        NSDictionary returnDico = null;
        int timeout = 0;
        int temps = TIMEOUT_REPORT*app.nbTimeOut();
        String messageOld = "\n Information : Delai maximal de "+milisecondeToString(temps)+".";
        String messageNew = messageOld;
        app.afficherUnLogDansTransactionLog(messageOld,45);
        while (true){
            Thread.sleep(TIMEOUT_REPORT);
            timeout = timeout+TIMEOUT_REPORT;
            temps = temps-TIMEOUT_REPORT;
            messageOld = messageNew;
            messageNew = "\n Information : Expiration dans "+milisecondeToString(temps)+".";
       
            try {
                returnDico = (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestReturnValuesForLastJxlsParametresLong", null, null, false);
                System.out.println("returnDico = " + returnDico);
            } catch (Exception e) {
                app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
                app.finirTransactionLog();
                throw new Exception(e.getMessage());
            } 
            
            if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE)
                    || returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)
                    || returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)) {
                break;
            }
            
            app.rafficherUnLogDansTransactionLog(messageOld,messageNew,45);
            if (timeout == TIMEOUT_REPORT*(app.nbTimeOut()+1))
                break; 
        }

        if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)){
            app.afficherUnLogDansTransactionLog("D\u00E9lai expir\u00E9 !!"+"\n Impossible de finir l'extraction Excel.",100);
            app.finirTransactionLog();
            return new ExcelExportResult(null, null);
        }

        if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)){
            app.afficherUnLogDansTransactionLog("EDITION VIDE !!",100);
            app.afficherUnLogDansTransactionLog("FIN...",100);
            app.finirTransactionLog();
            return new ExcelExportResult(null, null);
        }
        
        if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE)) {
            tmp = (NSData) returnDico.valueForKey("DATAS");
            fileServerPath = (String) returnDico.valueForKey("REMOTEFILEPATH");
        }
            
        if (tmp != null) {
            try {
                app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'extraction sur votre poste", 80);
                filePath = getApp().getToolsCocktailSystem().saveToUserHomeDir(tmp, fileName, 1);
            } catch (Exception e){
                e.printStackTrace();

                app.afficherUnLogDansTransactionLog("probl\u00E8me !!" + e.getMessage(),0);
                app.finirTransactionLog();
            }
            app.afficherUnLogDansTransactionLog("FIN...",100);
            app.finirTransactionLog();
            app.fermerTransactionLog();
        } else {
            app.afficherUnLogDansTransactionLog("EXTRACTION VIDE !!",100);
            app.afficherUnLogDansTransactionLog("FIN...",100);
            app.finirTransactionLog();
        }
        
        return new ExcelExportResult(filePath, fileServerPath);
    }
	
	public void exportWithJxls(String xlstemplate, String sql,String fileName) throws Exception{
		app.ceerTransactionLog();
		app.afficherUnLogDansTransactionLog("DEBUT....",10);
		NSData tmp=null;


		try	{
			app.afficherUnLogDansTransactionLog("Cr\u00E9ation  de l'extraction sur le serveur",40);
			((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(),
					"session.remoteCallDelagate",
					"clientSideRequestExportWithJxlsLong",
					new Class[] {String.class,String.class},
					new Object[] {xlstemplate,sql},true);
		}
		catch (Exception e) {
			app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
			app.finirTransactionLog();
			throw new Exception(e.getMessage());
		} 
        
        NSDictionary returnDico = null;
        int timeout = 0;
        int temps = TIMEOUT_REPORT*app.nbTimeOut();
        String messageOld = "\n Information : Delai maximal de "+milisecondeToString(temps)+".";
        String messageNew = messageOld;
        app.afficherUnLogDansTransactionLog(messageOld,45);
        while (true){
            Thread.sleep(TIMEOUT_REPORT);
            timeout = timeout+TIMEOUT_REPORT;
            temps = temps-TIMEOUT_REPORT;
            messageOld = messageNew;
            messageNew = "\n Information : Expiration dans "+milisecondeToString(temps)+".";
       
//		NSDictionary returnDico;
//		while (true){
//			Thread.sleep(TIMEOUT_REPORT);
			try	{
				returnDico = (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestReturnValuesForLastJxlsParametresLong", null, null, false);
                System.out.println("returnDico = "+returnDico);
			}
			catch (Exception e) {
				app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
				app.finirTransactionLog();
				throw new Exception(e.getMessage());
			} 
			if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE) ||
					returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)||
					returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)
			){break;}
            app.rafficherUnLogDansTransactionLog(messageOld,messageNew,45);
            if (timeout == TIMEOUT_REPORT*(app.nbTimeOut()+1))
                break; 
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)){
			app.afficherUnLogDansTransactionLog("D\u00E9lai expir\u00E9 !!"+"\n Impossible de finir l'extraction Excel.",100);
			app.afficherUnLogDansTransactionLog(" "+returnDico.valueForKey("EXCEPTION"),100);
			app.finirTransactionLog();
			return;
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)){
			app.afficherUnLogDansTransactionLog("EDITION VIDE !!",100);
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.afficherUnLogDansTransactionLog(" "+returnDico.valueForKey("EXCEPTION"),100);
			app.finirTransactionLog();
			return;
		}
		
		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE))
			tmp=(NSData)returnDico.valueForKey("DATAS");

//		if (tmp != null  && ){
		if ( tmp != null) {
			try {
				app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'extraction sur votre poste",80);
				String filePath = getApp().getToolsCocktailSystem().saveToUserHomeDir(tmp, fileName,1);

				getApp().getToolsCocktailSystem().openFile(filePath);          
			}
			catch(Exception e){	
				e.printStackTrace();

				app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
				app.finirTransactionLog();
			}
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
			app.fermerTransactionLog();
		}else
		{
			app.afficherUnLogDansTransactionLog("EXTRACTION VIDE !!",100);
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
		}

	}
	
	public void setTableName (String pValue){
		gTableName = pValue;
	}

	public String getTableName(){
		return(gTableName);
	}

	public void setSql(String pValue){
		gSql = pValue;
	}

	public String getSql(){
		return(gSql);
	}

	public Connection getConnection(){
		return(gConnection);
	}


	public String getDataSource(){
		return(gDataSource);
	}

	public void setDataSource(String pValue){
		gDataSource = pValue;
	}

	public void setDriver(String pValue){
		gDriver = pValue;
	}

	public void setUrl(String pValue){
		gUrl = pValue;
	}

	public String getDriver (){
		return (gDriver);
	}

	public String getUrl (){
		return (gUrl);
	}

	private Connection gConnection = null;
	private String gDataSource = null;
	private String gTableName = null;
	private String gSql = null;
	private String gDriver = null;
	private String gUrl = null;
	
	public class ExcelExportResult {
	    private String localFilePath;
	    private String remoteFilePath;
	    
	    public ExcelExportResult(String localFilePath, String remoteFilePath) {
	        this.localFilePath = localFilePath;
	        this.remoteFilePath = remoteFilePath;
	    }
	    
	    public String localFilePath() {
	        return localFilePath;
	    }
	    
	    public String remoteFilePath() {
	        return remoteFilePath;
	    }
	}
	
}


