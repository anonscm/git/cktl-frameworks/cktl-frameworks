/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.tools;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class Process {

    boolean withLogs;
    
    public Process() {
        super();
        setWithLogs(false);
    }
    
    public Process(boolean logs) {
        super();
        setWithLogs(logs);
    }



    public void trace(final String prefix, final NSArray a) {
        for (int i = 0; i < a.count(); i++) {
            trace(prefix +"  "+i+"-->" , a.objectAtIndex(i));
        }
    }

    
//    public void trace(String m) {
//        if (withLogs())
//            System.out.println(m);
//    }
//    
//    public void trace(final String prefix, String m) {
//        if (withLogs())
//            System.out.println(m);
//    }
//    
    public void trace(final Object obj) { 
        trace("", obj);
    }
    
    public void trace(final String prefix, final Object obj) {
        if (withLogs()) {
            if (obj==null) {
                System.out.println(prefix+"null");
            }
            else {
                if (obj instanceof NSArray) {
                    trace (prefix, (NSArray)obj);
                }
                else if (obj instanceof EOEnterpriseObject) {
                    trace (prefix, (EOEnterpriseObject)obj);
                }
                else {
                    System.out.println (prefix + obj.toString());
                }
            }
            
        }
    }    
    


    
    public void trace(final String prefix, final EOEnterpriseObject object) {
        if (object!=null) {
            final Iterator iter = object.attributeKeys().vector().iterator();
            while (iter.hasNext()) {
                String obj = (String) iter.next();
                trace(prefix+"  "+ obj  +"-->", object.valueForKey(obj) );
            }

            Iterator iter2 = object.toOneRelationshipKeys().vector().iterator();
            while (iter2.hasNext()) {
                String obj = (String) iter2.next();
                trace(prefix+"  "+ obj  +"-->" + object.valueForKey(obj) );
            }
            Iterator iter3 = object.toManyRelationshipKeys().vector().iterator();
            while (iter3.hasNext()) {
                String obj = (String) iter3.next();
                if (prefix != null && prefix.length()>250) {
                    trace(prefix+"  "+ obj  +"-->" + object.valueForKey(obj) );
                }
                else {
                    trace(prefix+"  "+ obj  +"-->" , object.valueForKey(obj) );
                }
                
            }
        }
    }    
    
    /**
     * @param withLogs  The withLogs to set.
     * @uml.property  name="withLogs"
     */
    public void setWithLogs(boolean b) {
        this.withLogs = b;
    }

    public boolean withLogs() {
        return withLogs;
    }
    
    
    private final GregorianCalendar getToday() {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        return gc;
    }

    /**
     * @return
     */
    public final NSTimestamp getDateJour() {
        return new NSTimestamp(getToday().getTime());
    }
    
    
    
    public BigDecimal computeSumForKeyBigDecimal(NSArray eo, String keyBigDecimal) {

        BigDecimal total = new BigDecimal((double)0);
        int i = 0;
        while (i < eo.count()) {
            total = total.add((BigDecimal) ((EOEnterpriseObject) eo.objectAtIndex(i)).valueForKey(keyBigDecimal));
            i = i + 1;
        }
        return total;

    }
    
    public static boolean inferieur(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean inferieurOuEgal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean superieur(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

    public static boolean superieurOuEgal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

    public static boolean egal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean different(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

}
