/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 19 sept. 07
 * author mparadot 
 */
package org.cocktail.application.client.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Zip {

    public static void zipFolder(String pathDir, String pathDirZip, String removePath) throws IOException {
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(pathDirZip));
        zipDir(pathDir, zos, removePath);
        zos.close();
    }

    // here is the code for the method
    private static void zipDir(String dir2zip, ZipOutputStream zos,String rmPath) {
        try {
            // create a new File object based on the directory we
            // have to zip File
            File zipDir = new File(dir2zip);
            // get a listing of the directory content
            String[] dirList = zipDir.list();
            byte[] readBuffer = new byte[2156];
            int bytesIn = 0;
            // loop through dirList, and zip the files
            for (int i = 0; i < dirList.length; i++) {
                File f = new File(zipDir, dirList[i]);
                if (f.isDirectory()) {
                    // if the File object is a directory, call this
                    // function again to add its content recursively
                    String filePath = f.getPath();
                    zipDir(filePath, zos,rmPath);
                    // loop again
                    continue;
                }
                
                // if we reached here, the File object f was not
                // a directory
                // create a FileInputStream on top of f
                FileInputStream fis = new FileInputStream(f);
                // create a new zip entry
                ZipEntry anEntry = new ZipEntry(f.getPath().substring(rmPath.length()));
//                anEntry.setMethod(ZipEntry.STORED);
                // place the zip entry in the ZipOutputStream object
                zos.putNextEntry(anEntry);
                // now write the content of the file to the ZipOutputStream
                while ((bytesIn = fis.read(readBuffer)) != -1) {
                    zos.write(readBuffer, 0, bytesIn);
                }
                // close the Stream
                fis.close();
            }
        } catch (Exception e) {
            // handle exception
        }

    }

    // Nom du fichier
    private String fichier;

    // Liste des fichiers contenus dans le zip
    private Vector liste = new Vector();

    // separateur de fichier, selon le sytème d'exploitaion utilise. Ainsi le code est valide partou
    private static final char separ = File.separatorChar;

    /**
     * Le rang où se trouve le nom
     */
    public static final int placeNom = 0;

    /**
     * Le rang où se trouve la taille
     */
    public static final int placeTaille = 1;

    /**
     * Le rang où se trouve la taille de compression
     */
    public static final int placeCompression = 2;

    /**
     * Le rang où se trouve la date
     */
    public static final int placeDate = 3;

    /**
     * Le rang où se trouve le commentaire
     */
    public static final int placeCommentaire = 4;

    /**
     * Crée le zip
     * 
     * @param fichier
     *            Le nom du fichier zip, avec son chemin complet
     */
    public Zip(String fichier) throws Exception {
        this.fichier = fichier;
        File f = new File(fichier);
        // Si le fichier existe, on récupère le nom et les attributs des fichiers compresses dans le zip
        if (f.exists()) {
            // C'est un fichier zip
            ZipFile fic = new ZipFile(fichier);
            // Récupération des fichiers
            Enumeration enume = fic.entries();
            while (enume.hasMoreElements())
                liste.addElement(enume.nextElement());
        }
    }

    /**
     * Change le nom du fichier zip, avec un chemin complet
     */
    public void setNom(String nom) {
        fichier = nom;
    }

    /**
     * Donne un vecteur titre. Peut servir pour l'ent^te d'un JTable
     */
    public static Vector getTitre() {
        Vector titre = new Vector();
        titre.addElement("Nom");
        titre.addElement("Taille");
        titre.addElement("Compression");
        titre.addElement("Date");
        titre.addElement("Commentaire");
        return titre;
    }

    /**
     * Vecteur de vecteur contenant les donneees <BR>
     * Pour récupérer la ligne l, colonnne c, faire : <BR>
     * Vector donnes=zip.contenu(); <BR>
     * ... <BR>
     * Vector lig=(Vector)donnees.get(l); <BR>
     * String valeur=(String)lig.get(c);
     */
    public Vector contenu() {
        int nb = liste.size();
        Vector donnees = new Vector();
        for (int i = 0; i < nb; i++) {
            Vector ligne = new Vector();
            ZipEntry entre = (ZipEntry) liste.elementAt(i);
            ligne.addElement(entre.getName());
            ligne.addElement("" + entre.getSize());
            long j = entre.getCompressedSize();
            if (j >= 0)
                ligne.addElement("" + j);
            else
                ligne.addElement("");
            Date date = new Date(entre.getTime());
            Calendar calendrier = Calendar.getInstance();
            calendrier.setTime(date);
            ligne.addElement(calendrier.get(Calendar.HOUR_OF_DAY) + "h" + calendrier.get(Calendar.MINUTE) + "m" + calendrier.get(Calendar.SECOND) + "s le " + calendrier.get(Calendar.DAY_OF_MONTH)
                    + "/" + calendrier.get(Calendar.MONTH) + "/" + calendrier.get(Calendar.YEAR));
            ligne.addElement(entre.getComment());
            donnees.addElement(ligne);
        }
        return donnees;
    }

    /**
     * Donne une chaine representant la caractéristique du fichier <BR>
     * 
     * @param place
     *            Rang de la caratéristique. <BR>
     * @param numero
     *            numero du fichier contenu dans le zip <BR>
     *            Exemple : <BR>
     *            Pour avoir la taille de compression du fichier numero 5 faire : <BR>
     *            String taille=zip.get(ZIP.placeCompression,5);
     */
    public String get(int place, int numero) {
        // Récupération des caractéristiques du fichier
        ZipEntry entre = (ZipEntry) liste.elementAt(numero);
        // Selon la caractéristique demandée, retourner la valeur corespondante
        switch (place) {
        // Le commentaire
        case placeCommentaire:
            return entre.getComment();
            // La taille de compression
        case placeCompression:
            long j = entre.getCompressedSize();
            if (j >= 0)
                return "" + j;
            return "";
            // La date du fichier
        case placeDate:
            // Transformation de la date exprimée en milliseconde, en date plus courante
            Date date = new Date(entre.getTime());
            // Mise en place d'un calendrier à partir de la date
            Calendar calendrier = Calendar.getInstance();
            calendrier.setTime(date);
            // Renvoie des donnees
            return calendrier.get(Calendar.HOUR_OF_DAY) + "h" + calendrier.get(Calendar.MINUTE) + "m" + calendrier.get(Calendar.SECOND) + "s le " + calendrier.get(Calendar.DAY_OF_MONTH) + "/"
                    + calendrier.get(Calendar.MONTH) + "/" + calendrier.get(Calendar.YEAR);
            // Le nom du fichier
        case placeNom:
            return entre.getName();
            // La taille réelle du fichier
        case placeTaille:
            return "" + entre.getSize();
        }
        // Renvoie la chaîne vide si la caractéristique n'existe pas
        return "";
    }

    /**
     * Ajoute un fichier au fichier zip
     */
    public void ajouteFichier(File fichier) {
        ajouteFichier(fichier, null);
    }

    /**
     * Ajoute un fichier au fichier zip avec un commentaire
     */
    public void ajouteFichier(File fichier, String commentaire) {
        // Transforme le fichier en entrée du zip
        ZipEntry entre = new ZipEntry(fichier.getAbsolutePath().substring(3).replace(separ, '/'));
        entre.setSize(fichier.length());
        entre.setTime(fichier.lastModified());
        if (commentaire != null)
            entre.setComment(commentaire);
        // Ajoute l'éntrée
        liste.addElement(entre);
    }

    /**
     * Retire le fichier du zip
     */
    public void retire(File fichier) {
        // Cacul le nom de l'entrée, en majuscule, corespondante au fichier
        String s = fichier.getAbsolutePath().substring(3).replace(separ, '/').toUpperCase();
        // Chercher le rang de l'éntrée corespondante
        int rang = -1;
        int nb = liste.size();
        for (int i = 0; (i < nb) && (rang < 0); i++) {
            ZipEntry entre = (ZipEntry) liste.elementAt(i);
            if (entre.getName().toUpperCase().equals(s))
                rang = i;
        }
        // Si l'éntrée existe, la retirée
        if (rang >= 0)
            liste.removeElementAt(rang);
    }

    /**
     * @return Le nom du fichier
     */
    public int getNombreFichier() {
        return liste.size();
    }

    /**
     * Retire l'élélement numero numero
     */
    public void retire(int numero) {
        liste.removeElementAt(numero);
    }

    /**
     * Zip les fichiers dans le nom du zip correspondant. Met à jour les changements.
     */
    public void ziper() throws Exception {
        // Si le fichier n'existe pas, le créer
        File f = new File(fichier);
        if (!f.exists())
            f.createNewFile();
        // Ouverture du fichier zip en écriture
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(fichier));
        // Pour chaque entrée du zip
        int nb = liste.size();
        for (int i = 0; i < nb; i++) {
            // Récupérer l'éntrée courante
            ZipEntry entre = (ZipEntry) liste.elementAt(i);
            // Récupérer le nom de l'entrée
            String nom = entre.getName();
            // Ajouter l'entrée au fichier physique zip
            zos.putNextEntry(entre);
            // Ouvrir l'entrée en lecture
            FileInputStream fis = new FileInputStream("C:\\" + nom.replace('/', separ));
            // Ziper l'entrée dans le fichier zip
            byte[] tab = new byte[4096];
            int lu = -1;
            do {
                lu = fis.read(tab);
                if (lu > 0)
                    zos.write(tab, 0, lu);
            } while (lu > 0);
            // Fermer l'entrée
            fis.close();
        }
        // Force à finir le zipage, si jamais il reste des bits non traites
        zos.flush();
        // Ferme le fichier zip
        zos.close();
    }

    /**
     * Dézipe le fichier dans le dossier en paramètre. Respect du nom des dossiers et sous-dossiers
     */
    public void deziper(File dossier) throws Exception {
        // Ouverture du fichier zip en lecture
        ZipInputStream zis = new ZipInputStream(new FileInputStream(fichier));
        // Récupére les entrées effectivement zipées dans le zip
        ZipEntry ze = zis.getNextEntry();
        // Tant qu'il y a des entrées
        while (ze != null) {
            // Crée le dossier contenant le fichier une fois déziper, si il n'existe pas
            File f = new File(dossier.getAbsolutePath() + separ + ze.toString().replace('/', separ));
            f.getParentFile().mkdirs();
            // Ouvre l'entrée le fichier à déziper en écriture, le crée s'il n'existe pas
            FileOutputStream fos = new FileOutputStream(f);
            // Dézipe le fichier
            int lu = -1;
            byte[] tampon = new byte[4096];
            do {
                lu = zis.read(tampon);
                if (lu > 0)
                    fos.write(tampon, 0, lu);
            } while (lu > 0);
            // Finir proprement
            fos.flush();
            // Fermer le fichier à déziper
            fos.close();
            // Passer au suivant
            ze = zis.getNextEntry();
        }
        // Fermer le fichier zip
        zis.close();
    }

    /**
     * Modifie le commentaire du fichier numero numero
     */
    public void setCommentaire(int numero, String commentaire) {
        ZipEntry entre = (ZipEntry) liste.elementAt(numero);
        entre.setComment(commentaire);
    }
}