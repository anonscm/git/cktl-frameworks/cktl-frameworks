/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 25 juil. 07
 * author mparadot 
 */
package org.cocktail.application.client.tools;

import com.webobjects.foundation.NSDictionary;

public class EmailCocktail extends NSDictionary {

    /**
     * Permet la creation d'un NSDictionary pour l'envoi d'email avec {@link ToolsCocktailSystem#envoyerMail(NSDictionary) }
     * @param expediteur l'email de l'expediteur
     * @param destinataire l'email du destinataire
     * @param sujet le sujet de l'email
     * @param texte le message de l'email
     */
    public EmailCocktail(String expediteur,String destinataire,String sujet,String texte) {
        super(new Object[]{expediteur,destinataire,sujet,texte},new Object[]{"expediteur","destinataire","sujet","texte"});
    }
    
    public EmailCocktail(String expediteur,String destinataire,String cc, String sujet,String texte){
        super(new Object[]{expediteur,destinataire,cc,sujet,texte},new Object[]{"expediteur","destinataire","cc","sujet","texte"});
    }
    
    /**
     * @return l'email de l'expediteur
     */
    public String expediteur(){
        if(valueForKey("expediteur")==null)
            return "noreplay@cocktail.org";
        return (String)valueForKey("expediteur");
    }
    
    /**
     * @return l'email du destinataire
     */
    public String destinataire(){
        return (String)valueForKey("destinataire");
    }
    
    /**
     * @return le sujet de l'email
     */
    public String sujet(){
        if(valueForKey("sujet")==null)
            return "Sans sujet";
        return (String)valueForKey("sujet");
    }
    
    /**
     * @return le message de l'email
     */
    public String texte(){
        if(valueForKey("texte")==null)
            return "";
        return (String)valueForKey("texte");
    }
    
    /**
     * @return les cc de l'email
     */
    public String cc(){
        return (String)valueForKey("cc");
    }
    
    /**
     * Met à jour l'email de l'expediteur
     * @param expediteur l'email de l'expediteur
     */
    public void setExpediteur(String expediteur){
        takeValueForKey(expediteur,"expediteur");
    }
    
    /**
     * Met à jour l'email du destinataire
     * @param destinataire l'email du destinataire
     */
    public void setDestinataire(String destinataire){
        takeValueForKey(destinataire,"destinataire");
    }
    
    /**
     * Met à jour le sujet de l'email
     * @param sujet le sujet de l'email
     */
    public void setSujet(String sujet){
        takeValueForKey(sujet,"sujet");
    }
    
    /**
     * Met à jour le message de l'email
     * @param texte le message de l'email
     */
    public void setTexte(String texte){
        takeValueForKey(texte,"texte");
    }
    
    /**
     * Met à jour les cc de l'email
     * @param cc les cc de l'email
     */
    public void setCc(String cc){
        takeValueForKey(cc,"cc");
    }
    
    /**
     * Ajout un email au cc
     * @param email email a ajouter
     */
    public void addCc(String email){
        if(cc()==null)
            setCc(email);
        else
            setCc(cc()+","+email);
    }
    
    /**
     * Supprimer un email parmi les cc
     * @param email email a supprimer
     */
    public void removeCc(String email){
        if(cc()==null)
            return;
        if(cc().indexOf(email)==0)
        {
            setCc(cc().replaceAll(email, ""));
            if(cc().indexOf(",")==0)
                setCc(cc().substring(1));
            else
                setCc(cc().replaceAll(",,", ","));
        }
        else
        {
            setCc(cc().replaceAll(","+email, ""));
        }
    }
    

}
