/*
 * Copyright COCKTAIL, 1995-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.tools;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZAbstractPanel;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.client.swing.ZEOTableModelColumnHashMap;
import org.cocktail.application.client.swing.ZListUtil;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;



/**
 * @author rodolphe.prin@univ-lr.fr
 */
public class ConnectedUsersPanel extends ZAbstractPanel  implements ZEOTableListener {
    protected ZEOTable myEOTable;
    protected ZEOTableModel myTableModel;
    private ArrayList myArrayList;
    protected TableSorter myTableSorter;
    protected Vector myCols;
    private IConnectedUsersPanelListener myListener;

    private EODisplayGroup myDisplayGroup = new EODisplayGroup();

    /**
     * @param editingContext
     */
    public ConnectedUsersPanel(IConnectedUsersPanelListener listener) {
        super();
        myListener = listener;
        myArrayList = new ArrayList();
        
        myCols = new Vector(2);

        ZEOTableModelColumn col0 = new ZEOTableModelColumnHashMap(myArrayList, "login", "Login", 60);
        col0.setAlignment(SwingConstants.LEFT);
        ZEOTableModelColumn col1 = new ZEOTableModelColumnHashMap(myArrayList, "ip", "Machine", 60);
        col0.setAlignment(SwingConstants.LEFT);


        myCols.add(col0);
        myCols.add(col1);

        myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
        myTableSorter = new TableSorter(myTableModel);
        myEOTable = new ZEOTable(myTableSorter);
        myEOTable.addListener(this);
        myTableSorter.setTableHeader(myEOTable.getTableHeader());
        myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);    
        setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        setLayout(new BorderLayout());
        add(new JScrollPane(myEOTable), BorderLayout.CENTER);           
        
        
    }



    /**
     * @see fr.univlr.karukera.client.CommonPanel#updateData()
     */
    public void updateData() throws Exception {
        NSArray datas = myListener.getData();
        myArrayList.clear();

        for (int i = 0; i < datas.count(); i++) {
            NSDictionary d = (NSDictionary) datas.objectAtIndex(i);
            myArrayList.add(ZListUtil.convertNSDictionaryToHashMap(d));
        }
        myDisplayGroup.setObjectArray(datas);
        myEOTable.updateData();
        myTableModel.setInnerRowCount(myArrayList.size());
        myTableModel.fireTableDataChanged();

    }

    /**
     * @see org.cocktail.kiwi.client.swing.karukera.client.zutil.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        myListener.onDbClick();
    }

    /**
     * @see org.cocktail.kiwi.client.swing.karukera.client.zutil.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        myListener.onSelectionChanged();
    }

    public interface IConnectedUsersPanelListener {

        /**
         * @return
         */
        public NSArray getData();

        /**
         *
         */
        public void onSelectionChanged();

        /**
         *
         */
        public void onDbClick();

    }
    public ZEOTableModel getMyTableModel() {
        return myTableModel;
    }




}
