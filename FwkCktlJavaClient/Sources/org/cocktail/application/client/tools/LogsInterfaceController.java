/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.application.client.tools;

import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.nibfinder.NibFinder;
import org.cocktail.application.client.swingfinder.SwingFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

public class LogsInterfaceController extends EOInterfaceControllerCocktail {
    public EOTextArea txtErroutClient;
    public EOTextArea txtStdoutClient;
    public EOTextArea txtLogServer;
    public JTextField tfDestinataire;
    public JPanel erroutClient;
    public JPanel stdoutClient;
    public JSplitPane viewClient;
//	private static String VIDE="Email destinataire";

    public LogsInterfaceController() {
        super();
    }
    
    public void componentDidBecomeVisible() {
        super.componentDidBecomeVisible();
        initView();
    }
    
    private void initView(){
        refresh();
        viewClient.setOrientation(JSplitPane.VERTICAL_SPLIT);
        viewClient.setBorder(BorderFactory.createEmptyBorder());
        viewClient.setOneTouchExpandable(true);
        viewClient.setDividerSize(6);
        viewClient.setResizeWeight(0.5);
        viewClient.setTopComponent(stdoutClient);
        viewClient.setBottomComponent(erroutClient);
        tfDestinataire.setText(cocktailApp.getApplicationParametre("ADMIN_MAIL"));
    }
    
    public void refresh(){
        txtErroutClient.setText(cocktailApp.clientErrLog());
        txtStdoutClient.setText(cocktailApp.clientOutLog());
        NSData logserveur = ToolsCocktailLogs.logsServeur();
        if(logserveur !=null)
            txtLogServer.setText(new String(logserveur.bytes()));
        else
            txtLogServer.setText("Impossible de lire le fichier de log du serveur !!!\nSi vous etes en developement, vous devez ajouter au fichier de properties : WOOutputPath=\\le_chemin\\le_fichier.txt");
    }

    public LogsInterfaceController(EOEditingContext substitutionEditingContext) {
        super();
        setEditingContext(substitutionEditingContext);
    }

    public NSArray getResultat() {
        
        return null;
    }

    public void nibFinderAnnuler(NibFinder arg0) {
        
        
    }

    public void nibFinderTerminer(NibFinder arg0) {
        
        
    }

    public void swingFinderAnnuler(SwingFinder arg0) {
        
        
    }

    public void swingFinderTerminer(SwingFinder arg0) {
        
        
    }
    
    public void sendLogs(){
//        String dest = tfDestinataire.getText();
//        if(dest==null || "".equals(dest.trim()) || VIDE.equals(dest))
//            dest = cocktailApp.getApplicationParametre("ADMIN_MAIL");
    	String dest = cocktailApp.getApplicationParametre("ADMIN_MAIL");
    	if(tfDestinataire.getText()!=null && !"".equals(tfDestinataire.getText().trim()))
    		dest = tfDestinataire.getText().trim();
        if(dest==null||"".equals(dest))
        {
            fenetreDeDialogueInformation("Attention : vous devez saisir un email pour le destinataire des logs de l'application.");
            return;
        }
        StringBuffer mess = new StringBuffer();
        mess.append("\n\n");
        mess.append("*****************LOG SERVER*****************\n");
        mess.append("\n");
        mess.append(txtLogServer.textArea().getText());
        mess.append("\n\n\n");
        mess.append("*****************LOG CLIENT*****************\n");
        mess.append("Sortie standard:\n");
        mess.append(txtStdoutClient.textArea().getText());
        mess.append("\n\n");
        mess.append("Sortie d'erreur:\n");
        mess.append(txtErroutClient.textArea().getText());
        mess.append("\n");
        mess.append("\n\n\n*****************INFO UTILS*****************\n");
        mess.append(cocktailApp.getToolsCocktailLogs().messageAPropos());
        String exp = cocktailApp.getApplicationParametre("ADMIN_MAIL");
        if(exp==null || "".equals(exp))
            exp = "noreply@cocktail.org";
        String titre = cocktailApp.applicationName();
        EmailCocktail email = new EmailCocktail(exp,dest,"[Log] "+titre,mess.toString());
        String name = cocktailApp.getToolsCocktailSystem().getTemporaryFileName("screenshot",".jpg");
        String path = System.getProperty("java.io.tmpdir");
        if (!path.endsWith(java.io.File.separator)) {
            path = path + java.io.File.separator;
        }
        path+=name;
        this.component().getTopLevelAncestor().setVisible(false);
        this.component().getTopLevelAncestor().setVisible(true);
        try {
            if(cocktailApp.getToolsCocktailSystem().envoyerMail(email,new NSData(new File(path)),name))
                fenetreDeDialogueInformation("Les logs ont \u00E9t\u00E9 envoy\u00E9 avec succ\u00E9s !!!");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}
