/*
 * Copyright COCKTAIL, 1995-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.application.client.tools;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import org.cocktail.application.client.ServerProxyCocktail;
import org.cocktail.application.client.swing.CommonCtrl;
import org.cocktail.application.client.swing.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;


/**
 * 
 * @author rodolphe.prin@univ-lr.fr
 */
public class ConnectedUsersCtrl extends CommonCtrl {
    private static final Dimension WINDOW_DIMENSION = new Dimension(420,380);
    private static final Dimension BUTTONS_DIMENSION = new Dimension(120,20);
    private static final String TITLE = "Liste des utilisateurs connectés";
    private ZAbstractPanel myPanel;
    private MyPanel myPanel2;
    private Action actionClose = new ActionClose();
    
    /**
     * @param editingContext
     */
    public ConnectedUsersCtrl(EOEditingContext editingContext) {
        super(editingContext);
        myPanel2 = new MyPanel();
    }
 

	private final JPanel createCommentaireUI() {
		//Le titre
		JLabel titre = new JLabel("Liste des utilisateurs connectés");
		titre.setFont(myPanel.getFont().deriveFont((float)12).deriveFont(Font.BOLD));
		
		//Le blabla
		JTextArea tmpTextArea = new JTextArea("Les utilisateurs listés ci-dessous sont actuellement connectés à l'application.");
		//tmpTextArea.setMargin(new Insets(3,3,3,3));
		Color bgcol = Color.decode("#FFFFFF");
		tmpTextArea.setFont(myPanel.getFont().deriveFont((float)11));
		tmpTextArea.setEditable(false);
		tmpTextArea.setAlignmentX(SwingConstants.LEFT);
//		tmpTextArea.setBackground(bgcol);
//		tmpJPanel.add(Box.createRigidArea(new Dimension(1,40)));
		
		JPanel tmpJPanel = new JPanel();
		tmpJPanel.setLayout(new BorderLayout());
		tmpJPanel.setBorder(BorderFactory.createMatteBorder(5,5,5,5, bgcol));
		tmpJPanel.setBackground(bgcol);
		
		tmpJPanel.setPreferredSize(new Dimension( 1   ,40));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
//		tmpJPanel.setMaximumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(Box.createRigidArea(new Dimension(1,40)), BorderLayout.LINE_START);		
		tmpJPanel.add(titre, BorderLayout.PAGE_START);
		tmpJPanel.add(tmpTextArea, BorderLayout.CENTER);
		return tmpJPanel;
	}    
    
	private JPanel buildDefaultButtonsPanel() {
//		JButton prevButton;
//		JButton nextButton;
		JButton closeButton;
//		JButton actionButton1;
		
		Box tmpBox = Box.createHorizontalBox();
		tmpBox.add(Box.createRigidArea(new Dimension(2,40)));
		tmpBox.add(Box.createHorizontalGlue());
	

		//Bouton Fermer
		if (actionClose()!=null) {
			closeButton = new JButton(actionClose());
			closeButton.setPreferredSize(BUTTONS_DIMENSION);
			closeButton.setMinimumSize(BUTTONS_DIMENSION);
			closeButton.setHorizontalAlignment(SwingConstants.LEFT);
			closeButton.setHorizontalTextPosition(SwingConstants.RIGHT);
			tmpBox.add(closeButton );
			tmpBox.add( Box.createRigidArea(new Dimension(25,30)) );
		}
			

		

		JPanel tmpJPanel = new JPanel(new BorderLayout());
		tmpJPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		tmpJPanel.setPreferredSize(new Dimension(1, 40));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(new JPanel(), BorderLayout.CENTER);	
		tmpJPanel.add(tmpBox, BorderLayout.LINE_END);
		return tmpJPanel;		
		
	}
	
	

    /**
     * @return
     */
    private Action actionClose() {
        return actionClose;
    }



    private final class ConnectedUsersPanelListener implements ConnectedUsersPanel.IConnectedUsersPanelListener {

        public NSArray getData() {
            return ServerProxyCocktail.clientSideRequestGetConnectedUsers(getEditingContext());
        }

        public void onSelectionChanged() {
            return;
            
        }

        /**
         * @see org.cocktail.kiwi.client.admin.karukera.client.administration.ConnectedUsersPanel.IConnectedUsersPanelListener#onDbClick()
         */
        public void onDbClick() {
            
        }
        
    }
    
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CLOSE);
        }
	    
        public void actionPerformed(ActionEvent e) {
            getMyDialog().onCancelClick();
        }
	    
	}

    
    
    public String title() {
        return TITLE;
    }

    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }



    public ZAbstractPanel mainPanel() {
        return myPanel2;
    }
    
    private final class MyPanel extends ZAbstractPanel {
        public MyPanel() {
            super(new BorderLayout());
            myPanel = new ConnectedUsersPanel(new ConnectedUsersPanelListener());
            add(myPanel, BorderLayout.CENTER);
            add(createCommentaireUI(), BorderLayout.NORTH);
            add(buildDefaultButtonsPanel(), BorderLayout.SOUTH);            
        }
        
        public void updateData() throws Exception {
            myPanel.updateData();
        }
        
    }
    
    
}
