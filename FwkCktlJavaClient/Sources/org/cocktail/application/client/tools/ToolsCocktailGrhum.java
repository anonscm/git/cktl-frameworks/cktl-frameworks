/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.tools;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOTypeApplication;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.finder.FinderGrhum;

import com.webobjects.foundation.NSArray;

public class ToolsCocktailGrhum {

	private ApplicationCocktail app =null;
	
	public ToolsCocktailGrhum(ApplicationCocktail app) {
		super();
		this.setApp(app);
	}

	void setApp(ApplicationCocktail app) {
		this.app = app;
	}
	
	// Tools des exercices
	public  NSArray  lesExercices(){
		return FinderGrhum.findLesExercices(app);
	}

	// Tools des Utilisateurs
	public  EOUtilisateur  getUtilisateurConnecte(Number persid){
		return FinderGrhum.findUtilisateurWithPersId(app,persid);
	}
	
	
	// Tools des applications
	public  EOTypeApplication  getTypeApplication(String tyapLibelle){
		return FinderGrhum.findTypeApplication(app,tyapLibelle);
	}
	
	
	// Tools des fonctions
	public  NSArray  getLesFonctionsUtilisateur(EOUtilisateur utilisateur,EOTypeApplication typeApplication){
	return FinderGrhum.findUtilisateurFonctions(app, utilisateur, typeApplication);
	}
	
	
	// Tools des fonctions
	public  NSArray  getLesUtilisateurOrgan(EOUtilisateur utilisateur){
	return FinderGrhum.findUtilisateurOrgans(app, utilisateur);
	}
	

}
