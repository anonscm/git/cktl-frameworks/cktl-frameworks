package org.cocktail.application.client.tools;
/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 19 sept 07
 * author mparadot 
 */

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.webobjects.foundation.NSArray;

public class CocktailFileChooser {

    public static final int PDF_FORMAT_ID = 1;

    public static final int XLS_FORMAT_ID = 2;

    public static final int CSV_FORMAT_ID = 3;

    public static final int HTML_FORMAT_ID = 4;

    public static final int RTF_FORMAT_ID = 5;

    public static final int JPG_FORMAT_ID = 6;

    public static final int PNG_FORMAT_ID = 7;

    public static final int GIF_FORMAT_ID = 8;

    public static final int DOC_FORMAT_ID = 9;

    public static final int TXT_FORMAT_ID = 10;
    
    public static final int WOA_FORMAT_ID = 11;
    
    public static final int FWK_FORMAT_ID = 12;
    
    public static final int MODE_FILE_ONLY = JFileChooser.FILES_ONLY;
    
    public static final int MODE_DIRECTORIES_ONLY = JFileChooser.DIRECTORIES_ONLY;
    
    public static final int MODE_FILES_AND_DIRECTORIES = JFileChooser.FILES_AND_DIRECTORIES;

    protected Component component;

    private String _title;

    /**
     * Constructeur.
     * @param component Compposant graphique pere.
     */
    public CocktailFileChooser(final Component component, String title) {
        this.component = component;
        _title = title;
    }

    /**
     * Affichage d'un explorateur de fichier pour choisir un fichier unique (pas de repertoire).
     * @param fileFilter Filtre des noms de fichiers acceptables.
     * @param currentFile Fichier ou repertoire de depart de l'exploration.
     * @param write true pour indiquer qu'il s'agit d'un choix de fichier qui va etre cree
     * (donc confirmation d'ecrasement), false sinon.
     */
    protected File chooseFile(NSArray arrayFilter, final File currentFile, final boolean write) {
//            JFileChooser fileChooser = new JFileChooser();
//            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
//            fileChooser.setApproveButtonText("Choisir");
//            fileChooser.setMultiSelectionEnabled(false);
//            fileChooser.setAcceptAllFileFilterUsed(true);
//            for (int i = 0; i < arrayFilter.count(); i++) {
//                FileFilter array_element = (FileFilter) arrayFilter.objectAtIndex(i);
//                fileChooser.addChoosableFileFilter(array_element);
//            }
//            fileChooser.setDialogTitle(_title);
//            if (currentFile != null)
//                if (currentFile.isDirectory())
//                    fileChooser.setCurrentDirectory(currentFile);
//                else
//                    fileChooser.setSelectedFile(currentFile);
//            if (fileChooser.showSaveDialog(this.component) == JFileChooser.APPROVE_OPTION) {
//                File file = fileChooser.getSelectedFile();
//                if (write && file.exists()) {
//                    if (JOptionPane.showConfirmDialog(this.component, "Le fichier existe d\u00E9ja, souhaitez-vous vraiment le remplacer ?", "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
//                        return null;
//                    }
//                }
//                System.out.println("chooseFile() :  fichier de sortie = " + file.getAbsolutePath());
//                return file;
//            } else
//                return null;
        return chooseFile(arrayFilter, currentFile, write, JFileChooser.FILES_ONLY);
    }
    
    /**
     * Affichage d'un explorateur de fichier pour choisir un fichier unique (pas de repertoire).
     * @param fileFilter Filtre des noms de fichiers acceptables.
     * @param currentFile Fichier ou repertoire de depart de l'exploration.
     * @param write true pour indiquer qu'il s'agit d'un choix de fichier qui va etre cree
     * @param mode le mode de selection des fichiers ( JFileChooser.FILES_ONLY , JFileChooser.DIRECTORIES_ONLY, JFileChooser.FILES_AND_DIRECTORIES)
     * (donc confirmation d'ecrasement), false sinon.
     */
    protected File chooseFile(NSArray arrayFilter, final File currentFile, final boolean write,final int mode) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(mode);
        fileChooser.setApproveButtonText("Choisir");
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setAcceptAllFileFilterUsed(true);
        for (int i = 0; i < arrayFilter.count(); i++) {
            FileFilter array_element = (FileFilter) arrayFilter.objectAtIndex(i);
            fileChooser.addChoosableFileFilter(array_element);
        }
        fileChooser.setDialogTitle(_title);
        if (currentFile != null)
            if (currentFile.isDirectory())
                fileChooser.setCurrentDirectory(currentFile);
            else
                fileChooser.setSelectedFile(currentFile);
        if (fileChooser.showSaveDialog(this.component) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (write && file.exists()) {
                if (JOptionPane.showConfirmDialog(this.component, "Le fichier existe d\u00E9ja, souhaitez-vous vraiment le remplacer ?", "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                    return null;
                }
            }
            System.out.println("chooseFile() :  fichier de sortie = " + file.getAbsolutePath());
            return file;
        } else
            return null;
    }
    
    /**
     * Affichage d'un explorateur de fichier pour choisir un fichier unique (pas de repertoire).
     * @param fileFilter Filtre des noms de fichiers acceptables.
     * @param currentFile Fichier ou repertoire de depart de l'exploration.
     * @param write true pour indiquer qu'il s'agit d'un choix de fichier qui va etre cree
     * (donc confirmation d'ecrasement), false sinon.
     */
    protected NSArray chooseMultiFile(NSArray arrayFilter, final File currentFile, final boolean write) {
//            JFileChooser fileChooser = new JFileChooser();
//            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
//            fileChooser.setMultiSelectionEnabled(true);
//            fileChooser.setAcceptAllFileFilterUsed(true);
//            FileFilter defaultFilter = fileChooser.getFileFilter();
//            for (int i = 0; i < arrayFilter.count(); i++) {
//                FileFilter array_element = (FileFilter) arrayFilter.objectAtIndex(i);
//                fileChooser.addChoosableFileFilter(array_element);
//            }
//            fileChooser.setFileFilter(defaultFilter);
//            fileChooser.setDialogTitle(_title);
//            fileChooser.setApproveButtonText("Choisir");
//            fileChooser.setEnabled(true);
//            fileChooser.setDragEnabled(true);
//            if (currentFile != null)
//                if (currentFile.isDirectory())
//                    fileChooser.setCurrentDirectory(currentFile);
//                else
//                    fileChooser.setSelectedFile(currentFile);
//            if (fileChooser.showOpenDialog(this.component) == JFileChooser.APPROVE_OPTION) {
//                return new NSArray(fileChooser.getSelectedFiles());
//            } else
//                return null;
        return chooseMultiFile(arrayFilter, currentFile, write, JFileChooser.FILES_ONLY);
    }
    
    /**
     * Affichage d'un explorateur de fichier pour choisir un fichier unique (pas de repertoire).
     * @param fileFilter Filtre des noms de fichiers acceptables.
     * @param currentFile Fichier ou repertoire de depart de l'exploration.
     * @param write true pour indiquer qu'il s'agit d'un choix de fichier qui va etre cree
     * @param mode le mode de selection des fichiers ( JFileChooser.FILES_ONLY , JFileChooser.DIRECTORIES_ONLY, JFileChooser.FILES_AND_DIRECTORIES)
     * (donc confirmation d'ecrasement), false sinon.
     */
    public NSArray chooseMultiFile(NSArray arrayFilter, final File currentFile, final boolean write, final int mode) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(mode);
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setAcceptAllFileFilterUsed(true);
        FileFilter defaultFilter = fileChooser.getFileFilter();
        for (int i = 0; i < arrayFilter.count(); i++) {
            FileFilter array_element = (FileFilter) arrayFilter.objectAtIndex(i);
            fileChooser.addChoosableFileFilter(array_element);
        }
        fileChooser.setFileFilter(defaultFilter);
        fileChooser.setDialogTitle(_title);
        fileChooser.setApproveButtonText("Choisir");
        fileChooser.setEnabled(true);
        fileChooser.setDragEnabled(true);
        if (currentFile != null)
            if (currentFile.isDirectory())
                fileChooser.setCurrentDirectory(currentFile);
            else
                fileChooser.setSelectedFile(currentFile);
        if (fileChooser.showOpenDialog(this.component) == JFileChooser.APPROVE_OPTION) {
            return new NSArray(fileChooser.getSelectedFiles());
        } else
            return null;
    }

    /**
     * Affichage d'un explorateur de fichier pour choisir un fichier unique (pas de repertoire) a creer
     * (demande de confirmation en cas de selection d'un fichier existant).
     * @param fileFilter Filtre des noms de fichiers acceptables.
     * @param currentFile Fichier ou repertoire de depart de l'exploration.
     * @return Le fichier choisi.
     */
    public File chooseFileToCreate(final FileFilter fileFilter, final File currentFile) {
        return chooseFile(new NSArray(fileFilter), currentFile, true);
    }

    /**
     * Affichage d'un explorateur de fichier pour choisir un fichier unique (pas de repertoire) a creer
     * (demande de confirmation en cas de selection d'un fichier existant).
     * @param fileFilter Filtre des noms de fichiers acceptables.
     * @param currentFile Fichier ou repertoire de depart de l'exploration.
     * @return Le fichier choisi.
     */
    public File chooseFile(final FileFilter fileFilter, final File currentFile) {
        return chooseFile(new NSArray(fileFilter), currentFile, false);
    }

    /**
     * Affichage d'un explorateur de fichier pour choisir un fichier unique (pas de repertoire) a creer
     * (demande de confirmation en cas de selection d'un fichier existant).
     * @param fileFilter Filtre des noms de fichiers acceptables.
     * @param currentFile Fichier ou repertoire de depart de l'exploration.
     * @return Le fichier choisi.
     */
    public File chooseFile(NSArray arrayFilter, File currentFile) {
        return chooseFile(arrayFilter, currentFile, false);
    }
    
    /**
     * Affichage d'un explorateur de fichier pour choisir un fichier unique (pas de repertoire) a creer
     * (demande de confirmation en cas de selection d'un fichier existant).
     * @param fileFilter Filtre des noms de fichiers acceptables.
     * @param currentFile Fichier ou repertoire de depart de l'exploration.
     * @return NSArray des fichiers choisi.
     */
    public NSArray chooseMultiFile(NSArray arrayFilter, File currentFile) {
        return chooseMultiFile(arrayFilter, currentFile, false);
    }

    public static EditionFileFilter getFileFilterFor(int filter) {
        return new EditionFileFilter(filter);
    }

    /**
     * Filtre de nom de fichier pour les editions (.pdf, etc.)
     *
     * @author Bertrand GAUTHIER, Consortium Cocktail, 2006
     */
    private static class EditionFileFilter extends FileFilter {
        protected int idFormat;

        protected String filtreExtension;

        protected String description;

        /**
         * Constructeur.
         * @param idFormat Identifiant du format de sortie (exemple: ExporterFactory.PDF_FORMAT_ID)
         */
        public EditionFileFilter(final int idFormat) {
            this.idFormat = idFormat;
            switch (this.idFormat) {
            case PDF_FORMAT_ID:
                filtreExtension = "pdf";
                description = "Document PDF";
                break;
            case XLS_FORMAT_ID:
                filtreExtension = "xls";
                description = "Document Excel";
                break;
            case CSV_FORMAT_ID:
                filtreExtension = "csv";
                description = "Document format CSV";
                break;
            case TXT_FORMAT_ID:
                filtreExtension = "txt";
                description = "Document Texte Brute";
                break;
            case HTML_FORMAT_ID:
                filtreExtension = "html";
                description = "Document HTML";
                break;
            case RTF_FORMAT_ID:
                filtreExtension = "rtf";
                description = "Document texte RTF";
                break;
            case DOC_FORMAT_ID:
                filtreExtension = "doc";
                description = "Document Word";
                break;
            case GIF_FORMAT_ID:
                filtreExtension = "gif";
                description = "Image gif";
                break;
            case JPG_FORMAT_ID:
                filtreExtension = "jpg";
                description = "Image jpeg";
                break;
            case PNG_FORMAT_ID:
                filtreExtension = "png";
                description = "Image png";
                break;
            case WOA_FORMAT_ID:
                filtreExtension = "woa";
                description = "Application WebObject";
                break;
            case FWK_FORMAT_ID:
                filtreExtension = "framework";
                description = "Framework";
                break;
            default:;
            }
        }

        /**
         * Retourne l'extension courante sans le "." devant (ex: "rtf").
         * @return L'extension
         */
        public String getExtension() {
            return this.filtreExtension;
        }

        /**
         *
         */
        public boolean accept(File file) {
            String extension = getExtension(file);
            if (extension != null) {
                return extension.equals(this.filtreExtension);
            }
            return false;
        }

        /**
         *
         */
        public String getDescription() {
            return this.description;
        }

        /**
         * Get the extension of a file.
         */
        public String getExtension(File f) {
            String ext = null;
            String s = f.getName();
            int i = s.lastIndexOf('.');

            if (i > 0 && i < s.length() - 1) {
                ext = s.substring(i + 1).toLowerCase();
            }
            return ext;
        }
    }

}
