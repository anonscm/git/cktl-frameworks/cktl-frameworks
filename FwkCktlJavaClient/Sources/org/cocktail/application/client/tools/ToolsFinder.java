/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.tools;

import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSMutableArray;

public class ToolsFinder {
	private ApplicationCocktail app;
	
	

	public ToolsFinder(ApplicationCocktail app) {
		super();
		this.app = app;
	}


	public  NSMutableArray filterForRelationKey(NSMutableArray aFiltrer,NSMutableArray leFiltres,String key)
	{
		NSMutableArray result = new NSMutableArray();
		for (int i = 0; i < aFiltrer.count(); i++) {
			for (int j = 0; j < leFiltres.count(); j++) {
				if (((EOGenericRecord)aFiltrer.objectAtIndex(i)).storedValueForKey(key).equals(((EOGenericRecord)leFiltres.objectAtIndex(j)).storedValueForKey(key)))
					result.addObject(aFiltrer.objectAtIndex(i));
			}
		}
		return result;
	}
	
	
	public  NSMutableArray filterEOForKey(NSMutableArray aFiltrer,NSMutableArray lesFiltres,String key)
	{
		NSMutableArray result = new NSMutableArray();
		for (int i = 0; i < aFiltrer.count(); i++) {
			for (int j = 0; j < lesFiltres.count(); j++) {
				if (((EOGenericRecord)aFiltrer.objectAtIndex(i)).storedValueForKey(key).equals(((EOGenericRecord)lesFiltres.objectAtIndex(j))))
					result.addObject(aFiltrer.objectAtIndex(i));
			}
		}
		return result;
	}
	
	
	public  NSMutableArray filterEOsByEOsRelation(NSMutableArray aFiltrer,NSMutableArray lesFiltres,String relation)
	{
		NSMutableArray result = new NSMutableArray();
		for (int i = 0; i < aFiltrer.count(); i++) {
			for (int j = 0; j < lesFiltres.count(); j++) {
				if (((EOGenericRecord)aFiltrer.objectAtIndex(i)).equals(((EOGenericRecord)lesFiltres.objectAtIndex(j)).storedValueForKey(relation)))
					result.addObject(aFiltrer.objectAtIndex(i));
			}
		}
		return result;
	}
	
	
	
	public  NSMutableArray filterEOsRelationByEOsRelation(NSMutableArray aFiltrer,NSMutableArray lesFiltres,String relation)
	{
		NSMutableArray result = new NSMutableArray();
		for (int i = 0; i < aFiltrer.count(); i++) {
			for (int j = 0; j < lesFiltres.count(); j++) {
				if ((((EOGenericRecord)aFiltrer.objectAtIndex(i)).storedValueForKey(relation)).equals(((EOGenericRecord)lesFiltres.objectAtIndex(j)).storedValueForKey(relation)))
					result.addObject(aFiltrer.objectAtIndex(i));
			}
		}
		return result;
	}
	
	
	public  NSMutableArray filterEOsByArrayWithKey(NSMutableArray aFiltrer,NSMutableArray lesFiltres,String key)
	{
		NSMutableArray result = new NSMutableArray();
		for (int i = 0; i < aFiltrer.count(); i++) {
			for (int j = 0; j < lesFiltres.count(); j++) {
				if ((((EOGenericRecord)aFiltrer.objectAtIndex(i)).storedValueForKey(key)).equals(lesFiltres.objectAtIndex(j)))
					result.addObject(aFiltrer.objectAtIndex(i));
			}
		}
		return result;
	}
	
	
	public  NSMutableArray filterByKeyForObjet(NSMutableArray aFiltrer,EOGenericRecord leFiltre,String key) throws Exception
	{
		
//		System.out.println("aFiltrer "+aFiltrer.count());
//		System.out.println("leFiltre "+leFiltre);
//		System.out.println("key "+key);
			
		NSMutableArray result = new NSMutableArray();
		
		for (int i = 0; i < aFiltrer.count(); i++) {
				if (((EOGenericRecord)aFiltrer.objectAtIndex(i)).storedValueForKey(key).equals(leFiltre)){
					result.addObject(aFiltrer.objectAtIndex(i));
				}
		}
//		System.out.println("result "+result.count());
		return result;
	}
	
	public  NSMutableArray filterByKeyForValue(NSMutableArray aFiltrer,Object value,String key)
	{
		NSMutableArray result = new NSMutableArray();
		for (int i = 0; i < aFiltrer.count(); i++) {
				if (((EOGenericRecord)aFiltrer.objectAtIndex(i)).storedValueForKey(key).equals(value))
					result.addObject(aFiltrer.objectAtIndex(i));

		}
		return result;
	}
}
