/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.tools;

import java.lang.reflect.Method;

import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.foundation.NSMutableArray;

public class ToolsCocktailObjectMessage {


	private ApplicationCocktail app =null;
	private NSMutableArray mesObserveurs =null;

	public ToolsCocktailObjectMessage(ApplicationCocktail app) {
		super();
		this.app = app;
		mesObserveurs=new NSMutableArray();
	}


	public void sendMessageToObserver(Object sender,String message)
	{
		for (int i = 0; i < getMesObserveurs().count(); i++) {
			if (((Observer)getMesObserveurs().objectAtIndex(i)).getMessage().equals(message))
				callMethode(
						((Observer)getMesObserveurs().objectAtIndex(i)).getObj(),
						((Observer)getMesObserveurs().objectAtIndex(i)).getMessage()
				);
		}
		System.out.println("sendMessageToObserver.."+message);
	}

	public void addObserverForMessage(Object obj,String message){
		getMesObserveurs().addObject(new Observer(obj,message));
	}

	
	public void removeObserverForMethode(Object obj,String message){
		for (int i = 0; i < getMesObserveurs().count(); i++) {
			if (((Observer)getMesObserveurs().objectAtIndex(i)).getMessage().equals(message) &&
					((Observer)getMesObserveurs().objectAtIndex(i)).getObj() == obj)
				getMesObserveurs().removeObjectAtIndex(i);	
		}
	}

	private NSMutableArray getMesObserveurs() {
		return mesObserveurs;
	}


/*
	public void callMethode(Object delegate,String methode,Object sender,String param)
	{
		// try du call
		try {
			Class c;
			c = delegate.getClass();
			// Recupereation de la methode getXxxxx :
			Class parametres [] = {sender.getClass()};
			Method call = c.getMethod(methode,parametres);
			call.invoke(delegate, new Object[] { sender });

		} catch (Exception ex) {
			//System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
		}

	}
*/

	private void callMethode(Object delegate,String methode)
	{
		// try du call
		try {
			Class c;
			c = delegate.getClass();
			// Recupereation de la methode getXxxxx :
			Method call = c.getMethod(methode);
			// appel de la methode
			call.invoke(delegate);

		} catch (Exception ex) {
			//	System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
		}

	}

	class Observer {

		String message;
		Object obj;

		public Observer(Object obj,String message) {
			super();
			this.message = message;
			this.obj = obj;

		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public Object getObj() {
			return obj;
		}

		public void setObj(Object obj) {
			this.obj = obj;
		}

	}
}
