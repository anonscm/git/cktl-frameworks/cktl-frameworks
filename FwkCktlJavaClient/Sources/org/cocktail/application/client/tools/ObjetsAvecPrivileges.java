/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 3 sept. 07
 * author mparadot 
 */
package org.cocktail.application.client.tools;

import com.webobjects.foundation.NSKeyValueCoding;

public class ObjetsAvecPrivileges implements NSKeyValueCoding{
    Object obj;
    String key;
    public ObjetsAvecPrivileges(Object obj, String key) {
        super();
        this.obj = obj;
        this.key = key;
    }
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public Object getObj() {
        return obj;
    }
    public void setObj(Object obj) {
        this.obj = obj;
    }
    public void takeValueForKey(Object arg0, String arg1) {
        if("key".equals(arg1))
            setKey((String)arg0);
        if("obj".equals(arg1))
            setObj((String)arg0);
    }
    public Object valueForKey(String arg0) {
        if("key".equals(arg0))
            return getKey();
        if("obj".equals(arg0))
            return getObj();
        return null;
    }
    
    public String toStringPerso() {
        StringBuffer str = new StringBuffer();
        str.append("{(key = ").append(getKey());
        str.append("),(");
        str.append("(obj = ").append(getObj());
        str.append(") class = ").append(getObj().getClass().getName()).append("}");
        return str.toString();
    }
    
    public String toString() {
        return toStringPerso();
    }
}
