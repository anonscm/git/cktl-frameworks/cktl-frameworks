/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.tools;

import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

public class ToolsCocktailReports {

	private ApplicationCocktail app;

	private final static int TIMEOUT_REPORT=3000;

	public final static String REPORTSTATUS_ATTENTE = "ATTENTE";
	public final static String REPORTSTATUS_ENCOURS = "ENCOURS";
	public final static String REPORTSTATUS_TERMINE = "TERMINE";
	public final static String REPORTSTATUS_PROBLEME = "PROBLEME";
	public final static String REPORTSTATUS_VIDE = "VIDE";

	public void imprimerReportSql(String reportName,String select,NSDictionary parameters,String fileName) throws Exception 
	{

		app.ceerTransactionLog();
		app.afficherUnLogDansTransactionLog("DEBUT....",10);

		NSData tmp=null;

		try	{
			app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'\u00E9dition sur le serveur",40);
			tmp=(NSData)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(),
					"session.remoteCallDelagate",
					"clientSideRequestImprimerReportSql",
					new Class[] {String.class,String.class,NSDictionary.class},
					new Object[] {reportName,select,parameters},true);
		}
		catch (Exception e) {
			app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
			app.finirTransactionLog();
			throw new Exception(e.getMessage());
		} 

		try {
			app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'\u00E9dition sur voter poste",80);
			String filePath = getApp().getToolsCocktailSystem().saveToUserHomeDir(tmp, fileName,0);
			getApp().getToolsCocktailSystem().openFile(filePath);          
		}
		catch(Exception e){
			System.out.println("erreur " + e.getMessage());			   
			e.printStackTrace();
			app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
			app.finirTransactionLog();
		}

		app.afficherUnLogDansTransactionLog("FIN...",100);
		app.finirTransactionLog();
		app.fermerTransactionLog();
	}

	public void imprimerReportParametres(String reportName,NSDictionary parameters,String fileName) throws Exception 
	{
		app.ceerTransactionLog();
		app.afficherUnLogDansTransactionLog("DEBUT....",10);
		NSData tmp=null;


		try	{
			app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'\u00E9dition sur le serveur",40);
			((EODistributedObjectStore) getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(),
					"session.remoteCallDelagate",
					"clientSideRequestImprimerReportParametresLong",
					new Class[] {String.class,NSDictionary.class},
					new Object[] {reportName,parameters},true);
		}
		catch (Exception e) {
			app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
			app.finirTransactionLog();
			throw new Exception(e.getMessage());
		} 



		NSDictionary returnDico = null;
		int timeout = 0;
		int temps = TIMEOUT_REPORT*app.nbTimeOut();
		String messageOld = "\n Information : Delai maximal de "+milisecondeToString(temps)+".";
		String messageNew = messageOld;
		app.afficherUnLogDansTransactionLog(messageOld,45);
		boolean boucle = true;
		while (boucle){
			Thread.sleep(TIMEOUT_REPORT);
			timeout = timeout+TIMEOUT_REPORT;
			temps = temps-TIMEOUT_REPORT;
			messageOld = messageNew;
			messageNew = "\n Information : Expiration dans "+milisecondeToString(temps)+".";

			try	{
				returnDico = (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestReturnValuesForLastImprimerReportParametresLong", null, null, false);
			}
			catch (Exception e) {
				app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
				app.finirTransactionLog();
				boucle= false;
				throw new Exception(e.getMessage());
			
			} 
			
			if (returnDico.objectForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE))
				boucle= false;
			
			if (returnDico.objectForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME))
				boucle= false;
			
			if (returnDico.objectForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE))
				boucle= false;

			app.rafficherUnLogDansTransactionLog(messageOld,messageNew,45);
			if (timeout == TIMEOUT_REPORT*(app.nbTimeOut()+1))
				boucle= false;     
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)){
			app.afficherUnLogDansTransactionLog("D\u00E9lai expir\u00E9 !!!"+"\n Impossible de finir l'impression.",100);
			app.afficherUnLogDansTransactionLog(" " +returnDico.valueForKey("EXCEPTION"),100);
			app.finirTransactionLog();
			return;
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)){
			app.afficherUnLogDansTransactionLog("EDITION VIDE !!",100);
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.afficherUnLogDansTransactionLog(" "+returnDico.valueForKey("EXCEPTION"),100);
			app.finirTransactionLog();
			return;
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE))
			tmp=(NSData)returnDico.valueForKey("DATAS");

		if ( tmp != null) {
			try {
				app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'\u00E9dition sur voter poste",80);
				String filePath = getApp().getToolsCocktailSystem().saveToUserHomeDir(tmp, fileName,0);

				getApp().getToolsCocktailSystem().openFile(filePath);          
			}
			catch(Exception e){	
				e.printStackTrace();

				app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
				app.finirTransactionLog();
			}
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
			app.fermerTransactionLog();
		}else
		{
			app.afficherUnLogDansTransactionLog("EDITION VIDE !!",100);
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
		}

	}

	private String milisecondeToString(int nbsec){
		if(nbsec==0)
			return "0 seconde";
		nbsec = nbsec / 1000;
		int minutes = nbsec / 60;
		if(minutes==0)
			return nbsec+" secondes";
		nbsec = nbsec%60;
		if(nbsec!=0)
			return minutes+" minute(s) et "+nbsec+" seconde(s)";
		return minutes+" minute(s)";
	}

	public String imprimerReportParametresSansOuvrir(String reportName,NSDictionary parameters,String fileName) throws Exception 
	{
		app.ceerTransactionLog();
		app.afficherUnLogDansTransactionLog("DEBUT....",10);
		NSData tmp=null;
		String filePath=null;

		try	{
			app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'\u00E9dition sur le serveur",40);
			((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(),
					"session.remoteCallDelagate",
					"clientSideRequestImprimerReportParametresLong",
					new Class[] {String.class,NSDictionary.class},
					new Object[] {reportName,parameters},true);
		}
		catch (Exception e) {
			app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
			app.finirTransactionLog();
			throw new Exception(e.getMessage());
		} 

		NSDictionary returnDico = null;
		int timeout = 0;
		int temps = TIMEOUT_REPORT*app.nbTimeOut();
		String messageOld = "\n Information : Delai maximal de "+milisecondeToString(temps)+".";
		String messageNew = messageOld;
		app.afficherUnLogDansTransactionLog(messageOld,45);
		while (true){
			Thread.sleep(TIMEOUT_REPORT);
			timeout = timeout+TIMEOUT_REPORT;
			temps = temps-TIMEOUT_REPORT;
			messageOld = messageNew;
			messageNew = "\n Information : Expiration dans "+milisecondeToString(temps)+".";

			try	{
				returnDico = (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestReturnValuesForLastImprimerReportParametresLong", null, null, false);
				System.out.println("returnDico = "+returnDico);
			}
			catch (Exception e) {
				app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
				app.finirTransactionLog();
				throw new Exception(e.getMessage());
			} 
			if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE) ||
					returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)||
					returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)
			){break;}
			app.rafficherUnLogDansTransactionLog(messageOld,messageNew,45);
			if (timeout == TIMEOUT_REPORT*(app.nbTimeOut()+1))
				break;     
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_PROBLEME)){
			app.afficherUnLogDansTransactionLog("D\u00E9lai expir\u00E9 !!!"+"\n Impossible de finir l'impression.",100);
			app.finirTransactionLog();
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_VIDE)){
			app.afficherUnLogDansTransactionLog("EDITION VIDE !!",100);
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
		}

		if (returnDico.valueForKey("STATUS").equals(ToolsCocktailReports.REPORTSTATUS_TERMINE))
			tmp=(NSData)returnDico.valueForKey("DATAS");

		if (tmp != null) {
			try {
				app.afficherUnLogDansTransactionLog("Cr\u00E9ation de l'\u00E9dition sur voter poste",80);
				filePath = getApp().getToolsCocktailSystem().saveToUserHomeDir(tmp, fileName,0);

				//getApp().getToolsCocktailSystem().openFile(filePath);          
			}
			catch(Exception e){	
				e.printStackTrace();

				app.afficherUnLogDansTransactionLog("probl\u00E8me !!"+e.getMessage(),0);
				app.finirTransactionLog();
			}
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
			app.fermerTransactionLog();
		}else
		{
			app.afficherUnLogDansTransactionLog("EDITION VIDE !!",100);
			app.afficherUnLogDansTransactionLog("FIN...",100);
			app.finirTransactionLog();
			filePath=null;
		}

		return filePath;
	}


	public ToolsCocktailReports(ApplicationCocktail app) {
		super();
		this.setApp(app);
	}



	void setApp(ApplicationCocktail app) {
		this.app = app;
	}



	ApplicationCocktail getApp() {
		return app;
	}




//	class TimerReport extends TimerTask{
//	private  String reportName = null;
//	private  NSDictionary parameters = null;
//	private NSData leReport = null;

//	public void run() {
//	
//	imprimerReportParametresTimer();
//	}

//	public  TimerReport ( String reportName,NSDictionary parameters){
//	setReportName(reportName);
//	setParameters(parameters);
//	}

//	public NSDictionary getParameters() {
//	return parameters;
//	}

//	public void setParameters(NSDictionary parameters) {
//	this.parameters = parameters;
//	}

//	public String getReportName() {
//	return reportName;
//	}

//	public void setReportName(String reportName) {
//	this.reportName = reportName;
//	}

//	private void imprimerReportParametresTimer() 
//	{
//	setLeReport((NSData)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(),
//	"session.remoteCallDelagate",
//	"clientSideRequestImprimerReportParametres",
//	new Class[] {String.class,NSDictionary.class},
//	new Object[] {getReportName(),getParameters()},true));
//	}

//	public NSData getLeReport() {
//	return leReport;
//	}

//	public void setLeReport(NSData leReport) {
//	this.leReport = leReport;
//	}	
//	}
}
