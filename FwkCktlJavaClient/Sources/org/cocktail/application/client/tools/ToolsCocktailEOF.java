/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.tools;


import com.webobjects.eocontrol.EOGlobalID;
import java.util.TimeZone;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class ToolsCocktailEOF {

	private ApplicationCocktail app =null;


	//recup connectionBaseDonnees
	public String getConnectionBaseDonnees() {
		return (String)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestConnectionBaseDonnees", null, null, false);
	}

	//recup version de l application
	public String getVersionApplication() {
		return (String)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestVersionApplication", null, null, false);
	}

	// Initialisation du NSDictionary parametresApplication
	protected NSDictionary initParametresApplication() {
		return (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestParametresApplication", null, null, false);
	}


	// Recherche dans une table
	public NSArray fetchArrayWithNomTableWithQualifierWithSort(String leNomTable, EOQualifier leQualifier, NSArray leSort,
			boolean refresh,
			boolean distinct
	) {
//		EOFetchSpecification myFetch;
//		myFetch = new EOFetchSpecification(leNomTable, leQualifier, leSort);
//		myFetch.setRefreshesRefetchedObjects(refresh);
//		myFetch.setUsesDistinct(distinct);
//		return new NSArray(getApp().getAppEditingContext().objectsWithFetchSpecification(myFetch));
        return fetchArrayWithNomTableWithQualifierWithSort(getApp().getAppEditingContext(), leNomTable, leQualifier, leSort, refresh, distinct);
	}
    
    // Recherche dans une table
    public NSArray fetchArrayWithNomTableWithQualifierWithSort(EOEditingContext ec,String leNomTable, EOQualifier leQualifier, NSArray leSort,
            boolean refresh,
            boolean distinct
    ) {
        EOFetchSpecification myFetch;
        myFetch = new EOFetchSpecification(leNomTable, leQualifier, leSort);
        myFetch.setRefreshesRefetchedObjects(refresh);
        myFetch.setUsesDistinct(distinct);
        return new NSArray(ec.objectsWithFetchSpecification(myFetch));
    }

	public boolean executeStoredProcedure(String proc, NSDictionary dico) {
		Object codeRetour = Boolean.FALSE;
		// appel de la procedure sur la session
		codeRetour = ((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestExecuteStoredProcedure", new Class[]{String.class, NSDictionary.class}, new Object[]{proc, dico}, false);
		return ((Boolean)codeRetour).booleanValue();
	}

	public NSDictionary returnValuesForLastStoredProcedureInvocation() {
		NSDictionary returnDico;
		returnDico = (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestReturnValuesForLastStoredProcedureInvocation", null, null, false);
		return returnDico;
	}

	// Debut de transaction
	public boolean beginTransaction() {
		Boolean codeRetour;
		codeRetour = (Boolean)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestBeginTransaction", null, null, false);
		return codeRetour.booleanValue();
	}

	// Validation de transaction
	public boolean commitTransaction() {
		Boolean codeRetour;
		codeRetour = (Boolean)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestCommitTransaction", null, null, false);
		return codeRetour.booleanValue();
	}

	// Annulation de transaction
	public boolean rollbackTransaction() {
		Boolean codeRetour;
		codeRetour = (Boolean)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestRollbackTransaction", null, null, false);
		return codeRetour.booleanValue();
	}

	// Recherche dans une table
	public NSArray fetchArrayWithNomTableWithQualifierWithSort(String leNomTable, EOQualifier leQualifier, NSArray leSort) {
//		EOFetchSpecification myFetch;
//		myFetch = new EOFetchSpecification(leNomTable, leQualifier, leSort);
//		//myFetch.setRefreshesRefetchedObjects(true);
//		myFetch.setUsesDistinct(true);
//		return new NSArray(getApp().getAppEditingContext().objectsWithFetchSpecification(myFetch));
        return fetchArrayWithNomTableWithQualifierWithSort(getApp().getAppEditingContext(), leNomTable, leQualifier, leSort);
	}
    
    public NSArray fetchArrayWithNomTableWithQualifierWithSort(EOEditingContext ec,String leNomTable, EOQualifier leQualifier, NSArray leSort) {
//        EOFetchSpecification myFetch;
//        myFetch = new EOFetchSpecification(leNomTable, leQualifier, leSort);
//        //myFetch.setRefreshesRefetchedObjects(true);
//        myFetch.setUsesDistinct(true);
//        return new NSArray(getApp().getAppEditingContext().objectsWithFetchSpecification(myFetch));
        return fetchArrayWithNomTableWithQualifierWithSort(ec, leNomTable, leQualifier, leSort, true, true);
    }


	public NSData getSQlResult(String sql,NSArray keys)
	{
		try {

			// appel de la procedure sur la session
			return ((NSData) ((EODistributedObjectStore)app.getAppEditingContext().parentObjectStore()).
					invokeRemoteMethodWithKeyPath(app.getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestGetSQLResult",
							new Class[]{String.class,NSArray.class},
							new Object[]{sql,keys}, false));

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public ToolsCocktailEOF(ApplicationCocktail app) {
		super();
		this.setApp(app);
	}

	void setApp(ApplicationCocktail app) {
		this.app = app;
	}

	ApplicationCocktail getApp() {
		return app;
	}
    
    public EOQualifier getQualifierForPeriodeAndExercice(String pathKeyOuverture, String pathKeyCloture, EOExercice exercice){
        NSMutableArray ou = new NSMutableArray();
        NSMutableArray et1 = new NSMutableArray();
        et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" = nil", null));
        et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateFinForExercice(exercice))));
        NSMutableArray et2 = new NSMutableArray();
        et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" >= %@", new NSArray(dateDebutForExercice(exercice))));
        et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateFinForExercice(exercice))));
        NSMutableArray et3 = new NSMutableArray();
        et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" >= %@", new NSArray(dateFinForExercice(exercice))));
        et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateDebutForExercice(exercice))));
        ou.addObject(new EOAndQualifier(et1));
        ou.addObject(new EOAndQualifier(et2));
        ou.addObject(new EOAndQualifier(et3));
        return new EOOrQualifier(ou);
    }
    
    private NSTimestamp dateDebutForExercice(EOExercice exercice){
        String timeZone = app.getApplicationParametre("GRHUM_TIMEZONE");
        if(timeZone==null)
            timeZone = "Europe/Paris";
        //le 01/01/de l'anne de l'exercice
        return new NSTimestamp(exercice.exeOrdre().intValue(),1,1,0,0,0,TimeZone.getTimeZone(timeZone));
    }
    
    private NSTimestamp dateFinForExercice(EOExercice exercice){
        String timeZone = app.getApplicationParametre("GRHUM_TIMEZONE");
        if(timeZone==null)
            timeZone = "Europe/Paris";
        //lle 01/01/de l'anne suivant celle de l'exercice
        return new NSTimestamp(exercice.exeOrdre().intValue()+1,1,1,0,0,0,TimeZone.getTimeZone(timeZone));
    }
    
    public NSDictionary getPrimaryKeyForObject(EOGenericRecord obj){
        if(obj==null || obj.editingContext()==null)
            return new NSDictionary();
        return getPrimaryKeyForGlobalId(obj.editingContext().globalIDForObject(obj));
    }

    private NSDictionary getPrimaryKeyForGlobalId(EOGlobalID gId) {
        if(gId==null)
            return new NSDictionary();
        return  (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{gId}, false);
    }
    
    public UserInfoCocktail getUserInfoCompteIndividuforPersId(Number persId){
        UserInfoCocktail userInfo = new UserInfoCocktail();
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("toRepartCompte.persId = %@", new NSArray(persId));
        NSMutableArray sort = new NSMutableArray();
        sort.addObject(EOSortOrdering.sortOrderingWithKey("cptVlan", EOSortOrdering.CompareAscending));
        NSArray list = app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(getApp().getAppEditingContext(),"FwkCktlWebApp_Compte", qual, sort);
        if(list==null || list.count()==0)
        {
            return userInfo;
        }
        else
        {
            EOGenericRecord eog = (EOGenericRecord) list.lastObject();
            try {
                userInfo.setEmail((String)eog.valueForKey("email"));
                userInfo.setLogin((String)eog.valueForKey("cptLogin"));
                userInfo.setNoCompte((Number)eog.valueForKey("cptOrdre"));
                EOGenericRecord repartCompte = (EOGenericRecord)((NSArray)eog.valueForKey("toRepartCompte")).lastObject();
                userInfo.setNoIndividu((Number)repartCompte.valueForKeyPath("toIndividuUlr.noIndividu"));
                userInfo.setNom((String)repartCompte.valueForKeyPath("toIndividuUlr.nomUsuel"));
                userInfo.setPersId(persId);
                userInfo.setPrenom((String)repartCompte.valueForKeyPath("toIndividuUlr.prenom"));
                userInfo.setUserName((String)repartCompte.valueForKeyPath("toIndividuUlr.nomUsuel"));
                char c= ((String)eog.valueForKey("cptVlan")).charAt(0);
                switch(c){
                    case 'X':
                        userInfo.setVLan("Exterieur");
                        break;
                    case 'P':
                        userInfo.setVLan("Professionnelle");
                        break;
                    default:
                        userInfo.setVLan("Etudiant");
                }
                userInfo.setLastError(null);
            } catch (Throwable exception) {
                exception.printStackTrace();
                userInfo.setLastError(exception.getMessage());
            }
        }
        return userInfo;
    }

}
