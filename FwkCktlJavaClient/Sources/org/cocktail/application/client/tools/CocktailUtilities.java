/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.tools;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoundedRangeModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.TableColumn;

import org.cocktail.application.client.swing.ZDatePickerPanel;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedClassDescription;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.EOMasterDetailAssociation;
import com.webobjects.eointerface.EOTableAssociation;
import com.webobjects.eointerface.EOTextAssociation;
import com.webobjects.eointerface.swing.EOImageView;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Reprise des méthodes présentes à l'origine dans ULRUtilities
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public final class CocktailUtilities {

	private static final String LNX_CMD = "evince ";
	private static final String MAC_CMD = "open ";
	private static final	Color COULEUR_FOND_ACTIF = Color.white;
	private static final	Color COULEUR_FOND_INACTIF = new Color(222,222,222);
	//	private static final	Color COULEUR_FOND_INACTIF = new Color(255,0,0);

	private static final	Color COULEUR_TEXTE_ACTIF = Color.black;
	private static final	Color COULEUR_TEXTE_INACTIF = Color.black;

	private static final 	EOClientResourceBundle lesRessources = new EOClientResourceBundle(); 

	private static JTextField myTextField;

	private CocktailUtilities() {
	}

	public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) {

		EODistributedClassDescription description = (EODistributedClassDescription) EOClassDescription.classDescriptionForEntityName(entity);
		EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);

		return object;

	}

	/**
	 * 
	 * @param classeParent
	 * @param methodeName
	 * @param parametres
	 * @param objects
	 */
	public static void lancerOperation(Object target, String methodeName, Class[] parametres, Object[] objects) {
		try {
			java.lang.reflect.Method methode = target.getClass().getMethod(methodeName, parametres);
			methode.invoke(target, objects);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isEmpty(JTextField component) {
		return getTextFromField(component) == null;
	}
	public static boolean isEmpty(JTextArea component) {
		return getTextFromArea(component) == null;
	}

	/**
	 * 
	 * @param total
	 * @param sommePartielle
	 * @return Pourcentage d'une somme partielle par rapport à une somme globale
	 */
	public static BigDecimal getPourcentage(BigDecimal sommePartielle, BigDecimal sommeTotale) {
		BigDecimal pourcentage = (sommePartielle.multiply(new BigDecimal(100))).divide(sommeTotale, BigDecimal.ROUND_HALF_DOWN);
		pourcentage = pourcentage.setScale(2, BigDecimal.ROUND_HALF_DOWN);
		
		return pourcentage;
	}

	/**
	 * 
	 * @param valeur
	 * @param pourcentage
	 * @return
	 */
	public static BigDecimal appliquerPourcentage(BigDecimal valeur, BigDecimal pourcentage) {
		BigDecimal retour =  ( valeur.multiply(pourcentage)) .divide(new BigDecimal(100));
		return retour.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * Methode qui <b>tente</b> de contourner le bug EOF qui se produit lors
	 * d'un saveChanges avec l'erreur "reentered responseToMessage()".<br>
	 * <b>Il faut appeler cette methode avant de creer un descendant d'EOCustomObject, donc bien avant le saveChanges()</b><br>
	 *
	 * Le principe est d'appeler la methode EOClassDescription.classDescriptionForEntityName("A") pour chaque relation de l'objet qu'on va creer.
	 * Il faut appeler cette methode avant de creer un objet, au lancement de l'application par exemple, sur toutes les entites du modele.
	 * Par exemple dans le cas d'un objet Facture qui a des objets Ligne, appeler EOClassDescription.classDescriptionForEntityName("Facture")
	 * avant de creer un objet Ligne.
	 * Repeter l'operation pour toutes les relations de l'objet.
	 *
	 * @param list Liste de String identifiant une entite du modele.
	 * @see http://www.omnigroup.com/mailman/archive/webobjects-dev/2002-May/023698.html
	 */
	public static final void fixWoBug_responseToMessage(final String[] list) {
		for (int i = 0; i < list.length; i++) {
			EOClassDescription.classDescriptionForEntityName(list[i]);
		}
	} 
	/**
	 * 
	 * @param myTable
	 * @param selection
	 */
	public static void forceSelection(ZEOTable myTable, NSArray selection) {

		myTable.forceNewSelectionOfObjects(selection);

	}

	public static NSTimestamp debutAnneeCivile(Integer annee) {		
		return DateCtrl.stringToDate("01/01/"+annee);
	}
	public static NSTimestamp finAnneeCivile(Integer annee) {		
		return DateCtrl.stringToDate("31/12/"+annee);
	}

	public static NSTimestamp debutAnneeUniversitaire(NSTimestamp dateReference) {
		Integer mois = new Integer(DateCtrl.getMonth(dateReference));
		Integer annee = new Integer(DateCtrl.getYear(dateReference));
		if (mois < 9 )
			annee = new Integer(annee.intValue() - 1);

		return DateCtrl.stringToDate("01/09/"+annee.toString());
	}
	public static NSTimestamp finAnneeUniversitaire(NSTimestamp dateReference) {
		Integer mois = new Integer(DateCtrl.dateToString(dateReference, "%m"));
		Integer annee = new Integer(DateCtrl.dateToString(dateReference, "%Y"));
		if (mois > 9 )
			annee = new Integer(annee.intValue() + 1);

		return DateCtrl.stringToDate("31/08/"+annee.toString());
	}
	
	public static NSTimestamp debutAnneeCivile(NSTimestamp dateReference) {		
		return DateCtrl.stringToDate("01/01/"+DateCtrl.getYear(dateReference));
	}
	public static NSTimestamp finAnneeCivile(NSTimestamp dateReference) {
		return DateCtrl.stringToDate("31/12/"+DateCtrl.getYear(dateReference));
	}

	public static void setFocusOn(JTextField textField) {
		textField.setRequestFocusEnabled(true);
		textField.requestFocus();
	}

	public static String getErrorDialog(Exception e) {
		e.printStackTrace();

		String text = e.getMessage();

		if ((text == null) || (text.trim().length() == 0)) {
			System.out.println("ERREUR... Impossible de recuperer le message...");
			e.printStackTrace();
			text = "ERREUR. Impossible de récupérer le message, il doit etre accessible dans la console...";
		}
		else {
			String[] msgs = text.split("ORA-20001:");
			if (msgs.length > 1) {
				text = msgs[1].split("\n")[0];
			}
		}
		return text;
	}


	public static void changerFontForLabel(JLabel textField,int style) {
		Font font = textField.getFont();
		textField.setFont(new Font(font.getName(),style,font.getSize()));
	}


	/**
	 * 
	 * @param textField
	 * @param scale
	 * @return
	 */
	public static BigDecimal getBigDecimalFromFieldWithScale(JTextField textField, int scale) {
		try {
			return getBigDecimalFromField(textField).setScale(scale, BigDecimal.ROUND_HALF_UP);
		}
		catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	} 

	public static BigDecimal getBigDecimalFromField(JTextField textField) {
		try {
			String value = NSArray.componentsSeparatedByString(textField.getText(), ",").componentsJoinedByString(".");
			return new BigDecimal(value);
		}
		catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	} 
	public static Double getDoubleFromField(JTextField textField) {
		try {
			String value = NSArray.componentsSeparatedByString(textField.getText(), ",").componentsJoinedByString(".");
			return new Double(value);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	} 
	public static Integer getIntegerFromField(JTextField textField) {
		try {
			return new Integer(textField.getText());
		}
		catch (Exception e) {
			return null;
		}
	} 
	public static String getTextFromField(JTextField textField) {
		if (!StringCtrl.isEmpty(textField.getText() ))
			return textField.getText();
		return null;
	} 
	public static String getTextFromArea(JTextArea textField) {
		if (!StringCtrl.isEmpty(textField.getText() ))
			return textField.getText();
		return null;
	} 
	public static NSTimestamp getDateFromField(JTextField field) {
		try {
			return DateCtrl.stringToDate(field.getText());
		}
		catch (Exception e) {
			return null;			
		}
	}

	public static void setTextToArea(JTextArea area, String texte) {
		if (texte == null)
			area.setText("");
		else {
			area.setText(texte);
		}		
	} 
	public static void setTextToField(JTextField textfield, String texte) 
	{
		if (texte == null)
			textfield.setText("");
		else {

			textfield.setText(texte);
			textfield.moveCaretPosition(0);
			BoundedRangeModel tmpBoundedRangeModel = textfield.getHorizontalVisibility();       
			if (tmpBoundedRangeModel.getMaximum()>textfield.getWidth() ) {
				textfield.setToolTipText(texte);
			}
			else {
				textfield.setToolTipText(null);
			}
		}		
	} 
	public static void setDateToField(JTextField textfield, NSTimestamp date) {
		if (date == null)
			textfield.setText("");
		else {
			textfield.setText(DateCtrl.dateToString(date));
		}		
	} 
	public static void setDateToField(JTextField textfield, NSTimestamp date, String format) {
		if (date == null)
			textfield.setText("");
		else {
			textfield.setText(DateCtrl.dateToString(date, format));
		}		
	}
	public static void setNumberToField(JTextField textfield, Number myNumber) {
		if (myNumber == null)
			textfield.setText("");
		else {
			textfield.setText(myNumber.toString());
		}		
	} 
	public static void setTextToLabel(JLabel label, String texte) {
		if (texte == null)
			label.setText("");
		else {
			label.setText(texte);
		}		
	}

	public static void viderLabel(JLabel label)  {
		label.setText("");
	}
	public static void viderTextField(JTextField textfield)  {
		textfield.setText("");
	}
	public static void viderTextArea(JTextArea textArea)  {
		textArea.setText("");
	}

	public static void removeActionListeners(JComboBox[] liste) {
		for (JComboBox popup : liste) {
			for (ActionListener listener : popup.getActionListeners()) {
				popup.removeActionListener(listener);			
			}
		}
	}
	public static void removeActionListeners(JRadioButton[] liste) {
		for (JRadioButton radioButton : liste) {
			for (ActionListener listener : radioButton.getActionListeners()) {
				radioButton.removeActionListener(listener);			
			}
		}
	}
	public static void removeActionListeners(JCheckBox[] liste) {
		for (JCheckBox checkBox : liste) {
			for (ActionListener listener : checkBox.getActionListeners()) {
				checkBox.removeActionListener(listener);			
			}
		}
	}
	public static void viderPopup(JComboBox popup)  {
		popup.removeAllItems();
	}

	public static void removeActionListeners(JComboBox popup) {
		for (ActionListener listener : popup.getActionListeners()) {
			popup.removeActionListener(listener);			
		}
	}
	public static void removeActionListeners(JCheckBox checkBox) {
		for (ActionListener listener : checkBox.getActionListeners()) {
			checkBox.removeActionListener(listener);			
		}
	}
	public static void removeActionListeners(JRadioButton boutonRadio) {
		for (ActionListener listener : boutonRadio.getActionListeners()) {
			boutonRadio.removeActionListener(listener);			
		}
	}
	/**
	 * 
	 * @param debut
	 * @param fin
	 * @param triCroissant
	 * @return Liste d'annees en fonction des annees de debut et fin (Croissant ou decroissant suivant l'ordre des parametres)
	 */
	public static NSArray<Integer> getListeAnnees(Integer debut, Integer fin) {		
		NSMutableArray<Integer> annees = new NSMutableArray<Integer>();

		if (debut.intValue() <= fin.intValue()) {
			for (int i=debut.intValue();i<=fin.intValue();i++) {
				annees.addObject(new Integer(i));
			}
		}
		else {
			for (int i=debut.intValue();i>=fin.intValue();i--) {
				annees.addObject(new Integer(i));			
			}
		}
		return annees.immutableClone();		
	}

	public static void setImageForImageView(String nomImage, EOImageView laVue)
	{
		laVue.setImageScaling(EOImageView.ScaleToFit);

		if (nomImage != null)
			laVue.setImage(((ImageIcon)lesRessources.getObject(nomImage)).getImage());
		else
			laVue.setImage(null);
	}


	/** Initialisation d'un champ de saisie. Permet d'initialiser le texte, la couleur du texte, la couleur du fond , et de le rendre editable ou non*/
	public static void initTextField(JTextField leChamp, boolean clean, boolean actif)
	{
		if (actif)
		{
			leChamp.setForeground(COULEUR_TEXTE_ACTIF);
			leChamp.setBackground(COULEUR_FOND_ACTIF);
		}
		else
		{
			leChamp.setForeground(COULEUR_TEXTE_INACTIF);
			leChamp.setBackground(COULEUR_FOND_INACTIF);
		}

		if (clean)
			leChamp.setText(null);

		leChamp.setEditable(actif);
	}

	/** Initialisation d'un champ de saisie. Permet d'initialiser le texte, la couleur du texte, la couleur du fond , et de le rendre editable ou non*/
	public static void initTextArea(JTextArea leChamp, boolean clean, boolean actif)
	{
		if (actif)
		{
			leChamp.setForeground(COULEUR_TEXTE_ACTIF);
			leChamp.setBackground(COULEUR_FOND_ACTIF);
		}
		else
		{
			leChamp.setForeground(COULEUR_TEXTE_INACTIF);
			leChamp.setBackground(COULEUR_FOND_INACTIF);
		}

		if (clean)
			leChamp.setText(null);

		leChamp.setEditable(actif);
	}

	/**
	 * 
	 * @param lePopup
	 */
	public static void initPopupAvecListe(JComboBox lePopup, NSArray liste, boolean isSelectionVide, String stringSelectionVide) {

		lePopup.removeAllItems();

		if (isSelectionVide) {
			if (stringSelectionVide == null) {
				lePopup.addItem("");
			}
			else {
				lePopup.addItem(stringSelectionVide);				
			}
		}

		for (int i=0;i < liste.size();i++)
			lePopup.addItem(liste.objectAtIndex(i));

	}
	
	/**
	 * 
	 * @param lePopup
	 */
	public static void initPopupAvecListe(JComboBox lePopup, NSArray liste, boolean selectionVide) {

		lePopup.removeAllItems();

		if (selectionVide)
			lePopup.addItem("");

		for (int i=0;i < liste.size();i++)
			lePopup.addItem(liste.objectAtIndex(i));

	}
	/**
	 * 
	 * @param lePopup
	 */
	public static void initPopupAvecListe(JComboBox lePopup, Object[] liste, boolean selectionVide) {

		lePopup.removeAllItems();

		if (selectionVide)
			lePopup.addItem("");

		for (int i=0;i < liste.length;i++)
			lePopup.addItem(liste[i]);

	}

	/**
	 * 
	 * @param lePopup
	 */
	public static void initPopupOuiNon(JComboBox lePopup) {
		initPopupOuiNon(lePopup, true);
	}

	/**
	 * 
	 * @param lePopup
	 */
	public static void initPopupOuiNon(JComboBox lePopup, boolean selectionVide) {

		lePopup.removeAllItems();
		if (selectionVide) {
			lePopup.addItem("*");
		}
		lePopup.addItem(CocktailConstantes.VRAI);
		lePopup.addItem(CocktailConstantes.FAUX);

	}
	
	/** Initialisation d'un champ de saisie. Permet d'initialiser le texte, la couleur du texte, la couleur du fond , et de le rendre editable ou non*/
	public static void initPopup(JComboBox lePopup, boolean clean, boolean actif) {
		if (actif)
		{
			lePopup.setForeground(COULEUR_TEXTE_ACTIF);
			lePopup.setBackground(COULEUR_FOND_ACTIF);
		}
		else
		{
			lePopup.setForeground(COULEUR_TEXTE_INACTIF);
			lePopup.setBackground(COULEUR_FOND_INACTIF);
		}

		if (clean)
			lePopup.removeAllItems();

		lePopup.setEnabled(actif);
	}



	public static void openFile(String filePath) throws Exception {
		File aFile = new File(filePath);
		Runtime runtime = Runtime.getRuntime();
		if (System.getProperty("os.name").startsWith("Windows")) {
			runtime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\"" });
		}
		else
			if (System.getProperty("os.name").startsWith("Linux")) {
				runtime.exec(LNX_CMD + aFile);
			}
			else {
				runtime.exec(MAC_CMD + aFile);
			}

	}

	/**
	 * 
	 * @param URL
	 * @throws Exception
	 */
	public static void openURL(String URL) throws Exception {

		if (URL.startsWith("http://") == false) {
			URL = "http://" + URL;
			try {
				URL uneUrl = new URL(URL);
			} catch (Exception e) {
				throw new Exception("URL invalide");
			}
		}
		try {
			String os = System.getProperty("os.name");
			if (os.startsWith("Mac OS")) {
				Runtime.getRuntime().exec(MAC_CMD + URL);
			} else if (os.startsWith("Windows")) {
				Runtime.getRuntime().exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", URL} ); 
			}
			else if (os.startsWith("Linux")) {
				Runtime.getRuntime().exec(LNX_CMD + URL);
			}
		} catch (Exception e) {
			throw new Exception("Impossible d'afficher l'URL "+URL+"\nMESSAGE : " + e.getMessage());	
		}

	}


	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
	public static BigDecimal computeSumForKey(EODisplayGroup eo, String key) {

		try {
			BigDecimal total = new BigDecimal(0);

			int i = 0;
			while (i < eo.displayedObjects().count()) {
				try {
					if (NSDictionary.class.getName().equals(eo.allObjects().objectAtIndex(i).getClass().getName())
							|| NSMutableDictionary.class.getName().equals(eo.displayedObjects().objectAtIndex(i).getClass().getName())
							)	{
						NSDictionary dico = (NSDictionary) eo.displayedObjects().objectAtIndex(i);
						total = total.add((BigDecimal)dico.objectForKey(key));
					}
					else	{
						total = total.add(new BigDecimal(((EOEnterpriseObject) eo.displayedObjects().objectAtIndex(i)).valueForKey(key).toString()));
					}
				}
				catch (Exception e) {
					//e.printStackTrace();
					total = new BigDecimal(0);
					break;
				}
				i = i + 1;
			}

			return total.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		catch (Exception e) {
			return new BigDecimal(0);
		}
	}

	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
	public static BigDecimal computeSumForKey(NSArray myArray, String key) {

		BigDecimal total = new BigDecimal(0);

		int i = 0;
		while (i < myArray.count()) {
			try {
				if (NSDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						|| NSMutableDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						)	{
					NSDictionary dico = (NSDictionary) myArray.objectAtIndex(i);
					total = total.add((new BigDecimal(dico.objectForKey(key).toString())));
				}
				else	{
					total = total.add((BigDecimal) ((EOEnterpriseObject) myArray.objectAtIndex(i)).valueForKey(key));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				total = new BigDecimal(0);
				break;
			}
			i = i + 1;
		}

		return total.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
	public static Integer computeSumIntegerForKey(NSArray myArray, String key) {

		Integer total = new Integer(0);

		int i = 0;
		while (i < myArray.count()) {
			try {
				if (NSDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						|| NSMutableDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						)	{
					NSDictionary dico = (NSDictionary) myArray.objectAtIndex(i);
					total = total.intValue() + (new Integer(dico.objectForKey(key).toString())).intValue();
				}
				else	{
					total = total.intValue() + ((Integer)((EOEnterpriseObject) myArray.objectAtIndex(i)).valueForKey(key)).intValue();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				total = new Integer(0);
				break;
			}
			i = i + 1;
		}

		return total;
	}

	// DATE PICKER PANEL

	public static void setMyTextField(JTextField textField)	{
		myTextField = textField;
	}
	public static JTextField getMyTextField()	{
		return myTextField;
	}

	public static void showDatePickerPanel(Dialog parentWindow) {
		Date ladate=null;

		final JDialog datePickerDialog = new JDialog(parentWindow);
		if ((getMyTextField().getText()!=null) && ((getMyTextField().getText().length()>0)) ) {
			try {
				ladate = ((DateFormat)new SimpleDateFormat("dd/MM/yyyy")).parse(getMyTextField().getText());
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
		}

		ZDatePickerPanel datePickerPanel = new ZDatePickerPanel();

		ComponentAdapter myComponentAdapter = new ComponentAdapter() {
			public void componentHidden(final ComponentEvent evt) {
				final Date dt = ((ZDatePickerPanel) evt.getSource()).getDate();
				if (null != dt) {
					getMyTextField().setText( new SimpleDateFormat("dd/MM/yyyy").format(dt) );
				}
				datePickerDialog.dispose();
			}
		};
		datePickerPanel.addComponentListener(myComponentAdapter);

		datePickerPanel.open(ladate);
		final Point p = getMyTextField().getLocationOnScreen();
		p.setLocation(p.getX(), p.getY() - 1 + myTextField.getSize().getHeight());

		//Ajouter la gestion de la touche echap pour fermer la fenetre		
		KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);    
		Action escapeAction = new AbstractAction(){
			public void actionPerformed(ActionEvent e){
				datePickerDialog.hide(); 
			}
		};      
		datePickerDialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");    
		datePickerDialog.getRootPane().getActionMap().put("ESCAPE", escapeAction);	
		datePickerDialog.getRootPane().setFocusable(true);


		datePickerDialog.setResizable(false);
		datePickerDialog.setUndecorated(true);
		datePickerDialog.getContentPane().add(datePickerPanel);
		datePickerDialog.setLocation(p);
		datePickerDialog.pack();
		datePickerDialog.show();    		
	}

	public static void showDatePickerPanel(JFrame frameWindow) {
		Date ladate=null;

		final JDialog datePickerDialog = new JDialog(frameWindow);
		if ((getMyTextField().getText()!=null) && ((getMyTextField().getText().length()>0)) ) {
			try {
				ladate = ((DateFormat)new SimpleDateFormat("dd/MM/yyyy")).parse(getMyTextField().getText());
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
		}

		ZDatePickerPanel datePickerPanel = new ZDatePickerPanel();

		ComponentAdapter myComponentAdapter = new ComponentAdapter() {
			public void componentHidden(final ComponentEvent evt) {
				final Date dt = ((ZDatePickerPanel) evt.getSource()).getDate();
				if (null != dt) {
					getMyTextField().setText( new SimpleDateFormat("dd/MM/yyyy").format(dt) );
				}
				datePickerDialog.dispose();
			}
		};
		datePickerPanel.addComponentListener(myComponentAdapter);

		datePickerPanel.open(ladate);
		final Point p = getMyTextField().getLocationOnScreen();
		p.setLocation(p.getX(), p.getY() - 1 + myTextField.getSize().getHeight());

		//Ajouter la gestion de la touche echap pour fermer la fenetre		
		KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);    
		Action escapeAction = new AbstractAction(){
			public void actionPerformed(ActionEvent e){
				datePickerDialog.hide(); 
			}
		};      
		datePickerDialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");    
		datePickerDialog.getRootPane().getActionMap().put("ESCAPE", escapeAction);	
		datePickerDialog.getRootPane().setFocusable(true);


		datePickerDialog.setResizable(false);
		datePickerDialog.setUndecorated(true);
		datePickerDialog.getContentPane().add(datePickerPanel);
		datePickerDialog.setLocation(p);
		datePickerDialog.pack();
		datePickerDialog.show();    		
	}

	public static void affecterImageEtTextAuBouton(EOInterfaceController sender, ImageIcon icone, String text, JButton leBouton, String message) 
	{
		leBouton.setIcon(icone); 

		leBouton.setText(text);
		leBouton.setSelected(false);
		leBouton.setFocusPainted(false);
		leBouton.setToolTipText(message);
		leBouton.setDefaultCapable(false);

	}
	public static void putView(EOView maSwapView, Component maView)	{
		maSwapView.getComponent(0).setVisible(false);
		maSwapView.removeAll();
		maSwapView.setLayout(new BorderLayout());
		maSwapView.add(maView,BorderLayout.CENTER);
		maView.setVisible(true);
		maSwapView.validate();
	}

	/** Place une vue dans une autre. La swapView et la vue sont passes en parametres	*/
	public static void putView(EOView maSwapView, EOView maView)
	{
		maSwapView.getComponent(0).setVisible(false);
		maSwapView.removeAll();
		maSwapView.setLayout(new BorderLayout());
		maSwapView.add(maView,BorderLayout.CENTER);
		maView.setVisible(true);
		maSwapView.validate();
	}

	public static String returnTempStringName() {
		java.util.Calendar currentTime = java.util.Calendar.getInstance();

		String lastModif = "-"
				+ currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "."
				+ currentTime.get(java.util.Calendar.MONTH) + "."
				+ currentTime.get(java.util.Calendar.YEAR) + "-"
				+ currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h"
				+ currentTime.get(java.util.Calendar.MINUTE) + "m"
				+ currentTime.get(java.util.Calendar.SECOND);

		return lastModif;
	}

	/**
	 * Rendre les cellule d'une table non éditables
	 * 
	 * @param aTable
	 *            une {@link EOTable}
	 */
	public static void setNonEditableTable(EOTable aTable) {
		JTable actualTable = aTable.table();
		for (Enumeration e = actualTable.getColumnModel().getColumns(); e.hasMoreElements();) {
			((TableColumn) e.nextElement()).setCellEditor(null);
		}
	}

	/**
	 * Met à jour les associations d'une table (?)
	 * @param dg Un {@link EODisplayGroup}
	 */
	public static void informObservingAssociations(EODisplayGroup dg) {
		NSArray observingAssociations = dg.observingAssociations();
		int nbAssociations = observingAssociations.count();
		if (nbAssociations <= 0) {
			return;
		}
		for (int i = 0; i < nbAssociations; ++i) {
			if (observingAssociations.objectAtIndex(i).getClass().getName().equals("com.webobjects.eointerface.EOTableAssociation")) {
				((EOTableAssociation) observingAssociations.objectAtIndex(i)).subjectChanged();
			}

			if (observingAssociations.objectAtIndex(i).getClass().getName().equals("com.webobjects.eointerface.EOMasterDetailAssociation")) {
				EOMasterDetailAssociation anAssociation = (EOMasterDetailAssociation) observingAssociations.objectAtIndex(i);
				anAssociation.subjectChanged();

				EODisplayGroup detailGroup = (EODisplayGroup) anAssociation.object();
				informObservingAssociations(detailGroup);
			}
			if (observingAssociations.objectAtIndex(i).getClass().getName().equals("com.webobjects.eointerface.EOTextAssociation")) {
				((EOTextAssociation) observingAssociations.objectAtIndex(i)).subjectChanged();
			}
		}
	}
}
