/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)


package org.cocktail.application.client.tools;

import java.util.Iterator;

import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class ToolsCocktailLogs {

	
	static private ApplicationCocktail app = (ApplicationCocktail)EOApplication.sharedApplication();

	public void addLogMessage(Object source, String errorType, String message,boolean withLogs) {
		NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y - %H:%M:%S");
		String sourceClass = source.getClass().getName();
		NSLog.out.appendln("["+formatter.format(new NSTimestamp())+"] "+getApp().getAppName()+sourceClass+" - "+errorType+" : "+message);
	}

	public void trace(final String prefix, final NSArray a,boolean withLogs) {
		//System.out.println("Factory.trace() TRACE");
		for (int i = 0; i < a.count(); i++) {
			trace(prefix +"  "+i+"-->" , a.objectAtIndex(i), withLogs);
		}
	}

	public void trace(final Object obj,boolean withLogs) { 
		trace("", obj, withLogs);
	}

	public void trace(final String prefix, final Object obj,boolean withLogs) {
		if (withLogs) {
			if (obj==null) {
				System.out.println(prefix+"null");
			}
			else {
				if (obj instanceof NSArray) {
					trace (prefix, (NSArray)obj, withLogs);
				}
				else if (obj instanceof EOEnterpriseObject) {
					trace (prefix, (EOEnterpriseObject)obj, withLogs);
				}
				else {
					System.out.println (prefix + obj.toString());
				}
			}

		}
	}    

	public void trace(final String prefix, final EOEnterpriseObject object,boolean withLogs) {

		if (object!=null) {
			final Iterator iter = object.attributeKeys().vector().iterator();
			while (iter.hasNext()) {
				String obj = (String) iter.next();
				trace(prefix+"  "+ obj  +"-->", object.valueForKey(obj) , withLogs);
			}

			Iterator iter2 = object.toOneRelationshipKeys().vector().iterator();
			while (iter2.hasNext()) {
				String obj = (String) iter2.next();
				trace(prefix+"  "+ obj  +"-->" + object.valueForKey(obj) , withLogs);
			}
			Iterator iter3 = object.toManyRelationshipKeys().vector().iterator();
			while (iter3.hasNext()) {
				String obj = (String) iter3.next();
				if (prefix != null && prefix.length()>250) {
					trace(prefix+"  "+ obj  +"-->" + object.valueForKey(obj), withLogs );
				}
				else {
					trace(prefix+"  "+ obj  +"-->" , object.valueForKey(obj), withLogs );
				}

			}
		}
	}  
    
    
    public static NSData logsServeur(){
        return (NSData)((EODistributedObjectStore)app.getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(app.getAppEditingContext(),
                "session.remoteCallDelagate",
                "clientSideRequestGetLogsFromServeur",
                null,null,true);
    }
    
    public static NSData statServeur(){
        return (NSData)((EODistributedObjectStore)app.getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(app.getAppEditingContext(),
                "session.remoteCallDelagate",
                "clientSideRequestGetStatFromServeur",
                null,null,true);
    }
    
    public static void addStatistique(String action,String msg){
    	((EODistributedObjectStore)app.getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(app.getAppEditingContext(),
                "session.remoteCallDelagate",
                "clientSideRequestWriteToStatistique",new Class[]{String.class,String.class,String.class,String.class},new Object[]{app.getUserInfos().nom()+" "+app.getUserInfos().prenom(),action,msg,""+app.getUserInfos().persId().intValue()},true);
    }
    
    public String messageAPropos(){
//        StringBuffer mess = new StringBuffer();
//        mess.append("\t"+app.applicationName());
//        mess.append("\n\t"+app.getVersionApplication());
//        mess.append("\n");
//        mess.append("Utilisateur : "+app.getUserInfos().valueForKey("nom")+" "+app.getUserInfos().valueForKey("prenom")+" ("+app.getUserInfos().valueForKey("email")+")");
//        mess.append("\nVersion Java : "+System.getProperty("java.vm.version"));
//        mess.append("\nInstance de la base : "+app.getToolsCocktailEOF().getConnectionBaseDonnees());
//        return mess.toString();
        return messageAPropos(false);
    }
    
    public String messageAPropos(boolean useHtml){
        StringBuffer mess = new StringBuffer();
        if(useHtml){
            mess.append("<html><body background=\"#EBEBEB\">");
            mess.append("<p style=\"text-align:center;\">"+app.applicationName());
            mess.append("<br>"+app.getVersionApplication());
            mess.append("</p><br><br>");
            mess.append("<p>Utilisateur : "+app.getUserInfos().valueForKey("nom")+" "+app.getUserInfos().valueForKey("prenom")+" ("+app.getUserInfos().valueForKey("email")+")");
            mess.append("<br>Version Java : "+System.getProperty("java.vm.version"));
            mess.append("<br>Instance de la base : "+app.getToolsCocktailEOF().getConnectionBaseDonnees());
            mess.append("</p></body></html>");
        }
        else
        {
            mess.append(app.applicationName());
            mess.append("\n"+app.getVersionApplication());
            mess.append("\n\n");
            mess.append("Utilisateur : "+app.getUserInfos().valueForKey("nom")+" "+app.getUserInfos().valueForKey("prenom")+" ("+app.getUserInfos().valueForKey("email")+")");
            mess.append("\nVersion Java : "+System.getProperty("java.vm.version"));
            mess.append("\nInstance de la base : "+app.getToolsCocktailEOF().getConnectionBaseDonnees());
        }
        return mess.toString();
    }


	public ToolsCocktailLogs(ApplicationCocktail app) {
		super();
		this.setApp(app);
	}

	void setApp(ApplicationCocktail app) {
		this.app = app;
	}

	ApplicationCocktail getApp() {
		return app;
	}


}
