/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.application.client.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.text.Format;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;




/**
 * Represente un champ texte.
 * @author rodolphe.prin@univ-lr.fr
 */
public class ZTextField extends JPanel implements IZDataComponent {
    public static final Insets DEFAULT_MARGIN = new Insets(0,2,0,2);
    public static final int DEFAULT_HGAP = 0;
    public static final int DEFAULT_VGAP = 2;
    private JTextComponent myTexfield;
    private Format myFormat;
    protected ArrayList documentListeners;
    protected IZTextFieldModel myModel;


    public ZTextField() {
        super();
        initObject();
    }

    /**
     *
     */
    public ZTextField(IZTextFieldModel provider) {
        super();
        myModel = provider;
        initObject();
        
    }


    protected void initObject() {
        ((FlowLayout)getLayout()).setAlignment(FlowLayout.LEFT);
        ((FlowLayout)getLayout()).setHgap(DEFAULT_HGAP);
        ((FlowLayout)getLayout()).setVgap(DEFAULT_VGAP);        
        
        
//        setLayout(new BorderLayout());
	    documentListeners = new ArrayList();

		myTexfield = new JTextField();
		myTexfield.setMargin(DEFAULT_MARGIN);
		getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
		addDocumentListener(new InnerDocumentListener());
//		System.out.println("init ZTextField");
		add(myTexfield);

    }


	public void addDocumentListener(DocumentListener aDocumentListener) {
	    documentListeners.add(aDocumentListener);
	    getMyTextComponent().getDocument().addDocumentListener(aDocumentListener);
	}

	public void removeDocumentListener(DocumentListener aDocumentListener) {
	    documentListeners.remove(aDocumentListener);
	    getMyTextComponent().getDocument().removeDocumentListener(aDocumentListener);
	}

    
    public void addActionListener(ActionListener al) {
        if (myTexfield instanceof JTextField ) {
            getMyTexfield().addActionListener(al);
        }
    }
    
    public void removeActionListener(ActionListener al) {
        if (myTexfield instanceof JTextField ) {
            getMyTexfield().removeActionListener(al);
        }
    }
    
    
    /**
     * @see org.cocktail.zutil.client.ui.ZLabelField#getContentComponent()
     */
    public Component getContentComponent() {
        return getMyTextComponent();
    }

	/**
     * Supprime la bordure autour du textfield et met un fond blanc.
     */
    public void setUIReadOnly() {
        getMyTextComponent().setBorder(BorderFactory.createEmptyBorder(DEFAULT_MARGIN.top, DEFAULT_MARGIN.left, DEFAULT_MARGIN.bottom, DEFAULT_MARGIN.right));
        getMyTextComponent().setBackground(Color.WHITE);
    }

    public void setUIReadOnly(Color bgcol) {
        getMyTextComponent().setBorder(BorderFactory.createEmptyBorder(DEFAULT_MARGIN.top, DEFAULT_MARGIN.left, DEFAULT_MARGIN.bottom, DEFAULT_MARGIN.right));
        getMyTextComponent().setBackground(bgcol);
    }


    /**
     * @see javax.swing.JComponent#setEnabled(boolean)
     */
    public void setEnabled(boolean enabled) {
//        getMyTextComponent().setEnabled(enabled);
        
        for (int i = 0; i < getComponentCount(); i++) {
            getComponent(i).setEnabled(enabled);
        }        
        
        super.setEnabled(enabled);
    }


	/**
	 * @return
	 */
	public Format getFormat() {
		return myFormat;
	}

	/**
	 * @param format
	 */
	public void setFormat(Format format) {
		myFormat = format;
	}




	public JTextComponent getMyTextComponent() {
		return myTexfield;
	}


	public JTextField getMyTexfield() {
		return (JTextField)myTexfield;
	}

	protected void notifyDataChanged() {
		if (myModel!=null) {
			myModel.setValue(  getInnerValue() );
		}
	}

	protected void disableAllDocumentsListener() {
	    final Iterator iter = documentListeners.iterator();
	    while (iter.hasNext()) {
            final DocumentListener element = (DocumentListener) iter.next();
            getMyTextComponent().getDocument().removeDocumentListener(element);
        }
	}

	protected void enableAllDocumentsListener() {
	    final Iterator iter = documentListeners.iterator();
	    while (iter.hasNext()) {
            final DocumentListener element = (DocumentListener) iter.next();
            getMyTextComponent().getDocument().addDocumentListener(element);
        }
	}

	/**
	 * Remplace la chaine de caractere affichee par la valeur renvoyee par getValueToDisplay.
	 * Appeler cette methode pour mettre a jour les informations sans notification.
	 */
	public void updateData() {
	    //Virer les documentsListener
	    disableAllDocumentsListener();
		getMyTextComponent().setText(getValueToDisplay());
		enableAllDocumentsListener();
	}

	/**
	 * Remplace la chaine de caractere affichee par la valeur renvoyee par getValueToDisplay.
	 * Appeler cette methode pour mettre a jour les informations avec notification.
	 */
	public final void updateDataAndNotify() {
	    getMyTextComponent().setText(getValueToDisplay());
	}

	/**
	 * Surchargez eventuellement cette methode
	 * @return La valeur a afficher (par defaut toString(), sauf si un format est defini).
	 */
	protected String getValueToDisplay() {
		final Object val = myModel.getValue();
		if (val!=null) {
			if (myFormat!=null) {
				return myFormat.format(val);
			}
            return val.toString();
		}
        return null;
	}



	public void moveCaret(final int position) {
	    if (position <= getMyTextComponent().getDocument().getLength()) {
	        getMyTextComponent().moveCaretPosition(position);
	    }
	}


	/**
	 * @return La valeur construite a partir du contenu du champ texte.
	 * Si la valeur saisie ne respecte pas le format specifie, null est renvoye.
	 */
	protected Object getInnerValue() {
		final String txt = getMyTextComponent().getText();
//		System.out.println("getInnerValue = " + txt);
		if (txt==null) {
			return null;
		}
        if (getFormat()==null) {
        	return txt;
        }
        try {
        	return getFormat().parseObject(txt);
        } catch (ParseException e) {
        	return null;
        }
	}



	/**
	 * Pour repondre au saisies sur le champ texte.
	 */
	protected class InnerDocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			notifyDataChanged();
		}

		public void insertUpdate(DocumentEvent e) {
			notifyDataChanged();
		}

		public void removeUpdate(DocumentEvent e) {
			notifyDataChanged();
		}
	}

	public interface IZTextFieldModel extends IZDataCompModel {
	}
    
    public IZTextFieldModel getMyModel() {
        return myModel;
    }
    public void setMyModel(IZTextFieldModel myModel) {
        this.myModel = myModel;
    }
    
    public void setText(String text) {
        getMyTextComponent().setText(text);
    }
    
    public String getText() {
        return getMyTextComponent().getText();
    }


    /**
     * Permet d'affecter un filtre de saisie sur le document (par exemple pour limiter le nombre de caracteres). @see FixedSizeFilter.
     * @param documentFilter
     */
    public void setDocumentFilter(DocumentFilter documentFilter) {
        AbstractDocument doc = (AbstractDocument)getMyTextComponent().getDocument();
        doc.setDocumentFilter(documentFilter);
    }



    /**
     * Fixe le nombre de caracteres maximum d'un champ.
     * @param max
     */
    public void setMaxChars(final int max) {
        if (max>0) {
            FixedSizeFilter fixedSizeFilter = new FixedSizeFilter(max);
            setDocumentFilter(fixedSizeFilter);
        }
    }




    /**
     * eviter d'utiliser cette classe (
     * @author rodolphe.prin@univ-lr.fr
     */
    public static class DefaultTextFieldModel implements IZTextFieldModel {
        private Map _filter;
        private String _key;
        /**
         *
         */
        public DefaultTextFieldModel(final Map filter, final String key) {
            _filter = filter;
            _key = key;
        }

        /**
         * @see org.cocktail.application.client.swing.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
//            System.out.println(_key + " = " + _filter.get(_key));
            return _filter.get(_key);
        }

        /**
         * @see org.cocktail.application.client.swing.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            if (value instanceof String ) {
                if (((String)value).trim().length()==0 ) {
                    value = null;
                }
                else {
                    value = ((String)value).trim();
                }
            }
            _filter.put(_key,value);

        }

    }

    /**
     * Filtre permetttant de limiter le nombre de caracteres d'un champ.
     * @author rodolphe.prin@univ-lr.fr
     */
    public static class FixedSizeFilter extends DocumentFilter {
        final int maxSize;

        // limit is the maximum number of characters allowed.
        public FixedSizeFilter(final int limit) {
            maxSize = limit;
        }

        // This method is called when characters are inserted into the document
        public void insertString(DocumentFilter.FilterBypass fb, int offset, String str,
                AttributeSet attr) throws BadLocationException {
            replace(fb, offset, 0, str, attr);
        }

        // This method is called when characters in the document are replace with other characters
        public void replace(DocumentFilter.FilterBypass fb, int offset, int length,
                String str, AttributeSet attrs) throws BadLocationException {
            final int newLength = fb.getDocument().getLength()-length+str.length();
            if (newLength <= maxSize) {
                fb.replace(offset, length, str, attrs);
            } else {
                throw new BadLocationException("Vous ne pouvez pas dépasser " + maxSize +" caractères.", offset);
            }
        }
    }

}
