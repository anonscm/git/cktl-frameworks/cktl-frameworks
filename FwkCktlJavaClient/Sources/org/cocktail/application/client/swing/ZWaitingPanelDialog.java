/*
 * Copyright (C) 2004 Universite de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.application.client.swing;

import java.awt.Dialog;
import java.awt.Frame;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;



/**
 *
 *
 * @author Rodolphe Prin
 */
public class ZWaitingPanelDialog extends JDialog {

	private ZWaitingPanel myZWaitingPanel;

	public ZWaitingPanel getWaitingPanel() {
		return myZWaitingPanel;
	}

	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener   ) {
		myZWaitingPanel = new ZWaitingPanel(listener);
		setContentPane(myZWaitingPanel);
	}
	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, ImageIcon img) {
		myZWaitingPanel = new ZWaitingPanel(listener, img);
		setContentPane(myZWaitingPanel);
	}

	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, Dialog dial) {
	    super(dial);
		myZWaitingPanel = new ZWaitingPanel(listener);
		setContentPane(myZWaitingPanel);
	}


	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, Dialog dial, ImageIcon img) {
	    super(dial);
		myZWaitingPanel = new ZWaitingPanel(listener, img);
		setContentPane(myZWaitingPanel);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int)getGraphicsConfiguration().getBounds().getWidth();
		int y = (int)getGraphicsConfiguration().getBounds().getHeight();

		setResizable(false);
		setLocation((x/2)-((int)getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)getContentPane().getMinimumSize().getHeight()/2)));
		pack();
	}


	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, Frame dial) {
	    super(dial);
		myZWaitingPanel = new ZWaitingPanel(listener);
		setContentPane(myZWaitingPanel);
	}
	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, Frame dial, ImageIcon img) {
	    super(dial);
		myZWaitingPanel = new ZWaitingPanel(listener, img);
		setContentPane(myZWaitingPanel);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int)getGraphicsConfiguration().getBounds().getWidth();
		int y = (int)getGraphicsConfiguration().getBounds().getHeight();

		setResizable(false);
		setLocation((x/2)-((int)getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)getContentPane().getMinimumSize().getHeight()/2)));
		pack();
	}


	public static ZWaitingPanelDialog getWindow(ZWaitingPanel.ZWaitingPanelListener listener, ImageIcon img) {

		ZWaitingPanelDialog win = new ZWaitingPanelDialog(listener, img);
		win.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int)win.getGraphicsConfiguration().getBounds().getWidth();
		int y = (int)win.getGraphicsConfiguration().getBounds().getHeight();

		win.setResizable(false);
		win.setLocation((x/2)-((int)win.getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)win.getContentPane().getMinimumSize().getHeight()/2)));
		win.pack();
		return win;
	}
	public static ZWaitingPanelDialog getWindow(ZWaitingPanel.ZWaitingPanelListener listener) {

		ZWaitingPanelDialog win = new ZWaitingPanelDialog(listener);
		win.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int)win.getGraphicsConfiguration().getBounds().getWidth();
		int y = (int)win.getGraphicsConfiguration().getBounds().getHeight();

		win.setResizable(false);
		win.setLocation((x/2)-((int)win.getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)win.getContentPane().getMinimumSize().getHeight()/2)));
		win.pack();
		return win;
	}


	public void setTopText(String s) {
		getWaitingPanel().setGlobalTaskDescription(s);
	}
	public void setBottomText(String s) {
		getWaitingPanel().setCurrentTaskDescription(s);
	}

	public final JProgressBar getMyProgressBar() {
	    return getWaitingPanel().getMyProgressBar();
	}

	public void setImg(ImageIcon img) {
	    myZWaitingPanel.setIcon(img);
	}





}
