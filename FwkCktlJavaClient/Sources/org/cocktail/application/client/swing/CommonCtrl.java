/*
 * Copyright COCKTAIL, 1995-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swing;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.swing.ZAbstractPanel;
import org.cocktail.application.client.swing.ZCommonDialog;
import org.cocktail.application.serveur.CocktailApplication;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;


/**
 * Classe dont doivent hériter tous les controleurs de l'application
 * @author rodolphe.prin@univ-lr.fr
 */
public abstract class CommonCtrl {
    protected static final ApplicationCocktail myApp = (ApplicationCocktail)EOApplication.sharedApplication();
    private final EOEditingContext _editingContext;
    
    private ZCommonDialog myDialog;
    
//    private ZAbstractPanel _panel;
    
    public CommonCtrl(EOEditingContext editingContext) {
        _editingContext = editingContext;
    }
    public CommonCtrl() {
        _editingContext=getDefaultEditingContext();
    }
    
    public final EOEditingContext getDefaultEditingContext() {
        return myApp.getAppEditingContext();
    }
    
    public EOEditingContext getEditingContext() {
        return (_editingContext==null ? getDefaultEditingContext() : _editingContext);
    }    
     
    /**
     * Assure que les modifications sur l'editingContext sont bien annulees.
     */
    protected final void revertChanges() {
        if (getEditingContext()==null) {
//            myApp.appLog.trace("getEditingContext() "+ this.getClass().getName()  +" null");
        }
        else {
            if (getEditingContext().hasChanges()) {
                getEditingContext().revert();
            }
            else {
            }
        }
    }

    public final ZCommonDialog getMyDialog() {
        return myDialog;
    }

    public final void setMyDialog(ZCommonDialog myDialog) {
        this.myDialog = myDialog;
    }    
    
    public void setWaitCursor(final boolean bool) {
        if (getMyDialog() instanceof ZCommonDialog ) {
            ((ZCommonDialog)getMyDialog()).setWaitCursor(bool);
        }
    }    
    
    public final void showErrorDialog(final Exception e) {
        setWaitCursor(false);
    }
    public final void showWarningDialog(final String s) {
        setWaitCursor(false);
    }

    public ApplicationCocktail getMyApp() {
        return myApp;
    }    
    
    public abstract String title();
    public abstract Dimension defaultDimension();
    public abstract ZAbstractPanel mainPanel();
    
    
    public ZCommonDialog createDialog(final Window parentWindow, boolean modal ) {
        final ZCommonDialog win;
        if (parentWindow instanceof Dialog) {
            win = new ZCommonDialog((Dialog)parentWindow, title(), modal);
        }
        else {
            win = new ZCommonDialog((Frame)parentWindow, title(), modal);
        }
        setMyDialog(win);
        
        mainPanel().setPreferredSize(defaultDimension());
        win.setContentPane(mainPanel());
        win.pack();
        return win;
    }     
    
    
    public int openDialog(Window dial, boolean modal ) {
        int res = ZCommonDialog.MRCANCEL;
        ZCommonDialog win = createDialog(dial, modal);
        try {
            mainPanel().onBeforeShow();
            mainPanel().updateData();
            win.open();
            res = win.getModalResult();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
        return res;
    }     
    
    


    
    
    
}
