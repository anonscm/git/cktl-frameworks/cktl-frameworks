package org.cocktail.application.client.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.lang.ref.WeakReference;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class MyJTable extends JTable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1202589657512734986L;
	
	private WeakReference<TableCellRenderer> wrappedHeaderRendererRef = null;
	private TableCellRenderer wrapperHeaderRenderer = null;

	public MyJTable(TableModel dm) {

		super(dm);

		try {

			int height=15;
			if (getFont()!=null)
				height=getFontMetrics(getFont()).getHeight();
			getTableHeader().setPreferredSize(new Dimension(getTableHeader().getWidth(), height+4));
			
		}
		catch (Exception ex) {
		}

	}


	private class MyTableColumn extends TableColumn {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6643459785116014983L;
		MyTableColumn(int modelIndex) {
			super(modelIndex);
		}
		public TableCellRenderer getHeaderRenderer() {
			TableCellRenderer defaultHeaderRenderer = 
				MyJTable.this.getTableHeader().getDefaultRenderer();
			if (wrappedHeaderRendererRef == null 
					|| wrappedHeaderRendererRef.get() != defaultHeaderRenderer) {
				wrappedHeaderRendererRef = 
					new WeakReference<TableCellRenderer>(defaultHeaderRenderer);
				wrapperHeaderRenderer = 
					new DecoratedHeaderRenderer(defaultHeaderRenderer);
			}


			return wrapperHeaderRenderer;
		}
	}
	public void createDefaultColumnsFromModel() {
		TableModel m = getModel();
		if (m != null) {
			// Remove any current columns
			TableColumnModel cm = getColumnModel();
			while (cm.getColumnCount() > 0) {
				cm.removeColumn(cm.getColumn(0));
			}

			// Create new columns from the data model info
			for (int i = 0; i < m.getColumnCount(); i++) {
				TableColumn newColumn = new MyTableColumn(i);
				addColumn(newColumn);
			}
		}
	}

	private class DecoratedHeaderRenderer implements TableCellRenderer {

		public DecoratedHeaderRenderer(TableCellRenderer render){
			this.render = render;
		}
		private TableCellRenderer render;
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			Component c = render.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
			return c;
		}

	}


}
