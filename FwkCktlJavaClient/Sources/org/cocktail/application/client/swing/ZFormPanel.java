/*
 * Copyright CRI - Universite de La Rochelle, 1995-2004
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;




/**
 * @author rodolphe.prin@univ-lr.fr
 */
public class ZFormPanel extends JPanel implements IZDataComponent {
    public static final int DEFAULT_LABEL_HEIGHT = 14;
    public static final int DEFAULT_HGAP = 4;
    public static final int DEFAULT_VGAP = 0;

    
    private final ArrayList myFields = new ArrayList();

    /**
     *
     */
    public ZFormPanel() {
        this(new FlowLayout(FlowLayout.LEFT, DEFAULT_HGAP, DEFAULT_VGAP));
    }

    /**
     * @param layout
     */
    public ZFormPanel(LayoutManager layout) {
        super(layout);
    }

    /**
     * @see java.awt.Container#add(java.awt.Component)
     */
    public Component add(final Component comp) {
//        if (comp instanceof ZTextField ) {
        if (comp instanceof IZDataComponent ) {
            myFields.add(comp);
        }
        return super.add(comp);
    }
    


    /**
     * @see java.awt.Container#add(java.awt.Component, java.lang.Object)
     */
    public void add(final Component comp, final Object constraints) {
        if (comp instanceof ZTextField) {
            myFields.add(comp);
        }
        super.add(comp, constraints);
    }

    /**
     * @return Renvoie la liste des champs de type ZTextField ou Component contenus dans le panel.
     */
    public ArrayList getMyFields() {
        return myFields;
    }

    public void updateData() throws Exception {
        final Iterator iter = myFields.iterator();
        while (iter.hasNext()) {
            final Component element = (Component) iter.next();
            if (element instanceof IZDataComponent) {
                ((IZDataComponent)element).updateData();
            }
            
        }
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (int i = 0; i < getComponentCount(); i++) {
            getComponent(i).setEnabled(enabled);
        }
//        
//        final Iterator iter = myFields.iterator();
//        while (iter.hasNext()) {
//            final JComponent element = (JComponent) iter.next();
//            element.setEnabled(enabled);
//        }
    }


    public static final ZFormPanel buildLabelField(final String libelle, final ZTextField.IZTextFieldModel model1) {
        return buildLabelField(libelle, model1, -1);
    }


    public static final ZFormPanel buildLabelField(final String libelle, final ZTextField.IZTextFieldModel model1, final int minWidth) {
        final ZTextField fNumeroMin = new ZTextField(model1);
        fNumeroMin.getMyTexfield().setColumns(5);
        return buildLabelField(libelle, fNumeroMin, minWidth);
    }


    public static final ZFormPanel buildLabelField(final String libelle, final ZTextField txtField) {
        return buildLabelField(libelle, txtField, -1);
    }


    public static final ZFormPanel buildLabelField(final String libelle, final ZTextField txtField, final int minWidth) {
        final ZTextField fNumeroMin = txtField;
        final ZFormPanel p = new ZFormPanel();
        final JLabel l = new JLabel(libelle);
        if (minWidth>-1) {
            l.setPreferredSize(new Dimension(minWidth,DEFAULT_LABEL_HEIGHT));
        }
//        l.setBorder(BorderFactory.createEmptyBorder(DEFAULT_MARGIN_LABEL.top, DEFAULT_MARGIN_LABEL.left, DEFAULT_MARGIN_LABEL.bottom, DEFAULT_MARGIN_LABEL.right ));

        p.add(l);
        p.add(fNumeroMin);
//        p.setBorder(BorderFactory.createLineBorder(bordure));
        return p;
    }

    public static final ZFormPanel buildLabelField(final String libelle, final Component field) {

        final ZFormPanel p = new ZFormPanel();
        final JLabel l = new JLabel(libelle);
        p.add(l);
        p.add(field);
//        p.setBorder(BorderFactory.createLineBorder(bordure));
        return p;
    }
    
    public static final ZFormPanel buildLabelField(final String libelle, final Component field, final int minWidth) {
        final ZFormPanel p = new ZFormPanel();
        final JLabel l = new JLabel(libelle);
        if (minWidth>-1) {
            l.setPreferredSize(new Dimension(minWidth,DEFAULT_LABEL_HEIGHT));
        }        
        p.add(l);
        p.add(field);
        return p;
    }
    
    
    /**
     * 
     * @param libelle
     * @param model
     * @param map Une map dans laquelle on va memoriser la selection
     * @param key La cle dans la map pour remplir la selection
     * @param bordure
     * @return
     */
    public static final ZFormPanel buildLabelComboBoxField(final String libelle, final ComboBoxModel model, final Map map, final String key) {
        return buildLabelComboBoxField(libelle, model, map, key, null);
    }
    
    public static final ZFormPanel buildLabelComboBoxField(final String libelle, final ComboBoxModel model, final Map map, final String key, final ActionListener actionListener) {
        final ZFormPanel p = new ZFormPanel();
        final JLabel l = new JLabel(libelle);
        p.add(l);
        JComboBox com = buildComboBox(model, map, key, actionListener);
        p.add(com);
        p.getMyFields().add(com);
        
        return p;
    }
    
    public static final JComboBox buildComboBox(final ComboBoxModel model, final Map map, final String key, final ActionListener actionListener) {
        final JComboBox comboBox = new JComboBox(model);
        
        final ActionListener listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (map != null && key != null) {
                    map.put(key, model.getSelectedItem());
                }
            }
        };
        comboBox.addActionListener(listener);
        if (actionListener != null) {
            comboBox.addActionListener(actionListener);
        }
       
        return comboBox;
    }
  


    /**
     * Affecte l'action a executer lorsque l'utilisateur appuie sur Entree lorsqu'il effectue une saisie dedans.
     * @param action
     */
    public void setDefaultAction(final Action action) {
        final MyActionListener al = new MyActionListener(action);
        for (int i = 0; i < myFields.size(); i++) {
            final ZTextField array_element = (ZTextField) myFields.get(i);
            array_element.getMyTexfield().addActionListener(al);
        }
    }




    private class MyActionListener implements ActionListener {
        private final Action action;
        /**
         *
         */
        public MyActionListener(Action a) {
            super();
            action = a;
        }
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(final ActionEvent e) {
            action.actionPerformed(e);
        }

    }

    /**
     * Construit un panel a partir d'un buttongroup
     * @param buttonGroup
     * @param libelle
     * @param labelWidth
     * @return
     */
    public static final ZFormPanel buildRadioButtons(final ButtonGroup buttonGroup, final String libelle, final int labelWidth) {
        final ZFormPanel p = new ZFormPanel();
        final JLabel l = new JLabel(libelle);
        if (labelWidth!=-1) {
            l.setPreferredSize(new Dimension(labelWidth, 1));
        }
        p.add(l);

        while (buttonGroup.getElements().hasMoreElements()) {
            final JButton element = (JButton) buttonGroup.getElements().nextElement();
            p.add(element);
        }
        return p;
    }







    /**
     * @param actions
     * @return Un buttonGroup cree a partir d'un tableau d'actions.
     */
    public static final ButtonGroup createButtonGroupFromActions(final Action[] actions) {
        final ButtonGroup res = new ButtonGroup();
        for (int i = 0; i < actions.length; i++) {
            final Action action = actions[i];
            res.add( new JRadioButton(  action )  );
        }
        return res;
    }

    



}
