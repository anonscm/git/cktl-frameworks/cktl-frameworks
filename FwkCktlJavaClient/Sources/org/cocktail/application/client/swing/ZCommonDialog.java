/*
 * Copyright (C) 2004 Universite de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.application.client.swing;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JDialog;
import javax.swing.JPanel;


/**
 * Classe abstraite dont doivent heriter les fenetres de l'application.
 *
 * @author Rodolphe Prin
 */
public class ZCommonDialog extends JDialog {
	public static final int MROK=1;
	public static final int MRCANCEL=0;
	protected int modalResult;
	private BusyGlassPanel myBusyGlassPanel;
	private Point lastPos;



	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZCommonDialog(Dialog owner, String title, boolean modal)
		throws HeadlessException {
		super(owner, title, modal);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		myBusyGlassPanel = new BusyGlassPanel();
		setGlassPane(myBusyGlassPanel);
	}



	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZCommonDialog(Frame owner, String title, boolean modal)
		throws HeadlessException {
		super(owner, title, modal);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		myBusyGlassPanel = new BusyGlassPanel();
		setGlassPane(myBusyGlassPanel);
	}


	/**
	 *
	 */
	public int getModalResult() {
		return modalResult;
	}

	/**
	 * @param i
	 */
	public void setModalResult(int i) {
		modalResult = i;
	}


	public void onOkClick() {
		setModalResult(MROK);
		hide();
	}


	public void onCancelClick() {
		setModalResult(MRCANCEL);
		hide();
	}

	public void onCloseClick() {
		setModalResult(MRCANCEL);
		hide();
	}



	public void setWaitCursor(boolean bool) {
		if (bool) {
		    this.getGlassPane().setVisible(true);
		}
		else {
		    this.getGlassPane().setVisible(false);
		}
	}

	public int  open()  {
		setModalResult(MRCANCEL);
		centerWindow();
		show();
		//en modal la fenetre reste active jusqu'au closeWindow...
		return getModalResult();
	}

	public final void centerWindow() {
		int screenWidth = (int)this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int)this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth/2)-((int)this.getSize().getWidth()/2), ((screenHeight/2)-((int)this.getSize().getHeight()/2)));

	}



	public final class BusyGlassPanel extends JPanel 	{
	    public final Color COLOR_WASH = new Color(64, 64, 64, 32);
	    public BusyGlassPanel() {
	        super.setOpaque(false);
	        super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	        super.addKeyListener( (new KeyAdapter() { }) );
	        super.addMouseListener( (new MouseAdapter() { }) );
	        super.addMouseMotionListener( (new MouseMotionAdapter() { }) );
	    }



	    public final void paintComponent(Graphics p_graphics) {
	        Dimension l_size = super.getSize();

	        p_graphics.setColor(COLOR_WASH);
	        p_graphics.fillRect(0, 0, l_size.width, l_size.height);

	        p_graphics.setColor(Color.white);
	        for (int j=3; j<l_size.height; j+=8) {
	            for (int i=3; i<l_size.width; i+=8) {
	                p_graphics.fillRect(i,j,1,1);
	            }
	        }
	        p_graphics.setColor(Color.black);
	        for (int j=4; j<l_size.height; j+=8) {
	            for (int i=4; i<l_size.width; i+=8) {
	                p_graphics.fillRect(i,j,1,1);
	            }
	        }
	    }
	}
    
    
    public void maximize() {
        lastPos = getLocation();
        this.hide();
        this.setLocation(0,0);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screenSize.width, screenSize.height);
        this.show();    
    }

    
    public void normal() {
        if (lastPos != null) {
            this.setLocation(lastPos); 
        }
        this.hide();
        this.setLocation(0,0);
        this.setSize(getContentPane().getPreferredSize());
        this.show();    
    }
    

}
