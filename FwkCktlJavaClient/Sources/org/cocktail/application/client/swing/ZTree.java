/*
 * Copyright (C) 2004 Universite de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.application.client.swing;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 * 
 * 
 * @author Rodolphe Prin
 */
public class ZTree extends JTree {

	/**
	 * 
	 */
	public ZTree() {
		super();
	}

	/**
	 * @param value
	 */
	public ZTree(Object[] value) {
		super(value);
	}

	/**
	 * @param value
	 */
	public ZTree(Hashtable value) {
		super(value);
	}

	/**
	 * @param value
	 */
	public ZTree(Vector value) {
		super(value);
	}

	/**
	 * @param newModel
	 */
	public ZTree(TreeModel newModel) {
		super(newModel);
	}

	/**
	 * @param root
	 */
	public ZTree(TreeNode root) {
		super(root);
	}

	/**
	 * @param root
	 * @param asksAllowsChildren
	 */
	public ZTree(TreeNode root, boolean asksAllowsChildren) {
		super(root, asksAllowsChildren);
	}
	
	
	public void enableToolTips(boolean allow) {
		if (allow) {
			ToolTipManager.sharedInstance().registerComponent(this);
		}
		else {
			ToolTipManager.sharedInstance().unregisterComponent(this);	
		}
		
	}
	
	
	
	public void expandAll(boolean expand) {
		ZTreeNode root = (ZTreeNode)getModel().getRoot();
    
		// Traverse tree from root
		expandAll(new TreePath(root), expand);
	}
	
	private void expandAll(TreePath parent, boolean expand) {
		ZTreeNode node = (ZTreeNode)parent.getLastPathComponent();
		if (node.getChildCount() >= 0) {
			ArrayList tmpList=node.getMyChilds();
			if (tmpList!=null) {
				for (Iterator e=tmpList.iterator() ; e.hasNext(); ) {
					ZTreeNode n = (ZTreeNode)e.next();
					TreePath path = parent.pathByAddingChild(n);
					expandAll(path, expand);
				}
			}
		}
    
		
		if (expand) {
			expandPath(parent);
		} else {
			collapsePath(parent);
		}
	}
	
	

	public void expandAllObjectsAtLevel(final int level, final boolean expand ) {
		TreeNode root = (TreeNode)getModel().getRoot();
		// Traverse tree from root
		expandAllObjectsAtLevel(new TreePath(root), level, expand, 0);		
	}
	
	private void expandAllObjectsAtLevel(TreePath parent, final int level,final boolean expand, final int currentLevel) {
		TreeNode node = (TreeNode)parent.getLastPathComponent();
		if (currentLevel<level) {
			if (node.getChildCount() >= 0) {
                Enumeration enumeration = node.children();
                while (enumeration.hasMoreElements()) {
                    TreeNode element = (TreeNode) enumeration.nextElement();
                    TreePath path = parent.pathByAddingChild(element);
                    expandAllObjectsAtLevel(path, level, expand,currentLevel+1);
                }
//                
//                
//				ArrayList tmpList=node. ;
//				if (tmpList!=null) {
//					for (Iterator e=tmpList.iterator() ; e.hasNext(); ) {
//						ZTreeNode n = (ZTreeNode)e.next();
//						TreePath path = parent.pathByAddingChild(n);
//						expandAllObjectsAtLevel(path, level, expand,currentLevel+1);
//					}
//				}
			}
		}
    
		if (currentLevel==level  ) {
			if (expand) {
				expandPath(parent);
			} else {
				collapsePath(parent);
			}
		}
	}		
	
//	
//	public void expandAllObjectsWithClass(Class theClass, boolean expand ) {
//		TreeNode root = (TreeNode)getModel().getRoot();
//		// Traverse tree from root
//		expandAllObjectsWithClass(new TreePath(root), theClass, expand);		
//	}
//    
    
    
//	
//	private void expandAllObjectsWithClass(TreePath parent, Class theClass ,boolean expand) {
//		TreeNode node = (TreeNode)parent.getLastPathComponent();
//		if (node.getChildCount() >= 0) {
//			ArrayList tmpList=node.getMyChilds();
//			if (tmpList!=null) {
//				for (Iterator e=tmpList.iterator() ; e.hasNext(); ) {
//					TreeNode n = (TreeNode)e.next();
//					TreePath path = parent.pathByAddingChild(n);
//					expandAllObjectsWithClass(path, theClass, expand);
//				}
//			}
//		}
//    
//		if (node.getMyObject().getClass().equals(theClass)  ) {
//			if (expand) {
//				expandPath(parent);
//			} else {
//				collapsePath(parent);
//			}
//		}
//	}	
//	
//	
	

}
