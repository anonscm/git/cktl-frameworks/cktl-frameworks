/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

/***********************************************************

 This file is part of JavaDatePicker.

 JavaDatePicker is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser
 General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your
 option) any later version.

 JavaDatePicker is distributed in the hope that it will
 be useful, but WITHOUT ANY WARRANTY; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.  See the GNU Lesser General Public
 License for more details.

 You should have received a copy of the GNU Lesser
 General Public License along with JavaDatePicker; if
 not, write to the Free Software Foundation, Inc., 59
 Temple Place, Suite 330, Boston, MA  02111-1307  USA


***********************************************************/

package org.cocktail.application.client.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.plaf.BorderUIResource;



/**
 * GUI component that allows the user to choose a date from a calendar.
 * Usage is illustrated in the sample code below:
 * <PRE>
 *  JDialog dlg = new JDialog(new Frame(), true);
 *  DatePicker dp = new DatePicker();
 *  dp.setHideOnSelect(false);
 *  dlg.getContentPane().add(dp);
 *  dlg.pack();
 *  dlg.show();
 *  System.out.println(dp.getDate().toString());
 *  dlg.dispose();
 *  System.exit(0);
 * </PRE>
 * @author B. Bell
 * @version 1.1a
 */
public final class ZDatePickerPanel extends ZAbstractPanel {

	private int[] dows = new int[] {Calendar.SUNDAY, Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY};
	//private String[] dowsLabel = new String[] {"D","L","M","M","J","V","S"};
	
	private Color DEFAULTBGCOLOR = Color.decode("#FFFFFF");
	private Color DEFAULTHEADINGBGCOLOR = Color.decode("#808080");
	private Color DEFAULTHEADINGCOLOR = Color.decode("#D4D0C8");
//	private Color DEFAULSELECTIONCOLOR = Color.decode("#FFFFFF");
	private Color DEFAULSELECTIONBGCOLOR = Color.decode("#0A246A");
//	private boolean DEFAUTSHOWWEEKNUMBER = true;
	
//	private boolean showWeekNumber=DEFAUTSHOWWEEKNUMBER;	
	
	
	
	
	
    /**
     * X coordinate for upper left corner of week 1 day 1.
     */
    private static final int startX = 10;

    /**
     * Y coordinate for upper left corner of week 1 day 1.
     */
    private static final int startY = 60;

    /**
     * Small font.
     */
    private static final Font smallFont = new Font("Dialog", Font.PLAIN, 10);

    /**
     * Large font.
     */
    private static final Font largeFont = new Font("Dialog", Font.PLAIN, 12);

    /**
     * Insets for day components and small buttons.
     */
    private static final Insets insets = new Insets(2, 2, 2, 2);

    /**
     * Highlighted color.
     */
    private static final Color highlight = new Color(255, 255, 204);

    /**
     * Enabled color.
     */
    private static final Color white = new Color(255, 255, 255);

    /**
     * Disabled color.
     */
//    private static final Color gray = new Color(204, 204, 204);

    /**
     * Most recently selected day component.
     */
    private Component selectedDay = null;

    /**
     * Currently selected date.
     */
    private GregorianCalendar selectedDate = null;

    /**
     * Tracks the original date set when the component was created.
     */
    private GregorianCalendar originalDate = null;

    /**
     * When true, the panel will be hidden as soon as a day is
     * selected by clicking the day or clicking the Today button.
     */
    private boolean hideOnSelect = true;

    /**
     * When clicked, displays the previous month.
     */
    private final JButton backButton = new JButton();

    /**
     * Displays the currently selected month and year.
     */
    private final JLabel monthAndYear = new JLabel();

    /**
     * When clicked, displays the next month.
     */
    private final JButton forwardButton = new JButton();

    /**
     * Column headings for the days of the week.
     */
    private final JTextField[] dayHeadings = new JTextField[]{        
        new JTextField("L"),
        new JTextField("M"),
        new JTextField("M"),
        new JTextField("J"),
        new JTextField("V"),
        new JTextField("S"),
		new JTextField("D")        
        };



	private final JTextField[] weekNumbers = new JTextField[]{        
		new JTextField(""),
		new JTextField(""),
		new JTextField(""),
		new JTextField(""),
		new JTextField(""),
		new JTextField("")        
	}; 


    /**
     * 2-dimensional array for 6 weeks of 7 days each.
     */
    private final JTextField[][] daysInMonth = new JTextField[][]{
        {new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField()},
        {new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField()},
        {new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField()},
        {new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField()},
        {new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField()},
        {new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField(),
         new JTextField()}
    };

    /**
     * When clicked, sets the selected day to the current date.
     */
    private final JButton todayButton = new JButton();

    /**
     * When clicked, hides the calendar and sets the selected date to "empty".
     */
    private final JButton cancelButton = new JButton();

	public ZDatePickerPanel() {
		super();		
		//selectedDate = getToday();
		init();
	}

//    /**
//     * Alternate constructor that sets the currently selected date to the
//     * specified date if non-null.
//     * @param initialDate
//     */
//    public ZDatePickerPanel(final Date initialDate) {
//        super();
//        init();
//    }

    /**
     * Returns true if the panel will be made invisible after a day is
     * selected.
     * @return true or false
     */
    public boolean isHideOnSelect() {
        return hideOnSelect;
    }

    /**
     * Controls whether the panel will be made invisible after a day is
     * selected.
     * @param hideOnSelect
     */
    public void setHideOnSelect(final boolean hideOnSelect) {
        if (this.hideOnSelect != hideOnSelect) {
            this.hideOnSelect = hideOnSelect;
            initButtons(false);
        }
    }

    /**
     * Returns the currently selected date.
     * @return date
     */
    public Date getDate() {
        if (null != selectedDate)
            return selectedDate.getTime();
        return null;
    }

    /**
     * Initializes the panel components according to the current value
     * of selectedDate.
     */
    private void init() {
//        setLayout(new AbsoluteLayout());
//		new BoxLayout(panel,BoxLayout.Y_AXIS)
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        this.setMinimumSize(new Dimension(161, 226));
        this.setMaximumSize(getMinimumSize());
        this.setPreferredSize(getMinimumSize());
        this.setBorder(new BorderUIResource.EtchedBorderUIResource());


		//Entete
		Box boxTop = Box.createHorizontalBox();
		
        final Dimension btSize = new Dimension(20,20);
        
        backButton.setFont(smallFont);
		backButton.setFocusable(false);
        backButton.setText("<");
        backButton.setMargin(insets);
        backButton.setDefaultCapable(false);
        backButton.setMaximumSize(btSize);
        backButton.setMinimumSize(btSize);
        backButton.setPreferredSize(btSize);
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                onBackClicked(evt);
            }
        });
//        add(backButton, new AbsoluteConstraints(10, 10, 20, 20));
		boxTop.add(backButton);
		boxTop.add(Box.createGlue());

        monthAndYear.setFont(largeFont);
        monthAndYear.setHorizontalAlignment(JTextField.CENTER);
        monthAndYear.setText("");
//        add(monthAndYear, new AbsoluteConstraints(30, 10, 100, 20));
		boxTop.add(monthAndYear);

		boxTop.add(Box.createGlue());

        forwardButton.setFont(smallFont);
		forwardButton.setFocusable(false);
        forwardButton.setText(">");
        forwardButton.setMargin(insets);
        forwardButton.setDefaultCapable(false);
        forwardButton.setMaximumSize(btSize);
        forwardButton.setMinimumSize(btSize);
        forwardButton.setPreferredSize(btSize);        
        forwardButton.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                onForwardClicked(evt);
            }
        });
//        add(forwardButton, new AbsoluteConstraints(130, 10, 20, 20));
		boxTop.add(forwardButton);
		this.add(boxTop);

		
//		Corps du calendrier
		Box calendrierBox = Box.createHorizontalBox();
		Box weekNumberBox = Box.createVerticalBox();
		JTextField panel = new JTextField();
		panel.setBackground(DEFAULTHEADINGBGCOLOR);
		panel.setPreferredSize(new Dimension(16,20));
		panel.setFocusable(false);
		panel.setEditable(false);
		panel.setFont(smallFont);
		weekNumberBox.add(panel);
		for (int i = 0; i < weekNumbers.length; i++) {
			weekNumbers[i].setPreferredSize(new Dimension(16,20));
			weekNumbers[i].setBackground(DEFAULTHEADINGBGCOLOR);
			weekNumbers[i].setForeground(DEFAULTHEADINGCOLOR);
			weekNumbers[i].setHorizontalAlignment(JTextField.CENTER);
			weekNumbers[i].setFocusable(false);
			weekNumbers[i].setEditable(false);
			weekNumbers[i].setFont(smallFont);
			weekNumbers[i].setBorder(BorderFactory.createEmptyBorder());
//			weekNumbers[i].setBorder(BorderFactory.createLineBorder(gray) );
			weekNumberBox.add(weekNumbers[i]);
		}
		
		
		
		Box boxBody = Box.createVerticalBox();
		boxBody.setBackground(DEFAULTBGCOLOR);
		
		Box boxHeadings = Box.createHorizontalBox();
//		boxHeadings.setBackground(DEFAULTHEADINGBGCOLOR);
//		boxHeadings.setForeground(DEFAULTHEADINGCOLOR);
        // layout the column headings for the days of the week
        int x = startX;
        for (int ii = 0; ii < dayHeadings.length; ii++) {
            dayHeadings[ii].setBackground(DEFAULTHEADINGBGCOLOR);
			dayHeadings[ii].setForeground(DEFAULTHEADINGCOLOR);
			dayHeadings[ii].setBorder(BorderFactory.createEmptyBorder());
            dayHeadings[ii].setEditable(false);
            dayHeadings[ii].setFont(smallFont);
            dayHeadings[ii].setHorizontalAlignment(JTextField.CENTER);
            dayHeadings[ii].setFocusable(false);
			dayHeadings[ii].setPreferredSize(new Dimension(20,20));
//            add(dayHeadings[ii], new AbsoluteConstraints(x, 40, 21, 21));
			boxHeadings.add(dayHeadings[ii]);
            x += 20;
        }
		boxBody.add(boxHeadings);


        // layout the days of the month
        x = startX;
        int y = startY;
        for (int ii = 0; ii < daysInMonth.length; ii++) {
        	Box boxLine = Box.createHorizontalBox();
            for (int jj = 0; jj < daysInMonth[ii].length; jj++) {
                daysInMonth[ii][jj].setBackground(DEFAULTBGCOLOR);
                daysInMonth[ii][jj].setEditable(false);
                daysInMonth[ii][jj].setFont(smallFont);
                daysInMonth[ii][jj].setHorizontalAlignment(JTextField.CENTER);
                daysInMonth[ii][jj].setText("");
                daysInMonth[ii][jj].setFocusable(false);
				daysInMonth[ii][jj].setPreferredSize(new Dimension(20,20));
				daysInMonth[ii][jj].setBorder(BorderFactory.createEmptyBorder());
				                
                daysInMonth[ii][jj].addMouseListener(new MouseAdapter() {
                    public void mouseClicked(final MouseEvent evt) {
                        onDayClicked(evt);
                    }
                });
//                add(daysInMonth[ii][jj], new AbsoluteConstraints(x, y, 21, 21));
				boxLine.add(daysInMonth[ii][jj]);
                x += 20;
            }
			boxBody.add(boxLine);
            x = startX;
            y += 20;
        }

		calendrierBox.add(weekNumberBox);
		calendrierBox.add(boxBody);
		add(calendrierBox);
        add(initButtons(true));
        
    }


	/**
	 * Met a jour le calendrier
	 * 
	 * @param initialDate
	 */
	public void updateForDate(Date initialDate) {
		if (null == initialDate) {
			selectedDate = getToday();			
		}
		else {
			(selectedDate = new GregorianCalendar()).setTime(initialDate);
		}
		originalDate = new GregorianCalendar(selectedDate.get(Calendar.YEAR),selectedDate.get(Calendar.MONTH),selectedDate.get(Calendar.DATE));		
		calculateCalendar();
	}


	public void open(Date ladate) {
//		System.out.println("date selectionnee : "+ladate);
		updateForDate(ladate);
	}




    /**
     * Initializes Today and Cancel buttons dependent on whether hideOnSelect
     * is set; if the panel will stay open, the Cancel button is invisible.
     * @param firstTime
     */
    private Box initButtons(final boolean firstTime) {
    	Box boxButtons = Box.createHorizontalBox();
        if (firstTime) {
            final Dimension buttonSize = new Dimension(68, 24);
            todayButton.setFont(smallFont);
            todayButton.setText("Aujourd'hui");
			todayButton.setFocusable(false);            
            todayButton.setMargin(insets);
            todayButton.setMaximumSize(buttonSize);
            todayButton.setMinimumSize(buttonSize);
            todayButton.setPreferredSize(buttonSize);
            todayButton.setDefaultCapable(true);
            todayButton.setSelected(true);
            todayButton.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent evt) {
                    onToday(evt);
                }
            });

            cancelButton.setFont(smallFont);
            cancelButton.setText("Annuler");
            cancelButton.setFocusable(false);
            cancelButton.setMargin(insets);
            cancelButton.setMaximumSize(buttonSize);
            cancelButton.setMinimumSize(buttonSize);
            cancelButton.setPreferredSize(buttonSize);
            cancelButton.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent evt) {
                    onCancel(evt);
                }
            });
        } else {
            this.remove(todayButton);
            this.remove(cancelButton);
        }

        if (hideOnSelect) {
			boxButtons.add(todayButton);
            boxButtons.add(cancelButton);
        } else {
			boxButtons.add(todayButton);
            
        }
        return boxButtons;
    }

    /**
     * Event handler for the Today button that sets the currently selected
     * date to the current date.
     * @param evt
     */
    private void onToday(final java.awt.event.ActionEvent evt) {
        selectedDate = getToday();
        setVisible(!hideOnSelect);
        if (isVisible()) { // don't bother with calculation if not visible
            monthAndYear.setText(formatDateText(selectedDate.getTime()));
            calculateCalendar();
        }
    }

    /**
     * Event handler for the Cancel button that unsets the currently selected date.
     * @param evt
     */
    private void onCancel(final ActionEvent evt) {
//        selectedDate = originalDate;
		selectedDate = null;
        setVisible(!hideOnSelect);
    }

    /**
     * Event handler for the forward button that increments the currently
     * selected month.
     * @param evt
     */
    private void onForwardClicked(final java.awt.event.ActionEvent evt) {
        final int day = selectedDate.get(Calendar.DATE);
        selectedDate.set(Calendar.DATE, 1);
        selectedDate.add(Calendar.MONTH, 1);
        selectedDate.set(Calendar.DATE,Math.min(day, calculateDaysInMonth(selectedDate)));
//        monthAndYear.setText(formatDateText(selectedDate.getTime()));
        calculateCalendar();
    }

    /**
     * Event handler for the back button that decrements the currently selected
     * month.
     * @param evt
     */
    private void onBackClicked(final java.awt.event.ActionEvent evt) {
        final int day = selectedDate.get(Calendar.DATE);
        selectedDate.set(Calendar.DATE, 1);
        selectedDate.add(Calendar.MONTH, -1);
        selectedDate.set(Calendar.DATE,Math.min(day, calculateDaysInMonth(selectedDate)));
//        monthAndYear.setText(formatDateText(selectedDate.getTime()));
        calculateCalendar();
    }

    /**
     * Event handler that sets the currently selected date to the clicked day.
     * @param evt
     */
    private void onDayClicked(final java.awt.event.MouseEvent evt) {
        final javax.swing.JTextField fld = (javax.swing.JTextField) evt.getSource();
        if (!"".equals(fld.getText())) {
            if (null != selectedDay) {
                selectedDay.setBackground(white);
            }
            fld.setBackground(highlight);
            selectedDay = fld;
            selectedDate.set(Calendar.DATE,Integer.parseInt(fld.getText()));
            setVisible(!hideOnSelect);
        }
    }

    /**
     * Returns the current date.
     * @return date
     */
    private static GregorianCalendar getToday() {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        return gc;
    }

    /**
     * Calculates the days of the month.
     */
    private void calculateCalendar() {
        // clear the selected date
        if (null != selectedDay) {
            selectedDay.setBackground(white);
            selectedDay = null;
        }
        
//        System.out.println("selected date = "+selectedDate);
//        System.out.println("selected date week = "+selectedDate.get(Calendar.WEEK_OF_MONTH));
//        System.out.println("selected date week = "+selectedDate.get(GregorianCalendar.WEEK_OF_MONTH));

		monthAndYear.setText(formatDateText(selectedDate.getTime()));

        // get the first day of the selected year and month
        final GregorianCalendar c = new GregorianCalendar(
                selectedDate.get(Calendar.YEAR),
                selectedDate.get(Calendar.MONTH),
                1);
//        System.out.println("selected date week = "+c.get(Calendar.WEEK_OF_MONTH));
//        System.out.println("selected date week = "+c.get(GregorianCalendar.WEEK_OF_MONTH));
		

		//Ordre des jours de la semaine
		int fdow = c.getFirstDayOfWeek();
		int[] orderedDows = new int[7];
		int[] orderedDows2 = new int[7];
		int i;
		for (i = 0; i < fdow-1; i++) {
			orderedDows[6-i] = dows[i];
		}
		for (int j = 0; j <= 6-i; j++) {
			orderedDows[j] = dows[fdow+j-1];
		}
		for (int j = 0; j < orderedDows.length; j++) {
			orderedDows2[orderedDows[j]-1] = j; 
		}
		

        // figure out the maximum number of days in the month
        final int maxDay = calculateDaysInMonth(c);

        // figure out the day that should be selected in this month
        // based on the previously selected day and the maximum number
        // of days in the month
        final int selectedDay = Math.min(maxDay, selectedDate.get(Calendar.DAY_OF_MONTH));


		//Rï¿½initialiser toutes les cases
		for (int j = 0; j < daysInMonth.length; j++) {
			for (int k = 0; k < daysInMonth[j].length; k++) {
				daysInMonth[j][k].setText("");
				daysInMonth[j][k].setBorder( BorderFactory.createEmptyBorder() );
			}
		}
		for (int k = 0; k < weekNumbers.length; k++) {
			weekNumbers[k].setText("");
		}		


        // corrections en fonction des versions de java (en 1.5, 1ere semaine=0)
        int correction=0;
        
        if (c.get(Calendar.WEEK_OF_MONTH)==0) {
            correction = 1;
        }
        
		int dow;
        // construct the days in the selected month
        int week;
        do {
            
            
            week = c.get(Calendar.WEEK_OF_MONTH) + correction;
            dow = c.get(Calendar.DAY_OF_WEEK);
            
            final JTextField fld = this.daysInMonth[week-1][orderedDows2[dow -1]];
            fld.setText(Integer.toString(c.get(Calendar.DAY_OF_MONTH)));
            weekNumbers[week-1].setText( String.valueOf(  c.get(Calendar.WEEK_OF_YEAR  ) ));
            if (selectedDay == c.get(Calendar.DATE)) {
                fld.setBorder( BorderFactory.createLineBorder(DEFAULSELECTIONBGCOLOR) );
                this.selectedDay = fld;
            } 
            if (c.get(Calendar.DATE) >= maxDay)
                break;
            c.add(Calendar.DATE, 1);
        } while (c.get(Calendar.DATE) <= maxDay);



        // set the currently selected date
        c.set(Calendar.DATE, selectedDay);
        selectedDate = c;
    }

    /**
     * Calculates the number of days in the specified month.
     * @param c
     * @return number of days in the month
     */
    private static int calculateDaysInMonth(final Calendar c) {
        int daysInMonth = 0;
        switch (c.get(Calendar.MONTH)) {
            case 0:
            case 2:
            case 4:
            case 6:
            case 7:
            case 9:
            case 11:
                daysInMonth = 31;
                break;
            case 3:
            case 5:
            case 8:
            case 10:
                daysInMonth = 30;
                break;
            case 1:
                final int year = c.get(Calendar.YEAR);
                daysInMonth =
                        (0 == year % 1000) ? 29 :
                        (0 == year % 100) ? 28 :
                        (0 == year % 4) ? 29 : 28;
                break;
        }
        return daysInMonth;
    }

    /**
     * Returns a short string representation of the specified date (January, 2001).
     * @param dt
     * @return short string
     */
    private static String formatDateText(final Date dt) {
        final DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);

        final StringBuffer mm = new StringBuffer();
        final StringBuffer yy = new StringBuffer();
        final FieldPosition mmfp = new FieldPosition(DateFormat.MONTH_FIELD);
        final FieldPosition yyfp = new FieldPosition(DateFormat.YEAR_FIELD);
        df.format(dt, mm, mmfp);
        df.format(dt, yy, yyfp);
        return (mm.toString().substring(mmfp.getBeginIndex(), mmfp.getEndIndex()) +
                " " + yy.toString().substring(yyfp.getBeginIndex(), yyfp.getEndIndex()));
    }

    public void updateData() throws Exception {
        
    }


	
	

	

}
