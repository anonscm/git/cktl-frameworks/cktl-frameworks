/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.application.client.swing;

import java.awt.Color;
import java.awt.LayoutManager;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Classe abstraite pour les panel simples.
 * @author rodolphe.prin@univ-lr.fr
 */
public abstract class ZAbstractPanel extends JPanel implements IZPanel, IZDataComponent {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6460034635840330919L;


	/**
     * @param isDoubleBuffered
     */
    public ZAbstractPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
    }

    /**
     * @param layout
     * @param isDoubleBuffered
     */
    public ZAbstractPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
    }

    /**
     * 
     */
    public ZAbstractPanel() {
        super();
    }

    /**
     * @param layout
     */
    public ZAbstractPanel(LayoutManager layout) {
        super(layout);
    }

    /**
     * Cette methode est appelee apres la creation de la fenetre contenant le panel, donc avant son affichage. 
     * A utiliser pour initaliser eventuellement des composants qui necessitent une reference à une fenetre initialisee.
     * Par defaut elle ne fait rien. 
     *
     */
    public void onBeforeShow() {
        
    }
//    
    
    /**
     * Cette methode permet de rafraichir les infos affichees sur le panel. A surcharger.
     *
     */
    public void updateData() throws Exception {
        
    }


    public static void setDebugBorder(JComponent comp , Color col ) {
        comp.setBorder(BorderFactory.createLineBorder(col));    
    }
    
    
}
