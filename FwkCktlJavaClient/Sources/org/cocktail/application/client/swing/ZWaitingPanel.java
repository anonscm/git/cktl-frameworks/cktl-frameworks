package org.cocktail.application.client.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;



/*
 * Copyright (C) 2004 Universite de La Rochelle
 *
 * This file is part of TestJava.
 *
 * TestJava is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TestJava is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 *
 *
 * @author Rodolphe Prin
 */
public class ZWaitingPanel extends JPanel {
	private String globalTaskDescription;
	private String currentTaskDescription;

	private JProgressBar myProgressBar;
	private JLabel globalTaskLabel;
	private JLabel currentTaskLabel;

	private ImageIcon icon;

	private ZWaitingPanelListener myListener;

	/**
	 *
	 */
	public ZWaitingPanel(ZWaitingPanelListener listener ) {
		super();
		myListener = listener;
		initComponent();

	}
	public ZWaitingPanel(ZWaitingPanelListener listener, ImageIcon img) {
		super();
		myListener = listener;
		icon = img;
		initComponent();
	}

	private void initComponent() {
		myProgressBar = new JProgressBar(0, 100);
		myProgressBar.setValue(0);
		myProgressBar.setStringPainted(true);
		myProgressBar.setString("");
		myProgressBar.setIndeterminate(true);
		myProgressBar.setBorder(BorderFactory.createEmptyBorder());

		globalTaskLabel = new JLabel();
		globalTaskLabel.setAlignmentX(  (float) 0.5  );
		currentTaskLabel = new JLabel();
		currentTaskLabel.setAlignmentX(  (float) 0.5  );

		Box globalTaskBlock = Box.createHorizontalBox();
		globalTaskBlock.add(Box.createGlue());
		globalTaskBlock.add(globalTaskLabel);
		globalTaskBlock.add(Box.createGlue());

		Box currentTaskBlock = Box.createHorizontalBox();
		currentTaskBlock.add(Box.createGlue());
		currentTaskBlock.add(currentTaskLabel);
		currentTaskBlock.add(Box.createGlue());

//		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		Box mainBox = Box.createHorizontalBox();
		mainBox.add(Box.createRigidArea(new Dimension(2,100)));
		Box contentBox = Box.createVerticalBox();
		contentBox.add(Box.createRigidArea(new Dimension(400,2)));
		contentBox.add(globalTaskBlock);
		contentBox.add(myProgressBar);
		contentBox.add(currentTaskBlock);
		contentBox.add(Box.createRigidArea(new Dimension(400,2)));
		mainBox.add(contentBox);
		mainBox.add(Box.createRigidArea(new Dimension(2,100)));

		setLayout(new BorderLayout());
		if (icon != null) {
		    JLabel labelIcon = new JLabel(icon);
		    //Permet d'afficher les gif animes
		    icon.setImageObserver(labelIcon);
		    JPanel panelLeft = new JPanel(new BorderLayout());
		    panelLeft.setBorder(BorderFactory.createEmptyBorder(4,5,4,10));
		    panelLeft.add(labelIcon, BorderLayout.CENTER);
		    add(panelLeft, BorderLayout.WEST);
		}

		if (myListener != null && myListener.cancelAction() != null) {
		    final ArrayList list = new ArrayList();
		    list.add(myListener.cancelAction());
		    ArrayList buttons  = ZUiUtil.getButtonListFromActionList(list);

		    JPanel p = new JPanel(new BorderLayout());
		    p.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		    p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		    p.add(ZUiUtil.buildBoxLine(buttons), BorderLayout.EAST );
		    add(p, BorderLayout.SOUTH);
		}

		this.add(mainBox,BorderLayout.CENTER);
	}





	private void refresh() {
		globalTaskLabel.setText(globalTaskDescription);
		currentTaskLabel.setText(currentTaskDescription);
//		paintAll(this.getGraphics());
	}


	/**
	 * @param string
	 */
	public void setCurrentTaskDescription(String string) {
		currentTaskDescription = string;
		refresh();

	}

	/**
	 * @param string
	 */
	public void setGlobalTaskDescription(String string) {
		globalTaskDescription = string;
		refresh();
	}




    public JProgressBar getMyProgressBar() {
        return myProgressBar;
    }
    public ImageIcon getIcon() {
        return icon;
    }
    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }

    public interface ZWaitingPanelListener {
        public Action cancelAction();
    }
}
