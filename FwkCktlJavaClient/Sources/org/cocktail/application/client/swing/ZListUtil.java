/*
 * Copyright CRI - Universite de La Rochelle, 1995-2004
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swing;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;

import com.webobjects.foundation.NSDictionary;

/**
 * Des methodes utilitaires pour la gestion des listes.
 * @author rodolphe.prin@univ-lr.fr
 */
public abstract class ZListUtil {

    /**
     *
     * @param map
     * @param obj
     * @return Toutes les cles d'une HashMap qui sont associees a la valeur obj.
     */
    public static final ArrayList getKeyListForValue(final HashMap map, final Object obj) {
        ArrayList list = new ArrayList();
        Iterator iter = map.keySet().iterator();
        while (iter.hasNext()) {
            final Object element = iter.next();
            if (map.get(element).equals(obj)) {
                list.add(element);
            }
        }
        return list;
    }

    public static final HashMap convertNSDictionaryToHashMap(final NSDictionary d) {
        HashMap m = new HashMap(d.count());
        Enumeration enumeration = d.keyEnumerator();
        while (enumeration.hasMoreElements()) {
            final Object element = enumeration.nextElement();
            m.put(element, d.objectForKey(element));
        }
        return m;
    }




}
