/*
 * Copyright (C) 2004 Universite de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.application.client.swing;

import java.util.ArrayList;

/**
 * Classe proxy entre des donnees et un arbre.  
 * 
 * @author Rodolphe Prin
 */
public class ZTreeNode  {
	private ArrayList myChilds;
	private String title;
	private Object myObject;
	private int childCount;
	public ZTreeNode(String s, Object obj) {
		title = s;
		myObject = obj;
//		System.out.println("Nouveau noeud = "+s);
	}
	
	public void addChild( ZTreeNode obj ) {
		if (obj!=null) {
			if (myChilds==null) {
				myChilds = new ArrayList(1);
			}
			childCount++;
			myChilds.ensureCapacity(childCount);
			myChilds.add(obj);
		}
	}
	
	public ZTreeNode getChild(int index) {
		return (ZTreeNode)myChilds.get(index);
	}
	
	public Object getChildObject(int index) {
		return ((ZTreeNode)myChilds.get(index)).getMyObject() ;
	}
	
	public Object getChildTitle(int index) {
		return ((ZTreeNode)myChilds.get(index)).getTitle() ;
	}
	
	public int getIndexOfChild(ZTreeNode child) {
		return myChilds.indexOf(child);
	}		
	
	
	public boolean isLeaf() {
		return childCount==0;
	}

	/**
	 * @return
	 */
	public Object getMyObject() {
		return myObject;
	}


	/**
	 * @param object
	 */
	public void setMyObject(Object object) {
		myObject = object;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	public String toString() {
		return getTitle();
	}

	/**
	 * @return
	 */
	public int getChildCount() {
		return childCount;
	}

	/**
	 * @return
	 */
	public ArrayList getMyChilds() {
		return myChilds;
	}

}
