/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

import org.cocktail.application.client.TransactionPanel.TransactionLogNib;
import org.cocktail.application.client.TransactionPanel.TransactionLogNibCtrl;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeApplication;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.finder.FinderGrhum;
import org.cocktail.application.client.login.LoginNib;
import org.cocktail.application.client.login.LoginNibCtrl;
import org.cocktail.application.client.login.SplashWindow;
import org.cocktail.application.client.nibfinder.AProposDe;
import org.cocktail.application.client.tools.Logs;
import org.cocktail.application.client.tools.ToolsCocktailEOF;
import org.cocktail.application.client.tools.ToolsCocktailGrhum;
import org.cocktail.application.client.tools.ToolsCocktailJefyAdminPrivileges;
import org.cocktail.application.client.tools.ToolsCocktailLogs;
import org.cocktail.application.client.tools.ToolsCocktailObjectMessage;
import org.cocktail.application.client.tools.ToolsCocktailPrivileges;
import org.cocktail.application.client.tools.ToolsCocktailReports;
import org.cocktail.application.client.tools.ToolsCocktailSystem;
import org.cocktail.application.client.tools.ToolsExcel;
import org.cocktail.application.client.tools.ToolsFinder;
import org.cocktail.application.client.tools.UserInfoCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionAppFromJar;

import com.webobjects.eoapplication.EOAction;
import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;

public abstract class ApplicationCocktail extends EOApplication implements InterfaceApplicationCocktail {

	// Divers parameters d'applications
	public static final int DEFAULT_IMAGE_SIZE = 4096; // Taille du parametre chunkSize pour FileInputStream : 4Ko
	public static final int DEFAULT_DIRECTORY_SIZE = 65536; // Taille du parametre chunkSize pour FileInputStream : 64Ko
	public static final int DEFAULT_FILE_SIZE = 65536; // Taille du parametre chunkSize pour FileInputStream : 64Ko
	public static final int LOCATION_NORTH = 0;
	public static final int LOCATION_SOUTH = 1;
	public static final int LOCATION_EAST = 2;
	public static final int LOCATION_WEST = 3;
	public static final int LOCATION_NORTH_EAST = 4;
	public static final int LOCATION_NORTH_WEST = 5;
	public static final int LOCATION_SOUTH_EAST = 6;
	public static final int LOCATION_SOUTH_WEST = 7;
	public static final int LOCATION_MIDDLE = 8;

	private static final String A_DEFINIR = "A_DEFINIR";

	protected EOEditingContext appEditingContext = null;
	protected NSDictionary parametresApplication = null;
	protected LoginNibCtrl monLoginNibCtrl = null;
	protected NSMutableDictionary mesTools = null;
	protected EOUtilisateur currentUtilisateur = null;
	protected EOExercice currentExercice = null;
	protected String userLogin = null;

	private TransactionLogNibCtrl monTransactionLogNibCtrl = null;
	private ToolsCocktailObjectMessage observeurExerciceSelection = null;
	private ToolsCocktailJefyAdminPrivileges monToolsCocktailJefyAdmin = null;
	private EOTypeApplication currentTypeApplication = null;
	private NSMutableArray mesPrivileges = null;
	private UserInfoCocktail userInfos = null;
	private boolean withLogs = false;

	private NSMutableArray mesUtilisateurFonction = null;
	private NSMutableArray mesUtilisateurOrgan = null;
	private NSArray mesOrgans = null;
	private NSMutableArray mesTypeApplicationFonctions = null;

	private String TYAPLIBELLE = A_DEFINIR;
	private String TYAPSTRID = A_DEFINIR;
	private String NAME_APP = A_DEFINIR;
	private NSMutableArray lesPanelDeMonApplication;
	private boolean useConnexion = true;

	public JFrame screenshot;

	// Nouvelles sorties
	private MyByteArrayOutputStream redirectedOutStream, redirectedErrStream;
	//pour le nib des logs
	EOAction log = null;
	EOAction apropos = null;

	public ApplicationCocktail() {
		super();
		redirectLogs();
		initDefaultCocktailAction();
		AliveThreadClientServeur thr1 = new AliveThreadClientServeur(this);
		thr1.start();
		setMesPrivileges(new NSMutableArray());

	}

	private void initDefaultCocktailAction() {
		log = EOAction.actionForObject("showLog", "Aide/Voir les logs", null, null, null, null, EOAction.HelpCategoryPriority, EOAction.HelpActionPriority, this);
		apropos = EOAction.actionForObject("showAPropos", "Aide/A Propos de", null, null, null, null, EOAction.HelpCategoryPriority, EOAction.InfoActionPriority, this);
	}

	protected NSArray defaultActions() {
		NSMutableArray actions = new NSMutableArray();
		actions.addObject(log);
		actions.addObject(apropos);
		return actions;//EOAction.mergedActions(actions, super.defaultActions());
	}

	public void addJMenuToDefaultMenu(JComponent component, JMenuBar menuBar) {
		//      Ajout du menu
		JMenuBar defaultMenuBar = component.getRootPane().getJMenuBar();
		for (int i = 0; i < defaultMenuBar.getMenuCount(); i++)
			menuBar.add(defaultMenuBar.getMenu(i));
		component.getRootPane().setJMenuBar(menuBar);
	}
	
	public void setUseConnexion(boolean use) {
		useConnexion = use;
	}

	public void finishInitialization() {
		super.finishInitialization();
		//System.out.println("ApplicationCocktail.finishInitialization()");
		appEditingContext = new EOEditingContext();
		//System.err.println("1");
		// init de outils
		setMesTools(new NSMutableDictionary());
		getMesTools().setObjectForKey(new ToolsCocktailLogs(this), "logs");
		getMesTools().setObjectForKey(new ToolsCocktailEOF(this), "eof");
		getMesTools().setObjectForKey(new ToolsCocktailSystem(this), "system");
		getMesTools().setObjectForKey(new ToolsCocktailReports(this), "report");
		getMesTools().setObjectForKey(new ToolsCocktailGrhum(this), "grhum");
		getMesTools().setObjectForKey(new ToolsExcel(this), "excel");
		getMesTools().setObjectForKey(new ToolsFinder(this), "finder");
		getMesTools().setObjectForKey(new ToolsCocktailObjectMessage(this), "modal");
		//System.err.println("2");
		// init login + login 
		montrerSplashWindow();
		setFwkCktlWebApp(System.getProperty("user.name"));

		// Recuperation du typeApplication
		setCurrentTypeApplication(FinderGrhum.findTypeApplicationByStrid(this, TYAPSTRID));

		//System.err.println("3");
		if (useConnexion) {
			initLogin(this, "Authentification !");
		//System.err.println("4");
		//__lesPanelDeMonApplication= new NSMutableArray();
		}
	}

	public void montrerSplashWindow() {
		String message = "<html><body>http://www.asso-cocktail.fr<br>chargement en cours....</body></html>";
		String nomIcon = "Logo-Module";
		try {
			new SplashWindow(nomIcon, message, nomIcon);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// Initialisation de l'application a partir du userLogin
	public void initLogin(ApplicationCocktail monApp, String title) {
		monLoginNibCtrl = new LoginNibCtrl(monApp, 100, 100, 300, 200, true);
		LoginNib monLoginNib = new LoginNib();
		

		if (monLoginNibCtrl.afficherFenetreLogin()) {
			monLoginNibCtrl.creationFenetre(monLoginNib, title);
			monLoginNibCtrl.afficherFenetre();
			//            boolean ok = getVersionApplication().toLowerCase().indexOf(getMandatoryParameterForKey("APP_ALIAS").toLowerCase())>=0;
			//            //System.err.println("6");
			//            String bd = "".equals(getToolsCocktailEOF().getConnectionBaseDonnees()) ? "":" ("+getToolsCocktailEOF().getConnectionBaseDonnees()+")";
			//            if(ok)
			//            {
			//                monLoginNibCtrl.setInfos("<html><body><center>"+this.getVersionApplication()+bd+"</center></body></html>");
			//            }
			//            else
			//            {
			//                monLoginNibCtrl.setInfos("<html><body><center>"+getApplicationParametre("APP_ALIAS")+bd+"<br>"+this.getVersionApplication()+"</center></body></html>");
			//            }
			monLoginNibCtrl.setInfos("<html><body><center>" + getNomApplicationEtVersion(true) + "</center></body></html>");
		} else {
			accesOk();
		}
	}

	private String getNomApplicationEtVersion(boolean useHtml) {
		boolean ok = getVersionApplication().toLowerCase().indexOf(getMandatoryParameterForKey("APP_ALIAS").toLowerCase()) >= 0;
		String bd = "".equals(getToolsCocktailEOF().getConnectionBaseDonnees()) ? "" : " (" + getToolsCocktailEOF().getConnectionBaseDonnees() + ")";
		if (ok)
			return this.getVersionApplication() + bd + (useHtml ? "<br>" : " ") + this.getVersionApplication();
		else
			return getApplicationParametre("APP_ALIAS") + bd + (useHtml ? "<br>" : " ") + this.getVersionApplication();
	}

	public Class classPrivileges() {
		return null;
	}

	public void accesOk() {
		

		if (getCurrentTypeApplication() != null) {

			try {
				System.out.println("\n\n*************CONNEXION************\n");
				System.out.println(getCurrentUtilisateur().individu().nomUsuel() + " " + getCurrentUtilisateur().individu().prenom());
				System.out.println("\n le " + new NSTimestamp());
				System.out.println("\n***********************************\n");
				System.out.println(">>accesOk");

				setMonTransactionLogNibCtrl(new TransactionLogNibCtrl(this, 400, 100, 300, 200));
				getMonTransactionLogNibCtrl().creationFenetre(new TransactionLogNib(), "Transactions en cours ....");

				ceerTransactionLog();

				afficherUnLogDansTransactionLog("Bienvenue sur l'application " + getNomApplicationEtVersion(false) + " ....", 5);
				setObserveurExerciceSelection(new ToolsCocktailObjectMessage(this));
				NSTimeZone.setDefault(NSTimeZone.timeZoneWithName("WEST", true));

				afficherUnLogDansTransactionLog("Récuperation de vos droits et acces....", 20);
				setMonToolsCocktailJefyAdmin(new ToolsCocktailJefyAdminPrivileges(this, getMesPrivileges()));

				afficherUnLogDansTransactionLog("Filtrage de vos acces....", 40);
				long start = System.currentTimeMillis();
				setMesOrgans(FinderGrhum.findOrgans(this, getCurrentUtilisateur()));
				System.out.println("Fetch organs : " + (System.currentTimeMillis() - start) + "ms");

				initFindersAccesEtDroits();

				afficherUnLogDansTransactionLog("Application de vos droits....", 60);
				creationDesPrivileges();

				afficherUnLogDansTransactionLog("Cr\u00E9ation de l'application....", 80);
				initMonApplication();

				getMonToolsCocktailJefyAdmin().appliquerLesPrivilegesUI();

				finirTransactionLog();
				fermerTransactionLog();

			} catch (Throwable e) {
				e.printStackTrace();
				afficherUnLogDansTransactionLog(e.toString(), 100);
				fenetreDeDialogueInformation("PROBLEME D'ACCES .... l'application va se terminer ! \n" + getMonTransactionLogNibCtrl().getMonNib().getJTextAreaLogs().getText());
				quit();
			}
		} else {
			System.out.println("\n\n*************CONNEXION************\n");
			System.out.println("\n Le " + new NSTimestamp());
			System.out.println("\n***********************************\n");
			System.out.println(">>accesOk");
			System.out.println("Application non reliée à JefyAdmin");

			setMonTransactionLogNibCtrl(new TransactionLogNibCtrl(this, 400, 100, 300, 200));
			getMonTransactionLogNibCtrl().creationFenetre(new TransactionLogNib(), "Transactions en cours ....");

			ceerTransactionLog();

			afficherUnLogDansTransactionLog("Bienvenue sur l'application " + getNomApplicationEtVersion(false) + " ....", 5);
			//setObserveurExerciceSelection(new ToolsCocktailObjectMessage(this));
			//NSTimeZone.setDefault(NSTimeZone.timeZoneWithName("WEST", true));

			//afficherUnLogDansTransactionLog("Récuperation de vos droits et acces....", 20);
			//setMonToolsCocktailJefyAdmin(new ToolsCocktailJefyAdminPrivileges(this, getMesPrivileges()));

			//afficherUnLogDansTransactionLog("Filtrage de vos acces....", 40);
			//setMesUtilisateurOrgan(new NSMutableArray(FinderGrhum.findUtilisateurOrgans(this, getCurrentUtilisateur())));
			//initFindersAccesEtDroits();

			afficherUnLogDansTransactionLog("Application de vos droits....", 60);
			creationDesPrivileges();

			afficherUnLogDansTransactionLog("Cr\u00E9ation de l'application....", 80);
			initMonApplication();

			//getMonToolsCocktailJefyAdmin().appliquerLesPrivilegesUI();

			finirTransactionLog();
			fermerTransactionLog();
		}
	}

	public void creationDesPrivileges() {
	};

	public void initMonApplication() {
	};

	public void initFindersAccesEtDroits() {
		
		setCurrentExercice((EOExercice) FinderGrhum.findExerciceCourant(this));

//		try {
//			if (!A_DEFINIR.equals(TYAPSTRID)) {
//				setCurrentTypeApplication(FinderGrhum.findTypeApplicationByStrid(this, TYAPSTRID));
//			}
//			else {
//				throw new Exception("INITIALISATION IMCOMPLETE");
//			}
//		} catch (Exception e) {
//			getToolsCocktailLogs().addLogMessage(this, "INITIALISATION INCOMPLETE", "TYAPSTRID erroné :" + TYAPSTRID, true);
//			fenetreDeDialogueInformation("TYAPSTRID ou TYAPLIBELLE a DEFINIR.... l'application va se terminer !");
//			quit();
//			e.printStackTrace();
//		}

		if (currentTypeApplication != null) {

			try {

				setCurrentUtilisateur(FinderGrhum.findUtilisateurWithPersId(this, (Number) getUserInfos().objectForKey("persId")));

			} catch (Exception e) {
				String mess = "UTILISATEUR INCONNU DANS JEFY ADMIN : PERSID = " + getUserInfos().objectForKey("persId").toString();
				getToolsCocktailLogs().addLogMessage(this, "PROBLEME D'ACCES", mess, true);
				fenetreDeDialogueInformation("PROBLEME D'ACCES .... l application va se terminer ! \n" + mess);
				quit();
			}
			try {
				setMesUtilisateurFonction(FinderGrhum.findUtilisateurFonctions(this, getCurrentUtilisateur(), getCurrentTypeApplication()));
				if ((!"DEPENSE".equals(getCurrentTypeApplication().tyapLibelle())) && (mesUtilisateurFonction == null || mesUtilisateurFonction.count() == 0))
				{
					throw new Exception("Vous n'avez pas le droit d'executer cette application");
				}
			} catch (Exception e) {
				getToolsCocktailLogs().addLogMessage(this, "PAS DE DROIT", e.getMessage(), true);
				fenetreDeDialogueInformation("PROBLEME D'ACCES.... " + e.getMessage() + " !");
				quit();
				e.printStackTrace();
			}

			setMesTypeApplicationFonctions(FinderGrhum.findTypeApplicationfonctions(this, getCurrentTypeApplication()));

		}

	}

	public void addPrivileges(ToolsCocktailPrivileges obj) {

		boolean aAjouter = true;

		for (int i = 0; i < getMesPrivileges().count(); i++) {

			if (((ToolsCocktailPrivileges) getMesPrivileges().objectAtIndex(i)).getKey().equals(obj.getKey()))
				aAjouter = false;
		}
		if (aAjouter) {
			System.out.println("ajout privilège " + obj.toString());
			getMesPrivileges().addObject(obj);
		}

	}

	public String getNumeroVersionServeur() {
		return (String) ((EODistributedObjectStore) getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestNumeroVersionApplication", null, null, false);
	}

	public String getVersionApplication() {
		//System.out.println("ApplicationCocktail.getVersionApplication()");
		return (String) ((EODistributedObjectStore) getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestVersionApplication", null, null, false);
	}

	public String getApplicationParametre(String key) {
		//System.out.println("ApplicationCocktail.getApplicationParametre()");
		return (String) ((EODistributedObjectStore) getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestGetParametre", new Class[] {
				String.class
		}, new Object[] {
				key
		}, false);
	}

	// Modifier du userLogin
	private void setFwkCktlWebApp(String name) {
		userLogin = name;
	}

	// Accesseur retourne le user login
	public String userLogin() {

		if (userLogin == null) {
			return System.getProperty("user.name");
		} else {
			return userLogin;
		}
	}

	protected boolean launchApplication(String fileName) {
		boolean isOK = false;
		String commande;
		// Tests 
		if (fileName == null) {
			getToolsCocktailLogs().addLogMessage(this, "Erreur", "Argument null pour le lancement de l'application.", withLogs());
			return isOK;
		}
		// Tests
		if (fileName.equals("")) {
			getToolsCocktailLogs().addLogMessage(this, "Erreur", "Lancement de l'application : argument vide pour le lancement de l'application.", withLogs());
			return isOK;
		}
		// Mac OSX
		if (System.getProperties().getProperty("os.name").equals(ToolsCocktailSystem.MAC_OS_X_OS_NAME)) {
			try {
				commande = new String(ToolsCocktailSystem.MAC_OS_X_EXEC + " " + fileName);
				Runtime.getRuntime().exec(commande);
				isOK = true;
			} catch (Exception exception) {
				getToolsCocktailLogs().addLogMessage(this, "Exception", "launchApplication() : " + exception.getMessage(), withLogs());
				EODialogs.runErrorDialog("ERREUR", "Impossible d'executer l'application correspondant au fichier " + fileName);
				isOK = false;
			}
			//finally {
			return isOK;
			//	}
		}
		// Windows
		else if (System.getProperties().getProperty("os.name").startsWith(ToolsCocktailSystem.WINDOWSXP_OS_NAME)) {
			try {
				commande = new String(ToolsCocktailSystem.WINDOWSXP_EXEC + " " + fileName);
				Runtime.getRuntime().exec(commande);
				isOK = true;
			} catch (Exception exception) {
				getToolsCocktailLogs().addLogMessage(this, "Exception", "launchApplication() : " + exception.getMessage(), withLogs());
				EODialogs.runErrorDialog("ERREUR", "Impossible d'executer l'application correspondant au fichier " + fileName);
				isOK = false;
			}
			//finally {
			return isOK;
			//}
		}
		// Pas encore defini
		else {
			getToolsCocktailLogs().addLogMessage(this, "Erreur", "Aucune commande d'execution definie pour le systeme d'exploitation", withLogs());
			EODialogs.runErrorDialog("ERREUR", "Aucune commande d'execution definie pour le systeme d'exploitation " + System.getProperties().getProperty("os.name") + "\nVous pouvez lancer manuellement l'application et ouvrir le fichier " + fileName);
			return isOK;
		}
	}

	// Quitte l'application : mettre ici les operations a effectuer avant de quitter
	public void quit() {
		if (canQuit()) {
			super.quit();
		} else {
			EODialogs.runErrorDialog("ERREUR", "Impossible de quitter l'application pour le moment");
		}
	}

	public void setWithLogs(boolean b) {
		this.withLogs = b;
	}

	public boolean withLogs() {
		return withLogs;
	}

	public EOEditingContext getAppEditingContext() {
		return appEditingContext;
	}

	public void setAppEditingContext(EOEditingContext appEditingContext) {
		this.appEditingContext = appEditingContext;
	}

	private NSMutableDictionary getMesTools() {
		return mesTools;
	}

	private void setMesTools(NSMutableDictionary mesTools) {
		this.mesTools = mesTools;
	}

	/**
	 *
	 * @return
	 */
	public String getAppName() {
		try {
			return getNAME_APP();
		} catch (Exception e) {
			getToolsCocktailLogs().addLogMessage(this, "INITIALISATION IMCOMPLETE", "NAME_APP a DEFINIR", true);
			fenetreDeDialogueInformation("NAME_APP a DEFINIR.... l'application va se terminer !");
			quit();
			e.printStackTrace();
			return null;
		}
	}

	public ToolsCocktailLogs getToolsCocktailLogs() {
		//System.out.println("ApplicationCocktail.getToolsCocktailLogs()");
		return (ToolsCocktailLogs) mesTools.valueForKey("logs");
	}

	public ToolsCocktailEOF getToolsCocktailEOF() {
		return (ToolsCocktailEOF) mesTools.valueForKey("eof");
	}

	public ToolsCocktailSystem getToolsCocktailSystem() {
		return (ToolsCocktailSystem) mesTools.valueForKey("system");
	}

	public ToolsExcel getToolsCocktailExcel() {
		return (ToolsExcel) mesTools.valueForKey("excel");
	}

	public ToolsCocktailReports getToolsCocktailReports() {
		return (ToolsCocktailReports) mesTools.valueForKey("report");
	}

	public ToolsCocktailGrhum getToolsCocktailGrhum() {
		return (ToolsCocktailGrhum) mesTools.valueForKey("grhum");
	}

	public ToolsFinder getToolsFinder() {
		return (ToolsFinder) mesTools.valueForKey("finder");
	}

	public ToolsFinder getToolsModal() {
		return (ToolsFinder) mesTools.valueForKey("modal");
	}

	public NSDictionary getParametresApplication() {
		return parametresApplication;
	}

	public void setParametresApplication(NSDictionary parametresApplication) {
		this.parametresApplication = parametresApplication;
	}

	public EOUtilisateur getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOUtilisateur currentUtilisateur) {
		//getToolsCocktailLogs().addLogMessage(this, "info",  "Utilisateur :"+currentUtilisateur,withLogs());
		this.currentUtilisateur = currentUtilisateur;
	}

	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}

	public String getOptionnalParameterForKey(String key) {
		return (String) getApplicationParametre(key);
	}

	public String getMandatoryParameterForKey(String key) {
		//System.out.println("ApplicationCocktail.getMandatoryParameterForKey()");
		String s = (String) getApplicationParametre(key);
		//System.err.println("1");
		if (s == null)
		{
			//System.err.println("2");
			getToolsCocktailLogs().addLogMessage(this, "PROBLEME GRAVE :", "Parametre manquant : " + key, true);
			quit();
		}
		//System.err.println("3");
		return s;
	}

	public void afficherUnLogDansTransactionLog(String s, int i) {
		getMonTransactionLogNibCtrl().afficherUnLog(" " + s, i);
	}

	public void rafficherUnLogDansTransactionLog(String sOld, String sNew, int i) {
		getMonTransactionLogNibCtrl().rafficherUnLog(sOld, sNew, i);
	}

	public void fenetreDeDialogueInformation(String message) {
		String title = "Information !";
		JFrame frame = new JFrame(title);
		JOptionPane.showMessageDialog(frame, message);
	}

	public void finirTransactionLog() {
		getMonTransactionLogNibCtrl().terminer();
	}

	public void fermerTransactionLog() {
		getMonTransactionLogNibCtrl().masquerFenetre();
	}

	public void ceerTransactionLog() {
		getMonTransactionLogNibCtrl().afficherFenetre();
	}

	public void ceerTransactionLog(ModalInterfaceCocktail parent) {
		setMonTransactionLogNibCtrl(new TransactionLogNibCtrl(this, 400, 100, 300, 200));
		getMonTransactionLogNibCtrl().creationFenetre(new TransactionLogNib(), "Transactions en cours ....");
		getMonTransactionLogNibCtrl().afficherFenetre(parent);
	}

	protected void setMonTransactionLogNibCtrl(TransactionLogNibCtrl monTransactionLogNibCtrl) {
		this.monTransactionLogNibCtrl = monTransactionLogNibCtrl;
	}

	protected TransactionLogNibCtrl getMonTransactionLogNibCtrl() {
		return monTransactionLogNibCtrl;
	}

	public static void setLocationCenter(final Component component, final int location) {
		//		Dimensions de l'ecran
		int w = (int) component.getGraphicsConfiguration().getBounds().getWidth();
		int h = (int) component.getGraphicsConfiguration().getBounds().getHeight();

		//		coordonnees utiles
		int xMiddle = (w / 2) - ((int) component.getSize().getWidth() / 2);
		int yMiddle = (h / 2) - ((int) component.getSize().getHeight() / 2);
		int xRight = w - (int) component.getSize().getWidth();
		int yBottom = h - (int) component.getSize().getHeight();

		int x, y;
		switch (location) {
		case LOCATION_MIDDLE:
			x = xMiddle;
			y = yMiddle;
			break;
		case LOCATION_NORTH:
			x = xMiddle;
			y = 0;
			break;
		case LOCATION_SOUTH:
			x = xMiddle;
			y = yBottom;
			break;

		case LOCATION_NORTH_EAST:
			x = xRight;
			y = 0;
			break;
		case LOCATION_EAST:
			x = xRight;
			y = yMiddle;
			break;
		case LOCATION_SOUTH_EAST:
			x = xRight;
			y = yBottom;
			break;

		case LOCATION_NORTH_WEST:
			x = 0;
			y = 0;
			break;
		case LOCATION_WEST:
			x = 0;
			y = yMiddle;
			break;
		case LOCATION_SOUTH_WEST:
			x = 0;
			y = yBottom;
			break;

		default: //centre 
			x = xMiddle;
			y = yMiddle;
			break;
		}
		component.setLocation(x, y);

	}

	public static NSMutableDictionary getLocationCenter(final Component component, final int location) {
		try {

			//			Dimensions de l'ecran
			int w = (int) component.getGraphicsConfiguration().getBounds().getWidth();
			int h = (int) component.getGraphicsConfiguration().getBounds().getHeight();

			//			coordonnees utiles
			int xMiddle = (w / 2) - ((int) component.getSize().getWidth() / 2);
			int yMiddle = (h / 2) - ((int) component.getSize().getHeight() / 2);
			int xRight = w - (int) component.getSize().getWidth();
			int yBottom = h - (int) component.getSize().getHeight();

			int x, y;
			switch (location) {
			case LOCATION_MIDDLE:
				x = xMiddle;
				y = yMiddle;
				break;
			case LOCATION_NORTH:
				x = xMiddle;
				y = 0;
				break;
			case LOCATION_SOUTH:
				x = xMiddle;
				y = yBottom;
				break;

			case LOCATION_NORTH_EAST:
				x = xRight;
				y = 0;
				break;
			case LOCATION_EAST:
				x = xRight;
				y = yMiddle;
				break;
			case LOCATION_SOUTH_EAST:
				x = xRight;
				y = yBottom;
				break;

			case LOCATION_NORTH_WEST:
				x = 0;
				y = 0;
				break;
			case LOCATION_WEST:
				x = 0;
				y = yMiddle;
				break;
			case LOCATION_SOUTH_WEST:
				x = 0;
				y = yBottom;
				break;

			default: //centre 
				x = xMiddle;
				y = yMiddle;
				break;
			}
			component.setLocation(x, y);
			NSMutableDictionary monDico = new NSMutableDictionary();
			monDico.takeValueForKey(new Integer(x), "X");
			monDico.takeValueForKey(new Integer(y), "Y");

			return monDico;

		} catch (Exception e) {
			System.out.println("getLocationCenter OUPS" + e);
			return null;
		}
	}

	protected void setCurrentTypeApplication(EOTypeApplication currentTypeApplication) {
		this.currentTypeApplication = currentTypeApplication;
	}

	public EOTypeApplication getCurrentTypeApplication() {
		return currentTypeApplication;
	}

	public void setUserInfos(UserInfoCocktail userInfos) {
		this.userInfos = userInfos;
	}

	public UserInfoCocktail getUserInfos() {
		return userInfos;
	}

	private void setObserveurExerciceSelection(ToolsCocktailObjectMessage observeurExerciceSelection) {
		this.observeurExerciceSelection = observeurExerciceSelection;
	}

	public ToolsCocktailObjectMessage getObserveurExerciceSelection() {
		return observeurExerciceSelection;
	}

	public ToolsCocktailJefyAdminPrivileges getMonToolsCocktailJefyAdmin() {
		return monToolsCocktailJefyAdmin;
	}

	public void setMonToolsCocktailJefyAdmin(
			ToolsCocktailJefyAdminPrivileges monToolsCocktailJefyAdmin) {
		this.monToolsCocktailJefyAdmin = monToolsCocktailJefyAdmin;
	}

	private void setMesPrivileges(NSMutableArray mesPrivileges) {
		this.mesPrivileges = mesPrivileges;
	}

	public NSMutableArray getMesPrivileges() {
		return mesPrivileges;
	}

	/**
	 * @deprecated Utiliser le TYAP_STRID
	 */
	@Deprecated
	public void setTYAPLIBELLE(String tYAPLIBELLE) {
		TYAPLIBELLE = tYAPLIBELLE;
	}

	/**
	 * @deprecated Utiliser le TYAP_STRID
	 */
	@Deprecated
	public String getTYAPLIBELLE() throws Exception {
		if (TYAPLIBELLE.equals(A_DEFINIR))
			throw new Exception();
		return TYAPLIBELLE;
	}

	public void setTYAPSTRID(String tYAPSTRID) {
		TYAPSTRID = tYAPSTRID;
	}

	public void setNAME_APP(String nAME_APP) {
		NAME_APP = nAME_APP;
	}

	public String getNAME_APP() throws Exception {
		if (NAME_APP.equals(A_DEFINIR))
			throw new Exception();
		return NAME_APP;
	}

	void setMesUtilisateurOrgan(NSMutableArray mesUtilisateurOrgan) {
		this.mesUtilisateurOrgan = mesUtilisateurOrgan;
		//System.out.println("getMesUtilisateurOrgan   "+getMesUtilisateurOrgan().count());
	}

	/**
	 * Utiliser getMesOrgans à la place
	 * 
	 * @return les utilisateursOrgans / Utiliser getMesOrgans à la place
	 */
	@Deprecated
	public NSMutableArray getMesUtilisateurOrgan() {
		if (mesUtilisateurOrgan == null) {
			//			setMesUtilisateurOrgan(new NSMutableArray(FinderGrhum.findUtilisateurOrgans(this, getCurrentUtilisateur())));
			mesUtilisateurOrgan = getCurrentUtilisateur().utilisateurOrgans().mutableClone();
		}
		return mesUtilisateurOrgan;
	}

	void setMesOrgans(NSArray mesOrgans) {
		this.mesOrgans = mesOrgans;
		//System.out.println("getMesUtilisateurOrgan   "+getMesUtilisateurOrgan().count());
	}

	public NSArray getMesOrgans() {
		return mesOrgans;
	}

	public NSMutableArray getMesTypeApplicationFonctions() {
		return mesTypeApplicationFonctions;
	}

	public void setMesTypeApplicationFonctions(NSArray mesTypeApplicationFonctions) {
		if (mesTypeApplicationFonctions == null)
			return;
		this.mesTypeApplicationFonctions = new NSMutableArray(mesTypeApplicationFonctions);
	}

	public NSMutableArray getMesUtilisateurFonction() {
		return mesUtilisateurFonction;
	}

	public void setMesUtilisateurFonction(NSArray mesUtilisateurFonction) {
		if (mesUtilisateurFonction == null)
			return;
		this.mesUtilisateurFonction = new NSMutableArray(mesUtilisateurFonction);
	}

	public void invalidateEO(EOGenericRecord eo) {
		getAppEditingContext().invalidateObjectsWithGlobalIDs(
				new NSArray(getAppEditingContext().globalIDForObject(eo))
				);
	}

	public void invalidateObject(EOEditingContext ed, EOGenericRecord eo) {
		ed.invalidateObjectsWithGlobalIDs(
				new NSArray(ed.globalIDForObject(eo)));
	}

	// a ordonner plus tard 

	public NSTimestamp stringToTimestamp(String ddmmyyyy) {
		NSArray dataArray = new NSArray(NSArray.componentsSeparatedByString(ddmmyyyy, "/"));

		System.out.println("dataArray " + dataArray);
		int jour = new Integer(((String) dataArray.objectAtIndex(0))).intValue();
		int mois = new Integer(((String) dataArray.objectAtIndex(1))).intValue();
		int annee = new Integer(((String) dataArray.objectAtIndex(2))).intValue();

		GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
		gc.set(Calendar.YEAR, annee);
		gc.set(Calendar.MONTH, mois - 1);
		gc.set(Calendar.DAY_OF_MONTH, jour);
		gc.set(Calendar.HOUR_OF_DAY, 12);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		return new NSTimestamp(gc.getTime());
	}

	public class AliveThreadClientServeur extends Thread {
		/** Un attribut propre à chaque thread */
		private ApplicationCocktail appCock;

		/** Création et démarrage automatique du thread */
		public AliveThreadClientServeur(ApplicationCocktail appCock) {
			this.appCock = appCock;
		}

		/**
		 * Le but d'un tel thread est d'afficher 500 fois son attribut threadName. Notons que la methode <I>sleep</I> peut declancher des exceptions.
		 */
		public void run() {
			try {
				while (true) {
					Thread.sleep(300000);
					synchronized (this) {
						try {
							appCock.getUserInfos();
							if (getCurrentTypeApplication() != null) {
								FinderGrhum.findUtilisateurOrgans(appCock, appCock.getCurrentUtilisateur());
							}
							System.out.println("Client <-> Serveur ok");
						} catch (Exception e) {
							appCock.fenetreDeDialogueInformation("Probleme de connection !\n Hors service");
							System.out.println("Client <-> Serveur ko");
							appCock.quit();
						}
						;
					}
					;

				}
			} catch (InterruptedException exc) {
				exc.printStackTrace();
			}
		}
	}

	public NSData getSQlResult(String sql, NSArray keys) {
		return getToolsCocktailEOF().getSQlResult(sql, keys);
	}

	public NSData getFileFromServeurResouces(String fileName) {

		return (NSData) ((EODistributedObjectStore) getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getAppEditingContext(),
				"session.remoteCallDelagate",
				"clientSideRequestGetFileFromServeurResouces",
				new Class[] {
					String.class
				},
				new Object[] {
					fileName
				}, true);
	}

	//	pour les nib 

	//	tools
	public void activerLesGlassPane(ModalInterfaceCocktail sender) {
		//		System.out.println("ApplicationCocktail.activerLesGlassPane("+sender.getClass().getName()+")");
		try {
			//			System.out.println("getLesPanelsModal().count()="+getLesPanelsModal().count());
			//			System.out.println("getLesPanelsModal() = "+getLesPanelsModal());
			for (int i = 0; i < this.getLesPanelsModal().count(); i++) {
				//				System.out.println("------->"+this.getLesPanelsModal().objectAtIndex(i).getClass().getName());
				if (!(sender != null && sender.equals(this.getLesPanelsModal().objectAtIndex(i))))
					((ModalInterfaceCocktail) this.getLesPanelsModal().objectAtIndex(i)).poserGlassPane();
				else
				{
					if (this.getLesPanelsModal().objectAtIndex(i) != null)
						((ModalInterfaceCocktail) this.getLesPanelsModal().objectAtIndex(i)).retirerGlassPane();
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void activerLesGlassPane() {
		try {
			for (int i = 0; i < this.getLesPanelsModal().count(); i++) {
				((ModalInterfaceCocktail) this.getLesPanelsModal().objectAtIndex(i)).poserGlassPane();
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public NSMutableArray getLesPanelsModal() {
		try {
			if (lesPanelDeMonApplication == null)
				lesPanelDeMonApplication = new NSMutableArray();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return lesPanelDeMonApplication;
	}

	public void addLesPanelsModal(ModalInterfaceCocktail obj) {
		try {
			//			System.out.println("ApplicationCocktail.addLesPanelsModal("+obj.getClass().getName()+")");
			if (this.getLesPanelsModal() != null)
			{
				//				System.out.println("----> add "+obj.getClass().getName()+")");
				//				System.out.println("---->Avant nb = "+this.getLesPanelsModal().count());
				//				getToolsCocktailLogs().trace("----->", this.getLesPanelsModal(), true);
				this.getLesPanelsModal().addObjectsFromArray(new NSArray(obj));
				//				getToolsCocktailLogs().trace("----->", this.getLesPanelsModal(), true);
				//				System.out.println("---->Apres nb = "+this.getLesPanelsModal().count());
				//				System.out.println("----> @getLesPanelsModal() = "+getLesPanelsModal());
			}
			//else
			//				System.out.println("this.getLesPanelsModal() = null : "+this.getLesPanelsModal()+")");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void retirerLesGlassPane() {
		try {
			for (int i = 0; i < this.getLesPanelsModal().count(); i++)
				((ModalInterfaceCocktail) this.getLesPanelsModal().objectAtIndex(i)).retirerGlassPane();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void removeFromLesPanelsModal(ModalInterfaceCocktail obj) {
		//		System.out.println("ApplicationCocktail.removeFromLesPanelsModal("+obj.getClass().getName()+")");
		try {
			if (getLesPanelsModal() != null)
				for (int i = 0; i < getLesPanelsModal().count(); i++) {
					if (getLesPanelsModal().objectAtIndex(i).equals(obj))
						getLesPanelsModal().removeObjectAtIndex(i);
				}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void setMainComponentCocktail(String frameName, JPanelCocktail nib, int x, int y, int h, int w) {
		MainNibControlerInterfaceController main = new MainNibControlerInterfaceController(getAppEditingContext());
		EOFrameController.runControllerInNewFrame(main, frameName);
		main.view.setLayout(new BorderLayout());
		main.view.add(nib);
		main.component().getTopLevelAncestor().setBounds(x, y, h + 20, w + 150);
		addLesPanelsModal(main);
	}

	public void setMainComponentCocktail(String frameName, EOInterfaceControllerCocktail nib, int x, int y, int h, int w) {
		EOFrameController.runControllerInNewFrame(nib, frameName);
		nib.component().getTopLevelAncestor().setBounds(x, y, h + 20, w + 150);
		addLesPanelsModal(nib);
	}

	//Pour les logs client
	private void redirectLogs() {
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));
		// on force la sortie du NSLog (qui etait initialise par défaut sur System.xxx AVANT le changement)
		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);
	}

	// Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	// Garde en mémoire un log de taille entre maxCount/2 et maxCount octets
	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;
		protected int maxCount;

		public MyByteArrayOutputStream() {
			this(System.out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out) {
			this(out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out, int maxCount) {
			super(maxCount);
			this.out = out;
			this.maxCount = maxCount;
		}

		public synchronized void write(int b) {
			if (maxCount > 0 && count + 1 > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			if (maxCount > 0 && count + len > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b, off, len);
			out.write(b, off, len);
		}

		private void shift(int shift) {
			for (int i = shift; i < count; i++) {
				buf[i - shift] = buf[i];
			}
			count = count - shift;
		}
	}

	public String clientOutLog() {
		return redirectedOutStream.toString();
	}

	public String clientErrLog() {
		return redirectedErrStream.toString();
	}

	public void showLog() {
		Logs logInterface = new Logs(this.appEditingContext, null, false);
		logInterface.afficherFenetre();
	}

	public void showAPropos() {
		AProposDe aProposDe = new AProposDe((NibCtrl) null);
		aProposDe.afficherFenetre();
	}

	public int nbTimeOut() {
		if (getApplicationParametre("TIMEOUT_REPORT") == null)
			return 60;
		try {
			return Integer.parseInt(getApplicationParametre("TIMEOUT_REPORT")) / 3;
		} catch (Exception e) {
		}
		return 60;
	}

	public Color defaultBackgroundColor() {
		return null;
	}

	public Object invokeRemoteMethodWithKeyPath(EOEditingContext ec, String path, String name, Class[] classNames, Object[] objects, boolean bool)
	{
		try {
			return ((EODistributedObjectStore) EOEditingContext.defaultParentObjectStore()).invokeRemoteMethodWithKeyPath(ec, path, name, classNames, objects, bool);
		} catch (Throwable th) {
			System.out.println("Exception : execution de invokeRemoteMethod() - " + th.getMessage());
			th.printStackTrace();
		}
		return null;
	}

	/**
	 * Compare la version indiquée dans le manifest du jar client et le n° de version retourné par le serveur.
	 */
	public void compareJarVersionsClientAndServer() throws Exception {
		Boolean isDevMode = CktlServerInvoke.clientSideRequestIsDevelopmentMode(getAppEditingContext());

		if (isDevMode) {
			System.out.println("Mode développement, pas de vérification de cohérence entre les versions des jars client et serveur.");
			return;
		}

		String serverJarVersion = CktlServerInvoke.clientSideRequestGetVersionFromJar(getAppEditingContext());
		String clientJarVersion = clientJarVersion();
		System.out.println("Serveur build : " + serverJarVersion);
		System.out.println("Client build : " + clientJarVersion);
		if (serverJarVersion == null) {
			throw new Exception("Impossible de récupérer la version du jar coté serveur.");
		}
		if (clientJarVersion == null) {
			throw new Exception("Impossible de récupérer la version du jar coté client.");
		}
		if (!serverJarVersion.equals(clientJarVersion)) {
			throw new Exception("Incoherence entre la version cliente et la version serveur. Les ressources web ne sont probablement pas à jour." + serverJarVersion + " / " + clientJarVersion);
		}
	}

	protected String clientJarVersion() {
		String className = this.getClass().getName();
		VersionAppFromJar varAppFromJar = new VersionAppFromJar(className);
		return varAppFromJar.fullVersion();
	}

}
