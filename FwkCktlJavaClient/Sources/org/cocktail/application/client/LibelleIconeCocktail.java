/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 19 juil. 07
 * author mparadot 
 */
package org.cocktail.application.client;

import java.lang.reflect.Field;

import com.webobjects.foundation.NSKeyValueCoding;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public class LibelleIconeCocktail  {

        
        public static String  FERMER="Fermer";
        public static String  INSPECTER=null;
        public static String  COPIERCOLLER="Copier / Coller";
        
        public static String  ENTRER="Valider";
        public static String  QUITTER="Quitter";
        
        public static String  INTEGRER="Reprise";
        public static String  EXTRAIRE="Sortie";
        public static String  RECHERCHER="Rechercher";
        public static String  RECHERCHER2=null;
        
        public static String  FLECHEDROITE=null;
        public static String  FLECHEGAUCHE=null;
        
        public static String  REFRESH="Refresh";
        public static String  EMAIL=null;

        public static String  AJOUTER=null;
        public static String  MODIFIER=null;
        public static String  SUPPRIMER=null;
        public static String  IMPRIMER=null;
        
        public static String  POUBELLE=null;
        public static String  TRAITEMENT_BD=null;
        
        public static String  VISA="tasks_tsk";
        
        public static String  VALIDER="Valider";
        public static String  REFUSER="Annuler";
        
        public static String PAGE="Imprimer";
        public static String TABLEAU="Exercices";
        public static String FILTRER="Filtrer";
        

        public static String PDF = "PDF";
        public static String XLS = "XLS";
        public static String MAIL = "Mail";
        public static String COPIECOLLE = null;
        public static String TABCROISEXLS = "Rapport XLS";  
        public static String CSV = "CSV";
        
        public static String SHOW = null;
        
        public static String libelle(String str){
            try {
                Field f = LibelleIconeCocktail.class.getField(StringCtrl.toBasicString(str).trim().toUpperCase());
                return (String)f.get(null);
            } catch (SecurityException e) {
            } catch (NoSuchFieldException e) {
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            }
            return str;
        }
        
}
