/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOFonction;
import org.cocktail.application.client.eof.EOTypeApplication;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.eof.EOUtilisateurFonction;
import org.cocktail.application.client.eof.EOUtilisateurOrgan;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.application.serveur.eof.EOOrgan;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class FinderGrhum extends Finder{

	
	
	public FinderGrhum(boolean withLogs) throws Exception {
		super(withLogs);
		
	}



	static public NSMutableArrayDisplayGroup findLesExercices(ApplicationCocktail app)
	{
		 String entityName =EOExercice.ENTITY_NAME;
		 NSArray leSort = null;
		 EOQualifier aQualifier = null;

		// le sort 
	      leSort =new NSArray(new Object[]{
	                EOSortOrdering.sortOrderingWithKey(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareCaseInsensitiveDescending)
	        });
		// le qualifier
	//	 aQualifier = EOQualifier.qualifierWithQualifierFormat("tboSousType = %@ ", new NSArray(new Object[]{"DEPENSES"}));

		return Finder.find(app, entityName, leSort, aQualifier);
	}
	
	static public EOExercice findExerciceCourant (ApplicationCocktail app)
	
	{
		 String entityName =EOExercice.ENTITY_NAME;
		 NSArray leSort = null;
		 EOQualifier aQualifier = null;

		// le sort 
		 
		// le qualifier
		 aQualifier = EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_STAT_KEY+" = %@", new NSArray(new Object[]{"O"}));

		return (EOExercice) Finder.find(app, entityName, leSort, aQualifier).objectAtIndex(0);
	}




	static public EOUtilisateur  findUtilisateurWithPersId(ApplicationCocktail app,Number persId)
	{
		String entityName = EOUtilisateur.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier = null;
		//Integer persidtmp = new Integer( persId);

		// le sort 

		// le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.PERS_ID_KEY+" = %@", new NSArray(new Object[]{persId}));
		NSMutableArrayDisplayGroup tmp = new NSMutableArrayDisplayGroup(Finder.find(app, entityName, leSort, aQualifier));

		if (tmp.count() == 1)
			return (EOUtilisateur) tmp.objectAtIndex(0);
		else
			return null;
	}


	/**
	 * @deprecated Utiliser {@link #findTypeApplicationByStrid(ApplicationCocktail, String)}
	 */
	@Deprecated
	static public EOTypeApplication findTypeApplication (ApplicationCocktail app,String tyapLibelle)
	{
		
		String entityName = EOTypeApplication.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		// le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeApplication.TYAP_LIBELLE_KEY+" = %@", new NSArray(new Object[]{tyapLibelle}));
		NSMutableArrayDisplayGroup tmp = new NSMutableArrayDisplayGroup(Finder.find(app, entityName, leSort, aQualifier));

		if (tmp.count() == 1)
			return (EOTypeApplication) tmp.objectAtIndex(0);
		else
			return null;
	}
	
	/**
	 * @param app L'{@link ApplicationCocktail}
	 * @param tyapStrid Le TYAP_STRID de l'application
	 * @return Un {@link EOTypeApplication}
	 */
	static public EOTypeApplication findTypeApplicationByStrid (ApplicationCocktail app, String tyapStrid) {
		EOQualifier qual = new EOKeyValueQualifier(EOTypeApplication.TYAP_STRID_KEY, EOQualifier.QualifierOperatorEqual, tyapStrid);
		NSArray<EOTypeApplication> liste = Finder.find(app, EOTypeApplication.ENTITY_NAME, null, qual);
		if (liste.count() > 0) {
			return liste.objectAtIndex(0);
		}
		return null;
	}
	
	static public NSArray findTypeApplicationfonctions (ApplicationCocktail app,EOTypeApplication eoapp)
	{
		
		String entityName = EOFonction.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier = null;
        if(eoapp==null)
            return new NSArray();
		// le sort 

		// le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOFonction.TYPE_APPLICATION_KEY+" = %@", new NSArray(new Object[]{eoapp}));
		return Finder.find(app, entityName, leSort, aQualifier);
	}
	
	
	static public NSArray findUtilisateurFonctions (ApplicationCocktail app,EOUtilisateur utilisateur,EOTypeApplication typeApplication)
	{
		
		String entityName = EOUtilisateurFonction.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier = null;
        if(typeApplication==null)
            return new NSArray();
		// le sort 

		// le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY+"."+EOFonction.TYPE_APPLICATION_KEY+" = %@ and utilisateur = %@",
				new NSArray(new Object[]{typeApplication,utilisateur}));
		
		return Finder.find(app, entityName, leSort, aQualifier);
	}
    
    static public NSArray findFonctionsApplication (ApplicationCocktail app, EOTypeApplication typeApplication)
    {
        String entityName = EOFonction.ENTITY_NAME;
        NSArray leSort = null;
        EOQualifier aQualifier = null;
        if(typeApplication==null)
            return new NSArray();
        // le sort 

        // le qualifier
        aQualifier = EOQualifier.qualifierWithQualifierFormat(EOFonction.TYPE_APPLICATION_KEY+" = %@",
                new NSArray(new Object[]{typeApplication}));
        
        return Finder.find(app, entityName, leSort, aQualifier);
    }

	
//	
//	static public NSArray findUtilisateurOrgans (ApplicationCocktail app,EOUtilisateur utilisateur,EOExercice exer)
//	{
//		
//		String entityName ="UtilisateurOrgan";
//		NSArray leSort = null;
//		EOQualifier aQualifier = null;
//
//		// le sort 
//
//		// le qualifier
//		aQualifier = EOQualifier.qualifierWithQualifierFormat("utilisateur = %@",
//				new NSArray(new Object[]{utilisateur}));
//
//		return Finder.find(app, entityName, leSort, aQualifier);
//	}
	
	/**
	 * @deprecated
	 */
	static public NSArray findUtilisateurComptes (ApplicationCocktail app,EOUtilisateur utilisateur)
	{
		
		String entityName ="ca_Compte";
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		// le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat("fonction.typeApplication = %@ and utilisateur = %@",
				new NSArray(new Object[]{utilisateur}));
		return Finder.find(app, entityName, leSort, aQualifier);
	}

	
	
	
	static public NSArray findUtilisateurOrgans (ApplicationCocktail app,EOUtilisateur utilisateur)
	{
		
		String entityName = EOUtilisateurOrgan.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		// le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurOrgan.UTILISATEUR_KEY+" = %@ ",
				new NSArray(new Object[]{utilisateur}));
		return Finder.find(app, entityName, leSort, aQualifier);
	}
	
	/**
	 * @param app
	 * @param utilisateur
	 * @return Les organs autorisés pour l'utilisateur
	 */
	static public NSArray findOrgans(ApplicationCocktail app, EOUtilisateur utilisateur)
	{

		String entityName = EOOrgan.ENTITY_NAME;
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrgan.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateurOrgan.UTILISATEUR_KEY + " = %@ ",
				new NSArray(new Object[] {
					utilisateur
				}));
		return Finder.find(app, entityName, leSort, aQualifier);
	}

}
