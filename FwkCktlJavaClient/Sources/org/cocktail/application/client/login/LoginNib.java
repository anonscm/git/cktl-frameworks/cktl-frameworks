/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)


package org.cocktail.application.client.login;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JLabelCocktail;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.application.palette.JPasswordFieldCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;


/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class LoginNib extends JPanelCocktail {
    private JPasswordFieldCocktail jPasswordFieldPasse;
    private JLabelCocktail jLabelInfosLogin;

    private JLabelCocktail jLabelIcone;
    private JButtonCocktail jButtonCocktailValider;
    private JButtonCocktail jButtonCocktailQuitter;
    private JLabelCocktail jLabelInfos;
    private JTextFieldCocktail jTextFieldUtilisateur;
    private JLabelCocktail jLabelPasse;
    private JLabelCocktail jLabelUtilisateur;
    /**
     * Auto-generated main method to display this 
     * JPanel inside a new JFrame.
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.getContentPane().add(new LoginNib());
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public LoginNib() {
        super();
        initGUI();
    }


    public LoginNib(LoginNibCtrl monCtrl) {
        super();
        initGUI();
    }

    private void initGUI() {
        try {
			GridBagLayout thisLayout = new GridBagLayout();
            thisLayout.rowWeights = new double[] {0.1, 0.0, 0.0, 0.1, 0.0};
            thisLayout.rowHeights = new int[] {20, 27, 26, 7, 21};
            thisLayout.columnWeights = new double[] {0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
            thisLayout.columnWidths = new int[] {20, 20, 20, 20, 20, 20};
            this.setLayout(thisLayout);

            {
                jLabelUtilisateur = new JLabelCocktail();
                this.add(jLabelUtilisateur, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
                jLabelUtilisateur.setText("Utilisateur : ");
            }
            {
                jLabelPasse = new JLabelCocktail();
                this.add(jLabelPasse, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
                jLabelPasse.setText("Mot de Passe : ");
            }
            {
                jTextFieldUtilisateur = new JTextFieldCocktail();
                this.add(jTextFieldUtilisateur, new GridBagConstraints(3, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
            }
            {
                jPasswordFieldPasse = new JPasswordFieldCocktail();
                this.add(jPasswordFieldPasse, new GridBagConstraints(3, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
            }
            {
                jLabelInfos = new JLabelCocktail();
                this.add(jLabelInfos, new GridBagConstraints(2, 0, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
                jLabelInfos.setText("...");
            }
			{
				jButtonCocktailQuitter = new JButtonCocktail();
				this.add(getJButtonCocktailQuitter(), new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailQuitter.setText("Quitter");
				jButtonCocktailQuitter.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				jButtonCocktailValider = new JButtonCocktail();
				this.add(getJButtonCocktailValider(), new GridBagConstraints(3, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailValider.setText("Valider");
				jButtonCocktailValider.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				jLabelIcone = new JLabelCocktail();
				this.add(jLabelIcone, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));
				jLabelIcone.setHorizontalAlignment(SwingConstants.CENTER);
				jLabelIcone.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				jLabelInfosLogin = new JLabelCocktail();
				this.add(getJLabelInfosLogin(), new GridBagConstraints(1, 4, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));
                jLabelInfosLogin.setVisible(true);
                jLabelInfosLogin.setText("");
			}

			this.setPreferredSize(new java.awt.Dimension(400, 154));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public JLabel getJLabelUtilisateur() {
        return jLabelUtilisateur;
    }

    public JLabel getJLabelPasse() {
        return jLabelPasse;
    }

    public JTextFieldCocktail getJTextFieldUtilisateur() {
        return jTextFieldUtilisateur;
    }





    public JPasswordFieldCocktail getJPasswordFieldPasse() {
        return jPasswordFieldPasse;
    }

    public JLabel getJLabelInfos() {
        return jLabelInfos;
    }
    
	public JButtonCocktail getJButtonCocktailQuitter() {
		return jButtonCocktailQuitter;
	}
	
	public JButtonCocktail getJButtonCocktailValider() {
		return jButtonCocktailValider;
	}
	
	public JLabel getJLabelIcone() {
		return jLabelIcone;
	}
	
	public JLabelCocktail getJLabelInfosLogin() {
		return jLabelInfosLogin;
	}

}
