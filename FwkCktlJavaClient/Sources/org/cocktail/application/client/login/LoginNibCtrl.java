/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.login;

import javax.swing.Icon;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.finder.FinderGrhum;
import org.cocktail.application.client.tools.UserInfoCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JPasswordFieldCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;
import org.cocktail.fwkcktlwebapp.common.CktlDockClient;
import org.cocktail.fwkcktlwebapp.common.util.CRIpto;

import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;

public class LoginNibCtrl  extends NibCtrl {

	private static final String AUTHENTIFIEZ_VOUS_SVP = "Authentifiez vous SVP .";
	private static final String ERRREUR_AUTHENTIFICATION="<html><body><center color=\"red\">Probleme d'authentification !</center></body></html>";
	private static final String ERRREUR_AUTHENTIFICATION_PASS_VIDE="<html><body><center color=\"red\">Probleme d'authentification (Pas de mot de passe) !</center></body></html>";
	private static final String ERRREUR_AUTHENTIFICATION_PASSWORD="<html><body><center color=\"red\">Probleme d'authentification (Mot de passe invalide) !</center></body></html>";
	private static final String ERRREUR_AUTHENTIFICATION_UTILISATEUR="<html><body><center color=\"red\">Probleme d'authentification (Utilisateur) !</center></body></html>";

	private static final String METHODE_QUIT = "quit";
	private static final String METHODE_VERIFIER_ACCESS = "verifierAccess";

	private LoginNib monLoginNib = null;

	private UserInfoCocktail _userInfos=null;
	private String _userName="";
	private boolean utiliserZap=true;
	private ApplicationCocktail myCtrl;

	public LoginNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y,boolean utiliserZap) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		myCtrl = ctrl;
		this.setUtiliserZap(utiliserZap);
	}

	public void creationFenetre(LoginNib leLoginNib,String title){
		super.creationFenetre( leLoginNib,title);
		setMonLoginNib(leLoginNib);
		getFrameMain().setTitle(AUTHENTIFIEZ_VOUS_SVP);
		bindingAndCustomization() ;
		getMonLoginNib().getJTextFieldUtilisateur().setText(app.userLogin());
	}

	//	Initialisation des boutons en swing
	private void bindingAndCustomization() {
		final EOClientResourceBundle leBundle = new EOClientResourceBundle();
		// Chargement des icones pour les boutons
		try {
			// cablage des buttons
			((JButtonCocktail) getMonLoginNib().getJButtonCocktailValider()).addDelegateActionListener(this,METHODE_VERIFIER_ACCESS);
			((JButtonCocktail) getMonLoginNib().getJButtonCocktailQuitter()).addDelegateActionListener( app,METHODE_QUIT);
			((JPasswordFieldCocktail) getMonLoginNib().getJPasswordFieldPasse()).addDelegateKeyListener( this,METHODE_VERIFIER_ACCESS);
			((JTextFieldCocktail) getMonLoginNib().getJTextFieldUtilisateur()).addDelegateKeyListener( this,METHODE_VERIFIER_ACCESS);
			// les images des buttons
			getMonLoginNib().getJButtonCocktailQuitter().setIcone("quitter");
			getMonLoginNib().getJButtonCocktailValider().setIcone("valider16");

			// les images des labels
			getMonLoginNib().getJLabelIcone().setIcon((Icon)leBundle.getObject("keylock"));

		}
		catch(Exception e) {
			trace(this+ "Exception"+ e.getMessage());
		}
	}


	public void afficherFenetre(int alocation_x , int alocation_y,int ataille_x , int ataille_y){
		super.afficherFenetre();
		setLocation_taille(alocation_x,alocation_y,ataille_x,ataille_y);
		ApplicationCocktail.setLocationCenter(getMonLoginNib(), NibCtrl.LOCATION_MIDDLE);
		getMonLoginNib().getJTextFieldUtilisateur().setText(app.userLogin());
	}


	public boolean afficherFenetreLogin()
	{	
		//	trace("afficherFenetreLogin 1");
		String tmpUserName = getCASUserName();

		if(!isUtiliserZap() || tmpUserName!=null) {
			// authentification ZAP reussie et recup des infos dans les dicos

			set_userName(tmpUserName);
			//trace("Authentification 2");
			EODistributedObjectStore magasinObjets = (EODistributedObjectStore) app.getAppEditingContext().parentObjectStore();
			//trace("Authentification 3");
			UserInfoCocktail infos = new UserInfoCocktail((NSDictionary) magasinObjets.invokeRemoteMethodWithKeyPath(
					app.getAppEditingContext(),
					"session.remoteCallDelagate",
					"clientSideRequestCheckPassword",
					new Class[] { String.class },
					new Object[] { get_userName() },
					false));
			//trace("Authentification 4");
			if(infos!=null) {
				//trace("Authentification 5");
				setUserInfos(infos);
				setUserName(get_userName());
				trace("Persid : "+get_userInfos().persId());
				verificationUtilisateur();
				return false;
			}	
		}
		return true;
	}

	/**
	 * 
	 */
	public void verifierAccess (){

		String tmpPassord = new String (monLoginNib.getJPasswordFieldPasse().getPassword());

		// le password ne peut pas etre vide !!!
		if (tmpPassord.length() > 0 ){

			checkPassword(monLoginNib.getJTextFieldUtilisateur().getText(),tmpPassord);
			
			if(verificationUtilisateur()) {
				masquerFenetre();
				app.accesOk();
			}
		}
		else
			getMonLoginNib().getJLabelInfosLogin().setText(ERRREUR_AUTHENTIFICATION_PASS_VIDE);
	}

	/**
	 * 
	 * @return
	 */
	private boolean verificationUtilisateur() {

		if(get_userInfos()==null) {
			return false;
		}

		if (myCtrl.getCurrentTypeApplication() == null) {
			if (get_userInfos().allValues().count() == 0){
				getMonLoginNib().getJLabelInfosLogin().setText(ERRREUR_AUTHENTIFICATION_UTILISATEUR);
				return false;
			}
			return true;
		}
		else {

			// recup de l utilisateur dans jefy_admin
			try {
				if (get_userInfos().allValues().count() > 0){
					EOUtilisateur user = FinderGrhum.findUtilisateurWithPersId(app,get_userInfos().persId());
					if (user != null) {
						app.setCurrentUtilisateur(user);
						return true;
					}
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		getMonLoginNib().getJLabelInfosLogin().setText(ERRREUR_AUTHENTIFICATION_UTILISATEUR);

		return false;

	}


	public void setInfos (String s)
	{
		getMonLoginNib().getJLabelInfos().setText(s);
	}

	public void trace (String s)
	{
		app.getToolsCocktailLogs().trace(s,isWithLogs());
	}

	//----------------------------- ZAPification -----------------------
	/**
	 * Retourne le nom d'utilisateur donne par la barre de lancement des
	 * applications (le Dock). Le Dock lui-meme recupere le login a partir du
	 * serveur CAS.
	 *
	 * <p>Retourne <i>null</i> si le Dock n'est pas disponible ou le login CAS ne
	 * peut pas etre recupere pour d'autres raisons. On suppose que dans ce cas
	 * l'utilisateur devra saisir le login explicitement.</p>
	 *
	 * <p>Cette methode utilise les parametres <code>LRAppDockPort</code> et
	 * <code>LRAppDockTicket</code> passes en ligne de commandes lors de
	 * lancement de l'applications par Dock. Ils correspondent respectivement
	 * au numero du port d'ecoute du Dock et le ticket (unique) a presenter
	 * au Dock.</p>
	 */
	private String getCASUserName() {
		NSDictionary arguments = app.arguments();
		String dockPort = (String)arguments.objectForKey("LRAppDockPort");
		String dockTicket = (String)arguments.objectForKey("LRAppDockTicket");
		String uname = null;
		if ((dockTicket != null) && (dockPort != null))
			uname = CktlDockClient.getNetID(null, dockPort, dockTicket);
		System.out.println("uname = "+ (uname==null ? " " : uname));
		return uname;
	} 

	/**
	 * 
	 * @param login
	 * @param passwrd
	 */
	private void checkPassword (String login , String passwrd) {
		
		//cas ou l'authentification par ZAP a reussi : on recupere les user infos sans afficher la fenetre de login
		EODistributedObjectStore magasinObjets = (EODistributedObjectStore) app.getAppEditingContext().parentObjectStore();
		UserInfoCocktail infos =null;
		try {
			infos = new UserInfoCocktail((NSDictionary)magasinObjets.invokeRemoteMethodWithKeyPath(
					app.getAppEditingContext(),
					"session.remoteCallDelagate",
					"clientSideRequestCheckPassword",
					new Class[] { String.class ,String.class },
					new Object[] { CRIpto.crypt(login),CRIpto.crypt(passwrd) },
					false));
			System.out.println("info = "+(infos==null ? "NULL":infos));
		} catch (Throwable e) {
			e.printStackTrace();
		}

		setUserInfos(new UserInfoCocktail(infos));
		
	}

	public void setUserInfos(UserInfoCocktail dico) {
		this.set_userInfos(dico);
		app.setUserInfos(dico);
	} 

	public void setUserName(String _userName) {
		this.set_userName(_userName);
	}

	void setMonLoginNib(LoginNib monLoginNib) {
		this.monLoginNib = monLoginNib;
	}

	LoginNib getMonLoginNib() {
		return monLoginNib;
	}

	void setUtiliserZap(boolean utiliserZap) {
		this.utiliserZap = utiliserZap;
	}

	boolean isUtiliserZap() {
		return utiliserZap;
	}

	protected void set_userInfos(UserInfoCocktail _userInfos) {
		this._userInfos = _userInfos;
	}

	protected UserInfoCocktail get_userInfos() {
		return _userInfos;
	}

	protected void set_userName(String _userName) {
		this._userName = _userName;
	}

	protected String get_userName() {
		return _userName;
	}

}