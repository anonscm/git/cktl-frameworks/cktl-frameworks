package org.cocktail.application.client.login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class SplashWindow extends JWindow {

	private ApplicationCocktail app = (ApplicationCocktail) EOApplication.sharedApplication();
	private static final EOClientResourceBundle leBundle = new EOClientResourceBundle();
	private JProgressBar progress;
	public static JFrame frame;

	public SplashWindow(String imgPath, String message, String icone) {
		frame = new JFrame("Chargement");
		frame.setIconImage(((ImageIcon) leBundle.getObject(imgPath)).getImage());// icone de la Jframe
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255,255,255,255));// Couleur de fond du Panel
		panel.setSize(300, 150);
		panel.setBounds(0, 0, 300, 150);
		JLabel texte = new JLabel(message);// Texte de la String
		texte.setForeground(Color.DARK_GRAY);

		ImageIcon icon = (ImageIcon) leBundle.getObject(imgPath);
		ImageImplement img = new ImageImplement(icon.getImage());
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		progress = new JProgressBar(0, 100);
		progress.setStringPainted(true);
		progress.setForeground(Color.DARK_GRAY);
		// ajout des elements
		texte.setText("Chargement... ");
		panel.add(BorderLayout.PAGE_START, texte);
		panel.add(BorderLayout.CENTER, img);
		panel.add(BorderLayout.PAGE_END, progress);
		
		frame.getContentPane().add(BorderLayout.CENTER, panel);
		frame.setSize(300, 150);
		// Pour definir le Splash au milieu de l'ecran'
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation((screen.width - frame.getSize().width) / 2, (screen.height - frame.getSize().height) / 2);
		// pour que ca ai vraiement l air d un splash :p
		frame.setUndecorated(true);
		frame.setVisible(true);
		frame.setResizable(false);
		// Retaillage de la barre pour qu elle corresponde a la taille de la
		// frame
	    progress.setBounds(new Rectangle(10, 220,430, 20));
		// Creation de thread pour afficher la progression de la barre
		for (int j = 1; j < 100; j++) {
			progress.setValue(j);
			progress.setString(j + " %");
			try {
				Thread.sleep(5);// determination de la rapiditee de la frame
				frame.paintAll(frame.getGraphics());// on force la mise a jour
													// de l'interface
			} catch (Exception e) {
				e.printStackTrace();
				frame.dispose();// en cas d' erreur pour pas rester bloqué sur
								// le splash
			}
		}
		frame.dispose();
	}

	class ImageImplement extends JPanel {

		private Image img;

		public ImageImplement(Image img) {
			this.img = img;
			Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
			setPreferredSize(size);
			setMinimumSize(size);
			setMaximumSize(size);
			setSize(size);
			setLayout(null);
		}

		public void paintComponent(Graphics g) {
			g.drawImage(img, 0, 0, null);
		}

	}

}
