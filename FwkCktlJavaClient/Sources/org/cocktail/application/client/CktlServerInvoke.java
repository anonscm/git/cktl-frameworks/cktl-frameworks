package org.cocktail.application.client;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;

public class CktlServerInvoke {

	protected static String getRemoteKeyPath() {
		return "session.cktlClientInvocation";
	}

	public static String clientSideRequestGetVersionFromJar(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestGetVersionFromJar", new Class[] {
		}, new Object[] {
		}, false);
	}

	public static Boolean clientSideRequestIsDevelopmentMode(EOEditingContext ec) {
		return (Boolean) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestIsDevelopmentMode", new Class[] {
				}, new Object[] {
				}, false);
	}

}
