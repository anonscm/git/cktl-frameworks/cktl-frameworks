/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.TransactionPanel;

import java.awt.Cursor;

import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.ModalInterfaceCocktail;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JPanelCocktail;

import java.awt.Rectangle;
import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class TransactionLogNibCtrl extends NibCtrl{

	private static final String METHODE_OK = "masquerFenetre";

	private TransactionLogNib monNib = null;
	public ApplicationCocktail app;
    public ModalInterfaceCocktail _parent;


	public TransactionLogNibCtrl(ApplicationCocktail app,int alocation_x , int alocation_y,int ataille_x , int ataille_y){
		super(app, alocation_x, alocation_y, ataille_x, ataille_y);
		this.app=app;
	}

	public void afficherUnLog(String mess,int i){
		getMonNib().getJTextAreaLogs().setText(getMonNib().getJTextAreaLogs().getText()+"\n"+mess);
        try {
            int longueur = getMonNib().getJTextAreaLogs().getDocument().getLength();
            Rectangle rectangle = getMonNib().getJTextAreaLogs().modelToView(longueur);
            rectangle.x = 0;
            getMonNib().getJTextAreaLogs().scrollRectToVisible(rectangle);
        } catch (Exception err) {
            err.printStackTrace();
        }
		getMonNib().getJProgressBar().setValue(i);
		getFrameMain().paintAll(getFrameMain().getGraphics());

	}
    
    public void rafficherUnLog(String oldMess,String newMess,int i){
        System.out.println("oldMess="+oldMess);
        System.out.println("newMess="+newMess);
        System.out.println("getMonNib().getJTextAreaLogs().getText() = "+getMonNib().getJTextAreaLogs().getText());
        System.out.println("index = "+getMonNib().getJTextAreaLogs().getText().indexOf(oldMess));
        System.out.println("subStr = "+getMonNib().getJTextAreaLogs().getText().substring(getMonNib().getJTextAreaLogs().getText().indexOf(oldMess)));
        getMonNib().getJTextAreaLogs().setText(getMonNib().getJTextAreaLogs().getText().substring(0,getMonNib().getJTextAreaLogs().getText().indexOf(oldMess))+newMess);
        try {
            int longueur = getMonNib().getJTextAreaLogs().getDocument().getLength();
            Rectangle rectangle = getMonNib().getJTextAreaLogs().modelToView(longueur);
            rectangle.x = 0;
            getMonNib().getJTextAreaLogs().scrollRectToVisible(rectangle);
        } catch (Exception err) {
            err.printStackTrace();
        }
        getMonNib().getJProgressBar().setValue(i);
        getFrameMain().paintAll(getFrameMain().getGraphics());
        getMonNib().getJTextAreaLogs().updateUI();
    }


	//	Initialisation des boutons en swing
	protected void bindingAndCustomization() {
		// Chargement des icones pour les boutons
		try {
			EOClientResourceBundle leBundle = new EOClientResourceBundle();
			// cablage des buttons
			((JButtonCocktail) getMonNib().getJButtonCocktailOk()).addDelegateActionListener(this,METHODE_OK);
			// les images des buttons
			getMonNib().getJButtonCocktailOk().setIcone("close_view");
			getMonNib().getJLabelImage().setIcon((ImageIcon)leBundle.getObject("waiting"));
		}
		catch(Exception e) {
			//trace(this+ "Exception"+ e.getMessage());
		}
	}


	public void creationFenetre(TransactionLogNib leNib,String title){
		super.creationFenetre( leNib,title);
		setMonNib(leNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();

	}


	public void afficherFenetre(){

		super.afficherFenetre();

		getMonNib().setCursor( Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) );
		activerLesGlassPane();
		getMonNib().getJProgressBar().setIndeterminate(false);
		getMonNib().getJButtonCocktailOk().setEnabled(false);
		getMonNib().getJTextAreaLogs().setText(null);
		getMonNib().setVisible(true);
		getFrameMain().setVisible(true);

	}
    
    public void afficherFenetre(ModalInterfaceCocktail parent){

        super.afficherFenetre();
        _parent = parent;
        getMonNib().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) );
        activerLesGlassPane();
        getMonNib().getJProgressBar().setIndeterminate(false);
        getMonNib().getJButtonCocktailOk().setEnabled(false);
        getMonNib().getJTextAreaLogs().setText(null);
        getMonNib().setVisible(true);
        getFrameMain().setVisible(true);

    }

	public void terminer (){
		getMonNib().setCursor( Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR) );
		getMonNib().getJProgressBar().setIndeterminate(false);
		getMonNib().getJButtonCocktailOk().setEnabled(true);
		getMonNib().setVisible(true);
		getFrameMain().toFront();
	}

	public void masquerFenetre()
	{
        if(_parent!=null)
            retirerLesGlassPane(_parent);
        else
            retirerLesGlassPane();
		getMonNib().setCursor( Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR) );
		getFrameMain().setVisible(false);
	}


	public TransactionLogNib getMonNib() {
		return monNib;
	}

	private void setMonNib(TransactionLogNib leNib) {
		monNib = leNib;

	}

	private void activerLesGlassPane(){
			app.activerLesGlassPane(getMonNib());
	}

	private void retirerLesGlassPane(ModalInterfaceCocktail finder){
		app.retirerLesGlassPane();
        app.activerLesGlassPane(finder);
	}
    
//    /**
//     * @deprecated utiliser retirerLesGlassPane(ModalInterfaceCocktail finder)
//     */
    private void retirerLesGlassPane(){
        app.retirerLesGlassPane();
    }

	public void swingFinderAnnuler(SwingFinder s) {
		

	}

	public void swingFinderTerminer(SwingFinder s) {
		

	}

}
