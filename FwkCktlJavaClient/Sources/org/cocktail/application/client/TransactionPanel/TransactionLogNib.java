/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.TransactionPanel;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.WindowConstants;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JLabelCocktail;
import org.cocktail.application.palette.JPanelCocktail;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class TransactionLogNib extends JPanelCocktail {
	private JButtonCocktail jButtonCocktailOk;
	private JScrollPane jScrollPaneLogs;
	private JProgressBar jProgressBar;
	private JLabelCocktail jLabelImage;
	private JLabelCocktail jLabelInfos;
	private JTextArea jTextAreaLogs;

	/**
	* Auto-generated main method to display this JDialog
	*/
	public static void main(String[] args) {
//		JFrame frame = new JFrame();
//		TransactionLogNib inst = new TransactionLogNib(frame);
//		inst.setVisible(true);
        JFrame frame = new JFrame();
        frame.getContentPane().add(new TransactionLogNib());
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
	}
	


		
	public TransactionLogNib() {
		//super(frame);
		super();
		initGUI();
	}
	
	private void initGUI() {
		try {
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.0, 0.0, 0.1, 0.1, 0.0, 0.0, 0.1};
			thisLayout.rowHeights = new int[] {14, 16, 20, 7, 105, 11, 7};
			thisLayout.columnWeights = new double[] {0.1, 0.1, 0.1, 0.1, 0.1};
			thisLayout.columnWidths = new int[] {7, 20, 7, 7, 7};
			//getContentPane().setLayout(thisLayout);
			setLayout(thisLayout);
			//this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			{
				jButtonCocktailOk = new JButtonCocktail();
				add(getJButtonCocktailOk(), new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
				jButtonCocktailOk.setText("Fermer");
			}
			{
				jScrollPaneLogs = new JScrollPane();
				add(getJScrollPaneLogs(), new GridBagConstraints(1, 2, 3, 3, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
				{
					jTextAreaLogs = new JTextArea();
					jScrollPaneLogs.setViewportView(getJTextAreaLogs());
                    jScrollPaneLogs.setAutoscrolls(true);
                    jScrollPaneLogs.setWheelScrollingEnabled(true);
					jTextAreaLogs.setEditable(false);
					jTextAreaLogs.setFont(new java.awt.Font("Tahoma",0,9));
//                    /** On souhaite un retour à ligne automatique : */
//                    jTextAreaLogs.setLineWrap(true);
//                    /** On souhaite que les mots ne soient pas coupés : */
//                    jTextAreaLogs.setWrapStyleWord(true);
				}
			}
			{
				jProgressBar = new JProgressBar();
				add(getJProgressBar(), new GridBagConstraints(1, 5, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
			}
			{
				jLabelImage = new JLabelCocktail();
				add(getJLabelImage(), new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
			}
			{
				jLabelInfos = new JLabelCocktail();
				add(getJLabelInfos(), new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
				jLabelInfos.setText("Transactions ...");
			}
			this.setSize(400, 300);
			this.setPreferredSize( new Dimension(400, 300));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JButtonCocktail getJButtonCocktailOk() {
		return jButtonCocktailOk;
	}
	
	public JScrollPane getJScrollPaneLogs() {
		return jScrollPaneLogs;
	}
	
	public JProgressBar getJProgressBar() {
		return jProgressBar;
	}
	
	public JLabel getJLabelImage() {
		return jLabelImage;
	}
	
	public JTextArea getJTextAreaLogs() {
		return jTextAreaLogs;
	}
	
	public JLabel getJLabelInfos() {
		return jLabelInfos;
	}


}
