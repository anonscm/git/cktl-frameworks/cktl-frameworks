/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.client.TransactionPanel;

import java.lang.reflect.Method;


public abstract class LoadingThreadTransactionLogNib {//extends Thread {

	private TransactionLogNib _dialog;
	private Object  delegate;
	private String methode;
	private boolean progessBar =false;
	/**
	* Implementation de la méthode run() pour le Thread
	*/
	public void run(){
//    _dialog.getJProgressBar().setIndeterminate(isProgessBar());
//	_dialog.getJButtonCocktailOk().setEnabled(false);
//	_dialog.getJTextAreaLogs().setText(null);
//	_dialog.setVisible(true);

	}
//
//
//	public void setDialog(TransactionLogNib dialog){
//	_dialog = dialog;
//	}
//
//	void setDelegate(Object delegate) {
//		this.delegate = delegate;
//	}
//
//	Object getDelegate() {
//		return delegate;
//	}
//
//	void setMethode(String methode) {
//		this.methode = methode;
//	}
//
//	String getMethode() {
//		return methode;
//	}
//	
//	
//	
//	void callMethode(Object  delegate,String methode)
//	{
//		// try du call
//		try {
//			Class c;
//			c = delegate.getClass();
//			// Recupereation de la methode getXxxxx :
//			Method call = c.getMethod(methode, null);
//			// appel de la methode
//			call.invoke(delegate, null);
//
//		} catch (Exception ex) {
//		//	System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
//		}
//
//	}
//
//
//	public boolean isProgessBar() {
//		return progessBar;
//	}
//
//
//	public void setProgessBar(boolean progessBar) {
//		this.progessBar = progessBar;
//	}
}



