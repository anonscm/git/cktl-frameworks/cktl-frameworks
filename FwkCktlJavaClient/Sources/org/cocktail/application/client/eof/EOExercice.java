

// EOExercice.java
// 
package org.cocktail.application.client.eof;

import org.cocktail.application.common.utilities.CocktailFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;



public class EOExercice extends _EOExercice {
	
	private static final String EXERCICE_DEMARRAGE = "2004";
	public static final String	PRIMARY_KEY_KEY	= "exeOrdre";
	public static NSArray SORT_ARRAY_EXER_DESC = new NSArray(new EOSortOrdering(EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending));

	public static final String	EXERCICE_OUVERT = "O";
	public static final String	EXERCICE_RESTREINT = "R";
	public static final String	EXERCICE_PREPARATION = "P";
	public static final String	EXERCICE_CLOS = "C";

    public EOExercice() {
        super();
    }
    public String toString() {
    	return String.valueOf(exeExercice());
    }
    
	public static NSArray<EOExercice> findExercices(EOEditingContext ec)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(
				EOExercice.EXE_EXERCICE_KEY + " >= " + EXERCICE_DEMARRAGE + " and (" + 
				EOExercice.EXE_STAT_KEY  + " = 'C' or " +
				EOExercice.EXE_STAT_KEY  + " = 'R' or " +
				EOExercice.EXE_STAT_KEY  + " = 'O' or " +
				EOExercice.EXE_STAT_KEY  + " = 'P')", null);
		
		EOFetchSpecification fs = new EOFetchSpecification(EOExercice.ENTITY_NAME, myQualifier, SORT_ARRAY_EXER_DESC);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	/** 
	 * 
	 * @param ec
	 * @return
	 */
	public static EOExercice findExercice(EOEditingContext edc, Number exercice)	{
		try {			
			return fetchFirstByQualifier(edc, CocktailFinder.getQualifierEqual(EXE_EXERCICE_KEY, exercice));
		}
		catch (Exception e)	{
			return null;
		}
	}
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static EOExercice exercicePrecedent(EOEditingContext edc, EOExercice exercice)	{
		try {
			int exercicePrecedent = exercice.exeExercice().intValue() - 1;			
			return fetchFirstByQualifier(edc, CocktailFinder.getQualifierEqual(EXE_EXERCICE_KEY, new Integer(exercicePrecedent)));
		}
		catch (Exception e)	{return null;}
	}

	/**
	 * 
	 * @param edc
	 * @return
	 */
	public static EOExercice exerciceCourant(EOEditingContext edc)	{
		try {
			return fetchFirstByQualifier(edc, CocktailFinder.getQualifierEqual(EXE_STAT_KEY, "O"));
		}
		catch (Exception e)	{return null;}
	}
}
