

// EOOrgan.java
// 
package org.cocktail.application.client.eof;

import com.webobjects.eocontrol.EOSortOrdering;



public class EOOrgan extends _EOOrgan
{

	public static final Integer ORG_NIV_0 = new Integer(0);
	public static final Integer ORG_NIV_1 = new Integer(1);
	public static final Integer ORG_NIV_2 = new Integer(2);
	public static final Integer ORG_NIV_3 = new Integer(3);
	public static final Integer ORG_NIV_4 = new Integer(4);

	public static final int ORG_NIV_MAX = EOOrgan.ORG_NIV_4.intValue();    
	public static final String LONG_STRING_KEY  = "longString";

	public static final EOSortOrdering SORT_ORG_UNIV_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UNIV_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_ETAB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_ETAB_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_UB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UB_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_CR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_CR_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_SOUSCR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending);

	public static final String ORG_NIV_0_LIB = "UNIVERSITE";
	public static final String ORG_NIV_1_LIB = "ETABLISSEMENT";
	public static final String ORG_NIV_2_LIB = "UB";
	public static final String ORG_NIV_3_LIB = "CR";
	public static final String ORG_NIV_4_LIB = "SOUS CR";

	public static final Object ERREUR_STRUCTURE_VIDE = "Une structure de l'annuaire devrait être associée.";
	public static final String ERREUR_SIGNATAIRE_PRINCIPAL="Un signataire principal valide est nécessaire au niveau le plus haut de l'organigramme budgétaire. Ce signataire principal doit être l'unique signataire.";


	public EOOrgan() {
		super();
	}

	/**
	 * Renvoie selon le niveau le bon champ (orgUniv ou orgEtab ou orgUb, etc.)
	 * @param s
	 */
	public final String getShortString() {
		final int niv = orgNiveau().intValue();
		String res = null;
		switch (niv) {
		case 0 :  
			res = orgUniv();
			break;
		case 1 :  
			res = orgEtab();
			break;

		case 2 :  
			res = orgUb();
			break;

		case 3 :  
			res = orgCr();
			break;

		case 4 :  
			res = orgSouscr();
			break;

		default:
			break;
		}
		return res;
	}


	/**
	 * Renvoie le libelle de la ligne budgetaire
	 * @return
	 */
	public final String getLongString() {

		String tmp =  orgEtab();
		if (orgUb()!=null )
			tmp = tmp + " / " + orgUb();
		if (orgCr()!=null) {
			tmp = tmp + " / "+orgCr();
		}
		if (orgSouscr()!=null) {
			tmp = tmp + " / "+orgSouscr();
		}
		return tmp;
	}

}
