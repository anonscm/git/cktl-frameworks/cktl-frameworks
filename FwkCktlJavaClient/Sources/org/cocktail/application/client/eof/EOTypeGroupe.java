

// EOTypeGroupe.java
// 
package org.cocktail.application.client.eof;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.*;

public class EOTypeGroupe extends _EOTypeGroupe
{
    private static EOTypeGroupe _TOUS;
    
    public static EOTypeGroupe getTypeGroupeTous(){
        if(_TOUS==null)
        {
            EOClassDescription.classDescriptionForEntityName("ca_TypeGroupe");
            _TOUS = new EOTypeGroupe();
            _TOUS.setTgrpLibelle("Tous");
        }
        return _TOUS;
    }

    public EOTypeGroupe() {
        super();
    }

}
