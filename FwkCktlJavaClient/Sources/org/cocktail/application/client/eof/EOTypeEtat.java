

// EOTypeEtat.java
// 
package org.cocktail.application.client.eof;



public class EOTypeEtat extends _EOTypeEtat
{
    public static final String ETAT_VALIDE="VALIDE"; 
    public static final String ETAT_ANNULE="ANNULE";
    
    public EOTypeEtat() {
        super();
    }

}
