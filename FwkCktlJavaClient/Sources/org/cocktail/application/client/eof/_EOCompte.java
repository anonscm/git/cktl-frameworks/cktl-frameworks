// _EOCompte.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCompte.java instead.
package org.cocktail.application.client.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCompte extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ca_Compte";
	public static final String ENTITY_TABLE_NAME = "GRHUM.COMPTE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cptOrdre";

	public static final String ADRESSE_MAIL_COMPLETE_KEY = "adresseMailComplete";
	public static final String CPT_CHARTE_KEY = "cptCharte";
	public static final String CPT_CONNEXION_KEY = "cptConnexion";
	public static final String CPT_DOMAINE_KEY = "cptDomaine";
	public static final String CPT_EMAIL_KEY = "cptEmail";
	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String CPT_PASSWD_KEY = "cptPasswd";
	public static final String CPT_UID_GID_KEY = "cptUidGid";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_KEY = "persId";

// Attributs non visibles
	public static final String CPT_VLAN_KEY = "cptVlan";

//Colonnes dans la base de donnees
	public static final String ADRESSE_MAIL_COMPLETE_COLKEY = "$attribute.columnName";
	public static final String CPT_CHARTE_COLKEY = "CPT_CHARTE";
	public static final String CPT_CONNEXION_COLKEY = "CPT_CONNEXION";
	public static final String CPT_DOMAINE_COLKEY = "CPT_DOMAINE";
	public static final String CPT_EMAIL_COLKEY = "CPT_EMAIL";
	public static final String CPT_LOGIN_COLKEY = "CPT_LOGIN";
	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String CPT_PASSWD_COLKEY = "CPT_PASSWD";
	public static final String CPT_UID_GID_COLKEY = "CPT_UID_GID";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String CPT_VLAN_COLKEY = "CPT_VLAN";


	// Relationships
	public static final String REPART_COMPTES_KEY = "repartComptes";



	// Accessors methods
  public String adresseMailComplete() {
    return (String) storedValueForKey(ADRESSE_MAIL_COMPLETE_KEY);
  }

  public void setAdresseMailComplete(String value) {
    takeStoredValueForKey(value, ADRESSE_MAIL_COMPLETE_KEY);
  }

  public String cptCharte() {
    return (String) storedValueForKey(CPT_CHARTE_KEY);
  }

  public void setCptCharte(String value) {
    takeStoredValueForKey(value, CPT_CHARTE_KEY);
  }

  public String cptConnexion() {
    return (String) storedValueForKey(CPT_CONNEXION_KEY);
  }

  public void setCptConnexion(String value) {
    takeStoredValueForKey(value, CPT_CONNEXION_KEY);
  }

  public String cptDomaine() {
    return (String) storedValueForKey(CPT_DOMAINE_KEY);
  }

  public void setCptDomaine(String value) {
    takeStoredValueForKey(value, CPT_DOMAINE_KEY);
  }

  public String cptEmail() {
    return (String) storedValueForKey(CPT_EMAIL_KEY);
  }

  public void setCptEmail(String value) {
    takeStoredValueForKey(value, CPT_EMAIL_KEY);
  }

  public String cptLogin() {
    return (String) storedValueForKey(CPT_LOGIN_KEY);
  }

  public void setCptLogin(String value) {
    takeStoredValueForKey(value, CPT_LOGIN_KEY);
  }

  public Integer cptOrdre() {
    return (Integer) storedValueForKey(CPT_ORDRE_KEY);
  }

  public void setCptOrdre(Integer value) {
    takeStoredValueForKey(value, CPT_ORDRE_KEY);
  }

  public String cptPasswd() {
    return (String) storedValueForKey(CPT_PASSWD_KEY);
  }

  public void setCptPasswd(String value) {
    takeStoredValueForKey(value, CPT_PASSWD_KEY);
  }

  public Integer cptUidGid() {
    return (Integer) storedValueForKey(CPT_UID_GID_KEY);
  }

  public void setCptUidGid(Integer value) {
    takeStoredValueForKey(value, CPT_UID_GID_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public NSArray repartComptes() {
    return (NSArray)storedValueForKey(REPART_COMPTES_KEY);
  }

  public NSArray repartComptes(EOQualifier qualifier) {
    return repartComptes(qualifier, null, false);
  }

  public NSArray repartComptes(EOQualifier qualifier, boolean fetch) {
    return repartComptes(qualifier, null, fetch);
  }

  public NSArray repartComptes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.application.client.eof.EORepartCompte.COMPTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.application.client.eof.EORepartCompte.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartComptes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartComptesRelationship(org.cocktail.application.client.eof.EORepartCompte object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_COMPTES_KEY);
  }

  public void removeFromRepartComptesRelationship(org.cocktail.application.client.eof.EORepartCompte object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_COMPTES_KEY);
  }

  public org.cocktail.application.client.eof.EORepartCompte createRepartComptesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_RepartCompte");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_COMPTES_KEY);
    return (org.cocktail.application.client.eof.EORepartCompte) eo;
  }

  public void deleteRepartComptesRelationship(org.cocktail.application.client.eof.EORepartCompte object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_COMPTES_KEY);
  }

  public void deleteAllRepartComptesRelationships() {
    Enumeration objects = repartComptes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartComptesRelationship((org.cocktail.application.client.eof.EORepartCompte)objects.nextElement());
    }
  }


  public static EOCompte createca_Compte(EOEditingContext editingContext, String cptCharte
, String cptConnexion
, String cptLogin
, Integer cptOrdre
, Integer cptUidGid
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persId
) {
    EOCompte eo = (EOCompte) createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME);    
		eo.setCptCharte(cptCharte);
		eo.setCptConnexion(cptConnexion);
		eo.setCptLogin(cptLogin);
		eo.setCptOrdre(cptOrdre);
		eo.setCptUidGid(cptUidGid);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersId(persId);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCompte.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCompte.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOCompte creerInstance(EOEditingContext editingContext) {
		  		EOCompte object = (EOCompte)createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOCompte localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCompte)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCompte localInstanceIn(EOEditingContext editingContext, EOCompte eo) {
    EOCompte localInstance = (eo == null) ? null : (EOCompte)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCompte#localInstanceIn a la place.
   */
	public static EOCompte localInstanceOf(EOEditingContext editingContext, EOCompte eo) {
		return EOCompte.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCompte fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCompte fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCompte eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCompte fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCompte fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCompte eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCompte fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCompte eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCompte ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCompte fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
