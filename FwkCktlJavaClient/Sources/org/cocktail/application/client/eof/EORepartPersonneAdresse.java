

// EORepartPersonneAdresse.java
// 
package org.cocktail.application.client.eof;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.*;

public class EORepartPersonneAdresse extends _EORepartPersonneAdresse
{
	public static final EOQualifier QUALIFIER_ADRESSE_FACT = 
		EOQualifier.qualifierWithQualifierFormat("tadrCode = 'FACT'", null);
	public static final EOQualifier QUALIFIER_ADRESSE_PRO = 
		EOQualifier.qualifierWithQualifierFormat("tadrCode = 'PRO'", null);
	public static final EOQualifier QUALIFIER_ADRESSE_PERSO = 
		EOQualifier.qualifierWithQualifierFormat("tadrCode = 'PERSO'", null);
	public static final EOQualifier QUALIFIER_ADRESSE_VALIDE = 
		EOQualifier.qualifierWithQualifierFormat("rpaValide = 'O'", null);
	public static final EOQualifier QUALIFIER_ADRESSE_PRINCIPALE = 
		EOQualifier.qualifierWithQualifierFormat("rpaPrincipal = 'O'", null);	
	public static final EOQualifier QUALIFIER_ADRESSE_PRO_PRINCIPALE = 
		EOQualifier.qualifierWithQualifierFormat("rpaValide = 'O' and tadrCode = 'PRO' and rpaPrincipal = 'O'", null);	
	public static final EOQualifier QUALIFIER_ADRESSE_FACT_PRINCIPALE = 
		EOQualifier.qualifierWithQualifierFormat("rpaValide = 'O' and tadrCode = 'FACT' and rpaPrincipal = 'O'", null);
	public static final EOQualifier QUALIFIER_ADRESSE_PERSO_PRINCIPALE = 
		EOQualifier.qualifierWithQualifierFormat("rpaValide = 'O' and tadrCode = 'PERSO' and rpaPrincipal = 'O'", null);


    public EORepartPersonneAdresse() {
        super();
    }

}
