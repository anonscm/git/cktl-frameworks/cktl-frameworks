

// EOAdresse.java
// 
package org.cocktail.application.client.eof;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.*;

public class EOAdresse extends _EOAdresse
{

    public EOAdresse() {
        super();
    }
    
    public String afficheAdresse(){
        StringBuffer str = new StringBuffer();
        str.append(adrAdresse1());
        if(adrAdresse2()!=null)
            str.append(adrAdresse2());
        if(codePostal()!=null)
            str.append(" ").append(codePostal());
        if(ville() !=null)
            str.append(" ").append(ville());
        return str.toString();
    }

}
