

// EOPersonne.java
// 
package org.cocktail.application.client.eof;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;



public class EOPersonne extends _EOPersonne
{

    public EOPersonne() {
        super();
    }


    /**
     * 
     * @param principalesOuRien
     * @return
     */
    public NSArray adressesProfessionelles(boolean principalesOuRien) {
    	
    	NSArray repartPersonneAdresses = 
    		EOQualifier.filteredArrayWithQualifier(
    				repartPersonneAdresses(), EORepartPersonneAdresse.QUALIFIER_ADRESSE_PRO_PRINCIPALE);
    	
    	if (repartPersonneAdresses.count() == 0 && !principalesOuRien)
    		repartPersonneAdresses = 
        		EOQualifier.filteredArrayWithQualifier(repartPersonneAdresses(), EORepartPersonneAdresse.QUALIFIER_ADRESSE_PRO);
    	
    	return (NSArray) repartPersonneAdresses.valueForKey("adresse");
    }
    /**
     * 
     * @param principalesOuRien
     * @return
     */
    public NSArray adressesFacturation(boolean principalesOuRien) {
    	
    	NSArray repartPersonneAdresses = 
    		EOQualifier.filteredArrayWithQualifier(repartPersonneAdresses(), EORepartPersonneAdresse.QUALIFIER_ADRESSE_FACT_PRINCIPALE);
    	
    	if (repartPersonneAdresses.count() == 0 && !principalesOuRien)
    		repartPersonneAdresses = 
        		EOQualifier.filteredArrayWithQualifier(repartPersonneAdresses(), EORepartPersonneAdresse.QUALIFIER_ADRESSE_FACT);
    	
    	return (NSArray) repartPersonneAdresses.valueForKey("adresse");
    }
    /**
     * 
     * @param principalesOuRien
     * @return
     */
    public NSArray adressesPersonnelles(boolean principalesOuRien) {
    	
    	NSArray repartPersonneAdresses = 
    		EOQualifier.filteredArrayWithQualifier(
    				repartPersonneAdresses(), EORepartPersonneAdresse.QUALIFIER_ADRESSE_PERSO_PRINCIPALE);
    	
    	if (repartPersonneAdresses.count() == 0 && !principalesOuRien)
    		repartPersonneAdresses = 
        		EOQualifier.filteredArrayWithQualifier(repartPersonneAdresses(), EORepartPersonneAdresse.QUALIFIER_ADRESSE_PERSO);
    	
    	return (NSArray) repartPersonneAdresses.valueForKey("adresse");
    }
    
    /**
     * 
     * @return
     */
    public NSArray mailProfessionnel(boolean principalOuRien) {
    	NSArray adressesProfessionelles = adressesProfessionelles(principalOuRien);
    	return (NSArray) adressesProfessionelles.valueForKey("eMail");
    }
    /**
     * 
     * @return
     */
    public NSArray mailPersonnel(boolean principalOuRien) {
    	NSArray adressesPersonnelles = adressesPersonnelles(principalOuRien);
    	return (NSArray) adressesPersonnelles.valueForKey("eMail");
    }
    
    /**
     * @see com.webobjects.eocontrol.EOCustomObject#toString()
     */
    public String toString() {
    	return persLc()+" "+persLibelle();
    }
}
