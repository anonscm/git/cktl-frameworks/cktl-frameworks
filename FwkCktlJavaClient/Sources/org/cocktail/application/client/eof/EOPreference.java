

// EOPreference.java
// 
package org.cocktail.application.client.eof;



import com.webobjects.foundation.NSValidation;

public class EOPreference extends _EOPreference {

	public static final String ID_PREF_STRUCTURE = "C_STRUCTURE";

	public static final String ID_PREF_STRUCTURE_SIRET = "C_STRUCTURE_SIRET";

	public static final String ID_PREF_TYPE_CREDIT = "TCD_ORDRE";

	public static final String ID_PREF_DURABILITE_RUBRIQUES = "DURABILITE_RUBRIQUES";

	public static final String ID_PREF_NIV_IMP_BDX = "NIV_IMP_BORDEREAUX";

	public EOPreference() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
