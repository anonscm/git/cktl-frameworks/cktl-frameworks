

// VPersonneTelephone.java
// 
package org.cocktail.application.client.eof;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.*;

public class VPersonneTelephone extends _VPersonneTelephone
{

    public VPersonneTelephone() {
        super();
    }
    
    public String typeTelephone(){
        if("PRF".equals(typeTel()))
            return "Professionnel";
        if("INT".equals(typeTel()))
            return "Interne";
        return "Privee";
    }

}
