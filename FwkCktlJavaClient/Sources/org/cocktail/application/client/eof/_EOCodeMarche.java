// _EOCodeMarche.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCodeMarche.java instead.
package org.cocktail.application.client.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCodeMarche extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ca_CodeMarche";
	public static final String ENTITY_TABLE_NAME = "jefy_marches.CODE_MARCHE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cmOrdre";

	public static final String CM_CODE_KEY = "cmCode";
	public static final String CM_DETAIL_KEY = "cmDetail";
	public static final String CM_LIB_KEY = "cmLib";
	public static final String CM_LIB_COURT_KEY = "cmLibCourt";
	public static final String CM_NIVEAU_KEY = "cmNiveau";
	public static final String CM_PERE_KEY = "cmPere";
	public static final String CM_SUPPR_KEY = "cmSuppr";
	public static final String TCM_ID_KEY = "tcmId";

// Attributs non visibles
	public static final String CM_ORDRE_KEY = "cmOrdre";

//Colonnes dans la base de donnees
	public static final String CM_CODE_COLKEY = "CM_CODE";
	public static final String CM_DETAIL_COLKEY = "CM_DETAIL";
	public static final String CM_LIB_COLKEY = "CM_LIB";
	public static final String CM_LIB_COURT_COLKEY = "CM_LIB_COURT";
	public static final String CM_NIVEAU_COLKEY = "CM_NIVEAU";
	public static final String CM_PERE_COLKEY = "CM_PERE";
	public static final String CM_SUPPR_COLKEY = "CM_SUPPR";
	public static final String TCM_ID_COLKEY = "TCM_ID";

	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";


	// Relationships
	public static final String CODE_MARCHES_FILS_KEY = "codeMarchesFils";
	public static final String TYPE_CODE_MARCHE_KEY = "typeCodeMarche";



	// Accessors methods
  public String cmCode() {
    return (String) storedValueForKey(CM_CODE_KEY);
  }

  public void setCmCode(String value) {
    takeStoredValueForKey(value, CM_CODE_KEY);
  }

  public String cmDetail() {
    return (String) storedValueForKey(CM_DETAIL_KEY);
  }

  public void setCmDetail(String value) {
    takeStoredValueForKey(value, CM_DETAIL_KEY);
  }

  public String cmLib() {
    return (String) storedValueForKey(CM_LIB_KEY);
  }

  public void setCmLib(String value) {
    takeStoredValueForKey(value, CM_LIB_KEY);
  }

  public String cmLibCourt() {
    return (String) storedValueForKey(CM_LIB_COURT_KEY);
  }

  public void setCmLibCourt(String value) {
    takeStoredValueForKey(value, CM_LIB_COURT_KEY);
  }

  public Integer cmNiveau() {
    return (Integer) storedValueForKey(CM_NIVEAU_KEY);
  }

  public void setCmNiveau(Integer value) {
    takeStoredValueForKey(value, CM_NIVEAU_KEY);
  }

  public Integer cmPere() {
    return (Integer) storedValueForKey(CM_PERE_KEY);
  }

  public void setCmPere(Integer value) {
    takeStoredValueForKey(value, CM_PERE_KEY);
  }

  public String cmSuppr() {
    return (String) storedValueForKey(CM_SUPPR_KEY);
  }

  public void setCmSuppr(String value) {
    takeStoredValueForKey(value, CM_SUPPR_KEY);
  }

  public Integer tcmId() {
    return (Integer) storedValueForKey(TCM_ID_KEY);
  }

  public void setTcmId(Integer value) {
    takeStoredValueForKey(value, TCM_ID_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord typeCodeMarche() {
    return (com.webobjects.eocontrol.EOGenericRecord)storedValueForKey(TYPE_CODE_MARCHE_KEY);
  }

  public void setTypeCodeMarcheRelationship(com.webobjects.eocontrol.EOGenericRecord value) {
    if (value == null) {
    	com.webobjects.eocontrol.EOGenericRecord oldValue = typeCodeMarche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CODE_MARCHE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CODE_MARCHE_KEY);
    }
  }
  
  public NSArray codeMarchesFils() {
    return (NSArray)storedValueForKey(CODE_MARCHES_FILS_KEY);
  }

  public NSArray codeMarchesFils(EOQualifier qualifier) {
    return codeMarchesFils(qualifier, null);
  }

  public NSArray codeMarchesFils(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = codeMarchesFils();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToCodeMarchesFilsRelationship(org.cocktail.application.client.eof.EOCodeMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODE_MARCHES_FILS_KEY);
  }

  public void removeFromCodeMarchesFilsRelationship(org.cocktail.application.client.eof.EOCodeMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_MARCHES_FILS_KEY);
  }

  public org.cocktail.application.client.eof.EOCodeMarche createCodeMarchesFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_CodeMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODE_MARCHES_FILS_KEY);
    return (org.cocktail.application.client.eof.EOCodeMarche) eo;
  }

  public void deleteCodeMarchesFilsRelationship(org.cocktail.application.client.eof.EOCodeMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_MARCHES_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodeMarchesFilsRelationships() {
    Enumeration objects = codeMarchesFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodeMarchesFilsRelationship((org.cocktail.application.client.eof.EOCodeMarche)objects.nextElement());
    }
  }


  public static EOCodeMarche createca_CodeMarche(EOEditingContext editingContext, String cmCode
, String cmLib
, Integer tcmId
, com.webobjects.eocontrol.EOGenericRecord typeCodeMarche) {
    EOCodeMarche eo = (EOCodeMarche) createAndInsertInstance(editingContext, _EOCodeMarche.ENTITY_NAME);    
		eo.setCmCode(cmCode);
		eo.setCmLib(cmLib);
		eo.setTcmId(tcmId);
    eo.setTypeCodeMarcheRelationship(typeCodeMarche);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCodeMarche.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCodeMarche.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOCodeMarche creerInstance(EOEditingContext editingContext) {
		  		EOCodeMarche object = (EOCodeMarche)createAndInsertInstance(editingContext, _EOCodeMarche.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOCodeMarche localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCodeMarche)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCodeMarche localInstanceIn(EOEditingContext editingContext, EOCodeMarche eo) {
    EOCodeMarche localInstance = (eo == null) ? null : (EOCodeMarche)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCodeMarche#localInstanceIn a la place.
   */
	public static EOCodeMarche localInstanceOf(EOEditingContext editingContext, EOCodeMarche eo) {
		return EOCodeMarche.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCodeMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCodeMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCodeMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
