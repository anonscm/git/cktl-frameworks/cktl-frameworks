/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 26 avr. 07
 * author mparadot 
 */
package org.cocktail.application.client.nibfinder;

import org.cocktail.application.client.eof.EOLolfNomenclatureAbstract;
import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOLolfNomenclatureRecette;
import org.cocktail.application.palette.interfaces.JTreeObjectCocktail;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.ModalInterfaceCocktail;
import org.cocktail.application.nibctrl.NibCtrl;

public class NibFinderLolfNomanclature extends NibFinder {
    NibFinderLolfNomanclatureIC monNibFinderLolfNomanclatureInterfaceController; 
    NibFinderLolftICTreeView monNibFinderLolftICTreeView;
    /**
     * @deprecated
     */
    public static final int OPTION_LOLF_RECETTE_ET_DEPENSE=0;
    public static final int OPTION_LOLF_RECETTE_SEUL=1;
    public static final int OPTION_LOLF_DEPENSE_SEUL=2;
    public static String ENTITY_DEPENSE=EOLolfNomenclatureDepense.ENTITY_NAME;//"ca_LolfNomenclatureDepense";
    public static String ENTITY_RECETTE=EOLolfNomenclatureRecette.ENTITY_NAME;//"ca_LolfNomenclatureRecette";
    
    private String entityEnCours;
    private boolean useTreeView;
    
    /**
     * @deprecated
     * @param substitutionEditingContext
     * @param responder
     * @param ismodal
     * @param obtionGraphique
     */
    public NibFinderLolfNomanclature(EOEditingContext substitutionEditingContext, EOInterfaceControllerCocktail responder, boolean ismodal, int obtionGraphique) {
        this(substitutionEditingContext, (ModalInterfaceCocktail)responder, ismodal, obtionGraphique, false);
    }
    
    public NibFinderLolfNomanclature(EOEditingContext substitutionEditingContext, ModalInterfaceCocktail responder, boolean ismodal, int obtionGraphique) {
        this(substitutionEditingContext, responder, ismodal, obtionGraphique, false);
    }
    
    public NibFinderLolfNomanclature(EOEditingContext substitutionEditingContext, NibCtrl responder, boolean ismodal, int obtionGraphique) {
        this(substitutionEditingContext, responder, ismodal, obtionGraphique, false);
    }
    
    public NibFinderLolfNomanclature(EOEditingContext substitutionEditingContext, ModalInterfaceCocktail responder, boolean ismodal, int optionGraphique,boolean useTreeView) {
        super(substitutionEditingContext, responder, ismodal);
        this.useTreeView = useTreeView;
//        String type = "";
//        if(!useTreeView)
//        {
//            monNibFinderLolfNomanclatureInterfaceController = new NibFinderLolfNomanclatureIC(substitutionEditingContext);
//            switch (obtionGraphique) {
//            case OPTION_LOLF_DEPENSE_SEUL:
//                monNibFinderLolfNomanclatureInterfaceController.choixGraphique = NibFinderLolfNomanclatureIC.SHOW_TAB_DEPENSE_ONLY;
//                type=" depense";
//                break;
//            case OPTION_LOLF_RECETTE_SEUL:
//                monNibFinderLolfNomanclatureInterfaceController.choixGraphique = NibFinderLolfNomanclatureIC.SHOW_TAB_RECETTE_ONLY;
//                type=" recette";
//                break;
//            default:
//                break;
//            }
//            creationFenetre(monNibFinderLolfNomanclatureInterfaceController,"Lolf nomanclature"+type);
//        }
//        else
//        {
//            
//            switch (obtionGraphique) {
//            case OPTION_LOLF_DEPENSE_SEUL:
//                entityEnCours = ENTITY_DEPENSE;
//                type=" depense";
//                break;
//            case OPTION_LOLF_RECETTE_SEUL:
//                entityEnCours = ENTITY_RECETTE;
//                type=" recette";
//                break;
//            default:
//                entityEnCours = ENTITY_DEPENSE;
//                break;
//            }
//            lolfObject resp = new lolfObject();
//            monNibFinderLolftICTreeView = new NibFinderLolftICTreeView(substitutionEditingContext,resp);
//            creationFenetre(monNibFinderLolftICTreeView,"Lolf nomanclature"+type);
//        }
        initInterface(optionGraphique);
        
    }
    
    public NibFinderLolfNomanclature(EOEditingContext substitutionEditingContext, NibCtrl responder, boolean ismodal, int optionGraphique,boolean useTreeView) {
        super(substitutionEditingContext, responder, ismodal);
        this.useTreeView = useTreeView;
       initInterface(optionGraphique); 
    }
    
    private void initInterface(int optionGraphique){
        String type = "";
        if(!useTreeView)
        {
            monNibFinderLolfNomanclatureInterfaceController = new NibFinderLolfNomanclatureIC(editingContext());
            switch (optionGraphique) {
            case OPTION_LOLF_DEPENSE_SEUL:
                monNibFinderLolfNomanclatureInterfaceController.choixGraphique = NibFinderLolfNomanclatureIC.SHOW_TAB_DEPENSE_ONLY;
                type=" depense";
                break;
            case OPTION_LOLF_RECETTE_SEUL:
                monNibFinderLolfNomanclatureInterfaceController.choixGraphique = NibFinderLolfNomanclatureIC.SHOW_TAB_RECETTE_ONLY;
                type=" recette";
                break;
            default:
                break;
            }
            creationFenetre(monNibFinderLolfNomanclatureInterfaceController,"Lolf nomanclature"+type);
        }
        else
        {
            
            switch (optionGraphique) {
            case OPTION_LOLF_DEPENSE_SEUL:
                entityEnCours = ENTITY_DEPENSE;
                type=" depense";
                break;
            case OPTION_LOLF_RECETTE_SEUL:
                entityEnCours = ENTITY_RECETTE;
                type=" recette";
                break;
            default:
                entityEnCours = ENTITY_DEPENSE;
                break;
            }
            lolfObject resp = new lolfObject();
            monNibFinderLolftICTreeView = new NibFinderLolftICTreeView(editingContext(),resp);
            creationFenetre(monNibFinderLolftICTreeView,"Lolf nomanclature"+type);
        }
    }
    
    public void setEditingContext(EOEditingContext ec) {
        super.setEditingContext(ec);
        monNibFinderLolfNomanclatureInterfaceController.setEditingContext(editingContext());
    }
    
    public void afficherFenetre(EOQualifier qualifier, NSArray sort) {
        if(!useTreeView)
        {
            monNibFinderLolfNomanclatureInterfaceController.dgLolfDep.setQualifier(qualifier);
            monNibFinderLolfNomanclatureInterfaceController.dgLolfRec.setQualifier(qualifier);
            monNibFinderLolfNomanclatureInterfaceController.dgLolfDep.setSortOrderings(sort);
            monNibFinderLolfNomanclatureInterfaceController.dgLolfRec.setSortOrderings(sort);
            monNibFinderLolfNomanclatureInterfaceController.dgLolfDep.fetch();
            monNibFinderLolfNomanclatureInterfaceController.dgLolfRec.fetch();
        }
        super.afficherFenetre(qualifier, sort);
    }
    
    
    
     class lolfObject implements JTreeObjectCocktail{
    	 private NSMutableArray sort;
    	 
    	 public lolfObject() {
             NSMutableArray sort = new NSMutableArray();
             sort.addObject(EOSortOrdering.sortOrderingWithKey(EOLolfNomenclatureAbstract.LOLF_CODE_KEY, EOSortOrdering.CompareAscending));
//             sort.addObject(EOSortOrdering.sortOrderingWithKey(EOLolfNomenclatureAbstract.LOLF_LIBELLE_KEY, EOSortOrdering.CompareAscending));	
		}
        
        public NSArray getChilds(Object ob) {
            NSArray list =  ((EOLolfNomenclatureAbstract)ob).lolfNomenclatureFils();
            if(list==null || list.count()==0)
            	return list;
            return EOSortOrdering.sortedArrayUsingKeyOrderArray(list, sort);
        }

        public NSArray getRootsNode() {
            NSMutableArray args = new NSMutableArray();
            args.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY+" = 0", null));
            NSArray list =  app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(editingContext(),entityEnCours,new EOAndQualifier(args), null);
            if(list==null || list.count()==0)
            	return list;
            return EOSortOrdering.sortedArrayUsingKeyOrderArray(list, sort);
        }

        public String toString(Object ob) {
            return ((EOLolfNomenclatureAbstract)ob).lolfCode()+" "+((EOLolfNomenclatureAbstract)ob).lolfLibelle();
        }
        
        public boolean isSelectableObject(Object ob) {
            return true;
        }
        
    }
}
