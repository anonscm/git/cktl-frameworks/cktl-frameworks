/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.application.client.nibfinder;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.swingfinder.SwingFinder;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eoapplication.*;

public class NibFinderLolfNomanclatureIC extends EOInterfaceControllerCocktail {
    public static final int TAB_DEPENSE=0;
    public static final int TAB_RECETTE=1;
	public static final String SHOW_TAB_DEPENSE_ONLY="Lolf Nomenclature Depense";
	public static final String SHOW_TAB_RECETTE_ONLY="Lolf Nomenclature Recette";
    
    public EODisplayGroup dgLolfDep;
    public EODisplayGroup dgLolfRec;
    public JTabbedPane tabbedPane;
    public String choixGraphique=null;

    public NibFinderLolfNomanclatureIC() {
        super();
    }

    public NibFinderLolfNomanclatureIC(EOEditingContext substitutionEditingContext) {
        super();
        setEditingContext(substitutionEditingContext);
    }
    
    public void componentDidBecomeVisible() {
        super.componentDidBecomeVisible();
//        try {
//          Attention, si on supprime 1 tab, le dernier tab a pour index 1 !!! meme si c'etait le tab 2 !!!!
            if(SHOW_TAB_DEPENSE_ONLY.equals(choixGraphique))
                tabbedPane.remove(TAB_RECETTE);
            if(SHOW_TAB_RECETTE_ONLY.equals(choixGraphique))
                tabbedPane.remove(TAB_DEPENSE);
//        } catch (Throwable e) {
//        }

    }

    public void swingFinderAnnuler(SwingFinder s) {
    }

    public void swingFinderTerminer(SwingFinder s) {
    }

    public void nibFinderAnnuler(NibFinder s) {
    }

    public void nibFinderTerminer(NibFinder s) {
    }
    

    public NSArray getResultat() {
        System.out.println("NibFinderLolfNomanclatureInterfaceController.getResultat()");
        System.out.println("rec "+dgLolfRec.selectedObjects());
        System.out.println("dep "+dgLolfDep.selectedObjects());
		System.out.println("choixGraphique "+choixGraphique);
        if(choixGraphique!=null)
        {
            if(SHOW_TAB_DEPENSE_ONLY.equals(choixGraphique))
                return dgLolfDep.selectedObjects();
            return dgLolfRec.selectedObjects();
        }
		System.out.println("choix "+tabbedPane.getSelectedIndex());
        switch (tabbedPane.getSelectedIndex()) {
            case TAB_DEPENSE:
            {
                System.out.println("TAB_DEPENSE");
                return dgLolfDep.selectedObjects();
            }
            case TAB_RECETTE:
            {
                System.out.println("TAB_RECETTE");
                return dgLolfRec.selectedObjects();
            }
        }
        return new NSArray();
    }
}
