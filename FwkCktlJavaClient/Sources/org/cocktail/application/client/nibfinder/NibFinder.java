/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 26 avr. 07
 * author mparadot 
 */
package org.cocktail.application.client.nibfinder;

import java.awt.Dimension;
import java.awt.geom.Dimension2D;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOCodeMarcheNib;
import org.cocktail.application.nibctrl.NibCtrl;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import java.lang.reflect.Method;
import org.cocktail.application.client.ModalInterfaceCocktail;
import org.cocktail.application.palette.JButtonCocktail;

public abstract class NibFinder {
    /**
     * Constantes de positionnement de fenetre
     */
    public static final int LOCATION_NORTH = 0;
    public static final int LOCATION_SOUTH = 1;
    public static final int LOCATION_EAST = 2;
    public static final int LOCATION_WEST = 3;
    public static final int LOCATION_NORTH_EAST = 4;
    public static final int LOCATION_NORTH_WEST = 5;
    public static final int LOCATION_SOUTH_EAST = 6;
    public static final int LOCATION_SOUTH_WEST = 7;
    public static final int LOCATION_MIDDLE = 8;
    
    /**
     * Constantes de taille par default de la fenetre;
     */
    public static final int DEFAULT_WIDTH = 640;
    public static final int DEFAULT_HEIGHT = 480;
    
    private EOEditingContext _ec;
    protected static ApplicationCocktail app;
    private ModalInterfaceCocktail parentControleur;
    private NibCtrl parentControleurSW;
    private EOInterfaceControllerCocktail currentNib;
    private int location_x, location_y, taille_x, taille_y;
    private boolean modal;
    private JFrame frameMain;
    private EOQualifier defaultQualifier;
    private NSArray _resultat;
    
    public static String METHODE_AFFICHER_FENETRE="afficherFenetre";
    
    static {
        app = (ApplicationCocktail)EOApplication.sharedApplication();
    }
    
//    abstract public NSArray getResultat();
//    abstract public void setResultat(NSArray resultat);

    
    /**
     * Cr?ation du finder
     * @param substitutionEditingContext ec ? utiliser
     * @param responder le parent
     * @param alocation_x position x de la fen?tre
     * @param alocation_y position y de la fen?tre
     * @param ataille_x taille x de la fen?tre
     * @param ataille_y taille y de la fen?tre
     * @param ismodal si la fen?tre est modal ou non
     */
    public NibFinder(EOEditingContext substitutionEditingContext,ModalInterfaceCocktail responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean ismodal) 
    {
        super();
        _ec = substitutionEditingContext;
        setParentControleur(responder);
        setSize(taille_x, taille_y);
        location_x = alocation_x;
        location_y = alocation_y;
        setModal(ismodal);
    }
    
     /**
     * Cr?ation du finder
     * @param substitutionEditingContext ec ? utiliser
     * @param responder le parent
     * @param alocation_x position x de la fen?tre
     * @param alocation_y position y de la fen?tre
     * @param ataille_x taille x de la fen?tre
     * @param ataille_y taille y de la fen?tre
     * @param ismodal si la fen?tre est modal ou non
     */
    public NibFinder(EOEditingContext substitutionEditingContext,NibCtrl responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean ismodal) 
    {
        super();
        _ec = substitutionEditingContext;
        setParentControleurSW(responder);
        setSize(taille_x, taille_y);
        location_x = alocation_x;
        location_y = alocation_y;
        setModal(ismodal);
    }
    
    /**
     * Cr?ation du finder
     * @param substitutionEditingContext ec ? utiliser
     * @param responder le parent
     * @param ismodal si la fen?tre est modal ou non
     * 
     * Par default, la fen?tre est au milieu, et de taille {@value #DEFAULT_HEIGHT} x {@value #DEFAULT_WIDTH}
     */
    public NibFinder(EOEditingContext substitutionEditingContext,ModalInterfaceCocktail responder,boolean ismodal) 
    {
        this(substitutionEditingContext,responder,LOCATION_MIDDLE,LOCATION_MIDDLE,DEFAULT_WIDTH,DEFAULT_HEIGHT,ismodal);
    }
    
        /**
     * Cr?ation du finder
     * @param substitutionEditingContext ec ? utiliser
     * @param responder le parent
     * @param ismodal si la fen?tre est modal ou non
     * 
     * Par default, la fen?tre est au milieu, et de taille {@value #DEFAULT_HEIGHT} x {@value #DEFAULT_WIDTH}
     */
    public NibFinder(EOEditingContext substitutionEditingContext,NibCtrl responder,boolean ismodal) 
    {
        this(substitutionEditingContext,responder,LOCATION_MIDDLE,LOCATION_MIDDLE,DEFAULT_WIDTH,DEFAULT_HEIGHT,ismodal);
    }
    
    /**
     * Cr?ation du finder
     * @param substitutionEditingContext ec ? utiliser
     * @param responder le parent
     * 
     * Par default, la fen?tre est au milieu, non modal, et de taille {@value #DEFAULT_HEIGHT} x {@value #DEFAULT_WIDTH}
     */
    public NibFinder(EOEditingContext substitutionEditingContext,ModalInterfaceCocktail responder) 
    {
        this(substitutionEditingContext,responder,false);
    }
    
        /**
     * Cr?ation du finder
     * @param substitutionEditingContext ec ? utiliser
     * @param responder le parent
     * 
     * Par default, la fen?tre est au milieu, non modal, et de taille {@value #DEFAULT_HEIGHT} x {@value #DEFAULT_WIDTH}
     */
    public NibFinder(EOEditingContext substitutionEditingContext,NibCtrl responder) 
    {
        this(substitutionEditingContext,responder,false);
    }
    
    public NibFinder(ModalInterfaceCocktail responder,boolean ismodal) 
    {
        this(app.getAppEditingContext(),responder,ismodal);
    }
    
    public NibFinder(NibCtrl responder,boolean ismodal) 
    {
        this(app.getAppEditingContext(),responder,ismodal);
    }
    
    public NibFinder(ModalInterfaceCocktail responder) 
    {
        this(app.getAppEditingContext(),responder,false);
    }
    
    public NibFinder(NibCtrl responder) 
    {
        this(app.getAppEditingContext(),responder,false);
    }
    
    /**
     * Permet d'obtenir l'editingContext utilis? par le nib....
     * @return l'editingContext utilis?
     */
    public EOEditingContext editingContext(){
        return _ec;
    }
    
    /**
     * Permet de changer l'editingContext utilis? par le nib....
     * @param ec l'editingContext ? utiliser
     */
    public void setEditingContext(EOEditingContext ec){
        if(ec!=null)
        {
            _ec = ec;
            if(currentNib!=null)
                currentNib.setEditingContext(ec);
        }
    }

    /**
     * 
     * @return
     */
    public ModalInterfaceCocktail getParentControleur() {
        return parentControleur;
    }

    /**
     * 
     * @param parentControleur
     */
    public void setParentControleur(ModalInterfaceCocktail parentControleur) {
        this.parentControleur = parentControleur;
    }

    /**
     * Renvoi si la fen?tre est modal
     * @return true si la fen?tre est modal, false sinon
     */
    public boolean isModal() {
        return modal;
    }

    /**
     * Permet de rendre / ou non, la fen?tre modal
     * @param modal
     */
    public void setModal(boolean modal) {
        this.modal = modal;
    }
    
    public void setSize(int height, int width){
        taille_x = width;
        taille_y = height;
    }
    
    /**
     * Position de la fen?tre
     * @param x 
     * @param y
     * 
     * Remarque: si x a pour valeur {@link #LOCATION_EAST}, {@link #LOCATION_EAST}, {@link #LOCATION_EAST},<br>
     * {@link #LOCATION_EAST}, {@link #LOCATION_EAST}, {@link #LOCATION_EAST}, {@link #LOCATION_EAST},<br>
     * {@link #LOCATION_EAST} ou {@link #LOCATION_EAST}<br>
     * alors le positionnement ce fais de fa?on relative par rapport ? la fen?tre global.
     */
    public void setPosition(int x,int y){
        switch (x) {
        case LOCATION_EAST:
        case LOCATION_MIDDLE:
        case LOCATION_NORTH:
        case LOCATION_NORTH_EAST:
        case LOCATION_NORTH_WEST:
        case LOCATION_SOUTH:
        case LOCATION_SOUTH_EAST:
        case LOCATION_SOUTH_WEST:
        case LOCATION_WEST:
            {
                setNibCtrlLocation(x);
            }
            break;
        default:
            {
            location_x = x;
            location_y = y;
            }
            break;
        }
    }
    
    protected void setFrameMain(JFrame frameMain) {
        this.frameMain = frameMain;
    }

    /**
     * La frame !!!!
     * @return
     */
    public JFrame getFrameMain() {
        return frameMain;
    }
    
    /**
     * Cr?ation de la fen?tre (mise en place du nib dans la frame)
     * @param nib le nib a placer dans la fen?tre
     * @param title titre de la fen?tre
     */
    public void creationFenetre(EOInterfaceControllerCocktail nib,String title) {
        if (getFrameMain() == null )
        {
            setFrameMain(new JFrame());
            getFrameMain().getContentPane().add(nib.component());
            getFrameMain().setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            setPosition(location_x,location_y);
            getFrameMain().setSize(new java.awt.Dimension(taille_x,taille_y));
            getFrameMain().pack();
            getFrameMain().setLocation(location_x,location_y);
        }
        getFrameMain().setTitle(title);
        currentNib=nib;
        currentNib.fenetre = this;
        currentNib.componentDidBecomeVisible();
        if(isModal())
            app.addLesPanelsModal(currentNib);
//        else
//            app.removeFromLesPanelsModal(currentNib);
    }
    
    /**
     * Permet d'afficher la fen?tre
     */
    public void afficherFenetre()
    {
//        if(isModal())
//        {
//            app.activerLesGlassPane(currentNib);
//        }
//        getFrameMain().setVisible(true);
//        getFrameMain().pack();
        afficherFenetre(null,null);
    }
	
	/**
     * Permet d'afficher la fen?tre avec un qualifier.
     */
    public void afficherFenetre(EOQualifier qualifier, NSArray sort)
    {
        if(isModal())
        {
            app.activerLesGlassPane(currentNib);
        }
        //app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(currentNib.entityName(), leQualifier, leSort, refresh, distinct)
        if(currentNib.displayGroup()!=null && (qualifier!=null || sort!=null))
        {
            //System.out.println("currentNib.displayGroup().allObjects().count()"+currentNib.displayGroup().allObjects().count());
            currentNib.displayGroup().setQualifier(qualifier);
            currentNib.displayGroup().setSortOrderings(sort);
            currentNib.displayGroup().fetch();
            if(currentNib.displayGroup().allObjects().count()==1)
            {
                setResultat(currentNib.displayGroup().allObjects());
                if(getParentControleur()!=null)
                {
//                    getParentControleur().nibFinderTerminer(this);
                    Class c = getParentControleur().getClass();
                    try {
                        Method met = c.getMethod("nibFinderTerminer", new Class[]{ModalInterfaceCocktail.class});
                        met.invoke(getParentControleur(), new Object[]{this});
                    } catch (Exception exception) {
                    }

                    
                }
                else
                {
                    getParentControleurSW().swingFinderTerminer(this); 
                }
                return;
            }
           // System.out.println("currentNib.displayGroup().allObjects().count()"+currentNib.displayGroup().allObjects().count());
        }
        getFrameMain().setVisible(true);
        getFrameMain().pack();
    }
    
    /**
     * Permet de masquer la fen?tre
     */
    public void masquerFenetre()
    {
        if(isModal())
        {
            app.retirerLesGlassPane();
            if(parentControleur!=null)
            	app.activerLesGlassPane(parentControleur);
            else
            {
            	if(parentControleurSW.currentNib!=null)
            		app.activerLesGlassPane(parentControleurSW.currentNib);
            }
        }
        getFrameMain().setVisible(false);
    }
    
    public EOQualifier getDefaultQualifier() {
        return defaultQualifier;
    }

    public void setDefaultQualifier(EOQualifier defaultQualifier) {
        this.defaultQualifier = defaultQualifier;
    }
	
	
	public NSArray getResultat() {
        return _resultat;
    }

    public void setResultat(NSArray arg0) {
        if(arg0==null)
            _resultat = new NSArray();
        else
            _resultat = (NSArray) arg0.clone();
    }
    
    protected void setNibCtrlLocation(int location)
    {
        
//      Dimensions de l'ecran
        int w = (int) getFrameMain().getGraphicsConfiguration().getBounds().getWidth();
        int h = (int) getFrameMain().getGraphicsConfiguration().getBounds().getHeight();

//      coordonnees utiles
        int xMiddle = (w/2) - ((int) getFrameMain().getSize().getWidth() / 2);
        int yMiddle = (h/2) - ((int) getFrameMain().getSize().getHeight() / 2);
        int xRight = w - (int) getFrameMain().getSize().getWidth();
        int yBottom = h - (int) getFrameMain().getSize().getHeight();

        int x, y;
        switch (location) {
        case LOCATION_MIDDLE: 
            x = xMiddle; y = yMiddle; 
            break;
        case LOCATION_NORTH: 
            x = xMiddle; y = 0; 
            break;
        case LOCATION_SOUTH: 
            x = xMiddle; y = yBottom; 
            break;

        case LOCATION_NORTH_EAST: 
            x = xRight; y = 0; 
            break;
        case LOCATION_EAST: 
            x = xRight; y = yMiddle; 
            break;
        case LOCATION_SOUTH_EAST:
            x = xRight; y = yBottom; 
            break;

        case LOCATION_NORTH_WEST:
            x = 0; y = 0; 
            break;
        case LOCATION_WEST: 
            x = 0; y = yMiddle; 
            break;
        case LOCATION_SOUTH_WEST:
            x = 0; y = yBottom; 
            break;

        default:     //centre 
            x = xMiddle; y = yMiddle; 
            break;
        }
        getFrameMain().setLocation(x, y);
        getFrameMain().pack();
        location_x = x;
        location_y = y;
    }

    public NibCtrl getParentControleurSW() {
        return parentControleurSW;
    }

    public void setParentControleurSW(NibCtrl parentControleurSW) {
        this.parentControleurSW = parentControleurSW;
    }
    
    public void relierBouton(JButtonCocktail bt) {
        if(getParentControleurSW()!=null)
            bt.addDelegateActionListener(getParentControleurSW(),METHODE_AFFICHER_FENETRE);
        else
            bt.addDelegateActionListener(getParentControleur(),METHODE_AFFICHER_FENETRE);	
    }
}
