package org.cocktail.application.serveur;

/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frédéric (frederic.rivalland@univ-paris5.fr)

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRException;

import org.cocktail.application.client.tools.UserInfoCocktail;
import org.cocktail.application.serveur.ExcelTool.ExcelToolResult;
// import org.cocktail.geide.fwkcktlgedibus.tools.GedTool;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import org.cocktail.fwkcktlwebapp.common.util.CktlMailMessage;
import org.cocktail.fwkcktlwebapp.common.util.CRIpto;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlResourceManager;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;

public class SessionCocktailRemoteCall {
	public final static String RAISE_SQL = "RAISE_SQL";
	public final static String THREADSTATUS_ATTENTE = "ATTENTE";
	public final static String THREADSTATUS_ENCOURS = "ENCOURS";
	public final static String THREADSTATUS_TERMINE = "TERMINE";
	public final static String THREADSTATUS_PROBLEME = "PROBLEME";
	public final static String THREADSTATUS_VIDE = "VIDE";
	public final static String THREADEXECPTION_VIDE = " ";

	private ThreadReports thrReports;
	private ThreadReports lastthreadReports;
	private int lastCallReports = 0;

	private ThreadJxls thrJxls;
	private ThreadJxls lastthreadJxls;
	private int lastCallJxls = 0;

	public File threadFichierResultat;
	public NSData threadDatas;
	public String threadStatus = SessionCocktailRemoteCall.THREADSTATUS_ATTENTE;
	public String threadException = SessionCocktailRemoteCall.THREADEXECPTION_VIDE;

	static CocktailApplication woApplication = (CocktailApplication) WOApplication.application();

	EOEditingContext defaultEC;

	private CktlUserInfoDB lrUser;

	private UserInfoCocktail userInfos;

	private String lastExecuteProcedure = "";

	// pour la geid
	// private GedTool ged=null;

	public SessionCocktailRemoteCall(CocktailApplication woApplication, EOEditingContext defaultEC) {
		this.woApplication = woApplication;
		this.defaultEC = defaultEC;
	}

	// Lecture d'un NSArray a partir d'une table
	public NSArray clientSideRequestFetchArrayFromTable(String table, EOQualifier qualifier, NSArray sort) {
		return this.fetchArrayFromTable(table, qualifier, sort);
	}

	// Recherche d'un tableau d'elements.
	// Utilise l'editingContexte par default de la session
	protected NSArray fetchArrayFromTable(String name, EOQualifier qualifier, NSArray sorts) {
		NSArray anArray;
		// Definition des specifications
		EOFetchSpecification aFetch = new EOFetchSpecification(name, qualifier, sorts);
		aFetch.setRefreshesRefetchedObjects(true);
		aFetch.setUsesDistinct(true);
		// Recuperation des enregistrements
		anArray = defaultEC.objectsWithFetchSpecification(aFetch);

		return anArray;

	}

	// recup du numéro de version la version de l'application
	public String clientSideRequestNumeroVersionApplication() {
		try {
			return woApplication.versionApplication().appliVersion();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return "....";
	}

	// recup de la version de la pplication
	public String clientSideRequestVersionApplication() {
		try {
			return woApplication.getVersionApp();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return "....";
	}

	// recup de la version de la pplication
	public String clientSideRequestMailHost() {
		return woApplication.getMailHost();
	}

	// recup de la version de la pplication
	public String clientSideRequestGetParametre(String key) {
		return (String) woApplication.config().stringForKey(key);
	}

	// recup de la version de la pplication
	public String clientSideRequestConnectionBaseDonnees() {
		String db = woApplication.bdConnexionName();
		if (db == null)
			return null;
		String[] split = db.split(":");
		if (split.length > 1)
			return split[split.length - 1];
		return db;
	}

	// Execution de procedure stockee
	/*
	 * Execution de procedure stockee public Boolean clientSideRequestExecuteStoredProcedure(String proc, NSDictionary dico) { Boolean returnedValue;
	 * // Appel de la procedure stockee returnedValue = new Boolean(woApplication.executeStoredProcedure(proc, dico));
	 * 
	 * return returnedValue; }
	 */

	// Execution de procedure stockee
	public Boolean clientSideRequestExecuteStoredProcedure(String proc, NSDictionary dico) {
		// Appel de la procedure stockee
		lastExecuteProcedure = woApplication.executeStoredProcedureRaiseSQL(proc, dico);

		if (lastExecuteProcedure.equals(""))
			return new Boolean(true);
		else
			return new Boolean(false);

	}

	//
	// Retourne la valeur de retour de la derniere procedure executee vers la client
	/*
	 * public NSDictionary clientSideRequestReturnValuesForLastStoredProcedureInvocation() { return
	 * woApplication.returnValuesForLastStoredProcedureInvocation(); }
	 */

	public NSDictionary clientSideRequestReturnValuesForLastStoredProcedureInvocation() {
		NSMutableDictionary result = new NSMutableDictionary(woApplication.returnValuesForLastStoredProcedureInvocation());
		result.setObjectForKey(formaterRaiseSql(lastExecuteProcedure), RAISE_SQL);
		return (NSDictionary) result;
	}

	private String formaterRaiseSql(String s) {
		/*
		 * 
		 * DEBUT.... Recuperation d'un numero de selection ...OK probleme !!{RAISE_SQL =
		 * "EvaluateExpression failed: <com.webobjects.jdbcadaptor.OraclePlugIn$OracleExpression: "{ call MARACUJA.BORDEREAU_ABRICOT.SET_SELECTION_ID
		 * (?, ?, ?, ?, ?, ?, ?, ?)}" withBindings: 1:685(a01abrid), 2:"8128.0$$"(a02lesdepid), 3:""(a03lesrecid), 4:207(a04utlordre),
		 * 5:2007(a05exeordre), 6:1(a06tboordre), 7:"bordereau_1D1M"(a07abrgroupby), 8:"922"(a08gescode)>: Next exception:SQL State:72000 -- error
		 * code: 20001 -- msg: ORA-20001: XXXXXXXXXXXXXXX ORA-06512: à "MARACUJA.BORDEREAU_ABRICOT", ligne 20 ORA-06512: à
		 * "MARACUJA.BORDEREAU_ABRICOT", ligne 397 ORA-06512: à ligne 1 "; }
		 */
		if (s.equals(""))
			return s;
		else {
			// System.out.println("1 "+s);
			NSArray tmp = NSArray.componentsSeparatedByString(s, "ORA-");
			// System.out.println("2 "+tmp);
			// System.out.println( "3"+tmp.objectAtIndex(1));
			return (String) tmp.objectAtIndex(1);
		}
	}

	// Debut de transaction
	public Boolean clientSideRequestBeginTransaction() {
		return new Boolean(woApplication.beginTransaction());
	}

	// Validation de transaction
	public Boolean clientSideRequestCommitTransaction() {
		return new Boolean(woApplication.commitTransaction());
	}

	// Annulation de transaction
	public Boolean clientSideRequestRollbackTransaction() {
		return new Boolean(woApplication.rollbackTransaction());
	}

	// jasperreport :

	static private String getAppResourcesDir() {
		String resourcesDir = WOApplication.application().path() + "/Contents/Resources/";
		return resourcesDir;
	}

	public final NSData clientSideRequestImprimerReportSql(String reportName, String select, NSDictionary parameters) throws Exception {

		NSMutableDictionary tmp = new NSMutableDictionary(parameters);
		HashMap params = new HashMap();
		Reporter reporter = null;

		File f = new File((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL") + reportName);
		if (f.exists()) {
			reporter = new Reporter((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL") + reportName, 0);
			tmp.takeValueForKey((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL"), "REP_BASE_JASPER_PATH");
		}
		else
			reporter = new Reporter((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH") + reportName, 0);

		try {
			for (int i = 0; i < tmp.allKeys().count(); i++)
				params.put(tmp.allKeys().objectAtIndex(i), tmp.valueForKey((String) tmp.allKeys().objectAtIndex(i)));

			String sqlQuery = select;
			// woApplication.addLogMessage("reporting","\n reportName :"+ reportName+"\n sqlQuery " + sqlQuery+"\n parameters " + params);

			if (sqlQuery != null) {
				params.put("REQUETE_SQL", sqlQuery);
			}

			if (reporter != null) {
				// TODO Amodifier
				int ret = reporter.printReport(woApplication.getJDBCConnection(), params);
				if (ret == 1) {
					ByteArrayOutputStream resultStream = reporter.exportReportToDocumentStream();
					return new NSData(resultStream.toByteArray());
				}
				else
					return null;
			}
			return null;
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public final NSData clientSideRequestImprimerReportParametres(String reportName, NSDictionary parameters) throws Exception {

		NSMutableDictionary tmp = new NSMutableDictionary(parameters);
		HashMap params = new HashMap();
		Reporter reporter = null;

		File f = new File((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL") + reportName);
		if (f.exists()) {
			reporter = new Reporter((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL") + reportName, 0);
			tmp.takeValueForKey((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL"), "REP_BASE_PATH");
		}
		else
			reporter = new Reporter((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH") + reportName, 0);

		try {
			for (int i = 0; i < tmp.allKeys().count(); i++)
				params.put(tmp.allKeys().objectAtIndex(i), tmp.valueForKey((String) tmp.allKeys().objectAtIndex(i)));

			if (reporter != null) {
				// TODO Amodifier
				int ret = reporter.printReport(woApplication.getJDBCConnection(), params);
				if (ret == 1) {
					ByteArrayOutputStream resultStream = reporter.exportReportToDocumentStream();
					return new NSData(resultStream.toByteArray());
				}
				else
					return null;
			}
			return null;
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	// ZAP

	public NSDictionary clientSideRequestCheckPassword(String userName) {
		lrUser = new CktlUserInfoDB(woApplication.dataBus());
		lrUser.compteForLogin(userName, null, true);
		if (!lrUser.hasError()) {
			Number persId = lrUser.persId();
			userInfos = new UserInfoCocktail();
			userInfos.setPersId(persId);
			userInfos.setNoIndividu(lrUser.noIndividu());
			userInfos.setUserName(userName);
			userInfos.setNoCompte(lrUser.noCompte());
			userInfos.setNom(lrUser.nom());
			userInfos.setPrenom(lrUser.prenom());
			userInfos.setEmail(lrUser.email());
			userInfos.setVLan(lrUser.vLan());
			userInfos.setLogin(lrUser.login());
			return userInfos;
		}
		else {
			woApplication.addLogMessage("INFO LOGIN KO CAS", "login=" + userName);
			return null;
		}
	}

	public NSDictionary clientSideRequestCheckPassword(String login, String passwd) {
		String userName = CRIpto.decrypt(login);
		String passwdClair = CRIpto.decrypt(passwd);

		String rootPW = ((CocktailApplication) WOApplication.application()).config().stringForKey("APP_ADMIN_PASSWORD");
		lrUser = new CktlUserInfoDB(woApplication.dataBus());
		lrUser.setRootPass(rootPW);
		//        lrUser.setForceCryptedPass(true);
		lrUser.setAcceptEmptyPass(false);
		lrUser.compteForLogin(userName, passwdClair, true);

		if (lrUser.errorCode() == CktlUserInfo.ERROR_NONE) {
			Number persId = lrUser.persId();
			woApplication.addLogMessage("INFO LOGIN OK", "login=" + userName + " noIndividu=" + lrUser.noIndividu());
			userInfos = new UserInfoCocktail();
			userInfos.setPersId(persId);
			userInfos.setNoIndividu(lrUser.noIndividu());
			userInfos.setUserName(userName);
			userInfos.setNoCompte(lrUser.noCompte());
			userInfos.setNom(lrUser.nom());
			userInfos.setPrenom(lrUser.prenom());
			userInfos.setEmail(lrUser.email());
			userInfos.setVLan(lrUser.vLan());
			userInfos.setLogin(lrUser.login());
			System.out.println("userInfo = " + userInfos);
			return userInfos;
		}
		else {
			woApplication.addLogMessage("INFO LOGIN KO", "login=" + userName);
			return null;
		}
	}

	/**
	 * @param msg
	 */
	public void clientSideRequestServerLog(String msg) {
		NSLog.out.appendln("DEBUG :: " + msg);
	}

	public void clientSideRequestWriteToStatistique(String nom, String action, String msg, String persId) {
		if (FileCtrl.existsFile(getUrlForWebResources("Statistique.csv")))
		{
			try {
				FileWriter fic = null;
				fic = new FileWriter(getUrlForWebResources("Statistique.csv"), true);
				fic.write(DateCtrl.dateToString(new NSTimestamp()) + ";" + DateCtrl.dateToString(new NSTimestamp(), "HH:MM:SS") + ";" + nom + ";" + action + ";" + msg + ";" + persId + "\n");
				fic.flush();
				fic.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public NSData clientSideRequestGetSQLResult(String sql, NSArray keys) {
		NSArray result = new NSArray();
		try {
			result = new NSArray((NSArray) EOUtilities.rawRowsForSQL(defaultEC, "DataCom", sql, keys));

		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.out.println("\n sql : " + sql + "\n resultat nb :" + result.count());
		return makeArrayDicoToNSData(result, keys);
	}

	private NSData makeArrayDicoToNSData(NSArray array, NSArray keys) {

		NSMutableArray valuesTmp = new NSMutableArray();
		for (int i = 0; i < array.count(); i++) {
			valuesTmp.addObject((((NSDictionary) array.objectAtIndex(i)).objectsForKeys(keys, " ")));
		}

		StringBuffer maSringBuffer = new StringBuffer();

		maSringBuffer.append(keys.componentsJoinedByString("||") + "##");
		for (int i = 0; i < valuesTmp.count(); i++) {
			NSMutableArray rawRow = (NSMutableArray) valuesTmp.objectAtIndex(i);
			for (int j = 0; j < rawRow.count(); j++) {
				if (NSArray.NullValue.equals(rawRow.objectAtIndex(j))) {
					rawRow.set(j, "");
				}
			}
			maSringBuffer.append(rawRow.componentsJoinedByString("||") + "##");
		}

		return new NSData(maSringBuffer.toString().getBytes());
	}

	public NSData clientSideRequestGetFileFromServeurResources(String fileName) {
		return getFileFromServeurResources(fileName);
	}

	public NSData clientSideRequestGetLogsFromServeur() {
		return getFileFromServeur(woApplication.pathLog());
	}

	public NSData clientSideRequestGetFileFromWebServeurResources(String fileName) {
		return getFileFromServeur(getUrlForWebResources(fileName));
	}

	public NSData getFileFromServeurResources(String fileName) {

		File f = new File((String) woApplication.config().stringForKey("REP_BASE_XLS_PATH_LOCAL") + fileName);
		if (f.exists()) {
			try {
				return new NSData(f.toURL());

			} catch (Throwable e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		else {
			System.err.println(" REP XLS LOCAL INEXISTANT : " + (String) woApplication.config().stringForKey("REP_BASE_XLS_PATH_LOCAL") + fileName);
			f = new File((String) woApplication.config().stringForKey("REP_BASE_XLS_PATH") + fileName);
			if (f.exists()) {
				try {
					return new NSData(f.toURL());

				} catch (Throwable e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public String getUrlForWebResources(String fileName) {
		CktlResourceManager manager = new CktlResourceManager();
		return manager.pathForResource(fileName);
	}

	public NSData getFileFromServeur(String pathFile) {
		if (pathFile == null)
		{
			System.out.println("Erreur : le chemin est null");
			return null;
		}
		File f = new File(pathFile);
		if (f.exists()) {
			try {
				return new NSData(f.toURL());
			} catch (Throwable e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return null;
	}

	public boolean clientSideRequestEnvoyerMail(NSDictionary leObjet, String pathfile) {

		String from, to, ccs, text, subject;
		CktlMailMessage mailMessage;
		// Creation d'un objet CktlMailMessage a chaque envoi
		try {
			mailMessage = new CktlMailMessage(woApplication.config().stringForKey("HOST_MAIL"));
		} catch (Exception exception) {
			woApplication.addLogMessage("Exception", "Envoi de mail : " + exception.getMessage());	
			exception.printStackTrace();
			return false;
		}
		// Preparation des donnees et tests (expediteur et destinataire obligatoire)
		from = (String) leObjet.objectForKey("expediteur");
		to = (String) leObjet.objectForKey("destinataire");
		if (from.equals("") || to.equals("")) {
			woApplication.addLogMessage("Erreur", "Envoi mail() : Expediteur ou destinataire manquant");
			return false;
		}

		subject = (String) leObjet.objectForKey("sujet");
		text = (String) leObjet.objectForKey("texte");
		ccs = (String) leObjet.objectForKey("cc");
		// Le chemin de fichier attache
		String[] fileNames;
		// Un tableau des noms des fichiers
		fileNames = CktlMailMessage.toArray(pathfile);

		// Envoi du message
		try {
			// System.out.println("1");
			mailMessage.initMessage(from, to, subject, text, fileNames);
			if (ccs != null || to.equals(""))
				mailMessage.addCCs(ccs.split(","));
			mailMessage.safeSend();
		} catch (Throwable exception) {
			woApplication.addLogMessage("Exception", "Envoi mail : " + exception.getMessage());
			exception.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean clientSideRequestEnvoyerMail(NSDictionary leObjet, NSData file, String fileName) {

		String from, to, ccs, text, subject;
		CktlMailMessage mailMessage;
		// Creation d'un objet CktlMailMessage a chaque envoi
		try {
			mailMessage = new CktlMailMessage(woApplication.config().stringForKey("HOST_MAIL"));
		} catch (Exception exception) {
			woApplication.addLogMessage("Exception", "Envoi de mail : " + exception.getMessage());
			return false;
		}
		// Preparation des donnees et tests (expediteur et destinataire obligatoire)
		from = (String) leObjet.objectForKey("expediteur");
		to = (String) leObjet.objectForKey("destinataire");
		ccs = (String) leObjet.objectForKey("cc");
		if (from.equals("") || to.equals("")) {
			woApplication.addLogMessage("Erreur", "Envoi mail() : Expediteur ou destinataire manquant");
			return false;
		}

		subject = (String) leObjet.objectForKey("sujet");
		text = (String) leObjet.objectForKey("texte");

		// Le chemin de fichier attache
		String[] fileNames;
		// Un tableau des noms des fichiers
		fileNames = CktlMailMessage.toArray(fileName);
		byte[][] datas = new byte[][] {
				file.bytes()
		};

		// Envoi du message
		try {
			// System.out.println("1");
			mailMessage.initMessage(from, to, subject, text, fileNames, datas);
			if (ccs != null && !"".equals(ccs))
				mailMessage.addCCs(ccs.split(","));
			// System.out.println("1");
			mailMessage.safeSend();
			// System.out.println("1");
		} catch (Throwable exception) {
			// System.out.println("11111111");

			woApplication.addLogMessage("Exception", "Envoi mail : " + exception.getMessage());
			return false;
		}

		return true;
	}

	/*
	 * //------------------------------------------------------------------------------------------------ // pour la geid !!!!
	 * 
	 * protected GedTool ged() { if(ged==null) { try { if(woApplication.config().booleanForKey("APP_USE_GED")) ged = new GedTool(new
	 * EOEditingContext()); } catch (Throwable e) { e.printStackTrace(); } System.out.println("Erreur d'initialiosation de la GED...");
	 * if(clientSideRequestGedDisponible().intValue()==-1) System.out.println("-> le parametre \"APP_USE_GED\" doit etre a TRUE"); else
	 * System.out.println("-> Vous devez utiliser le parametre \"APP_USE_GED\" dans le fichier de config"); } return ged; }
	 * 
	 * // private void setGed(GedTool ged) { // System.out.println("SessionCocktailRemoteCall.setGed()"); // this.ged = (GedTool)ged; // }
	 * 
	 * public String clientSideRequestGedLastError() { System.out.println("SessionCocktailRemoteCall.clientSideRequestGedLastError()"); if(ged()!=null
	 * && ged().getLastError()!=null) return ged().getLastError().getMessage(); return ""; }
	 * 
	 * public Integer clientSideRequestGedDisponible() { System.out.println("SessionCocktailRemoteCall.clientSideRequestGedDisponible()");
	 * if(woApplication.config().valueForKey("APP_USE_GED")==null) return new Integer(0); return
	 * woApplication.config().booleanForKey("APP_USE_GED")?new Integer(1):new Integer(0); }
	 * 
	 * // public Integer clientSideRequestAjouterFicherDansGed(NSData data, String name, Number persId) { //// ged.edtitingContext().lock(); //
	 * Integer couNum = null; // try { // couNum = ged.saveFileIntoGed(data, name, new Integer(persId.intValue())); // } catch (Throwable e) { //
	 * e.printStackTrace(); // return new Integer(-1); // } //// ged.edtitingContext().unlock(); // return couNum; // } public Integer
	 * clientSideRequestAjouterFicherDansGed(NSData data, String name, Number persId) {
	 * System.out.println("SessionCocktailRemoteCall.clientSideRequestAjouterFicherDansGed()"); Integer couNum = null; try {
	 * ged().edtitingContext().lock(); System.out.println("type root = "+woApplication.config().stringForKey("TYPE_ROOT_GED"));
	 * if(ged().getRootGed()==null) ged().setRootGed(woApplication.config().stringForKey("TYPE_ROOT_GED")); couNum = ged().saveFileIntoGed(data, name,
	 * new Integer(persId.intValue())); } catch(Throwable e) { e.printStackTrace(); } finally{ ged().edtitingContext().unlock(); } return couNum; }
	 * 
	 * public void setVisibiliteFichierDansGedPourApplication(Integer i){
	 * System.out.println("SessionCocktailRemoteCall.setVisibiliteFichierDansGedPourApplication()"); if(ged()!=null) ged().setVisibilite(i); }
	 * 
	 * public Integer getVisibiliteFichierDansGedPourApplication(){
	 * System.out.println("SessionCocktailRemoteCall.getVisibiliteFichierDansGedPourApplication()"); if(ged()==null) return null; return
	 * ged().getVisibilite(); }
	 * 
	 * public Boolean clientSideRequestSupprimerFicherDansGed(Integer numero, Number persId) {
	 * System.out.println("SessionCocktailRemoteCall.clientSideRequestSupprimerFicherDansGed()"); return new Boolean(ged().deleteFileIntoGed(numero,
	 * new Integer(persId.intValue()))); }
	 * 
	 * public NSArray clientSideRequestUrlFicherDansGed(Integer numero, Number persId) {
	 * System.out.println("SessionCocktailRemoteCall.clientSideRequestUrlFicherDansGed(numero, persId)" + numero+" , "+ persId); NSArray list = null;
	 * try { ged().edtitingContext().lock(); list = ged().listUrlFileIntoGed(numero); } catch (Throwable e) { e.printStackTrace(); }finally{
	 * ged().edtitingContext().unlock(); } if (list == null) { // if(ged.getLastError().getMessage()!=null) // { // // le fichier n'a pas de courrier
	 * associÈ !!!! // GEDDescription desc =
	 * ((Application)Application.application()).gedBus(noIndividuForPersId(persId)).inspectDocumentGED(numero.intValue()); // desc. // } list = new
	 * NSArray(); } return list; }
	 * 
	 * public NSArray clientSideRequestNameFicherDansGed(Integer numero, Number persId) {
	 * System.out.println("SessionCocktailRemoteCall.clientSideRequestNameFicherDansGed()"); NSArray list = null; int noInd =
	 * noIndividuForPersId(persId); try { ged().edtitingContext().lock(); list = ged().listNameFileIntoGed(numero,noInd); } catch (Throwable e) {
	 * e.printStackTrace(); }finally{ ged().edtitingContext().unlock(); } if (list == null) { list = new NSArray(); } return list; }
	 * 
	 * private int noIndividuForPersId(Number persId) { System.out.println("SessionCocktailRemoteCall.noIndividuForPersId()"); return
	 * noIndividuForPersId(new Integer(persId.intValue())); }
	 * 
	 * private int noIndividuForPersId(Integer persId) { System.out.println("SessionCocktailRemoteCall.noIndividuForPersId()"); if(persId==null ||
	 * persId.intValue()==0 || persId.intValue()==-1) return -1; CktlUserInfoDB user = new CktlUserInfoDB(woApplication.dataBus());
	 * user.individuForPersId(persId, false); return user.noIndividu().intValue(); }
	 * 
	 * //Fin pour geid //------------------------------------------------------------------------------------------------------------
	 */

	public void setFichierResultat(File fichierResultat) {
	    threadFichierResultat = fichierResultat;
	}
	
	public void setDatas(NSData d) {
		threadDatas = d;
	}

	public void setDatasThreadStatus(String b) {
		threadStatus = b;
	}

	public void setDatasThreadException(String b) {
		threadException = b;
	}

	//	public final void clientSideRequestImprimerReportParametresLong(String reportName, NSDictionary parameters) throws Exception {
	//		ThreadRports thr1 = new ThreadRports(this,reportName,parameters);
	//		System.out.println("THREAD "+thr1);
	//		setDatasReportsStatus(SessionCocktailRemoteCall.REPORTSTATUS_ENCOURS);
	//		setDatasReports(new NSData());
	//		thr1.start();
	//	}
	//
	//	public NSDictionary clientSideRequestReturnValuesForLastImprimerReportParametresLong() {
	//		NSMutableDictionary result = new NSMutableDictionary();
	//		result.setObjectForKey(reportStatus, "STATUS");
	//		if (reportDatas != null )
	//			result.setObjectForKey(reportDatas, "DATAS");
	//		return (NSDictionary) result;
	//	}

	public class ThreadReports extends Thread {
		/** Un attribut propre à chaque thread */
		private SessionCocktailRemoteCall sender;
		private NSDictionary parameters;
		private String reportName;

		/** Création et démarrage automatique du thread */
		public ThreadReports(SessionCocktailRemoteCall sender, String reportName, NSDictionary parameters) {
			this.sender = sender;
			this.parameters = parameters;
			this.reportName = reportName;
		}

		public void run() {
			NSData datas = null;
			String exception = " ";
			try {
				datas = imprimerReportParametres();
				synchronized (this) {
					try {
						sender.setDatas(datas);

						if (datas == null)
							sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_VIDE);
						else
							sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_TERMINE);
					} catch (Throwable e1) {
						exception = "e1 " + e1.toString();
						System.out.println("e1" + e1.getMessage());
						sender.setDatas(new NSData());
						sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_PROBLEME);
						sender.setDatasThreadException(exception);
					}
					;
				}
				;

				//}
			} catch (Throwable e0) {
				e0.printStackTrace();
				exception = "e0 " + e0.toString();
				System.out.println("e0" + e0.getMessage());
				synchronized (this) {
					try {
						sender.setDatas(new NSData());
						sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_PROBLEME);
						sender.setDatasThreadException(exception);
					} catch (Throwable e) {
						e.printStackTrace();
						exception = "e " + e.toString();
						System.out.println("e" + e.getMessage());
						sender.setDatas(new NSData());
						sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_PROBLEME);
						sender.setDatasThreadException(exception);
					}
					;
				}
			}
		}

		public String toString() {
			StringBuffer buf = new StringBuffer("THREAD REPORT : ");
			if (reportName != null)
				buf.append(" reportName = [").append(reportName).append("] ");
			if (parameters != null)
				buf.append(" parameters = [").append(parameters).append("]");
			return buf.toString();
		}

		public final NSData imprimerReportParametres() throws Exception {
			System.out.println("imprimerReportParametres");
			NSMutableDictionary tmp = new NSMutableDictionary(parameters);
			HashMap params = new HashMap();
			Reporter reporter = null;

			try {

				File f = new File((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL") + reportName);
				if (f.exists()) {
					reporter = new Reporter((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL") + reportName, 0);
					tmp.takeValueForKey((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH_LOCAL"), "REP_BASE_PATH");
				}
				else {
					reporter = new Reporter((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH") + reportName, 0);
					tmp.takeValueForKey((String) woApplication.config().stringForKey("REP_BASE_JASPER_PATH"), "REP_BASE_PATH");
				}
				for (int i = 0; i < tmp.allKeys().count(); i++)
					params.put(tmp.allKeys().objectAtIndex(i), tmp.valueForKey((String) tmp.allKeys().objectAtIndex(i)));

				// TODO Amodifier
				int ret = reporter.printReport(woApplication.getJDBCConnection(), params);
				if (ret == 1) {
					ByteArrayOutputStream resultStream = reporter.exportReportToDocumentStream();
					return new NSData(resultStream.toByteArray());
				}
				else {
					throw new Exception("Rien à imprimer");
				}
			} catch (JRException e) {
				e.printStackTrace();
				System.out.println("\n \n exxxxxxx" + e.getMessage());
				throw new Exception(e.getMessage());

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("\n \n exxxxxxx" + e.getMessage());
				throw new Exception(e.getMessage());
			}

		}

	}

	public class ThreadJxls extends Thread {
		/** Un attribut propre à chaque thread */
		private SessionCocktailRemoteCall sender;
		private ExcelTool monExcelTool = null;
		private String xlstemplate;
		private String sql = null;

		/** Création et démarrage automatique du thread */
		public ThreadJxls(SessionCocktailRemoteCall sender, String xlstemplate, String sql) {
			this.sender = sender;
			this.xlstemplate = xlstemplate;
			this.sql = sql;

			monExcelTool = new ExcelTool(woApplication);
		}

		public void run() {
			String exception = "";
			try {
				ExcelToolResult exportResult = monExcelTool.exportWithJxlsAndComplexResult(xlstemplate, sql);
				File fichierResultat = exportResult.fichierResultat();
				NSData datas = exportResult.datas();
				
				synchronized (this) {
					try {
					    sender.setFichierResultat(fichierResultat);
						sender.setDatas(datas);

						if (datas == null) {
							sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_VIDE);
						} else {
							sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_TERMINE);
						}
					} catch (Throwable e) {
						sender.setDatas(new NSData());
						sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_PROBLEME);
						sender.setDatasThreadException(e.getMessage());
					}
				}
			} catch (Exception e0) {
				e0.printStackTrace();
				exception = "e0 " + e0.toString();
				System.out.println("e0" + e0.getMessage());
				synchronized (this) {
					try {
						sender.setDatas(new NSData());
						sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_PROBLEME);
						sender.setDatasThreadException(exception);
					} catch (Throwable e) {
						e.printStackTrace();
						exception = "e " + e.toString();
						System.out.println("e" + e.getMessage());
						sender.setDatas(new NSData());
						sender.setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_PROBLEME);
						sender.setDatasThreadException(exception);
					}
				}
			}
		}

		public String toString() {
			StringBuffer buf = new StringBuffer("THREAD XLS : ");
			if (xlstemplate != null)
				buf.append(" xlstemplate = [").append(xlstemplate).append("] ");
			if (sql != null)
				buf.append(" sql = [").append(sql).append("]");
			return buf.toString();
		}

	}

	public NSData clientSideRequestExportWithJxls(String xlstemplate, String sql) throws Exception {
		ExcelTool monExcelTool = new ExcelTool(woApplication);
		return monExcelTool.exportWithJxls(xlstemplate, sql);
	}

	public void clientSideRequestExportWithJxlsLong(String xlstemplate, String sql) {
		thrJxls = new ThreadJxls(this, xlstemplate, sql);
		setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_ENCOURS);
		setDatasThreadException(SessionCocktailRemoteCall.THREADEXECPTION_VIDE);
		setDatas(new NSData());
		setFichierResultat(null);
		thrJxls.start();
		lastthreadJxls = thrJxls;
		lastCallJxls = 0;

	}

	public NSDictionary clientSideRequestReturnValuesForLastJxlsParametresLong() {
		NSMutableDictionary result = new NSMutableDictionary();
		lastCallJxls = lastCallJxls + 1;
		System.out.println("lastCallJxls = " + lastCallJxls);
		if (lastCallJxls >= ((woApplication.nbTimeOut()) / 3) + 1 && lastthreadJxls == thrJxls) {
			System.out.println("interrupt...");
			thrJxls.interrupt();
			lastCallJxls = 0;
			setDatas(new NSData());
			setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_PROBLEME);
			lastthreadJxls = null;
		}
		
		System.out.println("reportStatus =" + threadStatus);
		result.setObjectForKey(threadStatus, "STATUS");
		result.setObjectForKey(threadException, "EXCEPTION");
		
		if (threadDatas != null) {
			result.setObjectForKey(threadDatas, "DATAS");
		}
		
		if (threadFichierResultat != null) {
		    result.setObjectForKey(threadFichierResultat.getAbsolutePath(), "REMOTEFILEPATH");
		}
		
		return (NSDictionary) result;
	}

	public final void clientSideRequestImprimerReportParametresLong(String reportName, NSDictionary parameters) throws Exception {
		thrReports = new ThreadReports(this, reportName, parameters);
		setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_ENCOURS);
		setDatasThreadException(SessionCocktailRemoteCall.THREADEXECPTION_VIDE);
		setDatas(new NSData());
		thrReports.start();
		lastthreadReports = thrReports;
		lastCallReports = 0;
	}

	public NSDictionary clientSideRequestPrimaryKeyForGlobalId(EOGlobalID gid) {
		try {
			return EOUtilities.primaryKeyForObject(defaultEC, defaultEC.faultForGlobalID(gid, defaultEC));
		} catch (Exception exception) {
			exception.printStackTrace();
			return new NSDictionary();
		}
	}

	public NSDictionary clientSideRequestReturnValuesForLastImprimerReportParametresLong() {
		NSMutableDictionary result = new NSMutableDictionary();
		lastCallReports = lastCallReports + 1;
		System.out.println("lastCall = " + lastCallReports);
		if (lastCallReports >= ((woApplication.nbTimeOut()) / 3) + 1 && lastthreadReports == thrReports) {
			System.out.println("interrupt...");
			thrReports.interrupt();
			lastCallReports = 0;
			setDatas(new NSData());
			setDatasThreadStatus(SessionCocktailRemoteCall.THREADSTATUS_PROBLEME);
			lastthreadReports = null;
		}

		result.setObjectForKey(threadStatus, "STATUS");
		result.setObjectForKey(threadException, "EXCEPTION");

		if (threadDatas != null) {
			result.setObjectForKey(threadDatas, "DATAS");
		}
		
		if (threadFichierResultat != null) {
		    result.setObjectForKey(threadFichierResultat.getAbsolutePath(), "REMOTEFILEPATH");
        }
		
		return (NSDictionary) result;
	}

}
