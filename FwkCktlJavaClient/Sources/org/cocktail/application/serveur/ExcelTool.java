/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.serveur;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlwebapp.common.util.services.FileName;

import net.sf.jxls.report.ResultSetCollection;
import net.sf.jxls.transformer.XLSTransformer;

import com.webobjects.foundation.NSData;

public class ExcelTool {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss-SS";
    
	private CocktailApplication woApplication = null;

	public ExcelToolResult exportWithJxlsAndComplexResult(String xlstemplate, String sql) throws Exception {
	    File templateFile = xlsTemplateFile(xlstemplate);
	    File fichierResultat = genererFichierUtilisantDateDuJour(xlstemplate);
	    return new ExcelToolResult(fichierResultat, exportWithJxlsImpl(xlstemplate, sql, templateFile, fichierResultat));
	}
	
    public NSData exportWithJxls(String xlstemplate, String sql) throws Exception {
        File templateFile = xlsTemplateFile(xlstemplate);
        File fichierResultat = genererFichierUtilisantTemplate(templateFile);
        return exportWithJxlsImpl(xlstemplate, sql, templateFile, fichierResultat);
	}
	
	public NSData exportWithJxlsImpl(String xlstemplate, String sql, File templateFile, File fichierResultat) throws Exception {

		// recherche du template sur le serveur
		String templateUrl = "";
		String templateUrlResultat = "";
        NSData resultat = null;
        Connection defaultCon = woApplication.getJDBCConnection();
        
        // connection 
        Connection con = woApplication.getNewJDBCConnectionForModelName();
        try {

            templateUrl = templateFile.getAbsolutePath();
    		templateUrlResultat = fichierResultat.getAbsolutePath();
    
    		// creation du select count(*)
            String sqlcount = "select count(*) from (" + sql + ")";
    
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sqlcount);

			int count = 0;
			if (rs.next()) {
				count = rs.getInt(1);
			}

			rs = stmt.executeQuery(sql);

			Map beans = new HashMap();
			ResultSetCollection rsc = new ResultSetCollection(rs, count, true);
			beans.put("item", rsc);
			XLSTransformer transformer = new XLSTransformer();
			transformer.transformXLS(templateUrl, beans, templateUrlResultat);
			stmt.close();
			rs.close();
            resultat = getFileFromServeur(templateUrlResultat);
		} catch (Throwable e) {
            System.out.println("FREDFRED "+e.getMessage());
            resultat = null;
            throw new Exception ("Erreur creation xls\n - template = "+xlstemplate+"\n - sql = "+sql);
		} finally {
		    if (!defaultCon.equals(con)) {
                try {
                    con.close();
                } catch (SQLException e) {
                    System.out.println("FREDFRED " + e.getMessage());
                    resultat = null;
                    throw new Exception (e.getMessage());
                }
            }
        }
        return resultat;
	}

	public NSData getFileFromServeur(String pathFile) {
		if (pathFile == null) {
            System.out.println("Erreur : le chemin est null");
            return null;
        }
		File f = new File(pathFile);
		if (f.exists()) {
			try {
				return new NSData(f.toURL());
			} catch (Throwable e) {
				e.printStackTrace();
			}
		} 
		return null;
	}
	
	protected File xlsTemplateFile(String xlstemplate) throws Exception {
	    File f = new File((String) woApplication.config().stringForKey("REP_BASE_XLS_PATH_LOCAL") + xlstemplate);
        if (f.exists()) {
            return f;
        } else {
            f = new File((String) woApplication.config().stringForKey("REP_BASE_XLS_PATH") + xlstemplate);
            if (f.exists()) {
                return f;
            } else {    
                System.out.println("fichier template inexistant " + f.getAbsolutePath());
                throw new Exception("fichier template inexistant " + f.getAbsolutePath());
            }
        }
	}

	protected File genererFichierUtilisantTemplate(File xlsFolder) {
        String targetDir = System.getProperty("java.io.tmpdir");
        if (!targetDir.endsWith(java.io.File.separator)) {
            targetDir = targetDir + java.io.File.separator;
        }
        
        java.util.Calendar calLastModif = java.util.Calendar.getInstance();
        calLastModif.setTime(new java.util.Date(xlsFolder.lastModified()));
        String lastModif = "-" + calLastModif.get(java.util.Calendar.DAY_OF_MONTH) + "." 
        + (calLastModif.get(java.util.Calendar.MONTH) + 1) + "." 
        + calLastModif.get(java.util.Calendar.YEAR) + "-"
        + calLastModif.get(java.util.Calendar.HOUR_OF_DAY) + "h" 
        + calLastModif.get(java.util.Calendar.MINUTE) + "m" 
        + calLastModif.get(java.util.Calendar.SECOND);
        String newName = targetDir + "resultat" + lastModif + ".xls";

        return new java.io.File(newName);
	}
	
	protected File genererFichierUtilisantDateDuJour(String nom) throws ParseException {
        String targetDir = System.getProperty("java.io.tmpdir");
        if (!targetDir.endsWith(java.io.File.separator)) {
            targetDir = targetDir + java.io.File.separator;
        }
        
        String newName = targetDir + new FileName().getNameWithCurrentDate(nom, DEFAULT_DATE_FORMAT); 
        
        return new java.io.File(newName);
    }
	
	public ExcelTool(CocktailApplication woApplication) {
		super();
		this.woApplication = woApplication;
	}
	
	public static class ExcelToolResult {
	    private File fichierResultat;
	    private NSData datas;
	    
	    public ExcelToolResult(File fichierResultat, NSData datas) {
	        this.fichierResultat = fichierResultat;
	        this.datas = datas;
	    }
	    
	    public File fichierResultat() {
	        return this.fichierResultat;
	    }
	    
	    public NSData datas() {
	        return this.datas;
	    }
	}
}
