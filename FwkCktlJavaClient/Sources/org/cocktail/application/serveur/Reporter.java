package org.cocktail.application.serveur;

/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr) 

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.foundation.NSDictionary;
/**
 * Classe fournissant les outils permettant de produire un report en utilisant
 * la librairie jasperReports.
 *
 * @author bertrand.gauthier@univ-paris5.fr
 * (adapt du travail de Rodolphe Prin de l'ULR)
 */
public class Reporter {

    private JasperReport jasperReport;
    private JRXmlDataSource _jrxmlds;
    private JasperPrint printResult;
	
	private JRExporter formatExporter;
    	
    /**
	* Constructeur
	* @param jasperFileName le nom du fichier .jasper (maquette)
	* @param formatChoisi le n¡ de format de sortie demand (PDF<->0, XLS<->1, etc.)
     * @throws JRException 
	*/
    public Reporter(final String jasperFileName, final int formatChoisi) throws Exception {
        super();
        //try {
            File f = new File(jasperFileName);
            if (!f.exists()) 
                throw new Exception("Le fichier "+jasperFileName+ " n'existe pas.");
           // }
           // else
           // {
			//chargement du report :
            jasperReport = (JasperReport) JRLoader.loadObject(jasperFileName);
			
			//instanciation de la classe d'exportation correcte :
			ExporterFactory factory = new ExporterFactory(formatChoisi);
			formatExporter = factory.getExporterInstance();
			
   //         System.out.println(jasperFileName + " charg.");
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            throw new Exception(e.getMessage());
//        }
    }

    /**
     * Initialise la datasource  partir du flux xml
     * @param xmlDataStream Flux des datas xml
     * @param xmlRecordPath Le recordPath 
     * @throws JRException
     */
    public void initDataSource(final InputStream xmlDataStream, final String xmlRecordPath) throws JRException {
        _jrxmlds = new JRXmlDataSource(xmlDataStream, xmlRecordPath);
     //   System.out.println("Reporter.initDataSource()>>> "+"Datasource XML cre avec recordpath '"+xmlRecordPath+"'.");
    }


    /**
	 * Remplie le report dans une structure de type JasperPrint
     * @param parameters Parametres ventuels  passer au report ( les $P{...} )
     * @throws JRException
     */
    public void printReport(final Map parameters) throws JRException {
        printResult = JasperFillManager.fillReport(jasperReport, parameters, _jrxmlds);
    //    System.out.println("Reporter.printReport()>>> " + "Report rempli." + printResult.toString());
    }
	
     /* Remplit le report à partir d'une requete sql Specifie en parametre. Fonctionne egalement si la requete est incluse dans le report (dans ce cas, Specifier null pour le parametre sqlQuery).
     * @param connection Connection à la base de donnees.
     * @param sqlQuery Requete SQL. Si la chaine n'est pas nulle, elle est ajoutée aux parametres passes au report (@see SQLQUERY_KEY) 
     * @param parameters Parametres à passer au report
     * @throws JRException
     */



    /**
	 * Exporte le report vers un flux de sortie (dans le format de document lisible choisi avant)
     * @throws JRException
     */
    public ByteArrayOutputStream exportReportToDocumentStream() throws JRException {
        ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
		
		//rglage des paramtres (d'autres paramtres peuvent tre rgls dans le ExporterFactory...) et export :
        formatExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, tmpStream);
        formatExporter.setParameter(JRExporterParameter.JASPER_PRINT, printResult);
        formatExporter.exportReport();
	//	System.out.println("Reporter.exportReportToDocumentStream()>>> " + "Document final gnr.");
        return tmpStream;
    }
	
	/**
	 * Exporte le report vers un fichier PDF local.
     * @throws JRException
     */
    public void exportReportToPdfFile(final String filePath) throws JRException {
		//rglage des paramtres (d'autres paramtres peuvent tre rgls dans le ExporterFactory...) et export :
		formatExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, filePath);
        formatExporter.setParameter(JRExporterParameter.JASPER_PRINT, printResult);
        formatExporter.exportReport();
        //System.out.println("Le fichier "+filePath+" a ete cr sur le disque." );
    }
    
    
    
//    public NSDictionary getConnectionDictionary(final String modelName) {
//        return EOModelGroup.defaultGroup().modelNamed(modelName).connectionDictionary();
//    }
    
    //
//  public int printReport(final Connection connection,final Map parameters) throws Exception {
//      
//      printResult = JasperFillManager.fillReport(jasperReport, parameters, connection);
//      int pageCount = printResult.getPages().size();
//      if (pageCount==0) {
//          return 0;
//      }        
//   return 1;                    
//  }


        public int printReport(final Connection connection,final Map parameters) throws Exception {

//        NSDictionary dicoConnection = getConnectionDictionary(((CocktailApplication)WOApplication.application()).mainModelName());
//         DatabaseHelper dbHelper = new DatabaseHelper();
//        Class.forName("oracle.jdbc.driver.OracleDriver");
//        Connection con = DriverManager.getConnection(
//        dicoConnection.valueForKey("URL").toString(), dicoConnection.valueForKey("username").toString(), dicoConnection.valueForKey("password").toString()
//        );
        Connection con = ((CocktailApplication)WOApplication.application()).getNewJDBCConnectionForModelName();
        try {
            printResult = JasperFillManager.fillReport(jasperReport, parameters, con);
        } catch (Throwable e) {
            System.out.println(e.getMessage());
            System.out.println("###### Erreur creation report\n - report = "+jasperReport.getName());
            System.out.println(" - parameters = "+parameters);
            throw new Exception(e.getMessage()+jasperReport.getQuery().getText());
        } finally{
            if(!connection.equals(con))
                    con.close();
        }

        int pageCount = printResult.getPages().size();
        if (pageCount==0) {
        return 0;
        } 
        return 1; 
        }

}
