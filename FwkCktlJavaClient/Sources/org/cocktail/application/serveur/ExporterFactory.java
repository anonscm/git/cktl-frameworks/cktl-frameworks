package org.cocktail.application.serveur;

/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)


import com.webobjects.foundation.*;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.*;
/*
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
*/

/**
* Classe fournissant le bon JRExporter en fonction du format choisi
* (PDF, XLS, etc.)
*
* Implementation assez classique je crois du "design pattern" Factory.
*
* L'interface ExporterConstants declare les constantes utiles a cette classe.
*
* @author bertrand.gauthier@univ-paris5.fr		//beber
*/
public class ExporterFactory
{
	/**
	* Future instance de la classe d'exportation.
	* Le type concret de cette instance dépend du format de sortie demandé.
	*/
	protected JRExporter exporterChoisi;
	
	/**
	* ID du format d'exportation choisi.
	*/
	protected int idFormatChoisi;
	
	/**
	* Constantes representants les codes de format de sortie.
	*/
	static final public int PDF_FORMAT_ID = 0;	//pdf
	static final public int XLS_FORMAT_ID = 1;	//tableur excel
	static final public int CSV_FORMAT_ID = 2;	//csv
	static final public int HTML_FORMAT_ID = 3;	//html
	
	static final public String PDF_EXTENSION = ".pdf";
	static final public String XLS_EXTENSION = ".xls";
	static final public String CSV_EXTENSION = ".csv";
	static final public String HTML_EXTENSION = ".html";
	
	/**
	* Constructeur avec comme argument le format de sortie demandé.
	* @param formatID le format de document demandé (PDF, XLS, etc.)
	*/
	public ExporterFactory(final int formatID) {
		idFormatChoisi = formatID;
		//System.out.println("format " + formatID);
		//instanciation et paramétrage particulier de l'exporter en fonction du format demandé :
		switch(formatID) {
			case PDF_FORMAT_ID:	//PDF
				exporterChoisi = new JRPdfExporter();
				break;
			case XLS_FORMAT_ID: //tableur excel
				exporterChoisi = new JRXlsExporter(); 
				exporterChoisi.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
				exporterChoisi.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				exporterChoisi.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				break;
			case CSV_FORMAT_ID: //CSV
				exporterChoisi = new JRCsvExporter();
				exporterChoisi.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, ";");
				break;
			case HTML_FORMAT_ID:
				exporterChoisi = new JRHtmlExporter();
				exporterChoisi.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				break;
				
			default: 
				exporterChoisi = new JRPdfExporter(); 
				idFormatChoisi = PDF_FORMAT_ID;
				break;
		}
	}
	
	/**
	* Constructeur par défaut. Un format de document est choisi arbitrairement, ici c'est PDF.
	*/
	public ExporterFactory() {
		this(PDF_FORMAT_ID);
	}
	
	/**
	* Constructeur par défaut. Un format de document est choisi arbitrairement.
	* @return Une instance de la classe d'exportation dans le format choisi
	*/
	public JRExporter getExporterInstance() {
		return exporterChoisi;
	}
	
	/*
	public NSDictionary getParticularExporterParametersToSet() {
	
		parameter:JRExporterParameter / value:Object
		NSMutableArray paramsArray = new NSMutableArray();
		NSMutableArray valuesArray = new NSMutableArray();
		
		switch(formatID) {
			case PDF_FORMAT_ID:
				paramsArray.addObject(JRExporterParameter.
				break;
			case XLS_FORMAT_ID: 
				
				break;
			default: 
				
				break;
		}
		NSDictionary paramToSet = new NSDictionary(paramsArray.immutableClone(), valuesArray.immutableClone());
		return paramToSet;
	}*/
}
