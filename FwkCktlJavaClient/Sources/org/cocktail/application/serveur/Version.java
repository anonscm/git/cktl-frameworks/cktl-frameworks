package org.cocktail.application.serveur;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

public class Version extends A_CktlVersion {
	// Nom de l'appli
		public static final String FRAMEWORKFINALNAME = VersionCocktailApplication.FRAMEWORKFINALNAME;
		public static final String FRAMEWORKINTERNALNAME = VersionCocktailApplication.FRAMEWORKINTERNALNAME;
		public static final String FRAMEWORK_STRID = VersionCocktailApplication.FRAMEWORK_STRID;
	    
		// Version appli
	    public static final long SERIALVERSIONUID = VersionCocktailApplication.SERIALVERSIONUID;
	    
	    public static final int VERSIONNUMMAJ =     VersionCocktailApplication.VERSIONNUMMAJ;
	    public static final int VERSIONNUMMIN =     VersionCocktailApplication.VERSIONNUMMIN;
	    public static final int VERSIONNUMPATCH =   VersionCocktailApplication.VERSIONNUMPATCH;
	    public static final int VERSIONNUMBUILD =   VersionCocktailApplication.VERSIONNUMBUILD;
	    
	    public static final String VERSIONDATE = VersionCocktailApplication.VERSIONDATE;
	    public static final String COMMENT = VersionCocktailApplication.COMMENT;
	    
	@Override
	public String name() {
		return FRAMEWORKFINALNAME;
	}
	
	public String internalName() {
		return FRAMEWORKINTERNALNAME;
	}

	@Override
	public int versionNumMaj() {
		return VERSIONNUMMAJ;
	}

	@Override
	public int versionNumMin() {
		return VERSIONNUMMIN;
	}

	@Override
	public int versionNumPatch() {
		return VERSIONNUMPATCH ;
	}

	@Override
	public int versionNumBuild() {
		return VERSIONNUMBUILD;
	}
	
	@Override
	public String date() {
		return VERSIONDATE;
	}
	
	@Override
	public String comment() {
		return COMMENT;
	}

	@Override
	public CktlVersionRequirements[] dependencies() {
		return null;
	}
}
