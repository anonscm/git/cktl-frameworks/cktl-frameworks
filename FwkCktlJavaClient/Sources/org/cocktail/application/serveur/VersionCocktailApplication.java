/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 14 juin 07
 * author mparadot 
 */
package org.cocktail.application.serveur;




public final class VersionCocktailApplication extends CocktailCollecteVersion{
   
    public static final String FRAMEWORKFINALNAME = "FwkCktlJavaClient";
	public static final String FRAMEWORKINTERNALNAME = "FwkCktlJavaClient";
	
	public static final String FRAMEWORK_STRID = "Framework JavaClient";
	
	public static final long SERIALVERSIONUID = 0x2220L;
	
	public static final int VERSIONNUMMAJ = 2;
	public static final int VERSIONNUMMIN = 2;
	public static final int VERSIONNUMPATCH = 1;
	public static final int VERSIONNUMBUILD = 0;
	public static final String VERSIONDATE = "21/06/2013";
	public static final String COMMENT = "CocktailApplication2 en WO5.4";

	public String appliDate() {
        return VERSIONDATE;
    }

    public String appliId() {
        return FRAMEWORK_STRID;
    }

    public int appliVersionMajeure() {
        return VERSIONNUMMAJ;
    }

    public int appliVersionMineure() {
        return VERSIONNUMMIN;
    }

    public int appliVersionPatch() {
        return VERSIONNUMPATCH;
    }

    public int appliVersionBuild() {
        return VERSIONNUMBUILD;
    }

    public String minAppliBdVersion() {
        return " ";
    }

    public void checkDependencies() throws Exception {
    }

    public String commentaire() {
        return COMMENT;
    }
    
}

