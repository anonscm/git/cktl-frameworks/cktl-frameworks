/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStructureUlr.java instead.
package org.cocktail.application.serveur.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOStructureUlr extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ca_StructureUlr";
	public static final String ENTITY_TABLE_NAME = "grhum.structure_ulr";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cStructure";

	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String GRP_RESPONSABLE_KEY = "grpResponsable";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "c_structure";
	public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
	public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
	public static final String GRP_RESPONSABLE_COLKEY = "grp_responsable";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "ll_structure";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String CA__STRUCTURE_ULRS_KEY = "ca_StructureUlrs";
	public static final String INDIVIDU_RESPONSABLE_KEY = "individuResponsable";
	public static final String TOS_REPART_PERSONNE_ADRESSES_KEY = "tosRepartPersonneAdresses";
	public static final String TOS_REPART_TYPE_GROUPES_KEY = "tosRepartTypeGroupes";
	public static final String TOS_V_PERSONNE_TELEPHONES_KEY = "tosVPersonneTelephones";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public String cStructurePere() {
    return (String) storedValueForKey(C_STRUCTURE_PERE_KEY);
  }

  public void setCStructurePere(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_PERE_KEY);
  }

  public String cTypeStructure() {
    return (String) storedValueForKey(C_TYPE_STRUCTURE_KEY);
  }

  public void setCTypeStructure(String value) {
    takeStoredValueForKey(value, C_TYPE_STRUCTURE_KEY);
  }

  public Integer grpResponsable() {
    return (Integer) storedValueForKey(GRP_RESPONSABLE_KEY);
  }

  public void setGrpResponsable(Integer value) {
    takeStoredValueForKey(value, GRP_RESPONSABLE_KEY);
  }

  public String lcStructure() {
    return (String) storedValueForKey(LC_STRUCTURE_KEY);
  }

  public void setLcStructure(String value) {
    takeStoredValueForKey(value, LC_STRUCTURE_KEY);
  }

  public String llStructure() {
    return (String) storedValueForKey(LL_STRUCTURE_KEY);
  }

  public void setLlStructure(String value) {
    takeStoredValueForKey(value, LL_STRUCTURE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.application.serveur.eof.EOIndividuUlr individuResponsable() {
    return (org.cocktail.application.serveur.eof.EOIndividuUlr)storedValueForKey(INDIVIDU_RESPONSABLE_KEY);
  }

  public void setIndividuResponsableRelationship(org.cocktail.application.serveur.eof.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOIndividuUlr oldValue = individuResponsable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_RESPONSABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_RESPONSABLE_KEY);
    }
  }
  
  public NSArray ca_StructureUlrs() {
    return (NSArray)storedValueForKey(CA__STRUCTURE_ULRS_KEY);
  }

  public NSArray ca_StructureUlrs(EOQualifier qualifier) {
    return ca_StructureUlrs(qualifier, null);
  }

  public NSArray ca_StructureUlrs(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = ca_StructureUlrs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToCa_StructureUlrsRelationship(org.cocktail.application.serveur.eof.EOStructureUlr object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CA__STRUCTURE_ULRS_KEY);
  }

  public void removeFromCa_StructureUlrsRelationship(org.cocktail.application.serveur.eof.EOStructureUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CA__STRUCTURE_ULRS_KEY);
  }

  public org.cocktail.application.serveur.eof.EOStructureUlr createCa_StructureUlrsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_StructureUlr");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CA__STRUCTURE_ULRS_KEY);
    return (org.cocktail.application.serveur.eof.EOStructureUlr) eo;
  }

  public void deleteCa_StructureUlrsRelationship(org.cocktail.application.serveur.eof.EOStructureUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CA__STRUCTURE_ULRS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCa_StructureUlrsRelationships() {
    Enumeration objects = ca_StructureUlrs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCa_StructureUlrsRelationship((org.cocktail.application.serveur.eof.EOStructureUlr)objects.nextElement());
    }
  }

  public NSArray tosRepartPersonneAdresses() {
    return (NSArray)storedValueForKey(TOS_REPART_PERSONNE_ADRESSES_KEY);
  }

  public NSArray tosRepartPersonneAdresses(EOQualifier qualifier) {
    return tosRepartPersonneAdresses(qualifier, null);
  }

  public NSArray tosRepartPersonneAdresses(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosRepartPersonneAdresses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosRepartPersonneAdressesRelationship(org.cocktail.application.serveur.eof.EORepartPersonneAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_PERSONNE_ADRESSES_KEY);
  }

  public void removeFromTosRepartPersonneAdressesRelationship(org.cocktail.application.serveur.eof.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_PERSONNE_ADRESSES_KEY);
  }

  public org.cocktail.application.serveur.eof.EORepartPersonneAdresse createTosRepartPersonneAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_RepartPersonneAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_PERSONNE_ADRESSES_KEY);
    return (org.cocktail.application.serveur.eof.EORepartPersonneAdresse) eo;
  }

  public void deleteTosRepartPersonneAdressesRelationship(org.cocktail.application.serveur.eof.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_PERSONNE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartPersonneAdressesRelationships() {
    Enumeration objects = tosRepartPersonneAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartPersonneAdressesRelationship((org.cocktail.application.serveur.eof.EORepartPersonneAdresse)objects.nextElement());
    }
  }

  public NSArray tosRepartTypeGroupes() {
    return (NSArray)storedValueForKey(TOS_REPART_TYPE_GROUPES_KEY);
  }

  public NSArray tosRepartTypeGroupes(EOQualifier qualifier) {
    return tosRepartTypeGroupes(qualifier, null);
  }

  public NSArray tosRepartTypeGroupes(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosRepartTypeGroupes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosRepartTypeGroupesRelationship(org.cocktail.application.serveur.eof.EORepartTypeGroupe object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_TYPE_GROUPES_KEY);
  }

  public void removeFromTosRepartTypeGroupesRelationship(org.cocktail.application.serveur.eof.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_TYPE_GROUPES_KEY);
  }

  public org.cocktail.application.serveur.eof.EORepartTypeGroupe createTosRepartTypeGroupesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_RepartTypeGroupe");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_TYPE_GROUPES_KEY);
    return (org.cocktail.application.serveur.eof.EORepartTypeGroupe) eo;
  }

  public void deleteTosRepartTypeGroupesRelationship(org.cocktail.application.serveur.eof.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_TYPE_GROUPES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartTypeGroupesRelationships() {
    Enumeration objects = tosRepartTypeGroupes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartTypeGroupesRelationship((org.cocktail.application.serveur.eof.EORepartTypeGroupe)objects.nextElement());
    }
  }

  public NSArray tosVPersonneTelephones() {
    return (NSArray)storedValueForKey(TOS_V_PERSONNE_TELEPHONES_KEY);
  }

  public NSArray tosVPersonneTelephones(EOQualifier qualifier) {
    return tosVPersonneTelephones(qualifier, null);
  }

  public NSArray tosVPersonneTelephones(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosVPersonneTelephones();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosVPersonneTelephonesRelationship(org.cocktail.application.serveur.eof.VPersonneTelephone object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNE_TELEPHONES_KEY);
  }

  public void removeFromTosVPersonneTelephonesRelationship(org.cocktail.application.serveur.eof.VPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNE_TELEPHONES_KEY);
  }

  public org.cocktail.application.serveur.eof.VPersonneTelephone createTosVPersonneTelephonesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_VPersonneTelephone");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_V_PERSONNE_TELEPHONES_KEY);
    return (org.cocktail.application.serveur.eof.VPersonneTelephone) eo;
  }

  public void deleteTosVPersonneTelephonesRelationship(org.cocktail.application.serveur.eof.VPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNE_TELEPHONES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosVPersonneTelephonesRelationships() {
    Enumeration objects = tosVPersonneTelephones().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosVPersonneTelephonesRelationship((org.cocktail.application.serveur.eof.VPersonneTelephone)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOStructureUlr avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOStructureUlr createEOStructureUlr(EOEditingContext editingContext, String cStructure
, String cTypeStructure
, String llStructure
, String temValide
			) {
    EOStructureUlr eo = (EOStructureUlr) createAndInsertInstance(editingContext, _EOStructureUlr.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setCTypeStructure(cTypeStructure);
		eo.setLlStructure(llStructure);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOStructureUlr localInstanceIn(EOEditingContext editingContext) {
	  		return (EOStructureUlr)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructureUlr creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructureUlr creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOStructureUlr object = (EOStructureUlr)createAndInsertInstance(editingContext, _EOStructureUlr.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOStructureUlr localInstanceIn(EOEditingContext editingContext, EOStructureUlr eo) {
    EOStructureUlr localInstance = (eo == null) ? null : (EOStructureUlr)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOStructureUlr#localInstanceIn a la place.
   */
	public static EOStructureUlr localInstanceOf(EOEditingContext editingContext, EOStructureUlr eo) {
		return EOStructureUlr.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOStructureUlr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStructureUlr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOStructureUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOStructureUlr)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOStructureUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStructureUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStructureUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStructureUlr)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOStructureUlr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOStructureUlr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOStructureUlr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOStructureUlr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
