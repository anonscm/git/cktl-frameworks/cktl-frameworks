

// EOAdresse.java
// 

package org.cocktail.application.serveur.eof;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.util.*;

public class EOAdresse extends _EOAdresse
{
    public EOAdresse() {
        super();
    }
    
    public String afficheAdresse(){
        StringBuffer str = new StringBuffer();
        str.append(adrAdresse1());
        if(adrAdresse2()!=null)
            str.append(adrAdresse2());
        if(codePostal()!=null)
            str.append(" ").append(codePostal());
        if(ville() !=null)
            str.append(" ").append(ville());
        return str.toString();
    }

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

}
