/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCodeAnal.java instead.
package org.cocktail.application.serveur.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCodeAnal extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ca_CodeAnal";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.CODE_ANALYTIQUE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "canId";

	public static final String CAN_CODE_KEY = "canCode";
	public static final String CAN_FERMETURE_KEY = "canFermeture";
	public static final String CAN_LIBELLE_KEY = "canLibelle";
	public static final String CAN_MONTANT_KEY = "canMontant";
	public static final String CAN_NIVEAU_KEY = "canNiveau";
	public static final String CAN_OUVERTURE_KEY = "canOuverture";
	public static final String CAN_PUBLIC_KEY = "canPublic";
	public static final String CAN_UTILISABLE_KEY = "canUtilisable";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CAN_ID_PERE_KEY = "canIdPere";

//Colonnes dans la base de donnees
	public static final String CAN_CODE_COLKEY = "CAN_CODE";
	public static final String CAN_FERMETURE_COLKEY = "CAN_FERMETURE";
	public static final String CAN_LIBELLE_COLKEY = "CAN_LIBELLE";
	public static final String CAN_MONTANT_COLKEY = "CAN_MONTANT";
	public static final String CAN_NIVEAU_COLKEY = "CAN_NIVEAU";
	public static final String CAN_OUVERTURE_COLKEY = "CAN_OUVERTURE";
	public static final String CAN_PUBLIC_COLKEY = "CAN_PUBLIC";
	public static final String CAN_UTILISABLE_COLKEY = "CAN_UTILISABLE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CAN_ID_PERE_COLKEY = "CAN_ID_PERE";


	// Relationships
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_ETAT_UTILISABLE_KEY = "typeEtatUtilisable";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String canCode() {
    return (String) storedValueForKey(CAN_CODE_KEY);
  }

  public void setCanCode(String value) {
    takeStoredValueForKey(value, CAN_CODE_KEY);
  }

  public NSTimestamp canFermeture() {
    return (NSTimestamp) storedValueForKey(CAN_FERMETURE_KEY);
  }

  public void setCanFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, CAN_FERMETURE_KEY);
  }

  public String canLibelle() {
    return (String) storedValueForKey(CAN_LIBELLE_KEY);
  }

  public void setCanLibelle(String value) {
    takeStoredValueForKey(value, CAN_LIBELLE_KEY);
  }

  public java.math.BigDecimal canMontant() {
    return (java.math.BigDecimal) storedValueForKey(CAN_MONTANT_KEY);
  }

  public void setCanMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CAN_MONTANT_KEY);
  }

  public Integer canNiveau() {
    return (Integer) storedValueForKey(CAN_NIVEAU_KEY);
  }

  public void setCanNiveau(Integer value) {
    takeStoredValueForKey(value, CAN_NIVEAU_KEY);
  }

  public NSTimestamp canOuverture() {
    return (NSTimestamp) storedValueForKey(CAN_OUVERTURE_KEY);
  }

  public void setCanOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, CAN_OUVERTURE_KEY);
  }

  public Integer canPublic() {
    return (Integer) storedValueForKey(CAN_PUBLIC_KEY);
  }

  public void setCanPublic(Integer value) {
    takeStoredValueForKey(value, CAN_PUBLIC_KEY);
  }

  public Integer canUtilisable() {
    return (Integer) storedValueForKey(CAN_UTILISABLE_KEY);
  }

  public void setCanUtilisable(Integer value) {
    takeStoredValueForKey(value, CAN_UTILISABLE_KEY);
  }

  public Integer tyetId() {
    return (Integer) storedValueForKey(TYET_ID_KEY);
  }

  public void setTyetId(Integer value) {
    takeStoredValueForKey(value, TYET_ID_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.application.serveur.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.serveur.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.application.serveur.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeEtat typeEtatUtilisable() {
    return (org.cocktail.application.serveur.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_UTILISABLE_KEY);
  }

  public void setTypeEtatUtilisableRelationship(org.cocktail.application.serveur.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeEtat oldValue = typeEtatUtilisable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_UTILISABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_UTILISABLE_KEY);
    }
  }
  

/**
 * Créer une instance de EOCodeAnal avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCodeAnal createEOCodeAnal(EOEditingContext editingContext, String canCode
, String canLibelle
, Integer canNiveau
, NSTimestamp canOuverture
, Integer canPublic
, Integer canUtilisable
, Integer tyetId
, org.cocktail.application.serveur.eof.EOTypeEtat typeEtat, org.cocktail.application.serveur.eof.EOTypeEtat typeEtatUtilisable			) {
    EOCodeAnal eo = (EOCodeAnal) createAndInsertInstance(editingContext, _EOCodeAnal.ENTITY_NAME);    
		eo.setCanCode(canCode);
		eo.setCanLibelle(canLibelle);
		eo.setCanNiveau(canNiveau);
		eo.setCanOuverture(canOuverture);
		eo.setCanPublic(canPublic);
		eo.setCanUtilisable(canUtilisable);
		eo.setTyetId(tyetId);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setTypeEtatUtilisableRelationship(typeEtatUtilisable);
    return eo;
  }

  
	  public EOCodeAnal localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCodeAnal)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeAnal creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeAnal creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCodeAnal object = (EOCodeAnal)createAndInsertInstance(editingContext, _EOCodeAnal.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCodeAnal localInstanceIn(EOEditingContext editingContext, EOCodeAnal eo) {
    EOCodeAnal localInstance = (eo == null) ? null : (EOCodeAnal)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCodeAnal#localInstanceIn a la place.
   */
	public static EOCodeAnal localInstanceOf(EOEditingContext editingContext, EOCodeAnal eo) {
		return EOCodeAnal.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeAnal fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeAnal fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeAnal eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeAnal)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeAnal fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeAnal fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeAnal eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeAnal)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeAnal fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeAnal eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeAnal ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeAnal fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
