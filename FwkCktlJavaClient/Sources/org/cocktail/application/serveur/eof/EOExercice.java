

// EOExercice.java
// 

package org.cocktail.application.serveur.eof;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.*;

public class EOExercice extends _EOExercice
{
	public static final String	PRIMARY_KEY_KEY	= "exeOrdre";
	
	public static final String	EXERCICE_OUVERT = "O";
	public static final String	EXERCICE_RESTREINT = "R";
	public static final String	EXERCICE_PREPARATION = "P";
	public static final String	EXERCICE_CLOS = "C";
	
    public EOExercice() {
        super();
    }

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

}
