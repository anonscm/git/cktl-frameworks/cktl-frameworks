package org.cocktail.application.serveur;

/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Iterator;

import org.cocktail.fwkcktlacces.server.handler.JarResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EODatabaseChannel;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOObjectNotAvailableException;
import com.webobjects.eoaccess.EOStoredProcedure;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;
import com.webobjects.jdbcadaptor.JDBCContext;

public abstract class CocktailApplication extends CktlWebApplication {

	// attributs
	EOEditingContext applicationEC;
	EODatabaseContext dbContext;

	NSDictionary confDictionary;
	private NSMutableDictionary appParametres;

	private String parametresTableName = null;
	private static VersionCocktailApplication versionCocktailApplication;


	public static void main(String argv[]) {
		//		System.out.println("CocktailApplication.main(argv)" + argv);
		WOApplication.main(argv, CocktailApplication.class);
	}

	public static VersionCocktailApplication versionCocktailApplication(){
		if(versionCocktailApplication==null)
			versionCocktailApplication = new VersionCocktailApplication();
		return versionCocktailApplication;
	}

	public boolean forceUseNewConnexion(){
		return !super.mainModelName().equals(mainModelName());
	}

	public CocktailApplication() {
		super();

		// redirection des logs
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));

		if (isDirectConnectEnabled()) {
			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}
		//permet d'aller chercher des ressources dans les jars
		registerRequestHandler(new JarResourceRequestHandler(), "_wr_");

		NSLog.out.appendln("");
		NSLog.out.appendln("");
		NSLog.out.appendln("");
		NSLog.out.appendln("-----------------------------------------------------------------------");
		NSLog.out.appendln("Bienvenue dans " + this.name() + " !");
		NSLog.out.appendln(this.getVersionApp());
		NSLog.out.appendln(versionCocktailApplication().appliId() +" "+versionCocktailApplication().txtAppliVersion());
		if (!super.mainModelName().equals(mainModelName())) {
			applicationEC = new EOEditingContext();

			// Delegue de EODatabaseContext pour generer des cles specifiques
			dbContext = EOUtilities.databaseContextForModelNamed(applicationEC, mainModelName());
			dbContext.setDelegate(this);

			// Definition du timeout (defaut = 12 heures)
			this.setSessionTimeOut(new Integer(getMandatoryParameterFoKeyString("DEFAULT_SESSION_TIME_OUT")));

			//         Recuperation d'une JDBCChannel pour execution des stored procedures
			try {
				// Recup de l'EC par default
				applicationEC = new EOEditingContext();
				dbContext = EOUtilities.databaseContextForModelNamed(applicationEC, mainModelName());

			}
			catch(Throwable e) {
				if(e instanceof EOObjectNotAvailableException) {
					this.addLogMessage("Erreur", "Application() - EOModel inconnu : \n"+e.getMessage());
				}
				else {
					this.addLogMessage("Exception", "Application() - "+e.getMessage());
				}
				this.terminate();
			}
			this.addLogMessage("INFORMATION","IDs SAUT : "+getOptionnalParameterFoKeyString("SAUT_ID_TRANSLATION"));
			this.addLogMessage("INFORMATION","fichier de configuration : "+configFileName());
		}
		if(config().intForKey("MODE_APPLICATION")>0)
		{
			//            LRLog.setLevel(2);
			// NSLog.debug.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupSQLGeneration);
			NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupDatabaseAccess);
		}
		if (checkVersionApplication()) {
			NSLog.out.appendln(versionApplication().txtAppliVersion());
			NSLog.out.appendln("-----------------------------------------------------------------------");
			NSLog.out.appendln("Test de la version des framworks :");
			try {
				versionApplication().checkDependencies();
			} catch (Exception e) {
				NSLog.err.appendln(e.getMessage());
				NSLog.err.appendln("Attention, tout les framworks ne sont pas a jour !!!!!");
				NSLog.err.appendln("L'application va ce terminer !!!");
				terminate();
			}
			NSLog.out.appendln("-----------------------------------------------------------------------");
		}
		if(!checkModels())
		{
			NSLog.err.appendln("ATTENTION : tous les models ne sont pas sur la meme instance de la Base !!!");
		}
		NSLog.out.appendln();
		NSLog.out.appendln("-----------------------------------------------------------------------");
		NSLog.out.appendln();
	}

	public String pathLog(){
		try {
			return System.getProperties().getProperty("WOOutputPath");
		} catch (Throwable e) {
		}
		return null;
	}

	private boolean enDev(){
		if(System.getProperties().getProperty("WODev")==null)
			return false;
		String ips = System.getProperties().getProperty("WODev");
		String ipLocal = null;
		try {
			InetAddress addr = InetAddress.getLocalHost();
			//            Get hostname
			String hostname = addr.getHostName();
			//            Get IP Address
			InetAddress ipAddr = InetAddress.getByName(hostname);
			ipLocal = ipAddr.getHostAddress();
		} catch (UnknownHostException e) {
		}
		return ips.indexOf(ipLocal)>=0;
	}

	public static String versionApplicationName(){
		// FIXME Se débarrasser de ce truc static tout moche
		return "org.cocktail."+((CocktailApplication)application()).applicationName().toLowerCase()+".serveur.Version"+((CocktailApplication)application()).applicationName();
	}

	public String applicationName(){
		if(config().stringForKey("APP_ALIAS")!=null)
			return config().stringForKey("APP_ALIAS");
		return name();
	}

	private cocktailClassLoader classLoader(){
		return new cocktailClassLoader();
	}

	public static CocktailCollecteVersion versionApplication(){
		try {
			Class version = Class.forName(versionApplicationName());
			return (CocktailCollecteVersion)version.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean checkVersionApplication(){
		try {
			Class c =  Class.forName(versionApplicationName());
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public Connection getJDBCConnection() {
		return ((JDBCContext)getAdaptorContext()).connection();
	}

	public Connection getNewJDBCConnectionForModelName(){
		NSDictionary dicoConnection = null;
		try {
			dicoConnection = EOModelGroup.defaultGroup().modelNamed(mainModelName()).connectionDictionary();
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Throwable e) {
			addLogMessage("INFORMATION","le modelName n'est pas redefini -> utilisation de la connexion par defaut");
			return getJDBCConnection();
		}
		try{
			return DriverManager.getConnection(dicoConnection.valueForKey("URL").toString(), dicoConnection.valueForKey("username").toString(), dicoConnection.valueForKey("password").toString());
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getJDBCConnection();
	}

	public EOAdaptorContext getAdaptorContext() {
		return getAdaptorChannel().adaptorContext();
	}

	public EODatabaseContext getDatabaseContext() {
		return CktlDataBus.databaseContext();
	}

	public EOAdaptorChannel getAdaptorChannel() {
		return getDatabaseContext().availableChannel().adaptorChannel();
	}

	public String bdConnexionName()
	{
		try{
			EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
			EOModel vModel = vModelGroup.modelNamed(mainModelName());
			NSDictionary vDico = vModel.connectionDictionary();
			return (String) vDico.valueForKey("URL");
		}catch(Throwable e){
			this.addLogMessage("Erreur","Impossible de trouver le model '"+mainModelName()+"'");
		}
		return "";
	}

	// Retourne le modele de l'application
	public String getMailHost() {
		return getMandatoryParameterFoKeyString("HOST_MAIL");
	}
	/* Methodes concernant l'execution des stored procedures */
	// Execution d'une procedure stockee, retourne true si la procedure a ete executee avec succes.
	// Cette methode est destinee a l'appel par la methode clientSideRequest...
	public boolean executeStoredProcedure(String proc, NSDictionary dico) {
		EOStoredProcedure eoProcedure = null;

		// Recuperation du databaseChannel
		this.addLogMessage(this.getClass().getName(), "executeStoredProcedure("+proc+")");
		EODatabaseChannel dbChannel = dbContext.availableChannel();
		EOAdaptorChannel adChannel = dbChannel.adaptorChannel();
		if (!adChannel.isOpen()) {
			this.addLogMessage(this.getClass().getName(), "executeStoredProcedure() - ouverture de adChannel");
			adChannel.openChannel();
		}
		// Effectuer un premier essai
		try {
			eoProcedure = EOModelGroup.defaultGroup().storedProcedureNamed(proc);
			if(eoProcedure==null) {
				this.addLogMessage("Erreur", "Application.executeStoredProcedure() - Procedure stockee inconnue : "+proc);
				return false;
			}
			adChannel.executeStoredProcedure(eoProcedure, dico);
		}
		catch(Exception e) {
			this.addLogMessage("Exception", "Application.executeStoredProcedure("+proc+") - "+e.getMessage()+"\n");

			e.printStackTrace();
			return false;
		}
		return  true;
	}


	public String executeStoredProcedureRaiseSQL(String proc, NSDictionary dico) {
		EOStoredProcedure eoProcedure = null;

		// Recuperation du databaseChannel
		this.addLogMessage(this.getClass().getName(), "executeStoredProcedure("+proc+")");
		EODatabaseChannel dbChannel = dbContext.availableChannel();
		EOAdaptorChannel adChannel = dbChannel.adaptorChannel();
		if (!adChannel.isOpen()) {
			this.addLogMessage(this.getClass().getName(), "executeStoredProcedure() - ouverture de adChannel");
			adChannel.openChannel();
		}
		// Effectuer un premier essai
		try {
			eoProcedure = EOModelGroup.defaultGroup().storedProcedureNamed(proc);
			if(eoProcedure==null) {
				this.addLogMessage("Erreur", "Application.executeStoredProcedure() - Procedure stockee inconnue : "+proc);
				return "Procedure stockee inconnue";
			}
			adChannel.executeStoredProcedure(eoProcedure, dico);
		}
		catch(Exception e) {
			this.addLogMessage("Exception", "Application.executeStoredProcedure("+proc+") - "+e.getMessage()+"\n");

			e.printStackTrace();
			return e.getMessage();
		}
		return  "";
	}

	// Debut de transaction
	public boolean beginTransaction() {
		addLogMessage(this.getClass().getName(), "beginTransaction()");
		EOAdaptorContext adContext = dbContext.adaptorContext();
		try {
			adContext.beginTransaction();
		}
		catch(Exception e) {
			this.addLogMessage("Erreur", "Application.clientSideRequestBeginTransaction() : RunTime Exception - "+e.getMessage());
			return false;
		}
		return true;
	}

	// Validation de la transaction en cours
	public boolean commitTransaction() {
		addLogMessage(this.getClass().getName(), "commitTransaction()");
		EOAdaptorContext adContext = dbContext.adaptorContext();

		try {
			adContext.commitTransaction();
		}
		catch(Exception e) {
			this.addLogMessage("Erreur", "Application.clientSideRequestCommitTransaction() : RunTime Exception - "+e.getMessage());
			return false;
		}
		return true;
	}

	// Annulation de la transation en cours
	public boolean rollbackTransaction() {
		addLogMessage(this.getClass().getName(), "rollbackTransaction()");
		EOAdaptorContext adContext = dbContext.adaptorContext();

		try {
			adContext.rollbackTransaction();
		}
		catch(Exception e) {
			this.addLogMessage("Erreur", "Application.rollbackTransaction() : RunTime Exception - "+e.getMessage());
			return false;
		}
		return true;
	}

	public String getVersionApp()
	{
		VersionApp versionApp = this.injector().getInstance(VersionApp.class);
		return versionApp.majVersion() + "." + versionApp.minVersion() + "." + versionApp.patchVersion() + "." + versionApp.buildVersion() + " du " + versionApp.dateVersion();
	}

	public NSDictionary returnValuesForLastStoredProcedureInvocation() {
		this.addLogMessage(this.getClass().getName(), "returnValuesForLastStoredProcedureInvocation()");
		// Recuperation de l'ad channel
		EODatabaseChannel dbChannel = dbContext.availableChannel();
		EOAdaptorChannel adChannel = dbChannel.adaptorChannel();
		// Ouverture si necessaire
		if (!adChannel.isOpen()) {
			adChannel.openChannel();
		}
		// Tester si toutes le valeurs sont recuperees... voir doc WO
		NSDictionary fetchedDico;
		do {
			fetchedDico = adChannel.fetchRow();
		}while(fetchedDico != null);
		// Recupe du dico
		NSDictionary returnDico = adChannel.returnValuesForLastStoredProcedureInvocation();
		return returnDico;
	}

	/**
    Ecriture de messages sur le log stream.
	 */
	public void addLogMessage(String errorType, String message) {
		NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y - %H:%M:%S");
		NSLog.out.appendln("["+formatter.format(new NSTimestamp())+"] "+version()+" - "+errorType+" : "+message);
	}

	public String copyright() {
		return versionCocktailApplication.copyright();
	}

	public String configFileName(){
		return super.configFileName();
	}

	public String mainModelName() {
		return super.mainModelName();
	}

	//    abstract public String mainModelName();

	public String getOptionnalParameterFoKeyString(String key){
		return (String) config().stringForKey(key);
	}

	public String getMandatoryParameterFoKeyString(String key){
		String s = (String) config().stringForKey(key);
		if (s == null)
		{
			addLogMessage( "PROBLEME GRAVE :", "Parametre manquant : "+key);
			terminate();
		}
		return s;
	}

	public boolean checkModels() {
		NSLog.out.appendln("Connexion(s) a la base de donnees :");
		NSMutableDictionary dicoBdConnexionServerName=null;
		NSMutableDictionary dicoBdConnexionServerId=null;
		try {
			// On recupere tous les modeles utilises
			dicoBdConnexionServerName = new NSMutableDictionary();
			dicoBdConnexionServerId = new NSMutableDictionary();
			EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
			for (int i = 0; i < vModelGroup.models().count(); i++) {
				EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
				dicoBdConnexionServerName.takeValueForKey(bdConnexionServerName(tmpEOModel), tmpEOModel.name());
				dicoBdConnexionServerId.takeValueForKey(bdConnexionServerId(tmpEOModel), tmpEOModel.name());
				NSLog.out.appendln("");
				NSLog.out.appendln("    Modele " + tmpEOModel.name() + " : ");
				NSLog.out.appendln("    - Connexion base de donnees = "+ tmpEOModel.connectionDictionary().valueForKey("URL"));
				NSLog.out.appendln("    - Instance = "+ bdConnexionServerId(tmpEOModel));
				NSLog.out.appendln("    - User base de donnees = "+ tmpEOModel.connectionDictionary().valueForKey("username"));
			}
			// Verifier que tous les modeles sont sur le meme serverId
			String sid = null;
			boolean erreurSid = false;
			for (int i = 0; i < vModelGroup.models().count(); i++) {
				EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
				if (sid == null) {
					sid = bdConnexionServerId(tmpEOModel);
				} else if (!sid.equalsIgnoreCase(bdConnexionServerId(tmpEOModel))) {
					erreurSid = true;
				}
			}
			if (erreurSid) {
				addLogMessage("INFORMATION","Les modeles pointent vers differentes instances de base de donnees (cf. ci-dessus), ceci peut causer des problemes d'incoherence lors de l'execution.");
			}
			return !erreurSid;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Renvoie le serverid de la base de donnees (par exemple gest).
	 */
	private String bdConnexionServerId(EOModel model) {
		String[] parts = bdSecondPartUrl(model);
		String serverBdName = null;
		if (parts.length > 1) {
			serverBdName = parts[parts.length - 1];
		}
		return serverBdName;
	}

	private String[] bdSecondPartUrl(EOModel model) {
		String url = (String) model.connectionDictionary().valueForKey("URL");
		if (url == null || url.length() == 0) {
			return new String[0];
		}
		String[] res;
		// L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
		// On separe la partie jdbc de la partie server
		res = url.split("@");
		if (res.length > 1) {
			String serverUrl = res[1];
			res = serverUrl.split(":");
			if (res.length > 0) {
				return res;
			}
		}
		return new String[0];
	}

	/**
	 * Renvoie le serverName de la base de donnees (par exemple jane).
	 */
	private String bdConnexionServerName(EOModel model) {
		String[] parts = bdSecondPartUrl(model);
		String serverName = null;
		if (parts.length > 0) {
			serverName = parts[0];
		}
		return serverName;
	}

	class cocktailClassLoader extends SecureClassLoader{
	}

	public int nbTimeOut(){
		if(config().intForKey("TIMEOUT_REPORT")<=0)
			return 60;
		return config().intForKey("TIMEOUT_REPORT");
	}

	/****************************************************************************************	
	  GESTION DES SESSIONS ET UTILISATEURS CONNECTES
	 ****************************************************************************************/	

	private HashMap<?, CocktailSession> mySessions = new HashMap();
	public HashMap getMySessions() {
		return mySessions;
	}
	public void setMySessions(final HashMap mySessions) {
		this.mySessions = mySessions;
	}

	/**
	 * Ecrit la liste des utilisateurs connetces dans la console.
	 */
	public final void listConnectedUsers() {

		for (Iterator iter = mySessions.values().iterator(); iter.hasNext();) {
			final CocktailSession element = (CocktailSession) iter.next();
			System.out.println("Application.listConnectedUsers()" + element.getInfoConnectedUser());
		}
	}

	/****************************************************************************************	
	  GESTION DES LOGS 
	 ****************************************************************************************/	

	protected 	MyByteArrayOutputStream	redirectedOutStream, redirectedErrStream;
	private		static final int LOG_OUT_TAILLE_MAXI = 100000;

	public void cleanLogs()	{
		redirectedErrStream.reset();
		redirectedOutStream.reset();		
	}
	public String outLogs()	{
		return redirectedOutStream.toString();
	}

	public String errLogs()	{
		return redirectedErrStream.toString();
	}

	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;
		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}
		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}
		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);	    			    		
			out.write(b, off, len);

			if (count > LOG_OUT_TAILLE_MAXI)	{
				try {
					String logServerOut = redirectedOutStream.toString();

					if (logServerOut.length() > (LOG_OUT_TAILLE_MAXI / 2)) {
						logServerOut = logServerOut.substring((LOG_OUT_TAILLE_MAXI / 2), logServerOut.length());
					}
					// On reaffecte cette valeur a la variable redirectOutStream
					redirectedOutStream.reset();
					redirectedOutStream.write(logServerOut.getBytes());
				}
				catch (Exception e)	{
					e.printStackTrace();
					redirectedOutStream.reset();
				}
			}
		}
	}
}

