/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 14 juin 07
 * author mparadot 
 */
package org.cocktail.application.serveur;

import com.webobjects.foundation.NSComparator;


public abstract class CocktailCollecteVersion{
	
	public static void main(String argv[]) {
		//A remplir dans les applications
    }
    
    public abstract void checkDependencies() throws Exception;
    
    /**
     * Version de l'application :
     *      VersionMajeur.VersionMineure.VersionPatch
     * @return String de la version de l'application
     */
    public final String appliVersion() {
        if(appliVersionBuild()==0)
            return appliVersionMajeure() + "." + appliVersionMineure() + "." + appliVersionPatch();
        return appliVersionMajeure() + "." + appliVersionMineure() + "." + appliVersionPatch()+ "."+ appliVersionBuild();
    }

    /**
     * Affichage HTML complet de la version de l'application avec date
     * @return String HTML de la verion de l'application
     */
    public final String htmlAppliVersion() {
        return "<font color=\"#FF0000\"><b>Version " + appliVersion() + " du " + appliDate() + "</b></font>";
    }

    /**
     * Affichage txt complet de la version de l'application avec date
     * @return String txt de la verion de l'application
     */
    public final String txtAppliVersion() {
        return "Version " + appliVersion() + " du " + appliDate();
    }

    /**
     * Copyright de l'application "(c)+date+Consortium Cocktail"
     * @return String du copyright
     */
    public final String copyright() {
		return "(c) " + appliDate().substring(appliDate().length() - 4) + " Association Cocktail";
    }
    
    /**
     * Date de l'application
     * @return String de la date de l'application
     */
    public abstract String appliDate();

    /**
     * Le nom de l'application
     * @return String du nom de l'application
     */
    public abstract String appliId();
    
    /**
     * Verion Majeur de l'application :
     *      "Gros" modification de l'application
     * @return int de la version majeur
     */
    public abstract int appliVersionMajeure();

    /**
     * Verion Mineure de l'application :
     *      modification et ajout de fonctionalite à l'application
     * @return int de la version mineure
     */
    public abstract int appliVersionMineure();

    /**
     * Verion Patch de l'application :
     *      +/- correction de bug
     * @return int de la version majeur
     */
    public abstract int appliVersionPatch();
    
    /**
     * Verion Build de l'application :
     *      les autres versions....
     * @return int de la version build
     */
    public abstract int appliVersionBuild();

    /**
     * Version minimale de la base de donnee pour executer l'application
     * @return String de la version minimal
     */
    public abstract String minAppliBdVersion();
    
    /**
     * Version de l'application
     */
    public int[] version(){
    	return new int[] {appliVersionMajeure(), appliVersionMineure(), appliVersionPatch(), appliVersionBuild()};
    }
    
    /**
     * Commentaire sur le version
     * @return String du Commentaire
     */
    public abstract String commentaire();
    
    public int compare(CocktailCollecteVersion obj) {
        if(obj.appliVersionMajeure()>appliVersionMajeure())
            return NSComparator.OrderedAscending;
        if(obj.appliVersionMajeure()==appliVersionMajeure()
        && obj.appliVersionMineure()>appliVersionMineure())
            return NSComparator.OrderedAscending;
        if(obj.appliVersionMajeure()==appliVersionMajeure()
                && obj.appliVersionMineure()==appliVersionMineure()
                && obj.appliVersionPatch()>appliVersionPatch())
            return NSComparator.OrderedAscending;
        if(obj.appliVersionMajeure()==appliVersionMajeure()
                && obj.appliVersionMineure()==appliVersionMineure()
                && obj.appliVersionPatch()==appliVersionPatch()
                && obj.appliVersionBuild()>appliVersionBuild())
            return NSComparator.OrderedAscending;
        if(obj.appliVersionMajeure()==appliVersionMajeure()
                && obj.appliVersionMineure()==appliVersionMineure()
                && obj.appliVersionPatch()==appliVersionPatch()
                && obj.appliVersionBuild()==appliVersionBuild())
            return NSComparator.OrderedSame;
        return NSComparator.OrderedDescending;
    }

    public int compare(int maj, int min, int patch, int build) {
    	return compare(version(), new int[] {maj,min,patch,build});
    }
    
    /**
	 * Compare 2 numeros de version donnes sous la forme de tableaux de int.
	 * Chaque numero de version peut contenir autant d'elements que l'on
	 * veut.<br>
	 * 2 numeros null ou vides sont consideres egaux.<br>
	 * 1 numero null est considere inferieur a l'autre non null.<br>
	 * 1 numero vide est considere inferieur a l'autre non vide.<br>
	 * <b>ATTENTION:<b> 1.2.0 est considere egal a 1.2 !!!
	 * 
	 * @param version
	 * @param anotherVersion
	 * @return Un entier negatif, zero, ou un entier positif si le premier
	 *         argument est inferieur, egal a, ou superieur au second.
	 */
	public static int compare(final int[] version, final int[] anotherVersion) {
		if (version == null && anotherVersion == null) {
			return 0;
		}
		if (version == null) {
			return -1;
		}
		if (anotherVersion == null) {
			return 1;
		}
		for (int i = 0; i < version.length && i < anotherVersion.length; i++) {
			if (version[i] < anotherVersion[i]) {
				return -1;
			}
			if (version[i] > anotherVersion[i]) {
				return 1;
			}
		}
		if (version.length < anotherVersion.length) {
			for (int i = version.length; i < anotherVersion.length; i++) {
				if (anotherVersion[i] > 0) {
					return -1;
				}
			}
		}
		if (version.length > anotherVersion.length) {
			for (int i = anotherVersion.length; i < version.length; i++) {
				if (version[i] > 0) {
					return 1;
				}
			}
		}
		return 0;
	}
}
