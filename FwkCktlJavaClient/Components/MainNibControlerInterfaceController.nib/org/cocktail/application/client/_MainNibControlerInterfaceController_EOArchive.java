// _MainNibControlerInterfaceController_EOArchive.java
// Generated by EnterpriseObjects palette at mardi 28 ao\u00fbt 2007 15 h 26 Europe/Paris

package org.cocktail.application.client;

import com.webobjects.eoapplication.*;
import com.webobjects.eocontrol.*;
import com.webobjects.eointerface.*;
import com.webobjects.eointerface.swing.*;
import com.webobjects.eointerface.swing.EOTable._EOTableColumn;
import com.webobjects.foundation.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.text.*;

public class _MainNibControlerInterfaceController_EOArchive extends com.webobjects.eoapplication.EOArchive {
    com.webobjects.eocontrol.EODataSource _eoDataSource0;
    com.webobjects.eocontrol.EOEditingContext _eoEditingContext0;
    com.webobjects.eointerface.EODisplayGroup _eoDisplayGroup0;
    com.webobjects.eointerface.EOTableAssociation _eoTableAssociation0;
    com.webobjects.eointerface.EOTableColumnAssociation _eoTableColumnAssociation0;
    com.webobjects.eointerface.swing.EOFrame _eoFrame0, _eoFrame1;
    com.webobjects.eointerface.swing.EOTable _nsTableView0;
    com.webobjects.eointerface.swing.EOTable._EOTableColumn _eoTableColumn0;
    com.webobjects.eointerface.swing.EOView _nsCustomView0;
    com.webobjects.foundation.NSNumberFormatter _nsNumberFormatter0;
    javax.swing.JPanel _nsView0, _nsView1;

    public _MainNibControlerInterfaceController_EOArchive(Object owner, NSDisposableRegistry registry) {
        super(owner, registry);
    }

    protected void _construct() {
        Object owner = _owner();
        EOArchive._ObjectInstantiationDelegate delegate = (owner instanceof EOArchive._ObjectInstantiationDelegate) ? (EOArchive._ObjectInstantiationDelegate)owner : null;
        Object replacement;

        super._construct();

        _nsNumberFormatter0 = (com.webobjects.foundation.NSNumberFormatter)_registered(new com.webobjects.foundation.NSNumberFormatter("0;-0"), "");
        _nsTableView0 = (com.webobjects.eointerface.swing.EOTable)_registered(new com.webobjects.eointerface.swing.EOTable(), "");
        _eoTableAssociation0 = (com.webobjects.eointerface.EOTableAssociation)_registered(new com.webobjects.eointerface.EOTableAssociation(_nsTableView0), "");

        if ((delegate != null) && ((replacement = delegate.objectForOutletPath(this, "view")) != null)) {
            _nsCustomView0 = (replacement == EOArchive._ObjectInstantiationDelegate.NullObject) ? null : (com.webobjects.eointerface.swing.EOView)replacement;
            _replacedObjects.setObjectForKey(replacement, "_nsCustomView0");
        } else {
            _nsCustomView0 = (com.webobjects.eointerface.swing.EOView)_registered(new com.webobjects.eointerface.swing.EOView(), "View");
        }

        if ((delegate != null) && ((replacement = delegate.objectForOutletPath(this, "component")) != null)) {
            _eoFrame1 = (replacement == EOArchive._ObjectInstantiationDelegate.NullObject) ? null : (com.webobjects.eointerface.swing.EOFrame)replacement;
            _replacedObjects.setObjectForKey(replacement, "_eoFrame1");
        } else {
            _eoFrame1 = (com.webobjects.eointerface.swing.EOFrame)_registered(new com.webobjects.eointerface.swing.EOFrame(), "MainWindow");
        }

        _nsView1 = (JPanel)_eoFrame1.getContentPane();

        if ((delegate != null) && ((replacement = delegate.objectForOutletPath(this, "editingContext")) != null)) {
            _eoEditingContext0 = (replacement == EOArchive._ObjectInstantiationDelegate.NullObject) ? null : (com.webobjects.eocontrol.EOEditingContext)replacement;
            _replacedObjects.setObjectForKey(replacement, "_eoEditingContext0");
        } else {
            _eoEditingContext0 = ((com.webobjects.eocontrol.EOEditingContext.substitutionEditingContext() != null) ? com.webobjects.eocontrol.EOEditingContext.substitutionEditingContext() : (com.webobjects.eocontrol.EOEditingContext)_registered(new com.webobjects.eocontrol.EOEditingContext(), "EditingContext"));
        }

        if ((delegate != null) && ((replacement = delegate.objectForOutletPath(this, "displayGroup.dataSource")) != null)) {
            _eoDataSource0 = (replacement == EOArchive._ObjectInstantiationDelegate.NullObject) ? null : (com.webobjects.eocontrol.EODataSource)replacement;
            _replacedObjects.setObjectForKey(replacement, "_eoDataSource0");
        } else {
            _eoDataSource0 = (com.webobjects.eocontrol.EODataSource)_registered(com.webobjects.eoapplication.EODataSourceFactory.defaultDataSourceFactory().newMasterDataSource(_eoEditingContext0, "ca_Exercice", null), "DataSource");
        }

        if ((delegate != null) && ((replacement = delegate.objectForOutletPath(this, "displayGroup")) != null)) {
            _eoDisplayGroup0 = (replacement == EOArchive._ObjectInstantiationDelegate.NullObject) ? null : (com.webobjects.eointerface.EODisplayGroup)replacement;
            _replacedObjects.setObjectForKey(replacement, "_eoDisplayGroup0");
        } else {
            _eoDisplayGroup0 = (com.webobjects.eointerface.EODisplayGroup)_registered(new com.webobjects.eointerface.EODisplayGroup(), "ca_Exercice");
        }

        _eoTableColumn0 = (com.webobjects.eointerface.swing.EOTable._EOTableColumn)_registered(new com.webobjects.eointerface.swing.EOTable._EOTableColumn(), "");
        _eoTableColumnAssociation0 = (com.webobjects.eointerface.EOTableColumnAssociation)_registered(new com.webobjects.eointerface.EOTableColumnAssociation(_eoTableColumn0, _nsTableView0), "");
        _eoFrame0 = (com.webobjects.eointerface.swing.EOFrame)_registered(new com.webobjects.eointerface.swing.EOFrame(), "Panel");
        _nsView0 = (JPanel)_eoFrame0.getContentPane();
    }

    protected void _awaken() {
        super._awaken();

        if (_replacedObjects.objectForKey("_eoDisplayGroup0") == null) {
            _connect(_owner(), _eoDisplayGroup0, "displayGroup");
        }

        if (_replacedObjects.objectForKey("_nsCustomView0") == null) {
            _connect(_owner(), _nsCustomView0, "view");
        }

        if (_replacedObjects.objectForKey("_eoFrame1") == null) {
            _connect(_owner(), _eoFrame1, "component");
        }

        if (_replacedObjects.objectForKey("_eoEditingContext0") == null) {
            _connect(_owner(), _eoEditingContext0, "editingContext");
        }
    }

    protected void _init() {
        super._init();
        _nsNumberFormatter0.setLocalizesPattern(true);
        _eoTableAssociation0.bindAspect(com.webobjects.eointerface.EOAssociation.SourceAspect, _eoDisplayGroup0, "");
        _eoTableAssociation0.setSortsByColumnOrder(true);
        _eoTableAssociation0.establishConnection();
        if (!(_nsView1.getLayout() instanceof EOViewLayout)) { _nsView1.setLayout(new EOViewLayout()); }
        _nsCustomView0.setSize(135, 65);
        _nsCustomView0.setLocation(13, 14);
        ((EOViewLayout)_nsView1.getLayout()).setAutosizingMask(_nsCustomView0, EOViewLayout.WidthSizable | EOViewLayout.HeightSizable);
        _nsView1.add(_nsCustomView0);

        if (_replacedObjects.objectForKey("_eoFrame1") == null) {
            _nsView1.setSize(161, 93);
            _eoFrame1.setTitle("Window");
            _eoFrame1.setLocation(195, 865);
            _eoFrame1.setSize(161, 93);
        }

        if (_replacedObjects.objectForKey("_eoDataSource0") == null) {
            _connect(_eoDataSource0, _eoEditingContext0, "editingContext");
        }

        if (_replacedObjects.objectForKey("_eoDisplayGroup0") == null) {
            _connect(_eoDisplayGroup0, _eoDataSource0, "dataSource");
        }

        if (_replacedObjects.objectForKey("_eoDisplayGroup0") == null) {
            _eoDisplayGroup0.setValidatesChangesImmediately(false);
            _eoDisplayGroup0.setFetchesOnLoad(true);
            _eoDisplayGroup0.setUsesOptimisticRefresh(false);
            _eoDisplayGroup0.setSelectsFirstObjectAfterFetch(true);
        }

        _eoTableColumnAssociation0.bindAspect(com.webobjects.eointerface.EOAssociation.ValueAspect, _eoDisplayGroup0, "exeOrdre");
        if (_eoTableColumnAssociation0.canSupportValueFormatter()) { _eoTableColumnAssociation0.setValueFormatter(_nsNumberFormatter0); }
        _eoTableColumnAssociation0.establishConnection();
        _eoTableColumn0.setMinWidth(10);
        _eoTableColumn0.setMaxWidth(1000);
        _eoTableColumn0.setPreferredWidth(300);
        _eoTableColumn0.setWidth(300);
        _eoTableColumn0.setResizable(true);
        _eoTableColumn0.setHeaderValue("Exe Ordre");
        if ((_eoTableColumn0.getHeaderRenderer() != null)) {
        	((DefaultTableCellRenderer)(_eoTableColumn0.getHeaderRenderer())).setHorizontalAlignment(javax.swing.JTextField.LEFT);
        }
        _nsTableView0.table().addColumn(_eoTableColumn0);
        _setFontForComponent(_nsTableView0.table().getTableHeader(), "Lucida Grande", 11, Font.PLAIN);
        _nsTableView0.table().setRowHeight(19);
        if (!(_nsView0.getLayout() instanceof EOViewLayout)) { _nsView0.setLayout(new EOViewLayout()); }
        _nsTableView0.setSize(320, 200);
        _nsTableView0.setLocation(13, 14);
        ((EOViewLayout)_nsView0.getLayout()).setAutosizingMask(_nsTableView0, EOViewLayout.MinYMargin);
        _nsView0.add(_nsTableView0);
        _nsView0.setSize(346, 228);
        _eoFrame0.setTitle("Panel");
        _eoFrame0.setLocation(403, 270);
        _eoFrame0.setSize(346, 228);
    }
}
