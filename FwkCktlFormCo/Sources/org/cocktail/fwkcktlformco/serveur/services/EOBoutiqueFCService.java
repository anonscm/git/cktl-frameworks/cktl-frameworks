/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.kava.server.finder.FinderBoutiqueType;
import org.cocktail.kava.server.metier.EOBoutique;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Factorise les traitement de gestion des boutique de FC
 * 
 * @author jb
 * 
 */
public class EOBoutiqueFCService {
	public static final String BOUTIQUE_TYPEFC = "FC";
	public static final String OUI="O";
	public static final String NON="N";

	/**
	 * Initialise les valeurs pour la creation d'une boutique de FC
	 * @param edc
	 * @return
	 * @throws FormcoException
	 */
	public static EOBoutique createBoutiqueFC(EOEditingContext edc) throws FormcoException {
		if (edc == null) {
			throw new FormcoException(
					"Instanciation d'une boutique FC Impossible: EditingContext null.");
		}
		EOBoutique boutiqueFC = EOBoutique.createNewInstance(edc);
		edc.insertObject(boutiqueFC);
		boutiqueFC.setBoutiqueType(FinderBoutiqueType.getTypeBoutique(edc,
				BOUTIQUE_TYPEFC));
		boutiqueFC.setBoutiqueArticleExclusif(NON);
		boutiqueFC.setBoutiqueArticleUnique(NON);
		boutiqueFC.setBoutiqueOptionUnique(NON);
		boutiqueFC.setPaiementWebPossible(false);
		return boutiqueFC;
	}
}
