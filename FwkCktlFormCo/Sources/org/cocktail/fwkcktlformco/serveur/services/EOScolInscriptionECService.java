/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.procedure.ProcedurePLmdInsIEcCarte;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * 
 * @author jb
 * 
 */
public abstract class EOScolInscriptionECService {

	/**
	 * Copie de SCOLARITE.P_LMD_INS_IEC_CARTE
	 * BUG
	 * @param ec
	 * @param idiplNumero
	 * @param mrueKey
	 * @param mrecKey
	 * @param msemOrdre
	 * @param imrecDispense
	 * @param fannKey
	 * @return
	 */
	public static EOScolInscriptionEc inscrireECALaCarte(EOEditingContext ec,
			int idiplNumero, int mrecKey, int msemOrdre, int imrecDispense,
			int fannKey) {
		EOScolInscriptionEc inscriptionaEC = EOScolInscriptionEc.create(ec);
		int modulo = msemOrdre % 2;
		String conditionStr = EOScolMaquetteRepartitionAp.MRAP_SEMESTRE_KEY
				+ " = %@ AND " + EOScolMaquetteRepartitionAp.FANN_KEY_KEY
				+ " = %@ AND " + EOScolMaquetteRepartitionEc.MREC_KEY_KEY
				+ " = %@ AND " + EOScolMaquetteRepartitionEc.MEC_KEY_KEY
				+ " = " + EOScolMaquetteRepartitionAp.MEC_KEY_KEY;

		NSMutableArray<Object> tbkeys = new NSMutableArray<Object>();
		tbkeys.add(modulo);
		tbkeys.add(fannKey);
		tbkeys.add(mrecKey);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbkeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteRepartitionAp.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<EOScolMaquetteRepartitionAp> mrepAPs = ec
				.objectsWithFetchSpecification(fetchSpec);

		// est-il inscrit a un EC
		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.add(new EOKeyValueQualifier(
				EOScolInscriptionEc.IMREC_SEMESTRE_KEY,
				EOQualifier.QualifierOperatorEqual, modulo));
		quals.add(new EOKeyValueQualifier(EOScolInscriptionEc.IDIPL_NUMERO_KEY,
				EOQualifier.QualifierOperatorEqual, idiplNumero));
		quals.add(new EOKeyValueQualifier(EOScolInscriptionEc.MREC_KEY_KEY,
				EOQualifier.QualifierOperatorEqual, mrecKey));

		NSArray<EOScolInscriptionEc> inscriptionECs = EOScolInscriptionEc
				.fetchAll(ec, new EOAndQualifier(quals));
		if ((inscriptionECs == null) || (inscriptionECs.size() <= 0)) {

			_CktlBasicDataBus dataBus = new _CktlBasicDataBus(ec);
			ProcedurePLmdInsIEcCarte.enregistrer(dataBus, idiplNumero, 0,mrecKey,modulo, 0, fannKey);
			NSArray<EOQualifier> qualIecs = new NSMutableArray<EOQualifier>();
			qualIecs.add(new EOKeyValueQualifier(
					EOScolInscriptionEc.IDIPL_NUMERO_KEY,
					EOQualifier.QualifierOperatorEqual, idiplNumero));
			qualIecs.add(new EOKeyValueQualifier(
					EOScolInscriptionEc.MREC_KEY_KEY,
					EOQualifier.QualifierOperatorEqual, mrecKey));
			qualIecs.add(new EOKeyValueQualifier(
					EOScolInscriptionEc.IMREC_SEMESTRE_KEY,
					EOQualifier.QualifierOperatorEqual, new Long((long) modulo)));
			inscriptionaEC = EOScolInscriptionEc.fetchFirstByQualifier(ec,
					new EOAndQualifier(qualIecs));
			// inscriptionaEC.setFannKey(fannKey);
			// // inscriptionaEC.setIdiplNumero(idiplNumero);
			// // inscriptionaEC.setMrecKey(mrecKey);
			// inscriptionaEC.setImrecSemestre(new Long((long) modulo));
			// inscriptionaEC.setImrecDispense(imrecDispense);
			// inscriptionaEC.setImrecEtat(new Long((long) 0));
			// inscriptionaEC.setImrecPoints1(null);
			// inscriptionaEC.setImrecAbsence1(new Long((long) 0));
			// inscriptionaEC.setImrecMention1(new Long((long) 0));
			// inscriptionaEC.setImrecSession1(null);
			// inscriptionaEC.setImrecPoints2(null);
			// inscriptionaEC.setImrecAbsence2(new Long((long) 0));
			// inscriptionaEC.setImrecMention2(new Long((long) 0));
			// inscriptionaEC.setImrecSession2(null);
			// inscriptionaEC.setImrecCoefficient(null);
			// inscriptionaEC.setImrecNoteBase(null);
			// inscriptionaEC.setImrecPonderation(null);
			// inscriptionaEC.setImrecPointJury(null);
			// inscriptionaEC.setImrecObtention(null);
			// EOScolInscriptionEtudiant insEtu =
			// FinderEoScolInscriptionEtudiant
			// .findInsEtud(ec, fannKey, idiplNumero);
			// if (insEtu != null) {
			// inscriptionaEC
			// .setToFwkScolarite_ScolInscriptionEtudiantRelationship(insEtu);
			// }
			// EOScolMaquetteRepartitionEc mrepEc = EOScolMaquetteRepartitionEc
			// .fetchFirstByQualifier(ec, new EOKeyValueQualifier(
			// EOScolMaquetteRepartitionEc.MREC_KEY_KEY,
			// EOQualifier.QualifierOperatorEqual, mrecKey));
			//
			// if (mrepEc != null) {
			// inscriptionaEC
			// .setToFwkScolarite_ScolMaquetteRepartitionEcRelationship(mrepEc);
			// }
		} else
			inscriptionaEC = inscriptionECs.get(0);

		// deja fait ds la procedure stockee
		// for (int i = 0; i < mrepAPs.count(); i++) {
		// // Inscription a l'AP
		// EOScolMaquetteRepartitionAp mrepAp = mrepAPs.get(i);
		// int mrapKey = Integer.parseInt(ERXEOControlUtilities
		// .primaryKeyStringForObject(mrepAp));
		// EOScolInscriptionAPService.inscrireAPALaCarte(ec, idiplNumero,
		// mrapKey, modulo, imrecDispense, fannKey, 1);
		// }

		return inscriptionaEC;
	}
}
