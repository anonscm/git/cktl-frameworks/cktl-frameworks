/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktlformco.serveur.finder.FinderEOScolMaquetteTypeEC;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe abstraite de manipulation d'une EOScolMaquetteRepartitionEC pour la
 * formation continue
 * 
 * @author jb
 * 
 */
public abstract class EOScolMaquetteRepartEcService {

	/**
	 * Creer une RepartEC pour la formation continue. EC et UE sont geres ds le
	 * service. Les autres attributs sont geres ici.
	 * 
	 * @param ec
	 * @param annee
	 * @return
	 */
	public static EOScolMaquetteRepartitionEc creerNouvelleScolMaquetteRepartECForFC(
			EOEditingContext ec, EOScolFormationAnnee annee) {
		if (ec == null)
			return null;
		if (annee == null)
			annee = FinderScolFormationAnnee.getScolFormationAnneeCourante(ec);
		EOScolMaquetteRepartitionEc repartEC = EOScolMaquetteRepartitionEc
				.create(ec);
		EOScolMaquetteTypeEc typeEC = FinderEOScolMaquetteTypeEC
				.getTypeECFacultatif(ec);
		// bonifiable
		repartEC.setMrecBonifiable(new Integer(0));
		// coefficient
		repartEC.setMrecCoefficient(new BigDecimal(1));
		// comptabilisable
		repartEC.setMrecComptabilisable(new Integer(1));
		// niveau
		repartEC.setMrecNiveau("1");
		// noteBase
		repartEC.setMrecNoteBase(new BigDecimal(20));
		// noteElimination
		repartEC.setMrecNoteElimination(new BigDecimal(0));
		// noteObtention
		repartEC.setMrecNoteObtention(new BigDecimal(10));
		// ordre
		repartEC.setMrecOrdre(new Long(1));
		// Annee
		repartEC.setToFwkScolarite_ScolFormationAnneeRelationship(annee);
		// typeEC
		repartEC.setToFwkScolarite_ScolMaquetteTypeEcRelationship(typeEC);

		return repartEC;
	}

	/**
	 * Retourne les EOScolMaquetteRepartitionEC a partir de l'isncription d'un etudiant a un diplome
	 * @param ec
	 * @param insDipl
	 * @return
	 */
	public static NSArray<EOScolMaquetteRepartitionEc> getRepEcFromInsDipl(
			EOEditingContext ec, EOInscDipl insDipl, int annee) {
		if (ec == null)
			return null;
		if (insDipl == null)
			return null;
		NSArray<EOScolMaquetteRepartitionEc> res = new NSMutableArray<EOScolMaquetteRepartitionEc>();

		NSArray<Object> tbKeys = new NSMutableArray<Object>();

		String conditionStr = EOScolInscriptionEc.TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ETUDIANT_KEY+"."+EOScolInscriptionEtudiant.IDIPL_NUMERO_KEY
				+ " = %@ AND " + EOScolInscriptionEc.FANN_KEY_KEY +" = %@ ";
		tbKeys.add(insDipl.idiplNumero());
		tbKeys.add(annee);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolInscriptionEc.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<EOScolInscriptionEc> resNonTrie = ec
				.objectsWithFetchSpecification(fetchSpec);

		Enumeration<EOScolInscriptionEc> enumInsECs = resNonTrie
				.objectEnumerator();
		while (enumInsECs.hasMoreElements()) {
			EOScolInscriptionEc insEc = (EOScolInscriptionEc) enumInsECs
					.nextElement();
			if (insEc.toFwkScolarite_ScolMaquetteRepartitionEc() != null) {
				res.add(insEc.toFwkScolarite_ScolMaquetteRepartitionEc());
			}
		}
		if(res.count()<=0)return null;
		return res;
	}
}
