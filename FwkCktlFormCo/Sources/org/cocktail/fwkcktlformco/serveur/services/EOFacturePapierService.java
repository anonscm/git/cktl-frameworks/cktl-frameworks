/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.util.Enumeration;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEOTypeEtat;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.kava.server.factory.FactoryFacturePapier;
import org.cocktail.kava.server.metier.EOBoutiqueInscription;
import org.cocktail.kava.server.metier.EOEngageBudget;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFacturePapierAdrClient;
import org.cocktail.kava.server.metier.EOFacturePapierLigne;
import org.cocktail.kava.server.metier.EONumerotation;
import org.cocktail.kava.server.metier.EOOrgan;
import org.cocktail.kava.server.metier.EOPlanComptable;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationAdresseClient;
import org.cocktail.kava.server.metier.EOPrestationBascule;
import org.cocktail.kava.server.metier.EOPrestationBudgetClient;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.EOTypeNumerotation;
import org.cocktail.kava.server.metier.EOUtilisateur;

import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

public abstract class EOFacturePapierService {
	private static final Format FORMAT_NUM = new DecimalFormat("00000.00");
	public static final String LBR = "\n";
	public static final String HTML_LBR = "<br/>";

	/**
	 * Copie de JEFY_RECETTE.API_PRESTATION.GENERE_FACTURE_PAPIER ajout du
	 * booleen forFc pour indiquer que si la fonction est appelée pour la FC, on
	 * enleve le controle des quantite restante et on cree une facture a 0 pour
	 * un nouveau partenaire de FC. UtlOrdre est utilise pour ajouter une
	 * facture a un utilisateur autre que celui specifie ds la prestation.
	 * A la creation d'un contrat de FC, on ne peut pas relier la facture a la convention car la prestation n'est pas reliee a cette convention.
	 * Il faut que le contrat/Convention soit en BDD pour pouvoir le requeter et le caster en PieFwk.Covnention pour pouvoir relier la convention a la prestation et a la facture.
	 * 
	 * @param ec
	 * @param prestId
	 * @param utlOrdre
	 * @return
	 * @throws FormcoException
	 */
//	public static EOFacturePapier genererFacturePapier(EOEditingContext ec,
//			EOPrestation prestation, int utlOrdre, boolean forAddPartenaire)
	public static EOFacturePapier genererFacturePapier(EOEditingContext ec,
			EOPrestation prestation,  boolean forAddPartenaire)
			throws FormcoException {
		if (ec == null || prestation == null
//				|| prestation.convention()==null
				)
			throw new FormcoException(
					"Ec ou Prestation ou Convention nul ou utilisateur absent.");

		prestation = prestation.localInstanceIn(ec);
		EOFacturePapier fap = EOFacturePapier.creerInstance(ec);
		int prestId = 0;

		if (ERXEOControlUtilities.primaryKeyStringForObject(prestation) == null)
			prestId = 0;

		fap.setPrestation(prestation);

//		 EOUtilisateur utilisateur = (EOUtilisateur) EOUtilities
//		 .objectMatchingKeyAndValue(ec, EOUtilisateur.ENTITY_NAME,
//		 EOUtilisateur.UTL_ORDRE_KEY, utlOrdre);
		 EOUtilisateur utilisateur =prestation.utilisateur();
		if (utilisateur == null)
			throw new FormcoException("Utilisateur introuvable");
		fap.setUtilisateur(utilisateur);

		fap.setExercice(prestation.exercice());

		if (prestId != 0)
			controlePrestationBascule(ec, prestId);

		if (prestation.prestDateCloture() == null)
			throw new FormcoException("La prestation n'est pas cloturée");

		EOTypeEtat typeEtat = prestation.typeEtat();
		int idTypeEtatPrest = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(typeEtat));

		org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtatValide = FinderEOTypeEtat
				.typeEtatValide(ec);
		int idTypeEtatValide = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(typeEtatValide));
		if (idTypeEtatPrest != idTypeEtatValide)
			throw new FormcoException("La prestation n'est pas valide");

		if (prestation.typePublic() == null)
			throw new FormcoException("La prestation n'a pas de type client");

		if (prestation.prestationBudgetClient() == null)
			throw new FormcoException(
					"La prestation n'a pas d'information budgétaire client");
//		EOAdresse adresseClient = getAdressePrestation(ec, prestId);

		if (prestation.planComptable() != null) {
			// -- on est dans le cas où on force l'imputation recette ==> une
			// seule facture papier, facile !
			int pcoNum = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prestation.planComptable()));

			// IL SEMBLE QUE CE SOIT PCONUM au lieu DE PCONUMTVA pour la
			// FORMATION CONTINUE

			// int pcoNumTVA = getPcoNumTVA(ec, pcoNum);
			// EOPlanComptable plancoTVA = getPlanComptableFromPcoNum(ec,
			// pcoNumTVA, prestation.exercice());
			//
			// fap.setPlanComptableTva(plancoTVA);
			System.out
					.println("On integre le PLAN COMPTABLE dans la FACTURE PAPIER");
			EOPlanComptable pc = getPlanComptableFromPcoNum(ec, pcoNum,
					prestation.exercice());
			fap.setPlanComptable(pc);

			int morOrdre = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prestation.modeRecouvrement()));
			int pcoNumCTP = getPcoNumCTP(ec, pcoNum, morOrdre);
			if (pcoNumCTP == 0)
				fap.setPlanComptableCtp(null);
			EOPlanComptable pcoCTP = getPlanComptableFromPcoNum(ec, pcoNumCTP,
					prestation.exercice());

			fap.setPlanComptableCtp(pcoCTP);

			int fapId = fap.creerFapId(ec);
			fap.setFapId(fapId);
			fap.setExercice(prestation.exercice());
			fap.setFapNumero(getNumerotation(ec, "FACTURE_PAPIER", prestation
					.exercice().exeOrdre(), null));
			fap.setFacture(null);
			fap.setUtilisateur(utilisateur);
			fap.setPrestation(prestation);
			fap.setFournisUlrPrest(prestation.fournisUlrPrest());
			fap.setPersonne(prestation.personne());
			fap.setFournisUlr(prestation.fournisUlr());
			fap.setIndividuUlr(prestation.individuUlr());
			fap.setFapDate(new NSTimestamp());

			String fapRef = getFapRef(ec, prestation.exercice(),
					prestation.organ(), fapId, null);
			fap.setFapRef(fapRef);

			fap.setFapLib(prestation.prestLibelle());
			fap.setFapDateValidationClient(null);
			fap.setFapDateValidationPrest(null);
			fap.setFapDateLimitePaiement(null);
			fap.setFapDateReglement(null);
			fap.setFapReferenceReglement(null);
			fap.setFapCommentairePrest(prestation.prestCommentairePrest());
			fap.setFapCommentaireClient(prestation.prestCommentaireClient());
			fap.setFapRemiseGlobale(prestation.prestRemiseGlobale());
			fap.setFapApplyTva(prestation.prestApplyTva());
			fap.setModeRecouvrement(prestation.modeRecouvrement());
			fap.setRibfourUlr(null);
			fap.setOrgan(prestation.organ());
			fap.setTauxProrata(prestation.tauxProrata());
			fap.setTypeCreditRec(prestation.typeCreditRec());
			fap.setLolfNomenclatureRecette(prestation.lolfNomenclatureRecette());
			fap.setPlanComptable(prestation.planComptable());
			fap.setCodeAnalytique(prestation.codeAnalytique());
//			 fap.setConvention(prestation.convention());
//			fap.setConventionRelationship(prestation.convention());
//				System.out.println("ATTENTION EOFacturePapierService.genererFacturePapier Fap.Setconvention verifier que FacturePapier.conOrdre soit initialisé.");

//			fap.setToCWContrat(prestation.toCWContrat());
			fap.setEcheId(null);
			fap.setTypePublic(prestation.typePublic());
			fap.setTypeEtat(prestation.typeEtat());
			fap.setUtilisateurValidationClient(null);
			fap.setUtilisateurValidationPrest(null);
			fap.setFapTotalHt(prestation.prestTotalHt());
			fap.setFapTotalTtc(prestation.prestTotalTtc());
			fap.setFapTotalTva(prestation.prestTotalTva());

			// Inutilisé dans le cadre d'une prestation externe
			fap.setEngageBudget(getEOEngageBudgetFromPrestation(ec, prestId));

			// Creation des prestations lignes
			NSArray<EOPrestationLigne> prestlignes = null;
			prestlignes = getPrestationLigneFromPrestId(ec, prestation,
					forAddPartenaire);
			if (prestlignes == null || prestlignes.isEmpty()) {
				throw new FormcoException(
						"Tout a été facturé. Rien a facturer sur la prestation (prestId:"
								+ prestId + ")");
			}
			Enumeration<EOPrestationLigne> enumpl = prestlignes
					.objectEnumerator();

			while (enumpl.hasMoreElements()) {
				EOPrestationLigne pl = (EOPrestationLigne) enumpl.nextElement();
				if (pl.prestation() != null) {
					EOFacturePapierLigne fpl = EOFacturePapierLigne
							.creerInstance(ec);
					int fplId = fpl.creerFapLigneId(ec);
					fpl.setFacturePapier(fap);
					fap.addToFacturePapierLignesRelationship(fpl);

					if (pl.prestationLignePere() != null) {
						fpl.setFacturePapierLignePere(getFacturePapierLignePere(
								ec, pl, fap));
					} else
						pl.setPrestationLignePere(null);

					fpl.setFligId(fplId);
					fpl.setPrestationLigne(pl);
					fpl.setFligDate(pl.prligDate());
					fpl.setFligReference(pl.prligReference());
					fpl.setFligDescription(pl.prligDescription());
					fpl.setFligArtHt(pl.prligArtHt());
					fpl.setFligArtTtc(pl.prligArtTtc());
					fpl.setFligArtTtcInitial(pl.prligArtTtcInitial());
					fpl.setFligQuantite(pl.prligQuantiteReste());
					fpl.setFligTotalHt(pl.prligTotalResteHt());
					fpl.setFligTotalTtc(pl.prligTotalResteTtc());
					fpl.setTva(pl.tva());
					fpl.setTvaInitial(pl.tvaInitial());
					fpl.setTypeArticle(pl.typeArticle());
					pl.setPrligQuantiteReste(new BigDecimal(BigInteger.ZERO));
				}
			}
//			// ajouter l'adresseClient
//			if (adresseClient != null) {
//				EOFacturePapierAdrClient fapad = EOFacturePapierAdrClient
//						.creerInstance(ec);
//				fapad.setToAdresse(adresseClient);
//				fapad.setToFacturePapier(fap);
//				fapad.setDateCreation(new NSTimestamp());
//				 fapad.setPersIdCreation(utilisateur.persId());
//				prestation.setPrestDateFacturation(new NSTimestamp());
//			}

		} else {
			// -- on est dans le cas où on regroupe par compte d'imputation
			// recette article, autant de factures papier que de comptes
			// différents, moins facile !
			// -- la liste des comptes imputation recette différents
			try {
				NSArray<NSDictionary<String, Object>> results = new NSArray<NSDictionary<String, Object>>();
				results = getPlanCoFromPrestation(ec, prestation,
						forAddPartenaire);
				if (!results.isEmpty()) {
					for (int i = 0; i < results.count(); i++) {
						int pcoNum = ((Number) FORMAT_NUM.parseObject(results
								.objectAtIndex(i).valueForKey("PCO_NUM")
								.toString())).intValue();
						BigDecimal totalResteHT = new BigDecimal(
								BigInteger.ZERO);
						BigDecimal totalResteTTC = new BigDecimal(
								BigInteger.ZERO);

						if (pcoNum != 0) {
							// System.out.println("pcoNum:" + pcoNum);
							// int pcoNumTVA = getPcoNumTVA(ec, pcoNum);
							// EOPlanComptable plancoTVA =
							// getPlanComptableFromPcoNum(
							// ec, pcoNumTVA, prestation.exercice());
							//
							// fap.setPlanComptableTva(plancoTVA);
							System.out
									.println("On integre le PLAN COMPTABLE dans la FACTURE PAPIER");
							EOPlanComptable pc = getPlanComptableFromPcoNum(ec,
									pcoNum, prestation.exercice());
							fap.setPlanComptable(pc);

							int morOrdre = Integer
									.parseInt(ERXEOControlUtilities
											.primaryKeyStringForObject(prestation
													.modeRecouvrement()));

							int pcoNumCTP = getPcoNumCTP(ec, pcoNum, morOrdre);
							if (pcoNumCTP == 0)
								fap.setPlanComptableCtp(null);

							EOPlanComptable pcoCTP = getPlanComptableFromPcoNum(
									ec, pcoNumCTP, prestation.exercice());
							fap.setPlanComptableCtp(pcoCTP);

							NSArray<NSDictionary<String, Object>> resultsTotaux = getTotaux(
									ec, prestation, pcoNum);
							if (resultsTotaux != null) {
								totalResteHT = new BigDecimal(resultsTotaux
										.objectAtIndex(0)
										.valueForKey("MY_FAP_TOTAL_HT")
										.toString());
								totalResteTTC = new BigDecimal(resultsTotaux
										.objectAtIndex(0)
										.valueForKey("MY_FAP_TOTAL_TTC")
										.toString());
							}
						} else {
							fap.setPlanComptableCtp(null);
							fap.setPlanComptableTva(null);

							NSArray<NSDictionary<String, Object>> resultsTotaux = getTotauxPCONull(
									ec, prestation);
							if (resultsTotaux != null) {
								totalResteHT = new BigDecimal(resultsTotaux
										.objectAtIndex(0)
										.valueForKey("MY_FAP_TOTAL_HT")
										.toString());
								totalResteTTC = new BigDecimal(resultsTotaux
										.objectAtIndex(0)
										.valueForKey("MY_FAP_TOTAL_TTC")
										.toString());
							}
						}
						int fapId = fap.creerFapId(ec);
						fap.setFapId(fapId);

						fap.setExercice(prestation.exercice());
						fap.setFapNumero(getNumerotation(ec, "FACTURE_PAPIER",
								prestation.exercice().exeOrdre(), null));
						fap.setFacture(null);
						 fap.setUtilisateur(utilisateur);
						fap.setPrestation(prestation);
						fap.setFournisUlrPrest(prestation.fournisUlrPrest());
						fap.setPersonne(prestation.personne());
						fap.setFournisUlr(prestation.fournisUlr());
						fap.setIndividuUlr(prestation.individuUlr());
						fap.setFapDate(new NSTimestamp());

						String fapRef = getFapRef(ec, prestation.exercice(),
								prestation.organ(), fapId, null);
						fap.setFapRef(fapRef);
						fap.setFapLib(prestation.prestLibelle());
						fap.setFapDateValidationClient(null);
						fap.setFapDateValidationPrest(null);
						fap.setFapDateLimitePaiement(null);
						fap.setFapDateReglement(null);
						fap.setFapReferenceReglement(null);
						fap.setFapCommentairePrest(prestation
								.prestCommentairePrest());
						fap.setFapCommentaireClient(prestation
								.prestCommentaireClient());
						fap.setFapRemiseGlobale(prestation.prestRemiseGlobale());
						fap.setFapApplyTva(prestation.prestApplyTva());
						fap.setModeRecouvrement(prestation.modeRecouvrement());
						fap.setRibfourUlr(null);
						fap.setOrgan(prestation.organ());
						fap.setTauxProrata(prestation.tauxProrata());
						fap.setTypeCreditRec(prestation.typeCreditRec());
						fap.setLolfNomenclatureRecette(prestation
								.lolfNomenclatureRecette());
						fap.setPlanComptable(prestation.planComptable());
						fap.setCodeAnalytique(prestation.codeAnalytique());
//						System.out.println("ATTENTION EOFacturePapierService.genererFacturePapier Fap.Setconvention verifier que FacturePapier.conOrdre soit initialisé.");
//						 fap.setConvention(prestation.convention());
//						fap.setConventionRelationship(prestation.convention());

//						fap.setToCWContrat(prestation.toCWContrat());
						fap.setEcheId(null);
						fap.setTypePublic(prestation.typePublic());
						fap.setTypeEtat(prestation.typeEtat());
						fap.setUtilisateurValidationClient(null);
						fap.setUtilisateurValidationPrest(null);
						fap.setFapTotalHt(totalResteHT);
						fap.setFapTotalTtc(totalResteTTC);
						fap.setFapTotalTva(totalResteTTC.subtract(totalResteHT));
						fap.setEngageBudget(getEOEngageBudgetFromPrestation(ec,
								prestId));

						if (pcoNum != 0) {
							// Creation des prestations lignes
							NSArray<EOPrestationLigne> prestlignes = getPrestationLigneFromPrestIdAndPCO(
									ec, prestation, pcoNum, forAddPartenaire);
							if (prestlignes == null) {
								throw new FormcoException(
										"Tout a été facturé. Rien a facturer sur la prestation "
												+ prestId);
							}
							if (prestlignes.isEmpty()) {
								throw new FormcoException(
										"Tout a été facturé. Rien a facturer sur la prestation "
												+ prestId);
							}
							Enumeration<EOPrestationLigne> enumpl = prestlignes
									.objectEnumerator();

							while (enumpl.hasMoreElements()) {
								EOPrestationLigne pl = (EOPrestationLigne) enumpl
										.nextElement();
								if (pl.prestation() != null) {
									EOFacturePapierLigne fpl = EOFacturePapierLigne
											.creerInstance(ec);
									int fplId = fpl.creerFapLigneId(ec);
									fpl.setFacturePapier(fap);
									fap.addToFacturePapierLignesRelationship(fpl);

									if (pl.prestationLignePere() != null) {
										fpl.setFacturePapierLignePere(getFacturePapierLignePere(
												ec, pl, fap));
									} else
										fpl.setFacturePapierLignePere(null);

									fpl.setFligId(fplId);
									fpl.setPrestationLigne(pl);
									fpl.setFligDate(pl.prligDate());
									fpl.setFligReference(pl.prligReference());
									fpl.setFligDescription(pl
											.prligDescription());
									fpl.setFligArtHt(pl.prligArtHt());
									fpl.setFligArtTtc(pl.prligArtTtc());
									fpl.setFligArtTtcInitial(pl
											.prligArtTtcInitial());
									fpl.setFligQuantite(pl.prligQuantiteReste());
									fpl.setFligTotalHt(pl.prligTotalResteHt());
									fpl.setFligTotalTtc(pl.prligTotalResteTtc());
									fpl.setTva(pl.tva());
									fpl.setTvaInitial(pl.tvaInitial());
									fpl.setTypeArticle(pl.typeArticle());
									pl.setPrligQuantiteReste(new BigDecimal(
											BigInteger.ZERO));
									pl.setPrligTotalResteHt(new BigDecimal(
											BigInteger.ZERO));
									pl.setPrligTotalResteTtc(new BigDecimal(
											BigInteger.ZERO));
								}
							}
						} else {

							// Creation des prestations lignes
							NSArray<EOPrestationLigne> prestlignes = null;
							prestlignes = getPrestationLigneFromPrestIdAndPCONull(
									ec, prestation, forAddPartenaire);
							if (prestlignes == null) {
								throw new FormcoException(
										"Tout a été facturé. Rien a facturer sur la prestation "
												+ prestId);
							}
							if (prestlignes.isEmpty()) {
								throw new FormcoException(
										"Tout a été facturé. Rien a facturer sur la prestation "
												+ prestId);
							}

							Enumeration<EOPrestationLigne> enumpl = prestlignes
									.objectEnumerator();

							while (enumpl.hasMoreElements()) {
								EOPrestationLigne pl = (EOPrestationLigne) enumpl
										.nextElement();
								if (pl.prestation() != null) {
									EOFacturePapierLigne fpl = EOFacturePapierLigne
											.creerInstance(ec);
									int fplId = fpl.creerFapLigneId(ec);
									fpl.setFacturePapier(fap);
									fap.addToFacturePapierLignesRelationship(fpl);

									if (pl.prestationLignePere() != null) {
										fpl.setFacturePapierLignePere(getFacturePapierLignePere(
												ec, pl, fap));
									} else
										pl.setPrestationLignePere(null);

									fpl.setFligId(fplId);
									fpl.setPrestationLigne(pl);
									fpl.setFligDate(pl.prligDate());
									fpl.setFligReference(pl.prligReference());
									fpl.setFligDescription(pl
											.prligDescription());
									fpl.setFligArtHt(pl.prligArtHt());
									fpl.setFligArtTtc(pl.prligArtTtc());
									fpl.setFligArtTtcInitial(pl
											.prligArtTtcInitial());
									fpl.setFligQuantite(pl.prligQuantiteReste());
									fpl.setFligTotalHt(pl.prligTotalResteHt());
									fpl.setFligTotalTtc(pl.prligTotalResteTtc());
									fpl.setTva(pl.tva());
									fpl.setTvaInitial(pl.tvaInitial());
									fpl.setTypeArticle(pl.typeArticle());
									pl.setPrligQuantiteReste(new BigDecimal(
											BigInteger.ZERO));
									pl.setPrligTotalResteHt(new BigDecimal(
											BigInteger.ZERO));
									pl.setPrligTotalResteTtc(new BigDecimal(
											BigInteger.ZERO));
								}
							}
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
				throw new FormcoException(e.getMessage());
			}
		}

		return fap;
	}

	/**
	 * -- annulation etape no 4 -- suppression d'une facture papier non recettee
	 * -- si issue d'une prestation, met a jour les quantites restantes a
	 * facturer
	 * 
	 * @param fapId
	 * @param utlOrdre
	 *            ne sert a rien
	 */
	public static void delFacturePapier(EOEditingContext ec, int fapId,
			int utlOrdre) throws FormcoException {
		if (ec == null || fapId <= 0)
			throw new FormcoException(
					"Suppression facture impossible : Ec ou fapId ou utlOrdre null");
		EOFacturePapier fap = (EOFacturePapier) EOUtilities
				.objectMatchingKeyAndValue(
						ec,
						EOFacturePapier.ENTITY_NAME,
						EOFacturePapier.FAP_ID_KEY, fapId);
		if (fap == null)
			throw new FormcoException("Facture introuvable. FapId:" + fapId);
		if (fap.facture() != null) {
			throw new FormcoException(
					"Facture recettee, impossible de la supprimer");
		}
		NSArray<EOFacturePapierLigne> fapLignes = fap.facturePapierLignes();
		Enumeration<EOFacturePapierLigne> enumfapl = fapLignes
				.objectEnumerator();

		if (fap.prestation() != null) {
			EOPrestation prest = fap.prestation();
			int prestId = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prest));
			while (enumfapl.hasMoreElements()) {
				EOFacturePapierLigne fapl = (EOFacturePapierLigne) enumfapl
						.nextElement();
				if (fapl.prestationLigne() != null) {
					EOPrestationLigne prestl = fapl.prestationLigne();
					prestl.setPrligQuantiteReste(fapl.fligQuantite());
					prestl.setPrligTotalResteHt(prestl.prligTotalResteHt().add(
							fapl.fligTotalHt()));
					prestl.setPrligTotalResteTtc(prestl.prligTotalResteTtc()
							.add(fapl.fligTotalTtc()));
				}
			}
			// -- si y'a plus de facture papier, nullifier la date de
			// facturation, sinon la mettre ! la première date de facture
			int count = getCountFapFromPrest(ec, prestId, fapId);
			if (count == 0) {
				prest.setPrestDateFacturation(null);
			} else {
				prest.setPrestDateFacturation(minFapDate(ec, prestId, fapId));
			}
			enumfapl = fapLignes.objectEnumerator();

			while (enumfapl.hasMoreElements()) {
				EOFacturePapierLigne fapl = (EOFacturePapierLigne) enumfapl
						.nextElement();
				ec.deleteObject(fapl);
			}
			NSArray<EOFacturePapierAdrClient> fapAdrs = EOUtilities
					.objectsMatchingKeyAndValue(ec,
							EOFacturePapierAdrClient.ENTITY_NAME,
							EOFacturePapierAdrClient.FAP_ID_KEY, fapId);
			Enumeration<EOFacturePapierAdrClient> enumFapAdrs = fapAdrs
					.objectEnumerator();
			while (enumFapAdrs.hasMoreElements()) {
				EOFacturePapierAdrClient fapAdr = enumFapAdrs.nextElement();
				ec.deleteObject(fapAdr);
			}

			// -- verif si c etait la derniere facture papier generee, auquel
			// cas un decremente le numero dans la numerotation
			NSArray<EOTypeNumerotation> typesNum = EOUtilities
					.objectsMatchingKeyAndValue(ec,
							EOTypeNumerotation.ENTITY_NAME,
							EOTypeNumerotation.TNU_ENTITE_KEY, "FACTURE_PAPIER");
			if (typesNum != null) {
				if (typesNum.count() == 1) {
					EOTypeNumerotation typeNum = typesNum.get(0);
					NSArray<NSDictionary<String, Object>> resNums = getNumsFromExe(
							ec, fap.exercice().exeOrdre(), typeNum.tnuId());
					if (resNums != null) {
						int num_numero;
						int num_id;
						try {
							num_numero = ((Number) FORMAT_NUM
									.parseObject(resNums.objectAtIndex(0)
											.valueForKey("MY_NUM_NUMERO")
											.toString())).intValue();
							num_id = ((Number) FORMAT_NUM.parseObject(resNums
									.objectAtIndex(0).valueForKey("MY_NUM_ID")
									.toString())).intValue();
						} catch (ParseException e) {
							e.printStackTrace();
							throw new FormcoException(e.getMessage());
						}
						if (num_numero == fapId) {

							EONumerotation numerotation = (EONumerotation) EOUtilities
									.objectMatchingKeyAndValue(ec,
											EONumerotation.ENTITY_NAME,
											EONumerotation.NUM_ID_KEY, num_id);
							num_numero--;
							numerotation.setNumNumero(num_numero);
						}
					}
				}
			}

			// try {
			// ec.saveChanges();
			// } catch (Exception e) {
			// ec.revert();
			// e.printStackTrace();
			// throw new FormcoException(e.getMessage());
			// }
			ec.deleteObject(fap);
		}

	}

	/**
	 * verification que la prestation n'est pas ds l'exercice suivant
	 * 
	 * @param ec
	 * @param prestId
	 * @throws FormcoException
	 */
	public static void controlePrestationBascule(EOEditingContext ec,
			int prestId) throws FormcoException {
		if (ec == null || prestId <= 0)
			throw new FormcoException("Ec ou prestation null");
		NSArray<EOPrestationBascule> pb = EOUtilities
				.objectsMatchingKeyAndValue(ec,
						EOPrestationBascule.ENTITY_NAME,
						EOPrestationBascule.PREST_ID_ORIGINE_KEY, prestId);
		if (pb.count() != 0)
			throw new FormcoException(
					"Impossible, la prestation existe sur l'exercice N+1");
	}

	/**
	 * recupere le plan comptable tva en fonction du pco_num, dans planco_visa
	 * 
	 * @param ec
	 * @param pcoNum
	 * @return
	 */
	public static int getPcoNumTVA(EOEditingContext ec, int pcoNum)
			throws FormcoException {
		int pcoNumTVA = 0;
		NSArray<NSDictionary<String, Object>> results = NSArray.emptyArray();
		String sql = " SELECT pco_num_tva AS MY_PCO_NUM_TVA FROM JEFY_RECETTE.v_planco_visa "
				+ "WHERE pco_num_ordonnateur = '"
				+ pcoNum
				+ "' AND ROWNUM=1";
		EOModel model = EOUtilities.entityForClass(ec, EOPrestationLigne.class)
				.model();
		NSArray<NSDictionary<String, Object>> res = EOUtilities.rawRowsForSQL(
				ec, model.name(), sql, null);
		if (res != null) {
			try {
				pcoNumTVA = ((Number) FORMAT_NUM.parseObject(res
						.objectAtIndex(0).valueForKey("MY_PCO_NUM_TVA")
						.toString())).intValue();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return pcoNumTVA;
	}

	//
	// if (ec == null || pcoNum <= 0)
	// throw new FormcoException("ec ou pcoNum null");
	// String valide = "VALIDE";
	// NSMutableArray quals = new NSMutableArray();
	// NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
	// String conditionStr = EOPlancoVisa.PCO_NUM_ORDONNATEUR_KEY +
	// " = %@  AND "
	// tbKeys.add(""+pcoNum);
	// tbKeys.add(valide);
	// EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
	// conditionStr, tbKeys);
	// quals.addObject(qual);
	//
	// EOFetchSpecification fs = new EOFetchSpecification(
	// EOPlancoVisa.ENTITY_NAME, new EOAndQualifier(quals), null);
	// fs.setUsesDistinct(true);
	// try {
	// NSArray res = (NSArray) ec.objectsWithFetchSpecification(fs);
	// if (res == null)
	// return null;
	// else
	// return (EOPlancoVisa) res.get(0);
	// } catch (Exception e) {
	// e.printStackTrace();
	// return null;
	// }
	// }

	/**
	 * -- recupere le plan comptable tva en fonction du pco_num et du mor_ordre,
	 * dans mode de recouvrement en priorite, sinon dans planco_visa
	 * 
	 * @param ec
	 * @param pcoNum
	 * @param morOrdre
	 * @return
	 * @throws FormcoException
	 */
	public static int getPcoNumCTP(EOEditingContext ec, int pcoNum, int morOrdre)
			throws FormcoException {
		if (ec == null || pcoNum <= 0 || morOrdre <= 0)
			throw new FormcoException("ec,pcoNum morOrdre null");
		int pcoNumCTP = 0;
		NSArray<NSDictionary<String, Object>> results = NSArray.emptyArray();
		String sql = "SELECT pco_num_visa AS my_pco_num_ctp FROM JEFY_RECETTE.v_mode_recouvrement "
				+ "WHERE mod_ordre = '" + pcoNum + "' ";
		EOModel model = EOUtilities.entityForClass(ec, EOPrestationLigne.class)
				.model();
		NSArray<NSDictionary<String, Object>> res = EOUtilities.rawRowsForSQL(
				ec, model.name(), sql, null);
		if (res != null && res.count() >= 1) {
			try {
				pcoNumCTP = ((Number) FORMAT_NUM.parseObject(res
						.objectAtIndex(0).valueForKey("MY_PCO_NUM_CTP")
						.toString())).intValue();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			String sql2 = "	SELECT pco_num_ctrepartie AS my_pco_num_ctp FROM JEFY_RECETTE.v_planco_visa "
					+ "WHERE pco_num_ordonnateur = '"
					+ pcoNum
					+ "' AND ROWNUM=1";
			EOModel model2 = EOUtilities.entityForClass(ec,
					EOPrestationLigne.class).model();
			NSArray<NSDictionary<String, Object>> res2 = EOUtilities
					.rawRowsForSQL(ec, model2.name(), sql2, null);
			if (res2 != null && res2.count() >= 1) {
				try {
					pcoNumCTP = ((Number) FORMAT_NUM.parseObject(res2
							.objectAtIndex(0).valueForKey("MY_PCO_NUM_CTP")
							.toString())).intValue();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		return pcoNumCTP;
	}

	/**
	 * constitution d'une reference de facture papier format :
	 * exercice-orgub-orgcr-xxx (xxx = numero incremental pour le triplet
	 * precedent, sauf si org_niv=2, pas de orgcr)
	 * 
	 * @param ec
	 * @param exeOrdre
	 * @param orgId
	 * @param fapId
	 * @param oldFapRef
	 * @return
	 */
	public static String getFapRef(EOEditingContext ec, EOExercice exercice,
			EOOrgan organ, int fapId, String oldFapRef) {
		if (ec == null || exercice == null || organ == null || fapId <= 0)
			return null;
		String res = "";
		res += exercice.exeExercice() + "-" + organ.orgUb();

		if (organ.orgNiv() > 2 && organ.orgCr() != null)
			res += "-" + organ.orgCr() + "-";

		if (organ.orgNiv() > 2) {
			if (oldFapRef != null
					&& oldFapRef.toLowerCase().contains(res.toLowerCase())) {
				res = oldFapRef;
			} else {
				int max = getMax1(ec, res);
				if (max == 0)
					res += "1";
				else {
					max++;
					res += +max;
				}
			}
		} else {
			if (oldFapRef != null) {
				String[] nbTiret = oldFapRef.split("-");
				if (nbTiret.length < 3
						&& oldFapRef.toLowerCase().contains(res.toLowerCase())) {
					res = oldFapRef;
				} else {
					int max = getMax2(ec, res);
					if (max == 0)
						res += "1";
					else {
						max++;
						res += max;
					}

				}
			}
		}
		return res;
	}

	public static int getMax1(EOEditingContext ec, String ref) {
		// Retourner le numero
		int max = 0;
		String sqlSelect = "SELECT MAX(TO_NUMBER(SUBSTR(fap_ref, LENGTH('"
				+ ref
				+ "')+1))) as MY_NB FROM JEFY_RECETTE.FACTURE_PAPIER WHERE fap_ref LIKE '"
				+ ref + "%'";
		EOModel model = EOUtilities.entityForClass(ec, EOFacturePapier.class)
				.model();
		NSArray<NSDictionary<String, ?>> res = EOUtilities.rawRowsForSQL(ec,
				model.name(), sqlSelect, null);
		if (res != null && !res.isEmpty()) {
			try {
				max = ((Number) FORMAT_NUM.parseObject(res.objectAtIndex(0)
						.valueForKey("MY_NB").toString())).intValue();
			} catch (ParseException e) {
				System.out.println("Premiere facture. Renvoie 0");
				return 0;
			}
		}

		return max;
	}

	public static int getMax2(EOEditingContext ec, String ref) {
		// Retourner le numero
		int max = 0;
		String sqlSelect = "SELECT MAX(TO_NUMBER(SUBSTR(fap_ref, LENGTH("
				+ ref
				+ ") + 1)))"
				+ " as MY_NB FROM JEFY_RECETTE.FACTURE_PAPIER fp, JEFY_RECETTE.v_organ o WHERE fp.fap_ref LIKE '"
				+ ref
				+ "%' AND fp.org_id = o.org_id AND fp.fap_id <> a_fap_id AND o.org_niv = 2";
		EOModel model = EOUtilities.entityForClass(ec, EOFacturePapier.class)
				.model();
		NSArray<NSDictionary<String, ?>> res = EOUtilities.rawRowsForSQL(ec,
				model.name(), sqlSelect, null);
		if (res != null) {
			try {
				max = ((Number) FORMAT_NUM.parseObject(res.objectAtIndex(0)
						.valueForKey("MY_NB").toString())).intValue();
			} catch (ParseException e) {
				e.printStackTrace();
				return 0;
			}
		}

		return max;
	}

	public static EOEngageBudget getEOEngageBudgetFromPrestation(
			EOEditingContext ec, int prestId) {
		if (ec == null || prestId == 0)
			return null;
		EOPrestationBudgetClient pbud = (EOPrestationBudgetClient) EOUtilities
				.objectMatchingKeyAndValue(ec,
						EOPrestationBudgetClient.ENTITY_NAME,
						EOPrestationBudgetClient.PREST_ID_KEY, prestId);
		if (pbud != null)
			return pbud.engageBudget();
		else
			return null;
	}

	public static NSArray<EOPrestationLigne> getPrestationLigneFromPrestId(
			EOEditingContext ec, EOPrestation prestation,
			boolean forAddPartenaire) {

		if (ec == null || prestation == null)
			return null;
		int prestId = 0;
		if (ERXEOControlUtilities.primaryKeyStringForObject(prestation) != null) {
			prestId = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prestation));
		}
		if (prestId == 0) {
			NSArray<EOPrestationLigne> res = new NSMutableArray<EOPrestationLigne>();
			NSArray<EOPrestationLigne> pls = prestation.prestationLignes();
			if (pls == null)
				return null;
			if (pls.isEmpty())
				return null;
			for (int i = 0; i < pls.count(); i++) {
				EOPrestationLigne pl = pls.get(i);
				if (!forAddPartenaire) {
					if (pl.prligQuantiteReste().compareTo(
							new BigDecimal(BigInteger.ZERO)) > 0) {
						res.add(pl);
					}
				} else {
					res.add(pl);
				}
			}
			return res;
		}
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOPrestationLigne.PRESTATION_KEY + " = %@  ";
		if (!forAddPartenaire)
			conditionStr += " AND "
					+ EOPrestationLigne.PRLIG_QUANTITE_RESTE_KEY + " > 0";
		tbKeys.add(prestation);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOPrestationLigne.ENTITY_NAME, qual, null);

		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	public static NSArray<EOPrestationLigne> getPrestationLigneFromPrestIdAndPCO(
			EOEditingContext ec, EOPrestation prestation, int pcoNum,
			boolean forAddPartenaire) {
		if (ec == null || prestation == null)
			return null;
		// Si la prestation n'est pas encore stockée en Base
		int prestId = 0;
		if (ERXEOControlUtilities.primaryKeyStringForObject(prestation) != null) {
			prestId = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prestation));
		}
		if (prestId == 0) {
			NSArray<EOPrestationLigne> res = new NSMutableArray<EOPrestationLigne>();
			NSArray<EOPrestationLigne> pls = prestation.prestationLignes();
			if (pls == null)
				return null;
			if (pls.isEmpty())
				return null;
			for (int i = 0; i < pls.count(); i++) {
				EOPrestationLigne pl = pls.get(i);
				if (pl.planComptable().pcoNum().equalsIgnoreCase("" + pcoNum)) {
					if (!forAddPartenaire) {
						if (pl.prligQuantiteReste().compareTo(
								new BigDecimal(BigInteger.ZERO)) > 0) {
							res.add(pl);
						}
					} else {
						res.add(pl);
					}
				}
			}
			return res;
		}

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOPrestationLigne.PRESTATION_KEY + " = %@ and "
				+ EOPrestationLigne.PCO_NUM_KEY + " = %@ ";
		if (!forAddPartenaire)
			conditionStr += " AND "
					+ EOPrestationLigne.PRLIG_QUANTITE_RESTE_KEY + " > 0";

		tbKeys.add(prestation);
		tbKeys.add("" + pcoNum);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOPrestationLigne.ENTITY_NAME, qual, null);

		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	public static NSArray<EOPrestationLigne> getPrestationLigneFromPrestIdAndPCONull(
			EOEditingContext ec, EOPrestation prestation,
			boolean forAddPartenaire) {
		if (ec == null || prestation == null)
			return null;
		// Si la prestation n'est pas encore stockée en Base
		// Si la prestation n'est pas encore stockée en Base
		int prestId = 0;
		if (ERXEOControlUtilities.primaryKeyStringForObject(prestation) != null) {
			prestId = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prestation));
		}
		if (prestId == 0) {
			NSArray<EOPrestationLigne> res = new NSArray<EOPrestationLigne>();
			NSArray<EOPrestationLigne> pls = prestation.prestationLignes();
			if (pls == null)
				return null;
			if (pls.isEmpty())
				return null;
			for (int i = 0; i < pls.count(); i++) {
				EOPrestationLigne pl = pls.get(i);
				if (pl.planComptable().pcoNum() == null) {
					if (!forAddPartenaire) {
						if (pl.prligQuantiteReste().compareTo(
								new BigDecimal(BigInteger.ZERO)) > 0) {
							res.add(pl);
						}
					} else {
						res.add(pl);
					}
				}
			}
			return res;
		}

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOPrestationLigne.PRESTATION_KEY + " = %@ and "
				+ EOPrestationLigne.PCO_NUM_KEY + " = NULL ";
		if (!forAddPartenaire)
			conditionStr += " AND "
					+ EOPrestationLigne.PRLIG_QUANTITE_RESTE_KEY + " > 0";

		tbKeys.add(prestation);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOPrestationLigne.ENTITY_NAME, qual, null);

		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	public static EOFacturePapierLigne getFacturePapierLignePere(
			EOEditingContext ec, EOPrestationLigne pl, EOFacturePapier fap) {
		if (ec == null || pl == null || fap == null)
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOFacturePapierLigne.FACTURE_PAPIER_KEY
				+ " = %@ and " + EOFacturePapierLigne.PRESTATION_LIGNE_KEY
				+ " = %@";
		tbKeys.add(fap);
		tbKeys.add(pl.prestationLignePere());
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOFacturePapierLigne.ENTITY_NAME, qual, null);
		NSArray<EOFacturePapierLigne> fapl = ec
				.objectsWithFetchSpecification(fetchSpec);
		if (fapl != null)
			return fapl.get(0);
		return null;
	}

	// list2
	public static NSArray<NSDictionary<String, Object>> getPlanCoFromPrestation(
			EOEditingContext ec, EOPrestation prestation,
			boolean forAddPartenaire) {
		NSArray<NSDictionary<String, Object>> results = new NSMutableArray<NSDictionary<String, Object>>();
		if (ec == null || prestation == null)
			return null;
		// Si la prestation n'est pas encore stockée en Base
		int prestId = 0;
		if (ERXEOControlUtilities.primaryKeyStringForObject(prestation) != null) {
			prestId = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prestation));
		}
		if (prestId == 0) {
			NSArray<EOPrestationLigne> pls = prestation.prestationLignes();
			NSArray<String> pcoNums = new NSMutableArray<String>();
			if (pls == null)
				return null;
			if (pls.isEmpty())
				return null;
			NSDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
			for (int i = 0; i < pls.count(); i++) {
				EOPrestationLigne pl = pls.get(i);
				if (!pcoNums.contains(pl.planComptable().pcoNum())) {
					pcoNums.add(pl.planComptable().pcoNum());
					dico = new NSMutableDictionary<String, Object>();
					dico.put("PCO_NUM", pl.planComptable().pcoNum());
				}
			}
			results.add(dico);
			return results;
		} else {
			String sql = "select distinct pl.pco_num from JEFY_RECETTE.PRESTATION_LIGNE pl "
					+ " WHERE pl.prest_id =" + prestId + " ";

			if (!forAddPartenaire)
				sql += " AND pl.prlig_quantite_reste > 0";

			EOModel model = EOUtilities.entityForClass(ec,
					EOPrestationLigne.class).model();
			return EOUtilities.rawRowsForSQL(ec, model.name(), sql, null);
		}
	}

	/**
	 * Recup totaux avec PCO non null
	 * 
	 * @param ec
	 * @param prestId
	 * @param pcoNum
	 * @return
	 * @throws FormcoException
	 */
	public static NSArray<NSDictionary<String, Object>> getTotaux(
			EOEditingContext ec, EOPrestation prestation, int pcoNum)
			throws FormcoException {
		if (ec == null || pcoNum <= 0 || prestation == null)
			throw new FormcoException(
					"Calcul totaux pour gerer la facture impossible, ec ou pcoNum null");
		NSArray<NSDictionary<String, Object>> results = new NSMutableArray<NSDictionary<String, Object>>();
		// Si la prestation n'est pas encore en Base
		// Si la prestation n'est pas encore stockée en Base
		int prestId = 0;
		if (ERXEOControlUtilities.primaryKeyStringForObject(prestation) != null) {
			prestId = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prestation));
		}
		if (prestId == 0) {

			NSArray<EOPrestationLigne> pls = prestation.prestationLignes();
			BigDecimal somTotalHT = new BigDecimal(BigInteger.ZERO);
			BigDecimal somTotalTTC = new BigDecimal(BigInteger.ZERO);

			if (pls == null)
				throw new FormcoException(
						"Calcul totaux pour gerer la facture impossible,aucune prestationLigne");
			if (pls.isEmpty())
				throw new FormcoException(
						"Calcul totaux pour gerer la facture impossible,aucune prestationLigne");
			for (int i = 0; i < pls.count(); i++) {
				EOPrestationLigne pl = pls.get(i);
				if (pl.planComptable().pcoNum().equalsIgnoreCase("" + pcoNum)) {
					somTotalHT = somTotalHT.add(pl.prligTotalResteHt());
					somTotalTTC = somTotalTTC.add(pl.prligTotalResteTtc());
				}
			}
			NSDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
			dico.put("MY_FAP_TOTAL_HT", "" + somTotalHT);
			dico.put("MY_FAP_TOTAL_TTC", "" + somTotalTTC);
			results.add(dico);
		} else {
			String sql = "SELECT SUM(prlig.prlig_total_reste_ht) as MY_FAP_TOTAL_HT, SUM(prlig.prlig_total_reste_ttc) "
					+ "as MY_FAP_TOTAL_TTC "
					+ "FROM jefy_recette.prestation_ligne prlig "
					+ "WHERE prlig.prest_id ="
					+ prestId
					+ " AND prlig.pco_num = " + pcoNum;
			EOModel model = EOUtilities.entityForClass(ec,
					EOPrestationLigne.class).model();
			results = EOUtilities.rawRowsForSQL(ec, model.name(), sql, null);
		}
		return results;
	}

	/**
	 * Recup totaux avec PCO Null
	 * 
	 * @param ec
	 * @param prestId
	 * @param pcoNum
	 * @return
	 * @throws FormcoException
	 */
	public static NSArray<NSDictionary<String, Object>> getTotauxPCONull(
			EOEditingContext ec, EOPrestation prestation)
			throws FormcoException {
		if (ec == null || prestation == null)
			throw new FormcoException(
					"Calcul totaux pour gerer la facture impossible, ec null");
		NSArray<NSDictionary<String, Object>> results = new NSMutableArray<NSDictionary<String, Object>>();
		// Si la prestation n'est pas encore stockée en Base
		int prestId = 0;
		if (ERXEOControlUtilities.primaryKeyStringForObject(prestation) != null) {
			prestId = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(prestation));
		}
		if (prestId == 0) {
			NSArray<EOPrestationLigne> pls = prestation.prestationLignes();
			BigDecimal somTotalHT = new BigDecimal(BigInteger.ZERO);
			BigDecimal somTotalTTC = new BigDecimal(BigInteger.ZERO);

			if (pls == null)
				throw new FormcoException(
						"Calcul totaux pour gerer la facture impossible,aucune prestationLigne");
			if (pls.isEmpty())
				throw new FormcoException(
						"Calcul totaux pour gerer la facture impossible,aucune prestationLigne");
			for (int i = 0; i < pls.count(); i++) {
				EOPrestationLigne pl = pls.get(i);
				somTotalHT = somTotalHT.add(pl.prligTotalResteHt());
				somTotalTTC = somTotalTTC.add(pl.prligTotalResteTtc());
			}
			NSDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
			dico.put("MY_FAP_TOTAL_HT", "" + somTotalHT);
			dico.put("MY_FAP_TOTAL_TTC", "" + somTotalTTC);
			results.add(dico);
		} else {

			String sql = "SELECT SUM(prlig.prlig_total_reste_ht) as MY_FAP_TOTAL_HT, SUM(prlig.prlig_total_reste_ttc) "
					+ "as MY_FAP_TOTAL_TTC "
					+ "FROM jefy_recette.prestation_ligne prlig "
					+ "WHERE prlig.prest_id ="
					+ prestId
					+ " AND prlig.pco_num IS NULL";
			EOModel model = EOUtilities.entityForClass(ec,
					EOPrestationLigne.class).model();
			results = EOUtilities.rawRowsForSQL(ec, model.name(), sql, null);
		}
		return results;
	}

	/**
	 * JEFY_RECETTE.GET_NUMEROTATION
	 * 
	 * @param ec
	 * @param tnuEntite
	 * @param exeOrdre
	 * @param gesCode
	 * @param tnuId
	 * @return
	 * @throws FormcoException
	 */
	public static int getNumerotation(EOEditingContext ec, String tnuEntite,
			int exeOrdre, String gesCode) throws FormcoException {
		int num_numero = 0;
		if (ec == null || tnuEntite == null || exeOrdre <= 0) {
			throw new FormcoException("getNumerotation: ec ou tnuEntite null");
		}
		int myNb = getCountTypeNumerotation(ec, tnuEntite);
		if (myNb != 1)
			throw new FormcoException(
					"getNumerotation : Il y a un problème avec le type de numerotation");
		EOTypeNumerotation typeNum = (EOTypeNumerotation) EOUtilities
				.objectMatchingKeyAndValue(ec, EOTypeNumerotation.ENTITY_NAME,
						EOTypeNumerotation.TNU_ENTITE_KEY, tnuEntite);
		int tnuId = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(typeNum));
		int count = getCountTypeNumerotationFromGesAndExe(ec, exeOrdre,
				gesCode, tnuId);
		if (count > 1) {
			throw new FormcoException(
					"getNumerotation: il y a un pb pour la numérotation");
		}
		try {
			if (count == 0) {
				EONumerotation num = EONumerotation.creerInstance(ec);
				num.setNumId(num.creerNumId(ec));
				num.setExeOrdre(exeOrdre);
				num.setGesCode(gesCode);
				num.setTnuId(typeNum.tnuId());
			}
			NSArray<NSDictionary<String, Object>> resNums = getNumsFromExeAndGes(
					ec, exeOrdre, gesCode, typeNum.tnuId());
			num_numero = ((Number) FORMAT_NUM.parseObject(resNums
					.objectAtIndex(0).valueForKey("MY_NUM_NUMERO").toString()))
					.intValue();
			num_numero++;
			int num_id = ((Number) FORMAT_NUM.parseObject(resNums
					.objectAtIndex(0).valueForKey("MY_NUM_ID").toString()))
					.intValue();

			EONumerotation numerotation = (EONumerotation) EOUtilities
					.objectMatchingKeyAndValue(ec, EONumerotation.ENTITY_NAME,
							EONumerotation.NUM_ID_KEY, num_id);
			numerotation.setNumNumero(num_numero);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new FormcoException(e.getMessage());
		}
		return num_numero;
	}

	/**
	 * Utilise pour JEFY_RECETTE.GET_NUMEROTATION
	 * 
	 * @param ec
	 * @param tnuEntite
	 * @return
	 * @throws FormcoException
	 */
	public static int getCountTypeNumerotation(EOEditingContext ec,
			String tnuEntite) throws FormcoException {
		if (ec == null || tnuEntite == null)
			throw new FormcoException(
					"getCountTypeNumerotation : ec ou tnuEntite null");

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOTypeNumerotation.TNU_ENTITE_KEY + " = %@ ";
		tbKeys.add(tnuEntite);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOTypeNumerotation.ENTITY_NAME, qual, null);
		NSArray<EOTypeNumerotation> types = ec
				.objectsWithFetchSpecification(fetchSpec);
		return types.count();
	}

	/**
	 * Utilise pour JEFY_RECETTE.GET_NUMEROTATION
	 * 
	 * @param ec
	 * @param exeOrdre
	 * @param gesCode
	 * @return
	 * @throws FormcoException
	 */
	public static int getCountTypeNumerotationFromGesAndExe(
			EOEditingContext ec, int exeOrdre, String gesCode, int tnuId)
			throws FormcoException {
		if (ec == null || exeOrdre <= 0 || tnuId <= 0)
			throw new FormcoException(
					"getCountTypeNumerotationFromGesAndExe : ec, exe ou tnuId null");
		int count = 0;
		String sqlSelect = "SELECT COUNT(*) AS MY_NB FROM JEFY_RECETTE.NUMEROTATION WHERE tnu_id="
				+ tnuId
				+ " AND DECODE(ges_code,NULL,'-',ges_code) = DECODE("
				+ gesCode
				+ ",NULL,'-',"
				+ gesCode
				+ ") "
				+ "AND DECODE(exe_ordre,NULL,'-',exe_ordre) = DECODE("
				+ exeOrdre + ",NULL,'-'," + exeOrdre + ")";
		EOModel model = EOUtilities.entityForClass(ec, EONumerotation.class)
				.model();
		NSArray<NSDictionary<String, ?>> res = EOUtilities.rawRowsForSQL(ec,
				model.name(), sqlSelect, null);
		if (res != null) {
			try {
				count = ((Number) FORMAT_NUM.parseObject(res.objectAtIndex(0)
						.valueForKey("MY_NB").toString())).intValue();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		return count;
	}

	/**
	 * Utilise pour JEFY_RECETTE.GET_NUMEROTATION
	 * 
	 * @param ec
	 * @param exeOrdre
	 * @param gesCode
	 * @param tnuId
	 * @return
	 * @throws FormcoException
	 */
	public static NSArray<NSDictionary<String, Object>> getNumsFromExeAndGes(
			EOEditingContext ec, int exeOrdre, String gesCode, int tnuId)
			throws FormcoException {
		if (ec == null || exeOrdre <= 0 || tnuId <= 0)
			throw new FormcoException(
					"getNumsFromexeAndGes : ec, exe ou gestion ou tnuId null");
		int count = 0;
		String sqlSelect = "SELECT num_numero as MY_NUM_NUMERO, num_id as MY_NUM_ID FROM JEFY_RECETTE.NUMEROTATION "
				+ "WHERE tnu_id="
				+ tnuId
				+ " AND DECODE(ges_code,NULL,'-',ges_code) = DECODE("
				+ gesCode
				+ ",NULL,'-',"
				+ gesCode
				+ ") "
				+ " AND DECODE(exe_ordre,NULL,'-',exe_ordre) = DECODE("
				+ exeOrdre + ",NULL,'-'," + exeOrdre + ")";
		EOModel model = EOUtilities.entityForClass(ec, EONumerotation.class)
				.model();
		NSArray<NSDictionary<String, Object>> res = EOUtilities.rawRowsForSQL(
				ec, model.name(), sqlSelect, null);
		return res;
	}

	/**
	 * Utilise pour JEFY_RECETTE.del_facture_papier
	 * 
	 * @param ec
	 * @param exeOrdre
	 * @param tnuId
	 * @return
	 * @throws FormcoException
	 */
	public static NSArray<NSDictionary<String, Object>> getNumsFromExe(
			EOEditingContext ec, int exeOrdre, int tnuId)
			throws FormcoException {
		if (ec == null || exeOrdre <= 0 || tnuId <= 0)
			throw new FormcoException("getNumsFromExe : ec, exe ou tnuId null");
		int count = 0;
		String sqlSelect = "SELECT num_numero as MY_NUM_NUMERO, num_id as MY_NUM_ID FROM JEFY_RECETTE.NUMEROTATION "
				+ "WHERE tnu_id="
				+ tnuId
				+ " AND DECODE(exe_ordre,NULL,'-',exe_ordre) = DECODE("
				+ exeOrdre + ",NULL,'-'," + exeOrdre + ")";
		EOModel model = EOUtilities.entityForClass(ec, EONumerotation.class)
				.model();
		NSArray<NSDictionary<String, Object>> res = EOUtilities.rawRowsForSQL(
				ec, model.name(), sqlSelect, null);
		return res;
	}

	/**
	 * Recupere la derniere adresse de prestation si elle existe
	 * 
	 * @param ec
	 * @param prestId
	 * @return
	 * @throws FormcoException
	 */
	public static EOAdresse getAdressePrestation(EOEditingContext ec,
			int prestId) throws FormcoException {
		if (ec == null)
			throw new FormcoException(
					"getAdressePrestation: ec ou prestid null");
		if (prestId == 0)
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOPrestationAdresseClient.PREST_ID_KEY
				+ " = %@ AND " + EOPrestationAdresseClient.DATE_FIN_KEY
				+ " = NULL";
		tbKeys.add(prestId);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOPrestationAdresseClient.ENTITY_NAME, qual, null);
		NSArray<EOPrestationAdresseClient> padcs = ec
				.objectsWithFetchSpecification(fetchSpec);
		if (padcs == null)
			return null;
		if (padcs.count() <= 0)
			return null;
		EOPrestationAdresseClient padc = padcs.get(padcs.count() - 1);
		return padc.toAdresse();
	}

	public static EOPlanComptable getPlanComptableFromPcoNum(
			EOEditingContext ec, int pcoNum, EOExercice exercice) {
		if (ec == null || pcoNum <= 0)
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOPlanComptable.PCO_NUM_KEY + " = %@ AND "
				+ EOPlanComptable.EXERCICE_KEY + " = %@ ";
		tbKeys.add("" + pcoNum);
		tbKeys.add(exercice);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOPlanComptable.ENTITY_NAME, qual, null);
		NSArray<EOPlanComptable> pco = ec
				.objectsWithFetchSpecification(fetchSpec);
		if (pco != null)
			return pco.get(0);
		return null;

	}

	public static int getCountFapFromPrest(EOEditingContext ec, int prestId,
			int fapId) throws FormcoException {
		if (ec == null || prestId <= 0 || fapId <= 0) {
			throw new FormcoException(
					"getCountFapFromPrest ec, fapId ou prestId null");
		}
		int count = 0;
		String sqlSelect = " SELECT COUNT(*) as MY_NB FROM jefy_recette.FACTURE_PAPIER WHERE fap_id <> "
				+ fapId + " AND prest_id = " + prestId + " ";
		EOModel model = EOUtilities.entityForClass(ec, EOFacturePapier.class)
				.model();
		NSArray<NSDictionary<String, ?>> res = EOUtilities.rawRowsForSQL(ec,
				model.name(), sqlSelect, null);
		if (res != null) {
			try {
				count = ((Number) FORMAT_NUM.parseObject(res.objectAtIndex(0)
						.valueForKey("MY_NB").toString())).intValue();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return count;
	}

	public static NSTimestamp minFapDate(EOEditingContext ec, int prestId,
			int fapId) {
		// SELECT MIN(fap_date) FROM FACTURE_PAPIER WHERE prest_id = my_prest_id
		// AND fap_id <> a_fap_id
		NSTimestamp temps = null;
		String sqlSelect = " SELECT FAP_ID as MY_FAP_ID, MIN(fap_date) as MY_NB FROM JEFY_RECETTE.FACTURE_PAPIER WHERE "
				+ "prest_id = "
				+ prestId
				+ " AND fap_id <> "
				+ fapId
				+ " GROUP BY FAP_ID";
		EOModel model = EOUtilities.entityForClass(ec, EOFacturePapier.class)
				.model();
		NSArray<NSDictionary<String, ?>> res = EOUtilities.rawRowsForSQL(ec,
				model.name(), sqlSelect, null);
		if (res != null) {
			try {
				int fapIdMin = ((Number) FORMAT_NUM.parseObject(res
						.objectAtIndex(0).valueForKey("MY_FAP_ID").toString()))
						.intValue();
				if (fapIdMin > 0) {
					EOFacturePapier fap = (EOFacturePapier) EOUtilities
							.objectMatchingKeyAndValue(ec,
									EOFacturePapier.ENTITY_NAME,
									EOFacturePapier.FAP_ID_KEY, fapIdMin);
					if (fap != null)
						temps = fap.fapDate();
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return temps;
	}

	static public Boolean validerDevis(EOEditingContext ec,
			EOFacturePapier fap, CktlDataBus dataBus) throws FormcoException {
		if (fap == null || ec == null)
			return false;

		try {
			EOQualifier qualPrest = new EOKeyValueQualifier(
					EOBoutiqueInscription.PRESTATION_KEY,
					EOQualifier.QualifierOperatorEqual, fap.prestation());
			NSArray<EOBoutiqueInscription> data = EOBoutiqueInscription
					.fetchAll(ec, qualPrest);
			EOBoutiqueInscription insc = null;
			if (data.count() > 0) {
				insc = data.lastObject();
				System.out.println("adminjefy="
						+ insc.boutique().jefyAdminUtilisateur());
				FactoryFacturePapier.valideClient(dataBus, ec, fap, insc
						.boutique().jefyAdminUtilisateur());
				ec.saveChanges();
				return true;
			}
		} catch (Exception e) {
			ec.revert();
			throw new FormcoException(e.getMessage());
		}
		return false;
	}

}
