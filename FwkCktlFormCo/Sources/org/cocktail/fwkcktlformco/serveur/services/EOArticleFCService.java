/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import java.util.Enumeration;

import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOArticleFCService {

	/**
	 * recuperer le codeanneesuivie qui est ds habilitation
	 * 
	 * @param ec
	 * @param article
	 * @param annee
	 * @return
	 * @throws FormcoException
	 */
	public static int getAnneeSuivie(EOEditingContext ec, EOArticleFC article,
			EOScolFormationAnnee annee) throws FormcoException {
		if (ec == null || article == null || annee == null) {
			throw new FormcoException(
					"Ec, Article ou Annee null. impossible de recuperer l'annee suivie");
		}

		if (article.toScolFormationDiplome() != null) {
			return EOArticleFCService.getAnneeSuiviFromDiplome(ec,
					article.toScolFormationDiplome(), annee);
		} else if (article.toScolMaquetteParcours() != null) {
			EOScolFormationSpecialisation specialisation = article
					.toScolMaquetteParcours()
					.toFwkScolarite_ScolFormationSpecialisation();
			return EOArticleFCService.getAnneeSuiviFromSpecialisation(ec,
					specialisation, annee);
		} else if (article.toScolMaquetteRepartitionSem() != null) {
			return EOArticleFCService.getAnneeSuiviFromSpecialisation(ec,
					article.toScolMaquetteRepartitionSem()
							.toFwkScolarite_ScolMaquetteParcours()
							.toFwkScolarite_ScolFormationSpecialisation(),
					annee);
		} else if (article.toScolMaquetteRepartitionUe() != null) {
			EOScolMaquetteSemestre sem = article.toScolMaquetteRepartitionUe()
					.toFwkScolarite_ScolMaquetteSemestre();
			NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.add(new EOKeyValueQualifier(
					EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY,
					EOQualifier.QualifierOperatorEqual, sem));
			quals.add(new EOKeyValueQualifier(
					EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY,
					EOQualifier.QualifierOperatorEqual, annee));
			EOScolMaquetteRepartitionSem repSem = EOScolMaquetteRepartitionSem
					.fetchFirstByQualifier(ec, new EOAndQualifier(quals));
			return EOArticleFCService.getAnneeSuiviFromSpecialisation(ec,
					repSem.toFwkScolarite_ScolMaquetteParcours()
							.toFwkScolarite_ScolFormationSpecialisation(),
					annee);
		} else if (article.toScolMaquetteRepartitionEc() != null) {
			return EOArticleFCService.getAnneeSuivieFromRepartEC(ec, article.toScolMaquetteRepartitionEc(), annee);
		}
		else if(article.toScolMaquetteRepartitionAp()!=null)
		{
			EOScolMaquetteEc mEc = article.toScolMaquetteRepartitionEc().toFwkScolarite_ScolMaquetteEc();
			NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.add(new EOKeyValueQualifier(EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, annee));
			quals.add(new EOKeyValueQualifier(EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY, EOQualifier.QualifierOperatorEqual, mEc));
			EOScolMaquetteRepartitionEc mRepEC  = EOScolMaquetteRepartitionEc.fetchFirstByQualifier(ec, new EOAndQualifier(quals));
			return EOArticleFCService.getAnneeSuivieFromRepartEC(ec, mRepEC, annee);
			
		}
		return 0;

	}

	public static int getAnneeSuiviFromDiplome(EOEditingContext ec,
			EOScolFormationDiplome diplome, EOScolFormationAnnee annee) {

		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.add(new EOKeyValueQualifier(
				EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY,
				EOQualifier.QualifierOperatorEqual, annee));
		quals.add(new EOKeyValueQualifier(
				EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY
						+ "."
						+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY,
				EOQualifier.QualifierOperatorEqual, diplome));
		EOScolFormationHabilitation habilitation = EOScolFormationHabilitation
				.fetchFirstByQualifier(ec, new EOAndQualifier(quals));
		if (habilitation != null)
			return habilitation.fhabNiveau().intValue();
		return 0;
	}

	public static int getAnneeSuiviFromSpecialisation(EOEditingContext ec,
			EOScolFormationSpecialisation specialisation,
			EOScolFormationAnnee annee) {
		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.add(new EOKeyValueQualifier(
				EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY,
				EOQualifier.QualifierOperatorEqual, annee));
		quals.add(new EOKeyValueQualifier(
				EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY,
				EOQualifier.QualifierOperatorEqual, specialisation));
		EOScolFormationHabilitation habilitation = EOScolFormationHabilitation
				.fetchFirstByQualifier(ec, new EOAndQualifier(quals));
		if (habilitation != null)
			return habilitation.fhabNiveau().intValue();
		return 0;
	}

	
	public static int getAnneeSuivieFromRepartEC(EOEditingContext ec,
			EOScolMaquetteRepartitionEc repEc, EOScolFormationAnnee annee) {
		Enumeration<EOScolMaquetteRepartitionUe> enumRepUes = repEc
				.toFwkScolarite_ScolMaquetteUe()
				.toFwkScolarite_ScolMaquetteRepartitionUes(
						new EOKeyValueQualifier(
								EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY,
								EOQualifier.QualifierOperatorEqual, annee))
				.objectEnumerator();
		NSArray<EOScolMaquetteSemestre> sems = new NSMutableArray<EOScolMaquetteSemestre>();
		while (enumRepUes.hasMoreElements()) {
			EOScolMaquetteRepartitionUe repUe = (EOScolMaquetteRepartitionUe) enumRepUes
					.nextElement();
			if (!sems.contains(repUe.toFwkScolarite_ScolMaquetteSemestre())) {
				sems.add(repUe.toFwkScolarite_ScolMaquetteSemestre());
			}
		}
		Enumeration<EOScolMaquetteSemestre> enumSems = sems.objectEnumerator();
		NSArray<EOScolMaquetteRepartitionSem> tabRepSems = new NSMutableArray<EOScolMaquetteRepartitionSem>();

		while (enumSems.hasMoreElements()) {
			EOScolMaquetteSemestre sem = (EOScolMaquetteSemestre) enumSems
					.nextElement();
			Enumeration<EOScolMaquetteRepartitionSem> enumRepSems = sem
					.toFwkScolarite_ScolMaquetteRepartitionSems(
							new EOKeyValueQualifier(
									EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY,
									EOQualifier.QualifierOperatorEqual, annee))
					.objectEnumerator();
			while (enumRepSems.hasMoreElements()) {
				EOScolMaquetteRepartitionSem repSem = (EOScolMaquetteRepartitionSem) enumRepSems
						.nextElement();
				if (!tabRepSems.contains(repSem)) {
					tabRepSems.add(repSem);
				}
			}
		}
		return EOArticleFCService.getAnneeSuiviFromSpecialisation(ec, tabRepSems.get(0).toFwkScolarite_ScolMaquetteParcours()
							.toFwkScolarite_ScolFormationSpecialisation(), annee);

	}
}
