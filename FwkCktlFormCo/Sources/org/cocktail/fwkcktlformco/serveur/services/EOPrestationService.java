/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.HashMap;

import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.fwkcktldroitsutils.common.metier.EOExercice;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEOArticleFC;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEOBoutique;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEOScolMaquetteRepartitionSem;
import org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC;
import org.cocktail.fwkcktlformco.serveur.utils.FCUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.kava.server.finder.FinderModeRecouvrement;
import org.cocktail.kava.server.finder.FinderTauxProrata;
import org.cocktail.kava.server.finder.FinderTypeCreditRec;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.metier.EOBoutique;
import org.cocktail.kava.server.metier.EOBoutiqueInscription;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOModeRecouvrement;
import org.cocktail.kava.server.metier.EOPlanComptable;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.EOTypePublic;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class EOPrestationService {
	// Code du type credit recette par defaut
	public static final String DEFAULT_TCD_CODE_RECETTE = "02";

	public static void deletePrestation(EOEditingContext ec,
			EOPrestation prestation) throws FormcoException {
		if ((ec == null) || (prestation == null))
			throw new FormcoException("suppression prestation impossible");
		try {
			NSArray<EOBoutiqueInscription> boutiqueInscriptions = EOUtilities
					.objectsMatchingKeyAndValue(ec,
							EOBoutiqueInscription.ENTITY_NAME,
							EOBoutiqueInscription.PRESTATION_KEY, prestation);
			for (EOBoutiqueInscription inscription : boutiqueInscriptions) {
				ec.deleteObject(inscription);
			}

			NSArray<EOPrestationLigne> prestLg = prestation.prestationLignes();
			for (EOPrestationLigne ligne : prestLg) {
				ligne.setPrestation(null);
				ec.deleteObject(ligne);
			}

			prestation.setPrestationLignes(null);
			ec.deleteObject(prestation);
			ec.saveChanges();
		} catch (Exception e) {
			e.printStackTrace();
			throw new FormcoException(e);
		}
	}

	/**
	 * Retourne toutes les RepartEC de la prestation.
	 * 
	 * @param ec
	 * @param prestation
	 * @return
	 */
	public static NSArray<EOScolMaquetteRepartitionEc> getRepEcFromPrestation(
			EOEditingContext ec, EOPrestation prestation)
			throws FormcoException {
		if (ec == null)
			return null;
		if (prestation == null)
			return null;
		NSArray<EOScolMaquetteRepartitionEc> res = new NSMutableArray<EOScolMaquetteRepartitionEc>();

		NSArray<EOPrestationLigne> prestLignes = prestation.prestationLignes();
		Enumeration<EOPrestationLigne> enumpl = prestLignes.objectEnumerator();
		while (enumpl.hasMoreElements()) {
			EOPrestationLigne pl = (EOPrestationLigne) enumpl.nextElement();
			EOArticleFC artFC = FinderEOArticleFC.getArticleFCFromCatAr(ec,
					pl.catalogueArticle());
			if (artFC != null) {
				if ((res.count() > 0)
						&& (artFC.toScolMaquetteParcours() != null)) {
					throw new FormcoException(
							"Une commande qui contient un Parcours/Diplome ne peut pas contenir d'autres articles. Veuillez créer une autre commande pour les EC aux choix.");
				}
				if (artFC.toScolMaquetteRepartitionEc() != null) {
					if (!res.contains(artFC.toScolMaquetteRepartitionEc()))
						res.add(artFC.toScolMaquetteRepartitionEc());
				}
			}
		}
		if (res.count() <= 0)
			return null;
		return res;
	}

	/**
	 * Retourne ts les parcours de la prestation
	 * 
	 * @param ec
	 * @param prestation
	 * @return
	 */
	public static NSArray<EOScolMaquetteParcours> getParcoursFromPrestation(
			EOEditingContext ec, EOPrestation prestation)
			throws FormcoException {
		if (ec == null) {
			throw new FormcoException("EC null");
		}
		if (prestation == null) {
			throw new FormcoException("Prestation null");
		}
		NSArray<EOScolMaquetteParcours> res = new NSMutableArray<EOScolMaquetteParcours>();

		NSArray<EOPrestationLigne> prestLignes = prestation.prestationLignes();
		Enumeration<EOPrestationLigne> enumpl = prestLignes.objectEnumerator();
		while (enumpl.hasMoreElements()) {
			EOPrestationLigne pl = (EOPrestationLigne) enumpl.nextElement();
			EOArticleFC artFC = FinderEOArticleFC.getArticleFCFromCatAr(ec,
					pl.catalogueArticle());
			if (artFC != null) {
				if ((res.count() > 0)
						&& (artFC.toScolMaquetteRepartitionEc() != null)) {
					throw new FormcoException(
							"Une commande qui contient un Parcours/Diplome ne peut pas contenir d'autres articles. Veuillez créer une autre commande pour les EC aux choix.");
				}
				if (artFC.toScolMaquetteParcours() != null) {
					if (!res.contains(artFC.toScolMaquetteParcours()))
						res.add(artFC.toScolMaquetteParcours());
				}

			}
		}
		// pour l'instant, on ne peut pas gérer une commande avec plusieurs
		// parcours différents.
		if (res.count() > 1) {
			throw new FormcoException(
					"Une commande ne peut pas contenir plusieurs parcours. Veuillez créer autant de commande/prestation que de parcours souhaité. ");
		}
		return res;
	}

	public static int getAnneeSuivieFromPrestation(EOEditingContext ec,
			EOPrestation prestation) throws FormcoException {
		if (ec == null) {
			throw new FormcoException("EC null");
		}
		if (prestation == null) {
			throw new FormcoException("Prestation null");
		}

		NSArray<EOPrestationLigne> prestLignes = prestation.prestationLignes();
		Enumeration<EOPrestationLigne> enumpl = prestLignes.objectEnumerator();
		while (enumpl.hasMoreElements()) {
			EOPrestationLigne pl = (EOPrestationLigne) enumpl.nextElement();
			EOArticleFC artFC = FinderEOArticleFC.getArticleFCFromCatAr(ec,
					pl.catalogueArticle());
			if (artFC != null) {
				return artFC.cAnneeSuivie();
			}
		}
		return 1;
	}

	/**
	 * Renvoie ts les EOScolMaquetteRepartitionSem a partir d'une prestation et
	 * de l'annee
	 * 
	 * @param ec
	 * @param prestation
	 * @return
	 */
	public static NSArray<EOScolMaquetteRepartitionSem> getRepSemFromPrestation(
			EOEditingContext ec, EOPrestation prestation, int anneeScolaire)
			throws FormcoException {
		NSArray<EOScolMaquetteRepartitionSem> res = new NSMutableArray<EOScolMaquetteRepartitionSem>();

		NSArray<EOScolMaquetteParcours> mPars = EOPrestationService
				.getParcoursFromPrestation(ec, prestation);
		if ((mPars == null) || (mPars.count() <= 0))
			return null;

		Enumeration<EOScolMaquetteParcours> enumPars = mPars.objectEnumerator();
		while (enumPars.hasMoreElements()) {
			EOScolMaquetteParcours mPar = (EOScolMaquetteParcours) enumPars
					.nextElement();
			NSArray<EOScolMaquetteRepartitionSem> repSems = FinderEOScolMaquetteRepartitionSem
					.getRepSemFromParcours(ec, mPar, anneeScolaire);
			Enumeration<EOScolMaquetteRepartitionSem> enumrepSems = repSems
					.objectEnumerator();
			while (enumrepSems.hasMoreElements()) {
				EOScolMaquetteRepartitionSem repSem = (EOScolMaquetteRepartitionSem) enumrepSems
						.nextElement();
				if (!res.contains(repSem)) {
					res.add(repSem);
				}

			}
		}
		if (res.count() <= 0)
			return null;
		return res;
	}

	/**
	 * Creer une prestation de formation continue a partir d'articles commandés.
	 * Le client de la prestation est inscrit dans BoutiqueInscription. Le
	 * Service des Formation Continues enregistré comme Structure , Personne et
	 * Fournisseur est le client de la prestation. Comme il peut y avoir
	 * plusieurs clients pour une même formation, on lie la prestation au
	 * service FC. On connait les payeurs en recupérant le contrat relié à la
	 * prestation et en recherchant les partenaires non principaux. Chaque
	 * RepartTranche du contrat spécifie les montants payés par les
	 * ContratPartenaires pour cette prestation. On modélise ainsi la
	 * subventions d'organismes qui seront ajoutés en tant que ContratPartenaire
	 * du contrat relié à cette prestation.Les stagiaires sont
	 * ContratPartenaireContact. On les recupère en recherchant les
	 * ContratPartenaireContact pour chaque ContratPartenaire du contrat relié à
	 * la prestation.
	 * La prestation n'est pas reliee a la convention ici car on a cree un Contrat, et il faut qu'il soit en base pour pouvoir 
	 * requeter sur le conOrdre et ramener un PieFwk.Convention pour pouvoir faire un prestation.setConvention.
	 * 
	 * @param ec
	 * @param articlesCommandes
	 * @param exercice
	 * @param fournisseurPartenaire
	 * @param appId
	 * @param typePublic
	 * @param persIdFC
	 * @return
	 * @throws FormcoException
	 */
	public static EOPrestation creerPrestation(EOEditingContext ec,
			HashMap<EOArticleFC, Integer> articlesCommandes,
			EOExercice exercice, EOFournis fournisseurPartenaire, String appId,
			EOTypePublic typePublic, int persIdFC) throws FormcoException {
		if (ec == null || articlesCommandes == null
				|| articlesCommandes.isEmpty() || exercice == null
				|| fournisseurPartenaire == null || typePublic == null
				|| persIdFC <= 0)
			return null;
		EOPrestation prestation = null;
		EOArticleFC firstArticle = null;
		org.cocktail.application.serveur.eof.EOExercice exercicePie = FCUtils
				.getExerciceDataComFromExeDroitsUtils(ec, exercice);

		EOArticleFC[] tabArticle = articlesCommandes.keySet().toArray(
				new EOArticleFC[articlesCommandes.size()]);
		firstArticle = tabArticle[0];
		EOBoutique boutique = FinderEOBoutique.findBoutiqueFromArticleFC(ec,
				firstArticle);
		if (fournisseurPartenaire.isFouValideValide()) {
			// Creer la prestation la facture et le contrat
			String libellePresta = "[" + appId + "]"
					+ boutique.boutiqueLibelleFr();
			NSTimestamp maintenant = new NSTimestamp();

			// Recuperer le catalogue, type de credit et planCoRec
			EOPlanComptable plancoRec = null;
			EOTypeCredit tCredit = null;

			// Mode de recouvrement
			EOModeRecouvrement modeRecouvre = FinderModeRecouvrement
					.modeRecouvrementPrestationExterne(ec, exercicePie);

			// TypeEtat Valide
			EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(ec);

			Integer prestaNum = EOPrestation.generatePrestaNumero(exercicePie,
					ec);
			BigDecimal totalHT = new BigDecimal(0);
			BigDecimal totalTTC = new BigDecimal(0);
			BigDecimal totalTVA = new BigDecimal(0);

			prestation= EOPrestation.creerInstance(ec);

			firstArticle = (EOArticleFC) tabArticle[0];
			firstArticle = firstArticle.localInstanceIn(ec);
			if (firstArticle != null) {

				plancoRec = firstArticle.toCatalogueArticle().catalogue()
						.cataloguePrestation().planComptableRecette();

				NSArray<EOTypeCredit> tCredits = FinderTypeCreditRec
						.findForPlanComptable(ec, plancoRec);
				if (tCredits.count() > 0) {
					tCredit = tCredits.lastObject();
				}
				if (tCredit == null) {
					tCredit = FinderTypeCreditRec.find(ec,
							DEFAULT_TCD_CODE_RECETTE);
					if (tCredit == null) {
						throw new FormcoException(
								"Type de crédit de recette par défaut introuvable");
					}
				}
			}
			// initialiser les valeurs de la prestation
			// a partir du premier CatalogueArticle
			if (prestation.fournisUlrPrest() == null) {
				prestation.setFournisUlrPrest(firstArticle.toCatalogueArticle()
						.catalogue().fournisUlr());
			}
			if (prestation.organ() == null) {
				prestation.setOrgan(firstArticle.toCatalogueArticle()
						.catalogue().cataloguePrestation().organRecette());
			}
			if (prestation.lolfNomenclatureRecette() == null) {
				prestation.setLolfNomenclatureRecette(firstArticle
						.toCatalogueArticle().catalogue().cataloguePrestation()
						.lolfNomenclatureRecette());
			}
			if (prestation.tauxProrata() == null) {
				prestation.setTauxProrata(FinderTauxProrata.findDefault(ec,
						firstArticle.toCatalogueArticle().catalogue()
								.cataloguePrestation().organRecette(),
						exercicePie));
			}

			// initialiser le catalogue de la prestation
			if (firstArticle.toCatalogueArticle().catalogue() != null) {
				prestation.setCatalogue(firstArticle.toCatalogueArticle()
						.catalogue());
			}
			// Indiquer le service FC comme client de la prestation car une
			// prestation peut avoir plusieurs clients.
			// (org.cocktail.kava.server.metier.EOPersonne)
			EOQualifier qualStrFCPersonne = EOQualifier
					.qualifierWithQualifierFormat(
							org.cocktail.kava.server.metier.EOPersonne.PERS_ID_KEY
									+ "=%@", new NSArray(
									new Object[] { persIdFC }));
			org.cocktail.kava.server.metier.EOPersonne structureFCPersonne = org.cocktail.kava.server.metier.EOPersonne
					.fetchFirstByQualifier(ec, qualStrFCPersonne);

			// Le fournisseur peut etre nul mais il est utilisé dans la creation
			// de devis. il faut qu'il soit non nul pour pouvoir envoyer un
			// fichier
			// pdf!
			EOFournisUlr fournisFC = EOFournisUlr.fetchFirstByQualifier(ec,
					new EOKeyValueQualifier(EOFournisUlr.PERS_ID_KEY,
							EOQualifier.QualifierOperatorEqual, persIdFC));
			prestation.setFournisUlr(fournisFC);
			prestation.setPlanComptable(plancoRec);
			prestation.setPrestApplyTva("O");
			prestation.setPrestDate(maintenant);
			prestation.setPrestLibelle(libellePresta);
			prestation.setPrestCommentaireClient("[FORMATION-CONTINUE]");
			prestation.setPrestNumero(prestaNum);
			prestation.setPrestTotalHt(totalHT);
			prestation.setPrestTotalTtc(totalTTC);
			prestation.setPrestTotalTva(totalTVA);
			prestation.setTypeCreditRec(tCredit);
			prestation.setTypePublic(typePublic);
			prestation.setTypeEtat(etatValide);
			prestation.setExercice(exercicePie);
			// Une prestation peut avoir plusieurs clients. Du coup, on la relie
			// au
			// service FC
			prestation.setPersonne(structureFCPersonne);
			// On indique l'utilisateur FC comme utilisateur de la prestation
			EOUtilisateur userFC = EOUtilisateur.fetchFirstByQualifier(ec,
					new EOKeyValueQualifier(EOUtilisateur.PERS_ID_KEY,
							EOQualifier.QualifierOperatorEqual,
							structureFCPersonne.persId()));
			org.cocktail.kava.server.metier.EOUtilisateur userFCPie = FCUtils
					.getUserFromUserCktlDroits(ec, userFC);
			prestation.setUtilisateur(userFCPie);
			prestation.setModeRecouvrement(modeRecouvre);

			EOCatalogueArticle catAr = null;
			NSMutableArray tabLignePrestation = new NSMutableArray();
			// Creer les lignes de prestation
			for (EOArticleFC artFC : articlesCommandes.keySet()) {
				catAr = artFC.toCatalogueArticle().localInstanceIn(ec);
				totalHT = catAr.caarPrixHt().multiply(new BigDecimal(articlesCommandes.get(artFC)));
				totalTTC = totalHT;
				totalTVA = new BigDecimal(0);
//				totalHT = totalHT.add(catAr.caarPrixHt());
//				totalTTC = totalTTC.add(catAr.caarPrixTtc());
//				totalTVA = totalTVA.add(totalTTC.subtract(totalHT));
				// creer la prestation
				tabLignePrestation.add(creerLignePrestation(ec, artFC,
						articlesCommandes.get(artFC), prestation));
			}

			// On inscrit le client
			EOQualifier qualPersonne = EOQualifier
					.qualifierWithQualifierFormat(
							org.cocktail.kava.server.metier.EOPersonne.PERS_ID_KEY
									+ "=%@", new NSArray(
									new Object[] { fournisseurPartenaire
											.toPersonne().persId() }));
			org.cocktail.kava.server.metier.EOPersonne clientPersonne = org.cocktail.kava.server.metier.EOPersonne
					.fetchFirstByQualifier(ec, qualPersonne);
			if (clientPersonne != null && boutique != null) {
				EOBoutiqueInscription boutiqueInsc = EOBoutiqueInscription
						.createEOBoutiqueInscription(ec, boutique,
								clientPersonne);
				boutiqueInsc.setPrestationRelationship(prestation);
			}
			// ajouter les lignes à la prestation
			prestation.setPrestationLignes(tabLignePrestation);
			// terminer l'initialisation de la prestation avec les totaux.
			prestation.setPrestTotalHt(totalHT);
			prestation.setPrestTotalTtc(totalTTC);
			prestation.setPrestTotalTva(totalTVA);

			boolean autoCloturePrestation = boutique.isAutoCloturePrestation();
			boolean autoValPrestaClient = boutique.isAutoValPrestaClient();
			boolean autoValPrestaPrestataire = boutique
					.isAutoValPrestaPrestataire();

			// validation de la presta côtés client et prestataire.
			try {
				EOTypeEtat etatAnnule = FinderTypeEtat.typeEtatAnnule(ec);
				if (autoCloturePrestation) {
					prestation.cloturerPrestation(ec, etatAnnule);
				} else if (autoValPrestaPrestataire) {
					prestation.validerPrestationPrestataire(ec, etatAnnule);
				} else if (autoValPrestaClient) {
					prestation.validerPrestationClient(ec, etatAnnule);
				}
			} catch (Exception e) {
				throw new FormcoException(e.getMessage());
			}
		}
		return prestation;
	}

	/**
	 * Creer une ligne de prestation
	 * 
	 * @param ec
	 * @param articleFC
	 * @param qte
	 * @param prestation
	 * @throws FormcoException
	 */
	public static EOPrestationLigne creerLignePrestation(EOEditingContext ec,
			EOArticleFC articleFC, int qte, EOPrestation prestation)
			throws FormcoException {
		if ((ec == null) || (articleFC == null)) {
			throw new FormcoException(
					"creation d'une ligne de prestation impossible");
		}
		EOCatalogueArticle catAr = articleFC.toCatalogueArticle();
		BigDecimal prLgTotalHT = catAr.caarPrixHt().multiply(
				new BigDecimal(qte));
		BigDecimal prLgTotalTTC = catAr.caarPrixTtc().multiply(
				new BigDecimal(qte));
		String libelleArt = articleFC.getLibelleParcours();
		BigDecimal qteDispo = new BigDecimal(
				articleFC.getQuantiteDisponible(ec));
		qteDispo = qteDispo.subtract(new BigDecimal(qte));
		EOPrestationLigne lignePrest = EOPrestationLigne
				.createEOPrestationLigne(ec, catAr.caarPrixHt(),
						catAr.caarPrixTtc(), catAr.caarPrixTtc(),
						new NSTimestamp(), articleFC.getLibelleParcours(),
						new BigDecimal(qte), new BigDecimal(qte), prLgTotalHT,
						prLgTotalHT, prLgTotalTTC, prLgTotalTTC);

		lignePrest.setCatalogueArticle(catAr);
		lignePrest.setPrligReference(catAr.caarReference());
		lignePrest.setPrligDescription(libelleArt);
		lignePrest.setPlanComptable(catAr.article().articlePrestation()
				.planComptableRecette());
		lignePrest.setPrestation(prestation);
		lignePrest.setTypeArticle(catAr.article().typeArticle());
		lignePrest.setTva(catAr.tva());
		return lignePrest;

	}
	// /**
	// * ApiPrestation.delFacturePapier ne renvoyant pas le bon utlOrdre, j'ai
	// * créé la methode ci-dessous
	// *
	// * @param dataBus
	// * @param ec
	// * @param facturePapier
	// * @param utilisateur
	// * @throws Exception
	// */
	// public static void delFacturePapier(_CktlBasicDataBus dataBus,
	// EOEditingContext ec, EOFacturePapier facturePapier,
	// EOUtilisateur utilisateur) throws Exception {
	// if (ec == null || facturePapier == null || utilisateur == null) {
	// throw new Exception(
	// "Facture papier a supprimer ou utilisateur a null!!");
	// }
	//
	// try {
	// NSMutableDictionary dico = new NSMutableDictionary();
	//
	// int fapId = Integer.parseInt(ERXEOControlUtilities
	// .primaryKeyStringForObject(facturePapier));
	// dico.takeValueForKey(fapId, "010aFapId");
	//
	// int utlOrdre = Integer.parseInt(ERXEOControlUtilities
	// .primaryKeyStringForObject(utilisateur));
	// dico.takeValueForKey(utlOrdre, "020aUtlOrdre");
	//
	// dataBus.beginTransaction();
	// if (!dataBus
	// .executeProcedure("ApiPrestationDelFacturePapier", dico)) {
	// throw new Exception((String) dataBus.executedProcResult()
	// .valueForKey(_CktlBasicDataBus.ERROR_KEY));
	// }
	// dataBus.commitTransaction();
	// } catch (Exception e) {
	// e.printStackTrace();
	// dataBus.rollbackTransaction();
	// throw e;
	// }
	// }

	// public static EOFacturePapier creerFacturePapier(_CktlBasicDataBus
	// dataBus,
	// EOEditingContext ec, EOPrestation prestation, EOUtilisateur userFC)
	// throws Exception {
	// // relier la facture au client et a la prestation
	// ApiPrestation.genereFacturePapier(dataBus, dataBus.editingContext(),
	// prestation, userFC);
	//
	// NSArray list = FinderFacturePapier.find(ec, prestation);
	// if (list == null) {
	// throw new Exception(
	// "Problème : aucune facture n'a été générée pour cette prestation.");
	// } else {
	// return (EOFacturePapier) list.objectAtIndex(0);
	// }
	// }

}
