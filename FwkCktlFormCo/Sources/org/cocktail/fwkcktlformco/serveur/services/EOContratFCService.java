/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Enumeration;

import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.ProjetContrat;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.fwkcktldroitsutils.common.CktlCallEOUtilities;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEOFacturePapier;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEOPrestation;
import org.cocktail.fwkcktlformco.serveur.finder.FinderRepartPartenaireTranche;
import org.cocktail.fwkcktlformco.serveur.utils.FCUtils;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.kava.server.metier.EOConvention;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOPrestation;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

public class EOContratFCService {

	/**
	 * Creer un contrat de FC à la validation d'un devis. l'envoyer par mail a
	 * celui qui a passé commande. relier le contrat à la prestation.
	 * 
	 * @param ec
	 * @param persIdCreateur
	 * @param etab
	 * @param service
	 * @param exercice
	 * @return
	 * @throws FormcoException
	 */
	public static Contrat creerContratFC(EOEditingContext ec,
			int persIdCreateur, EOStructure etab, EOStructure service,
			TypeContrat typeContrat, TypeClassificationContrat typeClassCtrt,
			EOPrestation prestation) throws FormcoException {
		if ((ec == null) || (persIdCreateur == 0) || (etab == null)
				|| (service == null) || (typeContrat == null)
				|| (typeClassCtrt == null) || (prestation == null)) {
			throw new FormcoException("Creation convention FC impossible");
		}
		FactoryConvention factory = new FactoryConvention(ec, true);
		EOUtilisateur createur = EOUtilisateur.fetchFirstByQualifier(ec,
				new EOKeyValueQualifier(EOUtilisateur.PERS_ID_KEY,
						EOKeyValueQualifier.QualifierOperatorEqual,
						persIdCreateur));

		// EOUtilisateur createur = (EOUtilisateur) EOUtilities
		// .objectMatchingKeyAndValue(ec, EOUtilisateur.ENTITY_NAME,
		// EOUtilisateur.PERS_ID_KEY, persIdCreateur);
		if (createur == null) {
			throw new FormcoException(
					"Impossible de recuperer le createur a partir du persid:"
							+ persIdCreateur);
		}
		Contrat contratFC;
		try {
			contratFC = factory.creerConventionVierge(createur, etab, service);

		} catch (ExceptionUtilisateur e) {
			e.printStackTrace();
			throw new FormcoException(e.getMessage());
		} catch (Exception e) {
			throw new FormcoException(e.getMessage());
		}

		contratFC.setTypeClassificationContratRelationship(typeClassCtrt);
		contratFC.setTypeContratRelationship(typeContrat);

		int exerciceId = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(prestation.exercice()));
		EOExerciceCocktail exeCktl = (EOExerciceCocktail) EOUtilities
				.objectMatchingKeyAndValue(ec, EOExerciceCocktail.ENTITY_NAME,
						EOExerciceCocktail.EXE_ORDRE_KEY, exerciceId);
		if (exeCktl == null) {
			throw new FormcoException(
					"Creation du contrat de formation continue impossible, l'exercice de la prestation est introuvable.");
		}
		contratFC.setExerciceCocktailRelationship(exeCktl);

		int organId = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(prestation.organ()));
		EOOrgan organ = (EOOrgan) EOUtilities.objectMatchingKeyAndValue(ec,
				EOOrgan.ENTITY_NAME, EOOrgan.ORG_ID_KEY, organId);
		if (organ == null) {
			throw new FormcoException(
					"Creation du contrat de formation continue impossible, le compte budgétaire de la prestation est introuvable.");
		}
		contratFC.setOrganComposanteRelationship(organ);
		contratFC.setConDateCreation(new NSTimestamp());
		contratFC.avenantZero().setAvtMontantHt(prestation.prestTotalHt());
		contratFC.setConObjet(prestation.prestLibelle());
		// pas la peine de setter le ttc car il est calcule au set du HT
		// contratFC.avenantZero().setAvtMontantTtc(prestation.prestTotalTtc());
//		contratFC.addToPrestations(prestation);
		contratFC.addToPrestationsRelationship(prestation);
//		System.out.println("ATTENTION EOCONTRATFCService.java.creerContratFC on ajoute la prestation au contrat et non l'inverse. Verifier que Prestation.ConOrdre est renseigné.");
		/**
		 * TESTER L utilisation de FacturePapier comme table de repart entre
		 * prestation et contrat
		 */
		// EORepartContratPrestation repConPres = EORepartContratPrestation
		// .creerNouveauContratPrestation(ec, contratFC, prestation);
		// EOEtatContrat etatContrat =
		// (EOEtatContrat)EOUtilities.objectMatchingKeyAndValue(ec,
		// EOEtatContrat.ENTITY_NAME, EOEtatContrat.CODE_ETAT_CONTRAT_KEY,
		// EOEtatContrat.BROUILLON);
		// repConPres.setToEtatContratRelationship(etatContrat);
		// repConPres.setRcpTotalHT(prestation.prestTotalHt());
		// repConPres.setRcpTotalTTC(prestation.prestTotalTtc());
		// repConPres.setRcpTotalTVA(prestation.prestTotalTva());

		return contratFC;
	}

	public static ContratPartenaire ajouterPayeurContrat(EOEditingContext ec,
			Contrat contratFC, IPersonne client, BigDecimal montant)
			throws FormcoException {
		if ((ec == null) || (contratFC == null) || (client == null)
				|| (montant == null)) {
			throw new FormcoException("Ajout partenaire impossible");
		}
		if (montant.compareTo(contratFC.avenantZero().avtMontantTtc()) == 1) {
			throw new FormcoException(
					"Impossible d'apporter une participation supérieure au montant du contrat");
		}

		FactoryConvention factory = new FactoryConvention(ec, true);
		ContratPartenaire partenaire = null;

		try {
			/*
			 * Comme il peut y avoir plusieurs tranches dans un contrat ACCORD
			 * on ajoute un partenaire avec un partenaire.cpMontant a 0. La
			 * participation du partenaire est indiquee ds les tranches du
			 * contrat. Dans le cas de la FC,on indique le montant dans la
			 * tranche de l'exercice en court.
			 */
			partenaire = factory.ajouterPartenaire(contratFC, client, null,
					Boolean.FALSE, BigDecimal.ZERO);
			// Recuperer la tranche relative au partenaire
			NSArray<Tranche> tranches = contratFC.tranches();
			// if (tranches.count() > 1) {
			// throw new FormcoException(
			// "Il ne peut pas y avoir plus d'une tranche pour un contrat de formation continue");
			// }

			/*
			 * Rechercher les repartPartenaireTranche correspondant a l'exercice
			 * en cours. Fait en objet car si c'est appelé a la creation, on ne
			 * peut pas requeter en bdd
			 */
			EOExercice currentExercice = null;
			int exeOrdre = 0;
			NSArray<EOExercice> tmp = EOExercice.fetchAll(ec,
					EOExercice.EXE_STAT_KEY, "O", new NSArray<EOSortOrdering>(
							new EOSortOrdering(EOExercice.EXE_EXERCICE_KEY,
									EOSortOrdering.CompareAscending)));
			if ((tmp != null) && (tmp.size() > 0)) {
				currentExercice = tmp.lastObject();
			}
			if (currentExercice == null)
				throw new FormcoException(
						"ajouterPayeurContrat: Impossible de recuperer l'exercice en court");

			if (currentExercice != null) {
				exeOrdre = Integer.parseInt(ERXEOControlUtilities
						.primaryKeyStringForObject(currentExercice));
				// exeCktl = (EOExerciceCocktail) EOUtilities
				// .objectMatchingKeyAndValue(ec,
				// EOExerciceCocktail.ENTITY_NAME,
				// EOExerciceCocktail.EXE_ORDRE_KEY, exeOrdre);
			}
			Enumeration<Tranche> enumTranche = tranches.objectEnumerator();
			Tranche laTranche = null;
			while (enumTranche.hasMoreElements()) {
				Tranche uneTranche = (Tranche) enumTranche.nextElement();
				if (laTranche == null) {
					EOExerciceCocktail anExeCktl = uneTranche
							.exerciceCocktail();
					int anExeOrdre = Integer.parseInt(ERXEOControlUtilities
							.primaryKeyStringForObject(anExeCktl));
					if (anExeOrdre == exeOrdre) {
						laTranche = uneTranche;
					}
				}
			}
			if (laTranche == null) {
				System.out.println("ajouterPayeurContrat: Impossible de recuperer la tranche correspondant à l'exercice en court");
			}
			//Rechercher le prochain exercice
			exeOrdre++;
			Enumeration<Tranche> enumTrancheSuivante = tranches.objectEnumerator();
			Tranche laTrancheSuivante = null;
			while (enumTrancheSuivante.hasMoreElements()) {
				Tranche uneTranche = (Tranche) enumTrancheSuivante.nextElement();
				if (laTrancheSuivante == null) {
					EOExerciceCocktail anExeCktl = uneTranche
							.exerciceCocktail();
					int anExeOrdre = Integer.parseInt(ERXEOControlUtilities
							.primaryKeyStringForObject(anExeCktl));
					if (anExeOrdre == exeOrdre) {
						laTranche = uneTranche;
					}
				}
			}
			if (laTranche == null) {
				throw new FormcoException(
						"AjouterPayeurContrat: Impossible de recuperer la tranche correspondant à l'exercice suivant l'exercice en court.");
			}
			// int idLaTranche = Integer.parseInt(ERXEOControlUtilities
			// .primaryKeyStringForObject(laTranche));
			NSArray<RepartPartenaireTranche> rpt = laTranche
					.toRepartPartenaireTranches();
			Enumeration<RepartPartenaireTranche> enumRPT = rpt
					.objectEnumerator();
			// La participation est indiquée pour la tranche de l'exercice
			// en court, pour le client de la FC
			while (enumRPT.hasMoreElements()) {
				RepartPartenaireTranche aRPT = enumRPT.nextElement();
				// int idTranche = Integer.parseInt(ERXEOControlUtilities
				// .primaryKeyStringForObject(aRPT.tranche()));
				// if (idTranche == idLaTranche) {
				int exeTranche = Integer.parseInt(ERXEOControlUtilities
						.primaryKeyStringForObject(aRPT.tranche()
								.exerciceCocktail()));
				if ((exeTranche == exeOrdre)
						&& (aRPT.contratPartenaire().persId() == client
								.persId())) {
					aRPT.setRptMontantParticipation(montant);
				}
				// }
			}
			// NSArray<RepartPartenaireTranche> repPTs =
			// FinderRepartPartenaireTranche.getRPTFromContratAndPartenaire(ec,contratFC,partenaire);
			// if(repPTs.count()!=1 )
			// {
			// throw new
			// FormcoException("Il ne peut pas y avoir plusieurs tranches pour un meme partenaire ds un contrat de Formation continue");
			// }
			// RepartPartenaireTranche laRepPT = repPTs.get(0);
			// laRepPT.setRptMontantParticipation(montant);
			// BigDecimal tauxParticipation = BigDecimal.ZERO;
			// tauxParticipation =montant.multiply(new BigDecimal(100));
			// tauxParticipation =
			// tauxParticipation.divide(laRepPT.tranche().traMontant());
			// tauxParticipation.round(new MathContext(0,
			// RoundingMode.HALF_UP));
		} catch (ExceptionUtilisateur e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return partenaire;
	}

	/**
	 * Valider un contrat de FC
	 * 
	 * @param ec
	 * @param contratFC
	 * @return
	 * @throws FormcoException
	 */
	public static Contrat validerContratFC(EOEditingContext ec,
			Contrat contratFC) throws FormcoException {
		if (ec == null || contratFC == null)
			throw new FormcoException(
					"Validation contrat FC impossible: EC ou contrat Null.");

		contratFC.setConDateValidAdm(new NSTimestamp());
		contratFC.avenantZero().setAvtDateValidAdm(new NSTimestamp());
		try {
			ec.saveChanges();
		} catch (Exception e) {
			ec.revert();
			throw new FormcoException("Validation contrat FC impossible: "
					+ e.getMessage());
		}
		return contratFC;

	}

	/**
	 * Signer le contrat
	 * 
	 * @param ec
	 * @param contratFC
	 * @return
	 * @throws FormcoException
	 */
	public static Contrat signerContratFC(EOEditingContext ec, Contrat contratFC)
			throws FormcoException {
		if (ec == null || contratFC == null)
			throw new FormcoException(
					"Validation contrat FC impossible: EC ou contrat Null.");

		contratFC.avenantZero().setAvtDateSignature(new NSTimestamp());
		try {
			ec.saveChanges();
		} catch (Exception e) {
			ec.revert();
			throw new FormcoException("Validation contrat FC impossible: "
					+ e.getMessage());
		}
		return contratFC;

	}

	/**
	 * Supprimer un contrat et les entités liées lors d'une erreur survenue au
	 * moment de la création d'un contrat de formation continue.
	 * 
	 * @param ec
	 * @param contratFC
	 * @throws FormcoException
	 */
	public static void supprimerContrat(EOEditingContext ec, Contrat contratFC)
			throws FormcoException {
		if (ec == null || contratFC == null) {
			throw new FormcoException(
					"Facture papier a supprimer ou utilisateur a null!!");
		}
		try {
			Avenant avenantZero = contratFC.avenantZero();
			EOStructure groupe = contratFC.groupePartenaire();
			EOPersonne personne = (EOPersonne) EOUtilities
					.objectMatchingKeyAndValue(ec, EOPersonne.ENTITY_NAME,
							EOPersonne.PERS_ID_KEY, groupe.persId());

			// Supprimer les liens avec la structure correspondant au groupe de
			// partenaires cree pour le contrat
			NSArray<EORepartStructure> repartStructures = EOUtilities
					.objectsMatchingKeyAndValue(ec,
							EORepartStructure.ENTITY_NAME,
							EORepartStructure.TO_STRUCTURE_GROUPE_KEY, groupe);
			Enumeration<EORepartStructure> enumRepStr = repartStructures
					.objectEnumerator();
			while (enumRepStr.hasMoreElements()) {
				EORepartStructure repartStructure = (EORepartStructure) enumRepStr
						.nextElement();
				ec.deleteObject(repartStructure);
			}
			ec.saveChanges();

			NSArray<EORepartStructure> repartStructuresPersonne = EOUtilities
					.objectsMatchingKeyAndValue(ec,
							EORepartStructure.ENTITY_NAME,
							EORepartStructure.PERS_ID_KEY, groupe.persId());
			Enumeration<EORepartStructure> enumRepStrPersonne = repartStructuresPersonne
					.objectEnumerator();
			while (enumRepStrPersonne.hasMoreElements()) {
				EORepartStructure repStrPers = (EORepartStructure) enumRepStrPersonne
						.nextElement();
				ec.deleteObject(repStrPers);
			}

			ec.saveChanges();

			// Supprimer les repart avec le projet nommé Contrats
			NSArray<ProjetContrat> projetContrats = contratFC.projetContrat();
			Enumeration<ProjetContrat> enumProCon = projetContrats
					.objectEnumerator();
			while (enumProCon.hasMoreElements()) {
				ProjetContrat projetContrat = (ProjetContrat) enumProCon
						.nextElement();
				ec.deleteObject(projetContrat);
			}

			ec.saveChanges();

			// Supprimer les RepartAssociation correspondant au groupe de
			// partenaires
			NSArray<EORepartAssociation> repartAssociations = EOUtilities
					.objectsMatchingKeyAndValue(ec,
							EORepartAssociation.ENTITY_NAME,
							EORepartAssociation.C_STRUCTURE_KEY,
							groupe.cStructure());
			Enumeration<EORepartAssociation> enumRepAss = repartAssociations
					.objectEnumerator();
			while (enumRepAss.hasMoreElements()) {
				EORepartAssociation repartAssociation = (EORepartAssociation) enumRepAss
						.nextElement();
				ec.deleteObject(repartAssociation);
			}

			ec.saveChanges();

			// Supprimer les RepartTypeGroupe correspondand au groupe de
			// partenaire
			NSArray<EORepartTypeGroupe> repartTypeGroupes = EOUtilities
					.objectsMatchingKeyAndValue(ec,
							EORepartTypeGroupe.ENTITY_NAME,
							EORepartTypeGroupe.C_STRUCTURE_KEY,
							groupe.cStructure());
			Enumeration<EORepartTypeGroupe> enumRepTypGr = repartTypeGroupes
					.objectEnumerator();
			while (enumRepTypGr.hasMoreElements()) {
				EORepartTypeGroupe repartTypeGroupe = (EORepartTypeGroupe) enumRepTypGr
						.nextElement();
				ec.deleteObject(repartTypeGroupe);
			}

			ec.saveChanges();

			// Supprimer les tranches
			NSArray<Tranche> tranches = contratFC.tranches();
			Enumeration<Tranche> enumTranche = tranches.objectEnumerator();
			while (enumTranche.hasMoreElements()) {
				Tranche tranche = (Tranche) enumTranche.nextElement();
				// factory.supprimerTranche ne supprime pas la tranche de la
				// bd
				// factory.supprimerTranche(tranche);
				// RepartPartenaireTranche est supprimé en cascade
				ec.deleteObject(tranche);
			}

			ec.saveChanges();

			// Supprimer les partenaires non principaux
			NSArray<ContratPartenaire> partenaires = contratFC
					.partenairesNonPrincipaux();
			Enumeration<ContratPartenaire> enumConPar = partenaires
					.objectEnumerator();

			while (enumConPar.hasMoreElements()) {
				ContratPartenaire partenaire = (ContratPartenaire) enumConPar
						.nextElement();
				ec.deleteObject(partenaire);
			}

			// Supprimer les partenaires
			ContratPartenaire partPrincipal = contratFC.partenairePrincipal();
			ec.deleteObject(partPrincipal);

			ec.saveChanges();

			// Supprimer le contrat, l'avenant0
			ec.deleteObject(avenantZero);
			ec.deleteObject(contratFC);
			ec.saveChanges();
			// Appel de la procédure stockée Detruire Structure
			// pour supprimer le groupe partenaire
			EOContratFCService.delStructure(ec, groupe);

		} catch (Exception e) {
			e.printStackTrace();
			throw new FormcoException(e);
		}

	}

	/**
	 * Supprimer un partenariat avec un montant de 0
	 * 
	 * @param ec
	 * @param partenaire
	 * @throws Exception
	 */
	public static void supprimerPartenariat(EOEditingContext ec,
			ContratPartenaire partenaire) throws Exception {
		if (ec == null || partenaire == null)
			throw new FormcoException(
					"Suppression contratPartenaire impossible");
		if (partenaire.contrat().partenairePrincipal().persId() == partenaire
				.persId()) {
			throw new FormcoException(
					"Suppression du partenaire impossible, c'est le partenaire principal");
		}

		// supprimer les RepartPartenaireTranche
		NSArray<RepartPartenaireTranche> rpts = EOUtilities
				.objectsMatchingKeyAndValue(ec,
						RepartPartenaireTranche.ENTITY_NAME,
						RepartPartenaireTranche.CONTRAT_PARTENAIRE_KEY,
						partenaire);
		Enumeration<RepartPartenaireTranche> enumRpts = rpts.objectEnumerator();
		NSArray<RepartPartenaireTranche> newRpts = new NSArray<RepartPartenaireTranche>();
		while (enumRpts.hasMoreElements()) {
			RepartPartenaireTranche rpt = (RepartPartenaireTranche) enumRpts
					.nextElement();
			Tranche tr = rpt.tranche();
			tr.removeFromToRepartPartenaireTranches(rpt);
			if (rpt.rptMontantParticipation().compareTo(
					new BigDecimal(BigInteger.ZERO)) != 0) {
				throw new FormcoException(
						"Suppression du partenariat impossible, le montant de participation du partenaire n'est pas nul");
			}
			ec.deleteObject(rpt);
		}
		Contrat contrat = partenaire.contrat();
		contrat.removeFromContratPartenaires(partenaire);
		ec.deleteObject(partenaire);

	}

	public static void delStructure(EOEditingContext ec, EOStructure structure)
			throws Exception {
		if (ec == null || structure == null) {
			throw new Exception("Structure a supprimer a null!!");
		}
		CktlDataBus databus = new CktlDataBus(ec);
		try {
			String exp = "ALTER TRIGGER GRHUM.LOG_DELETE_PERSONNE DISABLE";
			databus.executeSQLQuery(exp, NSArray.EmptyArray);
			NSMutableDictionary<String, Integer> procStockParams = new NSMutableDictionary<String, Integer>();
			procStockParams.put("persid", structure.persId());
			CktlCallEOUtilities.executeStoredProcedureNamed(ec,
					"FwkPers_prcDetruirePersonne", procStockParams);
			exp = "ALTER TRIGGER GRHUM.LOG_DELETE_PERSONNE ENABLE";
			databus.executeSQLQuery(exp, NSArray.EmptyArray);
		} catch (Throwable e) {
			databus.rollbackTransaction();
			throw new Exception(e);
		}
	}

	/**
	 * Modifier les partenariats d'un contrat Modifier les sommes des
	 * repartContratPartenaire. Modifier les somme des facturePapier
	 * 
	 * @param ec
	 * @param contratFC
	 * @return
	 * @throws FormcoException
	 */
	public static Contrat modifierPartenariats(EOEditingContext ec,
			Contrat contratFC) throws FormcoException {
		if (ec == null || contratFC == null) {
			throw new FormcoException(
					"Modification des partenariats impossible: Aucun Databus, EC ou contrat.");
		}

		// recuperer l'exercice en court
		EOExercice currentExercice = null;
		NSArray<EOExercice> tmp = EOExercice.fetchAll(ec,
				EOExercice.EXE_STAT_KEY, "O", new NSArray<EOSortOrdering>(
						new EOSortOrdering(EOExercice.EXE_EXERCICE_KEY,
								EOSortOrdering.CompareAscending)));
		if ((tmp != null) && (tmp.size() > 0)) {
			currentExercice = tmp.lastObject();
		}
		int currentExeOrdre = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(currentExercice));

		// Recuperer ts les partenariats pour ce contrat
		NSArray<RepartPartenaireTranche> rpts = FinderRepartPartenaireTranche
				.getRPTFromContrat(ec, contratFC);
		Enumeration<RepartPartenaireTranche> enumRpts = rpts.objectEnumerator();
		BigDecimal sommeParticipation = new BigDecimal(BigInteger.ZERO);
		while (enumRpts.hasMoreElements()) {
			RepartPartenaireTranche rpt = (RepartPartenaireTranche) enumRpts
					.nextElement();
			sommeParticipation = sommeParticipation.add(rpt
					.rptMontantParticipation());
		}
		// Verifier que la somme des participations est egale au montant de la
		// prestation
		if (!sommeParticipation.equals(FinderEOPrestation.getMontantPrestation(
				ec, contratFC))) {
			throw new FormcoException(
					"La somme des partenariats n'est pas égale au montant total");
		}

		Enumeration<RepartPartenaireTranche> enumRpts2 = rpts.objectEnumerator();
		while (enumRpts2.hasMoreElements()) {
			RepartPartenaireTranche rpt = (RepartPartenaireTranche) enumRpts2
					.nextElement();
			// Si le montant de la participation >= 0 et qu'on est ds la tranche
			// de l'exercice en court pour un partenaire non principal
			if ((rpt.rptMontantParticipation().compareTo(
					new BigDecimal(BigInteger.ZERO)) > -1)
					&& (rpt.tranche().exerciceCocktail().exeExercice() == currentExeOrdre)
					&& (!rpt.contratPartenaire().cpPrincipalBoolean())) {
				// recuperer les factures pour ce contrat et ce partenaire
				NSArray<EOFacturePapier> faps = FinderEOFacturePapier
						.getFacturesFromClientAndContrat(ec, contratFC, rpt
								.contratPartenaire().persId());
				// un partenaire ne peut pas avoir plus d'une facture pour une
				// tranche et un contrat
				if (faps.count() > 1) {
					int rptId = Integer.parseInt(ERXEOControlUtilities
							.primaryKeyStringForObject(rpt));
					throw new FormcoException(
							"Un partenaire ne peut pas avoir plus d'une facture pour un contrat et une tranche donnée. (rpt:"
									+ rptId + ")");
				}
				if (faps.count() == 1) {
					// Enumeration<EOFacturePapier> enumFap = faps
					// .objectEnumerator();
					// while (enumFap.hasMoreElements()) {
					// EOFacturePapier fap = enumFap.nextElement();
					EOFacturePapier fap = faps.get(0);
					if (fap != null) {
						if (!fap.fapTotalHt().equals(
								rpt.rptMontantParticipation())) {
							// Les montants ont été modifés
							if (fap.facture() != null) {
								// la facture est recettée: impossible de
								// modifier
								throw new FormcoException(
										"Impossible de modifier cette facture. Elle est recettée");
							} else {
								fap.setFapTotalHt(rpt.rptMontantParticipation());
								fap.setFapTotalTtc(rpt
										.rptMontantParticipation());
								rpt.contratPartenaire().setCpMontant(
										rpt.rptMontantParticipation());
								// contratFC.avenantZero().setAvtMontantHt(
								// rpt.rptMontantParticipation());
								// contratFC.avenantZero().setAvtMontantTtc(
								// rpt.rptMontantParticipation());
							}
							if (fap.fapTotalHt().compareTo(
									new BigDecimal(BigInteger.ZERO)) == 0) {
								// la facture a un montant à 0, on supprime
								int utlOrdre = Integer
										.parseInt(ERXEOControlUtilities
												.primaryKeyStringForObject(fap));
								EOFacturePapierService.delFacturePapier(ec,
										fap.fapId(), utlOrdre);
							}
						}
					}
				} else {
					// creer une nvelle facture pour ce partenaire.
					EOPrestation prestation = FinderEOPrestation
							.getPrestationFromContrat(ec, rpt
									.contratPartenaire().contrat());
//					EOFacturePapier newFacture = EOFacturePapierService
//							.genererFacturePapier(ec, prestation, prestation
//									.utilisateur().getPkUtlOrdre(), true);
					EOFacturePapier newFacture = EOFacturePapierService
					.genererFacturePapier(ec, prestation,  true);
//					newFacture.setFapLib("[FORMATION-CONTINUE] "
//							+ newFacture.fapLib());
					newFacture.setFapTotalHt(rpt.rptMontantParticipation());
					newFacture.setFapTotalTtc(rpt.rptMontantParticipation());
					rpt.contratPartenaire().setCpMontant(
							rpt.rptMontantParticipation());
					// Relier la facture au client qui paie la prestation
					org.cocktail.kava.server.metier.EOPersonne clientPartenaire = (org.cocktail.kava.server.metier.EOPersonne) EOUtilities
							.objectMatchingKeyAndValue(
									ec,
									org.cocktail.kava.server.metier.EOPersonne.ENTITY_NAME,
									org.cocktail.kava.server.metier.EOPersonne.PERS_ID_KEY,
									rpt.contratPartenaire().persId());
					newFacture.setPersonne(clientPartenaire);
					Contrat contrat = rpt.contratPartenaire().contrat();
					EOConvention conv = FCUtils.getContratPieFromContratAccord(ec, contrat);
//					newFacture
//							.setToCWContrat(rpt.contratPartenaire().contrat());
					newFacture.setConvention(conv);
				}
			}
		}
		try {
			ec.saveChanges();
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
			throw new FormcoException("Modification partenariat impossible "
					+ e.getMessage());
		}

		return contratFC;
	}
}
