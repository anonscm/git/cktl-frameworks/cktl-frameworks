/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import java.math.BigDecimal;

import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;
/**
 * Classe abstraite de creation d'une EOScolMaquetteRepartitionUE pour la formation continue
 * @author jb
 *
 */
public abstract class EOScolMaquetteRepartUeService {
	 /**
     * Creer une nouvelle RepartUE pour la formation continue.
     * Semestre gere par ihm et maquetteUE gere par le service.
     * Les autres attributs sont initialises ici.
     * @param ec
     * @return
     * @throws NSValidation.ValidationException
     */
    public static EOScolMaquetteRepartitionUe creerNouvelleScolMaquetteRepartUEForFC(EOEditingContext ec, EOScolFormationAnnee annee) throws NSValidation.ValidationException
    {
    	if(ec==null)return null;
    	if(annee ==null)
    		annee=FinderScolFormationAnnee.getScolFormationAnneeCourante(ec);
    	EOScolMaquetteRepartitionUe repartUE = EOScolMaquetteRepartitionUe.create(ec);
    	//bonifiable
    	repartUE.setMrueBonifiable(new Integer(0));
    	//coefficient
    	repartUE.setMrueCoefficient(new BigDecimal(1));
    	//comptabilisable
    	repartUE.setMrueComptabilisable(new Integer(1));
    	//noteBase
    	repartUE.setMrueNoteBase(new BigDecimal(20));
    	//noteElimination
    	repartUE.setMrueNoteElimination(null);
    	//noteObtention
    	repartUE.setMrueNoteObtention(new BigDecimal(10));
    	//Ordre
    	repartUE.setMrueOrdre(new Integer(1));
    	//annee
    	repartUE.setToFwkScolarite_ScolFormationAnneeRelationship(annee);
    	//semestre gere par ihm
    	//maquetteUE gere par le service
    	return repartUE; 
    }
}
