/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import org.cocktail.fwkcktldroitsutils.common.CktlCallEOUtilities;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEORepartPersonneAdresse;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.metier.AFinder;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.kava.server.metier.EOBoutique;
import org.cocktail.kava.server.metier.EOFournisUlr;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Classe Service utilisee pour initialiser les valeurs utilisees par le
 * formulaire de création d'un client.
 * 
 * @author jb
 * 
 */
public abstract class ClientService {

	/**
	 * Instancier et initialiser un client
	 * 
	 * @param ec
	 * @param boutique
	 * @return
	 * @throws FormcoException
	 */
	public static EOIndividu creerNouveauClient(EOEditingContext ec,
			EOBoutique boutique) throws FormcoException {

		EOIndividu client = EOIndividu.creerInstance(ec);
		NSTimestamp now = new NSTimestamp();
		EOTypeTel typeTelPrf = EOTypeTel.fetchByKeyValue(ec,
				EOTypeTel.C_TYPE_TEL_KEY, EOTypeTel.C_TYPE_TEL_PRF);
		EOTypeNoTel typeNoTel = EOTypeNoTel.fetchByKeyValue(ec,
				EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL);
		EOPaysIndicatif defaultIndicatif = EOPaysIndicatif.fetchByKeyValue(ec,
				EOPaysIndicatif.C_PAYS_KEY, "404"); // états-unis d'amérique

		try {

			// Creation de l'adresse
			EOAdresse adresse = EOAdresse.creerInstance(ec);
			adresse.setPersIdCreation(boutique.jefyAdminUtilisateur().persId());
			adresse.setPersIdModification(boutique.jefyAdminUtilisateur()
					.persId());
			adresse.setDCreation(now);
			adresse.setDModification(now);

			// creation du client.
			EOFournis fournis = EOFournis.creerInstance(ec);
			client.setNoIndividuCreateur(boutique.jefyAdminUtilisateur()
					.noIndividu());
			client.setPersIdModification(boutique.jefyAdminUtilisateur()
					.persId());
			// creation du fournisseur
			// fournis.setToPersonne(client);
			fournis.setToIndividu(client);

			fournis.initialise(boutique.jefyAdminUtilisateur().persId(),
					adresse, true, EOFournis.FOU_TYPE_CLIENT);
			fournis.toValideFournis().setValCreation(
					boutique.jefyAdminUtilisateur().getPkUtlOrdre());
			fournis.toValideFournis().setValValidation(
					boutique.jefyAdminUtilisateur().getPkUtlOrdre());
			fournis.setFouValide("O");

			// Liaison d'adresse
			EORepartPersonneAdresse repartPersonneAdresse = EORepartPersonneAdresse
					.creerInstance(ec);
			EOTypeAdresse typeAdresse = EOTypeAdresse.fetchByQualifier(ec,
					EOTypeAdresse.QUAL_TADR_CODE_FACT);
			repartPersonneAdresse.initForPersonne(ec, client, adresse,
					typeAdresse);

			repartPersonneAdresse.setDCreation(now);
			repartPersonneAdresse.setDModification(now);
			repartPersonneAdresse
					.setRpaPrincipal(EORepartPersonneAdresse.RPA_PRINCIPAL_OUI);
			repartPersonneAdresse
					.setRpaValide(EORepartPersonneAdresse.RPA_VALIDE_OUI);

			// Creation du téléphone
			EOPersonneTelephone personneTelephone = EOPersonneTelephone
					.creerInstance(ec);
			personneTelephone.setToTypeTelRelationship(typeTelPrf);
			personneTelephone.setToTypeNoTelRelationship(typeNoTel);
			personneTelephone.setTelPrincipal(EOPersonneTelephone.OUI);
			personneTelephone.setDCreation(now);
			personneTelephone.setDModification(now);
			personneTelephone.setToPersonneRelationship(client);
			personneTelephone.setIndicatif(defaultIndicatif.indicatif());

			// // Creation d'un compte
			// EOCompte compte = EOCompte.creerInstance(ec);
			// fournis.setToCompteRelationship(compte);
			// EOCompteEmail compteEmail = EOCompteEmail.creerInstance(ec);
			// compteEmail.setDCreation(now);
			// compteEmail.setDModification(now);
			// compteEmail.setToCompteRelationship(compte);

		} catch (Exception e) {
			throw new FormcoException(e.getMessage());
		}
		return client;
	}

	/**
	 * Verifie et prépare l'insertion du client en BD
	 * 
	 * @param ec
	 * @param client
	 * @param boutique
	 * @throws FormcoException
	 */
	public static void getClePrimaireNouveauClient(EOEditingContext ec,
			EOIndividu client, String email) throws FormcoException {
		if ((client == null) || (ec == null))
			throw new FormcoException(
					"Impossible de stocker le client, informations manquantes (EditingContext, Client, Boutique Null");
		// Verifier les infos saisies
		checkClient(ec, client, email);

		// Si tout est ok, creer le persId
		Integer persId = AFinder.construirePersId(ec);
		Integer noIndividu = AFinder.clePrimairePour(ec,
				EOIndividu.ENTITY_NAME, EOIndividu.NO_INDIVIDU_KEY,
				EOIndividu.SEQ_INDIVIDU_ENTITY_NAME, true);
		client.setNoIndividu(noIndividu);

		// initialiser l'email
		NSArray<String> compEmail = NSArray.componentsSeparatedByString(email,
				"@");

		if (compEmail.count() == 2) {
			// Creation d'un compte
			NSTimestamp now = new NSTimestamp();
			EOCompte compte = EOCompte.compteForPersId(ec, persId);
			if (compte == null) {
				try {
					compte = client.toFournis().associerCompte(client.persId());
				} catch (Exception e) {
					throw new FormcoException(e.getMessage());
				}
			}

			EOCompteEmail compteEmail = EOCompteEmail.creerInstance(ec);
			compteEmail.setDCreation(now);
			compteEmail.setDModification(now);
			compteEmail.setToCompteRelationship(compte);
			// compteEmail = client.toFournis().toCompte()
			// .toCompteEmail();
			compteEmail.setCemEmail((String) compEmail.objectAtIndex(0));
			compteEmail.setCemDomaine((String) compEmail.objectAtIndex(1));
		}

	}

	/**
	 * Verifie les informations du client
	 * 
	 * @param client
	 * @throws FormcoException
	 */
	public static void checkClient(EOEditingContext ec, EOIndividu client,
			String email) throws FormcoException {

		if (client == null)
			throw new FormcoException(
					"Impossible de stocker le client, client Null");

		EOTypeAdresse typeAdresse = EOTypeAdresse.fetchByQualifier(ec,
				EOTypeAdresse.QUAL_TADR_CODE_FACT);
		String errorMessage = "";
		if (client.toCivilite() == null) {
			errorMessage += "Civilité, ";
		}
		if (client.nomUsuel() == null) {
			errorMessage += "Nom, ";
		}
		if (client.prenom() == null) {
			errorMessage += "Prénom, ";
		}
		if (client.dNaissance() == null) {
			errorMessage += "Date de naissance, ";
		}
		if (client.indOrigine() == null) {
			errorMessage += "Affilitation, Etablissement,  ";
		}
		if (email == null) {
			errorMessage += "Email,  ";
		}
		// Recuperer l'adresse de facturation du client

		IAdresse adresseFact = client.toFournis().toAdresse();
		if (adresseFact == null) {
			errorMessage += "Adresse de facturation,  ";
		} else {
			if (adresseFact.adrAdresse1() == null) {
				errorMessage += "Adresse,  ";
			}
			// (si pays nul et CP nul) OU (si pays non nul et France et code
			// postal
			// nul)
			if (((adresseFact.toPays() == null) && (adresseFact.codePostal() == null))
					|| ((adresseFact.toPays() != null && adresseFact.toPays()
							.isPaysDefaut()) && adresseFact.codePostal() == null)) {
				errorMessage += "CP,  ";
			}
			if (adresseFact.ville() == null) {
				errorMessage += "Ville,  ";
			}
		}
		if (!EOIndividu.individusWithNameEqualsAndFirstNameEquals(ec,
				client.nomUsuel(), client.prenom(), null, 10).isEmpty())
			throw new FormcoException("Le client est deja enregistré");
		// if (!errorMessage.isEmpty()) {
		if (errorMessage.length() > 0) {
			errorMessage += " manquant ou erroné. ";
			throw new FormcoException(errorMessage);
		}
	}

	public static EOIndividu creerFournisseur(EOEditingContext ec,
			EOIndividu client, EOBoutique boutique, EOUtilisateur createurFC)
			throws FormcoException {
		if (ec == null)
			return null;
		// Recuperation de l'individu connecté
		if (client == null)
			return null;
		if (createurFC == null)
			return null;
		int persIdCreateur = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(createurFC.personne()));
		int utlOrdreCreateur = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(createurFC));
		if (persIdCreateur == 0 || utlOrdreCreateur == 0)
			throw new FormcoException(
					"Impossible de trouver le createur du fournisseur");
		// creation du fournisseur
		EOFournis fournis = EOFournis.creerInstance(ec);
		NSTimestamp now = new NSTimestamp();
		fournis.setToIndividu(client);

		// EOQualifier qualAdressePrincipale = EOQualifier
		// .qualifierWithQualifierFormat(
		// EORepartPersonneAdresse.RPA_PRINCIPAL_KEY + "=%@ AND "
		// + EORepartPersonneAdresse.PERS_ID_KEY + "=%@",
		// new NSArray(new Object[] {
		// EORepartPersonneAdresse.RPA_PRINCIPAL_OUI,
		// client.persId() }));
		// EORepartPersonneAdresse repartadressePrincipale =
		// EORepartPersonneAdresse
		// .fetchFirstByQualifier(getEditingContext(),
		// qualAdressePrincipale);
		Integer persId = client.persId();
		NSArray<EORepartPersonneAdresse> tabRepartAdresse = FinderEORepartPersonneAdresse
				.getRepPersAdr(ec, persId);
		EORepartPersonneAdresse repartAdressePrincipale = (EORepartPersonneAdresse) tabRepartAdresse
				.get(0);
		Integer persIdCreationAdr = repartAdressePrincipale.toAdresse()
				.persIdCreation();
		if (persIdCreationAdr == null) {
			repartAdressePrincipale.toAdresse().setPersIdCreation(
					persIdCreateur);
			repartAdressePrincipale.toAdresse().setPersIdModification(
					persIdCreateur);
		}
		if (repartAdressePrincipale == null) {
			CktlLog.log("Commande impossible, Aucune adresse principale n'est definie pour l'utilisateur.");
			return null;
		}
		try {

			// Creation de la repartAdresse pour le client
			// Liaison Adresse Client
			EORepartPersonneAdresse repartPersonneAdresse = EORepartPersonneAdresse
					.creerInstance(ec);
			EOTypeAdresse typeAdresse = EOTypeAdresse.fetchByQualifier(ec,
					EOTypeAdresse.QUAL_TADR_CODE_FACT);
			// repartPersonneAdresse.initForPersonne(ec, fournis.toIndividu(),
			// repartAdressePrincipale.toAdresse(), typeAdresse);
			repartPersonneAdresse
					.setToAdresseRelationship(repartAdressePrincipale
							.toAdresse());
			repartPersonneAdresse
					.setToPersonneRelationship(repartAdressePrincipale
							.toPersonne());
			repartPersonneAdresse.setToTypeAdresseRelationship(typeAdresse);

			repartPersonneAdresse.setDCreation(now);
			repartPersonneAdresse.setDModification(now);
			repartPersonneAdresse
					.setRpaPrincipal(EORepartPersonneAdresse.RPA_PRINCIPAL_NON);
			repartPersonneAdresse
					.setRpaValide(EORepartPersonneAdresse.RPA_VALIDE_OUI);

			fournis.initialise(persIdCreateur,
					repartPersonneAdresse.toAdresse(), false,
					EOFournis.FOU_TYPE_CLIENT);

			fournis.toValideFournis().setValCreation(utlOrdreCreateur);
			fournis.toValideFournis().setValValidation(utlOrdreCreateur);
			fournis.setFouValide("O");
		} catch (Exception e) {
			CktlLog.log("Commande impossible: " + e.getMessage());
			throw new FormcoException("Commande impossible: " + e.getMessage());
		}
		// Integer persId = (Integer) session().applicationUser().getPersId();
		// repartPersonneAdresse.setPersIdCreation(persId);
		// repartPersonneAdresse.setPersIdModification(persId);
		// repartPersonneAdresse.fixPersIdCreationEtModification();
		// try {
		// Integer persIdCreation = (Integer)
		// session().applicationUser().getPersId();
		// repartPersonneAdresse.setPersIdCreation(persIdCreation);
		// repartPersonneAdresse.setPersIdModification(persIdCreation);
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// e.getMessage();
		// }
		// repartPersonneAdresse.setPersIdModification(session().applicationUser()
		// .getPersId());

		try {
			ec.saveChanges();
		} catch (Exception e) {
			ec.revert();
			throw new FormcoException("Impossible de créer le fournisseur");
		}

		return client;
	}

	/**
	 * Supprimer un client
	 * 
	 * @param ec
	 * @param client
	 * @throws Exception
	 */
	public static void delClient(EOEditingContext ec, EOIndividu client)
			throws Exception {
		if (ec == null || client == null) {
			throw new Exception(
					"Facture papier a supprimer ou utilisateur a null!!");
		}
		CktlDataBus databus = new CktlDataBus(ec);
		
		try {
			// suppriemr la repartadresse
			EORepartPersonneAdresse rpa = (EORepartPersonneAdresse) EOUtilities
					.objectMatchingKeyAndValue(ec,
							EORepartPersonneAdresse.ENTITY_NAME,
							EORepartPersonneAdresse.PERS_ID_KEY,
							client.persId());
			if (rpa != null) {
				ec.deleteObject(rpa);
			}

			// Supprimer l'adresse
			EOAdresse adr = (EOAdresse) EOUtilities.objectMatchingKeyAndValue(
					ec, EOAdresse.ENTITY_NAME, EOAdresse.ADR_ORDRE_KEY,
					rpa.adrOrdre());
			if (adr != null) {
				ec.deleteObject(adr);
			}

			// Supprimer le telephone
			EOPersonneTelephone tel = (EOPersonneTelephone) EOUtilities
					.objectMatchingKeyAndValue(ec,
							EOPersonneTelephone.ENTITY_NAME,
							EOPersonneTelephone.PERS_ID_KEY, client.persId());
			if (tel != null) {
				ec.deleteObject(tel);
			}

			// supprimer le fournisseur
			EOFournisUlr four = (EOFournisUlr) EOUtilities
					.objectMatchingKeyAndValue(ec, EOFournisUlr.ENTITY_NAME,
							EOFournisUlr.PERS_ID_KEY, client.persId());
			if (four != null) {
				ec.deleteObject(four);
			}

			// Supprimer le compte
			EOCompte compte = (EOCompte) EOUtilities.objectMatchingKeyAndValue(
					ec, EOCompte.ENTITY_NAME, EOCompte.PERS_ID_KEY,
					client.persId());
			int cpt_ordre = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(compte));
			
			// Supprimer le compteEmail
			EOCompteEmail email = (EOCompteEmail) EOUtilities
					.objectMatchingKeyAndValue(ec, EOCompteEmail.ENTITY_NAME,
							EOCompteEmail.CPT_ORDRE_KEY, cpt_ordre);

			if (email != null) {
				ec.deleteObject(email);
			}

			if (compte != null) {
				ec.deleteObject(compte);
			}
			
			ec.saveChanges();

			//Supprimer le client
			String exp = "ALTER TRIGGER GRHUM.LOG_DELETE_PERSONNE DISABLE";
			databus.executeSQLQuery(exp, NSArray.EmptyArray);
			NSMutableDictionary<String, Integer> procStockParams = new NSMutableDictionary<String, Integer>();
			procStockParams.put("persid", client.persId());
			CktlCallEOUtilities.executeStoredProcedureNamed(ec,
					"FwkPers_prcDetruirePersonne", procStockParams);
			exp = "ALTER TRIGGER GRHUM.LOG_DELETE_PERSONNE ENABLE";
			databus.executeSQLQuery(exp, NSArray.EmptyArray);
		} catch (Throwable e) {
			ec.revert();
			databus.rollbackTransaction();
			throw new Exception(e);
		}

	}
}
