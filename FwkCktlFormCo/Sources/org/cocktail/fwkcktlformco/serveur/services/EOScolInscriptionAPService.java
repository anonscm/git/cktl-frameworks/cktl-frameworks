/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import org.cocktail.fwkcktlformco.serveur.finder.FinderEoScolInscriptionEtudiant;
import org.cocktail.scolaritefwk.serveur.exception.ScolariteFwkException;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author jb
 *
 */
public abstract class EOScolInscriptionAPService {

	public static  EOScolInscriptionAp inscrireAPALaCarte(
			EOEditingContext ec, int idiplNumero, int mrapKey, int msemOrdre,
			int imrapDispense, int fannKey, int etat) {

		NSArray<Integer> verifDispenses = new NSArray<Integer>(4, 5, 6, 12, 13,
				14, 15);

		int modulo = msemOrdre % 2;

		// Est-il inscrit a l'ap
		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.add(new EOKeyValueQualifier(
				EOScolInscriptionAp.IMRAP_SEMESTRE_KEY,
				EOQualifier.QualifierOperatorEqual, modulo));
		quals.add(new EOKeyValueQualifier(EOScolInscriptionAp.IDIPL_NUMERO_KEY,
				EOQualifier.QualifierOperatorEqual, idiplNumero));
		quals.add(new EOKeyValueQualifier(EOScolInscriptionAp.MRAP_KEY_KEY,
				EOQualifier.QualifierOperatorEqual, mrapKey));

		NSArray<EOScolInscriptionAp> inscriptionAps = EOScolInscriptionAp
				.fetchAll(ec, new EOAndQualifier(quals));
		if ((etat == 1)
				&& ((inscriptionAps == null) || (inscriptionAps.size() <= 0))) {
			EOScolInscriptionAp inscriptionAp = EOScolInscriptionAp.create(ec);
//			inscriptionAp.setIdiplNumero(idiplNumero);
//			inscriptionAp.setMrapKey(mrapKey);
			inscriptionAp.setImrapSemestre(modulo);
			inscriptionAp.setImrapDispense(imrapDispense);
			inscriptionAp.setFannKey(fannKey);
			EOScolInscriptionEtudiant insEtud = FinderEoScolInscriptionEtudiant.findInsEtud(ec, fannKey, idiplNumero);
			
			if (insEtud != null) {
				inscriptionAp.setToScolInscriptionEtudiantRelationship(insEtud);
			}
			
			EOScolMaquetteRepartitionAp repAp = EOScolMaquetteRepartitionAp.fetchFirstByQualifier(ec, new EOKeyValueQualifier(EOScolMaquetteRepartitionAp.MRAP_KEY_KEY, EOQualifier.QualifierOperatorEqual, mrapKey));
			if(repAp!=null)
			{
				inscriptionAp.setToScolMaquetteRepartitionApRelationship(repAp);
			}
			return inscriptionAp;
		}
		if ((etat == 0)
				&& ((inscriptionAps != null) && (inscriptionAps.size() > 0))) {

			NSArray<EOQualifier> qualsVerif = new NSMutableArray<EOQualifier>();
			qualsVerif.add(new EOKeyValueQualifier(
					EOScolInscriptionAp.IMRAP_SEMESTRE_KEY,
					EOQualifier.QualifierOperatorEqual, modulo));
			qualsVerif.add(new EOKeyValueQualifier(
					EOScolInscriptionAp.IDIPL_NUMERO_KEY,
					EOQualifier.QualifierOperatorEqual, idiplNumero));
			qualsVerif.add(new EOKeyValueQualifier(
					EOScolInscriptionAp.MRAP_KEY_KEY,
					EOQualifier.QualifierOperatorEqual, mrapKey));
			EOScolInscriptionAp scolInsAPexistant = EOScolInscriptionAp
					.fetchFirstByQualifier(ec, new EOAndQualifier(qualsVerif));
			if (scolInsAPexistant != null) {
				if (verifDispenses.contains(scolInsAPexistant.imrapDispense()
						.intValue())) {
					throw new ScolariteFwkException(
							"Impossible, il a valide un A.P. !");
				}
			}
			ec.deleteObject(scolInsAPexistant);
		}
		return null;
	}
}
