/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import java.io.IOException;
import java.math.BigDecimal;

import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEOArticleFC;
import org.cocktail.fwkcktlformco.serveur.finder.FinderParcoursPedagogique;
import org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Classe de manipulation d'une UE, EC, RepartUE et RepartEC pour la formation
 * continue
 * 
 * @author jb
 * 
 */
public class UeFcService {

	private EOArticleFC artFC;
	private EOScolMaquetteRepartitionUe repUe;
	private EOScolMaquetteUe ueFC;
	private EOScolMaquetteRepartitionEc repEc;
	private EOScolMaquetteEc ecFC;
	private EOScolMaquetteAp apFC;
	private EOScolMaquetteRepartitionAp repAp;

	public UeFcService() {
		super();
	}

	/**
	 * Creer tous les composants nécessaires pour créer une UE de formation
	 * continue.
	 * 
	 * @param ec
	 * @throws IOException
	 */
	public void creerUeForFC(EOEditingContext ec, EOCatalogue catalogue)
			throws FormcoException {
		if (ec == null)
			throw new FormcoException(
					"Erreur de creation d'une UE de formation continue. EditingContext absent.");
		if (catalogue == null)
			throw new FormcoException(
					"Erreur de creation d'une UE de formation continue. Catalogue absent.");
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);
		this.repUe = EOScolMaquetteRepartUeService
				.creerNouvelleScolMaquetteRepartUEForFC(ec, annee);
		this.ueFC = EOScolMaquetteUeService.creerNouvelleUeForFC(ec, annee);
		this.repUe.setToFwkScolarite_ScolMaquetteUeRelationship(ueFC);

		this.repEc = EOScolMaquetteRepartEcService
				.creerNouvelleScolMaquetteRepartECForFC(ec, annee);
		this.ecFC = EOScolMaquetteEcService.creerNouvelECForFC(ec, annee);
		this.repEc.setToFwkScolarite_ScolMaquetteEcRelationship(ecFC);
		this.repEc.setToFwkScolarite_ScolMaquetteUeRelationship(ueFC);

		this.repAp = EOScolMaquetteRepartApService
				.creerNouvelleScolMaquetteRepartAPForFC(ec, annee);
		this.apFC = EOScolMaquetteApService.creerNouvelAP(ec, annee);
		this.repAp.setToFwkScolarite_ScolMaquetteApRelationship(apFC);
		this.repAp.setToFwkScolarite_ScolMaquetteEcRelationship(ecFC);

		this.artFC = EOArticleFC.creerNouvelArticleFC(ec, catalogue);
		this.artFC.setToScolMaquetteRepartitionUeRelationship(repUe);

		checkCoherence();
	}

	/**
	 * Synchronisation des UE, EC, AP, RepartUE, RepartEC, RepartAP
	 * 
	 * @param repartUE
	 * @param repartEC
	 * @return
	 */
	public void synchroUEForFC() throws FormcoException {
		this.checkCoherence();
		this.checkValidite();
		try {
			
			// Valeurs de l'UE
			// propager le code a partir de celui donne a l'UE
			ecFC.setMecCode(ueFC.mueCode());
			// la reference de l'article est le code de l'UE
			artFC.toCatalogueArticle().setCaarReference(ueFC.mueCode());
			// propager les horairesEtu a partir de celui donne a l'UE (CF IHM)
			ueFC.setMueHoraireEtu(new BigDecimal(artFC.nbHeureArt()));
			ecFC.setMecHoraireEtu(ueFC.mueHoraireEtu());
			// propager le libelle a partir de celui donne a l'UE (CF IHM)
			ecFC.setMecLibelle(ueFC.mueLibelle());
			apFC.setMapLibelle(ueFC.mueLibelle());
			artFC.toCatalogueArticle().article()
					.setArtLibelle(ueFC.mueLibelle());
			// propager les points
			ecFC.setMecPoints(ueFC.muePoints());
			artFC.setNbCreditsECTSArt(new Double(ueFC.muePoints().doubleValue()));
			// Valeurs de RepartUE
			repAp.setMrapSemestre(repUe.toFwkScolarite_ScolMaquetteSemestre()
					.msemOrdre());
			// Valeurs AP
			// propager le nbMaxInscrits
			if ((artFC.nbMaxInscritArt() != null)
					&& (artFC.prctDepassArt() != null)) {
				Double seuilDouble = artFC.nbMaxInscritArt()
						* artFC.prctDepassArt();
				int seuilInt = Integer.parseInt(""
						+ Math.floor(seuilDouble + 0.5));
				apFC.setMapSeuil(seuilInt);
			}
		} catch (Exception e) {
			throw new FormcoException(
					"Erreur de validation d'une UE de formation continue. Valeur manquante."
							+ e.getMessage());
		}

	}

	/**
	 * Vérifie la validité des informations saisies ds le formulaire
	 * @throws FormcoException
	 */
	public void checkValidite() throws FormcoException
	{
		String errorMsg="";
		if (artFC.toCatalogueArticle() == null)
			errorMsg += " Catalogue null.";
		if (artFC.dateDebutArt() == null)
			errorMsg += "Date de début null";
		if (artFC.dateFinArt() == null)
			errorMsg += "Date de fin null";
		if (artFC.nbHeureArt() == null)
			errorMsg += " Nombre d'heure null.";
		if (repUe.toFwkScolarite_ScolMaquetteSemestre() == null)
			errorMsg += "Semestre null.";
		if (repEc.toFwkScolarite_ScolMaquetteTypeEc() == null)
			errorMsg += "Type null.";
		if (apFC.toFwkScolarite_ScolMaquetteHoraireCode() == null)
			errorMsg += "Type d'enseignement null.";
		if (apFC.toFwkScolarite_ScolMaquetteHoraireMode() == null)
			errorMsg += "Mode d'enseignement null.";
		if (artFC.nbMinInscritArt() == null)
			errorMsg += "Quantité min null.";
		if (artFC.nbMaxInscritArt()== null)
			errorMsg += "Quantité max null.";
		if(ueFC.mueLibelle()==null)
			errorMsg += "Libellé null.";
		if(ecFC.toFwkScolarite_ScolFormationDiscipline()==null)
			errorMsg += "Discipline null.";
		if(ueFC.mueCode()==null)
			errorMsg += "Code null.";
		if(ueFC.muePoints()==null)
			errorMsg += "Points ECTS null.";
		if(artFC.toNiveauLMD()==null)
			errorMsg += "Niveau LMD null.";
		if(ueFC.toFwkScolarite_ScolFormationDomaine()==null)
			errorMsg += "Domaine null.";
		if(artFC.toObjectifs()==null)
			errorMsg += "Objectifs null.";
		if(artFC.toSpecialite()==null)
			errorMsg += "Spécialités null.";
		if(artFC.toTypeActions()==null)
			errorMsg += "Type d'actions null.";
		if(artFC.toCatalogueArticle().caarPrixHt()==null)
			errorMsg += "Prix HT null.";
		
		if(errorMsg.length()>0)
			throw new FormcoException(errorMsg);
	}
	// /**
	// * Initialise les valeurs à partir d'une RepartUE de formation continue
	// *
	// * @param selectedRepartUE
	// * @throws FormcoException
	// */
	// public void initAllValuesFromRepartUE(
	// EOScolMaquetteRepartitionUe selectedRepartUE)
	// throws FormcoException {
	// if (selectedRepartUE == null)
	// throw new FormcoException(
	// "Initialisation de UeForService impossible.(org.cocktail.fwkcktlformco.serveur.services.UeForService)");
	// EOEditingContext editingContxt = selectedRepartUE.editingContext();
	// repUe = selectedRepartUE;
	// ueFC = selectedRepartUE.toFwkScolarite_ScolMaquetteUe();
	// NSArray<EOScolMaquetteRepartitionEc> repEcTab = FinderParcoursPedagogique
	// .getRepECFromUE(editingContxt, this.ueFC);
	// if (repEcTab.count() > 1)
	// throw new FormcoException("Erreur d'initialisation. Plusieurs RepartEC");
	// repEc = repEcTab.get(0);
	// ecFC = repEc.toFwkScolarite_ScolMaquetteEc();
	// NSArray<EOScolMaquetteRepartitionAp> repApTab = FinderParcoursPedagogique
	// .getRepAPFromEC(editingContxt, ecFC);
	// if (repApTab.count() > 1)
	// throw new FormcoException(
	// "Erreur d'initialisation. Plusieurs RepartAP");
	// repAp = repApTab.get(0);
	// apFC = repAp.toFwkScolarite_ScolMaquetteAp();
	// checkCoherence();
	// }

	/**
	 * Initialise les valeurs à partir d'un articleFC de formation continue
	 * 
	 * @param selectedArticle
	 * @throws FormcoException
	 */
	public void initAllValuesFromArticleFC(EOArticleFC selectedArticle)
			throws FormcoException {
		if (selectedArticle == null)
			throw new FormcoException(
					"Initialisation de UeForService impossible.(org.cocktail.fwkcktlformco.serveur.services.UeForService)");
		EOEditingContext editingContxt = selectedArticle.editingContext();
		artFC = selectedArticle;
		repUe = artFC.toScolMaquetteRepartitionUe();
		ueFC = artFC.toScolMaquetteRepartitionUe()
				.toFwkScolarite_ScolMaquetteUe();
		NSArray<EOScolMaquetteRepartitionEc> repEcTab = FinderParcoursPedagogique
				.getRepECFromUE(editingContxt, this.ueFC);
		if (repEcTab.count() > 1)
			throw new FormcoException(
					"Erreur d'initialisation. Plusieurs RepartEC");
		repEc = repEcTab.get(0);
		ecFC = repEc.toFwkScolarite_ScolMaquetteEc();
		NSArray<EOScolMaquetteRepartitionAp> repApTab = FinderParcoursPedagogique
				.getRepAPFromEC(editingContxt, ecFC);
		if (repApTab.count() > 1)
			throw new FormcoException(
					"Erreur d'initialisation. Plusieurs RepartAP");
		repAp = repApTab.get(0);
		apFC = repAp.toFwkScolarite_ScolMaquetteAp();
		checkCoherence();
	}

	// /**
	// * Valide les modification apportees a une UE de formation continue.
	// * @param ec
	// * @throws FormcoException
	// */
	// public void addOrUpdateUEForFC(EOEditingContext ec) throws
	// FormcoException
	// {
	// if(ec==null)throw new FormcoException(
	// "Erreur de validation. EditingContext absent");
	// try {
	// ec.saveChanges();
	// } catch (Exception e) {
	// throw new FormcoException(
	// "Erreur de validation. EditingContext absent" + e.getMessage());
	// }
	//
	// }

	/**
	 * Vérifie la cohérence des entités pour assurer la gestion
	 * 
	 * @throws FormcoException
	 */
	public void checkCoherence() throws FormcoException {
		if ((ueFC == null) || (ecFC == null) || (apFC == null)
				|| (repUe == null) || (repEc == null) || (repAp == null))
			throw new FormcoException(
					"Gestion de la formation. Erreur de cohérence. ue, ec, ap, repue, repec, repap null.");
		if (repUe.toFwkScolarite_ScolMaquetteUe() != ueFC)
			throw new FormcoException(
					"Gestion de la formation. Erreur de cohérence. repUe et UE ne sont pas reliées.");
		if ((repEc.toFwkScolarite_ScolMaquetteUe() != ueFC)
				|| (repEc.toFwkScolarite_ScolMaquetteEc() != ecFC))
			throw new FormcoException(
					"Gestion de la formation. Erreur de cohérence. repEc, EC et UE ne sont pas reliées.");
		if ((repAp.toFwkScolarite_ScolMaquetteEc() != ecFC)
				|| (repAp.toFwkScolarite_ScolMaquetteAp() != apFC))
			throw new FormcoException(
					"Gestion de la formation. Erreur de cohérence. repEc, EC et UE ne sont pas reliées.");
		if (artFC.toScolMaquetteRepartitionUe() != repUe) {
			throw new FormcoException(
					"Gestion de la formation. Erreur de cohérence. repUE et artFC ne sont pas reliés.");
		}
		
	}

	// ===========================
	// ACCESSEURS
	// ===========================

	public EOScolMaquetteRepartitionUe getRepUe() {
		return repUe;
	}

	public void setRepUe(EOScolMaquetteRepartitionUe repUe) {
		this.repUe = repUe;
	}

	public EOScolMaquetteUe getUeFC() {
		return ueFC;
	}

	public void setUeFC(EOScolMaquetteUe ueFC) {
		this.ueFC = ueFC;
	}

	public EOScolMaquetteRepartitionEc getRepEc() {
		return repEc;
	}

	public void setRepEc(EOScolMaquetteRepartitionEc repEc) {
		this.repEc = repEc;
	}

	public EOScolMaquetteEc getEcFC() {
		return ecFC;
	}

	public void setEcFC(EOScolMaquetteEc ecFC) {
		this.ecFC = ecFC;
	}

	public EOScolMaquetteAp getApFC() {
		return apFC;
	}

	public void setApFC(EOScolMaquetteAp apFC) {
		this.apFC = apFC;
	}

	public EOScolMaquetteRepartitionAp getRepAp() {
		return repAp;
	}

	public void setRepAp(EOScolMaquetteRepartitionAp repAp) {
		this.repAp = repAp;
	}

	public EOArticleFC getArtFC() {
		return artFC;
	}

	public void setArtFC(EOArticleFC artFC) {
		this.artFC = artFC;
	}

}
