/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe abstraite de manipulation d'une RepartAP pour la formation continue
 * 
 * @author jb
 * 
 */
public abstract class EOScolMaquetteRepartApService {

	/**
	 * Creation d'une AP pour la formation continue. Annee et Majeur gere ici.
	 * Les autres attributs sont geres par le service ou l'ihm.
	 * 
	 * @param ec
	 * @param annee
	 * @return
	 */
	public static EOScolMaquetteRepartitionAp creerNouvelleScolMaquetteRepartAPForFC(
			EOEditingContext ec, EOScolFormationAnnee annee) {
		if (ec == null)
			return null;
		if (annee == null)
			annee = FinderScolFormationAnnee.getScolFormationAnneeCourante(ec);
		EOScolMaquetteRepartitionAp repartAP = EOScolMaquetteRepartitionAp
				.create(ec);
		// majeur
		repartAP.setMrapMajeur(null);
		// semestre gere par service ou ihm
		// Annee
		repartAP.setToFwkScolarite_ScolFormationAnneeRelationship(annee);
		// AP et EC gere par service ou ihm

		return repartAP;
	}


}
