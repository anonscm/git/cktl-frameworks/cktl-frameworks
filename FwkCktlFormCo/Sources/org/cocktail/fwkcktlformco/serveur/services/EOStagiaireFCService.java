/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.TypeContact;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.finder.FinderContratPartenaire;
import org.cocktail.fwkcktlformco.serveur.finder.FinderInscDipl;
import org.cocktail.fwkcktlformco.serveur.utils.FCUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.procedure.ProcedureDelDossier;
import org.cocktail.scolaritefwk.serveur.procedure.ProcedureDelDossierAnnee;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription;
import org.cocktail.scolarix.serveur.procedure.ProcedureDelIInscription;
import org.cocktail.scolarix.serveur.procedure.ProcedureDelInscDipl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOControlUtilities;

public class EOStagiaireFCService {
	public static String ROLE_STAG_FC = "STAGIAIRE FORMATION CONTINUE";
	public static String ABREVIATION_STAGIAIRE = "STAGIAIRE";
	private static EOAssociation roleStagiaire;
	private static EOTypeInscription typeInscriptionFC;

	public static EOAssociation getRoleStagiaire(EOEditingContext ec) {
		if (roleStagiaire == null) {
			// Recuperer le role Stagiaire FC
			try {
				EOStagiaireFCService.roleStagiaire = EOAssociation
						.fetchFirstByQualifier(ec, new EOKeyValueQualifier(
								EOAssociation.ASS_LIBELLE_KEY,
								EOQualifier.QualifierOperatorEqual,
								ROLE_STAG_FC));

			} catch (Exception e) {
				throw new RuntimeException(
						":-( Impossible de recuperer le rôle Stagiaire Formation continue. Veuillez verifier que ce role est bien cree par les fichiers de parametrages SQL fournis");
			}
		}
		return roleStagiaire;
	}

	public static EOTypeInscription getTypeInscriptionFC(EOEditingContext ec) {
		if (ec == null)
			return null;
		if (typeInscriptionFC == null) {
			try {
				typeInscriptionFC = EOTypeInscription.fetchFirstByKeyValue(ec,
						EOTypeInscription.ABREVIATION_INSCRIPTION_KEY,
						EOStagiaireFCService.ABREVIATION_STAGIAIRE);
				if (typeInscriptionFC == null) {
					throw new RuntimeException(
							"Impossible de recuperer le type d'inscription STAGIAIRE . Veuillez contacter l'administrateur.");

				}
			} catch (Exception e) {
				throw new RuntimeException(
						"Impossible de recuperer le type d'inscription STAGIAIRE FORMATION CONTINUE. Veuillez contacter l'administrateur."
								+ e.getMessage());
			}
		}
		return typeInscriptionFC;
	}

	/**
	 * Supprime le stagiaire et ses inscription.ATTENTION: Utilise des
	 * procédures stockées!
	 * 
	 * @param ec
	 * @param stagiaireToDel
	 * @param contratFromDel
	 * @param persIdUtilisateur persId de la personne qui fait la suppression
	 * @throws FormcoException
	 */
	public static void delStagiaire(EOEditingContext ec,
			EOEtudiant stagiaireToDel, Contrat contratFromDel,
			EOScolFormationSpecialisation specialisation, int anneeSuivie,
			ContratPartenaire cp, Integer persIdUtilisateur) throws FormcoException {
		if (ec == null || stagiaireToDel == null || contratFromDel == null
				|| specialisation == null)
			throw new FormcoException(
					"EC, Stagiaire,Contrat ou specialisation FC null.");

		try {
			EOInscDipl inscription = null;

			TypeContact typeStagiaireContact = TypeContact.fetch(ec,
					new EOKeyValueQualifier(TypeContact.TC_LIBELLE_KEY,
							EOQualifier.QualifierOperatorEqual, "STAGIAIRE"));
			if (stagiaireToDel != null) {
				NSArray<EOQualifier> qualCpcs = new NSMutableArray<EOQualifier>();
				qualCpcs.add(new EOKeyValueQualifier(
						ContratPartContact.PERS_ID_CONTACT_KEY,
						EOQualifier.QualifierOperatorEqual, stagiaireToDel
								.toIndividu().persId()));
				qualCpcs.add(new EOKeyValueQualifier(
						ContratPartContact.TYPE_CONTACT_KEY,
						EOQualifier.QualifierOperatorEqual,
						typeStagiaireContact));
				qualCpcs.add(new EOKeyValueQualifier(
						ContratPartContact.CONTRAT_PARTENAIRE_KEY,
						EOQualifier.QualifierOperatorEqual, cp));
				ContratPartContact contact = ContratPartContact.fetch(ec,
						new EOAndQualifier(qualCpcs));
				if (contact == null) {
					throw new FormcoException(
							"Suppression stagiaire impossible. Impossible de retrouver le contact.");
				}
				if (cp == null) {
					cp = FinderContratPartenaire
							.getContratPartenaireFromContratAndContact(ec,
									contact);
				}

				FactoryContratPartenaire factCP = new FactoryContratPartenaire(
						ec, true);
				factCP.supprimerContact(contact, cp, persIdUtilisateur);
				// supprimer la repartasso creer lors de l'ajout du
				// contratPartContact.
				NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
				quals.add(new EOKeyValueQualifier(
						EORepartAssociation.PERS_ID_KEY,
						EOQualifier.QualifierOperatorEqual, contact
								.persIdContact()));
				int cleAssoFC = Integer.parseInt(ERXEOControlUtilities
						.primaryKeyStringForObject(getRoleStagiaire(ec)));
				quals.add(new EOKeyValueQualifier(
						EORepartAssociation.ASS_ID_KEY,
						EOQualifier.QualifierOperatorEqual, cleAssoFC));
				String cleGrpePartenaire = ERXEOControlUtilities
						.primaryKeyStringForObject(contratFromDel
								.groupePartenaire());
				quals.add(new EOKeyValueQualifier(
						EORepartAssociation.C_STRUCTURE_KEY,
						EOQualifier.QualifierOperatorEqual, cleGrpePartenaire));
				EORepartAssociation repartAsso = EORepartAssociation
						.fetchByQualifier(ec, new EOAndQualifier(quals));
				ec.deleteObject(repartAsso);
				ec.saveChanges();
				EOPrestation prestationFC = contratFromDel.prestations().get(0);
				// recuperer le type d'inscription et la specialisation pour
				// pouvoir rechercher l'inscription diplome correspondante.
				NSArray<EOScolMaquetteParcours> parcoursFC = EOPrestationService
						.getParcoursFromPrestation(ec, prestationFC);
				if (parcoursFC != null && !parcoursFC.isEmpty()) {
					specialisation = parcoursFC.get(0)
							.toFwkScolarite_ScolFormationSpecialisation();
					anneeSuivie = EOPrestationService
							.getAnneeSuivieFromPrestation(ec, prestationFC);
				}

				org.cocktail.scolarix.serveur.metier.eos.EOEtudiant etudiantScolASuppr = FCUtils
						.getEtudiantScolFromEtudiantPersonne(ec, stagiaireToDel);
				NSArray<EOInscDipl> inscDiplomes = FinderInscDipl.getInscDipl(
						ec, etudiantScolASuppr, specialisation,
						getTypeInscriptionFC(ec), anneeSuivie);
				if (inscDiplomes.count() == 1)
					inscription = inscDiplomes.get(0);
				if (inscription != null) {
					_CktlBasicDataBus dataBus = new _CktlBasicDataBus(ec);
					ProcedureDelIInscription.enregistrer(dataBus, inscription);
					ProcedureDelInscDipl.enregistrer(dataBus, inscription);
//					ProcedureDelDossier.enregistrer(dataBus, stagiaireToDel);
					
				}
			}
		} catch (Exception e) {
			throw new FormcoException("Erreur de suppression du stagiaire: "
					+ e.getMessage());
		}

	}

	public static void delDossier(EOEditingContext ec,
			EOEtudiant stagiaireToDel) {
		if (stagiaireToDel != null) {
			Integer noIndividu = stagiaireToDel.toIndividu().noIndividu();
			_CktlBasicDataBus dataBus = new _CktlBasicDataBus(ec);		
			ProcedureDelDossier.enregistrer(dataBus, stagiaireToDel);
			invalideIndividu(ec, noIndividu, dataBus);
		}
	}
	
	public static void delDossierAnnee(EOEditingContext ec,
			EOEtudiant stagiaireToDel, Integer histAnnee) {
		if (stagiaireToDel != null && histAnnee >0) {
			_CktlBasicDataBus dataBus = new _CktlBasicDataBus(ec);		
			ProcedureDelDossierAnnee.enregistrer(dataBus, stagiaireToDel, histAnnee);
		}
	}
	
	private static void invalideIndividu(EOEditingContext ec, Integer noIndividu, _CktlBasicDataBus dataBus) {
		if(noIndividu != null){
			EOIndividu individu = EOIndividu.fetchRequiredByKeyValue(ec, EOIndividu.NO_INDIVIDU_KEY, noIndividu);
			if (individu != null) {
				individu.setTemValide("N");
				try {
					String exp = "ALTER TRIGGER GRHUM.TRG_BR_INDIVIDU_ULR DISABLE";
					dataBus.executeSQLQuery(exp, NSArray.EmptyArray);

					ec.saveChanges();
					exp = "ALTER TRIGGER GRHUM.TRG_BR_INDIVIDU_ULR ENABLE";
					dataBus.executeSQLQuery(exp, NSArray.EmptyArray);
				
					
				} catch (Exception e) {
					System.out.println("Erreur lors du passe de l'individu à l'état invalide");
					e.printStackTrace();
				}
			}
		}
	}
	
}
