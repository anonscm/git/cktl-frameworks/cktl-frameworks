/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEORepartPersonneAdresse;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOFonction;
import org.cocktail.fwkcktljefyadmin.common.metier.EOPersonne;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateurFonction;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateurFonctionExercice;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlLog;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

public abstract class ApplicationFCService {
	public static final String PRENOM_FC = "APPLICATION";
	public static final NSTimestamp DATE_NAISSANCE_FC = new NSTimestamp(2011,
			9, 1, 0, 0, 0, NSTimeZone.timeZoneWithName("Europe/Paris", true));

	/**
	 * Verifie si l'individu n'existe pas. le cree sinon
	 * 
	 * @param ec
	 * @param libelle
	 * @param exercice
	 * @return
	 * @throws FormcoException
	 */
	public static EOUtilisateur getOrCreateUserFormationContinue(
			EOEditingContext ec, IPersonne structureFC, EOExercice exercice,
			EOIndividu individuResponsable) throws FormcoException {

		if ((ec == null) || (structureFC == null))
			return null;
		EOUtilisateur utilisateurServiceFC = null;
		try {

			// rechercher une personne ayant le PERSID = structureFC.persId
			EOPersonne personneFC = EOPersonne.fetchByKeyValue(ec,
					EOPersonne.PERS_ID_KEY, structureFC.persId());
			// IPersonne personneFC = PersonneDelegate.fetchPersonneByPersId(ec,
			// structureFC.persId());
			// Chercher le user FC
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					EOUtilisateur.PERS_ID_KEY + " = " + structureFC.persId(),
					null);
			utilisateurServiceFC = EOUtilisateur
					.fetchFirstByQualifier(ec, qual);

			if (utilisateurServiceFC == null) {
				utilisateurServiceFC = ApplicationFCService.creerUtilisateurFC(
						ec, structureFC.persId());
			}

			// Verifier et creer les droits JEFYAdmin pour ce user
			ApplicationFCService.creerDroitsJefyAdmin(ec, utilisateurServiceFC,
					exercice);

			ec.saveChanges();
			ec.invalidateObjectsWithGlobalIDs(new NSArray(utilisateurServiceFC
					.globalID()));
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
			throw new FormcoException(e.getMessage());
		}
		System.out.println("user nom:" + utilisateurServiceFC.utlNom());
		return utilisateurServiceFC;
	}

	/**
	 * Verifie l'existance de la structure Service FC
	 * 
	 * @param ec
	 * @param libelle
	 * @return
	 * @throws FormcoException
	 */
	public static EOStructure existStructrue(EOEditingContext ec, String libelle)
			throws FormcoException {
		EOStructure res = (EOStructure) EOUtilities.objectMatchingKeyAndValue(
				ec, EOStructure.ENTITY_NAME, EOStructure.LL_STRUCTURE_KEY,
				libelle);

		if (res == null) {
			String errorMsg = ":-( Impossible de recuperer le service des Formations continues ayant pour libelle "
					+ libelle
					+ ". Veuillez créer ce service dans (GRHUM.STRUCTURE_ULR) ou corriger le parametre LIB_SERVICE_FC du fichier de conf";
			System.err.println(errorMsg);
			throw new FormcoException(errorMsg);
		}
		return res;
	}

	// /**
	// * Creer l'individu relatif au service FC
	// *
	// * @param ec
	// * @param structureServiceFC
	// * @param exercice
	// * @return
	// * @throws FormcoException
	// */
	// public static EOIndividu creerIndividuFC(EOEditingContext ec,
	// EOStructure structureServiceFC, EOExercice exercice,
	// EOIndividu individuCreateur) throws FormcoException {
	//
	//
	//
	//
	// EOIndividu individuServiceFC = null;
	// EOCivilite madame = (EOCivilite) EOUtilities.objectMatchingKeyAndValue(
	// ec, EOCivilite.ENTITY_NAME, EOCivilite.C_CIVILITE_KEY,
	// EOCivilite.C_CIVILITE_MADAME);
	// individuServiceFC = EOIndividu.creerInstance(ec);
	// individuServiceFC.setToCiviliteRelationship(madame);
	// individuServiceFC.setNomUsuel(structureServiceFC.libelle());
	// individuServiceFC.setPrenom(ApplicationFCService.PRENOM_FC);
	// NSTimestamp dateDujour = new NSTimestamp();
	// individuServiceFC.setDNaissance(ApplicationFCService.DATE_NAISSANCE_FC);
	// individuServiceFC.setNoIndividuCreateur(individuCreateur.noIndividu());
	// individuServiceFC.setPersIdModification(individuCreateur.persId());
	// individuServiceFC.setDCreation(dateDujour);
	// individuServiceFC.setDModification(dateDujour);
	//
	//
	// try {
	// ec.saveChanges();
	// }
	// catch (Exception e)
	// {
	// ec.revert();
	// e.printStackTrace();
	// throw new FormcoException(e.getMessage());
	// }
	//
	//
	//
	//
	//
	//
	// EOPersonne personne = EOPersonne.fetchByKeyValue(ec,
	// EOPersonne.PERS_ID_KEY, individuServiceFC.persId());
	// EOUtilisateurInfo userInfo = EOUtilisateurInfo.fetchByKeyValue(ec,
	// EOUtilisateur.NO_INDIVIDU_KEY, individuServiceFC.noIndividu());
	// // Creer l'utilisateur JEFY_ADMIN
	// EOUtilisateur userFC = EOUtilisateur.creerInstance(ec);
	// userFC.setToPersonneRelationship(personne);
	// userFC.setToUtilisateurInfoRelationship(userInfo);
	// // userFC.setPersId(personne.persId());
	// // userFC.setNoIndividu(individuServiceFC.noIndividu());
	// // userFC.setIndividu(individuServiceFC);
	// // userFC.setNoIndividu(individuULR.noIndividu());
	// // userFC.setPersId(individuULR.persId());
	// userFC.setUtlOuverture(dateDujour);
	// EOTypeEtat valide = (EOTypeEtat) EOUtilities.objectMatchingKeyAndValue(
	// ec, EOTypeEtat.ENTITY_NAME, EOTypeEtat.TYET_ID_KEY, 1);
	// userFC.setToTypeEtatRelationship(valide);
	//
	// EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(userFC);
	// try {
	// // Avant de sauvegarder les données, nous modifions le modèle
	// // pour que l'on puisse avoir accès aussi en écriture sur les données
	// entityParameter.setReadOnly(false);
	// ec.saveChanges();
	// }
	// catch (EOGeneralAdaptorException e)
	// {
	// // Logger l'exception + Annuler les changements et les caches
	// ec.invalidateAllObjects();
	// // Affichage de l'exception
	// throw new FormcoException(e.getMessage());
	// }
	// catch (ValidationException e)
	// {
	// throw new FormcoException(e.getMessage());
	// } finally {
	// entityParameter.setReadOnly(true);
	// }
	//
	// ApplicationFCService.creerDroitsJefyAdmin(ec, userFC, exercice);
	//
	// try {
	// ec.saveChanges();
	// } catch (Exception e) {
	// ec.revert();
	// e.printStackTrace();
	// throw new FormcoException(e.getMessage());
	// }
	// return individuServiceFC;
	//
	// }

	/**
	 * Creer un Utilisateur dedie a la FC, relie a la structure Service
	 * Formation Continue. Creer le fournisseur interne FC
	 */
	public static EOUtilisateur creerUtilisateurFC(EOEditingContext ec,
			int persId) {
		if ((ec == null) || (persId <= 0))
			return null;
		EOPersonne personne = EOPersonne.fetchByKeyValue(ec,
				EOPersonne.PERS_ID_KEY, persId);

		// Creer l'utilisateur JEFY_ADMIN
		EOUtilisateur userFC = EOUtilisateur.creerInstance(ec);
		userFC.setPersonneRelationship(personne);
		NSTimestamp dateDujour = new NSTimestamp();
		userFC.setUtlOuverture(dateDujour);
		EOTypeEtat valide = (EOTypeEtat) EOUtilities.objectMatchingKeyAndValue(
				ec, EOTypeEtat.ENTITY_NAME, EOTypeEtat.TYET_ID_KEY, 1);
		userFC.setTypeEtatRelationship(valide);
		
		return userFC;
		// EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(userFC);
		// try {
		// // Avant de sauvegarder les données, nous modifions le modèle
		// // pour que l'on puisse avoir accès aussi en écriture sur les données
		// entityParameter.setReadOnly(false);
		// ec.saveChanges();
		// }
		// catch (EOGeneralAdaptorException e)
		// {
		// // Logger l'exception + Annuler les changements et les caches
		// ec.invalidateAllObjects();
		// // Affichage de l'exception
		// throw new FormcoException(e.getMessage());
		// }
		// catch (ValidationException e)
		// {
		// throw new FormcoException(e.getMessage());
		// } finally {
		// entityParameter.setReadOnly(true);
		// }
	}

	/**
	 * Creer les droits jefyadmin pour le user ServiceFC. * Recettes: creation
	 * de prestation cumulative 614; consultation de paiements web: 613; voir
	 * toutes les recettes/titres: 315
	 * 
	 * prestation droits de creer/modifier/supprimer des prestations: 309; droit
	 * de facturer pendant la periode de blocage:301; droit de facturer hors de
	 * la periode de blocage: 300;
	 * 
	 * creation de fournisseur: 601, consultation fournisseur: 603, validation
	 * fournisseur: 602
	 * 
	 * @param ec
	 * @param userFC
	 * @param exercice
	 */
	public static void creerDroitsJefyAdmin(EOEditingContext ec,
			EOUtilisateur userFC, EOExercice exercice) throws FormcoException {
		int[] fonOrdre = { 601, 602, 603, 614, 613, 315, 309, 301, 300 };

		for (int i = 0; i < fonOrdre.length; i++) {
			// Verification de l'existence de la fonction
			EOFonction fonction = (EOFonction) EOUtilities
					.objectMatchingKeyAndValue(ec, EOFonction.ENTITY_NAME,
							EOFonction.FON_ORDRE_KEY, fonOrdre[i]);
			if (fonction == null) {
				throw new FormcoException(
						"Impossible de recuperer la fonction FonOrdre:"
								+ fonOrdre
								+ ". Vérifiez sa présence dans la table JEFY_ADMIN.FONCTION.");
			}

			// Verifier l'existence de l'enregistrement EOUtilisateurFonction
			EOUtilisateurFonction userFonction = EOUtilisateurFonction
					.fetchByQualifier(ec,
							ApplicationFCService.qualUF(userFC, fonction));
			if (userFonction == null) {
				// Si l'enregistrement UtilisateurFonction n'existe pas, le
				// creer
				userFonction = EOUtilisateurFonction.creerInstance(ec);
				userFonction.setFonctionRelationship(fonction);
				userFonction.setUtilisateurRelationship(userFC);

			}

			// Verifier l'existence de l'enregistrement
			// EOUtilisateurFonctionExercice
			EOQualifier fue = ApplicationFCService.qualUFE(userFonction,
					exercice);
			EOUtilisateurFonctionExercice userFoncExe = EOUtilisateurFonctionExercice
					.fetchByQualifier(ec, fue);
			if (userFoncExe == null) {
				// Si l'enregistrement n'existe pas, le creer
				userFoncExe = EOUtilisateurFonctionExercice.creerInstance(ec);

				userFoncExe.setExerciceRelationship(exercice);
				userFoncExe.setUtilisateurFonctionRelationship(userFonction);
				// Ajouter la liaison retour
				// EOUtilisateurFonction.to_UtilisateurFonctionExercices
				userFonction
						.addToUtilisateurFonctionExercicesRelationship(userFoncExe);
			}

		}

	}
	
	
	
	public static EOFournis creerFournisseurInterneStructure(
			EOEditingContext ec, EOUtilisateur userFC) throws FormcoException {
		// Recuperation de l'individu connecté
		if (ec == null || userFC == null )
			return null;
		int persIdCreateur = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(userFC.personne()));
		int utlOrdreCreateur = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(userFC));
		if (persIdCreateur == 0 || utlOrdreCreateur == 0)
			throw new FormcoException(
					"Impossible de trouver le createur du fournisseur");
		// creation du fournisseur
		EOFournis fournis = EOFournis.creerInstance(ec);
		NSTimestamp now = new NSTimestamp();
		EOStructure structureFC = (EOStructure) EOUtilities
				.objectMatchingKeyAndValue(ec, EOStructure.ENTITY_NAME,
						EOStructure.PERS_ID_KEY, persIdCreateur);
		if (structureFC == null) {
			CktlLog.log("Creation fournisseur impossible, Aucun fournisseur n'est defini pour l'utilisateur persId:"+persIdCreateur);
			throw new FormcoException(
			"Creation fournisseur impossible, Aucun fournisseur n'est defini pour l'utilisateur persId:"+persIdCreateur);
		}
		fournis.setToStructure(structureFC);
		NSArray<EORepartPersonneAdresse> tabRepartAdresse = FinderEORepartPersonneAdresse
				.getRepPersAdr(ec, persIdCreateur);
		if(tabRepartAdresse==null || tabRepartAdresse.isEmpty())
		{
			CktlLog.log("PB, Aucune adresse principale n'est definie pour l'utilisateur.");
			throw new FormcoException(
			"Aucune adresse principale n'est definie pour l'utilisateur pers_id: "+persIdCreateur);
		}
		EORepartPersonneAdresse repartAdressePrincipale = (EORepartPersonneAdresse) tabRepartAdresse
				.get(0);
		if (repartAdressePrincipale == null) {
			CktlLog.log("Commande impossible, Aucune adresse principale n'est definie pour l'utilisateur.");
			throw new FormcoException(
			"Impossible de trouver le createur du fournisseur");
		}
		Integer persIdCreationAdr = repartAdressePrincipale.toAdresse()
				.persIdCreation();
		if (persIdCreationAdr == null) {
			repartAdressePrincipale.toAdresse().setPersIdCreation(
					persIdCreateur);
			repartAdressePrincipale.toAdresse().setPersIdModification(
					persIdCreateur);
		}
		
		try {
			// Creation de la repartAdresse pour le fournisseur
			// Liaison Adresse Client
			EORepartPersonneAdresse repartPersonneAdresse = EORepartPersonneAdresse
					.creerInstance(ec);
			EOTypeAdresse typeAdresse = EOTypeAdresse.fetchByQualifier(ec,
					EOTypeAdresse.QUAL_TADR_CODE_FACT);
			// repartPersonneAdresse.initForPersonne(ec, fournis.toIndividu(),
			// repartAdressePrincipale.toAdresse(), typeAdresse);
			repartPersonneAdresse
					.setToAdresseRelationship(repartAdressePrincipale
							.toAdresse());
			repartPersonneAdresse
					.setToPersonneRelationship(repartAdressePrincipale
							.toPersonne());
			repartPersonneAdresse.setToTypeAdresseRelationship(typeAdresse);

			repartPersonneAdresse.setDCreation(now);
			repartPersonneAdresse.setDModification(now);
			repartPersonneAdresse
					.setRpaPrincipal(EORepartPersonneAdresse.RPA_PRINCIPAL_NON);
			repartPersonneAdresse
					.setRpaValide(EORepartPersonneAdresse.RPA_VALIDE_OUI);

			fournis.toValideFournis().setValCreation(utlOrdreCreateur);
			fournis.toValideFournis().setValValidation(utlOrdreCreateur);
			fournis.setFouValide("O");
			fournis.initialise(persIdCreateur,
					repartPersonneAdresse.toAdresse(), false,
					EOFournis.FOU_TYPE_FOURNISSEUR);
			
			ec.saveChanges();
			System.out.println("Creer Fournisseur");
		} catch (Exception e) {
			ec.revert();
			throw new FormcoException("Impossible de créer le fournisseur");
		}

		return fournis;
	}

	public static EOQualifier qualUF(EOUtilisateur u, EOFonction f) {
		NSArray<Object> tabArg = new NSMutableArray<Object>();
		tabArg.add(u);
		tabArg.add(f);
		return EOQualifier.qualifierWithQualifierFormat(
				
				EOUtilisateurFonction.UTILISATEUR_KEY + " = %@ AND "
						+ EOUtilisateurFonction.FONCTION_KEY + " = %@",
				tabArg);
	}

	public static EOQualifier qualUFE(EOUtilisateurFonction uf, EOExercice exerc) {
		NSArray<Object> tabArg = new NSMutableArray<Object>();
		tabArg.add(uf);
		tabArg.add(exerc);
		return EOQualifier.qualifierWithQualifierFormat(
				EOUtilisateurFonctionExercice.UTILISATEUR_FONCTION_KEY
						+ " = %@ AND "
						+ EOUtilisateurFonctionExercice.EXERCICE_KEY + " = %@",
				tabArg);
	}

}