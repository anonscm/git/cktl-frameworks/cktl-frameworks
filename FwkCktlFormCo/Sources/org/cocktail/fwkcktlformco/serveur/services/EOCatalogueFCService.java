/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.services;

import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.kava.server.factory.FactoryCatalogue;
import org.cocktail.kava.server.finder.FinderCatalogue;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.finder.FinderTypePublic;
import org.cocktail.kava.server.metier.EOBoutique;
import org.cocktail.kava.server.metier.EOBoutiqueCatalogue;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOTypeApplication;
import org.cocktail.kava.server.metier.EOTypePublic;
import org.cocktail.kava.server.procedures.GetNumerotation;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOCatalogueFCService {

	public static String TYAP_STR_ID_FC = "FORMATION_CONTINUE";
	/**
	 * Creer un catalogue directement relie a la boutique passee en parametre.
	 * Effectue les actions anciennement effectuees par le controleur
	 * org.cocktail.pie.server.components.controllers.ModCreationCatalogueCtrl.
	 * validerCreationCatalogue(Integer catNumero) Copie egalement le code de la
	 * factory : org.cocktail.kava.server.factory.FActoryCatalogue.newObject(
	 * EOEditingContext ec)
	 * 
	 * @see FactoryCatalogue
	 * @param ec
	 * @param boutique
	 * @return
	 */
	public static EOCatalogue creerNouveauCatalogue(CktlDataBus dataBus,
			EOEditingContext ec, EOBoutique boutique) {
		if (dataBus == null)
			throw new NSValidation.ValidationException(
					"Erreur de creation d'une nouvelle instance de catalogue. CktlDataBus absent.");
		if (ec == null)
			throw new NSValidation.ValidationException(
					"Erreur de creation d'une nouvelle instance de catalogue. EOEditingContext absent.");
		if (boutique == null) {
			throw new NSValidation.ValidationException(
					"Erreur de creation d'une nouvelle instance de catalogue. EOBoutique manquante.");
		}

		EOCatalogue newCatalogue = null;
		newCatalogue = EOCatalogue.creerInstance(ec);
		newCatalogue.setTypeEtat(FinderTypeEtat.typeEtatValide(ec));
		EOTypeApplication typeAppliFC =(EOTypeApplication) EOUtilities.objectMatchingKeyAndValue(ec, EOTypeApplication.ENTITY_NAME, EOTypeApplication.TYAP_STRID_KEY, TYAP_STR_ID_FC);
		newCatalogue.setTypeApplication(typeAppliFC);
		NSTimestamp dateDebut = new NSTimestamp();
		newCatalogue.setCatDateDebut(dateDebut);
		newCatalogue.setCatDateFin(null);

		// Initialise cataloguePrestation
		// EOCataloguePrestation catPrestation = EOCataloguePrestation
		// .creerInstance(ec);
		// catPrestation.setCatPublieWeb("O");
		// newCatalogue.setCataloguePrestation(catPrestation);
		newCatalogue.cataloguePrestation().setCatPublieWeb("O");
		
		
		// Initialise le numero de catalogue
		Integer nextCatNumero = null;
		Number numero;
		try {
			numero = GetNumerotation.get(dataBus, ec, null, null,
					GetNumerotation.CATALOGUE);
			if (numero != null) {
				nextCatNumero = new Integer(numero.intValue());
				newCatalogue.cataloguePrestation().setCatNumero(nextCatNumero);
			} else {
				throw new NSValidation.ValidationException(
						"Erreur à l'initialisation du numéro de catalogue !");
			}
		} catch (Exception e) {
			throw new NSValidation.ValidationException(
					"Erreur à la génération du numéro de catalogue !"
							+ e.getMessage());
		}

		// Initialise la relation avec EOBoutique
		EOBoutiqueCatalogue boutiqueCatalogue = EOBoutiqueCatalogue
				.creerInstance(ec);
		EOTypePublic typePublicFC = FinderTypePublic
				.typePublicFormationContinueStr(ec);
		NSMutableArray<EOTypePublic> tabTypePublic = new NSMutableArray<EOTypePublic>();
		tabTypePublic.add(typePublicFC);
		newCatalogue.setTypesPublicAffectes(tabTypePublic);
		boutiqueCatalogue.setBoutiqueRelationship(boutique);
		boutiqueCatalogue.setCatalogueRelationship(newCatalogue);
		boutiqueCatalogue.setDateDebut(newCatalogue.catDateDebut());
		boutiqueCatalogue.setDateFin(newCatalogue.catDateFin());

		// Recopie les infos budgetaires s'il existe un autre catalogue de la
		// meme boutique
		NSArray<EOCatalogue> catalogues = new NSArray<EOCatalogue>();
		catalogues = FinderCatalogue.find(ec, boutique);
		if ((catalogues != null) && (!catalogues.isEmpty())) {
			if (catalogues.size() > 0) {
				EOCatalogue catalogueOld = (EOCatalogue) catalogues
						.lastObject();
				if (catalogueOld != null) {
					FactoryCatalogue.recopieInfosBudgetaires(catalogueOld,
							newCatalogue);
				}
			}
		}
		return newCatalogue;
	}

}
