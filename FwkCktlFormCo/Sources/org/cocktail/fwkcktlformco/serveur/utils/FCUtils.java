/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.utils;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.fwkcktlformco.serveur.finder.FinderEORepartPersonneAdresse;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.kava.server.metier.EOConvention;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOTva;
import org.cocktail.kava.server.metier.EOUtilisateur;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;

import com.ibm.icu.util.TimeZone;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

import er.extensions.eof.ERXEOControlUtilities;

public final class FCUtils {
	private static EOExerciceCocktail exeCktl;

	/**
	 * Utilitaire pour afficher le libelle d'un parcours
	 * 
	 * @param parcours
	 * @return
	 */
	public static final String getLibelleParcours(
			EOScolMaquetteParcours parcours) {
		String res = "";
		if (parcours == null) {
			res = "NR";
		} else {
			res += "Parcours: " + parcours.mparLibelle() + " ("
					+ parcours.mparCode() + ")";
		}
		return res;
	}

	/**
	 * Utilitaire pour retourner les elements a afficher lorsqu'on recupere un
	 * enregistrement de RepartSemestre
	 * 
	 * @param repSem
	 * @return
	 */
	public static final String getLibelleRepartSemestre(
			EOScolMaquetteRepartitionSem repSem) {
		String res = "";
		if (repSem == null) {
			res = "NR";
		} else {
			if (repSem.toFwkScolarite_ScolMaquetteParcours() != null) {
				res += " Diplôme: "
						+ repSem.toFwkScolarite_ScolMaquetteParcours()
								.toFwkScolarite_ScolFormationSpecialisation()
								.toFwkScolarite_ScolFormationDiplome()
								.fdipLibelle();
			}
			res += FCUtils.getLibelleSemestre(repSem
					.toFwkScolarite_ScolMaquetteSemestre());

		}
		return res;
	}

	/**
	 * Retourne le libelle a afficher pour un repartEC
	 * 
	 * @param repEC
	 * @return
	 */
	public static final String getLibelleRepartEC(
			EOScolMaquetteRepartitionEc repEC) {
		String res = "";
		if (repEC == null) {
			return "NR";
		} else {
			if (repEC.toFwkScolarite_ScolMaquetteUe() != null) {
				res += FCUtils.getLibelleUE(repEC
						.toFwkScolarite_ScolMaquetteUe());
			}
			if (repEC.toFwkScolarite_ScolMaquetteEc() != null) {
				res += FCUtils.getLibelleEC(repEC
						.toFwkScolarite_ScolMaquetteEc());
			}
		}
		return res;
	}

	/**
	 * Retourne le libelle a afficher pour un repart UE
	 * 
	 * @param repUE
	 * @return
	 */
	public static final String getLibelleRepartUE(
			EOScolMaquetteRepartitionUe repUE) {
		String res = "";
		if (repUE == null) {
			res = "NR";
		} else {
			res = FCUtils.getLibelleSemestre(repUE
					.toFwkScolarite_ScolMaquetteSemestre());
			res += FCUtils.getLibelleUE(repUE.toFwkScolarite_ScolMaquetteUe());
		}
		return res;
	}

	/**
	 * Retourne un libelle affichable pour un RepartAP
	 * 
	 * @param repAP
	 */
	public static final String getLibelleRepartAP(
			EOScolMaquetteRepartitionAp repAP) {

		String res = "";
		if (repAP == null) {
			res = "NR";
		} else {
			res = FCUtils.getLibelleEC(repAP.toFwkScolarite_ScolMaquetteEc());
			res += FCUtils.getLibelleAP(repAP.toFwkScolarite_ScolMaquetteAp());
		}
		return res;
	}

	/**
	 * Retourne un libelle a afficher pour un Semestre
	 * 
	 * @see EOScolMaquetteSemestre
	 * @param sem
	 * @return
	 */
	public final static String getLibelleSemestre(EOScolMaquetteSemestre sem) {
		String res = "";
		if (sem == null) {
			res = "NR";
		} else {
			res = " Semestre ";
			NSTimestampFormatter formatter = new NSTimestampFormatter(
					"%d/%m/%y");
			NSTimestamp date;
//			if (sem.dateForKey("msemDateDebut") != null) {
			if(sem.msemDateDebut()!=null){
				date = new NSTimestamp();
				date = sem.msemDateDebut();
				res += " Du: " + formatter.format(date);
			} else {
				res += " Du: NR";
			}

			if (sem.msemDateFin()!= null) {
				date = new NSTimestamp();
				date = sem.msemDateFin();
				res += " Au: " + formatter.format(date);
			} else {
				res += " Au: NR ";
			}
			if (sem != null) {
				res += " (Points: " + sem.msemPoints() + ")";
			}
		}
		return res;
	}

	/**
	 * Retourne un libelle a afficher pour une UE
	 * 
	 * @see EOScolMaquetteUe
	 * @param ue
	 * @return
	 */
	public static final String getLibelleUE(EOScolMaquetteUe ue) {
		String res = "";
		if (ue == null) {
			res = "NR";
		} else {
			res = " UE: " + ue.mueLibelle() + " (Points: "
					+ ue.muePoints() + ")";
		}
		return res;
	}

	/**
	 * Retourne un libelle a afficher pour une EC
	 * 
	 * @param ec
	 * @return
	 */
	public static final String getLibelleEC(EOScolMaquetteEc ec) {
		String res = "";
		if (ec == null) {
			res = "NR";
		} else {
			res = " EC: ";
			res += ec.codeEtLibelle();
			res += " (Points:" + ec.mecPoints() + ")";

		}
		return res;
	}

	/**
	 * Retourne un libelle affichable pour un AP
	 * 
	 * @param ap
	 * @return
	 */
	public static final String getLibelleAP(EOScolMaquetteAp ap) {
		String res = "";
		if (ap == null) {
			res = "NR";
		} else {
			res = "AP: "
					+ ap.mapLibelle();
		}
		return res;
	}

	/**
	 * Retourne le nb de jour entre 2 dates
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static final Long getNbDays(NSTimestamp dateDebut,
			NSTimestamp dateFin) {
		if (dateDebut == null)
			return null;
		if (dateFin == null)
			return null;
		Long debut = dateDebut.getTime();
		Long fin = dateFin.getTime();
		return ((fin - debut) / (1000 * 60 * 60 * 24));
	}

	/**
	 * Sensé renvoyé la date au bon TimeZone
	 * 
	 * @param initialeDate
	 * @return
	 */
	public static final NSTimestamp getGoodDate(NSTimestamp initialeDate) {
		if (initialeDate == null)
			return null;
		NSTimeZone tz = NSTimeZone.timeZoneWithName(TimeZone.getDefault()
				.getID(), true);

		NSTimestamp goodDate = new NSTimestamp(initialeDate.getTime(), tz);
		// long milliseconds = initialeDate.getTime() + offset * 1000;
		// goodDate = new NSTimestamp(milliseconds);
		GregorianCalendar myCalendar = new GregorianCalendar(tz);
		myCalendar.setTime(goodDate);
		// myCalendar.setTime(initialeDate);
		NSTimestamp res = new NSTimestamp(
				myCalendar.get(GregorianCalendar.YEAR),
				myCalendar.get(GregorianCalendar.MONTH),
				myCalendar.get(GregorianCalendar.DATE), 0, 0, 0, null);

		return res;
	}

	/**
	 * Factoriser la mise en negatif des prix pour la gestion des remises
	 * 
	 * @param nombre
	 * @return
	 */
	public static BigDecimal negativerBigDecimal(BigDecimal nombre) {
		if (nombre == null)
			return null;
		if (nombre.signum() == 1)
			return nombre.negate();
		return nombre;
	}

	/**
	 * Factoriser la mise en positif des prix pour la gestion des remises
	 * 
	 * @param nombre
	 * @return
	 */
	public static BigDecimal positiverBigDecimal(BigDecimal nombre) {
		if (nombre == null)
			return null;
		if (nombre.signum() == -1)
			return nombre.negate();
		return nombre;
	}

	/**
	 * Retourne un Contrat a partie d'une EOConvention
	 * 
	 * @param ec
	 * @param contratPie
	 * @return
	 */
	public static Contrat getContratAccordFromContratPie(EOEditingContext ec,
			EOConvention contratPie) {
		int conOrdre = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(contratPie));
		// méthode sans ERXKey
		// Contrat contratAccord = Contrat.fetch(ec,Contrat.CON_ORDRE_KEY,
		// conOrdre);

		// Contrat contratAccord = Contrat.fetch(ec,"conOrdre", conOrdre);
		// Avec ERXKey
		Contrat contratAccord = Contrat.fetch(ec,
				Contrat.CON_ORDRE.eq(conOrdre));
		return contratAccord;
	}

	public static Contrat getContratAccordFromContratPie(EOEditingContext ec,
			int conOrdre) {
//		int conOrdre = Integer.parseInt(ERXEOControlUtilities
//				.primaryKeyStringForObject(contratPie));
		// méthode sans ERXKey
		// Contrat contratAccord = Contrat.fetch(ec,Contrat.CON_ORDRE_KEY,
		// conOrdre);

		// Contrat contratAccord = Contrat.fetch(ec,"conOrdre", conOrdre);
		// Avec ERXKey
		Contrat contratAccord = Contrat.fetch(ec,
				Contrat.CON_ORDRE.eq(conOrdre));
		return contratAccord;
	}
	/**
	 * Renvoie un EOFournisUlr a partir d'un EOFourni
	 * @param ec
	 * @param fournisseur
	 * @return
	 */
	public static EOFournisUlr getFouPieFromFou(EOEditingContext ec,
			EOFournis fournisseur) {
		int fouOrdre = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(fournisseur));
		EOFournisUlr fournisseurPie = EOFournisUlr.fetchFirstByQualifier(ec, new EOKeyValueQualifier(EOFournisUlr.FOU_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, fouOrdre));
		return fournisseurPie;
	}
	
	/**
	 * Renvoie un EOFournis a partir d'un EOFourniUlr
	 * @param ec
	 * @param fournisseur
	 * @return
	 */
	public static EOFournis getFouFromFouPie(EOEditingContext ec,
			EOFournisUlr fournisseur) {
		int fouOrdre = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(fournisseur));
		EOFournis fournisseurPie = EOFournis.fetchFirstByQualifier(ec, new EOKeyValueQualifier(EOFournisUlr.FOU_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, fouOrdre));
		return fournisseurPie;
	}
	/**
	 * Retourne la TVA jefyAdmin depuis la TVA Pie
	 * 
	 * @param ec
	 * @param tvaPie
	 * @return
	 */
	public static org.cocktail.fwkcktljefyadmin.common.metier.EOTva getTVAJefyFromTVAPie(
			EOEditingContext ec, EOTva tvaPie) {
		if (ec == null || tvaPie == null)
			return null;
		org.cocktail.fwkcktljefyadmin.common.metier.EOTva tvaJefy = null;
		BigDecimal taux = tvaPie.tvaTaux();
		int tvaId = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(tvaPie));
		if (tvaId > 0) {
			tvaJefy = (org.cocktail.fwkcktljefyadmin.common.metier.EOTva) EOUtilities
					.objectMatchingKeyAndValue(
							ec,
							org.cocktail.fwkcktljefyadmin.common.metier.EOTva.ENTITY_NAME,
							org.cocktail.fwkcktljefyadmin.common.metier.EOTva.TVA_ID_KEY,
							tvaId);
		}
		return tvaJefy;
	}

	/**
	 * Retourne une Convention a partir d'un Contrat
	 * 
	 * @param ec
	 * @param contratFC
	 * @return
	 */
	public static EOConvention getContratPieFromContratAccord(
			EOEditingContext ec, Contrat contratAccord) {
		int conOrdre = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(contratAccord));
		EOConvention contratPie = (EOConvention) EOUtilities
				.objectMatchingKeyAndValue(ec, EOConvention.ENTITY_NAME,
						EOConvention.CON_ORDRE_KEY, conOrdre);
		return contratPie;
	}

	/**
	 * Transforme un etudiant de FwkCktlScol en etudiant de FwkcktlPersonne
	 * 
	 * @param ec
	 * @param etudiant
	 * @return
	 */
	public static org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant getEtudiantPersonneFromEtudiantScol(
			EOEditingContext ec, EOEtudiant etudiant) {
		if (etudiant == null) {
			return null;
		}
		int etudNumero = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(etudiant));
		org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant etudiantPersonne = org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant
				.fetchByKeyValue(
						ec,
						org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant.ETUD_NUMERO_KEY,
						etudNumero);
		return etudiantPersonne;
	}

	/**
	 * Transforme un etudiant de FwkcktlPersonne en etudiant de FwkCktlScol
	 * 
	 * @param ec
	 * @param etudiantPersonne
	 * @return
	 */
	public static EOEtudiant getEtudiantScolFromEtudiantPersonne(
			EOEditingContext ec,
			org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant etudiantPersonne) {
		int etudNumero = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(etudiantPersonne));
		EOEtudiant etudiantScol = EOEtudiant.fetchByKeyValue(ec,
				EOEtudiant.ETUD_NUMERO_KEY, etudNumero);
		return etudiantScol;
	}

	/**
	 * Utilitaire pour retourner les elements a afficher lorsqu'on recupere un
	 * enregistrement de Contrat (Affiche les partenaires et le montant pour les
	 * tranches du contrat
	 * 
	 * @param repSem
	 * @return
	 */
	public static final String getPartenaires(Contrat contratFC) {
		String res = "";
		EOExerciceCocktail exeCktlTmp = null;
		if (contratFC == null) {
			res = "<b>NR</b>";
		} else {
			// if (exeCktl == null) {
			EOExercice currentExercice = null;
			@SuppressWarnings("unchecked")
			NSArray<EOExercice> tmp = EOExercice.fetchAll(contratFC
					.editingContext(), EOExercice.EXE_STAT_KEY, "O",
					new NSArray<EOSortOrdering>(new EOSortOrdering(
							EOExercice.EXE_EXERCICE_KEY,
							EOSortOrdering.CompareAscending)));
			if ((tmp != null) && (tmp.size() > 0)) {
				currentExercice = tmp.lastObject();
			}
			int exeOrdre = Integer.parseInt(ERXEOControlUtilities
					.primaryKeyStringForObject(currentExercice));
			exeCktlTmp = (EOExerciceCocktail) EOUtilities
					.objectMatchingKeyAndValue(contratFC.editingContext(),
							EOExerciceCocktail.ENTITY_NAME,
							EOExerciceCocktail.EXE_ORDRE_KEY, exeOrdre);
			// }
			if (contratFC.tranches() != null) {
				NSArray<Tranche> tranches = contratFC.tranches();
				Enumeration<Tranche> enumTranche = tranches.objectEnumerator();
				while (enumTranche.hasMoreElements()) {
					Tranche tranche = enumTranche.nextElement();
					NSArray<RepartPartenaireTranche> repPTs = tranche
							.toRepartPartenaireTranches();
					Enumeration<RepartPartenaireTranche> enumRPT = repPTs
							.objectEnumerator();
					while (enumRPT.hasMoreElements()) {
						RepartPartenaireTranche rpt = (RepartPartenaireTranche) enumRPT
								.nextElement();
						if (rpt.tranche().exerciceCocktail().exeExercice() == exeCktlTmp
								.exeExercice()) {
							if (rpt.contratPartenaire().cpPrincipalBoolean()) {
								res += " Part. Principal: "
										+ rpt.contratPartenaire().partenaire()
												.getNomPrenomAffichage();
							} else {
								res += " Part: "
										+ rpt.contratPartenaire().partenaire()
												.getNomPrenomAffichage();
							}
							res += " ("
									+ rpt.rptMontantParticipation()
									+ " Euros) (Exercice: "
									+ rpt.tranche().exerciceCocktail()
											.exeExercice() + ")<br>";
						}

					}
				}
			}
		}
		return res;
	}

	public static final String getDebiteurFromFacture(EOFacturePapier facture) {
		if (facture == null)
			return null;
		return facture.personne_persNomPrenom();
	}

	public static final String getDateSignature(Contrat contratFC) {
		String res = "";
		if (contratFC == null)
			return "";
		NSTimestampFormatter formatter = new NSTimestampFormatter("%d/%m/%y");
		if (contratFC.avenantZero().avtDateSignature() == null)
			return "";
		return formatter.format(contratFC.avenantZero().avtDateSignature());
	}

	public static final String getConOrdreFromFacture(EOFacturePapier fap) {
		if (fap == null)
			return null;
		 EOConvention conv = fap.convention();
			System.out.println("ATTENTION FCUtils.getConOrdre convention utilisee a la place d'un contrat cocowork.");

//		Contrat conv = fap.toCWContrat();
		String conOrdre = ERXEOControlUtilities.primaryKeyStringForObject(conv);
		return conOrdre;
	}

	public static final String getLibellePartenaire(RepartPartenaireTranche rpt) {
		if (rpt == null)
			return null;
		return rpt.contratPartenaire().partenaire().getNomCompletAffichage();
	}

	public static final String getLibelleTypePartenaire(
			RepartPartenaireTranche rpt) {
		if (rpt == null)
			return null;
		if (rpt.contratPartenaire().cpPrincipalBoolean())
			return "Organisateur de la formation";
		return "Débiteur (client, employeur ou organisme)";
	}

	public static final String getMontantTranchePartenaire(
			RepartPartenaireTranche rpt) {
		if (rpt == null)
			return null;
		return "" + rpt.contratPartenaire().cpMontant();

	}

	/**
	 * Retourne l'adresse qual de la personne persid
	 * 
	 * @param ec
	 * @param persId
	 * @param qual
	 * @return
	 */
	public static final String getAdresse(EOEditingContext ec, int persId,
			EOQualifier qual) {
		String adresseFactStr = "";
		EOAdresse adresseFacturation = null;
		if (persId == 0 || ec == null)
			return null;
		EOTypeAdresse typeAdrFact = EOTypeAdresse.fetchByQualifier(ec, qual);
		NSArray<EORepartPersonneAdresse> tabRepAdr = FinderEORepartPersonneAdresse
				.getRepPersAdrByType(ec, persId, typeAdrFact);
		if ((tabRepAdr != null) && (!tabRepAdr.isEmpty())) {
			return getLibelleAdresse((EOAdresse) tabRepAdr.get(0).toAdresse());
		}
		return adresseFactStr;
	}

	public static final String getLibelleAdresse(EOAdresse adresse) {
		String adresseStr = "";
		if (adresse != null) {
			if (adresse.adrAdresse1() != null)
				adresseStr += adresse.adrAdresse1();
			if (adresse.adrAdresse2() != null)
				adresseStr += "<br/>" + adresse.adrAdresse2();
			if (adresse.codePostal() != null)
				adresseStr += "<br/>" + adresse.codePostal();
			if (adresse.ville() != null)
				adresseStr += "<br/>" + adresse.ville();
		} else {
			adresseStr = "Non renseignée <br/>";
		}
		return adresseStr;
	}

	
	public static final String getLibelleAdressePDF(EOAdresse adresse) {
		String adresseStr = "";
		if (adresse != null) {
			if (adresse.adrAdresse1() != null)
				adresseStr += adresse.adrAdresse1();
			if (adresse.adrAdresse2() != null)
				adresseStr += "\n" + adresse.adrAdresse2();
			if (adresse.codePostal() != null)
				adresseStr += "\n" + adresse.codePostal();
			if (adresse.ville() != null)
				adresseStr += "\n" + adresse.ville();
		} else {
			adresseStr = "Non renseignée \n";
		}
		return adresseStr;
	}
	public static final EOAdresse getAdresseFromAdressePie(EOEditingContext ec,
			org.cocktail.kava.server.metier.EOAdresse adressePie) {
		if (ec == null || adressePie == null)
			return null;
		int idAdrPie = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(adressePie));
		EOAdresse adrPersonne = (EOAdresse) EOUtilities
				.objectMatchingKeyAndValue(ec, EOAdresse.ENTITY_NAME,
						EOAdresse.ADR_ORDRE_KEY, idAdrPie);
		return adrPersonne;
	}

	public static final EOUtilisateur getUserFromUserCktlDroits(
			EOEditingContext ec,
			org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur userDroitsUtils) {
		if (ec == null || userDroitsUtils == null)
			return null;
		EOUtilisateur userPIE = (EOUtilisateur) EOUtilities
				.objectMatchingKeyAndValue(ec, EOUtilisateur.ENTITY_NAME,
						EOUtilisateur.UTL_ORDRE_KEY, userDroitsUtils.utlOrdre());
		return userPIE;
	}
	
	public static final  org.cocktail.application.serveur.eof.EOExercice getExerciceDataComFromExeDroitsUtils(EOEditingContext ec, org.cocktail.fwkcktldroitsutils.common.metier.EOExercice exercice){
		if(ec==null || exercice == null)return null;
		org.cocktail.application.serveur.eof.EOExercice res = null;
		int exeOrdre = Integer.parseInt(ERXEOControlUtilities.primaryKeyStringForObject(exercice));
		res = org.cocktail.application.serveur.eof.EOExercice.fetchFirstByQualifier(ec, new EOKeyValueQualifier(org.cocktail.application.serveur.eof.EOExercice.EXE_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, exeOrdre));
		return res;
	}
	
	public static final  org.cocktail.fwkcktldroitsutils.common.metier.EOExercice getExerciceDrtsUtilFromExeDataCom(EOEditingContext ec, org.cocktail.application.serveur.eof.EOExercice exercice){
		if(ec==null || exercice == null)return null;
		org.cocktail.fwkcktldroitsutils.common.metier.EOExercice res = null;
		int exeOrdre = Integer.parseInt(ERXEOControlUtilities.primaryKeyStringForObject(exercice));
		res = org.cocktail.fwkcktldroitsutils.common.metier.EOExercice.fetchFirstByQualifier(ec, new EOKeyValueQualifier(org.cocktail.fwkcktldroitsutils.common.metier.EOExercice.EXE_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, exeOrdre));
		return res;
	}
}
