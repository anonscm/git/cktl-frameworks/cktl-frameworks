/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.utils;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.foundation.NSLog;

import er.extensions.jdbc.ERXSQLHelper;

public abstract class PrimaryKeyGetter {
	//
	// /**
	// * Recupere la cle primaire d'un diplome à créer en Base. Utilise le USER
	// * SCOLARITE et la sequence DDIP_SEQ
	// *
	// * @param ec
	// * @return
	// */
	// public static EOGlobalID getEOScolFormationDiplomeSiseKey(
	// EOEditingContext ec) {
	// if (ec == null)
	// return null;
	// // EOGlobalID diplomeSiseGlobalID = null;
	// // String cleDiplomeSiseStr = "";
	// // try {
	// // ERXSQLHelper sqlHelper = ERXSQLHelper.OracleSQLHelper.newSQLHelper(
	// // ec, "Scolarite");
	// // Number cleCodeSise = sqlHelper.getNextValFromSequenceNamed(ec,
	// // "Scolarite", "DDIP_SEQ");
	// // cleDiplomeSiseStr = "" + cleCodeSise;
	// // diplomeSiseGlobalID = EOKeyGlobalID.globalIDWithEntityName(
	// // EOScolFormationDiplome.ENTITY_NAME,
	// // new Object[] { cleDiplomeSiseStr });
	// // } catch (Exception e) {
	// // NSLog.err
	// //
	// .appendln("Erreur de generation de la clé primaire d'un EOScolFormationDiplomeSise."
	// // + e.getMessage());
	// // e.printStackTrace();
	// // return null;
	// // }
	// // return diplomeSiseGlobalID;
	// return PrimaryKeyGetter.getGlobalIdFromModelAndSequence(ec, "Scolarite",
	// "DDIP_SEQ", EOScolFormationDiplome.ENTITY_NAME);
	// }

	/**
	 * Generer la cle primaire d'une specialisation
	 */
	public static Number getEOScolFormationSpecialisationKey(EOEditingContext ec) {
		if (ec == null)
			return null;
		Number cleNumber = null;
		try {
			ERXSQLHelper sqlHelper = ERXSQLHelper.OracleSQLHelper.newSQLHelper(
					ec, "Scolarite");
			cleNumber = sqlHelper.getNextValFromSequenceNamed(ec, "Scolarite",
					"SCOLARITE.FSPN_SEQ");
		} catch (Exception e) {
			NSLog.err
					.appendln("Erreur de generation de la clé primaire d'un EOScolFormationSpecialisation."
							+ e.getMessage());
			e.printStackTrace();
			return null;
		}
		return cleNumber;
	}

	/**
	 * Retourne un GlobalId d'une EntityName à partir d'un modele, d'une
	 * sequence.
	 * 
	 * @param ec
	 * @param modelName
	 * @param sequenceName
	 * @param entityName
	 * @return
	 */
	public static EOGlobalID getGlobalIdFromModelAndSequence(
			EOEditingContext ec, String modelName, String sequenceName,
			String entityName) {
		if ((ec == null) || (modelName == null) || (sequenceName == null)
				|| (entityName == null))
			return null;
		EOGlobalID globalID = null;
		//Prefixer avec le nom du model
		if (!sequenceName.contains(modelName)) {
			sequenceName = modelName + "." + sequenceName;
		}
		try {
			ERXSQLHelper sqlHelper = ERXSQLHelper.OracleSQLHelper.newSQLHelper(
					ec, modelName);
			Object cle = sqlHelper.getNextValFromSequenceNamed(ec, modelName,
					sequenceName);
			globalID = EOKeyGlobalID.globalIDWithEntityName(entityName,
					new Object[] { cle });
		} catch (Exception e) {
			NSLog.err.appendln("Erreur de generation de la clé primaire d'un "
					+ entityName + ". " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		return globalID;
	}
}
