/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.utils;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

/**
 * Classe copiée de PlanningExtension2 pour éviter de dépendre de ce framework
 * pour une seule classe
 *  Utilisée dans l'envoie de mail a la validation d'une commande
 * @author jb
 * 
 */
public class StringHelper {

	public static final String ISO_8859_1 = "ISO-8859-1";
	public static final String UTF_8 = "UTF-8";

	public static String unicodeToIsoLatin(String aString) {

		if (aString == null) {
			return null;
		}

		Charset charset = Charset.forName(ISO_8859_1);
		Charset utf8 = Charset.forName(UTF_8);
		CharsetDecoder decoder = charset.newDecoder();
		CharsetEncoder encoder = utf8.newEncoder();
		String result = null;
		try {
			ByteBuffer encBuff = encoder.encode(CharBuffer.wrap(aString));
			CharBuffer decBuff = decoder.decode(encBuff);
			result = decBuff.toString();
		} catch (CharacterCodingException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String onlyNumericChars(String str) {

		if (str == null) {
			return null;
		}

		StringBuffer strBuff = new StringBuffer();
		char c;

		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);

			if (Character.isDigit(c)) {
				strBuff.append(c);
			}
		}
		return strBuff.toString();
	}

}
