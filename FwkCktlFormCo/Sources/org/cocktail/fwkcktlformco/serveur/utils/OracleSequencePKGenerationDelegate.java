/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.utils;


import java.util.*;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.eof.ERXEC;

/**
 * {@link EODatabaseContext} delegate to handle the generation of new pk via
 * specific Oracle sequences.
 * 
 * <p>
 * By default EOF generates PK with a sequence that must be named
 * TABLE_NAME_SEQ, but in some cases this naming scheme can cause a too long
 * identifier for Oracle.
 * </p>
 * 
 * @author Alexis Tual 
 */
public class OracleSequencePKGenerationDelegate {
	 
    /** The constant used to extract a primary key value from a sequence result. */
    private static final NSArray PK_KEY = new NSArray("pk");

    private final NSMutableDictionary sequencesForEntities;
    
    /**
     * Default constructor that creates an empty delegate. Use
     * <code>addSequnceForEntity</code> method to register sequences for
     * entities.
     */
    public OracleSequencePKGenerationDelegate() {
        sequencesForEntities = new NSMutableDictionary();
    }
    
    /**
     * Helper constructor to register this delegate for a single entity named
     * <code>entityName</code> and corresponding sequence <code>sequenceName</code>.
     */
    public OracleSequencePKGenerationDelegate(String entityName, String sequenceName) {
        this();
        addSequenceForEntity(sequenceName, entityName);
    }
    
//    /**
//     * Constructor registering entities and corresponding sequences with whom the
//     * primary key generation must be done.
//     * 
//     * @param sequencesForEntities
//     */
//    public OracleSequencePKGenerationDelegate(
//            NSDictionary sequencesForEntities) {
//        this.sequencesForEntities = sequencesForEntities;
//    }
    
    /**
     * Registers the Oracle sequence named <code>sequenceName</code> to be used
     * to generate primary keys of the entity named <code>entityName</code>.
     */
    public void addSequenceForEntity(String sequenceName, String entityName) {
        sequencesForEntities.setObjectForKey(sequenceName, entityName);
    }
    
    /**
     * 
     * @param dbCtxt the database context
     * @param object 
     * @param entity the entity
     * @return the new primary key as a dictionary
     * 
     * @see EODatabaseContext.Delegate#databaseContextNewPrimaryKey(EODatabaseContext, Object, EOEntity)
     */
    public NSDictionary databaseContextNewPrimaryKey(
            EODatabaseContext dbCtxt, Object object, EOEntity entity)
    {
        if (sequencesForEntities.allKeys().containsObject(entity.name())) {
            String sequenceName = (String)sequencesForEntities.objectForKey(entity.name());
            String sql = 
                "SELECT " + sequenceName + ".NEXTVAL FROM DUAL";
            NSArray rawRows = EOUtilities.rawRowsForSQL(
//            		new EOEditingContext(), entity.model().name(), sql, PK_KEY);
            		ERXEC.newEditingContext(), entity.model().name(), sql, PK_KEY);
            NSDictionary rowWithPK = (NSDictionary)rawRows.objectAtIndex(0);
            NSMutableDictionary newPrimaryKey = new NSMutableDictionary();
            NSArray entityPrimaryKeys = entity.primaryKeyAttributeNames();
            Enumeration primaryKeyEnumerator = entityPrimaryKeys.objectEnumerator();
            while (primaryKeyEnumerator.hasMoreElements()) {
                String pkName = (String)primaryKeyEnumerator.nextElement();
                newPrimaryKey.takeValueForKey(rowWithPK.objectForKey("pk"), pkName);
            }
            return newPrimaryKey;
        }
        return null;
    }

    /**
     * Registers this object as a delegate of given database context.
     */
    public void registerWithContext(EODatabaseContext context) {
        if (context == null)
            throw new IllegalArgumentException("Need a not-null EODatabaseContext");
        context.setDelegate(this);
    }


    /*
     * Default implementation for the rest of the EODatabaseContext.Delegate interface, to avoid noises in logs
     */
    
    
//	public void databaseContextDidFetchObjects(EODatabaseContext dbCtxt,
//			NSArray array, EOFetchSpecification fetchSpec, EOEditingContext ec) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	public void databaseContextDidSelectObjects(EODatabaseContext dbCtxt,
//			EOFetchSpecification fetchSpec, EODatabaseChannel dbChannel) {
//		// TODO Auto-generated method stub
//		
//	}

//	public boolean databaseContextFailedToFetchObject(EODatabaseContext dbCtxt,
//			Object object, EOGlobalID gid) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	public boolean databaseContextShouldFetchArrayFault(
//			EODatabaseContext dbCtxt, Object object) {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	public boolean databaseContextShouldFetchObjectFault(
//			EODatabaseContext dbCtxt, Object object) {
//		// TODO Auto-generated method stub
//		return true;
//	}

//	public NSArray databaseContextShouldFetchObjects(EODatabaseContext dbCtxt,
//			EOFetchSpecification fetchSpec, EOEditingContext ec) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public boolean databaseContextShouldHandleDatabaseException(
//			EODatabaseContext dbCtxt, Throwable exception) {
//		// TODO Auto-generated method stub
//		return false;
//	}

//	public boolean databaseContextShouldInvalidateObjectWithGlobalID(
//			EODatabaseContext dbCtxt, EOGlobalID gid, NSDictionary dic) {
//		// TODO Auto-generated method stub
//		return true;
//	}

//	public boolean databaseContextShouldLockObjectWithGlobalID(
//			EODatabaseContext dbCtxt, EOGlobalID gid, NSDictionary dic) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	public boolean databaseContextShouldRaiseExceptionForLockFailure(
//			EODatabaseContext dbCtxt, Throwable exception) {
//		// TODO Auto-generated method stub
//		return false;
//	}

//	public boolean databaseContextShouldSelectObjects(EODatabaseContext dbCtxt,
//			EOFetchSpecification fetchSpec, EODatabaseChannel dbChannel) {
//		// TODO Auto-generated method stub
//		return true;
//	}

//	public NSDictionary databaseContextShouldUpdateCurrentSnapshot(
//			EODatabaseContext dbCtxt, NSDictionary dic, NSDictionary dic2,
//			EOGlobalID gid, EODatabaseChannel dbChannel) {
//		// TODO Auto-generated method stub
//		return null;
//	}

//	public boolean databaseContextShouldUsePessimisticLock(
//			EODatabaseContext dbCtxt, EOFetchSpecification fetchSpec,
//			EODatabaseChannel dbChannel) {
//		// TODO Auto-generated method stub
//		return false;
//	}

//	public void databaseContextWillFireArrayFaultForGlobalID(
//			EODatabaseContext dbCtxt, EOGlobalID gid, EORelationship rel,
//			EOFetchSpecification fetchSpec, EOEditingContext ec) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	public void databaseContextWillFireObjectFaultForGlobalID(
//			EODatabaseContext dbCtxt, EOGlobalID gid,
//			EOFetchSpecification fetchSpec, EOEditingContext ec) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	public NSArray databaseContextWillOrderAdaptorOperations(
//			EODatabaseContext dbCtxt, NSArray databaseOps) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public NSArray databaseContextWillPerformAdaptorOperations(
//			EODatabaseContext dbCtxt, NSArray adaptorOps,
//			EOAdaptorChannel adChannel) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public boolean databaseContextWillRunLoginPanelToOpenDatabaseChannel(
//			EODatabaseContext dbCtx, EODatabaseChannel dbChannel) {
//		// TODO Auto-generated method stub
//		return false;
//	}
}
