/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOArticleFC.java instead.
package org.cocktail.fwkcktlformco.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOArticleFC extends AFwkCktlFormCoRecord {
	public static final String ENTITY_NAME = "EOArticleFC";
	public static final String ENTITY_TABLE_NAME = "FORMCO.ARTICLE_FC";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cArtFCId";

	public static final String C_ANNEE_SUIVIE_KEY = "cAnneeSuivie";
	public static final String C_ART_FC_ID_KEY = "cArtFCId";
	public static final String C_NIVEAU_LMDFK_KEY = "cNiveauLMDFK";
	public static final String DATE_CREATION_ART_KEY = "dateCreationArt";
	public static final String DATE_DEBUT_ART_KEY = "dateDebutArt";
	public static final String DATE_FIN_ART_KEY = "dateFinArt";
	public static final String DATE_MODIF_ART_KEY = "dateModifArt";
	public static final String NB_CREDITS_ECTS_ART_KEY = "nbCreditsECTSArt";
	public static final String NB_HEURE_ART_KEY = "nbHeureArt";
	public static final String NB_MAX_INSCRIT_ART_KEY = "nbMaxInscritArt";
	public static final String NB_MIN_INSCRIT_ART_KEY = "nbMinInscritArt";
	public static final String PRCT_DEPASS_ART_KEY = "prctDepassArt";

// Attributs non visibles
	public static final String C_MRSEM_KEY_FK_KEY = "cMrsemKeyFK";
	public static final String C_FDIP_CODE_FK_KEY = "cFdipCodeFK";
	public static final String C_TYPE_ORGAN_ID_FK_KEY = "cTypeOrganIdFK";
	public static final String C_MREC_KEY_FK_KEY = "cMrecKeyFK";
	public static final String C_MRUE_KEY_FK_KEY = "cMrueKeyFK";
	public static final String CAAR_IDFK_KEY = "caarIDFK";
	public static final String C_MRAP_KEY_KEY = "cMrapKey";
	public static final String C_MPAR_KEY_FK_KEY = "cMparKeyFK";

//Colonnes dans la base de donnees
	public static final String C_ANNEE_SUIVIE_COLKEY = "C_ANNEE_SUIVIE";
	public static final String C_ART_FC_ID_COLKEY = "C_ART_FC_ID";
	public static final String C_NIVEAU_LMDFK_COLKEY = "NIV_LMD_FK";
	public static final String DATE_CREATION_ART_COLKEY = "D_CREATION_ART";
	public static final String DATE_DEBUT_ART_COLKEY = "D_DEBUT_ART";
	public static final String DATE_FIN_ART_COLKEY = "D_FIN_ART";
	public static final String DATE_MODIF_ART_COLKEY = "D_MODIF_ART";
	public static final String NB_CREDITS_ECTS_ART_COLKEY = "NB_CRDTS_ECTS_ART";
	public static final String NB_HEURE_ART_COLKEY = "NB_HEURE_ART";
	public static final String NB_MAX_INSCRIT_ART_COLKEY = "NB_MAX_INSCRIT_ART";
	public static final String NB_MIN_INSCRIT_ART_COLKEY = "NB_MIN_INSCRIT_ART";
	public static final String PRCT_DEPASS_ART_COLKEY = "PRCT_DEPASS_ART";

	public static final String C_MRSEM_KEY_FK_COLKEY = "MRSEM_KEY_FK";
	public static final String C_FDIP_CODE_FK_COLKEY = "FDIP_CODE_FK";
	public static final String C_TYPE_ORGAN_ID_FK_COLKEY = "TYPE_ORGAN_FK";
	public static final String C_MREC_KEY_FK_COLKEY = "MREC_KEY_FK";
	public static final String C_MRUE_KEY_FK_COLKEY = "MRUE_KEY_FK";
	public static final String CAAR_IDFK_COLKEY = "CAAR_ID_FK";
	public static final String C_MRAP_KEY_COLKEY = "MRAP_KEY_FK";
	public static final String C_MPAR_KEY_FK_COLKEY = "MPAR_KEY_FK";


	// Relationships
	public static final String TO_CATALOGUE_ARTICLE_KEY = "toCatalogueArticle";
	public static final String TO_NIVEAU_LMD_KEY = "toNiveauLMD";
	public static final String TO_OBJECTIFS_KEY = "toObjectifs";
	public static final String TO_SCOL_FORMATION_DIPLOME_KEY = "toScolFormationDiplome";
	public static final String TO_SCOL_MAQUETTE_PARCOURS_KEY = "toScolMaquetteParcours";
	public static final String TO_SCOL_MAQUETTE_REPARTITION_AP_KEY = "toScolMaquetteRepartitionAp";
	public static final String TO_SCOL_MAQUETTE_REPARTITION_EC_KEY = "toScolMaquetteRepartitionEc";
	public static final String TO_SCOL_MAQUETTE_REPARTITION_SEM_KEY = "toScolMaquetteRepartitionSem";
	public static final String TO_SCOL_MAQUETTE_REPARTITION_UE_KEY = "toScolMaquetteRepartitionUe";
	public static final String TO_SPECIALITE_KEY = "toSpecialite";
	public static final String TO_TYPE_ACTIONS_KEY = "toTypeActions";



	// Accessors methods
  public Integer cAnneeSuivie() {
    return (Integer) storedValueForKey("cAnneeSuivie");
  }

  public void setCAnneeSuivie(Integer value) {
    takeStoredValueForKey(value, "cAnneeSuivie");
  }

  public Integer cArtFCId() {
    return (Integer) storedValueForKey("cArtFCId");
  }

  public void setCArtFCId(Integer value) {
    takeStoredValueForKey(value, "cArtFCId");
  }

  public Integer cNiveauLMDFK() {
    return (Integer) storedValueForKey("cNiveauLMDFK");
  }

  public void setCNiveauLMDFK(Integer value) {
    takeStoredValueForKey(value, "cNiveauLMDFK");
  }

  public NSTimestamp dateCreationArt() {
    return (NSTimestamp) storedValueForKey("dateCreationArt");
  }

  public void setDateCreationArt(NSTimestamp value) {
    takeStoredValueForKey(value, "dateCreationArt");
  }

  public NSTimestamp dateDebutArt() {
    return (NSTimestamp) storedValueForKey("dateDebutArt");
  }

  public void setDateDebutArt(NSTimestamp value) {
    takeStoredValueForKey(value, "dateDebutArt");
  }

  public NSTimestamp dateFinArt() {
    return (NSTimestamp) storedValueForKey("dateFinArt");
  }

  public void setDateFinArt(NSTimestamp value) {
    takeStoredValueForKey(value, "dateFinArt");
  }

  public NSTimestamp dateModifArt() {
    return (NSTimestamp) storedValueForKey("dateModifArt");
  }

  public void setDateModifArt(NSTimestamp value) {
    takeStoredValueForKey(value, "dateModifArt");
  }

  public Double nbCreditsECTSArt() {
    return (Double) storedValueForKey("nbCreditsECTSArt");
  }

  public void setNbCreditsECTSArt(Double value) {
    takeStoredValueForKey(value, "nbCreditsECTSArt");
  }

  public Double nbHeureArt() {
    return (Double) storedValueForKey("nbHeureArt");
  }

  public void setNbHeureArt(Double value) {
    takeStoredValueForKey(value, "nbHeureArt");
  }

  public Integer nbMaxInscritArt() {
    return (Integer) storedValueForKey("nbMaxInscritArt");
  }

  public void setNbMaxInscritArt(Integer value) {
    takeStoredValueForKey(value, "nbMaxInscritArt");
  }

  public Integer nbMinInscritArt() {
    return (Integer) storedValueForKey("nbMinInscritArt");
  }

  public void setNbMinInscritArt(Integer value) {
    takeStoredValueForKey(value, "nbMinInscritArt");
  }

  public Double prctDepassArt() {
    return (Double) storedValueForKey("prctDepassArt");
  }

  public void setPrctDepassArt(Double value) {
    takeStoredValueForKey(value, "prctDepassArt");
  }

  public org.cocktail.kava.server.metier.EOCatalogueArticle toCatalogueArticle() {
    return (org.cocktail.kava.server.metier.EOCatalogueArticle)storedValueForKey("toCatalogueArticle");
  }

  public void setToCatalogueArticleRelationship(org.cocktail.kava.server.metier.EOCatalogueArticle value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOCatalogueArticle oldValue = toCatalogueArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCatalogueArticle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCatalogueArticle");
    }
  }
  
  public org.cocktail.fwkcktlformco.serveur.metier.EONiveauLMD toNiveauLMD() {
    return (org.cocktail.fwkcktlformco.serveur.metier.EONiveauLMD)storedValueForKey("toNiveauLMD");
  }

  public void setToNiveauLMDRelationship(org.cocktail.fwkcktlformco.serveur.metier.EONiveauLMD value) {
    if (value == null) {
    	org.cocktail.fwkcktlformco.serveur.metier.EONiveauLMD oldValue = toNiveauLMD();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toNiveauLMD");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toNiveauLMD");
    }
  }
  
  public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toScolFormationDiplome() {
    return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome)storedValueForKey("toScolFormationDiplome");
  }

  public void setToScolFormationDiplomeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome value) {
    if (value == null) {
    	org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome oldValue = toScolFormationDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolFormationDiplome");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolFormationDiplome");
    }
  }
  
  public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toScolMaquetteParcours() {
    return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours)storedValueForKey("toScolMaquetteParcours");
  }

  public void setToScolMaquetteParcoursRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours value) {
    if (value == null) {
    	org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours oldValue = toScolMaquetteParcours();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolMaquetteParcours");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolMaquetteParcours");
    }
  }
  
  public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp toScolMaquetteRepartitionAp() {
    return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp)storedValueForKey("toScolMaquetteRepartitionAp");
  }

  public void setToScolMaquetteRepartitionApRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp value) {
    if (value == null) {
    	org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp oldValue = toScolMaquetteRepartitionAp();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolMaquetteRepartitionAp");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolMaquetteRepartitionAp");
    }
  }
  
  public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc toScolMaquetteRepartitionEc() {
    return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc)storedValueForKey("toScolMaquetteRepartitionEc");
  }

  public void setToScolMaquetteRepartitionEcRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc value) {
    if (value == null) {
    	org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc oldValue = toScolMaquetteRepartitionEc();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolMaquetteRepartitionEc");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolMaquetteRepartitionEc");
    }
  }
  
  public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem toScolMaquetteRepartitionSem() {
    return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem)storedValueForKey("toScolMaquetteRepartitionSem");
  }

  public void setToScolMaquetteRepartitionSemRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem value) {
    if (value == null) {
    	org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem oldValue = toScolMaquetteRepartitionSem();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolMaquetteRepartitionSem");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolMaquetteRepartitionSem");
    }
  }
  
  public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe toScolMaquetteRepartitionUe() {
    return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe)storedValueForKey("toScolMaquetteRepartitionUe");
  }

  public void setToScolMaquetteRepartitionUeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe value) {
    if (value == null) {
    	org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe oldValue = toScolMaquetteRepartitionUe();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolMaquetteRepartitionUe");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolMaquetteRepartitionUe");
    }
  }
  
  public NSArray toObjectifs() {
    return (NSArray)storedValueForKey("toObjectifs");
  }

  public NSArray toObjectifs(EOQualifier qualifier) {
    return toObjectifs(qualifier, null);
  }

  public NSArray toObjectifs(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = toObjectifs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToObjectifsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOObjectifArticle object) {
    addObjectToBothSidesOfRelationshipWithKey(object, "toObjectifs");
  }

  public void removeFromToObjectifsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOObjectifArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toObjectifs");
  }

  public org.cocktail.fwkcktlformco.serveur.metier.EOObjectifArticle createToObjectifsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOObjectifArticle");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toObjectifs");
    return (org.cocktail.fwkcktlformco.serveur.metier.EOObjectifArticle) eo;
  }

  public void deleteToObjectifsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOObjectifArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toObjectifs");
    editingContext().deleteObject(object);
  }

  public void deleteAllToObjectifsRelationships() {
    Enumeration objects = toObjectifs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToObjectifsRelationship((org.cocktail.fwkcktlformco.serveur.metier.EOObjectifArticle)objects.nextElement());
    }
  }

  public NSArray toSpecialite() {
    return (NSArray)storedValueForKey("toSpecialite");
  }

  public NSArray toSpecialite(EOQualifier qualifier) {
    return toSpecialite(qualifier, null);
  }

  public NSArray toSpecialite(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = toSpecialite();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToSpecialiteRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite object) {
    addObjectToBothSidesOfRelationshipWithKey(object, "toSpecialite");
  }

  public void removeFromToSpecialiteRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSpecialite");
  }

  public org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite createToSpecialiteRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOSpecialite");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toSpecialite");
    return (org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite) eo;
  }

  public void deleteToSpecialiteRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSpecialite");
    editingContext().deleteObject(object);
  }

  public void deleteAllToSpecialiteRelationships() {
    Enumeration objects = toSpecialite().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSpecialiteRelationship((org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite)objects.nextElement());
    }
  }

  public NSArray toTypeActions() {
    return (NSArray)storedValueForKey("toTypeActions");
  }

  public NSArray toTypeActions(EOQualifier qualifier) {
    return toTypeActions(qualifier, null);
  }

  public NSArray toTypeActions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = toTypeActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToTypeActionsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, "toTypeActions");
  }

  public void removeFromToTypeActionsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toTypeActions");
  }

  public org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction createToTypeActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOTypeAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toTypeActions");
    return (org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction) eo;
  }

  public void deleteToTypeActionsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toTypeActions");
    editingContext().deleteObject(object);
  }

  public void deleteAllToTypeActionsRelationships() {
    Enumeration objects = toTypeActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToTypeActionsRelationship((org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction)objects.nextElement());
    }
  }


  public static EOArticleFC createEOArticleFC(EOEditingContext editingContext, Integer cAnneeSuivie
, Integer cArtFCId
, NSTimestamp dateCreationArt
, NSTimestamp dateModifArt
, Double nbCreditsECTSArt
, Double nbHeureArt
, org.cocktail.kava.server.metier.EOCatalogueArticle toCatalogueArticle) {
    EOArticleFC eo = (EOArticleFC) createAndInsertInstance(editingContext, _EOArticleFC.ENTITY_NAME);    
		eo.setCAnneeSuivie(cAnneeSuivie);
		eo.setCArtFCId(cArtFCId);
		eo.setDateCreationArt(dateCreationArt);
		eo.setDateModifArt(dateModifArt);
		eo.setNbCreditsECTSArt(nbCreditsECTSArt);
		eo.setNbHeureArt(nbHeureArt);
    eo.setToCatalogueArticleRelationship(toCatalogueArticle);
    return eo;
  }

  
	  public EOArticleFC localInstanceIn(EOEditingContext editingContext) {
	  		return (EOArticleFC)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOArticleFC creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param eoeditingcontext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOArticleFC creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOArticleFC object = (EOArticleFC)createAndInsertInstance(editingContext, _EOArticleFC.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOArticleFC localInstanceIn(EOEditingContext editingContext, EOArticleFC eo) {
    EOArticleFC localInstance = (eo == null) ? null : (EOArticleFC)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOArticleFC#localInstanceIn a la place.
   */
	public static EOArticleFC localInstanceOf(EOEditingContext editingContext, EOArticleFC eo) {
		return EOArticleFC.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOArticleFC fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOArticleFC fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOArticleFC eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOArticleFC)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOArticleFC fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOArticleFC fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOArticleFC eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOArticleFC)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOArticleFC fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOArticleFC eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOArticleFC ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOArticleFC fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
