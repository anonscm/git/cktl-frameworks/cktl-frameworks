/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartTypeAction.java instead.
package org.cocktail.fwkcktlformco.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORepartTypeAction extends AFwkCktlFormCoRecord {
	public static final String ENTITY_NAME = "EORepartTypeAction";
	public static final String ENTITY_TABLE_NAME = "FORMCO.REPART_TYPE_ACTION";



	// Attributes


	public static final String ARTICLE_FC_REPART_KEY = "articleFCRepart";
	public static final String TYPE_ACTION_REPART_KEY = "typeActionRepart";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ARTICLE_FC_REPART_COLKEY = "C_ART_FC_ID";
	public static final String TYPE_ACTION_REPART_COLKEY = "C_TYPE_ACTION_ID";



	// Relationships
	public static final String ARTICLE_FC_KEY = "articleFC";
	public static final String TYPE_ACTION_KEY = "typeAction";



	// Accessors methods
  public Integer articleFCRepart() {
    return (Integer) storedValueForKey("articleFCRepart");
  }

  public void setArticleFCRepart(Integer value) {
    takeStoredValueForKey(value, "articleFCRepart");
  }

  public Integer typeActionRepart() {
    return (Integer) storedValueForKey("typeActionRepart");
  }

  public void setTypeActionRepart(Integer value) {
    takeStoredValueForKey(value, "typeActionRepart");
  }

  public org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC articleFC() {
    return (org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC)storedValueForKey("articleFC");
  }

  public void setArticleFCRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC value) {
    if (value == null) {
    	org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC oldValue = articleFC();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "articleFC");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "articleFC");
    }
  }
  
  public org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction typeAction() {
    return (org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction)storedValueForKey("typeAction");
  }

  public void setTypeActionRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "typeAction");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "typeAction");
    }
  }
  

  public static EORepartTypeAction createEORepartTypeAction(EOEditingContext editingContext, Integer articleFCRepart
, Integer typeActionRepart
, org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC articleFC, org.cocktail.fwkcktlformco.serveur.metier.EOTypeAction typeAction) {
    EORepartTypeAction eo = (EORepartTypeAction) createAndInsertInstance(editingContext, _EORepartTypeAction.ENTITY_NAME);    
		eo.setArticleFCRepart(articleFCRepart);
		eo.setTypeActionRepart(typeActionRepart);
    eo.setArticleFCRelationship(articleFC);
    eo.setTypeActionRelationship(typeAction);
    return eo;
  }

  
	  public EORepartTypeAction localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepartTypeAction)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartTypeAction creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param eoeditingcontext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartTypeAction creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORepartTypeAction object = (EORepartTypeAction)createAndInsertInstance(editingContext, _EORepartTypeAction.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORepartTypeAction localInstanceIn(EOEditingContext editingContext, EORepartTypeAction eo) {
    EORepartTypeAction localInstance = (eo == null) ? null : (EORepartTypeAction)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORepartTypeAction#localInstanceIn a la place.
   */
	public static EORepartTypeAction localInstanceOf(EOEditingContext editingContext, EORepartTypeAction eo) {
		return EORepartTypeAction.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORepartTypeAction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartTypeAction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartTypeAction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartTypeAction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartTypeAction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartTypeAction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartTypeAction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartTypeAction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORepartTypeAction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartTypeAction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartTypeAction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartTypeAction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
