/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartSpecialite.java instead.
package org.cocktail.fwkcktlformco.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORepartSpecialite extends AFwkCktlFormCoRecord {
	public static final String ENTITY_NAME = "EORepartSpecialite";
	public static final String ENTITY_TABLE_NAME = "FORMCO.REPART_SPECIALITE";



	// Attributes


	public static final String ARTICLE_FC_REPART_KEY = "articleFCRepart";
	public static final String SPECIALITE_REPART_KEY = "specialiteRepart";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ARTICLE_FC_REPART_COLKEY = "C_ART_FC_ID";
	public static final String SPECIALITE_REPART_COLKEY = "C_SPECIALITE_ID";



	// Relationships
	public static final String ARTICLE_FC_KEY = "articleFC";
	public static final String SPECIALITE_KEY = "specialite";



	// Accessors methods
  public Integer articleFCRepart() {
    return (Integer) storedValueForKey("articleFCRepart");
  }

  public void setArticleFCRepart(Integer value) {
    takeStoredValueForKey(value, "articleFCRepart");
  }

  public Integer specialiteRepart() {
    return (Integer) storedValueForKey("specialiteRepart");
  }

  public void setSpecialiteRepart(Integer value) {
    takeStoredValueForKey(value, "specialiteRepart");
  }

  public org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC articleFC() {
    return (org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC)storedValueForKey("articleFC");
  }

  public void setArticleFCRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC value) {
    if (value == null) {
    	org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC oldValue = articleFC();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "articleFC");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "articleFC");
    }
  }
  
  public org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite specialite() {
    return (org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite)storedValueForKey("specialite");
  }

  public void setSpecialiteRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite value) {
    if (value == null) {
    	org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite oldValue = specialite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "specialite");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "specialite");
    }
  }
  

  public static EORepartSpecialite createEORepartSpecialite(EOEditingContext editingContext, Integer articleFCRepart
, Integer specialiteRepart
, org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC articleFC, org.cocktail.fwkcktlformco.serveur.metier.EOSpecialite specialite) {
    EORepartSpecialite eo = (EORepartSpecialite) createAndInsertInstance(editingContext, _EORepartSpecialite.ENTITY_NAME);    
		eo.setArticleFCRepart(articleFCRepart);
		eo.setSpecialiteRepart(specialiteRepart);
    eo.setArticleFCRelationship(articleFC);
    eo.setSpecialiteRelationship(specialite);
    return eo;
  }

  
	  public EORepartSpecialite localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepartSpecialite)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartSpecialite creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param eoeditingcontext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartSpecialite creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORepartSpecialite object = (EORepartSpecialite)createAndInsertInstance(editingContext, _EORepartSpecialite.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORepartSpecialite localInstanceIn(EOEditingContext editingContext, EORepartSpecialite eo) {
    EORepartSpecialite localInstance = (eo == null) ? null : (EORepartSpecialite)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORepartSpecialite#localInstanceIn a la place.
   */
	public static EORepartSpecialite localInstanceOf(EOEditingContext editingContext, EORepartSpecialite eo) {
		return EORepartSpecialite.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORepartSpecialite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartSpecialite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartSpecialite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartSpecialite)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartSpecialite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartSpecialite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartSpecialite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartSpecialite)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORepartSpecialite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartSpecialite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartSpecialite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartSpecialite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
