/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.metier;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.cocktail.fwkcktlformco.serveur.finder.FinderEOTva;
import org.cocktail.fwkcktlformco.serveur.utils.FCUtils;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.kava.server.finder.FinderTypeArticle;
import org.cocktail.kava.server.metier.EOArticle;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.EOTypeArticle;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Article de Formation continue. Basé sur {@link EOArticle}
 * 
 * @author jb
 * 
 */
public class EOArticleFC extends _EOArticleFC {

	private static final long serialVersionUID = -8743815439105158268L;

	public EOArticleFC() {
		super();
		NSTimestamp date = DateCtrl.now();
		this.setNbCreditsECTSArt(0.0);
		this.setDateCreationArt(date);
		this.setDateDebutArt(date);
		this.setDateFinArt(date);
	}

	/**
	 * Creation et initialisation d'un article de formation continue
	 * 
	 * @param ec
	 * @param catalogue
	 * @return
	 */
	public static EOArticleFC creerNouvelArticleFC(EOEditingContext ec,
			EOCatalogue catalogue) {
		if (ec == null) {
			throw new NSValidation.ValidationException(
					"Erreur de creation d'une nouvelle instance d'ArticleFC. EOEditingContext absent.");
		}
		if (catalogue == null) {
			throw new NSValidation.ValidationException(
					"Erreur de creation d'une nouvelle instance d'ArticleFC. EOCatalogue absent.");
		}
		// Creation d'un article
		EOArticle article = EOArticle.creerNouvelArticle(ec);

		// Initialisation d'un nouvel article pour la FC
		if (catalogue.codeMarche() != null) {
			article.setCodeMarche(catalogue.codeMarche());
		}

		// Creation d'un catalogueArticle
		EOCatalogueArticle catArticle = EOCatalogueArticle
				.creerNouveauCatalogueArticle(ec, catalogue, article);
		catArticle.setTva(FinderEOTva.findTaux(ec, FinderEOTva.TAUX_0));
		

		// Creation article de Formation Continue
		EOArticleFC articleFormCo = EOArticleFC.creerInstance(ec);
		articleFormCo.setToCatalogueArticleRelationship(catArticle);
		articleFormCo.setNbCreditsECTSArt(0.0);
		articleFormCo.setNbMinInscritArt(0);
		articleFormCo.setNbMaxInscritArt(0);
		articleFormCo.setDateDebutArt(DateCtrl.now());
		articleFormCo.setDateFinArt(DateCtrl.now());
		articleFormCo.setNbHeureArt(0.0);
		// articleFormCo.toCatalogueArticle();
		articleFormCo.toCatalogueArticle().article().articlePrestation()
				.setArtpInvisibleWeb("N");

		return articleFormCo;
	}

	/**
	 * Formate le libelle du parcours qui s'affiche dans l'IHM
	 * 
	 * @return
	 */
	public String getLibelleParcours() {
		String res = "NR";
		if (this.toScolFormationDiplome() != null) {
			res = "Diplôme: " + this.toScolFormationDiplome().fdipLibelle();
		} else if (this.toScolMaquetteParcours() != null) {
			EOScolMaquetteParcours parcours = this.toScolMaquetteParcours();
			res = FCUtils.getLibelleParcours(parcours);
		} else if (this.toScolMaquetteRepartitionSem() != null) {
			EOScolMaquetteRepartitionSem repSem = this
					.toScolMaquetteRepartitionSem();
			res = FCUtils.getLibelleRepartSemestre(repSem);
		} else if (this.toScolMaquetteRepartitionUe() != null) {
			EOScolMaquetteRepartitionUe repUE = this
					.toScolMaquetteRepartitionUe();
			res = FCUtils.getLibelleRepartUE(repUE);
		} else if (this.toScolMaquetteRepartitionEc() != null) {
			EOScolMaquetteRepartitionEc repEC = this
					.toScolMaquetteRepartitionEc();
			res = FCUtils.getLibelleRepartEC(repEC);
		} else if (this.toScolMaquetteRepartitionAp() != null) {
			EOScolMaquetteRepartitionAp repAP = this
					.toScolMaquetteRepartitionAp();
			res = FCUtils.getLibelleRepartAP(repAP);
		}
		return res;
	}

	/**
	 * Modification du parcours "diplome" d'un article
	 */
	public void setDiplomeArticle(EOScolFormationDiplome dipl) {
		if (dipl != null) {
			this.setOtherParcoursRelationsToNull(dipl.editingContext());
			this.setToScolFormationDiplomeRelationship(dipl
					.localInstanceIn(dipl.editingContext()));
		}
	}

	/**
	 * Modification de la relation avec un parcours Initialise automatiquement
	 * le nombre de crédits à partir du nbCredits du parcours selectionne
	 * 
	 * @param parcours
	 */
	public void setParcoursArticle(EOScolMaquetteParcours parcours) {
		if (parcours != null) {
			this.setOtherParcoursRelationsToNull(parcours.editingContext());
			this.setToScolMaquetteParcoursRelationship(parcours);
			this.setNBCreditsFromSelectedParcours();
		}
	}

	/**
	 * Modification du parcours "semestre" d'un article
	 */
	public void setSemestreArticle(EOScolMaquetteRepartitionSem repsem) {
		if (repsem != null) {
			this.setOtherParcoursRelationsToNull(repsem.editingContext());
			this.setToScolMaquetteRepartitionSemRelationship(repsem);
		}
	}

	/**
	 * Modification du parcours "UE" selectionne
	 * 
	 * @param repUE
	 */
	public void setUEArticle(EOScolMaquetteRepartitionUe repUE) {
		if (repUE != null) {
			this.setOtherParcoursRelationsToNull(repUE.editingContext());
			this.setToScolMaquetteRepartitionUeRelationship(repUE);
		}
	}

	/**
	 * Modification du parcours "EC" selectionne
	 * 
	 * @param repUE
	 */
	public void setECArticle(EOScolMaquetteRepartitionEc repEC) {
		if (repEC != null) {
			this.setOtherParcoursRelationsToNull(repEC.editingContext());
			this.setToScolMaquetteRepartitionEcRelationship(repEC);
		}
	}

	/**
	 * Modification du parcours "AP" selectionne
	 * 
	 * @param repAP
	 */
	public void setAPArticle(EOScolMaquetteRepartitionAp repAP) {
		if (repAP != null) {
			this.setOtherParcoursRelationsToNull(repAP.editingContext());
			this.setToScolMaquetteRepartitionApRelationship(repAP);
		}
	}

	/**
	 * Annule les relations avec un parcours
	 */
	public void setOtherParcoursRelationsToNull(EOEditingContext ec) {
		if (ec != null) {
			this.setToScolFormationDiplomeRelationship(null);
			this.setToScolMaquetteRepartitionSemRelationship(null);
			this.setToScolMaquetteRepartitionUeRelationship(null);
			this.setToScolMaquetteRepartitionEcRelationship(null);
			this.setToScolMaquetteRepartitionApRelationship(null);
		}
	}

	/**
	 * Indique si un seul parcours a été sélectionné
	 * 
	 * @param artFC
	 * @return
	 */
	public Boolean isOnlyOneParcoursSelected() {
		int nbParcours = 0;
		if (this == null)
			return false;
		if (this.toScolFormationDiplome() != null)
			nbParcours++;
		if (this.toScolMaquetteRepartitionSem() != null)
			nbParcours++;
		if (this.toScolMaquetteRepartitionUe() != null)
			nbParcours++;
		if (this.toScolMaquetteRepartitionEc() != null)
			nbParcours++;
		if (this.toScolMaquetteRepartitionAp() != null)
			nbParcours++;
		if (nbParcours > 1)
			return false;
		return true;
	}

	/**
	 * Initialise le NB CREDITS a partir du parcours selectionne
	 */
	public void setNBCreditsFromSelectedParcours() {
		Double nbCredits = 0.0;
		if (toScolMaquetteRepartitionSem() != null) {
			nbCredits = toScolMaquetteRepartitionSem()
					.toFwkScolarite_ScolMaquetteSemestre().msemPoints()
					.doubleValue();
		} else if (toScolMaquetteRepartitionAp() != null) {
			nbCredits = toScolMaquetteRepartitionAp()
					.toFwkScolarite_ScolMaquetteAp()
					.toOneFwkScolarite_ScolMaquetteEC().mecPoints()
					.doubleValue();
		} else if (toScolMaquetteRepartitionEc() != null) {
			nbCredits = toScolMaquetteRepartitionEc()
					.toFwkScolarite_ScolMaquetteEc().mecPoints().doubleValue();
		} else if (toScolMaquetteRepartitionUe() != null) {
			nbCredits = toScolMaquetteRepartitionUe()
					.toFwkScolarite_ScolMaquetteUe().muePoints().doubleValue();
		}
		setNbCreditsECTSArt(nbCredits);
	}

	/**
	 * Verifie la coherence des dates. La date de début doit etre avt la date de
	 * fin.
	 * 
	 * @return
	 */
	public Boolean checkDates() {
		if (this.dateDebutArt() == null)
			return false;
		if (this.dateFinArt() == null)
			return false;
		if (this.dateDebutArt().after(this.dateFinArt()))
			return false;
		return true;
	}

	/**
	 * Retourne la quantite disponible d'article. Cumule la quantite des
	 * prestationLignes en rapport avec l'article. Calcul (nbMaxInscrit +
	 * depassement) - cumule places commandees
	 * 
	 * @param ec
	 * @return
	 */
	public BigInteger getQuantiteDisponible(EOEditingContext ec) {
		if (ec == null)
			return null;
		BigInteger nbCommandeArticle = new BigInteger("0");
		BigInteger qteDispo = new BigInteger("0");
		EOCatalogueArticle catArt = this.toCatalogueArticle();
		NSArray<EOPrestationLigne> listPrestationLignes = this
				.toCatalogueArticle().getPrestationLignesDansPrestaValide();
		for (EOPrestationLigne prestation : listPrestationLignes) {
			if (prestation.catalogueArticle() == this.toCatalogueArticle()) {
				nbCommandeArticle = nbCommandeArticle.add(prestation
						.prligQuantite().toBigInteger());
			}
		}
		BigInteger nbMax = new BigInteger("" + this.nbMaxInscritArt());
		if (this.prctDepassArt() != null) {
			Double pourcentage = (1 + (this.prctDepassArt() / 100));
			Double res = this.nbMaxInscritArt()*pourcentage;
			nbMax = new BigInteger(""+res.intValue());
		}
		qteDispo = nbMax.subtract(nbCommandeArticle);
		return qteDispo;
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		// inscrit la date de création
		this.setDateCreationArt(DateCtrl.now());
		this.setDateModifArt(DateCtrl.now());

		if (DateCtrl.isBefore(FCUtils.getGoodDate(this.dateFinArt()),
				FCUtils.getGoodDate(DateCtrl.now())))
			throw new NSValidation.ValidationException(
					"Vous ne pouvez pas créer une formation passée. (Date de fin avant aujourd'hui)");

		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		// inscrit la date de mise à jour
		this.setDateModifArt(DateCtrl.now());
		this.validateObjectMetier();

		// GregorianCalendar myCalendar = new GregorianCalendar();
		// myCalendar.setTime(DateCtrl.now());
		// NSTimestamp dateDuJour = new NSTimestamp(
		// myCalendar.get(GregorianCalendar.YEAR),
		// myCalendar.get(GregorianCalendar.MONTH),
		// myCalendar.get(GregorianCalendar.DATE), 0, 0, 0, null);
		//
		// NSTimeZone tz =
		// NSTimeZone.timeZoneWithName(TimeZone.getDefault().getID(),true);
		// int offset = tz.secondsFromGMTForTimestamp(this.dateFinArt());
		// long milliseconds = this.dateFinArt().getTime() - offset * 1000;
		// NSTimestamp dateDeFinTZOK = new NSTimestamp(milliseconds);
		// myCalendar.setTime(dateDeFinTZOK);
		// myCalendar.setTimeZone(java.util.TimeZone.getDefault());
		// NSTimestamp dateDeFin = new NSTimestamp(
		// myCalendar.get(GregorianCalendar.YEAR),
		// myCalendar.get(GregorianCalendar.MONTH),
		// myCalendar.get(GregorianCalendar.DATE), 0, 0, 0, null);

		if (DateCtrl.isBefore(FCUtils.getGoodDate(this.dateFinArt()),
				FCUtils.getGoodDate(DateCtrl.now())))
			throw new NSValidation.ValidationException(
					"Vous ne pouvez pas créer une formation passée. (Date de fin avant aujourd'hui)");

		validateBeforeTransactionSave();
		super.validateForUpdate();

	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * Verifie qu'il n'y a qu'un seul parcours de selectionne pour cet article.
	 * Il ne peut pas y avoir deux parcours pédagogiques relies a un seul
	 * article de FC.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
		if (!isOnlyOneParcoursSelected())
			throw new NSValidation.ValidationException(
					"Il ne doit y avoir qu'un seul parcours pédagogique relié à l'article de formation continue");
		if (!checkDates())
			throw new NSValidation.ValidationException(
					"Les dates doivent être renseignées et cohérentes. (Le début avant la fin)");
		if (nbCreditsECTSArt() == null)
			throw new NSValidation.ValidationException(
					"Vous devez renseigner un nombre de crédits.");
		if (nbHeureArt() == null) {
			throw new NSValidation.ValidationException(
					"Vous devez renseigner un nombre d'heures.");
		}
		if (nbMinInscritArt() == null) {
			throw new NSValidation.ValidationException(
					"Vous devez renseigner un nombre minimum d'inscrits.");
		}
		if (nbMaxInscritArt() == null) {
			throw new NSValidation.ValidationException(
					"Vous devez renseigner un nombre maximum d'inscrits.");
		}
		if (nbMinInscritArt() > nbMaxInscritArt()) {
			throw new NSValidation.ValidationException(
					"Nombre d'inscrits incohérent.");
		}
		if (toCatalogueArticle() == null) {
			throw new NSValidation.ValidationException(
					"Erreur de liaison avec un catalogue.");
		}
		if (toObjectifs() == null) {
			throw new NSValidation.ValidationException(
					"Erreur de liaison avec les objectifs. Nécessaire pour le bilan pédagogique et financier");
		}
		if (toSpecialite() == null) {
			throw new NSValidation.ValidationException(
					"Erreur de liaison avec les specialites. Nécessaire pour le bilan pédagogique et financier");
		}
		if (toTypeActions() == null) {
			throw new NSValidation.ValidationException(
					"Erreur de liaison avec les types d'action. Nécessaire pour le bilan pédagogique et financier");
		}
		// initialiser le libelle de l'article
		String libelle = this.getLibelleParcours();
		if (libelle.length() > 500) {
			libelle = libelle.substring(0, 499);
		}
		this.toCatalogueArticle().article().setArtLibelle(libelle);
		// initialiser le plan comptable
		this.toCatalogueArticle()
				.article()
				.articlePrestation()
				.setPlanComptableRecetteRelationship(
						this.toCatalogueArticle().catalogue()
								.cataloguePrestation().planComptableRecette());

		// initialiser l'indicateur WEB
		if (this.toCatalogueArticle().article().articlePrestation()
				.artpInvisibleWeb() == null) {
			this.toCatalogueArticle().article().articlePrestation()
					.setArtpInvisibleWeb("N");
		}

		// Controle metier pour initialiser les quantite et montants
		EOTypeArticle typeRemise = FinderTypeArticle
				.typeArticleRemise(editingContext());
		if (typeRemise != null
				&& this.toCatalogueArticle().article().typeArticle()
						.equals(typeRemise)) {
			// C'est une remise
			BigDecimal nbMin = new BigDecimal(this.nbMinInscritArt());
			BigDecimal nbMax = new BigDecimal(this.nbMaxInscritArt());

			this.toCatalogueArticle().article().articlePrestation()
					.setArtpQteMin(nbMin);
			this.toCatalogueArticle().article().articlePrestation()
					.setArtpQteMax(nbMax);

			// Mettre les montants HT et TTC en négatif
			if (this.toCatalogueArticle().caarPrixHt() != null) {
				this.toCatalogueArticle().setCaarPrixHt(
						FCUtils.negativerBigDecimal(this.toCatalogueArticle()
								.caarPrixHt()));
			}
			if (this.toCatalogueArticle().caarPrixTtc() != null) {
				this.toCatalogueArticle().setCaarPrixTtc(
						FCUtils.negativerBigDecimal(this.toCatalogueArticle()
								.caarPrixTtc()));
			}
		} else {
			this.toCatalogueArticle().article().articlePrestation()
					.setArtpQteMin(null);
			this.toCatalogueArticle().article().articlePrestation()
					.setArtpQteMax(null);
			// Mettre les montants HT et TTC en positif
			if (this.toCatalogueArticle().caarPrixHt() != null) {
				this.toCatalogueArticle().setCaarPrixHt(
						FCUtils.positiverBigDecimal(this.toCatalogueArticle()
								.caarPrixHt()));
			}
			if (this.toCatalogueArticle().caarPrixTtc() != null) {
				this.toCatalogueArticle().setCaarPrixTtc(
						FCUtils.positiverBigDecimal(this.toCatalogueArticle()
								.caarPrixTtc()));
			}
		}

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave()
			throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

}
