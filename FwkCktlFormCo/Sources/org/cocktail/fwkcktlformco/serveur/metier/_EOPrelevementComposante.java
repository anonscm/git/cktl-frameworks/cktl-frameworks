/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrelevementComposante.java instead.
package org.cocktail.fwkcktlformco.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPrelevementComposante extends AFwkCktlFormCoRecord {
	public static final String ENTITY_NAME = "EOPrelevementComposante";
	public static final String ENTITY_TABLE_NAME = "FORMCO.PRELEVEMENT_COMPOSANTE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cPrelevementComposanteId";

	public static final String CAAR_ID_FK_KEY = "caarIdFK";
	public static final String C_PRELEVEMENT_COMPOSANTE_ID_KEY = "cPrelevementComposanteId";
	public static final String ORG_ID_FK_KEY = "orgIdFK";
	public static final String PRCT_PRELEVEMENT_COMPOSANTE_KEY = "prctPrelevementComposante";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String CAAR_ID_FK_COLKEY = "CAAR_FK";
	public static final String C_PRELEVEMENT_COMPOSANTE_ID_COLKEY = "C_PRELEV_COMPOS_ID";
	public static final String ORG_ID_FK_COLKEY = "ORGID_FK";
	public static final String PRCT_PRELEVEMENT_COMPOSANTE_COLKEY = "PRCT_PRLVT_COMPOS";



	// Relationships
	public static final String TO_CATALOGUE_ARTICLE_KEY = "toCatalogueArticle";
	public static final String TO_ORGAN_KEY = "toOrgan";



	// Accessors methods
  public Integer caarIdFK() {
    return (Integer) storedValueForKey("caarIdFK");
  }

  public void setCaarIdFK(Integer value) {
    takeStoredValueForKey(value, "caarIdFK");
  }

  public Float cPrelevementComposanteId() {
    return (Float) storedValueForKey("cPrelevementComposanteId");
  }

  public void setCPrelevementComposanteId(Float value) {
    takeStoredValueForKey(value, "cPrelevementComposanteId");
  }

  public Integer orgIdFK() {
    return (Integer) storedValueForKey("orgIdFK");
  }

  public void setOrgIdFK(Integer value) {
    takeStoredValueForKey(value, "orgIdFK");
  }

  public Double prctPrelevementComposante() {
    return (Double) storedValueForKey("prctPrelevementComposante");
  }

  public void setPrctPrelevementComposante(Double value) {
    takeStoredValueForKey(value, "prctPrelevementComposante");
  }

  public org.cocktail.kava.server.metier.EOCatalogueArticle toCatalogueArticle() {
    return (org.cocktail.kava.server.metier.EOCatalogueArticle)storedValueForKey("toCatalogueArticle");
  }

  public void setToCatalogueArticleRelationship(org.cocktail.kava.server.metier.EOCatalogueArticle value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOCatalogueArticle oldValue = toCatalogueArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCatalogueArticle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCatalogueArticle");
    }
  }
  
  public org.cocktail.kava.server.metier.EOOrgan toOrgan() {
    return (org.cocktail.kava.server.metier.EOOrgan)storedValueForKey("toOrgan");
  }

  public void setToOrganRelationship(org.cocktail.kava.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOOrgan oldValue = toOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toOrgan");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toOrgan");
    }
  }
  

  public static EOPrelevementComposante createEOPrelevementComposante(EOEditingContext editingContext, Integer caarIdFK
, Float cPrelevementComposanteId
, Integer orgIdFK
, Double prctPrelevementComposante
, org.cocktail.kava.server.metier.EOCatalogueArticle toCatalogueArticle, org.cocktail.kava.server.metier.EOOrgan toOrgan) {
    EOPrelevementComposante eo = (EOPrelevementComposante) createAndInsertInstance(editingContext, _EOPrelevementComposante.ENTITY_NAME);    
		eo.setCaarIdFK(caarIdFK);
		eo.setCPrelevementComposanteId(cPrelevementComposanteId);
		eo.setOrgIdFK(orgIdFK);
		eo.setPrctPrelevementComposante(prctPrelevementComposante);
    eo.setToCatalogueArticleRelationship(toCatalogueArticle);
    eo.setToOrganRelationship(toOrgan);
    return eo;
  }

  
	  public EOPrelevementComposante localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrelevementComposante)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrelevementComposante creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param eoeditingcontext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrelevementComposante creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPrelevementComposante object = (EOPrelevementComposante)createAndInsertInstance(editingContext, _EOPrelevementComposante.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrelevementComposante localInstanceIn(EOEditingContext editingContext, EOPrelevementComposante eo) {
    EOPrelevementComposante localInstance = (eo == null) ? null : (EOPrelevementComposante)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPrelevementComposante#localInstanceIn a la place.
   */
	public static EOPrelevementComposante localInstanceOf(EOEditingContext editingContext, EOPrelevementComposante eo) {
		return EOPrelevementComposante.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPrelevementComposante fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPrelevementComposante fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrelevementComposante eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrelevementComposante)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrelevementComposante fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrelevementComposante fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrelevementComposante eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrelevementComposante)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPrelevementComposante fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrelevementComposante eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrelevementComposante ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrelevementComposante fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
