/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartContratPrestation.java instead.
package org.cocktail.fwkcktlformco.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORepartContratPrestation extends AFwkCktlFormCoRecord {
	public static final String ENTITY_NAME = "EORepartContratPrestation";
	public static final String ENTITY_TABLE_NAME = "FORMCO.REP_CONT_PRES";



	// Attributes


	public static final String RCP_TOTAL_HT_KEY = "rcpTotalHT";
	public static final String RCP_TOTAL_TTC_KEY = "rcpTotalTTC";
	public static final String RCP_TOTAL_TVA_KEY = "rcpTotalTVA";

// Attributs non visibles
	public static final String ETAT_CONTRAT_PRESTATION_ID_KEY = "etatContratPrestationId";
	public static final String PRESTATION_REPART_KEY = "prestationRepart";
	public static final String CONTRAT_REPART_KEY = "contratRepart";

//Colonnes dans la base de donnees
	public static final String RCP_TOTAL_HT_COLKEY = "RCP_TOTAL_HT";
	public static final String RCP_TOTAL_TTC_COLKEY = "RCP_TOTAL_TTC";
	public static final String RCP_TOTAL_TVA_COLKEY = "RCP_TOTAL_TVA";

	public static final String ETAT_CONTRAT_PRESTATION_ID_COLKEY = "C_ETT_CONT_FK";
	public static final String PRESTATION_REPART_COLKEY = "C_PRES_ID";
	public static final String CONTRAT_REPART_COLKEY = "C_CONT_ID";


	// Relationships
	public static final String TO_CW_CONTRAT_KEY = "toCWContrat";
	public static final String TO_ETAT_CONTRAT_KEY = "toEtatContrat";
	public static final String TO_PRESTATION_KEY = "toPrestation";



	// Accessors methods
  public java.math.BigDecimal rcpTotalHT() {
    return (java.math.BigDecimal) storedValueForKey("rcpTotalHT");
  }

  public void setRcpTotalHT(java.math.BigDecimal value) {
    takeStoredValueForKey(value, "rcpTotalHT");
  }

  public java.math.BigDecimal rcpTotalTTC() {
    return (java.math.BigDecimal) storedValueForKey("rcpTotalTTC");
  }

  public void setRcpTotalTTC(java.math.BigDecimal value) {
    takeStoredValueForKey(value, "rcpTotalTTC");
  }

  public java.math.BigDecimal rcpTotalTVA() {
    return (java.math.BigDecimal) storedValueForKey("rcpTotalTVA");
  }

  public void setRcpTotalTVA(java.math.BigDecimal value) {
    takeStoredValueForKey(value, "rcpTotalTVA");
  }

  public org.cocktail.cocowork.server.metier.convention.Contrat toCWContrat() {
    return (org.cocktail.cocowork.server.metier.convention.Contrat)storedValueForKey("toCWContrat");
  }

  public void setToCWContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    if (value == null) {
    	org.cocktail.cocowork.server.metier.convention.Contrat oldValue = toCWContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCWContrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toCWContrat");
    }
  }
  
  public org.cocktail.fwkcktlformco.serveur.metier.EOEtatContrat toEtatContrat() {
    return (org.cocktail.fwkcktlformco.serveur.metier.EOEtatContrat)storedValueForKey("toEtatContrat");
  }

  public void setToEtatContratRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOEtatContrat value) {
    if (value == null) {
    	org.cocktail.fwkcktlformco.serveur.metier.EOEtatContrat oldValue = toEtatContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEtatContrat");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toEtatContrat");
    }
  }
  
  public org.cocktail.kava.server.metier.EOPrestation toPrestation() {
    return (org.cocktail.kava.server.metier.EOPrestation)storedValueForKey("toPrestation");
  }

  public void setToPrestationRelationship(org.cocktail.kava.server.metier.EOPrestation value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPrestation oldValue = toPrestation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPrestation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toPrestation");
    }
  }
  

  public static EORepartContratPrestation createEORepartContratPrestation(EOEditingContext editingContext, java.math.BigDecimal rcpTotalHT
, java.math.BigDecimal rcpTotalTTC
, java.math.BigDecimal rcpTotalTVA
, org.cocktail.cocowork.server.metier.convention.Contrat toCWContrat, org.cocktail.fwkcktlformco.serveur.metier.EOEtatContrat toEtatContrat, org.cocktail.kava.server.metier.EOPrestation toPrestation) {
    EORepartContratPrestation eo = (EORepartContratPrestation) createAndInsertInstance(editingContext, _EORepartContratPrestation.ENTITY_NAME);    
		eo.setRcpTotalHT(rcpTotalHT);
		eo.setRcpTotalTTC(rcpTotalTTC);
		eo.setRcpTotalTVA(rcpTotalTVA);
    eo.setToCWContratRelationship(toCWContrat);
    eo.setToEtatContratRelationship(toEtatContrat);
    eo.setToPrestationRelationship(toPrestation);
    return eo;
  }

  
	  public EORepartContratPrestation localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepartContratPrestation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartContratPrestation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param eoeditingcontext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartContratPrestation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORepartContratPrestation object = (EORepartContratPrestation)createAndInsertInstance(editingContext, _EORepartContratPrestation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORepartContratPrestation localInstanceIn(EOEditingContext editingContext, EORepartContratPrestation eo) {
    EORepartContratPrestation localInstance = (eo == null) ? null : (EORepartContratPrestation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORepartContratPrestation#localInstanceIn a la place.
   */
	public static EORepartContratPrestation localInstanceOf(EOEditingContext editingContext, EORepartContratPrestation eo) {
		return EORepartContratPrestation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORepartContratPrestation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartContratPrestation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartContratPrestation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartContratPrestation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartContratPrestation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartContratPrestation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartContratPrestation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartContratPrestation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORepartContratPrestation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartContratPrestation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartContratPrestation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartContratPrestation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
