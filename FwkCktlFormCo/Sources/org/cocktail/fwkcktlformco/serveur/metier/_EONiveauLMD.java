/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONiveauLMD.java instead.
package org.cocktail.fwkcktlformco.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EONiveauLMD extends AFwkCktlFormCoRecord {
	public static final String ENTITY_NAME = "EONiveauLMD";
	public static final String ENTITY_TABLE_NAME = "FORMCO.NIVEAU_LMD";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cNiveauLMDId";

	public static final String C_NIVEAU_LMD_ID_KEY = "cNiveauLMDId";
	public static final String LIB_CRT_NIVEAU_LMD_KEY = "libCrtNiveauLMD";
	public static final String LIB_LG_NIVEAU_LMD_KEY = "libLgNiveauLMD";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_NIVEAU_LMD_ID_COLKEY = "C_NIV_LMD_ID";
	public static final String LIB_CRT_NIVEAU_LMD_COLKEY = "LC_NIV_LMD";
	public static final String LIB_LG_NIVEAU_LMD_COLKEY = "LL_NIV_LMD";



	// Relationships
	public static final String FWK_FORM_CO__ARTICLE_F_CS_KEY = "fwkFormCo_ArticleFCs";



	// Accessors methods
  public Integer cNiveauLMDId() {
    return (Integer) storedValueForKey("cNiveauLMDId");
  }

  public void setCNiveauLMDId(Integer value) {
    takeStoredValueForKey(value, "cNiveauLMDId");
  }

  public String libCrtNiveauLMD() {
    return (String) storedValueForKey("libCrtNiveauLMD");
  }

  public void setLibCrtNiveauLMD(String value) {
    takeStoredValueForKey(value, "libCrtNiveauLMD");
  }

  public String libLgNiveauLMD() {
    return (String) storedValueForKey("libLgNiveauLMD");
  }

  public void setLibLgNiveauLMD(String value) {
    takeStoredValueForKey(value, "libLgNiveauLMD");
  }

  public NSArray fwkFormCo_ArticleFCs() {
    return (NSArray)storedValueForKey("fwkFormCo_ArticleFCs");
  }

  public NSArray fwkFormCo_ArticleFCs(EOQualifier qualifier) {
    return fwkFormCo_ArticleFCs(qualifier, null, false);
  }

  public NSArray fwkFormCo_ArticleFCs(EOQualifier qualifier, boolean fetch) {
    return fwkFormCo_ArticleFCs(qualifier, null, fetch);
  }

  public NSArray fwkFormCo_ArticleFCs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC.TO_NIVEAU_LMD_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = fwkFormCo_ArticleFCs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToFwkFormCo_ArticleFCsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC object) {
    addObjectToBothSidesOfRelationshipWithKey(object, "fwkFormCo_ArticleFCs");
  }

  public void removeFromFwkFormCo_ArticleFCsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "fwkFormCo_ArticleFCs");
  }

  public org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC createFwkFormCo_ArticleFCsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOArticleFC");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "fwkFormCo_ArticleFCs");
    return (org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC) eo;
  }

  public void deleteFwkFormCo_ArticleFCsRelationship(org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "fwkFormCo_ArticleFCs");
    editingContext().deleteObject(object);
  }

  public void deleteAllFwkFormCo_ArticleFCsRelationships() {
    Enumeration objects = fwkFormCo_ArticleFCs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFwkFormCo_ArticleFCsRelationship((org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC)objects.nextElement());
    }
  }


  public static EONiveauLMD createEONiveauLMD(EOEditingContext editingContext, Integer cNiveauLMDId
, String libLgNiveauLMD
) {
    EONiveauLMD eo = (EONiveauLMD) createAndInsertInstance(editingContext, _EONiveauLMD.ENTITY_NAME);    
		eo.setCNiveauLMDId(cNiveauLMDId);
		eo.setLibLgNiveauLMD(libLgNiveauLMD);
    return eo;
  }

  
	  public EONiveauLMD localInstanceIn(EOEditingContext editingContext) {
	  		return (EONiveauLMD)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONiveauLMD creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param eoeditingcontext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONiveauLMD creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EONiveauLMD object = (EONiveauLMD)createAndInsertInstance(editingContext, _EONiveauLMD.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EONiveauLMD localInstanceIn(EOEditingContext editingContext, EONiveauLMD eo) {
    EONiveauLMD localInstance = (eo == null) ? null : (EONiveauLMD)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EONiveauLMD#localInstanceIn a la place.
   */
	public static EONiveauLMD localInstanceOf(EOEditingContext editingContext, EONiveauLMD eo) {
		return EONiveauLMD.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EONiveauLMD fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EONiveauLMD fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONiveauLMD eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONiveauLMD)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONiveauLMD fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONiveauLMD fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONiveauLMD eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONiveauLMD)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EONiveauLMD fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONiveauLMD eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONiveauLMD ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONiveauLMD fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
