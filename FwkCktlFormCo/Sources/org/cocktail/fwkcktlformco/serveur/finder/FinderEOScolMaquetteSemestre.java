/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.util.Enumeration;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOScolMaquetteSemestre {
	/**
	 * Retourne les semestres a partir de l'annee passee en parametre
	 * 
	 * @param ec
	 * @param annee
	 * @return
	 */
	public static NSArray<EOScolMaquetteSemestre> getSemestresFromYear(
			EOEditingContext ec, EOScolFormationAnnee annee) {
		if (ec == null)
			return null;
		if (annee == null)
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@";
		tbKeys.add(annee);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteSemestre.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<EOScolMaquetteSemestre> resNonTrie = ec
				.objectsWithFetchSpecification(fetchSpec);

		NSMutableArray sortOrderings = new NSMutableArray();
		EOSortOrdering tri1 = new EOSortOrdering(
				EOScolMaquetteSemestre.MSEM_ORDRE_KEY,
				EOSortOrdering.CompareAscending);
		sortOrderings.add(tri1);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(resNonTrie,
				sortOrderings);
	}

	/**
	 * Retourne les semestres en fonction de l'anee et du parcours.
	 * 
	 * @param ec
	 * @param annee
	 * @param parcours
	 * @return
	 */
	public static NSArray<EOScolMaquetteSemestre> getSemestreFromParcoursAndYear(
			EOEditingContext ec, EOScolFormationAnnee annee,
			EOScolMaquetteParcours parcours) {
		if (ec == null)
			return null;
		if (annee == null)
			return null;
		if (parcours == null)
			return null;
		NSArray<EOScolMaquetteSemestre> res = new NSArray<EOScolMaquetteSemestre>();
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@ AND "
				+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY
				+ "."
				+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY
				+ " = %@ ";
		tbKeys.add(annee);
		tbKeys.add(parcours);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteSemestre.ENTITY_NAME, qual, null);

		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		res = ec.objectsWithFetchSpecification(fetchSpec);
		return res;
	}

	public static EOScolMaquetteSemestre getSemestre(EOEditingContext ec,
			EOScolFormationAnnee annee, EOScolMaquetteRepartitionEc mrec) {
		if (ec == null)
			return null;
		if (annee == null)
			return null;
		if (mrec == null)
			return null;
		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.add(new EOKeyValueQualifier(EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, annee));
		quals.add(new EOKeyValueQualifier(EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY, EOQualifier.QualifierOperatorEqual, mrec.toFwkScolarite_ScolMaquetteUe()));
		
		NSArray<EOScolMaquetteRepartitionUe> repUEs = EOScolMaquetteRepartitionUe.fetchAll(ec, new EOAndQualifier(quals));
		if(repUEs!=null)
		{ 
			Enumeration<EOScolMaquetteRepartitionUe> enumrepUe = repUEs.objectEnumerator();
			while (enumrepUe.hasMoreElements())
			{
				EOScolMaquetteRepartitionUe repUE = (EOScolMaquetteRepartitionUe)enumrepUe.nextElement();
				return repUE.toFwkScolarite_ScolMaquetteSemestre();
			}
		}
		return null;	
	}

}
