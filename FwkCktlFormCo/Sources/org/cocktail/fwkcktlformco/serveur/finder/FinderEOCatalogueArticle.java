/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC;
import org.cocktail.kava.server.finder.FinderTypeArticle;
import org.cocktail.kava.server.metier.EOArticle;
import org.cocktail.kava.server.metier.EOArticlePrestation;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOTypePublic;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOCatalogueArticle {

	public static NSArray find(EOEditingContext ec, EOCatalogue catalogue,
			EOTypePublic typePublic, EOTypeEtat typeEtat) {
		if (catalogue == null) {
			return new NSArray();
		}
		NSMutableArray quals = new NSMutableArray();
		NSArray<Object> typeArticleParam= new NSMutableArray<Object>();
		typeArticleParam.add(FinderTypeArticle.typeArticleArticle(ec));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCatalogueArticle.ARTICLE_KEY + "."
						+ EOArticle.TYPE_ARTICLE_KEY + " = %@", typeArticleParam));
		if (typeEtat != null) {
			NSArray<Object> typeEtatParam= new NSMutableArray<Object>();
			typeEtatParam.add(typeEtat);
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCatalogueArticle.TYPE_ETAT_KEY + " = %@", typeEtatParam));
		}
		if (catalogue != null) {
			NSArray<Object> catalogueParam= new NSMutableArray<Object>();
			catalogueParam.add(catalogue);
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCatalogueArticle.CATALOGUE_KEY + " = %@", catalogueParam));
		}
		if (typePublic != null) {
			NSArray<Object> typePublicParam= new NSMutableArray<Object>();
			typePublicParam.add(typePublic);
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCatalogueArticle.ARTICLE_KEY + "."
							+ EOArticle.ARTICLE_PRESTATION_KEY + "."
							+ EOArticlePrestation.TYPE_PUBLIC_KEY + " = %@ or "
							+ EOCatalogueArticle.ARTICLE_KEY + "."
							+ EOArticle.ARTICLE_PRESTATION_KEY + "."
							+ EOArticlePrestation.TYPE_PUBLIC_KEY + " = nil",
					typePublicParam));
		}
		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(
				EOCatalogueArticle.CAAR_REFERENCE_KEY,
				EOSortOrdering.CompareAscending));

		EOFetchSpecification fs = new EOFetchSpecification(
				EOCatalogueArticle.ENTITY_NAME, new EOAndQualifier(quals), sort);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

//	/**
//	 * Retourne le catalogueArticle correspondant
//	 * 
//	 * @param ec
//	 * @param catalogue
//	 * @param article
//	 * @return
//	 */
//	public static EOCatalogueArticle find(EOEditingContext ec,
//			EOCatalogue catalogue, EOArticleFC articleFC) {
//		if (ec == null)
//			return null;
//		if (catalogue == null) {
//			return null;
//		}
//		if (articleFC == null)
//			return null;
//		NSArray<EOCatalogueArticle> res = null;
//		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
//		String conditionStr = EOCatalogueArticle.CATALOGUE_KEY + " = %@ AND "
//				+ EOCatalogueArticle.ARTICLE_KEY + " = %@";
//		tbKeys.add(catalogue);
//		tbKeys.add(articleFC.t);
//		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
//				conditionStr, tbKeys);
//		EOFetchSpecification fetchSpec = new EOFetchSpecification(
//				EOCatalogueArticle.ENTITY_NAME, qual, null);
//		res = ec.objectsWithFetchSpecification(fetchSpec);
//		if (res.count() > 1)
//			return null;
//		return res.get(0);
//	}

}
