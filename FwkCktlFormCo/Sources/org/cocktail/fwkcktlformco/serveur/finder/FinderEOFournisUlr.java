/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.util.Enumeration;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.kava.server.metier.EOFournisUlr;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOFournisUlr {
	public static NSArray findByStr(EOEditingContext ec, String str)
			throws FormcoException {
		if (ec == null)
			throw new FormcoException(
					"Recherche des fournisseur impossible. pas d'EC");
		if (str.length() >= 3) {
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOFournisUlr.ADR_NOM_KEY
					+ " caseInsensitiveLike '*" + str + "*' or "
					+ EOFournisUlr.ADR_PRENOM_KEY + " caseInsensitiveLike '*"
					+ str + "*' AND " + EOFournisUlr.FOU_VALIDE_KEY + "= 'O'";
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, null);
			EOFetchSpecification fetchSpec = new EOFetchSpecification(
					EOFournisUlr.ENTITY_NAME, qual, null);

			return ec.objectsWithFetchSpecification(fetchSpec);
		}
		return null;
	}

	public static NSArray findPartenaire(EOEditingContext ec, String str,
			Contrat contratFC) {
		if (ec == null || contratFC == null || str==null)
			return null;

		NSArray<ContratPartenaire> cps = contratFC.contratPartenaires();
		Enumeration<ContratPartenaire> enumCps = cps.objectEnumerator();
		NSMutableArray quals = new NSMutableArray();

		NSMutableArray<Object> tbKeys1 = new NSMutableArray<Object>();
		String conditionStr1 = EOFournisUlr.ADR_NOM_KEY
				+ " caseInsensitiveLike '*" + str + "*' or "
				+ EOFournisUlr.ADR_PRENOM_KEY + " caseInsensitiveLike '*" + str
				+ "*' AND " + EOFournisUlr.FOU_VALIDE_KEY + "= 'O'";
		EOQualifier qual1 = EOQualifier.qualifierWithQualifierFormat(
				conditionStr1, null);
		quals.addObject(qual1);

		while (enumCps.hasMoreElements()) {
			ContratPartenaire cp = enumCps.nextElement();
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOFournisUlr.PERS_ID_KEY + " != %@  ";
			tbKeys.add(cp.persId());
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			quals.addObject(qual);
		}

		// NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(
		// EOFournis..PERSONNE_PERS_NOM_PRENOM_KEY,
		// EOSortOrdering.CompareAscending));

		EOFetchSpecification fs = new EOFetchSpecification(
				EOFournisUlr.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	/**
	 * Trouver des fournisseurs a la creation d'un contrat de FC. Pas de contrat initial puisque c la creation
	 * @param ec
	 * @param str
	 * @return
	 */
	public static NSArray findPartenaire(EOEditingContext ec, String str) {
		if (ec == null || str==null)
			return null;

		NSMutableArray quals = new NSMutableArray();

		NSMutableArray<Object> tbKeys1 = new NSMutableArray<Object>();
		String conditionStr1 = EOFournisUlr.ADR_NOM_KEY
				+ " caseInsensitiveLike '*" + str + "*' or "
				+ EOFournisUlr.ADR_PRENOM_KEY + " caseInsensitiveLike '*" + str
				+ "*' AND " + EOFournisUlr.FOU_VALIDE_KEY + "= 'O'";
		EOQualifier qual1 = EOQualifier.qualifierWithQualifierFormat(
				conditionStr1, null);
		quals.addObject(qual1);

		EOFetchSpecification fs = new EOFetchSpecification(
				EOFournisUlr.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
}