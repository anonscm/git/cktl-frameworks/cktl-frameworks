/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOCompteEmail {

	/**
	 * Retourne tous les emails d'un user a partir de son persId
	 * @param ec
	 * @param persId
	 * @return
	 */
	public static String getAllEmail(EOEditingContext ec, int persId) {
		if (ec == null)
			return null;
		if (persId <= 0)
			return null;

		String resEmails = "";
		// Recuperer les comptes du user
		String condStr1 = EOCompte.PERS_ID_KEY + " = %@ ";

		NSMutableArray<Object> tbKeys1 = new NSMutableArray<Object>();
		tbKeys1.add(persId);

		EOQualifier qual1 = EOQualifier.qualifierWithQualifierFormat(condStr1,
				tbKeys1);

		EOFetchSpecification fetchSpec1 = new EOFetchSpecification(
				EOCompte.ENTITY_NAME, qual1, null);
		NSArray<EOCompte> res1 = ec.objectsWithFetchSpecification(fetchSpec1);
		
		if ((res1 == null) || (res1.isEmpty()))
			return null;
		
		// Pour chaque compte, recuperer les emails
		for (EOCompte compte : res1) {
			String condStr2 = EOCompteEmail.CPT_ORDRE_KEY + " =%@";
			NSMutableArray<Object> tbKeys2 = new NSMutableArray<Object>();
			tbKeys2.add(compte._cptOrdreConsult());
			EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat(
					condStr2, tbKeys2);
			EOFetchSpecification fetchSpec2 = new EOFetchSpecification(
					EOCompteEmail.ENTITY_NAME, qual2, null);
			NSArray<EOCompteEmail> res2 = ec
					.objectsWithFetchSpecification(fetchSpec2);
			if ((res2 != null) && (!res2.isEmpty())) {
				for (EOCompteEmail email : res2) {
					resEmails += email.cemEmail() + "@" + email.cemDomaine() +",";
				}
			}
		}

		return resEmails;
	}

}
