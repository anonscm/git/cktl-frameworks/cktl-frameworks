/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC;
import org.cocktail.kava.server.metier.EOBoutique;
import org.cocktail.kava.server.metier.EOBoutiqueCatalogue;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOBoutique {

	public static EOBoutique findBoutiqueFromArticleFC(EOEditingContext ec,
			EOArticleFC article) {
		if (article == null) {
			return null;
		}
		NSArray<EOBoutiqueCatalogue> resIntermediaire = new NSArray<EOBoutiqueCatalogue>();
		NSArray<EOBoutique> res = new NSArray<EOBoutique>();

		// trouver le boutiqueCatalogue
		EOCatalogue catalogueArticle = article.toCatalogueArticle().catalogue();
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();

		if (catalogueArticle != null) {
			String conditionStr = EOBoutiqueCatalogue.CATALOGUE_KEY + " = %@";
			tbKeys.add(catalogueArticle);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			EOFetchSpecification fetchSpec = new EOFetchSpecification(
					EOBoutiqueCatalogue.ENTITY_NAME, qual, null);
			fetchSpec.setUsesDistinct(true);
			fetchSpec.setIsDeep(true);
			NSArray<String> tabParam = new NSMutableArray<String>();
			tabParam.add("catalogue");
			tabParam.add("boutique");
			fetchSpec.setPrefetchingRelationshipKeyPaths(tabParam);
			resIntermediaire = ec.objectsWithFetchSpecification(fetchSpec);
			if ((resIntermediaire == null) || (resIntermediaire.isEmpty()))
				return null;
			// Retourner la boutique
			EOBoutiqueCatalogue boutiqueCatalogue = (EOBoutiqueCatalogue) resIntermediaire
					.get(0);
			return boutiqueCatalogue.boutique();
		}
		return null;
	}
	
	/**
	 * Renvoie true dés qu'une boutique du catalogue a isPaiementWebPossible a true;
	 * @param prest
	 * @return
	 */
	public static boolean getIsPaiementWebPossibleFromPrestation(EOPrestation prest)
	{
		if(prest==null)return false;
		EOCatalogue cat = prest.catalogue();
		NSArray<EOBoutiqueCatalogue> boutCats = cat.boutiquesCatalogues();
		Enumeration<EOBoutiqueCatalogue> enumBoutCats = boutCats.objectEnumerator();
		while (enumBoutCats.hasMoreElements())
		{
			EOBoutiqueCatalogue boutCat = (EOBoutiqueCatalogue) enumBoutCats.nextElement();
			if(boutCat.boutique().isPaiementWebPossible()) return true;
		}
		return false;
	}
}
