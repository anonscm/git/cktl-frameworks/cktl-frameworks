/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.util.Enumeration;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.EOContratPartContact;
import org.cocktail.cocowork.server.metier.grhum.IndividuUlr;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOControlUtilities;

public abstract class FinderContratPartenaireContact {
	public static final String ROLE_STAG_FC = "STAGIAIRE FORMATION CONTINUE";

	public static NSArray<EOContratPartContact> getContratPartenaireContactFromContrat(
			EOEditingContext ec, Contrat contratFC) {
		if (ec == null || contratFC == null)
			return null;
		NSArray<ContratPartenaire> cps = contratFC.contratPartenaires();
		Enumeration<ContratPartenaire> enumCps = cps.objectEnumerator();
		NSMutableArray quals = new NSMutableArray();

		while (enumCps.hasMoreElements()) {
			ContratPartenaire aCP = (ContratPartenaire) enumCps.nextElement();
			// ne pas renvoyer les partprincipaux (en FC, c'est l'organisme de
			// FC)
			if (!aCP.cpPrincipalBoolean())
				quals.add(new EOKeyValueQualifier(
						ContratPartContact.CONTRAT_PARTENAIRE_KEY,
						EOQualifier.QualifierOperatorEqual, aCP));
		}

		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(
				EOContratPartContact.PERS_ID_CONTACT_KEY,
				EOSortOrdering.CompareAscending));

		EOFetchSpecification fs = new EOFetchSpecification(
				EOContratPartContact.ENTITY_NAME, new EOOrQualifier(quals),
				sort);
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * Retourne les etudiants partenaires Contact du contrat passé en parametre
	 * 
	 * @param ec
	 * @param contrat
	 * @return
	 */
	public static NSArray<EOEtudiant> getStagiaireFromContrat(
			EOEditingContext ec, Contrat contrat) throws FormcoException {
		if (ec == null || contrat == null)
			return new NSArray<EOEtudiant>();
		EOAssociation stagiaireAsso = EOAssociation.fetchByKeyValue(ec,
				EOAssociation.ASS_LIBELLE_KEY, ROLE_STAG_FC);
		if (stagiaireAsso == null)
			throw new FormcoException(
					"Impossible, l'association STAGIAIRE DE FORMATION CONTINUE n'existe pas");
//		 pb ici.il faut recuperer le cStructureID de la repartAsso (voir a
//		 quoi ça correspont et qd cmis a jour!)
//		 utilisé apres dans la verif d'ajout d'un stagiaire au contrat pour ne pas ajouter plus de stagiaire que de places payées
		try {
			NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			IPersonne personneEtudiant = null;
			NSArray<EOContratPartContact> contratPartContacts = FinderContratPartenaireContact
					.getContratPartenaireContactFromContrat(ec, contrat);

			EOContratPartContact contraPartContact = null;
			if (contratPartContacts.count() > 0) {
				Enumeration<EOContratPartContact> enumCPCts = contratPartContacts
						.objectEnumerator();
				while (enumCPCts.hasMoreElements()) {
					personneEtudiant = null;
					contraPartContact = (EOContratPartContact) enumCPCts
							.nextElement();
					NSArray<EOQualifier> qualsRole = new NSMutableArray<EOQualifier>();
					qualsRole.add(new EOKeyValueQualifier(
							EORepartAssociation.TO_ASSOCIATION_KEY,
							EOQualifier.QualifierOperatorEqual, stagiaireAsso));
					qualsRole.add(new EOKeyValueQualifier(
							EORepartAssociation.PERS_ID_KEY,
							EOQualifier.QualifierOperatorEqual,
							contraPartContact.persIdContact()));
					String cStructureId = ERXEOControlUtilities.primaryKeyStringForObject(contraPartContact.contratPartenaire().contrat().groupePartenaire());
					qualsRole.add(new EOKeyValueQualifier(EORepartAssociation.C_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, cStructureId));

					EORepartAssociation assoStagiaire = EORepartAssociation
							.fetchByQualifier(ec, new EOAndQualifier(qualsRole));

					if (assoStagiaire != null) {
						personneEtudiant = (IPersonne) EOUtilities
								.objectMatchingKeyAndValue(ec,
										IndividuUlr.ENTITY_NAME,
										IndividuUlr.PERS_ID_KEY,
										contraPartContact.persIdContact());
						// rechercher l'individu correspondant au noindivi de la
						// personne
						// rechercher l'etudiant avec ce noindividu.
						quals.add(new EOKeyValueQualifier(
								EOEtudiant.NO_INDIVIDU_KEY,
								EOQualifier.QualifierOperatorEqual, Integer
										.parseInt(personneEtudiant.getNumero())));
					}
				}
				NSArray sort = new NSArray(
						EOSortOrdering.sortOrderingWithKey(
								EOEtudiant.TO_INDIVIDU + "."
										+ EOIndividu.NOM_USUEL_KEY,
								EOSortOrdering.CompareAscending));

				EOFetchSpecification fs = new EOFetchSpecification(
						EOEtudiant.ENTITY_NAME, new EOOrQualifier(quals), sort);
				fs.setRefreshesRefetchedObjects(true);
				fs.setUsesDistinct(true);
				fs.setIsDeep(true);

				return EOEtudiant.fetchAll(ec, new EOOrQualifier(quals));

			}
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOEtudiant>();
		}
		return null;

	}
}
