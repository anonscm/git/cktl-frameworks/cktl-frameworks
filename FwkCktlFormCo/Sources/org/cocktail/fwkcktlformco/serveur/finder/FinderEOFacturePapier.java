/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.kava.server.metier.EOFacturePapier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOFacturePapier {
	/**
	 * Retourne les factures du client dont le persId est passe en parametre
	 * 
	 * @param ec
	 * @param persId
	 * @return
	 * @throws FormcoException
	 */
	public static NSArray<EOFacturePapier> getFacturesFromClient(EOEditingContext ec,
			int persId) throws FormcoException {
//		if ((ec == null) || (persId == 0)) {
//			throw new FormcoException(
//					"Recuperation des factures du client impossible. ec ou persId null.");
//		}
//		EOQualifier qualFacture = EOQualifier.qualifierWithQualifierFormat(
//				EOFacture.PERS_ID_KEY + "=%@", new NSArray(
//						new Object[] { persId }));
//		NSArray<EOFacturePapier> listFacturePapiers = EOFacturePapier.fetchAll(
//				ec, qualFacture);
//		return listFacturePapiers;
		return getFacturesFromClientAndContrat(ec, null, persId);
	}
	
	/**
	 * Renvoie toutes les factures reliées au contrat
	 * @param ec
	 * @param contrat
	 * @return
	 */
	public static NSArray<EOFacturePapier> getFacturesFromContrat(EOEditingContext ec, Contrat contrat)
	{
		if(ec==null || contrat==null)return null;
		
//		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
//		String conditionStr = EOFacturePapier.CON_ORDRE_KEY+" = %@ ";
//		tbKeys.add(contrat.conOrdre());
//		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
//				conditionStr, tbKeys);
//		EOFetchSpecification fetchSpec = new EOFetchSpecification(
//				EOFacturePapier.ENTITY_NAME, qual, null);
//
//		fetchSpec.setIsDeep(true);
//		fetchSpec.setUsesDistinct(true);
//		return ec.objectsWithFetchSpecification(fetchSpec);
		return getFacturesFromClientAndContrat(ec, contrat,0);
		
	}
	
	public static NSArray<EOFacturePapier> getFacturesFromClientAndContrat(EOEditingContext ec, Contrat contrat, int persId)
	{
		
		if(ec==null)return null;
		
		NSMutableArray quals = new NSMutableArray();
		
		if(contrat!=null) {
			quals.addObject( qual(contrat) );
		}
		
		if (persId > 0) {
			quals.addObject(qual(persId));
		}
		
		EOFetchSpecification fs = new EOFetchSpecification(EOFacturePapier.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
		
	}
	
	private static EOQualifier qual(Contrat contrat) {
		NSArray<Object> tabParam = new NSMutableArray<Object>();
		tabParam.add(contrat.conOrdre());
		return EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.CON_ORDRE_KEY
				+ "=%@", tabParam);
	}
	
	private static EOQualifier qual(int persId)
	{
		NSArray<Object> tabParam = new NSMutableArray<Object>();
		tabParam.add(persId);
		return EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.PERS_ID_KEY
				+ "=%@", tabParam);
	}
	
	public static EOFacturePapier getFactureFromPartenaire(EOEditingContext ec,  ContratPartenaire partenaire) throws FormcoException
	{
		EOFacturePapier fap=null;
		if(ec==null || partenaire==null)return null;
		try {
			NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.add(new EOKeyValueQualifier(EOFacturePapier.CON_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, partenaire.contrat().conOrdre()));
			quals.add(new EOKeyValueQualifier(EOFacturePapier.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, partenaire.persId()));
			fap= EOFacturePapier.fetchByQualifier(ec, new EOAndQualifier(quals));
		} catch (Exception e) {
			e.printStackTrace();
			throw new FormcoException(e.getMessage());
		}
		
		return fap;
		
	}
	
	
}
