/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class FinderEOIndividu {

	public static NSArray<EOIndividu> findIndividu(EOEditingContext ec, String nom,
			String prenom, NSTimestamp dateNaissance) {
		if ((ec == null) || (nom == null) )
			return null;

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOIndividu.NOM_USUEL_KEY
				+ " caseInsensitiveLike %@ ";
		tbKeys.add(nom);

		if (dateNaissance != null) {
			conditionStr += "AND " + EOIndividu.D_NAISSANCE_KEY
					+ " = %@ ";
			tbKeys.add(dateNaissance);
		}
		if (prenom != null) {
			conditionStr += "AND " + EOIndividu.PRENOM_KEY
					+ " caseInsensitiveLike %@  ";
			tbKeys.add(prenom);
		}
		conditionStr+=";";
		
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		return EOIndividu.fetchAll(ec, qual);
	}
	
public static NSArray<EOIndividu> findIndividuValide(EOEditingContext ec, String strToSearch) throws FormcoException
{
	if(ec==null || strToSearch==null)
	{
		throw new FormcoException("Impossible de rechercher un individu valide");
	}
	NSArray<EOQualifier>qualInd = new NSMutableArray<EOQualifier>();
	qualInd.add(new EOKeyValueQualifier(EOIndividu.NOM_USUEL_KEY,
			EOQualifier.QualifierOperatorCaseInsensitiveLike, strToSearch));
	qualInd.add(new EOKeyValueQualifier(EOIndividu.PRENOM_KEY,
			EOQualifier.QualifierOperatorCaseInsensitiveLike, strToSearch));
	NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(
			EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
	return EOIndividu.fetchAllValides(ec, new EOOrQualifier(qualInd), sort);
}
}
