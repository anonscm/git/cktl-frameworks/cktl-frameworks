/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.io.IOException;
import java.math.BigDecimal;

import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.utils.PrimaryKeyGetter;
import org.cocktail.scolaritefwk.serveur.factory.FactoryScolFormationDomaine;
import org.cocktail.scolaritefwk.serveur.factory.FactoryScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationVocation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;
import org.cocktail.scolarix.serveur.finder.FinderRne;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite;
import org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Classe Abstraite <br/>
 * Utilisée pour extraire les différents types de parcours accessibles en
 * formation continue.
 * 
 */
public abstract class FinderParcoursPedagogique {

	public static NSArray<EOScolMaquetteRepartitionAp> getRepAPFromEC(
			EOEditingContext ec, EOScolMaquetteEc ECPedago) {
		if (ec == null)
			return null;
		if (ECPedago == null)
			return null;

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		NSArray<EOScolMaquetteRepartitionAp> resTrie = new NSArray<EOScolMaquetteRepartitionAp>();
		NSArray<EOScolMaquetteRepartitionAp> resNonTrie = new NSArray<EOScolMaquetteRepartitionAp>();
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);

		String conditionStr = EOScolMaquetteRepartitionAp.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@ AND "
				+ EOScolMaquetteRepartitionAp.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY
				+ " = %@";
		tbKeys.add(annee);
		tbKeys.add(ECPedago);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteRepartitionAp.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<String> tabParam = new NSMutableArray<String>();
		tabParam.add("toFwkScolarite_ScolMaquetteAp");
		tabParam.add("toFwkScolarite_ScolMaquetteEc");
		fetchSpec.setPrefetchingRelationshipKeyPaths(tabParam);
		resNonTrie = ec.objectsWithFetchSpecification(fetchSpec);
		if ((resNonTrie == null) || (resNonTrie.isEmpty()))
			return resNonTrie;

		NSMutableArray sortOrderings = new NSMutableArray();
		EOSortOrdering tri1 = new EOSortOrdering(
				EOScolMaquetteRepartitionAp.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY
						+ "." + EOScolMaquetteEc.MEC_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		sortOrderings.add(tri1);

		EOSortOrdering tri2 = new EOSortOrdering(
				EOScolMaquetteRepartitionAp.TO_FWK_SCOLARITE__SCOL_MAQUETTE_AP_KEY
						+ "." + EOScolMaquetteAp.MAP_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		sortOrderings.add(tri2);
		resTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(resNonTrie,
				sortOrderings);

		return resTrie;
	}

	/**
	 * retourne les Repart EC correspondant a une UE
	 * 
	 * @param ec
	 * @param ue
	 * @return
	 */
	public static NSArray<EOScolMaquetteRepartitionEc> getRepECFromUE(
			EOEditingContext ec, EOScolMaquetteUe ue) {
		if (ec == null)
			return null;
		if (ue == null)
			return null;

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);
		String conditionStr = EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@ AND "
				+ EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY
				+ " = %@";
		tbKeys.add(annee);
		tbKeys.add(ue);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteRepartitionEc.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<String> tabParam = new NSMutableArray<String>();
		tabParam.add("toFwkScolarite_ScolMaquetteUe");
		tabParam.add("toFwkScolarite_ScolMaquetteEc");
		fetchSpec.setPrefetchingRelationshipKeyPaths(tabParam);

		NSArray<EOScolMaquetteRepartitionEc> resNonTrie = ec
				.objectsWithFetchSpecification(fetchSpec);

		NSMutableArray sortOrderings = new NSMutableArray();
		EOSortOrdering tri1 = new EOSortOrdering(
				EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY
						+ "." + EOScolMaquetteUe.MUE_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		sortOrderings.add(tri1);

		EOSortOrdering tri2 = new EOSortOrdering(
				EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY
						+ "." + EOScolMaquetteEc.MEC_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		sortOrderings.add(tri2);

		EOSortOrdering tri3 = new EOSortOrdering(
				EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY
						+ "." + EOScolMaquetteEc.MEC_POINTS_KEY,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri3);

		NSArray<EOScolMaquetteRepartitionEc> resTrie = EOSortOrdering
				.sortedArrayUsingKeyOrderArray(resNonTrie, sortOrderings);

		return resTrie;
	}
	
	/**
	 * Retourne les repartEC a partir de l'annee
	 * @param ec
	 * @return
	 */
	public static NSArray<EOScolMaquetteRepartitionEc> getRepECFromYear(
			EOEditingContext ec, EOScolFormationAnnee annee) {
		if (ec == null)
			return null;
		if(annee==null)
			 annee = FinderScolFormationAnnee
			.getScolFormationAnneeCourante(ec);

		NSArray<EOScolMaquetteRepartitionEc> resRepUE = new NSArray<EOScolMaquetteRepartitionEc>();

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@";
		tbKeys.add(annee);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteRepartitionEc.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
NSArray<String>tabParam = new NSMutableArray<String>();
tabParam.add("toFwkScolarite_ScolMaquetteUe");
tabParam.add("toFwkScolarite_ScolMaquetteEc");
		fetchSpec.setPrefetchingRelationshipKeyPaths(tabParam);

		NSArray<EOScolMaquetteRepartitionEc> resNonTrie = ec
				.objectsWithFetchSpecification(fetchSpec);

		NSMutableArray sortOrderings = new NSMutableArray();
		EOSortOrdering tri1 = new EOSortOrdering(
				EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY
						+ "." + EOScolMaquetteUe.MUE_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		sortOrderings.add(tri1);

		EOSortOrdering tri2 = new EOSortOrdering(
				EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY
						+ "." + EOScolMaquetteEc.MEC_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		sortOrderings.add(tri2);

		EOSortOrdering tri3 = new EOSortOrdering(
				EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY
						+ "." + EOScolMaquetteEc.MEC_POINTS_KEY,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri3);

		NSArray<EOScolMaquetteRepartitionEc> resTrie = EOSortOrdering
				.sortedArrayUsingKeyOrderArray(resNonTrie, sortOrderings);

		return resTrie;
	}
	
	
	

	/**
	 * Récupère les EC pour l'annee en cours. On peut spécifier un libelle pour
	 * rechercher ds le libelle de l'EC
	 * 
	 * @param ec
	 * @param ue
	 * @param number
	 * @param addQual
	 * @return
	 */
	public static NSArray<EOScolMaquetteEc> getEcFromString(
			EOEditingContext ec, String strToSearch) {

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);

		String conditionStr = EOScolMaquetteEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@";
		tbKeys.add(annee);
		// CktlLog.log(" - "+conditionStr);
		EOQualifier qualFinal = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOQualifier qualToAdd = null;
		if (strToSearch != null && !strToSearch.trim().equals("")) {
			qualToAdd = EOQualifier.qualifierWithQualifierFormat(
					EOScolMaquetteEc.MEC_LIBELLE_KEY
							+ " caseInsensitiveLike '*" + strToSearch
							+ "*' or " + EOScolMaquetteEc.MEC_CODE_KEY
							+ " caseInsensitiveLike '*" + strToSearch
							+ "*' or " + EOScolMaquetteEc.FDSC_KEY_KEY
							+ " caseInsensitiveLike '*" + strToSearch
							+ "*' or " + EOScolMaquetteEc.MEC_LIBELLE_COURT_KEY
							+ " caseInsensitiveLike '*" + strToSearch + "*' ",
					null);
		}

		if (qualToAdd != null) {
			qualFinal = new EOAndQualifier(new NSArray<EOQualifier>(
					new EOQualifier[] { qualFinal, qualToAdd }));
		}

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteEc.ENTITY_NAME, qualFinal,
				new NSArray(new Object[] { new EOSortOrdering(
						EOScolMaquetteEc.MEC_LIBELLE_KEY,
						EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * Retourne les diplomes de tous les domaines pour l'annee en cours et
	 * correspondant à l'année saisie
	 * 
	 * @param ec
	 * @param selectedDomaine
	 * @param year
	 * @param strToSearch
	 * @return
	 */
	public static NSArray<EOScolFormationDiplome> getDiplomeForStrInput(
			EOEditingContext ec, String strToSearch) {
		if (ec == null)
			return null;
		if (strToSearch == null)
			return null;
		if (strToSearch.trim().equals(""))
			return null;
		if (strToSearch.trim().length() < 3)
			return null;

		// Recupere l'annee en cours et les domaines
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);
		NSArray domainesArray = EOScolFormationDomaine.fetchAll(ec);
		NSArray<EOScolFormationDiplome> diplomesArray = new NSArray<EOScolFormationDiplome>();

		for (Object aDomaineObject : domainesArray) {
			EOScolFormationDomaine aDomaine = (EOScolFormationDomaine) aDomaineObject;
			NSArray<EOScolFormationDiplome> diplomes = findDiplomes(ec,
					aDomaine, annee, strToSearch);
			diplomesArray = diplomesArray
					.arrayByAddingObjectsFromArray(diplomes);
		}
		return diplomesArray;

	}

	/**
	 * Retourne les diplomes de l'annee en cours pour les domaines passés en
	 * parametres
	 * 
	 * @param arrayDomaines
	 * @return
	 */
	public static NSArray<EOScolFormationDiplome> getDiplomeFromDomaine(
			EOEditingContext ec, NSArray<EOScolFormationDomaine> arrayDomaines) {
		NSArray<EOScolFormationDiplome> res = new NSArray<EOScolFormationDiplome>();
		if (ec == null)
			return null;
		if (arrayDomaines == null)
			return null;
		if (arrayDomaines.isEmpty())
			return null;
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);
		for (int i = 0; i < arrayDomaines.size(); i++) {
			res = res.arrayByAddingObjectsFromArray(FinderScolFormationDiplome
					.getDiplomesForDomaineAndYear(ec, arrayDomaines.get(i),
							annee));
		}
		return res;
	}

	/**
	 * Retourne les parcours a partir d'un diplome selectionne
	 * 
	 * @param ec
	 * @param diplome
	 * @return
	 */
	public static NSMutableArray<EOScolMaquetteParcours> getParcoursFromDiplome(
			EOEditingContext ec, EOScolFormationDiplome diplome) {
		NSMutableArray<EOScolMaquetteParcours> res = new NSMutableArray<EOScolMaquetteParcours>();
		if (ec == null)
			return null;
		if (diplome == null)
			return null;
		if (diplome.toFwkScolarite_ScolFormationSpecialisations() == null)
			return null;
		NSArray<EOScolFormationSpecialisation> spes = diplome
				.toFwkScolarite_ScolFormationSpecialisations();

		EOScolFormationSpecialisation aSpe = null;
		if (!spes.isEmpty()) {
			for (int i = 0; i < spes.count(); i++) {
				aSpe = spes.get(i);
				if (aSpe.toFwkScolarite_ScolMaquetteParcourss() != null) {
					if (!aSpe.toFwkScolarite_ScolMaquetteParcourss().isEmpty()) {
						res.addAll(aSpe.toFwkScolarite_ScolMaquetteParcourss());
					}
				}
			}
		}

		return res;

	}

	/**
	 * Retourne les semestres à partir des parcours
	 * 
	 * @param ec
	 * @param arrayParcours
	 * @return
	 */
	public static NSArray<EOScolMaquetteSemestre> getSemestreFromParcours(
			EOEditingContext ec, NSArray<EOScolMaquetteParcours> arrayParcours) {
		if (ec == null)
			return null;
		if (arrayParcours == null)
			return null;
		if (arrayParcours.size() <= 0)
			return null;

		NSArray<EOScolMaquetteSemestre> res = new NSArray<EOScolMaquetteSemestre>();
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);

		for (int i = 0; i < arrayParcours.size(); i++) {
			res = res.arrayByAddingObjectsFromArray(FinderScolMaquetteParcours
					.getSemestresForParcoursAndYear(ec, arrayParcours.get(i),
							annee));
		}

		return res;

	}

	/**
	 * Retourne tous les enregistrements de RepartSem pour l'annee en cours
	 * 
	 * @param ec
	 * @param annee
	 * @return
	 */
	public static NSArray<EOScolMaquetteRepartitionSem> getRepartSemestreForYear(
			EOEditingContext ec) {
		if (ec == null)
			return null;

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);
		String conditionStr = EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@";
		tbKeys.add(annee);

		// CktlLog.log(" - "+conditionStr);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		NSMutableArray sortOrderings = new NSMutableArray();
		EOSortOrdering tri1 = new EOSortOrdering(
				EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY
						+ "." + EOScolMaquetteSemestre.MSEM_POINTS_KEY,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri1);

		EOSortOrdering tri2 = new EOSortOrdering(
				EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY
						+ "." + EOScolMaquetteSemestre.MSEM_DATE_DEBUT_KEY,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri2);

		EOSortOrdering tri3 = new EOSortOrdering(
				EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY
						+ "."
						+ EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY
						+ "."
						+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY
						+ "." + EOScolFormationDiplome.FDIP_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		sortOrderings.add(tri3);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteRepartitionSem.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<String>tabParam = new NSMutableArray<String>();
		tabParam.add("toFwkScolarite_ScolMaquetteSemestre");
		fetchSpec.setPrefetchingRelationshipKeyPaths(tabParam);
		NSArray resNonTrie = ec.objectsWithFetchSpecification(fetchSpec);
		NSArray resTrie = new NSArray();
		resTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(resNonTrie,
				sortOrderings);

		return resTrie;
	}

	/**
	 * Retourne ts les enregistrements de EOScolMaquetteRepartitionUe pour
	 * l'annee en cours
	 * 
	 * @param ec
	 * @return
	 * @see EOScolMaquetteRepartitionUe
	 */
	public static NSArray<EOScolMaquetteRepartitionUe> getRepUEFromYear(
			EOEditingContext ec) {
		if (ec == null)
			return null;
		NSArray<EOScolMaquetteRepartitionUe> resRepUE = new NSArray<EOScolMaquetteRepartitionUe>();

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);
		String conditionStr = EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@";
		tbKeys.add(annee);

		// CktlLog.log(" - "+conditionStr);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteRepartitionUe.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
NSArray<String>tabParam  = new NSMutableArray<String>();
tabParam.add("toFwkScolarite_ScolMaquetteSemestre");
		fetchSpec.setPrefetchingRelationshipKeyPaths(tabParam);
		NSArray<EOScolMaquetteRepartitionUe> resNonTrie = ec
				.objectsWithFetchSpecification(fetchSpec);

		NSArray<EOScolMaquetteRepartitionUe> resTrie = new NSArray<EOScolMaquetteRepartitionUe>();

		NSMutableArray sortOrderings = new NSMutableArray();
		EOSortOrdering tri1 = new EOSortOrdering(
				EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY
						+ "." + EOScolMaquetteSemestre.MSEM_POINTS_KEY,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri1);

		EOSortOrdering tri2 = new EOSortOrdering(
				EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY
						+ "." + EOScolMaquetteSemestre.MSEM_DATE_DEBUT_KEY,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri2);

		resTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(resNonTrie,
				sortOrderings);

		return resTrie;
	}

	/**
	 * Retourne ts les enregistrements de EOScolMaquetteRepartitionUe pour
	 * l'annee en cours et le parcours passe en parametre
	 * 
	 * @param ec
	 * @return
	 * @see EOScolMaquetteRepartitionUe
	 */
	public static NSArray<EOScolMaquetteRepartitionUe> getRepUEFromYearAndParcours(
			EOEditingContext ec, EOScolFormationAnnee annee,
			EOScolMaquetteParcours parcours) {
		if (ec == null)
			return null;
		NSArray<EOScolMaquetteRepartitionUe> resRepUE = new NSArray<EOScolMaquetteRepartitionUe>();

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ " = %@ AND "
				+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY
				+ "."
				+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY
				+ "."
				+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY
				+ " = %@";
		tbKeys.add(annee);
		tbKeys.add(parcours);

		// CktlLog.log(" - "+conditionStr);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteRepartitionUe.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<String>tabParam  = new NSMutableArray<String>();
		tabParam.add("toFwkScolarite_ScolMaquetteSemestre");
		fetchSpec.setPrefetchingRelationshipKeyPaths(tabParam);
		NSArray<EOScolMaquetteRepartitionUe> resNonTrie = ec
				.objectsWithFetchSpecification(fetchSpec);

		NSArray<EOScolMaquetteRepartitionUe> resTrie = new NSArray<EOScolMaquetteRepartitionUe>();

		NSMutableArray sortOrderings = new NSMutableArray();
		EOSortOrdering tri1 = new EOSortOrdering(
				EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY
						+ "." + EOScolMaquetteSemestre.MSEM_POINTS_KEY,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri1);

		EOSortOrdering tri2 = new EOSortOrdering(
				EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY
						+ "." + EOScolMaquetteSemestre.MSEM_DATE_DEBUT_KEY,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri2);

		resTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(resNonTrie,
				sortOrderings);

		return resTrie;
	}

	/**
	 * Retourne les UE a partir du semestre
	 * 
	 * @param ec
	 * @param semestre
	 * @return
	 */
	public static NSArray<EOScolMaquetteUe> getUEFromSemestre(
			EOEditingContext ec, EOScolMaquetteSemestre semestre) {
		if (ec == null)
			return null;
		if (semestre == null)
			return null;
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);
		if (annee == null)
			return null;

		return FinderScolMaquetteSemestre.getUeForSemestreAndYear(ec, semestre,
				annee);
	}

	/**
	 * Retourne les Semestres à partir du parcours selectionne
	 */
	public static NSArray<EOScolMaquetteSemestre> getSemestreFromParcours(
			EOEditingContext ec, EOScolMaquetteParcours parcours) {
		if (ec == null)
			return null;
		if (parcours == null)
			return null;
		EOScolFormationAnnee annee = FinderScolFormationAnnee
				.getScolFormationAnneeCourante(ec);
		return FinderScolMaquetteParcours.getSemestresForParcoursAndYear(ec,
				parcours, annee);

	}

	/**
	 * Reecriture d'un finder de diplome en fonction d'une chaine saisie.
	 * 
	 * @param ec
	 * @param selectedDomaine
	 * @param year
	 * @param strToSearch
	 * @return
	 */
	private static NSArray<EOScolFormationDiplome> findDiplomes(
			EOEditingContext ec, EOScolFormationDomaine selectedDomaine,
			EOScolFormationAnnee year, String strToSearch) {
		if (ec == null)
			return null;
		if (year == null)
			return null;
		if (selectedDomaine == null)
			return null;
		if (strToSearch == null)
			return null;
		EOQualifier qual = EOQualifier
				.qualifierWithQualifierFormat(
						EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY
								+ "."
								+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY
								+ "."
								+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
								+ " = %@ "
								+ " AND "
								+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY
								+ " = %@", new NSArray<Object>(new Object[] {
								year, selectedDomaine }));

		EOQualifier qualStrInput = EOQualifier
				.qualifierWithQualifierFormat(
						EOScolFormationDiplome.FDIP_ABREVIATION_KEY
								+ " caseInsensitiveLike '*" + strToSearch
								+ "*' or "
								+ EOScolFormationDiplome.FDIP_CODE_KEY
								+ " caseInsensitiveLike '*" + strToSearch
								+ "*' or "
								+ EOScolFormationDiplome.FDIP_LIBELLE_KEY
								+ " caseInsensitiveLike '*" + strToSearch
								+ "*' ", null);
		qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				qual, qualStrInput }));

		NSArray<EOSortOrdering> sort = new NSArray<EOSortOrdering>(
				new EOSortOrdering[] { new EOSortOrdering(
						EOScolFormationDiplome.FDIP_LIBELLE_KEY,
						EOSortOrdering.CompareCaseInsensitiveAscending) });

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolFormationDiplome.ENTITY_NAME, qual, sort);

		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * Retourne le diplome de formation continue
	 * 
	 * @param ec
	 * @return
	 */
	public static EOScolFormationDiplome findDiplomeFC(EOEditingContext ec) {
		String codeSiseDefaut = "9090000";

		EOScolFormationDiplome res;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolFormationDiplome.FDIP_CODE_KEY + " = %@";
		tbKeys.add(codeSiseDefaut);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		res = EOScolFormationDiplome.fetchFirstByQualifier(ec, qual);

		return res;
	}

	/**
	 * Retourne le parcours de formation continue
	 * 
	 * @param ec
	 * @return
	 */
	public static EOScolMaquetteParcours findParcoursFC(EOEditingContext ec) {
		EOScolMaquetteParcours res;
		String code = "FC";
		String libelle = "FormationContinue";

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteParcours.MPAR_CODE_KEY
				+ " = %@ AND " + EOScolMaquetteParcours.MPAR_ABREVIATION_KEY
				+ " = %@ AND " + EOScolMaquetteParcours.MPAR_LIBELLE_KEY
				+ " = %@ ";
		tbKeys.add(code);
		tbKeys.add(libelle);
		tbKeys.add(libelle);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		res = EOScolMaquetteParcours.fetchFirstByQualifier(ec, qual);
		return res;

	}

	/**
	 * Création d'un DiplomeSISE par défaut pour la formation continue
	 * 
	 * @param ec
	 * @return
	 */
	public static EOScolFormationDiplomeSise checkDiplomeSiseFCAndCreate(
			EOEditingContext ec) {
		if (ec == null)
			return null;
		EOScolFormationDiplomeSise res = null;
		try {
			String codeSiseDefaut = "9090000";
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOScolFormationDiplomeSise.FDIP_CODE_SISE_KEY
					+ " = %@";
			tbKeys.add(codeSiseDefaut);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolFormationDiplomeSise.fetchFirstByQualifier(ec, qual);

			if (res == null) {

				// Creer un diplomeSise
				res = EOScolFormationDiplomeSise.create(ec);
				EOGlobalID diplomeSiseGlobalID = EOKeyGlobalID
						.globalIDWithEntityName(
								EOScolFormationGrade.ENTITY_NAME,
								new Object[] { codeSiseDefaut });
				ec.insertObjectWithGlobalID(res, diplomeSiseGlobalID);
				// EOGlobalID diplomeSiseGlobalID = PrimaryKeyGetter
				// .getGlobalIdFromModelAndSequence(ec, "Scolarite",
				// "SCOL_GROUPE_OBJET_SEQ", EOScolFormationDiplome.ENTITY_NAME);
				res.setFdipCodeSise(codeSiseDefaut);
				res.setFdipCodeSise(codeSiseDefaut);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return res;
	}

	/**
	 * Creation d'un grade par defaut pour la formation continue
	 * 
	 * @param ec
	 * @return
	 */
	public static EOScolFormationGrade checkGradeFCAndCreate(EOEditingContext ec) {
		if (ec == null)
			return null;
		String gradeCode = "FC";
		String gradeLibelle = "Formation Continue";
		EOScolFormationGrade res = null;
		try {
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOScolFormationGrade.FGRA_CODE_KEY + " = %@ ";
			tbKeys.add(gradeCode);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolFormationGrade.fetchFirstByQualifier(ec, qual);
			if (res == null) {

				EOGlobalID cle = EOKeyGlobalID.globalIDWithEntityName(
						EOScolFormationGrade.ENTITY_NAME,
						new Object[] { gradeCode });
				res = FactoryScolFormationGrade.createWithDefaultValues(ec);
				ec.insertObjectWithGlobalID(res, cle);
				res.setFgraCode(gradeCode);
				res.setFgraAbreviation(gradeCode);
				res.setFgraLibelle(gradeLibelle);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return res;
	}

	/**
	 * Creation d'un domaine par defaut pour un diplome de formation continue
	 * 
	 * @param ec
	 * @return
	 */
	public static EOScolFormationDomaine checkDomaineFCDiplomeAndCreate(
			EOEditingContext ec) throws FormcoException {
		return FinderParcoursPedagogique.checkDomaineFCAndCreate(ec, "FCD",
				EOScolFormationDomaine.DOMAINE_TYPE_DIPLOME);
	}

	/**
	 * Creation d'un domaine par defaut pour une UE de formation continue
	 * 
	 * @param ec
	 * @return
	 * @throws FormcoException
	 */
	public static EOScolFormationDomaine checkDomaineFCUeAndCreate(
			EOEditingContext ec) throws FormcoException {
		return FinderParcoursPedagogique.checkDomaineFCAndCreate(ec, "FCUE",
				EOScolFormationDomaine.DOMAINE_TYPE_UE);
	}

	/**
	 * Verifie l'existence du domaine et le cree sinon
	 * 
	 * @param ec
	 * @param type
	 * @return
	 * @throws FormcoException
	 */
	private static EOScolFormationDomaine checkDomaineFCAndCreate(
			EOEditingContext ec, String code, String type)
			throws FormcoException {
		if ((ec == null) || (type == null) || (code == null))
			return null;
		String domaineLibelle = "Formation Continue";
		EOScolFormationDomaine res = null;
		try {
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOScolFormationDomaine.FDOM_CODE_KEY
					+ " = %@ AND " + EOScolFormationDomaine.FDOM_TYPE_KEY
					+ " = %@ ";
			tbKeys.add(code);
			tbKeys.add(type);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolFormationDomaine.fetchFirstByQualifier(ec, qual);
			if (res == null) {
				res = FactoryScolFormationDomaine.createWithDefaultValues(ec);

				EOGlobalID cle = EOKeyGlobalID.globalIDWithEntityName(
						EOScolFormationDomaine.ENTITY_NAME,
						new Object[] { code });
				ec.insertObjectWithGlobalID(res, cle);
				res.setFdomCode(code);
				res.setFdomAbreviation(domaineLibelle);
				res.setFdomLibelle(domaineLibelle);
				res.setFdomType(type);
			}
		} catch (Exception e) {
			throw new FormcoException(e.getMessage());
		}

		return res;
	}

	/**
	 * Trouver la vocation professionnelle
	 * 
	 * @param ec
	 * @return
	 */
	public static EOScolFormationVocation findVocationProfessionnelle(
			EOEditingContext ec) {
		if (ec == null)
			return null;
		EOScolFormationVocation res;
		String vocationCode = "VP";
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolFormationVocation.FVOC_CODE_KEY + " = %@ ";
		tbKeys.add(vocationCode);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		res = EOScolFormationVocation.fetchFirstByQualifier(ec, qual);
		return res;

	}

	/**
	 * Retourne un EOVcomposanteScolarite a partir d'un code RNE
	 * 
	 * @param ec
	 * @param codeRne
	 * @return
	 */
	public static EOVComposanteScolarite findVComposanteScolariteFromRne(
			EOEditingContext ec, EORne codeRne) {

		if (ec == null)
			return null;
		if (codeRne == null)
			return null;
		EOVComposanteScolarite res = null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOVComposanteScolarite.C_RNE_KEY + " = %@ ";
		tbKeys.add(codeRne.cRne());
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		res = EOVComposanteScolarite.fetchFirstByQualifier(ec, qual);
		return res;
	}

	/**
	 * Retourne un EOVEtablissementScolarite a partir du code Rne passe en
	 * parametre
	 * 
	 * @param ec
	 * @param codeRne
	 * @return
	 */
	public static EOVEtablissementScolarite findVEtablissementScolariteFromRne(
			EOEditingContext ec, EORne codeRne) {
		if (ec == null)
			return null;
		if (codeRne == null)
			return null;
		EOVEtablissementScolarite res = null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOVEtablissementScolarite.C_RNE_KEY + " = %@ ";
		tbKeys.add(codeRne.cRne());
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		res = EOVEtablissementScolarite.fetchFirstByQualifier(ec, qual);
		return res;
	}

	/**
	 * Verifie l'existence d'un droit/login pour la formation continue et le
	 * cree sinon.
	 * 
	 * @param ec
	 * @return
	 */
	public static EOScolDroitLogin checkDroitLoginAndCreate(EOEditingContext ec) {
		if (ec == null)
			return null;
		String loginFC = "a_la_main";
		String nomFC = "A LA MAIN";
		int cleLoginALaMain = 0;
		EOScolDroitLogin res = null;
		try {
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOScolDroitLogin.DLOG_LOGIN_KEY
					+ " = %@ AND " + EOScolDroitLogin.DLOG_NOM_KEY
					+ " = %@ AND " + EOScolDroitLogin.DLOG_VALIDE_KEY
					+ " = %@ ";
			tbKeys.add(loginFC);
			tbKeys.add(nomFC);
			tbKeys.add(EOScolDroitLogin.UTILISATEUR_INVALIDE);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolDroitLogin.fetchFirstByQualifier(ec, qual);
			if (res == null) {
				res = EOScolDroitLogin.create(ec);
				EOGlobalID loginGLobalId = EOKeyGlobalID
						.globalIDWithEntityName(
								EOScolFormationSpecialisation.ENTITY_NAME,
								new Object[] { cleLoginALaMain });
				ec.insertObjectWithGlobalID(res, loginGLobalId);
				res.setDlogLogin(loginFC);
				res.setDlogNom(nomFC);
				res.setDlogPrenom(null);
				res.setDlogValide(EOScolDroitLogin.UTILISATEUR_INVALIDE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return res;
	}

	/**
	 * Verifie l'exstence d'un droitDiplome et le cree sinon
	 * 
	 * @param ec
	 * @param login
	 * @param habilitation
	 * @param annee
	 * @return
	 */
	public static EOScolDroitDiplome checkDroiDiplomeFCAndCreate(
			EOEditingContext ec, EOScolDroitLogin login,
			EOScolFormationHabilitation habilitation, EOScolFormationAnnee annee) {
		if (ec == null)
			return null;
		if (login == null)
			return null;
		if (habilitation == null)
			return null;
		if (annee == null)
			return null;
		EOScolDroitDiplome res = null;
		String actif = "A";
		String passif = "P";
		try {
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOScolDroitDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
					+ " = %@ AND "
					+ EOScolDroitDiplome.TO_FWK_SCOLARITE__SCOL_DROIT_LOGIN_KEY
					+ " = %@ AND "
					+ EOScolDroitDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATION_KEY
					+ " = %@ ";
			tbKeys.add(annee);
			tbKeys.add(login);
			tbKeys.add(habilitation);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolDroitDiplome.fetchFirstByQualifier(ec, qual);
			if (res == null) {
				res = EOScolDroitDiplome.create(ec);
				res.setToFwkScolarite_ScolDroitLoginRelationship(login);
				res.setToFwkScolarite_ScolFormationAnneeRelationship(annee);
				res.setToFwkScolarite_ScolFormationHabilitationRelationship(habilitation);
				res.setDdipBilan(passif);
				res.setDdipCharges(passif);
				res.setDdipComplements(passif);
				res.setDdipEdt(passif);
				res.setDdipExamens(passif);
				res.setDdipGroupes(passif);
				res.setDdipIpedagogiques(actif);
				res.setDdipMaquettes(actif);
				res.setDdipStatistiques(actif);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return res;
	}

	/**
	 * vérifie l'existence d'une habilitation pour la formation continue et la
	 * cree sinon.
	 * 
	 * @param ec
	 * @param annee
	 * @param specialisation
	 * @return
	 */
	public static EOScolFormationHabilitation checkHabilitationFCAndCreate(
			EOEditingContext ec, EOScolFormationAnnee annee,
			EOScolFormationSpecialisation specialisation) {
		if (ec == null)
			return null;
		if (annee == null)
			return null;
		if (specialisation == null)
			return null;
		EOScolFormationHabilitation res = null;
		try {
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			int niveauHabilitation = 1;
			String conditionStr = EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
					+ " = %@ AND "
					+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY
					+ " = %@ ";
			tbKeys.add(annee);
			tbKeys.add(specialisation);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolFormationHabilitation.fetchFirstByQualifier(ec, qual);
			if (res == null) {
				res = EOScolFormationHabilitation.create(ec);
				res.setToFwkScolarite_ScolFormationAnneeRelationship(annee);
				res.setToFwkScolarite_ScolFormationSpecialisationRelationship(specialisation);
				res.setFspnKey(specialisation.fspnKey());
				res.setFhabOuvert(EOScolFormationHabilitation.HABILITATION_OUVERTE);
				res.setFhabNiveau(niveauHabilitation);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return res;
	}

	/**
	 * Creation d'un diplome par defaut pour la formation continue. CodeSise,
	 * grade,domaine ne doivent pas etre nuls pour la creation On passe les
	 * grade etc.. en param pour eviter les requetes puisque normalement ces
	 * methodes sont appelees uniquement au demarrage de l'appli de gestion des
	 * FC.
	 * 
	 * @param ec
	 * @return
	 */
/*	public static EOScolFormationDiplome checkDiplomeFCAndCreate(
			EOEditingContext ec, EOScolFormationGrade gradeFC,
			EOScolFormationDomaine domaineFC,
			EOScolFormationVocation vocationPro,
			EOScolFormationDiplomeSise diplomeSise) throws IOException {
		if (ec == null)
			return null;
		if (gradeFC == null)
			return null;
		if (domaineFC == null)
			return null;
		if (vocationPro == null)
			return null;

		String diplomeCode = "FC";
		String diplomeLibelle = "Formation Continue";
		EOScolFormationDiplome res = null;
		try {
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOScolFormationDiplome.FDIP_ABREVIATION_KEY
					+ " = %@ AND "
					+ EOScolFormationDiplome.FDIP_MENTION_KEY
					+ " = %@ AND "
					+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY
					+ " = %@ AND "
					+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY
					+ " = %@ AND "
					+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_VOCATION_KEY
					+ " = %@ ";
			tbKeys.add(diplomeCode);
			tbKeys.add(diplomeLibelle);
			tbKeys.add(gradeFC);
			tbKeys.add(domaineFC);
			tbKeys.add(vocationPro);

			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolFormationDiplome.fetchFirstByQualifier(ec, qual);
			if (res == null) {

				res = EOScolFormationDiplome.create(ec);

				res.setFdipAbreviation(diplomeCode);
				res.setFdipArrivee(new Integer(1));
				String cleDiplome = ERXEOControlUtilities
						.primaryKeyStringForObject(diplomeSise);

				res.setFdipCode(cleDiplome);
				res.setFdipCycle(new Integer(3));
				res.setFdipDeliberation(EOScolFormationDiplome.DELIBERATION_SEMESTRIELLE);
				res.setFdipDepart(new Integer(1));
				res.setFdipLibelle(diplomeLibelle);
				res.setFdipMention(diplomeLibelle);
				res.setFdipMonoPosition(new Integer(1));
				res.setFdipMonoSemestre(EOScolFormationDiplome.REPARTITION_MULTI_SEMESTRE);
				res.setFdipSemestrialisation(EOScolFormationDiplome.ORGANISATION_SEMESTRIELLE);
				res.setFdipType(EOScolFormationDiplome.FORMATION_FICTIVE);
				res.setFdipTypeDroit("M");
				res.setToFwkScolarite_ScolFormationDomaineRelationship(domaineFC);
				res.setToFwkScolarite_ScolFormationGradeRelationship(gradeFC);
				res.setToFwkScolarite_ScolFormationVocationRelationship(vocationPro);
				res.setCodeDiplomeObligatoire(true);
				res.setToFwkScolarite_ScolFormationDiplomeSiseRelationship(diplomeSise);

				EORne rneDefaut = FinderRne.getRneLocal(ec);
				if (rneDefaut == null) {
					throw new IOException(
							"Erreur de creation d'un diplome de formation continue, CodeRneLocal (GRHUM.PARAMETRES.GRHUM_DEFAULT_RNE) null.");
				}
				EOVComposanteScolarite composanteScolariteDefaut = FinderParcoursPedagogique
						.findVComposanteScolariteFromRne(ec, rneDefaut);

				if (composanteScolariteDefaut == null) {
					throw new IOException(
							"Erreur de creation d'un diplome de formation continue, Impossible de trouver la EOVComposanteScolarite (GRHUM.V_COMPOSANTE_SCOLARITE) avec le CodeRne par défaut  null.");
				}
				res.setToFwkScolarix_VComposanteScolariteRelationship(composanteScolariteDefaut);
				EOVEtablissementScolarite etablissementScolariteDefaut = FinderParcoursPedagogique
						.findVEtablissementScolariteFromRne(ec, rneDefaut);
				if (etablissementScolariteDefaut == null) {
					throw new IOException(
							"Erreur de creation d'un diplome de formation continue, Impossible de trouver le EOVEtablissementScolarite (GRHUM.V_ETABLISSEMENT_SCOLARITE) avec le CodeRne par défaut  null.");
				}
				res.setToFwkScolarix_VEtablissementScolariteRelationship(etablissementScolariteDefaut);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return res;
	}
*/
	/**
	 * Verifie l'existence des semestres et les cree sinon
	 * 
	 * @param ec
	 * @param parcoursFC
	 * @return
	 */
	public static NSArray<EOScolMaquetteSemestre> checkSemestreAndCreate(
			EOEditingContext ec, EOScolFormationAnnee annee,
			EOScolMaquetteParcours parcoursFC) {
		if (ec == null)
			return null;
		if ((annee == null) || (parcoursFC == null))
			return null;
		NSMutableArray<EOScolMaquetteSemestre> semestres = new NSMutableArray<EOScolMaquetteSemestre>();
		try {
			semestres = FinderEOScolMaquetteSemestre
					.getSemestreFromParcoursAndYear(ec, annee, parcoursFC)
					.mutableClone();
			if (semestres.isEmpty()) {
				// creer les semestres
				EOScolMaquetteSemestre semestre1 = EOScolMaquetteSemestre
						.create(ec, new Integer(0),
								new java.math.BigDecimal(0), new BigDecimal(0),
								new BigDecimal(0), new Integer(1),
								new BigDecimal(0), new Integer(0), annee);
				EOScolMaquetteSemestre semestre2 = EOScolMaquetteSemestre
						.create(ec, new Integer(0),
								new java.math.BigDecimal(0), new BigDecimal(0),
								new BigDecimal(0), new Integer(2),
								new BigDecimal(0), new Integer(0), annee);

				semestres.add(semestre1);
				semestres.add(semestre2);

				EOScolMaquetteRepartitionSem repSem1 = EOScolMaquetteRepartitionSem
						.create(ec, 0, annee, parcoursFC, semestre1);
				EOScolMaquetteRepartitionSem repSem2 = EOScolMaquetteRepartitionSem
						.create(ec, 0, annee, parcoursFC, semestre2);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return semestres;
	}

	/**
	 * Verifie l'existance de la specialisation Formation continue et la cree
	 * sinon. Le diplome Formation Continue est passe en parametre Pour la
	 * creation, le diplome ne doit pas etre nul
	 * 
	 * @param ec
	 * @param diplome
	 * @return
	 */
	public static EOScolFormationSpecialisation checkSpecialisationFCAndCreate(
			EOEditingContext ec, EOScolFormationDiplome diplome) {
		if (ec == null)
			return null;
		if (diplome == null)
			return null;
		EOScolFormationSpecialisation res = null;
		try {
			String specialisationCode = "FC";
			String specialisationLibelle = "FormationContinue";
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOScolFormationSpecialisation.FSPN_ABREVIATION_KEY
					+ " = %@ AND "
					+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY
					+ " = %@";
			tbKeys.add(specialisationCode);
			tbKeys.add(diplome);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolFormationSpecialisation.fetchFirstByQualifier(ec, qual);
			if (res == null) {

				res = EOScolFormationSpecialisation.create(ec);
				Number cleSpecialisation = PrimaryKeyGetter
						.getEOScolFormationSpecialisationKey(ec);
				EOGlobalID speGLobalId = EOKeyGlobalID.globalIDWithEntityName(
						EOScolFormationSpecialisation.ENTITY_NAME,
						new Object[] { cleSpecialisation });
				ec.insertObjectWithGlobalID(res, speGLobalId);
				res.setFspnKey(cleSpecialisation.intValue());
				res.setFspnAbreviation(specialisationCode);
				res.setFspnLibelle(specialisationLibelle);
				res.setToFwkScolarite_ScolFormationDiplomeRelationship(diplome);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return res;
	}

	/**
	 * Verifie l'existance du parcours FC et le cree sinon. Pour la creation, la
	 * specialisation et la vocation ne peuvent pas etre nuls
	 * 
	 * @param ec
	 * @param specialisation
	 * @param vocation
	 * @return
	 */
	public static EOScolMaquetteParcours checkParcoursFCAndCreate(
			EOEditingContext ec, EOScolFormationSpecialisation specialisation,
			EOScolFormationVocation vocation) {
		if (ec == null)
			return null;
		if (specialisation == null)
			return null;
		if (vocation == null)
			return null;
		EOScolMaquetteParcours res = null;
		try {
			String parcoursCode = "FC";
			String parcoursLibelle = "FormationContinue";
			String validite = "O";
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = EOScolMaquetteParcours.MPAR_CODE_KEY
					+ " = %@ AND "
					+ EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY
					+ " = %@ AND "
					+ EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_FORMATION_VOCATION_KEY
					+ " = %@ ";
			;
			tbKeys.add(parcoursCode);
			tbKeys.add(specialisation);
			tbKeys.add(vocation);
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			res = EOScolMaquetteParcours.fetchFirstByQualifier(ec, qual);
			if (res == null) {

				res = EOScolMaquetteParcours.create(ec);
				res.setMparCode(parcoursCode);
				res.setMparAbreviation(parcoursLibelle);
				res.setMparLibelle(parcoursLibelle);
				res.setMparValidite(validite);
				res.setToFwkScolarite_ScolFormationSpecialisationRelationship(specialisation);
				res.setToFwkScolarite_ScolFormationVocationRelationship(vocation);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return res;
	}

}
