/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.util.Enumeration;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.EOContrat;
import org.cocktail.fwkcktlformco.serveur.utils.FCUtils;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCataloguePrestation;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOPrestation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;

public abstract class FinderContrat {

	// public static NSArray<Contrat> getContratFromCatalogue(EOEditingContext
	// ec, EOCatalogue catalogue)
	// {
	// if(ec==null || catalogue==null)return null;
	// NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
	// String conditionStr = EOContrat.PRESTATIONS_KEY+ "."
	// +EOPrestation.CATALOGUE_KEY+" = %@";
	// tbKeys.add(catalogue);
	//
	// EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
	// conditionStr, tbKeys);
	// EOFetchSpecification fetchSpec = new EOFetchSpecification(
	// Contrat.ENTITY_NAME, qual, null);
	//
	// return ec.objectsWithFetchSpecification(fetchSpec);
	// }

	public static NSArray<Contrat> getContratFromCatalogue(EOEditingContext ec,
			EOCatalogue catalogue) {
		if (ec == null || catalogue == null)
			return null;
		// recuperer les prestations du catalogue
		int catId = Integer.parseInt(ERXEOControlUtilities
				.primaryKeyStringForObject(catalogue));
		EOQualifier qualPrest = EOQualifier.qualifierWithQualifierFormat(
				EOPrestation.CAT_ID_KEY + " =%@ ", new NSArray(catId));

		EOFetchSpecification fetchSPecPrest = new EOFetchSpecification(
				EOPrestation.ENTITY_NAME, qualPrest, null);

		NSArray<EOPrestation> prestArray = ec
				.objectsWithFetchSpecification(fetchSPecPrest);
		NSMutableArray<Contrat> tabContrat = new NSMutableArray<Contrat>();

		Enumeration<EOPrestation> enumPrest = prestArray.objectEnumerator();
		while(enumPrest.hasMoreElements()){
			EOPrestation prest = (EOPrestation) enumPrest.nextElement();
//			int prestId = Integer.parseInt(ERXEOControlUtilities.primaryKeyStringForObject(prest));
//			EOPrestation prestOK = EOPrestation.fetchFirstByQualifier(ec, new EOKeyValueQualifier(EOPrestation.PREST_ID_KEY, EOQualifier.QualifierOperatorEqual, prestId));
//			
			if (prest.convention() != null) {
				Contrat aContrat = FCUtils.getContratAccordFromContratPie(ec,
						prest.convention());
				if (!tabContrat.contains(aContrat)) {
					tabContrat.add(aContrat);
				}
			}
		}
		return tabContrat;
	}
}
