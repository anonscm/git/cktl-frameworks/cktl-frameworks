/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEtudiant {
	
	public static NSArray<EOEtudiant> findEtudiantValide(EOEditingContext ec,String strToSearch) throws FormcoException
	{
		if (ec == null || strToSearch == null || strToSearch.length()<=0) {
			throw new FormcoException("Erreur: Impossible de rechercher l'etudiant.");
		}
		
		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.add(new EOKeyValueQualifier(EOEtudiant.TO_INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, strToSearch));
		quals.add(new EOKeyValueQualifier(EOEtudiant.TO_INDIVIDU_KEY+"."+EOIndividu.PRENOM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, strToSearch));
		EOQualifier qualFouValide = new EOKeyValueQualifier(EOEtudiant.TO_INDIVIDU_KEY+"."+EOIndividu.TEM_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EOIndividu.TEM_VALIDE_O);
		NSArray<EOQualifier> qualF = new NSMutableArray<EOQualifier>();
		qualF.add(new EOOrQualifier(quals));
		qualF.add(qualFouValide);
		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(
				EOEtudiant.TO_INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
	return EOEtudiant.fetchAll(ec, new EOAndQualifier(qualF),sort);
	}
	
	public static NSArray<EOIndividu> findIndEtudiantValide(EOEditingContext ec,String strToSearch) throws FormcoException
	{
		NSArray<EOEtudiant> resEtudiant = FinderEtudiant.findEtudiantValide(ec, strToSearch);
		NSArray<EOIndividu> indsEtudValides=new NSMutableArray<EOIndividu>();
		for (Enumeration<EOEtudiant> indEtudValide = resEtudiant
				.objectEnumerator(); indEtudValide.hasMoreElements();) {
			EOEtudiant etudLu = (EOEtudiant)indEtudValide.nextElement();
			indsEtudValides.add(etudLu.toIndividu());
		}
		return indsEtudValides;
	}

}
