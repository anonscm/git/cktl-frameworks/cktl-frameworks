/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.EORepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.EOTranche;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderRepartPartenaireTranche {

	public static NSArray<RepartPartenaireTranche> getRPTFromContrat(
			EOEditingContext ec, Contrat contrat) {
		if (ec == null || contrat == null)
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = RepartPartenaireTranche.TRANCHE_KEY + "."
				+ Tranche.CONTRAT_KEY + " = %@ ";
		tbKeys.add(contrat);
		
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				RepartPartenaireTranche.ENTITY_NAME, qual, null);

		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		NSArray<EORepartPartenaireTranche> res1= ec.objectsWithFetchSpecification(fetchSpec);
		NSMutableArray sortOrderings = new NSMutableArray();
		EOSortOrdering tri1 = new EOSortOrdering(
				 RepartPartenaireTranche.TRANCHE_KEY ,
				EOSortOrdering.CompareDescending);
		sortOrderings.add(tri1);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res1,
				sortOrderings);
	}
	
	public static NSArray<RepartPartenaireTranche> getRPTFromContratAndExe(
			EOEditingContext ec, Contrat contrat, EOExerciceCocktail exeCktl) {
		if (ec == null || contrat == null || exeCktl==null)
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = RepartPartenaireTranche.TRANCHE_KEY + "."
				+ Tranche.CONTRAT_KEY + " = %@ "+
				" AND "+RepartPartenaireTranche.TRANCHE_KEY + "."
				+ Tranche.EXERCICE_COCKTAIL_KEY + " =%@ ORDER BY "+Tranche.EXERCICE_COCKTAIL_KEY;
		tbKeys.add(contrat);
		tbKeys.add(exeCktl);
		
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				RepartPartenaireTranche.ENTITY_NAME, qual, null);

		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}
	
	public static NSArray<RepartPartenaireTranche> getRPTFromContratAndPartenaire(EOEditingContext ec, Contrat contrat,ContratPartenaire partenaire)
	{
		if (ec == null || contrat == null || partenaire==null)
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = RepartPartenaireTranche.TRANCHE_KEY + "."
				+ Tranche.CONTRAT_KEY + " = %@ AND " + RepartPartenaireTranche.CONTRAT_PARTENAIRE_KEY + " = %@";
		tbKeys.add(contrat);
		tbKeys.add(partenaire);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				RepartPartenaireTranche.ENTITY_NAME, qual, null);

		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}
}
