/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Class abstraite de recherche des repart EC de Formation continue
 * 
 * @author jb
 * 
 */
public abstract class FinderEOScolMaquetteRepartEC {

	/**
	 * Retourne la premiere RepartEC liée à l'ue passée en parametre
	 * S'il y a plusieurs UE pour la même EC, retourne null;
	 * @param ec
	 * @param ue
	 * @return
	 */
	public static EOScolMaquetteRepartitionEc getRepartECFromUEFC(
			EOEditingContext ec, EOScolMaquetteUe ue) {
		if ((ec == null) || (ue == null))
			return null;
		NSArray<EOScolMaquetteRepartitionEc> res = null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY
				+ " = %@";
		tbKeys.add(ue);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOScolMaquetteRepartitionEc.ENTITY_NAME, qual, null);
		res = ec.objectsWithFetchSpecification(fetchSpec);
		if (res.count() > 1)
			return null;
		return res.get(0);
	}

}
