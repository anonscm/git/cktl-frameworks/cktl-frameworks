/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.math.BigDecimal;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.EOContrat;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlformco.serveur.utils.FCUtils;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOTypePublic;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOPrestation {

	/**
	 * Retourne les prestations du client dont le persId est passe en parametre.
	 * 
	 * @param ec
	 * @param persId
	 * @return
	 * @throws FormcoException
	 */
	public static NSArray<EOPrestation> getPrestationsFromClient(
			EOEditingContext ec, int persId) throws FormcoException {
		if ((ec == null) || (persId == 0)) {
			throw new FormcoException(
					"Recuperation des prestations du client impossible. ec ou persId null.");
		}
		EOQualifier qualPrestation = EOQualifier.qualifierWithQualifierFormat(
				EOPrestation.PERS_ID_KEY + "=%@", new NSArray(
						new Object[] { persId }));
		NSArray<EOPrestation> listPrestation = EOPrestation.fetchAll(ec,
				qualPrestation);
		return listPrestation;
	}

	public static EOPrestation getPrestationFromClientAndContrat(
			EOEditingContext ec, int persId, Contrat contrat)
			throws FormcoException {
		if ((ec == null) || (persId == 0) || contrat == null) {
			throw new FormcoException(
					"Recuperation des prestations du client impossible. ec ou persId ou contrat null.");
		}
		NSArray<EOPrestation> res = new NSArray<EOPrestation>();
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
//		String conditionStr = EOPrestation.PERS_ID_KEY + " = %@  AND"
//				+ EOPrestation.TO_CW_CONTRAT_KEY + " = %@";
		String conditionStr = EOPrestation.PERS_ID_KEY + " = %@  AND"
		+ EOPrestation.CONVENTION_KEY+ " = %@";
		tbKeys.add(persId);
		tbKeys.add(FCUtils.getContratPieFromContratAccord(ec, contrat));

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOPrestation.ENTITY_NAME, qual, null);

		fetchSpec.setIsDeep(true);
		res = ec.objectsWithFetchSpecification(fetchSpec);
		if (res == null)
			return null;
		if (res.count() > 1)
			throw new FormcoException("Erreur: il existe " + res.count()
					+ " prestations pour le contrat (conOrdre:"
					+ contrat.conOrdre() + ") et le client (persId:" + persId
					+ ")");
		return res.get(0);
	}

	public static EOPrestation getPrestationFromContrat(
			EOEditingContext ec, Contrat contrat)
			throws FormcoException {
		if ((ec == null)  || contrat == null) {
			throw new FormcoException(
					"Recuperation des prestations du client impossible. ec ou contrat null.");
		}
		NSArray<EOPrestation> res = new NSArray<EOPrestation>();
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOPrestation.CONVENTION_KEY + " = %@";
		tbKeys.add(FCUtils.getContratPieFromContratAccord(ec, contrat));

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOPrestation.ENTITY_NAME, qual, null);

		fetchSpec.setIsDeep(true);
		res = ec.objectsWithFetchSpecification(fetchSpec);
		if (res == null)
			return null;
		if (res.count() > 1)
			throw new FormcoException("Erreur: il existe " + res.count()
					+ " prestations pour le contrat (conOrdre:"
					+ contrat.conOrdre() + ")");
		return res.get(0);
	}
	
	
	public static NSArray<EOPrestation> getPrestationsFromCatalogue(
			EOEditingContext ec, EOCatalogue catalogue) throws FormcoException {
		if ((ec == null)) {
			throw new FormcoException(
					"Recuperation des prestations du catalogue impossible. ec ou persId null.");
		}
		EOQualifier qualPrestation = null;
		if (catalogue != null) {
			qualPrestation = EOQualifier.qualifierWithQualifierFormat(
					EOPrestation.CATALOGUE_KEY + "=%@", new NSArray(
							new Object[] { catalogue }));
		}
		NSArray<EOPrestation> listPrestation = EOPrestation.fetchAll(ec,
				qualPrestation);
		if (listPrestation.isEmpty()) {
			listPrestation = EOPrestation.fetchAll(ec);
		}
		return listPrestation;
	}

	/**
	 * Retourne le montant global de la prestation associée au contrat
	 * 
	 * @return
	 */
	public static BigDecimal getMontantPrestation(EOEditingContext ec,
			Contrat contrat) {
		if (ec == null || contrat == null)
			return null;
		NSArray<EOFacturePapier> faps = FinderEOFacturePapier
				.getFacturesFromContrat(ec, contrat);
		if (faps.count() > 0) {
			return faps.get(0).prestation().prestTotalHt();
		}
		return null;

	}
}
