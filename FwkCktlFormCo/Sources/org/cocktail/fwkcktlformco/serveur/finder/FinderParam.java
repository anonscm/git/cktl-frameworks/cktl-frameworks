/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.scolarix.serveur.metier.eos.EOGarnucheParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public abstract class FinderParam {

	/**
	 * Recherche la liste des parametres correspondants a une cle dans GrhumParametres <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un NSArray de String
	 */
	public static final NSArray getGrhumParams(EOEditingContext ec, String key) {
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOGrhumParametres.PARAM_KEY_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, key);
		NSArray params = EOGrhumParametres.fetchAll(ec, qualifier, null);
		if (params == null || params.count() == 0) {
			return null;
		}
		return (NSArray) params.valueForKey(EOGrhumParametres.PARAM_VALUE_KEY);
	}
	
	/**
	 * Recherche la liste des parametres correspondants a une cle dans GarnucheParametres <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un NSArray de String
	 */
	public static final NSArray<EOGarnucheParametres> getGarnucheParams(EOEditingContext ec, String key) {
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOGarnucheParametres.PARAM_KEY_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, key);
		NSArray<EOGarnucheParametres> params = EOGarnucheParametres.fetchAll(ec, qualifier);
		if (params == null || params.count() == 0) {
			return null;
		}
		return params;
	}

}
