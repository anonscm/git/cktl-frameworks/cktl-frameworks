/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.fwkcktljefyadmin.common.metier.EOPersonne;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOCataloguePublic;
import org.cocktail.kava.server.metier.EOCatalogueResponsable;
import org.cocktail.kava.server.metier.EOTypePublic;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOCatalogue {

	public static NSArray findCatalogueFromEtatTypeResponsable(EOEditingContext ec,
			EOTypeEtat typeEtat, EOTypePublic typePublic, int persId) {
		if (typeEtat == null) {
			return new NSArray();
		}
		if (typePublic == null) {
			return new NSArray();
		}
		if (persId == 0)
			return new NSArray();
		
		EOUtilisateur utilisateur = EOUtilisateur.fetchByKeyValue(ec,
				EOUtilisateur.PERS_ID_KEY, persId);
		NSMutableArray quals = new NSMutableArray();
		NSArray<Object> typeEtatParam= new NSMutableArray<Object>();
		typeEtatParam.add(typeEtat);
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCatalogue.CATALOGUE_ARTICLES_KEY+"."+EOCatalogueArticle.TYPE_ETAT_KEY + " = %@", typeEtatParam));
		NSArray<Object> typePublicParam= new NSMutableArray<Object>();
		typePublicParam.add(typePublic);
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCatalogue.CATALOGUE_PUBLICS_KEY+"."+EOCataloguePublic.TYPE_PUBLIC_KEY+ " = %@", typePublicParam));
		NSArray<Object> utilisateurParam= new NSMutableArray<Object>();
		utilisateurParam.add(utilisateur);
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCatalogue.CATALOGUE_RESPONSABLES_KEY+"."+EOCatalogueResponsable.UTILISATEUR_KEY+ " = %@",utilisateurParam));

		
		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(
				EOCatalogue.CAT_LIBELLE_KEY,
				EOSortOrdering.CompareAscending));

		EOFetchSpecification fs = new EOFetchSpecification(
				EOCatalogue.ENTITY_NAME, new EOAndQualifier(quals), sort);
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
}
