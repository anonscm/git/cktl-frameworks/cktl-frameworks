/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.kava.server.metier.EOPlanComptable;
import org.cocktail.kava.server.metier.EOPlancoCreditRec;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;



public class FinderPlancoCreditRec {

	
	public static NSArray findAll(EOEditingContext ec) {
		EOFetchSpecification fs = new EOFetchSpecification(EOPlancoCreditRec.ENTITY_NAME, null, null);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	private static EOQualifier qualValide() {
		NSArray<Object>tabParam  = new NSMutableArray<Object>();
		tabParam.add( "VALIDE");
		tabParam.add("VALIDE");
		return EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PLANCO_CREDIT_RECS_KEY + "." + EOPlancoCreditRec.PCC_ETAT_KEY
				+ "=%@ AND " + EOPlanComptable.PLANCO_CREDIT_RECS_KEY + "." + EOPlancoCreditRec.PCO_VALIDITE_KEY + "=%@", tabParam);
	}

	private static EOQualifier qual(EOTypeCredit typeCreditRec) {
		NSArray<Object>tabParam  = new NSMutableArray<Object>();
		tabParam.add( typeCreditRec);
		return EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PLANCO_CREDIT_RECS_KEY + "." + EOPlancoCreditRec.TYPE_CREDIT_REC_KEY
				+ "=%@", tabParam);
	}

	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditRec) {
		return find(ec, typeCreditRec, null);
	}
	
	
	/************ Ajouts pour tenir compte de la table PLAN_COMPTABLE_EXER a la place de PLAN_COMPTABLE ************/
	
	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditRec, EOExercice exercice) {
		return find(ec, typeCreditRec, exercice,null);
	}
	
	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditRec, EOExercice exercice,EOQualifier additionalQualifier) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(qualValide());
		
		if(additionalQualifier!=null) {
			quals.addObject( additionalQualifier );
		}
		
		if (typeCreditRec != null) {
			quals.addObject(qual(typeCreditRec));
		}
		
		if(exercice!=null) {
			NSArray<Object>tabParam  = new NSMutableArray<Object>();
			tabParam.add(exercice);
			quals.addObject( EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.EXERCICE_KEY+"=%@",tabParam) );
		}

		
		EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	
	/**
	 * Retourne les EOPlancoCreditRec a partir de l'exercice et la chaine passée en param.
	 * recherche dans EOPlanComptable.PCO_NUM_KEY et EOPlanComptable.PCO_LIBELLE_KEY
	 * @param ec
	 * @param exercice
	 * @param plancoCreditRecInput
	 * @return
	 */
	public static NSArray<EOPlancoCreditRec> findPlancoCreditRecFromNumAndLibelle(EOEditingContext ec, EOExercice exercice, String plancoCreditRecInput)
	{
		if(ec==null)return null;
		if(exercice==null) return null;
			
		EOQualifier qual = null;
		if (plancoCreditRecInput != null) {
			qual = EOQualifier.qualifierWithQualifierFormat(
					EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike '*"
							+ plancoCreditRecInput + "*' or "
							+ EOPlanComptable.PCO_LIBELLE_KEY
							+ " caseInsensitiveLike '*" + plancoCreditRecInput
							+ "*'", null); 

		}
		NSArray<EOPlancoCreditRec> data = FinderPlancoCreditRec.find(ec, null,
				exercice, qual);
		return data;
		
	}
}
