/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOPays;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderEOPays {

	/**
	 * Renvoie les pays dont le libelle long ou court contient la chaine passé
	 * en parametre
	 * 
	 * @param ec
	 * @param ch
	 * @return
	 */
	public static NSArray findPaysFromString(EOEditingContext ec,
			String strToSearch) {
		if (ec == null)
			return null;
		if (strToSearch == null)
			return null;
//		if (strToSearch.isEmpty())
		if(strToSearch.length()<=0)
			return null;

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		NSArray<EOPays> resTrie = new NSArray<EOPays>();
		String srQualifier = EOPays.LC_PAYS_KEY + " caseInsensitiveLike '*"
				+ strToSearch + "*' OR " + EOPays.LL_PAYS_KEY
				+ " caseInsensitiveLike '*" + strToSearch + "*' OR "
				+ EOPays.LL_PAYS_EN_KEY + " caseInsensitiveLike '*"
				+ strToSearch + "*'";
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				srQualifier, null);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOPays.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		NSArray<EOPays> resNonTrie = ec
				.objectsWithFetchSpecification(fetchSpec);
		EOSortOrdering tri1 = new EOSortOrdering(EOPays.LL_PAYS_EN_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		NSMutableArray sortOrderings = new NSMutableArray();
		sortOrderings.add(tri1);
		resTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(resNonTrie,
				sortOrderings);
		return resTrie;
	}
}
