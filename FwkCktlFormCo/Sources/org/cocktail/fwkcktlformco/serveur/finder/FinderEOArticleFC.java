/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlformco.serveur.metier.EOArticleFC;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe Abstraite de select relativement aux article de FC
 * 
 * @author jb
 * 
 */
public abstract class FinderEOArticleFC {

	/**
	 * Retourne l'article FC a partie du catalogueArticle
	 * 
	 * @param ec
	 * @param catAr
	 * @return
	 */
	public static EOArticleFC getArticleFCFromCatAr(EOEditingContext ec,
			EOCatalogueArticle catAr) {
		if (ec == null)
			return null;
		if (catAr == null)
			return null;

		String conditionStr = EOArticleFC.TO_CATALOGUE_ARTICLE_KEY + " = %@ ";

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		tbKeys.add(catAr);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOArticleFC.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<EOArticleFC> res = ec.objectsWithFetchSpecification(fetchSpec);
		if (res.count() > 1)
			return null;

		return res.objectAtIndex(0);
	}

	/**
	 * Retourne les articles de FC en fonction d'un catalogue
	 * 
	 * @param ec
	 * @param catalogue
	 * @return
	 */
	public static NSArray<EOArticleFC> getArticlesFromCatalogue(
			EOEditingContext ec, EOCatalogue catalogue) {
		if (ec == null)
			return null;
		if (catalogue == null)
			return null;

		NSArray<EOArticleFC> res = new NSArray<EOArticleFC>();
		String conditionStr = EOArticleFC.TO_CATALOGUE_ARTICLE_KEY + "."
				+ EOCatalogueArticle.CATALOGUE_KEY + " = %@";
		NSMutableArray<Object> tbkeys = new NSMutableArray<Object>();
		tbkeys.add(catalogue);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbkeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOArticleFC.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);

		res = ec.objectsWithFetchSpecification(fetchSpec);

		return res;
	}

	/**
	 * Retourne les articles d'un catalogue valide
	 * @param catalogue
	 * @return
	 */
	public static NSArray<EOArticleFC> getArticlesFromCatalogueArticleValide(
			EOCatalogue catalogue) {
		if (catalogue == null)
			return null;
		EOEditingContext ec = catalogue.editingContext();
		EOTypeEtat typeEtat = FinderTypeEtat.typeEtatValide(ec);
		NSArray<EOArticleFC> res = new NSArray<EOArticleFC>();

		String conditionStr = EOArticleFC.TO_CATALOGUE_ARTICLE_KEY + "."
				+ EOCatalogueArticle.CATALOGUE_KEY + " = %@ AND "
				+ EOArticleFC.TO_CATALOGUE_ARTICLE_KEY + "."
				+ EOCatalogueArticle.TYPE_ETAT_KEY + "= %@";

		NSMutableArray<Object> tbkeys = new NSMutableArray<Object>();
		tbkeys.add(catalogue);
		tbkeys.add(typeEtat);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbkeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOArticleFC.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		res = ec.objectsWithFetchSpecification(fetchSpec);
		return res;
	}
	
	/**
	 * Retourne les articles a partir d'un parcours et d'une annee. Utilisé pour retourner les UE de formations continues.
	 * @param ec
	 * @param annee
	 * @param parcours
	 * @return
	 */
	public static NSArray<EOArticleFC> getArticlesFromYearAndParcours(EOEditingContext ec, EOScolFormationAnnee annee, EOScolMaquetteParcours parcours)
	{
		if(ec==null) return null;
		if(annee==null)return null;
		if(parcours==null)return null;
		
		String conditionStr = EOArticleFC.TO_SCOL_MAQUETTE_REPARTITION_UE_KEY+"."
		+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY 
		+ " = %@ AND "
		+ EOArticleFC.TO_SCOL_MAQUETTE_REPARTITION_UE_KEY+"."
		+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY
		+ "."
		+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY
		+ "."
		+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY
		+ " = %@";
		
		NSMutableArray<Object> tbkeys = new NSMutableArray<Object>();
		tbkeys.add(annee);
		tbkeys.add(parcours);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbkeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOArticleFC.ENTITY_NAME, qual, null);
//		fetchSpec.setUsesDistinct(true);
//		fetchSpec.setIsDeep(true);
		NSArray<EOArticleFC> res = ec.objectsWithFetchSpecification(fetchSpec);
		return res;
	}
	
	
	public static NSArray<EOArticleFC> getArticlesFromContrat(EOEditingContext ec, Contrat contrat)
	{
		if(ec==null || contrat==null)return null;
		NSArray<EOArticleFC> res = new NSMutableArray<EOArticleFC>();
		NSArray<EOPrestation> prests = contrat.prestations();
		for (int i=0;i<prests.count();i++)
		{
			NSArray<EOPrestationLigne> prestLigs = prests.get(i).prestationLignes();
			for(int j=0;j<prestLigs.count();j++)
			{
				if(prestLigs.get(j).catalogueArticle()!=null)
				{
					EOArticleFC article = FinderEOArticleFC.getArticleFCFromCatAr(ec, prestLigs.get(j).catalogueArticle());
					res.add(article);
				}
			}
		}
		
		return res;
	}
}
