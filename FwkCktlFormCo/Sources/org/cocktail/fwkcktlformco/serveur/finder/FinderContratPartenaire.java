/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.util.Enumeration;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderContratPartenaire {

	/**
	 * Trouver ts les autres partenaires qui ne sont pas partenaires du contrat
	 * 
	 * @param ec
	 * @param contratFC
	 * @return
	 */
	public static NSArray<EOPersonne> getOtherContratPartenaire(
			EOEditingContext ec, Contrat contratFC) {
		if (ec == null || contratFC == null)
			return null;
		NSArray<ContratPartenaire> cps = contratFC.contratPartenaires();
		Enumeration<ContratPartenaire> enumCps = cps.objectEnumerator();
		NSMutableArray quals = new NSMutableArray();

		while (enumCps.hasMoreElements()) {
			ContratPartenaire cp = enumCps.nextElement();
			NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
			String conditionStr = ContratPartenaire.PERS_ID_KEY + " != %@  ";
			tbKeys.add(cp.persId());
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys);
			quals.addObject(qual);
		}

		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(
				EOPersonne.PERS_LC_KEY, EOSortOrdering.CompareAscending));

		EOFetchSpecification fs = new EOFetchSpecification(
				EOPersonne.ENTITY_NAME, new EOAndQualifier(quals), sort);
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	public static ContratPartenaire getContratPartenaireFromContratAndContact(
			EOEditingContext ec, ContratPartContact cpc) throws FormcoException {

		if (ec == null || cpc == null) {
			throw new FormcoException("ec ou ContratPartContact null");
		}
		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		ContratPartenaire part = cpc.contratPartenaire();
		quals.add(ContratPartenaire.CONTRAT.eq(part.contrat()));
		quals.add(ContratPartenaire.CONTRAT_PART_CONTACTS.containsObject(cpc));
		ContratPartenaire partenaire = ContratPartenaire.fetch(ec,
				new EOAndQualifier(quals));
		return partenaire;
	}

	public static ContratPartenaire getContratPartenaireFromContratAndPersIdContact(
			EOEditingContext ec, Contrat contrat, int persId)
			throws FormcoException {
		NSArray<ContratPartContact> cpcs = ContratPartContact.fetchAll(ec,
				ContratPartContact.PERS_ID_CONTACT.eq(persId), null);
		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		for (int i = 0; i < cpcs.count(); i++) {
			quals.add(ContratPartenaire.CONTRAT_PART_CONTACTS
					.containsObject(cpcs.get(i)));
		}
		quals.add(ContratPartenaire.CONTRAT.eq(contrat));
		NSArray<ContratPartenaire> partenaires = ContratPartenaire.fetchAll(ec,
				new EOAndQualifier(quals), null);
		if (partenaires.count() > 1) {
			throw new FormcoException(
					"Erreur: Un contact a plusieurs partenaires pour un meme contrat!");
		}
		if (partenaires == null || partenaires.isEmpty()) {
			throw new FormcoException(
					"Erreur: Un contact n'a aucun partenaire!");
		}
		return partenaires.get(0);
	}
}
