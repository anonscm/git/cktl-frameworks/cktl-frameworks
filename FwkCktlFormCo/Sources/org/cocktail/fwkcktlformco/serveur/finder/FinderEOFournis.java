/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlformco.serveur.finder;

import java.util.Collection;
import java.util.Enumeration;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlformco.serveur.exception.FormcoException;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class FinderEOFournis {

	public static EOFournis findFournisseurById(EOEditingContext ec, int persId) {
		if (ec == null)
			return null;
		if (persId <= 0)
			return null;

		String conditionStr = EOFournis.PERS_ID_KEY + " = %@ ";

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		tbKeys.add(persId);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOFournis.ENTITY_NAME, qual, null);
		NSArray<EOFournis> res = ec.objectsWithFetchSpecification(fetchSpec);
		if ((res == null) || (res.isEmpty()))
			return null;
		return res.get(0);
	}

	public static NSArray find(EOEditingContext ec, String nom,
			String prenom, NSTimestamp dateNaissance) throws FormcoException {
		if ((ec == null) || (nom == null) || (prenom == null)
				|| (dateNaissance == null)) {
			throw new FormcoException("Erreur: Impossible de rechercher le fournisseur.");
		}
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOFournis.TO_INDIVIDUS_KEY + "."
				+ EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@% AND "
				+ EOFournis.TO_INDIVIDUS_KEY + "." + EOIndividu.PRENOM_KEY
				+ " caseInsensitiveLike %@ AND " + EOFournis.TO_INDIVIDUS_KEY
				+ "." + EOIndividu.D_NAISSANCE_KEY + "= %@";
		tbKeys.add(nom);
		tbKeys.add(prenom);
		tbKeys.add(dateNaissance);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOFournis.ENTITY_NAME, qual, null);
		
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	public static NSArray<EOFournis> findFouValide(EOEditingContext ec, String strToSearch ) throws FormcoException {
		if (ec == null || strToSearch == null || strToSearch.length()<=0) {
			throw new FormcoException("Erreur: Impossible de rechercher le fournisseur.");
		}
		
		NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.add(new EOKeyValueQualifier(EOFournis.TO_INDIVIDUS_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, strToSearch));
		quals.add(new EOKeyValueQualifier(EOFournis.TO_INDIVIDUS_KEY+"."+EOIndividu.PRENOM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, strToSearch));
		EOQualifier qualFouValide = new EOKeyValueQualifier(EOFournis.FOU_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EOFournis.FOU_VALIDE_OUI);
		NSArray<EOQualifier> qualF = new NSMutableArray<EOQualifier>();
		qualF.add(new EOOrQualifier(quals));
		qualF.add(qualFouValide);
		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(
				EOFournis.TO_INDIVIDUS_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
	return EOFournis.fetchAll(ec, new EOAndQualifier(qualF),sort);
	}
	
	public static NSArray<EOIndividu> findIndFouValide(EOEditingContext ec, String strToSearch ) throws FormcoException
	{
		NSArray<EOFournis> fouValides = FinderEOFournis.findFouValide(ec, strToSearch);
		NSArray<EOIndividu> indsFouValides=new NSMutableArray<EOIndividu>();
		for (Enumeration<EOFournis> indFouValide = fouValides
				.objectEnumerator(); indFouValide.hasMoreElements();) {
			EOFournis fouLu = (EOFournis)indFouValide.nextElement();
			indsFouValides.add(fouLu.toIndividu());
		}
		return indsFouValides;
	}
	
	
}
