CREATE OR REPLACE PROCEDURE FORMCO.CREER_DROITS_JEFY_ADMIN
-- initialiser la gestion des habilitations dans jefy_admin 
is
-- declaration des variables
domid     jefy_admin.DOMAINE.DOM_ID%type;
tyapid    jefy_admin.TYPE_APPLICATION.TYAP_ID%type;
fonOrdre  jefy_admin.FONCTION.fon_ordre%type;
appli     jefy_admin.TYPE_APPLICATION.TYAP_LIBELLE%type;


BEGIN

-- initialisation des constantes

  appli:= 'FORMATION_CONTINUE';
  tyapid := 32;
  

  select max(dom_id)+1
  into domid
  from JEFY_ADMIN.DOMAINE;



-- ajout des données
  INSERT INTO JEFY_ADMIN.DOMAINE ( DOM_ID, DOM_LIBELLE ) VALUES (domid, appli);

  jefy_admin.API_APPLICATION.creerTypeApplication(
				tyapid,
				domid,
				appli,
				appli
  );

  -- Droits généraux
  jefy_admin.API_APPLICATION.creerFonction(
			 32000,
			 'ADMIN',
			 'ADMINISTRATION',
			 'Droits généraux FC',
			 'Droits généraux FC',
			 'N',
			 'N',
			 tyapid
  );

  -- Droits GESTIONNAIRE FINANCIER
  jefy_admin.API_APPLICATION.creerFonction(
			 32100,
			 'GESTFIN',
			 'GEST_FINANCIER',
			 'Gestion financiere FC',
			 'Gestion financiere FC',
			 'N',
			 'N',
			 tyapid
  );

  -- Droits GESTIONNAIRE FORMATION CONTINUE
  jefy_admin.API_APPLICATION.creerFonction(
			 32200,
			 'GESTFC',
			 'GEST_FC',
			 'Gestion formation continue',
			 'Gestion formation continue',
			 'N',
			 'N',
			 tyapid
  );

  -- Droits STAGIAIRE
  jefy_admin.API_APPLICATION.creerFonction(
			 32300,
			 'STAGFC',
			 'STAGIAIRE_FC',
			 'Acces stagiaire FC',
			 'Acces stagiaire FC',
			 'N',
			 'N',
			 tyapid
  );

  -- Droits FORMATEUR
  jefy_admin.API_APPLICATION.creerFonction(
			 32400,
			 'FORMFC',
			 'FORMATEUR_FC',
			 'Acces formateur FC',
			 'Acces formateur FC',
			 'N',
			 'N',
			 tyapid
  );

 END;
/
commit;
-- Executer la procedure
EXECUTE FORMCO.CREER_DROITS_JEFY_ADMIN;


-- PROCEDURE D AJOUT D UN TYPE DE PUBLIC FC POUR L APPLI FC
CREATE OR REPLACE PROCEDURE FORMCO.CREER_TYPE_PUBLIC_FC
is
-- declaration des variables
typuid    jefy_recette.TYPE_PUBLIC.TYPU_ID%type;
tyapid    jefy_recette.TYPE_PUBLIC.TYAP_ID%type;
libelle   jefy_recette.TYPE_PUBLIC.TYPU_LIBELLE%type;
nbTypPu integer;

BEGIN

-- initialisation des constantes

  libelle:= 'FORMATION CONTINUE';
  tyapid := 32;
  select count(*) into nbTypPu
  from JEFY_RECETTE.TYPE_PUBLIC
  where UPPER(JEFY_RECETTE.TYPE_PUBLIC.TYPU_LIBELLE) like UPPER(libelle);

	if (nbTypPu<=0)then
	
	select max(TYPU_ID)+1
  into typuid
  from JEFY_RECETTE.TYPE_PUBLIC;
  
-- ajout des données
  INSERT INTO JEFY_RECETTE.TYPE_PUBLIC ( TYPU_ID,TYPU_LIBELLE,TYPU_ACCES_INTERNE,TYAP_ID ) 
  VALUES (typuid, libelle,'O',tyapid);
  end if;

 END;
/
COMMIT;
-- Executer la procedure
EXECUTE FORMCO.CREER_TYPE_PUBLIC_FC;

-- PROCEDURE D AJOUT D UN TYPE DE CONTRAT FC POUR L APPLI FC
CREATE OR REPLACE PROCEDURE FORMCO.CREER_TYPE_CONTRAT_FC
is
-- declaration des variables
tyconID		ACCORDS.TYPE_CONTRAT.TYCON_ID%type;
tyconLibelle	ACCORDS.TYPE_CONTRAT.TYCON_LIBELLE%type;
tyconIDInterne    ACCORDS.TYPE_CONTRAT.TYCON_ID_INTERNE%type;
tyconNature   ACCORDS.TYPE_CONTRAT.TYCON_NATURE%type;
nbTypcon integer;

BEGIN

-- initialisation des constantes

  tyconLibelle := 'CONTRAT DE FORMATION CONTINUE';
  tyconIDInterne := 'CONT_FC';
  tyconNature := 'TYPE';

  select count(*) into nbTypcon
  from ACCORDS.TYPE_CONTRAT
  where UPPER(ACCORDS.TYPE_CONTRAT.TYCON_ID_INTERNE) like UPPER(tyconIDInterne);
  
  if (nbTypcon<=0) then
  
    select max(TYCON_ID)+1
  into tyconID
  from ACCORDS.TYPE_CONTRAT;
-- ajout des données
  INSERT INTO ACCORDS.TYPE_CONTRAT ( TYCON_ID,TYCON_ID_PERE,TYCON_LIBELLE,TYCON_NATURE,TYCON_RECHERCHE,TYCON_NIVEAU,TYCON_ID_INTERNE) 
values (tyconID, 0, tyconLibelle, tyconNature, 'N', 0, tyconIDInterne);
end if;

 END;
/
commit;
-- Executer la procedure
EXECUTE FORMCO.CREER_TYPE_CONTRAT_FC;


-- PROCEDURE D AJOUT D UN TYPE DE CLASSIFICATION CONTRAT FC POUR L APPLI FC
CREATE OR REPLACE PROCEDURE FORMCO.CREER_TYPE_CLASS_CONTRAT_FC
is
-- declaration des variables
tccCode		ACCORDS.TYPE_CLASSIFICATION_CONTRAT.TCC_CODE%type;
tccLibelle	ACCORDS.TYPE_CLASSIFICATION_CONTRAT.TCC_LIBELLE%type;
tccId   	ACCORDS.TYPE_CLASSIFICATION_CONTRAT.TCC_ID%type;
nbTcc integer;

BEGIN

-- initialisation des constantes

  tccLibelle := 'CONTRAT';
  tccCode := 'CONT';

  select count(*) into nbTcc
  from ACCORDS.TYPE_CLASSIFICATION_CONTRAT
  where UPPER(ACCORDS.TYPE_CLASSIFICATION_CONTRAT.TCC_CODE) like UPPER(tccCode);
  
  if (nbTcc<=0) then
  
    select max(TCC_ID)+1
  into tccId
  from ACCORDS.TYPE_CLASSIFICATION_CONTRAT;

-- ajout des données
  INSERT INTO ACCORDS.TYPE_CLASSIFICATION_CONTRAT ( TCC_ID,TCC_CODE,TCC_LIBELLE) 
values (tccId, tccCode, tccLibelle);
end if;

 END;
/
commit;
-- Executer la procedure
EXECUTE FORMCO.CREER_TYPE_CLASS_CONTRAT_FC;


--Creation d'un diplome de formation continue pour y stocker toutes formations proposees a la carte en formation continue
create or REPLACE PROCEDURE FORMCO.CREER_DIPLOME_FC
IS
  -- Déclaration variables
  r_fdip SCOLARITE.SCOL_FORMATION_DIPLOME%ROWTYPE;
  pasBon BOOLEAN;
  nbenr  INTEGER;
  nbdipplomeFC INTEGER;
BEGIN
  -- Initialisation variables
  pasBon := FALSE;
  r_fdip.fdip_code := '0000001';
  r_fdip.fgra_code := 'DIV';
  r_fdip.fdom_code := 'FC';
  r_fdip.fvoc_code := 'VI';
  r_fdip.fdip_mention := 'FORMATION CONTINUE';
  r_fdip.fdip_libelle := 'mention FORMATION CONTINUE';
  r_fdip.fdip_abreviation := 'FORMATION CONTINUE';
  r_fdip.fdip_depart := 1;
  r_fdip.fdip_arrivee := 1;
  r_fdip.fdip_cycle := 0;
  r_fdip.fdip_type_droit := 'G';
  r_fdip.fdip_type := 'C';
  r_fdip.fdip_semestrialisation := 'O';
  r_fdip.fdip_deliberation := 'O';
  r_fdip.fdip_mono_semestre := 'O';
  r_fdip.fdip_mono_position := '1';
 
  -- Contrôles obligatoires
  SELECT COUNT(FDIP_CODE)
  INTO nbdipplomeFC
  FROM SCOLARITE.SCOL_FORMATION_DIPLOME 
  WHERE SCOLARITE.SCOL_FORMATION_DIPLOME.fdip_code='0000001';
  
  IF (nbdipplomeFC > 0) 
  THEN RAISE_APPLICATION_ERROR (-20001,'Creation diplome impossible: Le diplome a creer existe deja');
  END IF; 
  
  
  
  SELECT COUNT(C_RNE)
  INTO nbenr
  FROM GRHUM.V_COMPOSANTE_SCOLARITE 
  WHERE ROWNUM < 2;

  IF (nbenr <= 0) 
  THEN RAISE_APPLICATION_ERROR (-20001,'Creation diplome impossible: La vue GRHUM.V_COMPOSANTE_SCOLARITE ne renvoie aucun resultat');
  END IF; 
  
  SELECT COUNT(C_RNE)
  INTO nbenr
  FROM GRHUM.V_ETABLISSEMENT_SCOLARITE
  WHERE ROWNUM < 2;

  IF (nbenr <= 0) 
  THEN RAISE_APPLICATION_ERROR (-20001,'Creation diplome impossible: La vue GRHUM.V_ETABLISSEMENT_SCOLARITE ne renvoie aucun resultat');
  END IF;
  
  -- Contrôle ajout du GRADE DIV (FILIERE pour diplôme de type Classique)
  SELECT COUNT(FFIL_CODE)
  INTO nbenr
  FROM SCOLARITE.SCOL_FORMATION_FILIERE
  WHERE UPPER(FFIL_CODE) LIKE UPPER(r_fdip.fgra_code);
  
  IF (nbenr <= 0) 
  THEN INSERT INTO SCOLARITE.SCOL_FORMATION_FILIERE
       (FFIL_CODE,FFIL_LIBELLE,FFIL_ABREVIATION,FFIL_VALIDITE)
       VALUES
       (r_fdip.fgra_code,'DIVERS','Divers','O');
  END IF;
  
  -- Contrôle ajout du DOMAINE FC (DEPARTEMENT pour diplôme de type Classique)
  SELECT COUNT(FDEP_CODE)
  INTO nbenr
  FROM SCOLARITE.SCOL_FORMATION_DEPARTEMENT
  WHERE UPPER(FDEP_CODE) LIKE UPPER(r_fdip.fdom_code);
  
  IF (nbenr <= 0) 
  THEN INSERT INTO SCOLARITE.SCOL_FORMATION_DEPARTEMENT
       (FDEP_CODE,FDEP_LIBELLE,FDEP_ABREVIATION,FDEP_VALIDITE)
       VALUES
       (r_fdip.fdom_code,'FORMATION CONTINUE','FORMATION CONTINUE','O');
  END IF;
  
  -- Contrôle ajout de la VOCATION VI
  SELECT COUNT(FVOC_CODE)
  INTO nbenr 
  FROM SCOLARITE.SCOL_FORMATION_VOCATION
  WHERE UPPER(FVOC_CODE) LIKE UPPER(r_fdip.fvoc_code);

  IF (nbenr <= 0) 
  THEN INSERT INTO SCOLARITE.SCOL_FORMATION_VOCATION
       (FVOC_CODE,FVOC_LIBELLE,FVOC_ABREVIATION,FVOC_CODAGE,FVOC_VALIDITE)
       VALUES
       (r_fdip.fvoc_code,'VOCATION INDIFFÉRENCIÉE','Indifférenciée','0','O');
  END IF; 
   
  -- Préparation insertion
  SELECT C_RNE 
  INTO r_fdip.comp_code
  FROM GRHUM.V_COMPOSANTE_SCOLARITE 
  WHERE ROWNUM < 2;

  SELECT C_RNE 
  INTO r_fdip.etab_code
  FROM GRHUM.V_ETABLISSEMENT_SCOLARITE
  WHERE ROWNUM < 2;
  
  WHILE (pasBon = FALSE)
  LOOP
    SELECT COUNT(*)
    INTO   nbenr
    FROM   SCOLARITE.SCOL_FORMATION_DIPLOME
    WHERE  fdip_code = r_fdip.fdip_code;
    
    IF (nbenr > 0)
    THEN r_fdip.fdip_code := LPAD(TO_CHAR((TO_NUMBER(r_fdip.fdip_code) + 1)),7,'0');
    ELSE pasBon := TRUE;
    END IF;
  END LOOP;
   
  -- Ajout des données
  INSERT INTO SCOLARITE.SCOL_FORMATION_DIPLOME_SISE 
  (FDIP_CODE,FDIP_CODE_SISE) 
  VALUES 
  (r_fdip.fdip_code,r_fdip.fdip_code);

  INSERT INTO SCOLARITE.SCOL_FORMATION_DIPLOME 
  (
   FDIP_CODE,
   FGRA_CODE,
   FDOM_CODE,
   FVOC_CODE,
   FDIP_MENTION,
   FDIP_SPECIALITE,
   FDIP_LIBELLE,
   FDIP_ABREVIATION,
   FDIP_DEPART,
   FDIP_ARRIVEE,
   ETAB_CODE,
   COMP_CODE,
   FDIP_CYCLE,
   FDIP_MODELE,
   FDIP_TYPE_DROIT,
   SREMO_CODE,
   FDIP_TYPE,
   FDIP_SEMESTRIALISATION,
   FDIP_DELIBERATION,
   FDIP_MONO_SEMESTRE,
   FDIP_MONO_POSITION
  ) 
  VALUES 
  (
   r_fdip.fdip_code,
   r_fdip.fgra_code,
   r_fdip.fdom_code,
   r_fdip.fvoc_code,
   r_fdip.fdip_mention,
   r_fdip.fdip_specialite,
   r_fdip.fdip_libelle,
   r_fdip.fdip_abreviation,
   r_fdip.fdip_depart,
   r_fdip.fdip_arrivee,
   r_fdip.etab_code,
   r_fdip.comp_code,
   r_fdip.fdip_cycle,
   r_fdip.fdip_modele,
   r_fdip.fdip_type_droit,
   r_fdip.sremo_code,
   r_fdip.fdip_type,
   r_fdip.fdip_semestrialisation,
   r_fdip.fdip_deliberation,
   r_fdip.fdip_mono_semestre,
   r_fdip.fdip_mono_position
  );
END;
/
-- Executer la procedure
EXECUTE FORMCO.CREER_DIPLOME_FC;


-- PROCEDURE D AJOUT D UN ROLE STAGIAIRE FC
CREATE OR REPLACE PROCEDURE FORMCO.CREER_ROLE_STAG_FC
IS
-- declaration des variables
code    GRHUM.ASSOCIATION.ASS_CODE%type;
libelle   GRHUM.ASSOCIATION.ASS_LIBELLE%type;
nbRoleFC integer;

BEGIN
-- initialisation des constantes
  libelle:= 'STAGIAIRE FORMATION CONTINUE';
  select count(*) into nbRoleFC
  from GRHUM.ASSOCIATION
  where UPPER(GRHUM.ASSOCIATION.ASS_LIBELLE) like UPPER(libelle);

	if (nbRoleFC<=0)then
	-- ajout des données
	INSERT INTO GRHUM.ASSOCIATION (ASS_ID,ASS_LIBELLE,D_CREATION,D_MODIFICATION,TAS_ID,ASS_RACINE,ASS_CODE) VALUES 		(GRHUM.ASSOCIATION_SEQ.NEXTVAL,'STAGIAIRE FORMATION CONTINUE',SYSDATE,SYSDATE,1,NULL,'STAG. FC');
  end if;

 END;
/
COMMIT;
-- Executer la procedure
EXECUTE FORMCO.CREER_ROLE_STAG_FC;


create or replace PROCEDURE        CREER_TYPE_PUBLIC_FC
is
-- declaration des variables
typuid    jefy_recette.TYPE_PUBLIC.TYPU_ID%type;
tyapid    jefy_recette.TYPE_PUBLIC.TYAP_ID%type;
libelle   jefy_recette.TYPE_PUBLIC.TYPU_LIBELLE%type;
nbTypPu integer;

BEGIN

-- initialisation des constantes

  libelle:= 'FORMATION CONTINUE';
  tyapid := 32;
  select count(*) into nbTypPu
  from JEFY_RECETTE.TYPE_PUBLIC
  where UPPER(JEFY_RECETTE.TYPE_PUBLIC.TYPU_LIBELLE) like UPPER(libelle);

	if (nbTypPu<=0)then
	
	select max(TYPU_ID)+1
  into typuid
  from JEFY_RECETTE.TYPE_PUBLIC;
  
-- ajout des données
  INSERT INTO JEFY_RECETTE.TYPE_PUBLIC ( TYPU_ID,TYPU_LIBELLE,TYPU_ACCES_INTERNE,TYAP_ID ) 
  VALUES (typuid, libelle,'O',tyapid);
  end if;

 END;
 /
COMMIT;
