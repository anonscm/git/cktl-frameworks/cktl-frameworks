/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import org.cocktail.fwkcktlinternat.serveur.metier.EOPersonneStatut;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneForm;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXS;

public class InternatPersonneForm extends PersonneForm {
    public InternatPersonneForm(WOContext context) {
        super(context);
        
    }
   
    public void initialiseData() {
    	
    	super.initialiseData();
    	
    	if (isCreation())  leStatut = EOPersonneStatut.createEOPersonneStatut(edc(), getLaPersonne().persId());
        else leStatut=EOPersonneStatut.fetchByKeyValue(edc(), EOPersonneStatut.PERSI_ID_KEY, getLaPersonne().persId());
    	
    }
    
    public boolean aUnStatut() {   	
         return leStatut != null;
    }
    
    public boolean naPasUnStatut() {
    	return leStatut == null;
    }
    
    private EOPersonneStatut leStatut;
    public void setLeStatut(EOPersonneStatut ps) { this.leStatut = ps;	}
	public EOPersonneStatut getLeStatut() { return leStatut;	}
		
    //-----------------------------
	//	Type Statut
	//-----------------------------
    private EOTypeStatut unTypeStatut;
    public void setUnTypeStatut(EOTypeStatut unTypeStatut) { this.unTypeStatut = unTypeStatut;	}
	public EOTypeStatut getUnTypeStatut() {		return unTypeStatut;	}
		
    public NSArray<EOTypeStatut> lesTypesStatut (){
    	NSArray<EOSortOrdering> sort = ERXS.asc(EOTypeStatut.LIB_TYPE_STATUT_KEY).array();	
    	return EOTypeStatut.fetchAll(edc(),sort);
    }
}