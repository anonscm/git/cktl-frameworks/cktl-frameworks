/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOComponent;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
/**
 * XHTML equivalents of WOCheckboxList
 * 
 * @see WOCheckBoxList
 * @author mendis
 * 
 * Note : Marchait en 5.3.3 saboté par Apple en 5.4.3
 * Implémenté partiellement par Wonder donc bibi le refait ...
 * 
 *
 */
public class WXICheckBoxList extends WOComponent {
    public WXICheckBoxList(WOContext context) {
        super(context);
    }
    /**
	 * Do I need to update serialVersionUID?
	 * See section 5.6 <cite>Type Changes Affecting Serialization</cite> on page 51 of the 
	 * <a href="http://java.sun.com/j2se/1.4/pdf/serial-spec.pdf">Java Object Serialization Spec</a>
	 */
	private static final long serialVersionUID = 1L;

	public int index;
	private String _id;
	
    @Override
    public boolean synchronizesVariablesWithBindings() {
    	return false;
    }
    
    @Override
    public boolean isStateless() {
    	return true;
    }
    
    @Override
    public void reset() {
    	super.reset();
    	_id = null;
    }
    
	/*
	 *  api of component
	 */
	public static interface Bindings {
		public static final String selections = "selections";
		public static final String item = "item";
		public static final String id = "id";
		public static final String name = "name";
		public static final String index = "index";
		public static final String prefix = "prefix";	// Ce que j'ajoute
		public static final String suffix = "suffix";	// Ce que j'ajoute
	}
    
    // accesors
    @SuppressWarnings("unchecked")
	private NSArray selections() {
    	return (NSArray) valueForBinding(Bindings.selections);
    }
    
    private Object item() {
    	return valueForBinding(Bindings.item);
    }
    
    public Object selection() {
    	if (selections() == null) return null;
    	return selections().contains(item()) ? item() : null;
    }
    
    public void setSelection(Object value) {
     	NSMutableArray selections = (selections() != null) ? selections().mutableClone() : new NSMutableArray();
    	if (value != null) {
    		selections.addObject(item());
    	} else {
    		selections.removeObject(item());
    	}
    	setValueForBinding(selections, Bindings.selections);
    }
    
    public String elementName() {
    	return hasBinding(Bindings.name) ? (String) valueForBinding(Bindings.name) : _id();		
    }
    
    public String id() {
    	return _id() + "_" + index;
    }
    
    private String _id() {
    	if (_id == null) _id = hasBinding(Bindings.id) ? (String)  valueForBinding(Bindings.id) : context().elementID();	// RM: FIXME: convert to javascriptElementID() in WO 5.4
    	return _id;
    }
    
    public void setIndex(int i) {
    	index = i;
    	setValueForBinding(index, Bindings.index);
    }     
}