/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import java.util.Calendar;
import org.cocktail.fwkcktlinternat.serveur.util.Toolbox;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/*
 * 
 * Composant de selection ou de visualitation des jours dans une période
 * 
 * Ce composant doit être mis dans une FORM
 * 
 * Binding debut date de début
 * Binding fin   date de fin
 * Binding selections : liste de date
 * Binding modeLecture 
 * 
 * TODO
 * Binding sel/couleur etc...
 * 
 */

public class CalendrierJourMois extends AComponent {
	
	private static String[] lesMois = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet",
        "Août", "Septembre", "Octobre", "Novembre", "Décembre"};
	

	private NSTimestamp dateNull = new NSTimestamp(0L);
	
	static final String BINDING_DEBUT = "debut";
	static final String BINDING_FIN = "fin";
	static final String BINDING_SELECTIONS = "selections";
	static final String BINDING_LECTURE_SEULE ="lectureSeule";
	
	private static int NOMBRE_MOIS_PAR_TABLE = 6; 
	
	private NSTimestamp finPlusUnJour =  null;
	private NSTimestamp date_debut = null;
	private NSTimestamp date_fin = null;
	
    private NSArray <NSTimestamp> joursCalculeDuMois = null;
	
    public final String compteurID = getComponentId() + "_compteur";
    
    public final String containerCompteurID = getComponentId() + "_containerCompteur";
    
    public CalendrierJourMois(WOContext context) {
        super(context);
    }
    
	public void setLectureSeule(Boolean lectureSeule) {
		setValueForBinding(lectureSeule,BINDING_LECTURE_SEULE);
	}
	
	public Boolean getLectureSeule() {
		return (Boolean) valueForBinding(BINDING_LECTURE_SEULE);
	}
	
	public void setDebut(NSTimestamp debut) {
		setValueForBinding(debut, BINDING_DEBUT);
	}
	
	public NSTimestamp getDebut() {
		return (NSTimestamp) valueForBinding(BINDING_DEBUT);
	}
	
	public void setFin(NSTimestamp fin) {
		setValueForBinding(fin, BINDING_FIN);
	}
	
	public NSTimestamp getFin() {
		return (NSTimestamp) valueForBinding(BINDING_FIN);
	}
	
	public void setSelections(NSMutableArray <NSTimestamp> selections) {
		setValueForBinding(selections, BINDING_SELECTIONS);
	}
	
	public NSMutableArray <NSTimestamp> getSelections() {
		return (NSMutableArray <NSTimestamp>) valueForBinding(BINDING_SELECTIONS);
	}

	private NSTimestamp unJour; 
	public NSTimestamp getUnJour () {
		return unJour;
	}
	public void setUnJour (NSTimestamp j){
		unJour = j;
	}
	
	private Integer indexJour; 
	public Integer getIndexJour () {
		return indexJour;
	}
	public void setIndexJour (Integer i){
		indexJour = i;
	}
	
	/**
	 * 
	 * NSTimestampFormatter
	 * Specifier 	Description
		%% 	a '%' character
		%a 	abbreviated weekday name
		%A 	full weekday name
		%b 	abbreviated month name
		%B 	full month name
		%c 	shorthand for "%X %x", the locale format for date and time
		%d 	day of the month as a decimal number (01-31)
		%e 	same as %d but does not print the leading 0 for days 1 through 9
		%F 	milliseconds as a decimal number (000-999)
		%H 	hour based on a 24-hour clock as a decimal number (00-23)
		%I 	hour based on a 12-hour clock as a decimal number (01-12)
		%j 	day of the year as a decimal number (001-366)
		%m 	month as a decimal number (01-12)
		%M 	minute as a decimal number (00-59)
		%p 	AM/PM designation for the locale
		%S 	second as a decimal number (00-59)
		%w 	weekday as a decimal number (0-6), where Sunday is 0
		%x 	date using the date representation for the locale
		%X 	time using the time representation for the locale
		%y 	year without century (00-99)
		%Y 	year with century (such as 1990)
		%Z 	time zone name (such as "Europe/Paris" or "PST")
		%z 	time zone offset from GMT in hours and minutes (such as "+0200" or "-1200")
	 * 
	 * 
	 * @return
	 */
	
	public String jourChiffre () {
		if (unJour.equals(dateNull)) return "";
		/*
		 * Test %a ou %A
		 */
		StringBuffer format = new StringBuffer("%d %A");
		return new NSTimestampFormatter(format.toString()).format(unJour);
	}
	
	public String anneeDate (NSTimestamp date) {	
		StringBuffer format = new StringBuffer("%Y");
		return new NSTimestampFormatter(format.toString()).format(date);
	}
	
    public String moisDate (NSTimestamp date) {
		StringBuffer format = new StringBuffer("%m");
		return new NSTimestampFormatter(format.toString()).format(date);
	}
    public String jourDate(NSTimestamp date) {
		StringBuffer format = new StringBuffer("%d");
		return new NSTimestampFormatter(format.toString()).format(date);
	}
    public String jourNom() {
		StringBuffer format = new StringBuffer("%A");
		return new NSTimestampFormatter(format.toString()).format(unJour);
	}
	
    public class descr {
    	public descr(){	
    	}
    	public int mois, annee, nbjours;
    }
    
    private static int IS_DATE = 0;
    private static int IS_DATE_DEBUT_JOUR = 1;
    private static int IS_DATE_FIN_JOUR = 2;
    private static int IS_DATE_DEBUT_JOUR_MOIS_DEBUT_TABLE = 3;
    private static int IS_DATE_FIN_JOUR_FIN_TABLE = 4;
    int table_mois [];
    int table_descr [];
    NSMutableArray <descr> listeMois = new NSMutableArray <descr>();
   
    public NSArray <NSTimestamp> joursDuMois () {
		
	    /**
	     * 
	     * Contrôle
	     * 
	     */
		
		if (getDebut()==null) return null;
		if (getFin()==null) return null;
		
		if (getDebut().after(getFin())) return null;
		
		if (date_debut!=null && date_fin!=null && equals(getDebut(), date_debut) && equals(getFin(), date_fin)) return joursCalculeDuMois;
		
		date_debut = getDebut();
		date_fin = getFin();
		joursCalculeDuMois = null;

   	    finPlusUnJour =  Toolbox.opJoursDate(date_fin,+1);
   	    
		int date_debut_mois = Integer.valueOf(moisDate(date_debut));
		int date_debut_annee = Integer.valueOf(anneeDate(date_debut));
		
		int date_fin_mois = Integer.valueOf(moisDate(date_fin));
		int date_fin_annee = Integer.valueOf(anneeDate(date_fin));
		
		int annee = 0;
		int mois = 0; 
		
		 NSMutableArray <NSTimestamp> listeDate = new NSMutableArray <NSTimestamp>();
	     listeMois.removeAllObjects();
			
		Calendar cal = Calendar.getInstance();
		
		/**
		 * remplissage du tableau de date et description du dessin à l'écran
		 */
		
		for (annee = date_debut_annee; annee<=date_fin_annee; annee++ ) {   
		   if (annee==date_debut_annee) {
			   mois = date_debut_mois;
		   } else {   
			        mois = 1;
		   }
		   
		   int fin_mois = 12;
		   if (annee==date_fin_annee) {
			   fin_mois = date_fin_mois;
		   }
		   
	       for (;mois<=fin_mois; mois++) {	    	
	    	         cal.set(annee, mois - 1, 1,0,0,0);
		  	         int jours = cal.getActualMaximum(Calendar.DAY_OF_MONTH); 
		             descr d = new descr();	
			         d.annee = annee;
			         d.mois = mois;
			         d.nbjours = jours;
		             listeMois.add(d);
		  }		
		}		
		int d1=0; int d2=NOMBRE_MOIS_PAR_TABLE - 1;
		boolean notFin = true;
		int n = listeMois.size();
		int k=0;
		table_descr = new int[n*31];
		table_mois = new int[n*31];
		while (notFin) {
		 if (d2>=n) {
			        d2 = n-1;
			        notFin = false;
		 }
		 int t = d2-d1+1;
		 for (int i=1; i<=31; i++) {
			 /**
		      * Test pour affichage avant et après  
		      */
			 for (int j=d1; j<=d2;j++) {
			   if (i==1) {
				    if (j==d1) { 
				           table_descr[k] = IS_DATE_DEBUT_JOUR_MOIS_DEBUT_TABLE;
				           table_mois[k] = t; k++;
				    } else if (j==d2) {
				    	   table_descr[k] = IS_DATE_FIN_JOUR;
				           table_mois[k] = t; k++;
				    } else {
				    	   table_descr[k] = IS_DATE;
                           table_mois[k] = 0; k++;
				    }
			   }
			   if (i==31) {
				   if (j==d1) {
				            table_descr[k] = IS_DATE_DEBUT_JOUR;
				            table_mois[k] = t; k++;
				   } else if (j==d2) {
					        table_descr[k] = IS_DATE_FIN_JOUR_FIN_TABLE;
			                table_mois[k] = 0; k++;
				   } else {
					        table_descr[k] = IS_DATE;
			                table_mois[k] = 0; k++; 
				   }
			   } 
			   if (i>1 && i<31) {
				     if (j==d1) {
				             table_descr[k] = IS_DATE_DEBUT_JOUR;
	                         table_mois[k] = t; k++;
				     } else if (j==d2) {
				    	 table_descr[k] = IS_DATE_FIN_JOUR;
                         table_mois[k] = 0; k++;
				     } else {
				    	 table_descr[k] = IS_DATE;
                         table_mois[k] = 0; k++;
				     }
			   }
			   if (i<=listeMois.get(j).nbjours) {
				         cal.set(listeMois.get(j).annee,listeMois.get(j).mois - 1, i,0,0,0);      
				         listeDate.add(new NSTimestamp(cal.getTimeInMillis()+ cal.getTimeZone().getOffset(cal.getTimeInMillis())));
			   } else {listeDate.add(new NSTimestamp(dateNull)); }
			 }		 
	     }
		 d1 = d2 + 1; d2 = d1 + NOMBRE_MOIS_PAR_TABLE - 1;
		}
		joursCalculeDuMois = listeDate;
		return listeDate;
	}
	
	public String nomMois () {
		return lesMois[Integer.valueOf(moisDate(unJour))-1];
		
	}
	/**
	 * 
	 * Boolean
	 * 
	 */

	 public boolean isDebutTable () {
		 
		 return table_descr[indexJour]==IS_DATE_DEBUT_JOUR_MOIS_DEBUT_TABLE;
		 
	 }
     public boolean isFinTable () {
		 
		 return table_descr[indexJour]==IS_DATE_FIN_JOUR_FIN_TABLE;
		 
	 }
     public boolean isDebutMois () {
		 
		 return table_descr[indexJour]==IS_DATE_DEBUT_JOUR_MOIS_DEBUT_TABLE;
		 
	 }
     public boolean isDebutJour () {
		 
		 return table_descr[indexJour]==IS_DATE_DEBUT_JOUR || table_descr[indexJour]==IS_DATE_DEBUT_JOUR_MOIS_DEBUT_TABLE;
		 
	 }
     public boolean isFinJour () {
		 
		 return table_descr[indexJour]==IS_DATE_FIN_JOUR || table_descr[indexJour]==IS_DATE_FIN_JOUR_FIN_TABLE;
		 
	 }
     public boolean isJourValide () {
    	 
		 return unJour.after(dateNull) && (unJour.after(date_debut) && unJour.before(finPlusUnJour));
		 
	 }
     public boolean isDimanche () {
		 
		 return jourNom().equals("dimanche") || jourNom().equals("samedi");
		 
	 }
     public String ligneMois () {
    	 String ligne = "<tr style=\"background-color:ForestGreen\">";
    	 int debut = indexJour / 31;
    	 for (int i = debut; i <(debut+table_mois[indexJour]);i++) {
    		 ligne += "<td colspan =\"2\"> <b>" + lesMois[(listeMois.get(i).mois-1)] + "&nbsp;&nbsp;" +listeMois.get(i).annee + "</b></td>";
    	 }
    	 ligne += "</tr>";
    	 return ligne;
     }
     
     public String nbJourSelections () {
    	 
    	 if (getSelections()==null) return "0"; 
    	 return String.valueOf(getSelections().size());
    	 
     }
     
	/**
	 *  selection des dates 
	 *  
	 * @param selected
	 * 
	 */
	
	public void setSelectedItem(boolean selected) {
        // liste selectionnée
		if (getSelections()==null) setSelections(new NSMutableArray<NSTimestamp>());
		
	    boolean hasItem = contains(unJour);
		if (selected) {
	        if (! hasItem) {
	            getSelections().addObject(unJour);
	        }
	    } else if (hasItem) {
	        removeUnJour(unJour);
	    }
	}

	public boolean selectedItem() {
		if (getSelections()==null) return false;
	    return contains(unJour);
	} 
	
	public String dateDuJour (NSTimestamp date) {
		StringBuffer format = new StringBuffer("%d%m%Y");
		return new NSTimestampFormatter(format.toString()).format(date);
	}
	
	public boolean equals (NSTimestamp a, NSTimestamp b){	 
		// Sûr mais long
		// return dateDuJour(a).equals(dateDuJour(b));
		
		// Rapide attention aux arrondis.
		long aL = a.getTime();
		long bL = b.getTime();
		long diff = 0;
		if (aL>bL) diff = aL-bL; else diff = bL-aL;
		
		// Ajoute 3H pour éviter les erreurs 
		diff +=10800000L ;
		
		return ((diff/Toolbox.MILLISECS_PER_DAY)==0);
	}
	
	public void removeUnJour (NSTimestamp d) {
		NSMutableArray<NSTimestamp> liste = getSelections();
		for (int i=0; i<liste.size();i++) if (equals(d, liste.get(i))){ liste.removeObjectAtIndex(i); return; }
	}
	public boolean contains (NSTimestamp d) {
		NSMutableArray<NSTimestamp> liste = getSelections();
		if (liste==null) return false;
		for (int i=0; i<liste.size();i++) if (equals(d, liste.get(i))) return true;
		return false;
	}
}