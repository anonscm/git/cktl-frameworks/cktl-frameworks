/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import java.io.IOException;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneDetailView;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public class InternatPersonneDetailView extends PersonneDetailView {
    public InternatPersonneDetailView(WOContext context) {
        super(context);
    }
    public static final String BINDING_showEtudiantUI = "showEtudiantUI";
	public static final Boolean DEFAULT_showEtudiantUI = Boolean.FALSE;
    public Boolean showEtudiantUI() {
		return booleanValueForBinding(BINDING_showEtudiantUI, DEFAULT_showEtudiantUI);
    } 
    
    public String personneNom(){
       if (getPersonne() != null) {
    	   if (isIndividu()) {
    		   EOIndividu ind = (EOIndividu) getPersonne();
    		   return  ind.toCivilite().cCivilite() 
    	               + " " + ind.nomAffichage().toUpperCase()
    	               + " " + StringCtrl.capitalizeWords(ind.prenomAffichage())
    	               + " (N° " + getPersonne().getNumero() + ")";
    	   }                         
    	   return getPersonne().getNomCompletAffichage() + " (N° " + getPersonne().getNumero() + ")";
         }
        return "";
       }
     // photo ! photo ! photo ! photo ! 
     // Méthode qui évite le bug historique LONG RAW et BLOB 
    
      public NSData datasPhoto() {
    	  if (!isIndividu()) return null;
    	  NSData result = null;
    
    	  Integer noIndividu = getIndividu().noIndividu();
    	 
    	  // Tout le monde est mis dans photo etudiants !
    	  
    	  String sqlPhotos = "SELECT DATAS_PHOTO FROM PHOTO.PHOTOS_ETUDIANTS_GRHUM WHERE NO_INDIVIDU = "+noIndividu;
    	 
    	  try  {
		    	  NSArray sqlResults = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat",sqlPhotos , null);
		    	  if ( sqlResults != null && sqlResults.count() > 0 ) {
		    		  NSDictionary dic = (NSDictionary)sqlResults.get(0);
		    		  NSData data = (NSData)dic.get("DATAS_PHOTO");
		    		  if ( data != null && data.length() > 0 ) result = data;
		    	  }  
		    } catch(Exception e) {
	                         // e.printStackTrace();
	           }
    	  
    	  // Au cas ou ... mais pas necessaire
    	  if ( result == null ) {
    		  sqlPhotos = "SELECT DATAS_PHOTO FROM GRHUM.PHOTOS_EMPLOYES_GRHUM WHERE NO_INDIVIDU = "+noIndividu;
	    	  try  {
	    		  NSArray sqlResults = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat",sqlPhotos , null);
	    	     if ( sqlResults != null && sqlResults.count() > 0 ) {
	    		    NSDictionary dic = (NSDictionary)sqlResults.get(0);
	    		    NSData data = (NSData)dic.get("DATAS_PHOTO");
	    		    if ( data != null && data.length() > 0 ) result = data;
	    	      }
	    	        } catch(Exception e) {
	    		         //  e.printStackTrace();
	    	        }
    	  }
    	  
    	  if ( result == null ) {
    		  WOResourceManager resManager = application().resourceManager();
	    	  try {
				result = new NSData(resManager.pathURLForResourceNamed("images/photo-defaut.jpeg", "FwkCktlInternatGuiAjax", null));
			} catch (IOException e) {
				// e.printStackTrace();
			}
    	  }
    	  return result;
      }
    
    
    
    
}