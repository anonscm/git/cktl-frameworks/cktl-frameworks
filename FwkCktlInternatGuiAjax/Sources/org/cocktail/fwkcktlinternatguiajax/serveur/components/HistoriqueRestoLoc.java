/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement;
import org.cocktail.fwkcktlinternat.serveur.metier.EORestauration;
import org.cocktail.fwkcktlinternat.serveur.util.Toolbox;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

/**
 * 
 *  Retourne un historique de la la location et la restauration d'un individu sur une période. 
 * 
 * @author rlouissidney
 *
 *
 * Binding persId
 * 
 */

public class HistoriqueRestoLoc extends AComponent {
    public HistoriqueRestoLoc(WOContext context) {
        super(context);
        debut = Toolbox.opJoursDate( new NSTimestamp(), -365);
        fin = Toolbox.opJoursDate( new NSTimestamp(), 365);
    }
    
    public final String containerHistoriqueID = getComponentId() + "_containerHistorique";
    
    private NSTimestamp debut;
	
    private NSTimestamp fin;
    
    static final String BINDING_PERSID = "persId";
    
    public void setDebut(NSTimestamp debut) {
		 this.debut = debut;
	}
	
	public NSTimestamp getDebut() {
		return debut;
	}
	
	public void setFin(NSTimestamp fin) {
		this.fin = fin;
	}
	
	public NSTimestamp getFin() {
		return fin;
	}
	
	public void setPersId(Integer i) {
		setValueForBinding(i, BINDING_PERSID);
	}
	
	public Integer getPersId() {
		return (Integer) valueForBinding(BINDING_PERSID);
	}
	
	public NSArray<EODetailHebergement> listeLocation () {
    	if (getDebut() == null || getFin() == null) return null;
    	if (getFin().before(getDebut())) return null;
    	if (getPersId()==null) return null;
    	
		EOQualifier dateQual = ERXQ.lessThanOrEqualTo(EODetailHebergement.DATE_DEBUT_KEY,getFin()).and(ERXQ.greaterThanOrEqualTo(EODetailHebergement.DATE_FIN_KEY,getDebut()));
		EOQualifier individuQual = ERXQ.equals(EODetailHebergement.PERS_ID_CONCERNER_KEY, getPersId());
		EOQualifier qual = ERXQ.and(dateQual).and(individuQual);
		
		NSArray<EOSortOrdering> sort = ERXS.desc(EODetailHebergement.DATE_DEBUT_KEY).array();
		return EODetailHebergement.fetchAll(edc(), qual, sort); 
	}
	private EODetailHebergement unDetailHebergement;
	public void setUnDetailHebergement(EODetailHebergement unDetailHebergement) {this.unDetailHebergement = unDetailHebergement;}
	public EODetailHebergement getUnDetailHebergement() {	return unDetailHebergement; }

	public NSArray<EORestauration> listeRestauration () {
    	if (getDebut() == null || getFin() == null) return null;
    	if (getFin().before(getDebut())) return null;
    	if (getPersId()==null) return null;
    	
		EOQualifier dateQual = ERXQ.lessThanOrEqualTo(EORestauration.DATE_DEBUT_KEY,getFin()).and(ERXQ.greaterThanOrEqualTo(EORestauration.DATE_FIN_KEY,getDebut()));
		EOQualifier individuQual = ERXQ.equals(EORestauration.PERS_ID_KEY, getPersId());
		EOQualifier qual = ERXQ.and(dateQual).and(individuQual);
		
		NSArray<EOSortOrdering> sort = ERXS.desc(EORestauration.DATE_DEBUT_KEY).array();
		return EORestauration.fetchAll(edc(), qual, sort); 
	}
	private EORestauration uneRestauration;
	public EORestauration getUneRestauration() { return uneRestauration;}
	public void setUneRestauration(EORestauration uneRestauration) {this.uneRestauration = uneRestauration;}
	
	public String statut_location () {
		if (unDetailHebergement.dateRemiseCle()==null) return "Réservation ";
		return "Location ";
	}
}