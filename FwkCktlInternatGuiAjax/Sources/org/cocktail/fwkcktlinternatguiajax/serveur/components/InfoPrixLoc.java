package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlinternat.serveur.finder.FinderTarif;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeCalcul;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif;
import org.cocktail.fwkcktlinternat.serveur.util.Toolbox;


public class InfoPrixLoc extends AComponent {
	private final String BINDING_TypeTarif   = "typeTarif";
	private final String BINDING_TypeCalcul  = "typeCalcul";
	private final String BINDING_DateCalcul  = "dateCalcul";
		
	private String info;
	private FinderTarif calculateurTarif;
	
	public String getInfo() {
		return this.info;
	}
    public InfoPrixLoc(WOContext context) {
        super(context);
    }
       
    public String getModalWindowId() {
    	return getComponentId() + "_win";
    }
    
    public String getButtonId() {
         return getComponentId() + "_Bt";
    }
      
    public WOActionResults onOpenInfoPrix() {
       	calculateurTarif = new FinderTarif();
       	EOTypeTarif leTypeTarif = (EOTypeTarif)   valueForBinding(BINDING_TypeTarif);
       	EOTypeCalcul leTypeCalcul = (EOTypeCalcul)  valueForBinding(BINDING_TypeCalcul);
       	NSTimestamp laDateDeCalCul =  (NSTimestamp)   valueForBinding(BINDING_DateCalcul);
       	
       	info = calculateurTarif.getPrixTTC(edc(),leTypeTarif ,leTypeCalcul, laDateDeCalCul );
        	
       	info = "Prix total calculé au " 
       	        + (laDateDeCalCul==null ? "???" : Toolbox.DateDuJour(laDateDeCalCul)) 
       	        + " : " + (info==null ? "Aucun tarif trouvé" : info); 
       	
    	return null;
    }
        
    public WOActionResults fermer() {
       	calculateurTarif = null;
       	info = null;
       	CktlAjaxWindow.close(context(), getModalWindowId());
    	return null;
    }
}