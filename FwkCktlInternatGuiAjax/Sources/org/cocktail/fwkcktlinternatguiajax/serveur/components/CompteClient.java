/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import java.util.Enumeration;

import org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements;

import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;



public class CompteClient extends AComponent {
    public CompteClient(WOContext context) {
        super(context); 
    }
    
    static final String BINDING_PERSID = "persId";
    
    public void setPersId(Integer i) {
    	setValueForBinding(i, BINDING_PERSID);
    }

    public Integer getPersId() {
    	return (Integer) valueForBinding(BINDING_PERSID);
    }
    
    public Double dettesRestaurant() {
    	// Restauration
        String sql = "SELECT SUM(vfns.solde_a_payer) as DETTES FROM litchi.v_factures_non_soldees vfns WHERE vfns.persid_payeur = "+String.valueOf(getPersId())+" AND vfns.id_type_facture = 2";
   
        NSArray rows = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat", sql, null);
        if((rows != null) && (rows.count()>0)) {
               Enumeration e = rows.objectEnumerator();
               while(e.hasMoreElements()){
                      NSDictionary dic = (NSDictionary) e.nextElement();
                 if (dic.objectForKey("DETTES")==NSKeyValueCoding.NullValue) return null;
                 return (Double) dic.objectForKey("DETTES");
       
             }    
         } 
      return null;
    }
    
    public Double dettesHebergement() {
    	// Hébergement 
    	
        String sql = "SELECT SUM(vfns.solde_a_payer) as DETTES FROM litchi.v_factures_non_soldees vfns WHERE vfns.persid_payeur = "+String.valueOf(getPersId())+" AND vfns.id_type_facture = 1";
   
        NSArray rows = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat", sql, null);
        if((rows != null) && (rows.count()>0)) {
               Enumeration e = rows.objectEnumerator();
               while(e.hasMoreElements()){
                      NSDictionary dic = (NSDictionary) e.nextElement();
                 if (dic.objectForKey("DETTES")==NSKeyValueCoding.NullValue) return null;
                 return (Double) dic.objectForKey("DETTES");
             }    
         }
        return null;
    }
    
    public Double soldeCAF() { 	
          String sql = "SELECT LITCHI.SOLDE_CAF("+String.valueOf(getPersId())+")AS SOLDECAF FROM DUAL";
     
          NSArray rows = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat", sql, null);
          if((rows != null) && (rows.count()>0)) {
               Enumeration e = rows.objectEnumerator();
               while(e.hasMoreElements()){
  	               NSDictionary dic = (NSDictionary) e.nextElement();
  	               
  	               if (dic.objectForKey("SOLDECAF")==NSKeyValueCoding.NullValue) return null;
  	               else return (Double) dic.objectForKey("SOLDECAF");
               }    
          }
    	return null;
    }
    public Double soldeArrhes(){
        String sql = "SELECT  LITCHI.SOLDE_ARRHES("+String.valueOf(getPersId())+") AS SOLDEARRHES FROM DUAL";
   
        NSArray rows = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat", sql, null);
        if((rows != null) && (rows.count()>0)) {
             Enumeration e = rows.objectEnumerator();
             while(e.hasMoreElements()){
	               NSDictionary dic = (NSDictionary) e.nextElement();
	               if (dic.objectForKey("SOLDEARRHES")==NSKeyValueCoding.NullValue) return null;
	               return (Double) dic.objectForKey("SOLDEARRHES");
             }    
        }
  	    return null;
    }
    
    public Double soldeAvancesHeb() {
        String sql = "SELECT LITCHI.SOLDE_AVANCES("+String.valueOf(getPersId())+",1) AS SOLDEAVANCES  FROM DUAL";
        NSArray rows = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat", sql, null);
        if((rows != null) && (rows.count()>0)) {
             Enumeration e = rows.objectEnumerator();
             while(e.hasMoreElements()){
	               NSDictionary dic = (NSDictionary) e.nextElement();	               
	               if (dic.objectForKey("SOLDEAVANCES")==NSKeyValueCoding.NullValue) return null;
	               return (Double) dic.objectForKey("SOLDEAVANCES");
             }    
        }
  	    return null;
    }
    public Double soldeAvancesResto() {
        String sql = "SELECT LITCHI.SOLDE_AVANCES("+String.valueOf(getPersId())+",2) AS SOLDEAVANCES  FROM DUAL";
        NSArray rows = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat", sql, null);
        if((rows != null) && (rows.count()>0)) {
             Enumeration e = rows.objectEnumerator();
             while(e.hasMoreElements()){
	               NSDictionary dic = (NSDictionary) e.nextElement();	               
	               if (dic.objectForKey("SOLDEAVANCES")==NSKeyValueCoding.NullValue) return null;
	               return (Double) dic.objectForKey("SOLDEAVANCES");
             }    
        }
  	    return null;
    }
    public Double depotsGarantie (){
    	String sql = "SELECT SUM(ea.montant) AS DEPOT" 
                     +" FROM LITCHI.element_associe ea, LITCHI.repart_det_heb_ele_ass rdhea, LITCHI.ligne_facture lf, LITCHI.entete_facture ef,  LITCHI.detail_hebergement dh"
                     +" WHERE ea.code_element_associe = rdhea.id_element_associe"
                     +" AND ea.id_type_remboursement = 3"
                     +" AND rdhea.montant_rembourse IS NULL"
                     +" AND rdhea.date_remboursement IS NULL"
                     +" AND rdhea.id_ligne_facture IS NOT NULL"
                     +" AND rdhea.id_ligne_facture = lf.id_ligne_facture"
                     +" AND lf.id_facture = ef.id_facture"
                     +" AND ef.mnt_a_payer_ttc = (SELECT SUM (lre.montant_ttc) FROM LITCHI.lignes_reglement lre WHERE lre.id_facture = ef.id_facture)"
                     +" AND rdhea.id_detail_hebergement = dh.id_detail_hebergement"
                     +" AND dh.pers_id_concerner = "+String.valueOf(getPersId());
    	   
        NSArray rows = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat", sql, null);
        if((rows != null) && (rows.count()>0)) {
               Enumeration e = rows.objectEnumerator();
               while(e.hasMoreElements()){
                      NSDictionary dic = (NSDictionary) e.nextElement();
                 if (dic.objectForKey("DEPOT")==NSKeyValueCoding.NullValue) return null;
                 return (Double) dic.objectForKey("DEPOT");
             }    
         }
        return null;
    }   
    
    public String etatPrelevement() {
    	EOPrelevements pre;
    	EOQualifier qual = ERXQ.equals(EOPrelevements.PERS_ID_KEY, getPersId());
    	NSTimestamp dateDuJour = new NSTimestamp(); 
    	EOQualifier dateQual = ERXQ.and(
    	     ERXQ.isNull(EOPrelevements.DATE_DEBUT_KEY).or(ERXQ.lessThanOrEqualTo(EOPrelevements.DATE_DEBUT_KEY, dateDuJour)),
    	     ERXQ.isNull(EOPrelevements.DATE_FIN_KEY).or(ERXQ.greaterThanOrEqualTo(EOPrelevements.DATE_FIN_KEY, dateDuJour)));
    	
		NSArray<EOPrelevements> lesPre = EOPrelevements.fetchAll(edc(),ERXQ.and(qual,dateQual), null);
		if (lesPre!=null && lesPre.size()>0) pre = lesPre.get(0); else pre = null;
	    if (pre==null) return ("NON");	
	    String s = pre.ribfourUlr().fwkpers_Banque().domiciliation();
	    if (!pre.ribfourUlr().ribValide().equals("O")) s = " : RIB invalide !";
    	return s;
    }
    
    
    
}