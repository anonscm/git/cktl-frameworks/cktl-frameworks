/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneGestion;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import er.extensions.eof.ERXEC;

public class InternatPersonneGestion extends PersonneGestion {
	
	public InternatPersonneGestion(WOContext context) {
        super(context);  
       
    }	
	public ERXEC edcGestion = (ERXEC) ERXEC.newEditingContext();  
    

	public static final String BINDING_onSelectionnerPersonne = "onSelectionnerPersonne";
	public static final String BINDING_selectedPersonne = "selectedPersonne";	
	
	
	//--------------------------
	// Mes procédures : je sépare les editingcontext
	//--------------------------

	public WOActionResults onSelectionnerPersonne() {
		IPersonne personne = null;
		if (getSelectedPersonne()!= null) personne = PersonneDelegate.fetchPersonneByPersId(edc(), getSelectedPersonne().persId());
		setValueForBinding(personne, BINDING_selectedPersonne);
		if (valueForBinding(BINDING_onSelectionnerPersonne) != null) {
			return performParentAction((String) valueForBinding(BINDING_onSelectionnerPersonne));
		}
		return null;
	}	
	
    public WOActionResults onFormEnregistrer() {
		setIsVueSrch(Boolean.TRUE);
		setWantRefreshDetail(Boolean.TRUE);
		IPersonne personne = null;
		if (getSelectedPersonne()!= null) personne = PersonneDelegate.fetchPersonneByPersId(edc(), getSelectedPersonne().persId());
		setValueForBinding(personne, BINDING_selectedPersonne);
		if (valueForBinding(BINDING_callbackAfterFormEnregistrer) != null) {
			return performParentAction((String) valueForBinding(BINDING_callbackAfterFormEnregistrer));
		}
		return null;
	}
}