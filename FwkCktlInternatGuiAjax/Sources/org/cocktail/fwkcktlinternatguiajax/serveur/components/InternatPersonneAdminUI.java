/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import org.cocktail.fwkcktlinternat.serveur.finder.FinderAllocataires;
import org.cocktail.fwkcktlinternat.serveur.finder.FinderPersonne;
import org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire;
import org.cocktail.fwkcktlinternat.serveur.metier.EOPersonneStatut;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneAdminUI;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;


public class InternatPersonneAdminUI extends PersonneAdminUI {
    public InternatPersonneAdminUI(WOContext context) {
        super(context);
    }
    
    private Integer numeroEtudiant=null;
    private String numCAF=null;
    
    public String numEtudiant() {
    	return String.valueOf(numeroEtudiant);
    }
    
    public boolean isEtudiant() {
    	return numeroEtudiant!=null;
    }
    
    public String numAllocataire() {
    	if (numCAF==null) isAllocataire();
    	return numCAF;
    }
    
    public boolean isAllocataire() {
    	numCAF = FinderAllocataires.numerosAllocataire(edc(), getPersonne().persId());
    	return numCAF!=null;
    }
    
    public boolean isStatutInternat() {
    	return EOPersonneStatut.fetchByKeyValue(edc(), EOPersonneStatut.PERSI_ID_KEY, getPersonne().persId())!=null;
    }
    public String statutStructure() {
    	EOPersonneStatut internatStatus = EOPersonneStatut.fetchByKeyValue(edc(), EOPersonneStatut.PERSI_ID_KEY, getPersonne().persId()); 
    	if (internatStatus==null) return "";
    	return internatStatus.typeStatut().libTypeStatut();
    }
    
    public String statutIndividu() {  
    	NSDictionary myDic =  FinderPersonne.getIndividuStatus(edc(),getIndividu().persId());
    	String statusIndividu = new String(); 
    	
    	if (myDic!=null) {
    	   statusIndividu = ( myDic.valueForKey("003statut") == NSKeyValueCoding.NullValue ) ? null : (String)(myDic.valueForKey("003statut"));
    	   this.numeroEtudiant  = ( myDic.valueForKey("002etudNumero") == NSKeyValueCoding.NullValue ) ? null : (Integer)( myDic.valueForKey("002etudNumero")); 
    	}   	
    	
    	// Ajoute le département si c'est un étudiant
    	if (this.numeroEtudiant !=null) {
    	    String dept = FinderPersonne.getEtudiantInscriptionFromPersid (edc(),getIndividu().persId());
    	    if (dept!=null && statusIndividu!=null) statusIndividu += " : " + dept;
    	}
    	return statusIndividu;
    }
    
}