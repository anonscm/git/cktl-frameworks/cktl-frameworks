package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import java.io.IOException;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlinternat.serveur.finder.FinderPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

public class fichePhoto extends AComponent {
    public fichePhoto(WOContext context) {
        super(context);
    }
    
    IPersonne personne;
    
    private static final String BINDING_persId = "persId";
    
    public String getModalWindowId() {
		return getComponentId() + "_win";
	}

	public WOActionResults fermer() {
		CktlAjaxWindow.close(context(), getModalWindowId());
		return null;
	}
	
	public WOActionResults onOpenFiche() {
		return null;
	}
	
	public String nom () {
		Integer persId = valueForIntegerBinding(BINDING_persId, 0);
	    if (persId.intValue()==0) return null;
	    return FinderPersonne.personneCiviliteNomPrenom(edc(), persId);
	}
	
    // photo ! photo ! photo ! photo ! 
    // MÃ©thode qui Ã©vite le bug historique LONG RAW et BLOB 
   
     public NSData datasPhoto() {
    	 
       Integer persId = valueForIntegerBinding(BINDING_persId, 0);
       
       if (persId.intValue()==0) return null;
       
       IPersonne personne = PersonneDelegate.fetchPersonneByPersId(edc(), persId);
       
       if (!personne.isIndividu()) return null;
    	 
   	  NSData result = null;
   
   	  Integer noIndividu = ((EOIndividu) personne).noIndividu();
   	 
   	  // Tout le monde est mis dans photo etudiants !
   	  
   	  String sqlPhotos = "SELECT DATAS_PHOTO FROM PHOTO.PHOTOS_ETUDIANTS_GRHUM WHERE NO_INDIVIDU = "+noIndividu;
   	 
   	  try  {
		    	  NSArray sqlResults = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat",sqlPhotos , null);
		    	  if ( sqlResults != null && sqlResults.count() > 0 ) {
		    		  NSDictionary dic = (NSDictionary)sqlResults.get(0);
		    		  NSData data = (NSData)dic.get("DATAS_PHOTO");
		    		  if ( data != null && data.length() > 0 ) result = data;
		    	  }  
		    } catch(Exception e) {
	                         // e.printStackTrace();
	           }
   	  
   	  // Au cas ou ... mais pas necessaire
   	  if ( result == null ) {
   		  sqlPhotos = "SELECT DATAS_PHOTO FROM GRHUM.PHOTOS_EMPLOYES_GRHUM WHERE NO_INDIVIDU = "+noIndividu;
	    	  try  {
	    		  NSArray sqlResults = EOUtilities.rawRowsForSQL(edc(), "FwkCktlInternat",sqlPhotos , null);
	    	     if ( sqlResults != null && sqlResults.count() > 0 ) {
	    		    NSDictionary dic = (NSDictionary)sqlResults.get(0);
	    		    NSData data = (NSData)dic.get("DATAS_PHOTO");
	    		    if ( data != null && data.length() > 0 ) result = data;
	    	      }
	    	        } catch(Exception e) {
	    		         //  e.printStackTrace();
	    	        }
   	  }
   	  
   	  if ( result == null ) {
   		  WOResourceManager resManager = application().resourceManager();
	    	  try {
				result = new NSData(resManager.pathURLForResourceNamed("images/photo-defaut.jpeg", "FwkCktlInternatGuiAjax", null));
			} catch (IOException e) {
				// e.printStackTrace();
			}
   	  }
   	  return result;
     }

}