/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternatguiajax.serveur.components;

import org.cocktail.fwkcktlinternat.serveur.finder.FinderPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;


public class InternatEtudiantUI extends AComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_personne = "personne";
	public static final String BINDING_utilisateurPersId = "utilisateurPersId";
	
	private EOEtudiant UnEtudiant;
	private EOEditingContext editingContext;
	private IPersonne Personne;
	private Integer utilisateurPersId;
	
	Integer StudentNumber;

	public IPersonne getPersonne() {
		Personne = (IPersonne) valueForBinding(BINDING_personne);
		return Personne;
	}
	
	public void setPersonne(IPersonne Personne) {
		this.Personne = Personne;
		setValueForBinding(Personne, BINDING_personne);
	}
	
	public InternatEtudiantUI(WOContext context) {
        super(context);
    }
	
	public boolean LoadStudent(){
		// Chercher etudiant
		if (getPersonne() instanceof EOIndividu)			
			UnEtudiant = FinderPersonne.getEtudiantFromIndividu(geteditingContext(), (EOIndividu) Personne);	 
		return (this.UnEtudiant != null);
	}
	
	public String getStudentNumber () {
			if (this.UnEtudiant != null) 
				return String.valueOf(this.UnEtudiant.etudNumero());
		return null;
	}
	
	public String StudentStatus() {
		// L'étudiant a déjà été cherché
		return FinderPersonne.getEtudiantInscriptionFromEtudiant(geteditingContext(), this.UnEtudiant);
	}
	
	public void setutilisateurPersId(Integer utilisateurPersId) {
		this.utilisateurPersId = utilisateurPersId;
		setValueForBinding(utilisateurPersId, BINDING_utilisateurPersId);
	}
	public Integer getutilisateurPersId() {
		utilisateurPersId = (Integer) valueForBinding(BINDING_utilisateurPersId);
		return utilisateurPersId;
	}
	
	public EOEditingContext geteditingContext() {
		editingContext = (EOEditingContext) valueForBinding(BINDING_editingContext);
		return editingContext;
	}
	public void seteditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
		setValueForBinding(editingContext, BINDING_editingContext);
	}
	
	
}