/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOBac;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOMention;
import org.cocktail.scolarix.serveur.metier.eos.EOProfession;
import org.cocktail.scolarix.serveur.metier.eos.EORdvCandidat;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolarix.serveur.metier.eos.EOSituationScol;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeHebergement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public interface IEtudiant {

	public EOEditingContext edc();

	/**
	 * @return Integer le numero etudiant (etudNumero)
	 */
	public Integer numero();

	/**
	 * @param numero
	 *            le numero etudiant
	 */
	public void setNumero(Integer numero);

	/**
	 * @return l'EOEtudiant correspondant qui va bien...
	 */
	public EOEtudiant etudiant();

	/**
	 * Retourne l'historique associe a l'etudiant pour l'annee demandee.
	 * 
	 * @param anneeScol
	 *            l'annee universitaire
	 * @return un EOHistorique pour l'annee demandee, NULL si aucun n'existe.
	 */
	public EOHistorique historique(Integer anneeScol);

	/**
	 * Retourne l'EOHistorique le plus récent AVANT l'annee specifiee.
	 * 
	 * @param anneeEnCours
	 *            L'annee AVANT laquelle on veut chercher l'historique, si NULL retourne l'historique le plus recent.
	 * @return Un EOHistorique
	 */
	public EOHistorique historiquePlusRecent(Integer anneeEnCours);

	/**
	 * @return String Prenom Nom formattes pour l'affichage
	 */
	public String prenomNom();

	public String numeroINE();

	public void setNumeroINE(String numeroINE);

	public String etudReimmatriculation();

	public void setEtudReimmatriculation(String value);

	/**
	 * Reset des informations gardées en cache (comme les formations)
	 */
	public void reset();

	/**
	 * @return EOIndividuUlr attache a l'etudiant
	 */
	public EOIndividu individu();

	public void setIndividu(EOIndividu individu);

	public EOCandidatGrhum candidat();

	/**
	 * Methodes de gestion de la situation familiale
	 */
	public boolean isCelibataire();

	public void setIsCelibataire(Boolean isCelibataire);

	public boolean isEnCouple();

	public void setIsEnCouple(Boolean isEnCouple);

	public boolean isEnfantsACharge();

	public void setIsEnfantsACharge(Boolean isEnfantsACharge);

	/**
	 * @return EOAdresse de l'adresse stable/adresse des parents (derniere adresse connue)
	 */
	public EOAdresse adresseStable();

	/**
	 * @return EOAdresse de l'adresse etudiante (derniere adresse connue)
	 */
	public EOAdresse adresseUniversitaire();

	/**
	 * @return EOPersonneTelephone telephone stable / des parents (dernier connu)
	 */
	public EOPersonneTelephone telephoneStable();

	/**
	 * Positionne le telephone stable fixe de l'etudiant
	 * 
	 * @param telephone
	 */
	public void setTelephoneStable(EOPersonneTelephone telephone);

	/**
	 * @return String le NUMERO de telephone stable / des parents (dernier connu)
	 */
	public String noTelephoneStable();

	/**
	 * Positionne directement le NUMERO de telephone stable fixe de l'etudiant, en le creant si besoin (sinon mise à jour)
	 * 
	 * @param telephone
	 */
	public void setNoTelephoneStable(EOEditingContext ec, String telephone);

	/**
	 * @return EOPersonneTelephone telephone fixe etudiant (dernier connu)
	 */
	public EOPersonneTelephone telephoneUniversitaireFixe();

	/**
	 * Positionne le telephone universitaire fixe de l'etudiant
	 * 
	 * @param telephone
	 */
	public void setTelephoneUniversitaireFixe(EOPersonneTelephone telephone);

	/**
	 * @return String le NUMERO de telephone fixe etudiant (dernier connu)
	 */
	public String noTelephoneUniversitaireFixe();

	/**
	 * Positionne directement le NUMERO de telephone universitaire fixede l'etudiant, en le creant si besoin (sinon mise à jour)
	 * 
	 * @param telephone
	 */
	public void setNoTelephoneUniversitaireFixe(EOEditingContext ec, String telephone);

	/**
	 * @return EOPersonneTelephone telephone portable etudiant (dernier connu)
	 */
	public EOPersonneTelephone telephoneUniversitairePortable();

	/**
	 * Positionne le telephone universitaire mobile de l'etudiant
	 * 
	 * @param telephone
	 */
	public void setTelephoneUniversitairePortable(EOPersonneTelephone telephone);

	/**
	 * @return String le NUMERO de telephone portable etudiant (dernier connu)
	 */
	public String noTelephoneUniversitairePortable();

	/**
	 * Positionne directement le NUMERO de telephone universitaire mobile de l'etudiant, en le creant si besoin (sinon mise à jour)
	 * 
	 * @param telephone
	 */
	public void setNoTelephoneUniversitairePortable(EOEditingContext ec, String telephone);

	/**
	 * @return String = l'email de l'etudiant associe a son adresse type ETUD de RepartPersonneAdresse
	 */
	public String email();

	/**
	 * Positionne l'email de l'etudiant associe a son adresse type ETUD (RepartPersonneAdresse)<BR>
	 * - Si NULL ou vide, met l'email a NULL<BR>
	 * 
	 * @param email
	 */
	public void setEmail(String email);

	public Integer anneeObtentionTitreAcces();

	public void setAnneeObtentionTitreAcces(Integer annee);

	public EOBac titreAcces();

	public void setTitreAcces(EOBac titreAcces);

	public EOMention mentionTitreAcces();

	public void setMentionTitreAcces(EOMention mention);

	public EORne etablissementTitreAcces();

	public void setEtablissementTitreAcces(EORne etablissement);

	public String villeTitreAcces();

	public void setVilleTitreAcces(String ville);

	public EODepartement departementTitreAcces();

	public void setDepartementTitreAcces(EODepartement departement);

	public EOPays paysTitreAcces();

	public void setPaysTitreAcces(EOPays pays);

	public Integer anneePremiereInscriptionEnseignementSuperieur();

	public void setAnneePremiereInscriptionEnseignementSuperieur(Integer annee);

	public Integer anneePremiereInscriptionUniversiteFrancaise();

	public void setAnneePremiereInscriptionUniversiteFrancaise(Integer annee);

	public EORne universitePremiereInscription();

	public void setUniversitePremiereInscription(EORne universite);

	public Integer anneePremiereInscriptionUniversiteLocale();

	public void setAnneePremiereInscriptionUniversiteLocale(Integer annee);

	public String salarieEtablissement(Integer anneeScolaire);

	public void setSalarieEtablissement(String isSalarie, Integer anneeScolaire);

	/**
	 * Retourne l'inscription principale de l'etudiant pour l'annee universitaire demandee.
	 * 
	 * @param anneeScol
	 *            L'annee universitaire pour laquelle on veut retrouver l'inscription principale.
	 * @return un EOInscDipl pour l'inscription principale, NULL si aucune inscription principale trouvee.
	 * @throws EtudiantException
	 *             Si plusieurs inscriptions principales trouvees, ce qui ne devrait jamais se produire.
	 */
	public EOInscDipl inscriptionPrincipale(Integer anneeScol) throws EtudiantException;

	/**
	 * Pour les pre-re-inscriptions et les re-inscriptions.<BR>
	 * Retourne les formations en cours, soit pour l'annee en cours en inscription, soit pour l'annee precedente (ou la derniere annee
	 * d'inscription avant l'année de reinscription en cours) dans le cas d'une [Pre]ReInscription.<BR>
	 * Dans le cas de semestrialisation, les deux premiers elements du tableau representent l'inscription principale (les 2 semestres triés
	 * S1 puis S2).<BR>
	 * Dans la cas de l'annualisation (sans semestres), le premier element reprensente l'insc principale.
	 * 
	 * @return Un NSArray de EOVWebInscriptionResultat, vide si aucune formation en cours.
	 */
	public NSArray formationsEnCours();

	/**
	 * Pour les pre-inscriptions, pre-re-inscriptions, re-inscriptions et inscriptions.<BR>
	 * Retourne un tableau des formations (EOScolFormationHabilitation) pour l'annee de pre/pre-re/re-inscription en cours.<BR>
	 * En PreInscription, ce sont les voeux PostBac.<BR>
	 * En PreReInscription et ReInscription, c'est une deduction faite a partir des resultats de ses formations en cours (liste dynamique en
	 * lien avec les formations envisagees).<BR>
	 * En Inscription, ce sont toutes les formations proposees par l'etablissement.
	 * 
	 * @return Un NSMutableArray de EOScolFormationHabilitation.
	 */
	public NSMutableArray formationsEnvisageables();

	/**
	 * Pour les pre-inscriptions.<BR>
	 * Retourne un tableau de formations (EOScolFormationHabilitation) pour l'annee de pre-inscription en cours.<BR>
	 * Ce sont toutes les formations proposees par l'établissement accessibles aux neo-bacheliers.
	 * 
	 * @return Un NSMutableArray de EOScolFormationHabilitation.
	 */
	public NSMutableArray formationsPossibles();

	/**
	 * Pour les pre-inscriptions, pre-re-inscriptions, re-inscriptions et inscriptions.<BR>
	 * Retourne un tableau de formations (EOScolFormationHabilitation) pour l'annee de pre/re-inscription en cours.<BR>
	 * En PreInscription, c'est le choix effectue par l'etudiant.<BR>
	 * En PreReInscription, ReInscription et Inscription, ce sont les choix effectues par l'etudiant (liste dynamique en lien avec les
	 * formations envisageables).
	 * 
	 * @return Un NSMutableArray de EOScolFormationHabilitation.
	 */
	public NSMutableArray formationsEnvisagees();

	/**
	 * Indique s'il y a un probleme avec ses formations envisagees deja enregistrees suite a des changements dans sa situation (jurys ou
	 * autre).<BR>
	 * Si c'est le cas, renvoie une exception et vide le tableau des formations envisagees.
	 * 
	 * @throws EtudiantException
	 *             Si les formations envisagees ont ete reinitialisees suite a des changements dans la situation de l'etudiant, et qu'il
	 *             doit donc resaisir ses voeux d'inscription.
	 */
	public void alerteFormationsEnvisagees() throws EtudiantException;

	/**
	 * Retourne un tableau de langues vivantes possibles (EOLangue).
	 * 
	 * @return Un NSArray de EOLangue
	 */
	public NSArray languesVivantes();

	public Integer anneeDernierEtabFrequenteAvantAnnee(Integer anneeScol);

	public String enseignementDernierEtabFrequenteAvantAnnee(Integer anneeScol);

	public EORne dernierEtabFrequenteAvantAnnee(Integer anneeScol);

	public EODepartement departementDernierEtabFrequenteAvantAnnee(Integer anneeScol);

	public EOPays paysDernierEtabFrequenteAvantAnnee(Integer anneeScol);

	public EOSituationScol situationAnneePrecedenteDernierEtabFrequenteAvantAnnee(Integer anneeScol);

	/**
	 * 
	 * @return 1 si sportif de haut niveau sinon 0
	 */
	public Integer sportifHautNiveau();

	public void setSportifHautNiveau(Integer valueOf);

	/**
	 * 
	 * @return 1 si attestation de recensement au service national sinon 0
	 */
	public Integer attestationRecensement();

	public void setAttestationRecensement(Integer valueOf);

	/**
	 * 
	 * @return 1 si certificat d'appel au service national sinon 0
	 */
	public Integer certificationAppel();

	public void setCertificationAppel(Integer valueOf);

	public EOProfession professionDuParent1();

	public void setProfessionDuParent1(EOProfession profession);

	public EOProfession professionDuParent2();

	public void setProfessionDuParent2(EOProfession profession);

	public void validateIdentite();

	public void validateAdresses();

	public void validateDiplomes();

	public void validateTitreAcces();

	public void validatePremiereInscription();

	public void validateDernierDiplome();

	public void validateDernierEtablissement();

	public void validateBoursesEtExonerations();

	public void validateSituationSociale();

	public void validateRenseignementsDivers();

	public void validateAutres();

	public void setCharte(String charte);

	public String charte();

	public void setListeRouge(String listeRouge);

	public String listeRouge();

	public void setPhoto(String photo);

	public String photo();

	public EOTypeHebergement toTypeHebergement();

	public void setToTypeHebergementRelationship(EOTypeHebergement typeHebergement);

	public boolean isPreInscription();

	public boolean isPreReInscription();

	public boolean isInscription();

	public boolean isReInscription();

	public boolean isAdmission();

	public boolean isPreInscrit();

	public EORdvCandidat rendezVous();

	public boolean isRendezVousPossible();

	public NSArray userInfos();

	public Integer anneeInscriptionEnCours();

	public void setAnneeInscriptionEnCours(Integer anneeInscriptionEnCours);

	public String etudType();

	public void setEtudType(String etudType);

	public EORne rne();

	public void setRne(EORne rne);

	/**
	 * @param formation
	 *            Un EOScolFormationHabilitation à ajouter aux formations envisagées
	 * @return Un booleen pour indiquer si l'operation s'est bien deroulee
	 * @throws EtudiantException
	 *             Si la formation est deja dans le "panier" ou si on depasse la capacite
	 */
	public boolean addFormationEnvisagee(EOScolFormationHabilitation formation, Integer idiplNumero) throws EtudiantException;

	/**
	 * @param formation
	 *            Un EOScolFormationHabilitation à retirer des formations envisagées
	 * @return Un booleen pour indiquer si l'operation s'est bien deroulee
	 */
	public boolean removeFormationEnvisagee(EOScolFormationHabilitation formation);

	/**
	 * @param inscDipl
	 *            Un EOInscDipl à retirer des formations envisagées
	 * @return Un booleen pour indiquer si l'operation s'est bien deroulee
	 */
	public boolean removeFormationEnvisagee(EOInscDipl inscDipl);

	/**
	 * Sauvegarde l'etudiant.
	 * 
	 * @param databus
	 *            Le DataBus sur lequel on travaille.
	 * @param ec
	 *            EditingContext sur lequel on travaille.
	 * @return Le IEtudiant qui vient d'etre enregistre
	 * @throws ScolarixFwkException
	 *             Si on a un probleme pour enregistrer l'etudiant.
	 */
	public IEtudiant enregistrer(_CktlBasicDataBus databus, EOEditingContext ec) throws ScolarixFwkException, EtudiantException;

}
