/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.factory.FactoryEtudiant;
import org.cocktail.scolarix.serveur.factory.FactoryHistorique;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOPreCandidat;
import org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature;
import org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FinderEtudiant extends Finder {

	/**
	 * Recherche des etudiants par le FetchSpec Recherche<BR>
	 * Bindings pris en compte (*xxx* signifie : caseInsensitiveLike '*xxx*') :<BR>
	 * bacCode, candNumero, departementEtabBac, departementParent, paysEtabBac, rneCodeBac, rneCodeSup, etudAnbac, etudAnnee1InscSup,
	 * etudAnnee1InscUlr, etudAnnee1InscUniv, etudCodeIne, etudReimmatriculation, sitFamEtudiant, etudSnAttestation, etudSnCertification,
	 * etudSportHn, *etudVilleBac*, mentCode, profession, typeHebergement
	 * 
	 * @param ec
	 *            editingContext dans lequel on opere
	 * @param bindings
	 *            dictionnaire contenant les bindings (non NULL)
	 * @return un NSArray contenant des EOEtudiant, sinon NULL (si bindings NULL)
	 */
	public static NSArray<EOEtudiant> getEtudiants(EOEditingContext ec, NSDictionary<String, String> bindings) {
		if (bindings == null) {
			return null;
		}
		NSDictionary<String, String> newBindings = updatedDictionaryForCaseInsensitiveLike(bindings, new NSArray<String>(
				EOEtudiant.ETUD_VILLE_BAC_KEY));
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, EOEtudiant.ENTITY_NAME, "Recherche", newBindings);
	}

	/**
	 * Recherche d'etudiants<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param numeroEtudiant
	 *            FACULTATIF : Numero de l'etudiant
	 * @param numeroIne
	 *            FACULTATIF : Numero INE de l'etudiant
	 * @param nomPatronymique
	 *            FACULTATIF
	 * @param prenom
	 *            FACULTATIF
	 * @param dateDeNaissance
	 *            FACULTATIF : Date de naissance de l'etudiant
	 * @return un NSArray de EOEtudiant, vide si rien trouve
	 */
	public static final NSArray<EOEtudiant> getEtudiants(EOEditingContext ec, Integer annee, Integer numeroEtudiant, String numeroIne,
			String nomPatronymique, String prenom, NSTimestamp dateDeNaissance, EOVEtablissementScolarite etablissement,
			NSArray<EOSortOrdering> sortOrderings) {
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (annee != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiant.TO_HISTORIQUES_KEY + "." + EOHistorique.HIST_ANNEE_SCOL_KEY,
					EOQualifier.QualifierOperatorEqual, annee));
		}
		if (numeroEtudiant != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiant.ETUD_NUMERO_KEY, EOQualifier.QualifierOperatorEqual, numeroEtudiant));
		}
		if (numeroIne != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiant.ETUD_CODE_INE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, numeroIne));
		}
		if (nomPatronymique != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.NOM_PATRONYMIQUE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + nomPatronymique + "*"));
		}
		if (prenom != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.PRENOM_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + prenom + "*"));
		}
		if (dateDeNaissance != null) {
			quals.addObject(Finder.qualifierForDayDate(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.D_NAISSANCE_KEY, dateDeNaissance));
		}
		if (sortOrderings == null) {
			EOSortOrdering nomPatronymiqueOrdering = EOSortOrdering.sortOrderingWithKey(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "."
					+ EOIndividu.NOM_PATRONYMIQUE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
			EOSortOrdering prenomOrdering = EOSortOrdering.sortOrderingWithKey(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.PRENOM_KEY,
					EOSortOrdering.CompareCaseInsensitiveAscending);
			sortOrderings = new NSArray<EOSortOrdering>(new EOSortOrdering[] { nomPatronymiqueOrdering, prenomOrdering });
		}
		NSArray<EOEtudiant> etudiants = EOEtudiant.fetchAll(ec, new EOAndQualifier(quals), sortOrderings);
		if (etudiants != null && etablissement != null) {
			NSMutableArray<EOEtudiant> etudiantsFiltres = new NSMutableArray<EOEtudiant>();
			EORne rneVoulu = etablissement.toRne();
			Enumeration<EOEtudiant> en = etudiants.objectEnumerator();
			while (en.hasMoreElements()) {
				EOEtudiant e = en.nextElement();
				EORne rneEtudiant = e.rne();
				if (rneEtudiant != null && rneEtudiant.equals(rneVoulu)) {
					etudiantsFiltres.addObject(e);
				}
			}
			etudiants = etudiantsFiltres.immutableClone();
		}
		return etudiants;
	}

	/**
	 * Recherche d'un etudiant par son numero d'etudiant et sa date de naissance <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel on opere
	 * @param etudNumero
	 *            OBLIGATOIRE : Numero de l'etudiant
	 * @param dateDeNaissance
	 *            FACULTATIF : Date de naissance de l'etudiant
	 * @param anneeInscription
	 *            FACULTATIF : L'année d'inscription en cours
	 * @return un IEtudiant, sinon NULL
	 */
	public static IEtudiant getEtudiant(EOEditingContext ec, Integer etudNumero, NSTimestamp dateDeNaissance, Integer anneeInscription) {
		if (etudNumero == null) {
			return null;
		}
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.addObject(new EOKeyValueQualifier(EOEtudiant.ETUD_NUMERO_KEY, EOQualifier.QualifierOperatorEqual, etudNumero));
		if (dateDeNaissance != null) {
			quals.addObject(qualifierForDayDate(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.D_NAISSANCE_KEY, dateDeNaissance));
		}
		return EOEtudiant.fetchByQualifier(ec, new EOAndQualifier(quals));
	}
	
	/**
	 * Recherche d'un etudiant par son numero d'individu <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel on opere
	 * @param numeroIndividu
	 *            OBLIGATOIRE : Numero individu de l'etudiant
	 * @param annee
	 *            FACULTATIF : L'année d'inscription en cours
	 * @return un IEtudiant, sinon NULL
	 */
	public static IEtudiant getEtudiantByNumeroIndividu(EOEditingContext ec, Integer numeroIndividu, Integer annee) {
		if (numeroIndividu == null) {
			return null;
		}
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (annee != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiant.TO_HISTORIQUES_KEY + "."
					+ EOHistorique.HIST_ANNEE_SCOL_KEY, EOQualifier.QualifierOperatorEqual, annee));
		}
		quals.addObject(new EOKeyValueQualifier(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.NO_INDIVIDU_KEY,
				EOQualifier.QualifierOperatorEqual, numeroIndividu));

		return EOEtudiant.fetchByQualifier(ec, new EOAndQualifier(quals));
	}
	
	/**
	 * Recherche d'un etudiant par son <code>PERS_ID</code> <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel on opere
	 * @param persId
	 *            OBLIGATOIRE : <code>PERS_ID</code> de l'etudiant
	 * @param annee
	 *            FACULTATIF : L'année d'inscription en cours
	 * @return un IEtudiant, sinon NULL
	 */
	public static IEtudiant getEtudiantByPersId(EOEditingContext ec, Integer persId, Integer annee) {
		if (persId == null) {
			return null;
		}
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (annee != null) {
			quals.addObject(new EOKeyValueQualifier(EOEtudiant.TO_HISTORIQUES_KEY + "."
					+ EOHistorique.HIST_ANNEE_SCOL_KEY, EOQualifier.QualifierOperatorEqual, annee));
		}
		quals.addObject(new EOKeyValueQualifier(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY,
				EOQualifier.QualifierOperatorEqual, persId));
		
		return EOEtudiant.fetchByQualifier(ec, new EOAndQualifier(quals));
	}

	/**
	 * Recherche d'un dossier par son numero d'admission<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel on opere
	 * @param numAdm
	 *            OBLIGATOIRE : numéro d'admission du dossier d'admission
	 * @return un IEtudiant, sinon NULL
	 */
	public static IEtudiant getEtudiantAdmission(EOEditingContext ec, Integer numAdm) {
		if (numAdm == null) {
			return null;
		}
		EOCandidatGrhum candidat = EOCandidatGrhum.fetchFirstRequiredByKeyValue(ec, EOCandidatGrhum.CAND_NUM_ADM_KEY, numAdm);
		return initialiserEtudiantDepuisCandidatAdmission(ec, candidat);
	}

	/**
	 * Renvoie un IEtudiant (EOEtudiant) initialise a partir des infos retrouvees pour le PreEtudiant
	 * 
	 * @param ec
	 *            editingContext dans lequel on opere
	 * @param preEtudiant
	 *            le EOPreEtudiant a partir duquel on recupere l'EOEtudiant
	 * @param cRne
	 *            FACULTATIF : Code RNE de l'etablissement/instance de l'application qui appelle.
	 * @param oldEtudiant
	 *            FACULTATIF : Un IEtudiant qui existe deja, si non null on le met a jour et on renvoie celui-la.
	 * @return un EOEtudiant initialise a partir des infos contenues dans PreEtudiant
	 * @throws EtudiantException
	 *             <UL>
	 *             <LI>Il y a un probleme pour recuperer ses inscriptions de l'annee precedente
	 *             <LI>Ses inscriptions de l'annee precedente ne l'autorisent pas a se reinscrire
	 *             <LI>Il est en attente de jury pour valider au moins un semestre
	 *             <LI>Il est en attente de deliberation de jury pour valider son titre
	 *             <LI>Il n'a aucune formation envisageable pour une autre raison (pas de progression prevue par exemple)
	 *             </UL>
	 */
	public static IEtudiant getEtudiantPreEtudiant(EOEditingContext ec, EOPreEtudiant preEtudiant, String cRne, IEtudiant oldEtudiant)
			throws EtudiantException {
		if (preEtudiant == null) {
			throw new EtudiantException("Pas de Pré-étudiant trouvé.");
		}

		// verif ine/bea
		if (preEtudiant.etudCodeIne() == null) {
			throw new EtudiantException("Il y a un problème avec ce dossier (No INE/BEA manquant).");
		}

		//
		EOEtudiant etudiant = FactoryEtudiant.create(ec, preEtudiant, (EOEtudiant) oldEtudiant);
		//
		etudiant.setIsPreInscrit(true);

		// nettoyage des tableaux de formation qui peuvent eventuellement avoir deja ete initialises et changes
		etudiant.setFormationsEnvisageables(null);
		etudiant.setFormationsPossibles(null);
		etudiant.setFormationsEnvisagees(null);

		etudiant.setUserInfos(new NSMutableArray<EtudiantException>());
		return etudiant;
	}

	/**
	 * Renvoie un IEtudiant (EOEtudiant) initialise a partir des infos retrouvees pour le PreCandidat, selon sa situation (pre ou
	 * pre-re-inscription et deja pre-inscrit une premiere fois ou non).<BR>
	 * Repere le pre-candidat soit par son numero INE/BEA (en priorite), soit par son numero PostBac.<BR>
	 * Peut renvoyer une exception si bloquante, ou renvoyer le IEtudiant avec des EtudiantException dans son userInfos pour signaler
	 * d'éventuelles anomalies (bloquantes ou non selon l'usage que l'on veut en faire).
	 * 
	 * @param ec
	 *            editingContext dans lequel on opere
	 * @param preCandidat
	 *            le EOPreCandidat a partir duquel on recupere l'EOEtudiant
	 * @param cRne
	 *            FACULTATIF : Code RNE de l'etablissement/instance de l'application qui appelle.
	 * @param oldEtudiant
	 *            FACULTATIF : Un IEtudiant qui existe deja, si non null on le met a jour et on renvoie celui-la.
	 * @return un EOEtudiant initialise a partir des infos contenues dans (par ordre de recherche) :
	 *         <UL>
	 *         <LI>PreEtudiant si un pre-etudiant existe (deja pre-inscrit)
	 *         <LI>Sinon soit (exclusif) :
	 *         <UL>
	 *         <LI>PreCandidat s'il s'agit d'une pre-inscription (= nouvel etudiant non encore pre-inscrit)
	 *         <LI>Etudiant s'il s'agit d'une re-inscription
	 *         </UL>
	 *         </UL>
	 * @throws EtudiantException
	 *             Exception bloquante avec message informatif si un des cas suivants se produit :
	 *             <UL>
	 *             <LI>Le candidat n'est pas autorise a suivre cette procedure (preCandidat null)
	 *             <LI>Le candidat n'est pas autorise a s'inscrire dans cet etablissement
	 *             <LI>Le candidat n'a pas de numero ine/bea
	 *             <LI>La periode d'ouverture de la procedure de pre ou re-inscription est close
	 *             <LI>Le candidat n'est plus autorise a suivre cette procedure (deja inscrit administrativement)
	 *             <LI>Il y a un probleme pour recuperer ses preinscriptions (voeux)
	 *             <LI>Le candidat n'est pas inscrit
	 *             <LI>Le candidat a interrompu ses etudes trop longtemps
	 *             <LI>Il y a un probleme pour recuperer ses inscriptions de l'annee precedente
	 *             <LI>Ses inscriptions de l'annee precedente ne l'autorisent pas a se reinscrire
	 *             <LI>Il est en attente de jury pour valider au moins un semestre
	 *             <LI>Il est en attente de deliberation de jury pour valider son titre
	 *             <LI>Il n'a aucune formation envisageable pour une autre raison (pas de progression prevue par exemple)
	 *             </UL>
	 */
	public static IEtudiant getEtudiantPreCandidat(EOEditingContext ec, EOPreCandidat preCandidat, String cRne, IEtudiant oldEtudiant)
			throws EtudiantException {
		verifierCandidatConnu(ec, preCandidat);
		verifierAutoriseInscriptionDansEtablissement(preCandidat, cRne);

		Integer anneePreInscription = FinderParametre.getAnneePreInscriptionAdministrative(ec, preCandidat.toRne().cRne());
		Integer anneePreReInscription = FinderParametre.getAnneePreReInscriptionAdministrative(ec, preCandidat.toRne().cRne());

		verifierInscriptionOuverte(preCandidat, anneePreInscription, anneePreReInscription);

		NSMutableArray<EtudiantException> userInfos = new NSMutableArray<EtudiantException>();

		// verifier s'il est deja inscrit administrativement pour l'annee scolaire de pre ou re-inscription
		if (oldEtudiant == null) {
			if (preCandidat.isInscrit(anneePreInscription, anneePreReInscription)) {
				userInfos.addObject(new EtudiantException("Vous êtes déjà inscrit administrativement, vous ne pouvez plus suivre cette procédure"));
			}
		}

		// 3 cas possibles :
		// Cas No 1 : Deja pre-inscrit (deja passe par la procedure) : recupere depuis pre-etudiant
		// Cas No 2 : Premier passage, en PRE-inscription : recupere depuis pre-candidat
		// Cas No 3 : Premier passage, en PRE-RE-inscription : recupere depuis etudiant existant

		// Cas No 1 : deja pre-inscrit
		//
		// Recuperation dans tous les cas depuis pre-etudiant si existant, sans controle d'autorisation d'acces,
		// puisque ce n'est pas la premiere fois qu'il accede (deja pre-etudiant)...
		if (preCandidat.isPreInscrit()) {
			return initialiserEtudiantDepuisPreEtudiant(ec, preCandidat, oldEtudiant, anneePreInscription, anneePreReInscription, userInfos);
		}

		// C'est son premier passage, distinction pre ou re inscription...

		// Cas No 2 : Premier passage, en PRE-inscription
		//
		if (preCandidat.isPreInscription()) {
			return initialiserEtudiantDepuisPreCandidat(ec, preCandidat, oldEtudiant, anneePreInscription, userInfos);
		}

		// Cas No 3 : Premier passage, en PRE-RE-inscription
		//
		// On verifie qu'il a des inscriptions enregistrees et on recupere depuis etudiant existant...
		EOHistorique myHistoriquePlusRecent = preCandidat.toEtudiant().historiquePlusRecent(anneePreReInscription);
		verifierAutorisationReInscription(ec, anneePreReInscription, userInfos, myHistoriquePlusRecent);
		return initialiserEtudiantDepuisEtudiant(ec, preCandidat, anneePreReInscription, userInfos, myHistoriquePlusRecent);
	}

	// private...

	private static void verifierCandidatConnu(EOEditingContext ec, EOPreCandidat preCandidat) {
		if (preCandidat == null) {
			String message = FinderParametre.getParam(ec, "GARNUCHE_MESSAGE_PRE_INSC_CANDIDAT_INCONNU");
			if (message == null) {
				message = "\nCandidat inconnu, veuillez vérifier vos identifiants.\n\nSi ils sont corrects, il se peut que nous n'ayons pas encore reçu les données PostBac vous concernant. Veuillez dans ce cas essayer ultérieurement.";
			}
			throw new EtudiantException(message);
		}
	}

	private static void verifierAutoriseInscriptionDansEtablissement(EOPreCandidat preCandidat, String cRne) {
		// vérification si le rne de l'instance appelante correspond au rne du pre-candidat, qu'il n'essaye pas de
		// s'inscrire à un autre établissement que ce qu'il était prévu
		if (preCandidat.toRne() == null || (cRne != null && cRne.equals(preCandidat.toRne().cRne()) == false)) {
			throw new EtudiantException("Vous n'êtes pas autorisé(e) à vous pré-inscrire dans cet établissement.");
		}
	}

	private static void verifierInscriptionOuverte(EOPreCandidat preCandidat, Integer anneePreInscription, Integer anneePreReInscription) {
		// vérif si on peut encore se pré-(ré-)inscrire, cad si on est encore dans les dates d'ouverture
		if (preCandidat.isPreInscription() && anneePreInscription == null) {
			throw new EtudiantException("La période de pré-inscription n'est pas ouverte !");
		}
		if (preCandidat.isReInscription() && anneePreReInscription == null) {
			throw new EtudiantException("La période de pré-ré-inscription n'est pas ouverte !");
		}
	}

	private static EOEtudiant initialiserEtudiantDepuisCandidatAdmission(EOEditingContext ec, EOCandidatGrhum candidat) {
		if (candidat == null) {
			return null;
		}
		EOEtudiant etudiantExistant = null;
		if (candidat.etudNumero() != null) {
			etudiantExistant = EOEtudiant.fetchFirstByKeyValue(ec, EOEtudiant.ETUD_NUMERO_KEY, candidat.etudNumero());
		}
		EOEtudiant newEtudiant = FactoryEtudiant.create(ec, candidat, etudiantExistant);
		newEtudiant.setEtudType(EOEtudiant.ETUD_TYPE_ADMISSION);
		newEtudiant.setAnneeInscriptionEnCours(candidat.candAnneeScol());
		newEtudiant.setFormationsEnvisageables(null);
		newEtudiant.setFormationsPossibles(null);
		newEtudiant.setFormationsEnvisagees(null);

		newEtudiant.setUserInfos(new NSMutableArray<EtudiantException>());
		return newEtudiant;
	}

	private static EOEtudiant initialiserEtudiantDepuisPreEtudiant(EOEditingContext ec, EOPreCandidat preCandidat, IEtudiant oldEtudiant,
			Integer anneePreInscription, Integer anneePreReInscription, NSMutableArray<EtudiantException> userInfos) {
		EOEtudiant etudiant = FactoryEtudiant.create(ec, preCandidat.preEtudiant(), (EOEtudiant) oldEtudiant);
		//
		etudiant.setIsPreInscrit(true);
		etudiant.setPreCandidatures(preCandidat.toPreCandidatures());
		etudiant.setRne(preCandidat.toRne());
		if (preCandidat.isPreInscription()) {
			etudiant.setEtudType(EOEtudiant.ETUD_TYPE_PRE_INSCRIPTION);
			etudiant.setAnneeInscriptionEnCours(anneePreInscription);
		}
		else {
			etudiant.setEtudType(EOEtudiant.ETUD_TYPE_PRE_RE_INSCRIPTION);
			etudiant.setAnneeInscriptionEnCours(anneePreReInscription);
			// Ajout des formations en cours (histo N-x = last histo)
			if (preCandidat.toEtudiant() != null) {
				EOHistorique myHistoriquePlusRecent = preCandidat.toEtudiant().historiquePlusRecent(anneePreReInscription);
				if (myHistoriquePlusRecent == null) {
					userInfos.addObject(new EtudiantException("Vous ne pouvez pas utiliser cette procédure (vous n'êtes pas inscrit)"));
				}
				else {
					if (etudiant.historique(myHistoriquePlusRecent.histAnneeScol()) != null) {
						etudiant.removeFromToHistoriquesRelationship(etudiant.historique(myHistoriquePlusRecent.histAnneeScol()));
					}
					NSArray<EOInscDipl> inscDipls = myHistoriquePlusRecent.toInscDipls();
					if (inscDipls == null || inscDipls.count() == 0) {
						userInfos.addObject(new EtudiantException("Il y a un problème pour récupérer vos inscriptions actuelles"));
					}
					etudiant.addToToHistoriquesRelationship(myHistoriquePlusRecent);
				}
			}
		}
		// nettoyage des tableaux de formation qui peuvent eventuellement avoir deja ete initialises et changes
		etudiant.setFormationsEnvisageables(null);
		etudiant.setFormationsPossibles(null);
		etudiant.setFormationsEnvisagees(null);

		etudiant.setUserInfos(userInfos);
		return etudiant;
	}

	private static EOEtudiant initialiserEtudiantDepuisPreCandidat(EOEditingContext ec, EOPreCandidat preCandidat, IEtudiant oldEtudiant,
			Integer anneePreInscription, NSMutableArray<EtudiantException> userInfos) {
		// On verifie qu'il a des voeux d'inscription enregistres...
		NSArray<EOPreCandidature> preCandidatures = preCandidat.toPreCandidatures();
		// FIXME à voir pour paramétrage... on ne bloque plus ceux qui n'ont pas fait de voeux PostBac...
		// if (preCandidatures == null || preCandidatures.count() == 0) {
		// userInfos
		// .addObject(new EtudiantException(
		// "Vous n'avez pas fait de voeux d'inscription PostBac et ne pouvez donc pas suivre cette procédure. Contactez le service de la scolarité."));
		// }
		// Recuperation depuis pre-candidat
		//
		EOEtudiant etudiant = FactoryEtudiant.create(ec, preCandidat, anneePreInscription, (EOEtudiant) oldEtudiant);
		//
		etudiant.setIsPreInscrit(false);
		etudiant.setEtudType(EOEtudiant.ETUD_TYPE_PRE_INSCRIPTION);
		etudiant.setAnneeInscriptionEnCours(anneePreInscription);
		etudiant.setPreCandidatures(preCandidatures);
		etudiant.setRne(preCandidat.toRne());
		// nettoyage des tableaux de formation qui peuvent eventuellement avoir deja ete initialises et changes
		etudiant.setFormationsEnvisageables(null);
		etudiant.setFormationsPossibles(null);
		etudiant.setFormationsEnvisagees(null);

		etudiant.setUserInfos(userInfos);
		return etudiant;
	}

	private static boolean verifierAutorisationReInscription(EOEditingContext ec, Integer anneePreReInscription,
			NSMutableArray<EtudiantException> userInfos, EOHistorique myHistoriquePlusRecent) {
		if (myHistoriquePlusRecent == null) {
			userInfos.addObject(new EtudiantException("Vous ne pouvez pas utiliser cette procédure (vous n'êtes pas inscrit)."));
			return false;
		}
		else {
			// verifie que la duree d'interruption (s'il y a eu interruption) est inferieure ou egale a la duree
			// d'interruption maximale autorisee
			Integer anneeReInscriptionMoinsUn = new Integer(anneePreReInscription.intValue() - 1);
			Integer myInterruptionMax = FinderParametre.getInterruptionMaximale(ec);
			if (anneeReInscriptionMoinsUn - myHistoriquePlusRecent.histAnneeScol().intValue() > myInterruptionMax.intValue()) {
				userInfos.addObject(new EtudiantException("Vous ne pouvez pas utiliser cette procédure (interruption d'étude non conforme)"));
				return false;
			}

			NSArray<EOInscDipl> inscDipls = myHistoriquePlusRecent.toInscDipls();
			if (inscDipls == null || inscDipls.count() == 0) {
				userInfos.addObject(new EtudiantException("Il y a un problème pour récupérer vos inscriptions actuelles."));
				return false;
			}
			else {
				// verification si au moins un autorise a la reinscription
				boolean autorise = false;
				for (int i = 0; i < inscDipls.count(); i++) {
					EOInscDipl inscDipl = inscDipls.objectAtIndex(i);
					if (FinderPreOuvertureDiplome.isOuvertPourCriteres(ec, anneePreReInscription, inscDipl
							.toFwkScolarite_ScolFormationSpecialisation().fspnKey(), inscDipl.idiplAnneeSuivie())) {
						autorise = true;
						break;
					}
				}
				if (autorise == false) {
					userInfos.addObject(new EtudiantException(
							"Vous ne pouvez pas utiliser cette procédure (aucune de vos formations n'est autorisée à la réinscription)"));
					return false;
				}
			}
		}
		return true;
	}

	private static EOEtudiant initialiserEtudiantDepuisEtudiant(EOEditingContext ec, EOPreCandidat preCandidat, Integer anneePreReInscription,
			NSMutableArray<EtudiantException> userInfos, EOHistorique myHistoriquePlusRecent) {
		EOEtudiant etudiant = preCandidat.toEtudiant();
		//
		etudiant.setToTypeHebergementRelationship(preCandidat.toEtudiant().toTypeHebergement());
		if (etudiant.toFwkpers_Individu() != null) {
			etudiant.toFwkpers_Individu().setIndPhoto(null);
			if (etudiant.adresseStable() != null) {
				etudiant.adresseStable().setAdrListeRouge(null);
			}
			if (etudiant.adresseUniversitaire() != null) {
				etudiant.adresseUniversitaire().setAdrListeRouge(null);
			}
		}
		if (etudiant.etudVilleBac() == null && etudiant.toRneCodeBac() != null) {
			etudiant.setEtudVilleBac(etudiant.toRneCodeBac().ville());
		}
		etudiant.setIsPreInscrit(false);
		etudiant.setEtudType(EOEtudiant.ETUD_TYPE_PRE_RE_INSCRIPTION);
		etudiant.setAnneeInscriptionEnCours(anneePreReInscription);
		etudiant.setRne(preCandidat.toRne());
		// nettoyage des tableaux de formation qui peuvent eventuellement avoir deja ete initialises et changes
		etudiant.setFormationsEnvisageables(null);
		etudiant.setFormationsPossibles(null);
		etudiant.setFormationsEnvisagees(null);

		// Creer un nouvel EOHistorique...
		if (etudiant.historique(anneePreReInscription) != null) {
			etudiant.removeFromToHistoriquesRelationship(etudiant.historique(anneePreReInscription));
		}
		etudiant.addToToHistoriquesRelationship(FactoryHistorique.create(ec, myHistoriquePlusRecent, anneePreReInscription));

		// verifier qu'il a au moins une formation envisageable ou possible a choisir...
		// sinon chercher pourquoi pour lui retourner un message explicite.
		NSArray<EOScolFormationHabilitation> formationsEnvisageables = etudiant.formationsEnvisageables();
		NSArray<EOScolFormationHabilitation> formationsPossibles = etudiant.formationsPossibles();
		if (formationsEnvisageables == null || formationsEnvisageables.count() == 0) {
			if (formationsPossibles == null || formationsPossibles.count() == 0) {
				if (etudiant.messageInterditDeConnexion() != null) {
					userInfos.addObject(new EtudiantException(etudiant.messageInterditDeConnexion()));
				}
				else {
					userInfos
							.addObject(new EtudiantException(
									"Vous n'avez aucune formation envisageable possible, vous ne pouvez pas vous réinscrire pour l'instant. Contactez le service de la scolarité."));
				}
			}
		}

		etudiant.setUserInfos(userInfos);
		return etudiant;
	}

}
