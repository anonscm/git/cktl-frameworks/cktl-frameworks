/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.finder;

import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public abstract class Finder {

	public static NSArray tableauTrie(NSArray donnees, NSArray sort) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(donnees, sort);
	}

	public static NSArray fetchArray(EOEditingContext ec, String entityName, NSDictionary bindings, NSArray sortOrderings, boolean refreshObjects) {
		EOQualifier qual = EOQualifier.qualifierToMatchAllValues(bindings);
		EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, sortOrderings, true, true, null);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}

	public static NSArray fetchArray(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings,
			boolean refreshObjects) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
		EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, sortOrderings, true, true, null);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}

	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings,
			boolean refreshObjects) {
		NSArray res = fetchArray(ec, entityName, conditionStr, params, sortOrderings, refreshObjects);
		if ((res == null) || (res.count() == 0)) {
			return null;
		}
		return (EOEnterpriseObject) res.objectAtIndex(0);
	}

	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sortOrderings,
			boolean refreshObjects) {
		NSArray res = fetchArray(entityName, qualifier, sortOrderings, ec, refreshObjects);
		if ((res == null) || (res.count() == 0)) {
			return null;
		}
		return (EOEnterpriseObject) res.objectAtIndex(0);
	}

	public static NSArray fetchArrayWithPrefetching(EOEditingContext ec, String entityName, String conditionStr, NSArray params,
			NSArray sortOrderings, boolean refreshObjects, NSArray relationsToPrefetch) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
		EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, sortOrderings, true, true, null);
		spec.setPrefetchingRelationshipKeyPaths(relationsToPrefetch);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}

	public static void fetchTable(WODisplayGroup laTable, String leNomTable, EOQualifier leQualifier, NSArray leSort, EOEditingContext ec,
			boolean selectionnerPremier, boolean refresh) {
		EOFetchSpecification myFetch;
		NSArray myResult;

		laTable.setObjectArray(new NSArray());

		laTable.setSelectsFirstObjectAfterFetch(selectionnerPremier);
		laTable.setSortOrderings(leSort);

		myFetch = new EOFetchSpecification(leNomTable, leQualifier, null);
		myFetch.setRefreshesRefetchedObjects(refresh);
		myFetch.setUsesDistinct(true);

		myResult = new NSArray(ec.objectsWithFetchSpecification(myFetch));

		if (leSort != null) {
			myResult = new NSArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(myResult, leSort));
		}

		laTable.setObjectArray(myResult);
	}

	public static NSArray fetchArray(String leNomTable, EOQualifier leQualifier, NSArray leSort, EOEditingContext ec, boolean refresh) {
		EOFetchSpecification myFetch;
		NSArray myResult;

		myFetch = new EOFetchSpecification(leNomTable, leQualifier, null);
		myFetch.setRefreshesRefetchedObjects(refresh);
		myFetch.setUsesDistinct(true);

		myResult = new NSArray(ec.objectsWithFetchSpecification(myFetch));
		if (leSort != null) {
			myResult = new NSArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(myResult, leSort));
		}

		return myResult;
	}

	/**
	 * Pour chaque cle du tableau keys, retrouve la valeur dans le dictionnaire et "encadre" (prefixe et postfixe) la valeur par des '*'
	 * pour utiliser avec des caseInsensitiveLike. Retourne toujours un dictionnaire (meme vide), jamais NULL.
	 * 
	 * @param dico
	 *            Le dictionnaire de bindings a modifier
	 * @param keys
	 *            Un tableau des cles dont on doit modifier les valeurs dans le tableau
	 * @return Un NSDictionary semblable au dictionnaire d'entree, avec les valeurs voulues (pre/post)fixes
	 */
	public static NSDictionary<String, String> updatedDictionaryForCaseInsensitiveLike(NSDictionary<String, String> dico, NSArray<String> keys) {
		if (dico == null) {
			return new NSDictionary<String, String>();
		}
		if (dico.allKeys().count() == 0 || keys == null || keys.count() == 0) {
			return dico;
		}
		NSMutableDictionary<String, String> newDico = dico.mutableClone();
		for (int i = 0; i < keys.count(); i++) {
			String key = keys.objectAtIndex(i);
			if (newDico.valueForKey(key) != null) {
				newDico.takeValueForKey("*" + newDico.valueForKey(key) + "*", key);
			}
		}
		return newDico.immutableClone();
	}

	/**
	 * Construit un qualifier pour fetcher les rows correspondant à la date du jour pour le champ donné, pour faire abstraction des
	 * heures/minutes/... dans les valeurs du champ
	 * 
	 * @param key
	 *            Champ de la table
	 * @param dayDate
	 *            Date du jour sur lequel chercher
	 * @return un EOQualifier qui va bien
	 */
	public static EOQualifier qualifierForDayDate(String key, NSTimestamp dayDate) {
		dayDate = new NSTimestamp(dayDate.getTime() - (dayDate.getTime() % 86400000));
		EOQualifier qual1 = new EOKeyValueQualifier(key, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, dayDate);
		dayDate = dayDate.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
		EOQualifier qual2 = new EOKeyValueQualifier(key, EOQualifier.QualifierOperatorLessThan, dayDate);
		return new EOAndQualifier(new NSArray(new Object[] { qual1, qual2 }));
	}
}
