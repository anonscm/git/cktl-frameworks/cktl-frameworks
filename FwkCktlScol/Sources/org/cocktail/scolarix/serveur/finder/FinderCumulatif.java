/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.metier.eos.EOCumulatif;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class FinderCumulatif extends Finder {

	/**
	 * Recherche des types cumulatif par le FetchSpec Recherche<BR>
	 * Bindings pris en compte (*xxx* signifie : caseInsensitiveLike '*xxx*') :<BR>
	 * *cumCode*, *cumLibelle* <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param bindings
	 *            dictionnaire contenant les bindings (si NULL ==> fetchAll)
	 * @return un NSArray contenant des EOCumulatif
	 */
	public static final NSArray getCumulatifs(EOEditingContext ec, NSDictionary bindings) {
		NSArray caseInsensitiveLikeKeys = new NSArray(new String[] { EOCumulatif.CUM_CODE_KEY, EOCumulatif.CUM_LIBELLE_KEY });
		NSDictionary newBindings = updatedDictionaryForCaseInsensitiveLike(bindings, caseInsensitiveLikeKeys);
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, EOCumulatif.ENTITY_NAME, "Recherche", newBindings);
	}

	public static EOCumulatif getCumulatif(EOEditingContext ec, String cumCode, String cumLibelle) {
		EOCumulatif cumulatif = null;
		if (cumCode != null || cumLibelle != null) {
			NSMutableArray qualifiers = new NSMutableArray();
			if (cumCode != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOCumulatif.CUM_CODE_KEY, EOQualifier.QualifierOperatorEqual, cumCode));
			}
			if (cumLibelle != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOCumulatif.CUM_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, cumLibelle));
			}
			cumulatif = EOCumulatif.fetchByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		return cumulatif;
	}

	public static EOCumulatif getCumulatif(EOEditingContext ec, String cumCode) {
		EOCumulatif cumulatif = null;
		if (cumCode != null) {
			EOQualifier qualifier = new EOKeyValueQualifier(EOCumulatif.CUM_CODE_KEY, EOQualifier.QualifierOperatorEqual, cumCode);
			cumulatif = EOCumulatif.fetchByQualifier(ec, qualifier);
		}
		return cumulatif;
	}

	/**
	 * Recherche des types cumulatif par un filtre : where cumCode = filtre or cumLibelle caseInsensitiveLike *filtre*
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOCumulatif, sinon NULL
	 */
	public static NSArray getCumulatifsFiltre(EOEditingContext ec, String filtre) {
		NSArray cumulatifs = null;
		if (!StringCtrl.isEmpty(filtre)) {
			EOSortOrdering codeOrdering = EOSortOrdering.sortOrderingWithKey(EOCumulatif.CUM_LIBELLE_KEY, EOSortOrdering.CompareAscending);
			NSArray sortOrderings = new NSArray(new EOSortOrdering[] { codeOrdering });
			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOCumulatif.CUM_CODE_KEY, EOQualifier.QualifierOperatorEqual, filtre);
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EOCumulatif.CUM_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"
					+ filtre + "*");
			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
			EOOrQualifier qualifier = new EOOrQualifier(qualifiers);
			cumulatifs = EOCumulatif.fetchAll(ec, qualifier, sortOrderings);
		}
		return cumulatifs;
	}
}
