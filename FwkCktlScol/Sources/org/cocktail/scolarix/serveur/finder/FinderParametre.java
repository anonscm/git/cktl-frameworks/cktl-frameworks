/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolParametres;
import org.cocktail.scolarix.serveur.exception.ParametreException;
import org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication;
import org.cocktail.scolarix.serveur.metier.eos.EOGarnucheParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public final class FinderParametre extends Finder {

	/**
	 * Recherche si on est en annee civile ou non.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout
	 * @return boolean true si en annee civile, false si en annee universitaire
	 */
	public static final boolean getAnneeCivile(EOEditingContext ec) {
		String parametre = getParam(ec, EOGarnucheParametres.GARNUCHE_ANNEE_CIVILE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.garnucheAnneeCivileManquant);
		}
		return StringCtrl.toBool(parametre);
	}

	/**
	 * Recherche ce qu'on doit mettre dans l'indicateur Photo par défaut.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout
	 * @return String valeur de l'init a faire sur cet indicateur (contenu de la table, 'O', 'N' ou 'I')
	 */
	public static final String getInitIndPhoto(EOEditingContext ec) {
		String parametre = getParam(ec, EOGarnucheParametres.GARNUCHE_INIT_INDPHOTO);
		if (parametre == null) {
			throw new ParametreException(ParametreException.garnucheInitIndphotoManquant);
		}
		return parametre;
	}

	/**
	 * Recherche si on est utilse un droit universitaire special ou non.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout
	 * @return boolean true si oui, false si non
	 */
	public static final boolean getPaiementSpecial(EOEditingContext ec) {
		String parametre = getParam(ec, EOGarnucheParametres.GARNUCHE_PAIEMENT_SPECIAL);
		if (parametre == null) {
			throw new ParametreException(ParametreException.garnuchePaiementSpecialManquant);
		}
		return StringCtrl.toBool(parametre);
	}

	/**
	 * Recherche l'annee pour les PRE-inscriptions administratives.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param cRne
	 *            Code rne pour lequel on recherche l'annee en cours
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou s'il n'est pas correct
	 * @return l'annee des PRE-inscriptions administratives (Integer)
	 */
	public static final Integer getAnneePreInscriptionAdministrative(EOEditingContext ec, String cRne) {
		Integer annee = null;
		EOGarnucheApplication garnucheApplication = FinderGarnucheApplication.getGarnucheApplication(ec,
				EOGarnucheApplication.APPL_CODE_PREINSCRIPTION, cRne);
		if (garnucheApplication != null) {
			annee = garnucheApplication.anneeEnCours();
		}
		return annee;
	}

	/**
	 * Recherche l'annee pour les PRE-RE-inscriptions administratives.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param cRne
	 *            Code rne pour lequel on recherche l'annee en cours
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou s'il n'est pas correct
	 * @return l'annee des PRE-RE-inscriptions administratives (Integer)
	 */
	public static final Integer getAnneePreReInscriptionAdministrative(EOEditingContext ec, String cRne) {
		Integer annee = null;
		EOGarnucheApplication garnucheApplication = FinderGarnucheApplication.getGarnucheApplication(ec,
				EOGarnucheApplication.APPL_CODE_PREREINSCRIPTION, cRne);
		if (garnucheApplication != null) {
			annee = garnucheApplication.anneeEnCours();
		}
		return annee;
	}

	/**
	 * Recherche l'annee de reference pour les inscriptions administratives.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param cRne
	 *            Code rne pour lequel on recherche l'annee en cours
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou s'il n'est pas correct
	 * @return l'annee de reference des inscriptions administratives (Integer)
	 */
	public static final Integer getAnneeInscriptionAdministrative(EOEditingContext ec, String cRne) {
		Integer annee = null;
		EOGarnucheApplication garnucheApplication = FinderGarnucheApplication.getGarnucheApplication(ec, EOGarnucheApplication.APPL_CODE_INSCRIPTION,
				cRne);
		if (garnucheApplication != null) {
			annee = garnucheApplication.anneeEnCours();
		}
		return annee;
	}

	/**
	 * Recherche La duree maximale d'interruption autorisee pour la re-inscription (en annees).
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou s'il n'est pas correct
	 * @return La duree maximale d'interruption autorisee (Integer)
	 */
	public static final Integer getInterruptionMaximale(EOEditingContext ec) {
		String parametre = getParam(ec, EOGarnucheParametres.GARNUCHE_INTERRUPTION_MAXIMALE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.garnucheInterruptionMaximaleManquant);
		}
		try {
			Integer interruptionMaximale = new Integer(parametre);
			return interruptionMaximale;
		}
		catch (NumberFormatException e) {
			throw new ParametreException(ParametreException.garnucheInterruptionMaximaleIncoherent);
		}
	}

	/**
	 * Recherche d'un parametre par sa cle, toutes tables de parametres confondues<BR>
	 * L'ordre de priorite de la recherche est important.<BR>
	 * Ici GARNUCHE.GARNUCHE_PARAMETRES, SCOLARITE.SCOL_PARAMETRES, GRHUM.GRHUM_PARAMETRES. Si non trouve, retourne null.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un String qui va bien, sinon null
	 * @throws ParametreException
	 *             Si la cle est trouvee plusieurs fois dans une meme table
	 */
	public static final String getParam(EOEditingContext ec, String key) {
		String parametre = null;
		parametre = getGarnucheParam(ec, key);
		if (parametre == null) {
			parametre = getScolParam(ec, key);
			if (parametre == null) {
				parametre = getGrhumParam(ec, key);
			}
		}
		return parametre;
	}

	/**
	 * Recherche d'un parametre OBLIGATOIRE par sa cle, toutes tables de parametres confondues<BR>
	 * L'ordre de priorite de la recherche est important.<BR>
	 * Ici GARNUCHE.GARNUCHE_PARAMETRES, SCOLARITE.SCOL_PARAMETRES, GRHUM.GRHUM_PARAMETRES. Si non trouve, Exception !
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un String qui va bien, sinon une exception si non trouve
	 * @throws ParametreException
	 *             Si la cle est trouvee plusieurs fois dans une meme table ou si aucune cle n'est trouvee
	 * @see org.cocktail.scolarix.serveur.finder.FinderParametre#getParam(EOEditingContext, String)
	 */
	public static final String getRequiredParam(EOEditingContext ec, String key) {
		String parametre = getParam(ec, key);
		if (parametre == null) {
			throw new ParametreException("Il faut definir le parametre " + key);
		}
		return parametre;
	}

	/**
	 * Recherche de parametres par la cle, toutes tables de parametres confondues<BR>
	 * L'ordre de priorite de la recherche est important.<BR>
	 * Ici GARNUCHE.GARNUCHE_PARAMETRES, SCOLARITE.SCOL_PARAMETRES, GRHUM.GRHUM_PARAMETRES.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un tableau de String par ordre de recherche dans les tables
	 */
	public static final NSArray getParams(EOEditingContext ec, String key) {
		NSMutableArray params = new NSMutableArray();
		params.addObjectsFromArray(getGarnucheParams(ec, key));
		params.addObjectsFromArray(getScolParams(ec, key));
		params.addObjectsFromArray(getGrhumParams(ec, key));
		return params;
	}

	/**
	 * Recherche d'un parametre par sa cle dans GarnucheParametres<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans la table
	 * @return un String
	 */
	private static final String getGarnucheParam(EOEditingContext ec, String key) {
		NSArray params = getGarnucheParams(ec, key);
		if (params == null || params.count() == 0) {
			return null;
		}
		if (params.count() > 1) {
			throw new ParametreException("Le parametre " + key + " est defini plusieurs fois dans la table de parametres ("
					+ EOGarnucheParametres.ENTITY_TABLE_NAME + "), il ne devrait y etre qu'une seule fois");
		}
		if (params.lastObject() == null) {
			return null;
		}
		return (String) params.lastObject();
	}

	/**
	 * Recherche la liste des parametres correspondants a une cle dans GarnucheParametres <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un NSArray de String
	 */
	private static final NSArray getGarnucheParams(EOEditingContext ec, String key) {
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOGarnucheParametres.PARAM_KEY_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
				key);
		NSArray params = EOGarnucheParametres.fetchAll(ec, qualifier);
		if (params == null || params.count() == 0) {
			return null;
		}
		return (NSArray) params.valueForKey(EOGarnucheParametres.PARAM_VALUE_KEY);
	}

	/**
	 * Recherche d'un parametre par sa cle dans ScolParametres<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans la table
	 * @return un String
	 */
	private static final String getScolParam(EOEditingContext ec, String key) {
		NSArray params = getScolParams(ec, key);
		if (params == null || params.count() == 0) {
			return null;
		}
		if (params.count() > 1) {
			throw new ParametreException("Le parametre " + key + " est defini plusieurs fois dans la table de parametres ("
					+ EOScolParametres.ENTITY_TABLE_NAME + "), il ne devrait y etre qu'une seule fois");
		}
		return (String) params.lastObject();
	}

	/**
	 * Recherche la liste des parametres correspondants a une cle dans ScolParametres <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un NSArray de String
	 */
	private static final NSArray getScolParams(EOEditingContext ec, String key) {
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOScolParametres.PARAM_KEY_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, key);
		NSArray params = EOScolParametres.fetchAll(ec, qualifier);
		if (params == null || params.count() == 0) {
			return null;
		}
		return (NSArray) params.valueForKey(EOScolParametres.PARAM_VALUE_KEY);
	}

	/**
	 * Recherche d'un parametre par sa cle dans GrhumParametres<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans la table
	 * @return un String
	 */
	private static final String getGrhumParam(EOEditingContext ec, String key) {
		NSArray params = getGrhumParams(ec, key);
		if (params == null || params.count() == 0) {
			return null;
		}
		if (params.count() > 1) {
			throw new ParametreException("Le parametre " + key + " est defini plusieurs fois dans la table de parametres ("
					+ EOGrhumParametres.ENTITY_TABLE_NAME + "), il ne devrait y etre qu'une seule fois");
		}
		return (String) params.lastObject();
	}

	/**
	 * Recherche la liste des parametres correspondants a une cle dans GrhumParametres <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un NSArray de String
	 */
	private static final NSArray getGrhumParams(EOEditingContext ec, String key) {
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOGrhumParametres.PARAM_KEY_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
				key);
		NSArray params = EOGrhumParametres.fetchAll(ec, qualifier);
		if (params == null || params.count() == 0) {
			return null;
		}
		return (NSArray) params.valueForKey(EOGrhumParametres.PARAM_VALUE_KEY);
	}

}
