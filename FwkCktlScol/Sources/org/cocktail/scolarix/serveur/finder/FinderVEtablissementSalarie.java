/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class FinderVEtablissementSalarie extends Finder {

	/**
	 * Recherche des rne (Référentiel) + localisations (Scolarité) par le FetchSpec Recherche<BR>
	 * Bindings pris en compte (*xxx* signifie : caseInsensitiveLike '*xxx*') :<BR>
	 * *cRne*, *llRne* <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param bindings
	 *            dictionnaire contenant les bindings (non NULL)
	 * @return un NSArray contenant des EOVEtablissementSalarie, sinon NULL (si bindings NULL)
	 */
	public static final NSArray getVEtablissementSalaries(EOEditingContext ec, NSDictionary bindings) {
		if (bindings == null) {
			return null;
		}
		NSArray caseInsensitiveLikeKeys = new NSArray(new String[] { EOVEtablissementSalarie.C_RNE_KEY, EOVEtablissementSalarie.LL_RNE_KEY });
		NSDictionary newBindings = updatedDictionaryForCaseInsensitiveLike(bindings, caseInsensitiveLikeKeys);
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, EOVEtablissementSalarie.ENTITY_NAME, "Recherche", newBindings);
	}

	public static EOVEtablissementSalarie getRne(EOEditingContext ec, String cRne, String llRne) {
		EOVEtablissementSalarie rne = null;
		if (cRne != null || llRne != null) {
			NSMutableDictionary bindings = new NSMutableDictionary();
			if (cRne != null) {
				bindings.takeValueForKey(cRne, EOVEtablissementSalarie.C_RNE_KEY);
			}
			if (llRne != null) {
				bindings.takeValueForKey(llRne, EOVEtablissementSalarie.LL_RNE_KEY);
			}
			NSArray rnes = getVEtablissementSalaries(ec, bindings);
			if (rnes != null && rnes.count() == 1) {
				rne = (EOVEtablissementSalarie) rnes.lastObject();
			}
		}
		return rne;
	}

	public static EOVEtablissementSalarie getRne(EOEditingContext ec, String cRne) {
		EOVEtablissementSalarie rne = null;
		if (cRne != null) {
			rne = EOVEtablissementSalarie.fetchByKeyValue(ec, EOVEtablissementSalarie.C_RNE_KEY, cRne);
		}
		return rne;
	}

	/**
	 * Recherche des rne + localistations par un filtre : where cRne caseInsensitiveLike *filtre* or llRne caseInsensitiveLike *filtre* +
	 * caseInsensitiveLike codePostal* + caseInsensitiveLike *ville*
	 * 
	 * @param ec
	 * @param filtre
	 * @param codePostal
	 * @param ville
	 * @return un NSArray contenant des EOVEtablissementSalarie, sinon NULL
	 */
	public static NSArray getVEtablissementSalariesFiltre(EOEditingContext ec, String filtre, String codePostal, String ville) {
		NSArray etablissements = null;
		if (!StringCtrl.isEmpty(filtre)) {
			EOSortOrdering codeOrdering = EOSortOrdering.sortOrderingWithKey(EOVEtablissementSalarie.LL_RNE_KEY, EOSortOrdering.CompareAscending);
			NSArray sortOrderings = new NSArray(new EOSortOrdering[] { codeOrdering });
			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOVEtablissementSalarie.C_RNE_KEY, EOQualifier.QualifierOperatorLike, "*"
					+ filtre + "*");
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EOVEtablissementSalarie.LL_RNE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtre + "*");
			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
			EOQualifier qualifier = new EOOrQualifier(qualifiers);

			if (!StringCtrl.isEmpty(codePostal) && !StringCtrl.isEmpty(ville)) {
				EOKeyValueQualifier codePostalQualifier = new EOKeyValueQualifier(EOVEtablissementSalarie.CODE_POSTAL_KEY,
						EOQualifier.QualifierOperatorCaseInsensitiveLike, codePostal + "*");
				EOKeyValueQualifier villeQualifier = new EOKeyValueQualifier(EOVEtablissementSalarie.VILLE_KEY,
						EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + ville + "*");
				EOOrQualifier qualifier1 = new EOOrQualifier(new NSArray(new EOQualifier[] { villeQualifier, codePostalQualifier }));
				qualifier = new EOAndQualifier(new NSArray(new EOQualifier[] { qualifier, qualifier1 }));
			}
			else
				if (!StringCtrl.isEmpty(codePostal)) {
					EOKeyValueQualifier codePostalQualifier = new EOKeyValueQualifier(EOVEtablissementSalarie.CODE_POSTAL_KEY,
							EOQualifier.QualifierOperatorCaseInsensitiveLike, codePostal + "*");
					qualifier = new EOAndQualifier(new NSArray(new EOQualifier[] { qualifier, codePostalQualifier }));
				}
				else
					if (!StringCtrl.isEmpty(ville)) {
						EOKeyValueQualifier villeQualifier = new EOKeyValueQualifier(EOVEtablissementSalarie.VILLE_KEY,
								EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + ville + "*");
						qualifier = new EOOrQualifier(new NSArray(new EOQualifier[] { qualifier, villeQualifier }));
					}
			etablissements = EOVEtablissementSalarie.fetchAll(ec, qualifier, sortOrderings);
		}
		return etablissements;
	}
}
