/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderTypeInscription extends Finder {

	public static final NSArray<EOTypeInscription> getTypeInscriptionsForInscription(EOEditingContext ec) {
		EOKeyValueQualifier qual = new EOKeyValueQualifier(EOTypeInscription.TEMOIN_INSCRIPTION_KEY, EOQualifier.QualifierOperatorEqual, new Integer(
				1));
		return EOTypeInscription.fetchAll(ec, qual);
	}

	public static final EOTypeInscription getTypeInscription(EOEditingContext ec, Integer idiplTypeInscription) {
		EOKeyValueQualifier qual = new EOKeyValueQualifier(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_KEY, EOQualifier.QualifierOperatorEqual,
				idiplTypeInscription);
		return (EOTypeInscription) fetchObject(ec, EOTypeInscription.ENTITY_NAME, qual, null, true);
	}

	public static final EOTypeInscription getTypeInscriptionPrincipale(EOEditingContext ec) {
		return getTypeInscription(ec, EOTypeInscription.IDIPL_TYPE_INSCRIPTION_PRINCIPALE);
	}

	public static final EOTypeInscription getTypeInscriptionCumulatif(EOEditingContext ec) {
		return getTypeInscription(ec, EOTypeInscription.IDIPL_TYPE_INSCRIPTION_CUMULATIF);
	}

	public static final EOTypeInscription getTypeInscriptionComplementaire(EOEditingContext ec) {
		return getTypeInscription(ec, EOTypeInscription.IDIPL_TYPE_INSCRIPTION_COMPLEMENTAIRE);
	}

}
