/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class FinderDepartement extends Finder {

	/**
	 * Recherche d'un departement sa clé.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param cDepartement
	 * @return Un EODepartement.
	 */
	public static EODepartement getDepartementByKey(EOEditingContext ec, String cDepartement) {
		return EODepartement.fetchByKeyValue(ec, EODepartement.C_DEPARTEMENT_KEY, cDepartement);
	}

	/**
	 * Recherche des departements par un filtre : where cDepartement like *filtre* or llDepartement caseInsensitiveLike *filtre*
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EODepartement, sinon NULL
	 */
	public static NSArray<EODepartement> getDepartementsFiltre(EOEditingContext ec, String filtre) {
		NSArray<EODepartement> departements = null;
		if (!StringCtrl.isEmpty(filtre)) {
			EOSortOrdering ordering = EOSortOrdering.sortOrderingWithKey(EODepartement.LL_DEPARTEMENT_KEY, EOSortOrdering.CompareAscending);
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(new EOSortOrdering[] { ordering });
			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EODepartement.C_DEPARTEMENT_KEY, EOQualifier.QualifierOperatorLike, "*"
					+ filtre + "*");
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EODepartement.LL_DEPARTEMENT_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, filtre + "*");
			NSArray<EOQualifier> qualifiers = new NSArray<EOQualifier>(new EOQualifier[] { codeQualifier, libelleQualifier });
			EOOrQualifier qualifier = new EOOrQualifier(qualifiers);
			departements = EODepartement.fetchAll(ec, qualifier, sortOrderings);
		}
		return departements;
	}

	/**
	 * Tente de determiner le departement a partir du code postal.<BR>
	 * NDLR: pas fiable a 100% comme methode, pour certains codes postaux pourris on n'aura rien et pour la Corse on aura toujours 02A.<BR>
	 * Oui je sais c'est moyen, mais si quelqu'un a une meilleure idée (autre que de faire la liste en dur des codes postaux corses)...
	 * 
	 * @param ec
	 *            EditingContext dans lequel on travaille
	 * @param codePostal
	 *            Le code postal de la commune sur 5 chiffres
	 * @return Un EODepartement, sinon NULL.
	 */
	public static EODepartement getDepartementByCodePostal(EOEditingContext ec, String codePostal) {
		EODepartement departement = null;
		if (StringCtrl.isEmpty(codePostal) || codePostal.trim().length() != 5) {
			return null;
		}
		// code postal etranger
		if (codePostal.startsWith("99")) {
			return null;
		}
		String cDep = null;
		// tom
		if (codePostal.startsWith("98")) {
			cDep = codePostal.substring(0, 3);
		}
		else {
			// dom (sauf mayotte :-) )
			if (codePostal.startsWith("97")) {
				// bidouille reunion
				if (codePostal.startsWith("978") || codePostal.startsWith("977")) {
					cDep = "974";
				}
				else {
					// cas particulier mayotte
					if (codePostal.startsWith("976")) {
						cDep = "985";
					}
					else {
						// cas general dom
						cDep = codePostal.substring(0, 3);
					}
				}
			}
			else {
				// cas particulier Corse: un au pif !
				if (codePostal.startsWith("20")) {
					cDep = "02A";
				}
				else {
					// cas general
					cDep = "0" + codePostal.substring(0, 2);
				}
			}
		}
		if (cDep != null) {
			departement = EODepartement.fetchByKeyValue(ec, EODepartement.C_DEPARTEMENT_KEY, cDep);
		}
		return departement;
	}

}
