/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionTitre;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderScolInscriptionTitre extends Finder {

	/**
	 * Renvoie la liste des ScolFormationTitre pour l'etudiant et pour l'annee.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param historique
	 * @return Un tableau de EOScolFormationTitre qui va bien...
	 */
	public static final NSArray<EOScolInscriptionTitre> getScolFormationTitres(EOEditingContext ec, EOHistorique historique) {
		NSArray<EOScolInscriptionTitre> scolFormationTitres = null;
		if (historique != null) {
			NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
					+ EOScolFormationAnnee.FANN_DEBUT_KEY, EOQualifier.QualifierOperatorEqual, historique.histAnneeScol()));
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.TO_FWK_SCOLARIX__ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual,
					historique.toEtudiant()));
			EOFetchSpecification spec = new EOFetchSpecification(EOScolInscriptionTitre.ENTITY_NAME, new EOAndQualifier(quals), null, true, true,
					null);
			spec.setRefreshesRefetchedObjects(true);
			scolFormationTitres = ec.objectsWithFetchSpecification(spec);
		}
		return scolFormationTitres;
	}

	public static boolean isRecuPourTitre(EOEditingContext ec, Integer etudNumero, Integer fspnKey, Integer idiplAnneeSuivie) {
		boolean recu = false;
		if (etudNumero != null && fspnKey != null && idiplAnneeSuivie != null) {
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.IFTIT_ETAT_KEY, EOQualifier.QualifierOperatorGreaterThan, new Integer(0)));
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.TO_FWK_SCOLARIX__ETUDIANT_KEY + "." + EOEtudiant.ETUD_NUMERO_KEY,
					EOQualifier.QualifierOperatorEqual, etudNumero));
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
					+ EOScolFormationSpecialisation.FSPN_KEY_KEY, EOQualifier.QualifierOperatorEqual, fspnKey));
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.IDIPL_ANNEE_SUIVIE_KEY, EOQualifier.QualifierOperatorEqual,
					idiplAnneeSuivie));
			NSArray scolInscriptionTitres = EOScolInscriptionTitre.fetchAll(ec, new EOAndQualifier(quals));
			if (scolInscriptionTitres != null && scolInscriptionTitres.count() > 0) {
				recu = true;
			}
		}
		return recu;
	}

	public static boolean isRecuPourTitre(EOEditingContext ec, Integer etudNumero, EOScolFormationTitre scolFormationTitre) {
		boolean recu = false;
		if (etudNumero != null && scolFormationTitre != null) {
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.IFTIT_ETAT_KEY, EOQualifier.QualifierOperatorGreaterThan, new Integer(0)));
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.TO_FWK_SCOLARIX__ETUDIANT_KEY + "." + EOEtudiant.ETUD_NUMERO_KEY,
					EOQualifier.QualifierOperatorEqual, etudNumero));
			quals.addObject(new EOKeyValueQualifier(EOScolInscriptionTitre.TO_FWK_SCOLARITE__SCOL_FORMATION_TITRE_KEY,
					EOQualifier.QualifierOperatorEqual, scolFormationTitre));
			NSArray scolInscriptionTitres = EOScolInscriptionTitre.fetchAll(ec, new EOAndQualifier(quals));
			if (scolInscriptionTitres != null && scolInscriptionTitres.count() > 0) {
				recu = true;
			}
		}
		return recu;
	}
}
