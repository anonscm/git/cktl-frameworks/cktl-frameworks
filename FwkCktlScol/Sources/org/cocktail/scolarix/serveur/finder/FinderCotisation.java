/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.scolarix.serveur.metier.eos.EOCotisation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

public class FinderCotisation extends Finder {

	public static NSArray<EOCotisation> motifsNonAffiliation(EOEditingContext ec, Integer anneeScol) {
		return motifs(ec, anneeScol, false);
	}

	public static NSArray<EOCotisation> motifsAffiliation(EOEditingContext ec, Integer anneeScol) {
		return motifs(ec, anneeScol, true);
	}

	private static NSArray<EOCotisation> motifs(EOEditingContext ec, Integer anneeScol, boolean isAffiliation) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(new EOKeyValueQualifier(EOCotisation.COTI_VALIDITE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "O"));
		if (isAffiliation) {
			qualifiers.addObject(new EOKeyValueQualifier(EOCotisation.COTI_CODE_NUMBER_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo,
					new Integer(0)));
		}
		else {
			qualifiers
					.addObject(new EOKeyValueQualifier(EOCotisation.COTI_CODE_NUMBER_KEY, EOQualifier.QualifierOperatorGreaterThan, new Integer(0)));
		}
		if (anneeScol != null) {
			NSMutableArray<EOQualifier> andQuals = new NSMutableArray<EOQualifier>(2);
			andQuals.addObject(new EOKeyValueQualifier(EOCotisation.COTI_DEBUT_VALIDITE_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo,
					anneeScol));

			NSMutableArray<EOQualifier> orQuals = new NSMutableArray<EOQualifier>(2);
			orQuals.addObject(new EOKeyValueQualifier(EOCotisation.COTI_FIN_VALIDITE_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo,
					anneeScol));
			orQuals.addObject(new EOKeyValueQualifier(EOCotisation.COTI_FIN_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual,
					NSKeyValueCoding.NullValue));

			andQuals.addObject(new EOOrQualifier(orQuals));

			qualifiers.addObject(new EOAndQualifier(andQuals));
		}
		return EOCotisation.fetchAll(ec, new EOAndQualifier(qualifiers));
	}

}
