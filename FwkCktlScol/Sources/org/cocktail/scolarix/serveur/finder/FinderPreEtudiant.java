/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOPreIndividu;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FinderPreEtudiant extends Finder {

	/**
	 * Recherche des pre-etudiants par le FetchSpec Recherche<BR>
	 * Bindings pris en compte (*xxx* signifie : caseInsensitiveLike '*xxx*') :<BR>
	 * etudCodeIne, etudNumero
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param bindings
	 *            dictionnaire contenant les bindings (non NULL)
	 * @return un NSArray contenant des EOPreEtudiant, sinon NULL (si bindings NULL)
	 */
	public static final NSArray getPreEtudiants(EOEditingContext ec, NSDictionary bindings) {
		if (bindings == null) {
			return null;
		}
		// NSDictionary newBindings = updatedDictionaryForCaseInsensitiveLike(bindings, new
		// NSArray(EOPreEtudiant.ETUD_VILLE_BAC_KEY));
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, EOPreEtudiant.ENTITY_NAME, "Recherche", bindings);
	}

	/**
	 * Recherche d'un pre-etudiant par son numero INE et sa date de naissance <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param numeroINE
	 *            OBLIGATOIRE : Numero INE de l'etudiant
	 * @param dateDeNaissance
	 *            FACULTATIF : Date de naissance de l'etudiant
	 * @return un EOPreEtudiant, sinon NULL
	 */
	public static final EOPreEtudiant getPreEtudiantIne(EOEditingContext ec, String numeroINE, NSTimestamp dateDeNaissance) {
		if (numeroINE == null) {
			return null;
		}
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOPreEtudiant.ETUD_CODE_INE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, numeroINE));
		if (dateDeNaissance != null) {
			quals.addObject(qualifierForDayDate(EOPreEtudiant.TO_PRE_INDIVIDU_KEY + "." + EOPreIndividu.D_NAISSANCE_KEY, dateDeNaissance));
		}
		return EOPreEtudiant.fetchByQualifier(ec, new EOAndQualifier(quals));
	}

	/**
	 * Recherche de pre-etudiants<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param numeroEtudiant
	 *            FACULTATIF : Numero de l'etudiant
	 * @param numeroIne
	 *            FACULTATIF : Numero INE de l'etudiant
	 * @param nomPatronymique
	 *            FACULTATIF
	 * @param prenom
	 *            FACULTATIF
	 * @param dateDeNaissance
	 *            FACULTATIF : Date de naissance de l'etudiant
	 * @return un NSArray de EOPreEtudiant, vide si rien trouve
	 */
	public static final NSArray getPreEtudiants(EOEditingContext ec, Integer numeroEtudiant, String numeroIne, String nomPatronymique, String prenom,
			NSTimestamp dateDeNaissance, NSArray sortOrderings) {
		NSMutableArray quals = new NSMutableArray();
		if (numeroEtudiant != null) {
			quals.addObject(new EOKeyValueQualifier(EOPreEtudiant.ETUD_NUMERO_KEY, EOQualifier.QualifierOperatorEqual, numeroEtudiant));
		}
		if (numeroIne != null) {
			quals.addObject(new EOKeyValueQualifier(EOPreEtudiant.ETUD_CODE_INE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, numeroIne));
		}
		if (nomPatronymique != null) {
			quals.addObject(new EOKeyValueQualifier(EOPreEtudiant.TO_PRE_INDIVIDU_KEY + "." + EOPreIndividu.NOM_PATRONYMIQUE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + nomPatronymique + "*"));
		}
		if (prenom != null) {
			quals.addObject(new EOKeyValueQualifier(EOPreEtudiant.TO_PRE_INDIVIDU_KEY + "." + EOPreIndividu.PRENOM_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + prenom + "*"));
		}
		if (dateDeNaissance != null) {
			quals.addObject(qualifierForDayDate(EOPreEtudiant.TO_PRE_INDIVIDU_KEY + "." + EOPreIndividu.D_NAISSANCE_KEY, dateDeNaissance));
		}
		if (sortOrderings == null) {
			EOSortOrdering nomPatronymiqueOrdering = EOSortOrdering.sortOrderingWithKey(EOPreEtudiant.TO_PRE_INDIVIDU_KEY + "."
					+ EOPreIndividu.NOM_PATRONYMIQUE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
			EOSortOrdering prenomOrdering = EOSortOrdering.sortOrderingWithKey(EOPreEtudiant.TO_PRE_INDIVIDU_KEY + "." + EOPreIndividu.PRENOM_KEY,
					EOSortOrdering.CompareCaseInsensitiveAscending);
			sortOrderings = new NSArray(new EOSortOrdering[] { nomPatronymiqueOrdering, prenomOrdering });
		}
		return EOPreEtudiant.fetchAll(ec, new EOAndQualifier(quals), sortOrderings);
	}

}
