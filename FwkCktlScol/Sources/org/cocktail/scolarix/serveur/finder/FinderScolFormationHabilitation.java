/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.apache.log4j.Logger;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolarix.serveur.metier.eos.EOPreFormation;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite;
import org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.foundation.ERXProperties;

public class FinderScolFormationHabilitation extends Finder {

	private static final String PRISE_EN_COMPTE_ANNEE_SUIVIE = "scolFormationHabilitation.priseEnCompteAnneeSuivie"; 
	
	private static final Logger LOGGER = Logger.getLogger(FinderScolFormationHabilitation.class);
	
	/**
	 * Renvoie le EOScolFormationHabilitation
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param anneeScol
	 *            OBLIGATOIRE : L'annee universitaire
	 * @param scolFormationSpecialisation
	 *            OBLIGATOIRE : formation
	 * @param idiplAnneeSuivie
	 *            OBLIGATOIRE : niveau
	 * @return Un EOScolFormationHabilitation, sinon NULL
	 */
	public static final EOScolFormationHabilitation getScolFormationHabilitation(EOEditingContext ec, Integer anneeScol,
			EOScolFormationSpecialisation scolFormationSpecialisation, Integer idiplAnneeSuivie) {
		if (anneeScol == null || scolFormationSpecialisation == null || idiplAnneeSuivie == null) {
			return null;
		}
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
				+ EOScolFormationAnnee.FANN_DEBUT_KEY, EOQualifier.QualifierOperatorEqual, anneeScol));
		quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY,
				EOQualifier.QualifierOperatorEqual, scolFormationSpecialisation));
		quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.FHAB_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, idiplAnneeSuivie));
		return EOScolFormationHabilitation.fetchByQualifier(ec, new EOAndQualifier(quals));
	}

	public static final boolean isScolFormationHabilitationOuvert(EOEditingContext ec, Integer anneeScol,
			EOScolFormationSpecialisation scolFormationSpecialisation, Integer idiplAnneeSuivie) {
		EOScolFormationHabilitation scolFormationHabilitation = getScolFormationHabilitation(ec, anneeScol, scolFormationSpecialisation,
				idiplAnneeSuivie);
		if (scolFormationHabilitation != null) {
			return ("O".equals(scolFormationHabilitation.fhabOuvert()));
		}
		return false;
	}

	/**
	 * Retourne la liste des formations accessibles post-bac.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param anneeScol
	 *            OBLIGATOIRE : L'annee universitaire
	 * @param cRne
	 *            OBLIGATOIRE : Le code RNE de l'etablissement pour lequel on recherche
	 * @return Un NSArray de EOScolFormationHabilitation
	 */
	public static final NSArray<EOScolFormationHabilitation> getScolFormationHabilitationPostBacs(EOEditingContext ec, Integer anneeScol, String cRne) {
		NSArray<EOScolFormationHabilitation> formations = null;
		if (anneeScol != null && cRne != null) {
			NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
					+ EOScolFormationAnnee.FANN_DEBUT_KEY, EOQualifier.QualifierOperatorEqual, anneeScol));
			quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_PRE_FORMATIONS_KEY + "." + EOPreFormation.FORM_ETAB_CHOIX_KEY,
					EOQualifier.QualifierOperatorEqual, cRne));
			quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.FHAB_OUVERT_KEY, EOQualifier.QualifierOperatorEqual, "O"));
			if (ERXProperties.booleanForKeyWithDefault(PRISE_EN_COMPTE_ANNEE_SUIVIE, true)) {
				quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.FHAB_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, new Integer(1)));
			} else {
				LOGGER.info("Attention, on ne tient pas compte du niveau d'habilitation (Paramètre 'scolFormationHabilitation.priseEnCompteAnneeSuivie' mis à FALSE");
			}
	
			EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationHabilitation.ENTITY_NAME, new EOAndQualifier(quals), null);
			fetchSpec.setIsDeep(true);
			fetchSpec.setUsesDistinct(true);
			formations = ec.objectsWithFetchSpecification(fetchSpec);
		}
		return formations;
	}

	/**
	 * Retourne la liste de toutes les formations disponibles pour l'annee et l'etablissement.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param anneeScol
	 *            OBLIGATOIRE : L'annee universitaire
	 * @param rne
	 *            FACULTATIF : Le EORne = l'etablissement pour lequel on recherche
	 * @return Un NSArray de EOScolFormationHabilitation
	 */
	public static final NSArray<EOScolFormationHabilitation> getScolFormationHabilitations(EOEditingContext ec, Integer anneeScol, EORne rne) {
		NSArray<EOScolFormationHabilitation> formations = null;
		if (anneeScol != null) {
			NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
					+ EOScolFormationAnnee.FANN_DEBUT_KEY, EOQualifier.QualifierOperatorEqual, anneeScol));
			if (rne != null) {
				NSMutableArray<EOQualifier> orQuals = new NSMutableArray<EOQualifier>(2);
				orQuals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
						+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
						+ EOScolFormationDiplome.TO_FWK_SCOLARIX_V_ETABLISSEMENT_SCOLARITE_KEY + "." + EOVEtablissementScolarite.TO_RNE_KEY,
						EOQualifier.QualifierOperatorEqual, rne));
				orQuals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
						+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
						+ EOScolFormationDiplome.TO_FWK_SCOLARIX_V_COMPOSANTE_SCOLARITE_KEY + "." + EOVComposanteScolarite.TO_RNE_KEY,
						EOQualifier.QualifierOperatorEqual, rne));
				quals.addObject(new EOOrQualifier(orQuals));
			}
			quals.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.FHAB_OUVERT_KEY, EOQualifier.QualifierOperatorEqual, "O"));
			EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationHabilitation.ENTITY_NAME, new EOAndQualifier(quals), null);
			fetchSpec.setIsDeep(true);
			fetchSpec.setUsesDistinct(true);
			formations = ec.objectsWithFetchSpecification(fetchSpec);
		}
		return formations;
	}

}
