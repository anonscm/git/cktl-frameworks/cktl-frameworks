/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class FinderCommune extends Finder {

	/**
	 * Recherche des communes par le FetchSpec Recherche<BR>
	 * Bindings pris en compte (*xxx* signifie : caseInsensitiveLike '*xxx*') :<BR>
	 * *codePostal*, *libelleCourt*, *libelleLong* <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param bindings
	 *            dictionnaire contenant les bindings (non NULL)
	 * @return un NSArray contenant des EOCommune, sinon NULL (si bindings NULL)
	 */
	public static final NSArray getCommunes(EOEditingContext ec, NSDictionary bindings) {
		if (bindings == null) {
			return null;
		}
		NSArray caseInsensitiveLikeKeys = new NSArray(new String[] { EOCommune.C_POSTAL_KEY, EOCommune.LC_COM_KEY, EOCommune.LL_COM_KEY });
		NSDictionary newBindings = updatedDictionaryForCaseInsensitiveLike(bindings, caseInsensitiveLikeKeys);
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, EOCommune.ENTITY_NAME, "Recherche", newBindings);
	}

	public static EOCommune getCommune(EOEditingContext ec, String codePostal, String ville) {
		EOCommune commune = null;
		if (codePostal != null || ville != null) {
			NSMutableDictionary bindings = new NSMutableDictionary();
			if (codePostal != null) {
				bindings.takeValueForKey(codePostal, EOCommune.C_POSTAL_KEY);
			}
			if (ville != null) {
				bindings.takeValueForKey(ville, EOCommune.LC_COM_KEY);
			}
			NSArray communes = getCommunes(ec, bindings);
			if (communes != null && communes.count() == 1) {
				commune = (EOCommune) communes.lastObject();
			}
		}
		return commune;
	}

	/**
	 * Recherche des communes exactement par le code postal et la ville (operator equal, caseInsensitive pour la ville)
	 * 
	 * @param ec
	 * @param codePostal
	 * @param ville
	 * @return Un NSArray des EOCommune
	 */
	public static NSArray<EOCommune> getCommunesExact(EOEditingContext ec, String codePostal, String ville) {
		NSArray<EOCommune> communes = null;
		
		if (codePostal != null && ville != null) {
			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOCommune.C_POSTAL_KEY, EOQualifier.QualifierOperatorEqual, codePostal);
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EOCommune.LL_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
					ville);
			NSArray<EOQualifier> qualifiers = new NSArray<EOQualifier>(new EOQualifier[] { codeQualifier, libelleQualifier });
			communes = EOCommune.fetchAll(ec, new EOAndQualifier(qualifiers));
			if (communes == null || communes.size() == 0) {
				EOKeyValueQualifier libelleQualifierCourt = new EOKeyValueQualifier(EOCommune.LC_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
						ville);
				NSArray<EOQualifier> qualifiersC = new NSArray<EOQualifier>(new EOQualifier[] { codeQualifier, libelleQualifierCourt });
				communes = EOCommune.fetchAll(ec, new EOAndQualifier(qualifiersC));
			}
		}
		
		return communes;
	}

	/**
	 * Recherche des communes par le code postal et la ville, avec un nom de ville rapprochant.<BR>
	 * Les noms de ville avec ST ou STE sont transformes en SAINT et SAINTE, les espaces multiples sont ramenes a un seul espace, et les
	 * tirets ("-"), les apostrophes ("'") et les espaces sont ignores (like ?).
	 * 
	 * @param ec
	 * @param codePostal
	 * @param ville
	 * @return Un NSArray des EOCommune
	 */
	public static NSArray<EOCommune> getCommunesLike(EOEditingContext ec, String codePostal, String ville) {
		NSArray<EOCommune> communes = null;
		if (codePostal != null && ville != null) {
			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOCommune.C_POSTAL_KEY, EOQualifier.QualifierOperatorEqual, codePostal);

			EOKeyValueQualifier libelleQualifierExact = new EOKeyValueQualifier(EOCommune.LC_COM_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, ville);

			ville = StringCtrl.chaineSansAccents(ville).trim().toUpperCase().replaceAll("[ \t\n\f\r]+", " ");
			ville = StringCtrl.replace(ville, "ST ", "SAINT ");
			ville = StringCtrl.replace(ville, "ST-", "SAINT ");
			ville = StringCtrl.replace(ville, "STE ", "SAINTE ");
			ville = StringCtrl.replace(ville, "STE-", "SAINTE ");
			ville = StringCtrl.replace(ville, "-", "?");
			ville = StringCtrl.replace(ville, "'", "?");
			ville = StringCtrl.replace(ville, " ", "?");

			EOKeyValueQualifier libelleQualifierLike = new EOKeyValueQualifier(EOCommune.LC_COM_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, ville);

			EOQualifier libelleQualifier = new EOOrQualifier(new NSArray<EOQualifier>(
					new EOQualifier[] { libelleQualifierExact, libelleQualifierLike }));

			NSArray<EOQualifier> qualifiers = new NSArray<EOQualifier>(new EOQualifier[] { codeQualifier, libelleQualifier });
			communes = EOCommune.fetchAll(ec, new EOAndQualifier(qualifiers));
		}
		return communes;
	}

	/**
	 * Recherche des communes par le code postal et la ville, avec un nom de ville rapprochant.<BR>
	 * Les noms de ville avec SAINT ou SAINTE sont transformes en ST et STE, les espaces multiples sont ramenes a un seul espace, et les
	 * tirets ("-"), les apostrophes ("'") et les espaces sont ignores (like ?).
	 * 
	 * @param ec
	 * @param codePostal
	 * @param ville
	 * @return Un NSArray des EOCommune
	 */
	public static NSArray getCommunesLikeLibelleLong(EOEditingContext ec, String codePostal, String ville) {
		NSArray communes = null;
		if (codePostal != null && ville != null) {
			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOCommune.C_POSTAL_KEY, EOQualifier.QualifierOperatorEqual, codePostal);

			EOKeyValueQualifier libelleQualifierExact = new EOKeyValueQualifier(EOCommune.LL_COM_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, ville);

			ville = ville.trim().toUpperCase().replaceAll("[ \t\n\f\r]+", " ");
			ville = StringCtrl.replace(ville, "SAINT ", "ST ");
			ville = StringCtrl.replace(ville, "SAINT-", "ST ");
			ville = StringCtrl.replace(ville, "SAINTE ", "STE ");
			ville = StringCtrl.replace(ville, "SAINTE-", "STE ");
			ville = StringCtrl.replace(ville, "-", "?");
			ville = StringCtrl.replace(ville, "'", "?");
			ville = StringCtrl.replace(ville, " ", "?");

			EOKeyValueQualifier libelleQualifierLike = new EOKeyValueQualifier(EOCommune.LL_COM_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, ville);

			EOQualifier libelleQualifier = new EOOrQualifier(new NSArray(new EOQualifier[] { libelleQualifierExact, libelleQualifierLike }));

			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
			communes = EOCommune.fetchAll(ec, new EOAndQualifier(qualifiers));
		}
		return communes;
	}

	/**
	 * Recherche des communes par un filtre : where codePostal like *filtre* or libelleLong caseInsensitiveLike *filtre*
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOCommune, sinon NULL
	 */
	public static NSArray getCommunesFiltre(EOEditingContext ec, String filtre) {
		NSArray communes = null;
		if (!StringCtrl.isEmpty(filtre)) {
			EOSortOrdering ordering = EOSortOrdering.sortOrderingWithKey(EOCommune.LL_COM_KEY, EOSortOrdering.CompareAscending);
			NSArray sortOrderings = new NSArray(new EOSortOrdering[] { ordering });
			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOCommune.C_POSTAL_KEY, EOQualifier.QualifierOperatorLike, filtre + "*");
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EOCommune.LL_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
					"*" + filtre + "*");
			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
			communes = EOCommune.fetchAll(ec, new EOOrQualifier(qualifiers), sortOrderings);
		}
		return communes;
	}
//	public static NSArray getCommunesFiltre(EOEditingContext ec, String filtre) {
//		NSArray communes = null;
//		if (!StringCtrl.isEmpty(filtre)) {
//			EOSortOrdering ordering = EOSortOrdering.sortOrderingWithKey(EOCommune.LC_COM_KEY, EOSortOrdering.CompareAscending);
//			NSArray sortOrderings = new NSArray(new EOSortOrdering[] { ordering });
//			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOCommune.C_POSTAL_KEY, EOQualifier.QualifierOperatorLike, filtre + "*");
//			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EOCommune.LC_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
//					"*" + filtre + "*");
//			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
//			communes = EOCommune.fetchAll(ec, new EOOrQualifier(qualifiers), sortOrderings);
//		}
//		return communes;
//	}
}
