/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import java.util.Enumeration;

import org.cocktail.scolarix.serveur.metier.eos.EOPreCandidat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class FinderPreCandidat extends Finder {

	/**
	 * Recherche d'un pre-candidat par son numero INE et sa date de naissance <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param numeroINE
	 *            OBLIGATOIRE : Numero INE du candidat
	 * @param dateDeNaissance
	 *            FACULTATIF : Date de naissance du candidat
	 * @return un EOPreCandidat, sinon NULL
	 */
	public static EOPreCandidat getPreCandidatIne(EOEditingContext ec, String numeroINE, NSTimestamp dateDeNaissance) {
		if (numeroINE == null) {
			return null;
		}
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.addObject(ERXQ.likeInsensitive(EOPreCandidat.CAND_BEA_KEY, numeroINE));
		if (dateDeNaissance != null) {
			quals.addObject(qualifierForDayDate(EOPreCandidat.CAND_DATE_NAIS_KEY, dateDeNaissance));
		}
		return EOPreCandidat.fetchByQualifier(ec, new EOAndQualifier(quals));
	}

	/**
	 * Recherche d'un pre-candidat par son numero etudiant et sa date de naissance <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param numeroEtudiant
	 *            OBLIGATOIRE : Numero etudiant du candidat
	 * @param dateDeNaissance
	 *            FACULTATIF : Date de naissance du candidat
	 * @return un EOPreCandidat, sinon NULL
	 */
	public static EOPreCandidat getPreCandidatNumero(EOEditingContext ec, Integer numeroEtudiant, NSTimestamp dateDeNaissance) {
		if (numeroEtudiant == null || dateDeNaissance == null) {
			return null;
		}
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOPreCandidat.ETUD_NUMERO_KEY, EOQualifier.QualifierOperatorEqual, numeroEtudiant));
		if (dateDeNaissance != null) {
			quals.addObject(qualifierForDayDate(EOPreCandidat.CAND_DATE_NAIS_KEY, dateDeNaissance));
		}
		return EOPreCandidat.fetchByQualifier(ec, new EOAndQualifier(quals));
	}

	/**
	 * Recherche d'un pre-candidat par son numero PostBac et sa date de naissance <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param numeroPostBac
	 *            OBLIGATOIRE : Numero PostBac du candidat
	 * @param dateDeNaissance
	 *            OBLIGATOIRE : Date de naissance du candidat
	 * @return un EOPreCandidat, sinon NULL
	 */
	public static final EOPreCandidat getPreCandidatPostBac(EOEditingContext ec, String numeroPostBac, NSTimestamp dateDeNaissance) {
		if (numeroPostBac == null || dateDeNaissance == null) {
			return null;
		}
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOPreCandidat.CAND_NUMERO_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(numeroPostBac)));
		if (dateDeNaissance != null) {
			quals.addObject(qualifierForDayDate(EOPreCandidat.CAND_DATE_NAIS_KEY, dateDeNaissance));
		}
		return EOPreCandidat.fetchByQualifier(ec, new EOAndQualifier(quals));
	}

	/**
	 * Recherche des precandidats a partir d'un dictionnaire de bindings <BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE : editingContext dans lequel se fait le fetch
	 * @param bindings
	 *            OBLIGATOIRE : Dictionnaire de bindings
	 * @param sortOrderings
	 *            Tableaux des attributs pour realiser le tri
	 * @return un EOPreCandidat, sinon NULL
	 */
	public static NSArray getPreCandidats(EOEditingContext ec, NSMutableDictionary bindings, NSArray sortOrderings) {
		NSArray preCandidats = null;
		EOAndQualifier qualPreCandidats = null;
		NSMutableArray qualifiers = new NSMutableArray();
		Enumeration<String> enumBindings = bindings.keyEnumerator();
		while (enumBindings.hasMoreElements()) {
			String key = enumBindings.nextElement();
			EOKeyValueQualifier qualifier = null;
			if (key.equals(EOPreCandidat.CAND_NOM_KEY) || key.equals(EOPreCandidat.CAND_PRENOM_KEY)) {
				qualifier = new EOKeyValueQualifier(key, EOQualifier.QualifierOperatorCaseInsensitiveLike, bindings.valueForKey(key) + "*");
			}
			else {
				qualifier = new EOKeyValueQualifier(key, EOQualifier.QualifierOperatorEqual, bindings.valueForKey(key));
			}
			qualifiers.addObject(qualifier);
		}
		qualPreCandidats = new EOAndQualifier(qualifiers);
		// preCandidats = fetchArray(EOPreCandidat.ENTITY_NAME, qualPreCandidats, sortOrderings, ec, true);
		preCandidats = EOPreCandidat.fetchAll(ec, qualPreCandidats, sortOrderings);
		return preCandidats;
	}

}
