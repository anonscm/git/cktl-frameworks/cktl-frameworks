/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.metier.eos.EORne;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FinderRne extends Finder {

	public static EORne getRne(EOEditingContext ec, String cRne) {
		EORne rne = null;
		if (cRne != null) {
			rne = EORne.fetchByKeyValue(ec, EORne.C_RNE_KEY, cRne);
		}
		return rne;
	}

	public static EORne getRneLocal(EOEditingContext ec) {
		String cRne = FinderParametre.getParam(ec, "GRHUM_DEFAULT_RNE");
		return getRne(ec, cRne);
	}

	/**
	 * Recherche des rne par un filtre : where cRne caseInsensitiveLike *filtre* or llRne caseInsensitiveLike *filtre* + caseInsensitiveLike
	 * codePostal* + caseInsensitiveLike *ville* + annee(dateFinDeValidite) = null ou >=annee
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EORne, sinon NULL
	 */
	public static NSArray getRnesFiltre(EOEditingContext ec, String filtre, String codePostal, String ville, Integer annee) {
		NSArray etablissements = null;
		if (!StringCtrl.isEmpty(filtre)) {
			EOSortOrdering codeOrdering = EOSortOrdering.sortOrderingWithKey(EORne.LL_RNE_KEY, EOSortOrdering.CompareAscending);
			NSArray sortOrderings = new NSArray(new EOSortOrdering[] { codeOrdering });
			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EORne.C_RNE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"
					+ filtre + "*");
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EORne.LL_RNE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"
					+ filtre + "*");
			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
			EOQualifier qualifier = new EOOrQualifier(qualifiers);

			if (!StringCtrl.isEmpty(codePostal) && !StringCtrl.isEmpty(ville)) {
				EOKeyValueQualifier codePostalQualifier = new EOKeyValueQualifier(EORne.CODE_POSTAL_KEY,
						EOQualifier.QualifierOperatorCaseInsensitiveLike, codePostal + "*");
				EOKeyValueQualifier villeQualifier = new EOKeyValueQualifier(EORne.VILLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"
						+ ville + "*");
				EOOrQualifier qualifier1 = new EOOrQualifier(new NSArray(new EOQualifier[] { villeQualifier, codePostalQualifier }));
				qualifier = new EOAndQualifier(new NSArray(new EOQualifier[] { qualifier, qualifier1 }));
			}
			else
				if (!StringCtrl.isEmpty(codePostal)) {
					EOKeyValueQualifier codePostalQualifier = new EOKeyValueQualifier(EORne.CODE_POSTAL_KEY,
							EOQualifier.QualifierOperatorCaseInsensitiveLike, codePostal + "*");
					qualifier = new EOAndQualifier(new NSArray(new EOQualifier[] { qualifier, codePostalQualifier }));
				}
				else
					if (!StringCtrl.isEmpty(ville)) {
						EOKeyValueQualifier villeQualifier = new EOKeyValueQualifier(EORne.VILLE_KEY,
								EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + ville + "*");
						qualifier = new EOAndQualifier(new NSArray(new EOQualifier[] { qualifier, villeQualifier }));
					}

			if (annee != null) {
				NSTimestamp dateAnnee = DateCtrl.stringToDate(String.valueOf(annee.intValue()), "%Y");
				EOQualifier anneeQualifier = EOQualifier.qualifierWithQualifierFormat(EORne.D_FIN_VAL_KEY + "=nil OR " + EORne.D_FIN_VAL_KEY + "<%@",
						new NSArray(dateAnnee));
				qualifier = new EOAndQualifier(new NSArray(new EOQualifier[] { qualifier, anneeQualifier }));
			}
			etablissements = EORne.fetchAll(ec, qualifier, sortOrderings);
		}
		return etablissements;
	}
}
