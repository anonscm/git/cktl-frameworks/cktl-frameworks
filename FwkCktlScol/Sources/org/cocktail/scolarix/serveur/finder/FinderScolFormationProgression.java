/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationProgression;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderScolFormationProgression extends Finder {

	/**
	 * Retourne la liste de tous les "passages", selon le fproType passe (progression ou redoublement).
	 * 
	 * @param ec
	 * @param fannKey
	 *            OBLIGATOIRE
	 * @param fspnKey
	 *            OBLIGATOIRE
	 * @param fhabNiveau
	 *            OBLIGATOIRE
	 * @param fproType
	 *            FACULTATIF : EOScolFormationProgression.FPRO_TYPE_REDOUBLEMENT ou EOScolFormationProgression.FPRO_TYPE_PROGRESSION
	 * @return Un NSArray de EOScolFormationProgression
	 */
	public static NSArray<EOScolFormationProgression> getProgressionsForCriteres(EOEditingContext ec, Integer fannKey, Integer fspnKey,
			Integer fhabNiveau, String fproType) {
		NSArray<EOScolFormationProgression> progressions = null;
		if (fannKey != null && fspnKey != null && fhabNiveau != null) {
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(
					EOScolFormationProgression.FHAB_NIVEAU_KEY, EOSortOrdering.CompareAscending));
			NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
					+ EOScolFormationAnnee.FANN_KEY_KEY, EOQualifier.QualifierOperatorEqual, fannKey));
			quals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
					+ EOScolFormationSpecialisation.FSPN_KEY_KEY, EOQualifier.QualifierOperatorEqual, fspnKey));
			quals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.FHAB_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, fhabNiveau));
			if (fproType != null
					&& (fproType.equals(EOScolFormationProgression.TYPE_REDOUBLEMENT) || fproType.equals(EOScolFormationProgression.TYPE_PROGRESSION))) {
				quals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.FPRO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, fproType));
			}
			progressions = EOScolFormationProgression.fetchAll(ec, new EOAndQualifier(quals), sortOrderings);
		}
		return progressions;
	}

	/**
	 * Retourne la liste inversee de tous les "passages", selon le fproType passe (progression ou redoublement).
	 * 
	 * @param ec
	 * @param fannKey
	 *            OBLIGATOIRE
	 * @param fspnProKey
	 *            OBLIGATOIRE
	 * @param proFhabNiveau
	 *            OBLIGATOIRE
	 * @param fproType
	 *            FACULTATIF : EOScolFormationProgression.FPRO_TYPE_REDOUBLEMENT ou EOScolFormationProgression.FPRO_TYPE_PROGRESSION
	 * @return Un NSArray de EOScolFormationProgression
	 */
	public static NSArray<EOScolFormationProgression> getProgressionsInversesForCriteres(EOEditingContext ec, Integer fannKey, Integer fspnProKey,
			Integer proFhabNiveau, String fproType) {
		NSArray<EOScolFormationProgression> progressions = null;
		if (fannKey != null && fspnProKey != null && proFhabNiveau != null) {
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(
					EOScolFormationProgression.FPRO_FHAB_NIVEAU_KEY, EOSortOrdering.CompareAscending));
			NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
					+ EOScolFormationAnnee.FANN_KEY_KEY, EOQualifier.QualifierOperatorEqual, fannKey));
			quals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_PROGRESSION_KEY + "."
					+ EOScolFormationSpecialisation.FSPN_KEY_KEY, EOQualifier.QualifierOperatorEqual, fspnProKey));
			quals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.FPRO_FHAB_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual,
					proFhabNiveau));
			if (fproType != null
					&& (fproType.equals(EOScolFormationProgression.TYPE_REDOUBLEMENT) || fproType.equals(EOScolFormationProgression.TYPE_PROGRESSION))) {
				quals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.FPRO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, fproType));
			}
			progressions = EOScolFormationProgression.fetchAll(ec, new EOAndQualifier(quals), sortOrderings);
		}
		return progressions;
	}

	/**
	 * Retourne la liste de toutes les progressions.
	 * 
	 * @param ec
	 * @param fannKey
	 * @param anArray
	 * @return Un NSArray de EOScolFormationProgression
	 */
	public static NSArray<EOScolFormationProgression> getAllProgressions(EOEditingContext ec, Integer fannKey, NSArray<NSArray<Integer>> anArray,
			String fproType) {
		NSArray<EOScolFormationProgression> progressions = null;
		if (fannKey != null && anArray != null && anArray.count() > 0) {
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(
					EOScolFormationProgression.FHAB_NIVEAU_KEY, EOSortOrdering.CompareAscending));
			NSMutableArray<EOQualifier> orQuals = new NSMutableArray<EOQualifier>(anArray.count());
			for (int i = 0; i < anArray.count(); i++) {
				NSArray<Integer> params = anArray.objectAtIndex(i);
				if (params != null && params.count() == 2) {
					EOQualifier qual1 = new EOKeyValueQualifier(EOScolFormationProgression.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.FSPN_KEY_KEY, EOQualifier.QualifierOperatorEqual, params.objectAtIndex(0));
					EOQualifier qual2 = new EOKeyValueQualifier(EOScolFormationProgression.FHAB_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual,
							params.objectAtIndex(1));
					orQuals.addObject(new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qual1, qual2 })));
				}
			}
			NSMutableArray<EOQualifier> andQuals = new NSMutableArray<EOQualifier>();
			andQuals.addObject(new EOOrQualifier(orQuals));
			andQuals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
					+ EOScolFormationAnnee.FANN_KEY_KEY, EOQualifier.QualifierOperatorEqual, fannKey));
			if (fproType != null
					&& (fproType.equals(EOScolFormationProgression.TYPE_REDOUBLEMENT) || fproType.equals(EOScolFormationProgression.TYPE_PROGRESSION))) {
				andQuals.addObject(new EOKeyValueQualifier(EOScolFormationProgression.FPRO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, fproType));
			}
			progressions = EOScolFormationProgression.fetchAll(ec, new EOAndQualifier(andQuals), sortOrderings);
		}
		return progressions;
	}
}
