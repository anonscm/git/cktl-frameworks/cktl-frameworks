/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.factory;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolarix.serveur.finder.FinderCumulatif;
import org.cocktail.scolarix.serveur.metier.eos.EOCumulatif;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOPreInscription;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryInscDipl extends Factory {

	public static EOInscDipl create(EOEditingContext ec) {
		EOInscDipl newInscDipl = (EOInscDipl) Factory.instanceForEntity(ec, EOInscDipl.ENTITY_NAME);
		ec.insertObject(newInscDipl);
		newInscDipl.setIdiplDateInsc(new NSTimestamp());
		newInscDipl.setMstaCode(new Integer(0));
		newInscDipl.setToCumulatifRelationship(FinderCumulatif.getCumulatif(ec, EOCumulatif.CUM_CODE_DEFAUT));
		newInscDipl.setToFwkScolarite_ScolFormationSpecialisationRelationship(null);
		return newInscDipl;
	}

	/**
	 * Cree un nouvel EOInscDipl copie conforme du EOPreInscription.
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param preInscription
	 *            OBLIGATOIRE : le preInscription a recopier (si NULL, retourne un EOInscDipl vide).
	 * @return Un EOInscDipl.
	 */
	public static EOInscDipl create(EOEditingContext ec, EOPreInscription preInscription) {
		EOInscDipl newInscDipl = create(ec);

		if (preInscription != null) {
			// Recopie des valeurs
			newInscDipl.setToCumulatifRelationship(preInscription.toCumulatif());
			newInscDipl.setGrpdNumero(preInscription.grpdNumero());
			newInscDipl.setIdiplAnneeSuivie(preInscription.idiplAnneeSuivie());
			newInscDipl.setIdiplDateDemission(preInscription.idiplDateDemission());
			newInscDipl.setIdiplDateInsc(DateCtrl.now());
			newInscDipl.setIdiplMoyEtu(null);
			newInscDipl.setIdiplNotePonderation(null);
			newInscDipl.setIdiplNumeroDipl(preInscription.idiplNumeroDipl());
			newInscDipl.setIdiplPassageConditionnel(preInscription.idiplPassageConditionnel());
			newInscDipl.setIdiplRang(null);
			newInscDipl.setIdiplSemestre(preInscription.idiplSemestre());
			newInscDipl.setToMentionRelationship(null);
			newInscDipl.setMstaCode(preInscription.mstaCode());
			newInscDipl.setToResultatRelationship(preInscription.toResultat());
			newInscDipl.setToFwkScolarite_ScolFormationSpecialisationRelationship(preInscription.toFwkScolarite_ScolFormationSpecialisation());
			newInscDipl.setToTypeEchangeRelationship(preInscription.toTypeEchange());
			newInscDipl.setToTypeInscriptionRelationship(preInscription.toTypeInscription());
		}

		return newInscDipl;
	}

	/**
	 * Cree un nouvel EOInscDipl avec le type d'inscription et le diplome passe, plus les valeurs obligatoires (NOT NULL) remplies par
	 * defaut
	 * 
	 * @param ec
	 * @param typeInscription
	 * @param scolFormationSpecialisation
	 * @param idiplAnneeSuivie
	 * @return un EOInscDipl
	 */
	public static EOInscDipl createWithDefaultValues(EOEditingContext ec, EOTypeInscription typeInscription,
			EOScolFormationSpecialisation scolFormationSpecialisation, Integer idiplAnneeSuivie) {
		EOInscDipl newInscDipl = create(ec);
		newInscDipl.setPassageConditionnel(Boolean.FALSE);
		newInscDipl.setIdiplAnneeSuivie(idiplAnneeSuivie);
		newInscDipl.setToTypeInscriptionRelationship(typeInscription);
		newInscDipl.setToFwkScolarite_ScolFormationSpecialisationRelationship(scolFormationSpecialisation);
		return newInscDipl;
	}

}
