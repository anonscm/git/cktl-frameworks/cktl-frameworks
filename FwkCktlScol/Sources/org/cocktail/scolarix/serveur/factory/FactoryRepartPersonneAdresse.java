/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.factory;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOPreCandidat;
import org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryRepartPersonneAdresse extends Factory {

	public static EORepartPersonneAdresse create(EOEditingContext ec) {
		EORepartPersonneAdresse newRepartPersonneAdresse = (EORepartPersonneAdresse) Factory.instanceForEntity(ec,
				EORepartPersonneAdresse.ENTITY_NAME);
		ec.insertObject(newRepartPersonneAdresse);
		newRepartPersonneAdresse.setDCreation(new NSTimestamp());
		newRepartPersonneAdresse.setDModification(new NSTimestamp());
		newRepartPersonneAdresse.setRpaValide("O");
		return newRepartPersonneAdresse;
	}

	/**
	 * Cree un nouvel EORepartPersonneAdresse copie conforme du EOPreRepartAdresse.<BR>
	 * La relation Adresse est egalement dupliquee depuis PreAdresse.
	 * 
	 * @param ec
	 * @param preRepartAdresse
	 *            EOPreRepartAdresse a recopier (si NULL, retourne un EORepartPersonneAdresse vide).
	 * @param individu
	 *            l'individu auquel on associe ce repartPersonneAdresse
	 * @return le EORepartPersonneAdresse qui va bien.
	 * @see org.cocktail.scolarix.serveur.factory.FactoryAdresse#create(EOEditingContext,
	 *      org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse)
	 */
	public static EORepartPersonneAdresse create(EOEditingContext ec, EOPreRepartAdresse preRepartAdresse, EOIndividu individu) {
		EORepartPersonneAdresse newRepartPersonneAdresse = create(ec);
		individu.addToToRepartPersonneAdressesRelationship(newRepartPersonneAdresse);
		if (preRepartAdresse != null) {
			newRepartPersonneAdresse.setRpaPrincipal(preRepartAdresse.rpaPrincipal());
			newRepartPersonneAdresse.setRpaValide(preRepartAdresse.rpaValide());
			newRepartPersonneAdresse.setEMail(preRepartAdresse.eMail());
			newRepartPersonneAdresse.setToTypeAdresseRelationship(preRepartAdresse.toFwkpers_TypeAdresse());
			newRepartPersonneAdresse.setToAdresseRelationship(FactoryAdresse.create(ec, preRepartAdresse.toPreAdresse()));
		}
		return newRepartPersonneAdresse;
	}

	/**
	 * Cree un nouvel EORepartPersonneAdresse (avec la relation Adresse) pour l'adresse stable pour ce EOPreCandidat.
	 * 
	 * @param ec
	 * @param preCandidat
	 *            Le EOPreCandidat d'ou on prend les valeurs.
	 * @param individu
	 *            l'individu auquel on associe ce repartPersonneAdresse
	 * @return Un EORepartPersonneAdresse
	 */
	public static EORepartPersonneAdresse createAdresseStable(EOEditingContext ec, EOPreCandidat preCandidat, EOIndividu individu) {
		EORepartPersonneAdresse newRepartPersonneAdresse = create(ec);
		individu.addToToRepartPersonneAdressesRelationship(newRepartPersonneAdresse);
		newRepartPersonneAdresse.setRpaPrincipal("O");
		newRepartPersonneAdresse.setToTypeAdresseRelationship(EOTypeAdresse.fetchByQualifier(ec, EOTypeAdresse.QUAL_TADR_CODE_PAR));

		EOAdresse newAdresse = FactoryAdresse.create(ec);
		if (preCandidat != null) {
			newAdresse.setAdrAdresse1(preCandidat.candAdr1Parent());
			newAdresse.setAdrAdresse2(preCandidat.candAdr2Parent());
			newAdresse.setCodePostal(preCandidat.candCpParent());
			newAdresse.setVille(preCandidat.candVilleParent());
			newAdresse.setToPaysRelationship(preCandidat.toFwkpers_Pays_AdrParent());
			FactoryAdresse.verifierEtCorrigerVille(newAdresse);
		}
		newRepartPersonneAdresse.setToAdresseRelationship(newAdresse);

		return newRepartPersonneAdresse;
	}

	/**
	 * Cree un nouvel EORepartPersonneAdresse (avec la relation Adresse) pour l'adresse universitaire pour ce EOPreCandidat.
	 * 
	 * @param ec
	 * @param preCandidat
	 *            Le EOPreCandidat d'ou on prend les valeurs.
	 * @param individu
	 *            l'individu auquel on associe ce repartPersonneAdresse
	 * @return Un EORepartPersonneAdresse
	 */
	public static EORepartPersonneAdresse createAdresseUniversitaire(EOEditingContext ec, EOPreCandidat preCandidat, EOIndividu individu) {
		EORepartPersonneAdresse newRepartPersonneAdresse = create(ec);
		individu.addToToRepartPersonneAdressesRelationship(newRepartPersonneAdresse);
		newRepartPersonneAdresse.setRpaPrincipal("N");
		newRepartPersonneAdresse.setToTypeAdresseRelationship(EOTypeAdresse.fetchByQualifier(ec, EOTypeAdresse.QUAL_TADR_CODE_ETUD));

		EOAdresse newAdresse = FactoryAdresse.create(ec);
		if (preCandidat != null) {
			newRepartPersonneAdresse.setEMail(preCandidat.candEmailScol());
			newAdresse.setAdrAdresse1(preCandidat.candAdr1Scol());
			newAdresse.setAdrAdresse2(preCandidat.candAdr2Scol());
			newAdresse.setCodePostal(preCandidat.candCpScol());
			newAdresse.setVille(preCandidat.candVilleScol());
			newAdresse.setToPaysRelationship(preCandidat.toFwkpers_Pays_AdrScol());
			FactoryAdresse.verifierEtCorrigerVille(newAdresse);
		}
		newRepartPersonneAdresse.setToAdresseRelationship(newAdresse);

		return newRepartPersonneAdresse;
	}

	/**
	 * Cree un nouvel EORepartPersonneAdresse (avec la relation Adresse) pour l'adresse stable pour ce EOCandidatGrhum.
	 * 
	 * @param ec
	 * @param candidatAdmission
	 *            Le EOCandidatGrhum d'ou on prend les valeurs.
	 * @param individu
	 *            l'individu auquel on associe ce repartPersonneAdresse
	 * @return Un EORepartPersonneAdresse
	 */
	public static EORepartPersonneAdresse createAdresseStable(EOEditingContext ec, EOCandidatGrhum candidatAdmission, EOIndividu individu) {
		EORepartPersonneAdresse newRepartPersonneAdresse = create(ec);
		individu.addToToRepartPersonneAdressesRelationship(newRepartPersonneAdresse);
		newRepartPersonneAdresse.setRpaPrincipal("O");
		newRepartPersonneAdresse.setToTypeAdresseRelationship(EOTypeAdresse.fetchByQualifier(ec, EOTypeAdresse.QUAL_TADR_CODE_PAR));

		EOAdresse newAdresse = FactoryAdresse.create(ec);
		if (candidatAdmission != null) {
			newAdresse.setAdrAdresse1(candidatAdmission.candAdr1Parent());
			newAdresse.setAdrAdresse2(candidatAdmission.candAdr2Parent());
			newAdresse.setCodePostal(candidatAdmission.candCpParent());
			newAdresse.setVille(candidatAdmission.candVilleParent());
			newAdresse.setToPaysRelationship(candidatAdmission.toFwkpers_Pays_AdrParent());
			FactoryAdresse.verifierEtCorrigerVille(newAdresse);
		}
		newRepartPersonneAdresse.setToAdresseRelationship(newAdresse);

		return newRepartPersonneAdresse;
	}

	/**
	 * Cree un nouvel EORepartPersonneAdresse (avec la relation Adresse) pour l'adresse universitaire pour ce EOCandidatGrhum.
	 * 
	 * @param ec
	 * @param candidatAdmission
	 *            Le EOCandidatGrhum d'ou on prend les valeurs.
	 * @param individu
	 *            l'individu auquel on associe ce repartPersonneAdresse
	 * @return Un EORepartPersonneAdresse
	 */
	public static EORepartPersonneAdresse createAdresseUniversitaire(EOEditingContext ec, EOCandidatGrhum candidatAdmission, EOIndividu individu) {
		EORepartPersonneAdresse newRepartPersonneAdresse = create(ec);
		individu.addToToRepartPersonneAdressesRelationship(newRepartPersonneAdresse);
		newRepartPersonneAdresse.setRpaPrincipal("N");
		newRepartPersonneAdresse.setToTypeAdresseRelationship(EOTypeAdresse.fetchByQualifier(ec, EOTypeAdresse.QUAL_TADR_CODE_ETUD));

		EOAdresse newAdresse = FactoryAdresse.create(ec);
		if (candidatAdmission != null) {
			newRepartPersonneAdresse.setEMail(candidatAdmission.candEmailScol());
			newAdresse.setAdrAdresse1(candidatAdmission.candAdr1Scol());
			newAdresse.setAdrAdresse2(candidatAdmission.candAdr2Scol());
			newAdresse.setCodePostal(candidatAdmission.candCpScol());
			newAdresse.setVille(candidatAdmission.candVilleScol());
			newAdresse.setToPaysRelationship(candidatAdmission.toFwkpers_Pays_AdrScol());
			FactoryAdresse.verifierEtCorrigerVille(newAdresse);
		}
		newRepartPersonneAdresse.setToAdresseRelationship(newAdresse);

		return newRepartPersonneAdresse;
	}

}
