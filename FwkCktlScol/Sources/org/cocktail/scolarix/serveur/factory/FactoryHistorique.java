/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.factory;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionTitre;
import org.cocktail.scolarix.serveur.finder.FinderParametre;
import org.cocktail.scolarix.serveur.finder.FinderRne;
import org.cocktail.scolarix.serveur.finder.FinderScolInscriptionTitre;
import org.cocktail.scolarix.serveur.finder.FinderSituationScol;
import org.cocktail.scolarix.serveur.finder.FinderTypeDiplome;
import org.cocktail.scolarix.serveur.finder.FinderTypeDiplomeSise;
import org.cocktail.scolarix.serveur.finder.FinderTypeHandicap;
import org.cocktail.scolarix.serveur.finder.FinderTypeInscription;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOPreCandidat;
import org.cocktail.scolarix.serveur.metier.eos.EOPreHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOPreInscription;
import org.cocktail.scolarix.serveur.metier.eos.EORne;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplomeSise;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeHandicap;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeRegimeSise;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryHistorique extends Factory {

	private static final String TETAB_CODE_EFE = "EFE";

	public static EOHistorique create(EOEditingContext ec) {
		EOHistorique newHistorique = (EOHistorique) Factory.instanceForEntity(ec, EOHistorique.ENTITY_NAME);
		ec.insertObject(newHistorique);
		return newHistorique;
	}

	public static EOHistorique createWithDefaultValues(EOEditingContext ec, Integer anneeScol) {
		EOHistorique newHistorique = create(ec);
		newHistorique.setHistAnneeScol(anneeScol);
		newHistorique.setHistSalarie(EOHistorique.SALARIE_NON);
		newHistorique.setToVEtablissementSalarieRelationship(null);
		newHistorique.setHistSalarieLibelle(null);

		// Utile en Nouvelle-Caledonie ? newHistorique.setHistNbInscDeug(new Integer(0));

		newHistorique.setToTypeDiplomeRelationship(null);
		newHistorique.setToSituationScolRelationship(null);

		newHistorique.setHistBourse(EOHistorique.NON_BOURSIER);

		newHistorique.setToQuotiteTravailRelationship(null);
		newHistorique.setHistAffss(new Integer(0));
		newHistorique.setHistAyantDroit(new Integer(0));
		newHistorique.setHistAdhAgme(new Integer(0));
		newHistorique.setHistAccesInfo(new Integer(0));

		newHistorique.setHistForProf(new Integer(0));
		newHistorique.setHistIntEtud(new Integer(0));
		newHistorique.setHistDemEmpl(new Integer(0));
		newHistorique.setToTypeRegimeSiseRelationship(EOTypeRegimeSise.getTypeFormationInitiale(ec));
		newHistorique.setHistTeleens(new Integer(0));
		newHistorique.setToTypeHandicapRelationship(FinderTypeHandicap.getTypeHandicap(ec, EOTypeHandicap.THAN_CODE_AUCUN_HANDICAP));

		newHistorique.setHistResteBu(new Integer(0));

		return newHistorique;
	}

	/**
	 * Cree un nouvel EOHistorique copie conforme du EOPreHistorique.<BR>
	 * Des relations sont egalement creees en cascade (si elles existent dans EOPreHistorique) :
	 * <UL>
	 * <LI>InscDipl (depuis PreInscription)
	 * </UL>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param preHistorique
	 *            OBLIGATOIRE : Le EOPreHistorique a recopier (si NULL, retourne un EOHistorique vide).
	 * @return un EOHistorique.
	 */
	public static EOHistorique create(EOEditingContext ec, EOPreHistorique preHistorique) {
		EOHistorique newHistorique = create(ec);

		if (preHistorique != null) {
			// Recopie des valeurs...
			newHistorique.setToActiviteProfessionnelleRelationship(preHistorique.toActiviteProfessionnelle());
			newHistorique.setCotiCode(preHistorique.cotiCode());
			newHistorique.setDactCode(preHistorique.dactCode());
			newHistorique.setToFwkpers_Departement_AutEtabRelationship(preHistorique.toFwkpers_Departement_AutEtab());
			newHistorique.setToFwkpers_Departement_DerDiplRelationship(preHistorique.toFwkpers_Departement_DerDipl());
			newHistorique.setToFwkpers_Departement_DerEtabRelationship(preHistorique.toFwkpers_Departement_DerEtab());
			newHistorique.setToDptSituationRelationship(preHistorique.toDptSituation());
			newHistorique.setDspeCodeFutur(preHistorique.dspeCodeFutur());
			newHistorique.setEtudNumero(preHistorique.etudNumero());
			newHistorique.setHist1InscDeugDut(preHistorique.hist1inscDeugDut());
			newHistorique.setHistAcadBourse(preHistorique.histAcadBourse());
			newHistorique.setHistAccesInfo(preHistorique.histAccesInfo());
			newHistorique.setHistActivProf(preHistorique.histActivProf());
			newHistorique.setHistAdhAgme(preHistorique.histAdhAgme());
			newHistorique.setHistAffss(preHistorique.histAffss());
			newHistorique.setHistAnneeDerDipl(preHistorique.histAnneeDerDipl());
			newHistorique.setHistAnneeDerEtab(preHistorique.histAnneeDerEtab());
			newHistorique.setHistAnneeDiplFutur(preHistorique.histAnneeDiplFutur());
			newHistorique.setHistAnneeScol(preHistorique.histAnneeScol());
			newHistorique.setHistAssuranceCivile(preHistorique.histAssuranceCivile());
			newHistorique.setHistAssuranceCivileCie(preHistorique.histAssuranceCivileCie());
			newHistorique.setHistAssuranceCivileNum(preHistorique.histAssuranceCivileNum());
			newHistorique.setHistAutreCasExo(preHistorique.histAutreCasExo());
			newHistorique.setHistAyantDroit(preHistorique.histAyantDroit());
			newHistorique.setHistBourse(preHistorique.histBourse());
			newHistorique.setHistDateTransfert(preHistorique.histDateTransfert());
			newHistorique.setHistDemEmpl(preHistorique.histDemEmpl());
			newHistorique.setHistDureeInt(preHistorique.histDureeInt());
			newHistorique.setHistEchBourse(preHistorique.histEchBourse());
			newHistorique.setHistEnsDerEtab(preHistorique.histEnsDerEtab());
			newHistorique.setHistForProf(preHistorique.histForProf());
			newHistorique.setHistIntEtud(preHistorique.histIntEtud());
			newHistorique.setHistLibelleAutEtab(preHistorique.histLibelleAutEtab());
			newHistorique.setHistLibelleDerDipl(preHistorique.histLibelleDerDipl());
			newHistorique.setHistLibelleDerEtab(preHistorique.histLibelleDerEtab());
			newHistorique.setHistLibelleProf(preHistorique.histLibelleProf());
			newHistorique.setHistNbInscDeug(preHistorique.histNbInscDeug());
			newHistorique.setHistNumeroAlloc(preHistorique.histNumeroAlloc());
			newHistorique.setHistRedouble(preHistorique.histRedouble());
			if (preHistorique.histRedouble() != null && preHistorique.histRedouble().intValue() == 0) {
				newHistorique.setHistRedouble(EOHistorique.HIST_REDOUBLE_RIEN);
			}
			newHistorique.setHistRemarques(preHistorique.histRemarques());
			newHistorique.setHistResteBu(preHistorique.histResteBu());
			newHistorique.setHistSalarie(preHistorique.histSalarie());
			newHistorique.setToVEtablissementSalarieRelationship(preHistorique.toVEtablissementSalarie());
			newHistorique.setHistSalarieLibelle(preHistorique.histSalarieLibelle());
			newHistorique.setHistTeleens(preHistorique.histTeleens());
			newHistorique.setHistTransfert(preHistorique.histTransfert());
			newHistorique.setHistTuteur1(preHistorique.histTuteur1());
			newHistorique.setHistTuteur2(preHistorique.histTuteur2());
			newHistorique.setToTypeRegimeSiseRelationship(preHistorique.toTypeRegimeSise());
			newHistorique.setHistVilleAutEtab(preHistorique.histVilleAutEtab());
			newHistorique.setHistVilleDerDipl(preHistorique.histVilleDerDipl());
			newHistorique.setHistVilleDerEtab(preHistorique.histVilleDerEtab());
			newHistorique.setHistTypeRegime(preHistorique.histTypeRegime());
			newHistorique.setToLangueRelationship(preHistorique.toLangue());
			newHistorique.setToMutuelleMutRelationship(preHistorique.toMutuelleMut());
			newHistorique.setToMutuelleOrgaRelationship(preHistorique.toMutuelleOrga());
			newHistorique.setToOrigineRessourcesRelationship(preHistorique.toOrigineRessources());
			newHistorique.setToFwkpers_Pays_DerDiplRelationship(preHistorique.toFwkpers_Pays_DerDipl());
			newHistorique.setToFwkpers_Pays_DerEtabRelationship(preHistorique.toFwkpers_Pays_DerEtab());
			newHistorique.setProCodeEtud(preHistorique.proCodeEtud());
			newHistorique.setToQuotiteTravailRelationship(preHistorique.toQuotiteTravail());
			newHistorique.setToRneAutEtabRelationship(preHistorique.toRneAutEtab());
			newHistorique.setToRneDerDiplRelationship(preHistorique.toRneDerDipl());
			newHistorique.setToRneDerEtabRelationship(preHistorique.toRneDerEtab());
			newHistorique.setToRneTransRelationship(preHistorique.toRneTrans());
			newHistorique.setToSituationScolRelationship(preHistorique.toSituationScol());
			newHistorique.setToTypeDiplomeRelationship(preHistorique.toTypeDiplome());
			newHistorique.setToTypeHandicapRelationship(preHistorique.toTypeHandicap());

			// EOInscDipl(s) depuis PreInscription(s)
			if (preHistorique.toPreInscriptions() != null) {
				for (int i = 0; i < preHistorique.toPreInscriptions().count(); i++) {
					EOPreInscription preInscription = (EOPreInscription) preHistorique.toPreInscriptions().objectAtIndex(i);
					newHistorique.addToToInscDiplsRelationship(FactoryInscDipl.create(ec, preInscription));
				}
			}

		}
		return newHistorique;
	}

	/**
	 * Cree un EOHistorique pour le EOPreCandidat donne.<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param preCandidat
	 *            OBLIGATOIRE Le EOPreCandidat d'ou on prend les valeurs (si NULL, retourne un EOHistorique vide).
	 * @param anneeScol
	 *            OBLIGATOIRE L'annee universitaire pour laquelle on inscrit ce candidat
	 * @return Un EOHistorique.
	 */
	public static EOHistorique create(EOEditingContext ec, EOPreCandidat preCandidat, Integer anneeScol) {
		EOHistorique newHistorique = create(ec);

		if (preCandidat != null) {
			newHistorique.setToActiviteProfessionnelleRelationship(null);
			newHistorique.setCotiCode(null);
			newHistorique.setDactCode(null);
			newHistorique.setToFwkpers_Departement_AutEtabRelationship(null);
			newHistorique.setDspeCodeFutur(null);
			newHistorique.setEtudNumero(preCandidat.etudNumero());
			newHistorique.setToEtudiantRelationship(preCandidat.toEtudiant());
			newHistorique.setHist1InscDeugDut(new Integer(0));
			newHistorique.setHistAcadBourse(null);
			newHistorique.setHistAccesInfo(new Integer(0));
			newHistorique.setHistActivProf(new Integer(0));
			newHistorique.setHistAdhAgme(new Integer(0));
			newHistorique.setHistAffss(new Integer(0));
			newHistorique.setHistAnneeDiplFutur(null);
			newHistorique.setHistAnneeScol(anneeScol);
			newHistorique.setHistAssuranceCivile(new Integer(0));
			newHistorique.setHistAssuranceCivileCie(null);
			newHistorique.setHistAssuranceCivileNum(null);
			newHistorique.setHistAutreCasExo(null);
			newHistorique.setHistAyantDroit(new Integer(0));
			newHistorique.setHistBourse(new Integer(0));
			newHistorique.setHistDateTransfert(null);
			newHistorique.setHistDemEmpl(new Integer(0));
			newHistorique.setHistDureeInt(new Integer(0));
			newHistorique.setHistEchBourse(null);
			newHistorique.setHistForProf(new Integer(0));
			newHistorique.setHistIntEtud(new Integer(0));
			newHistorique.setHistLibelleAutEtab(null);
			newHistorique.setHistLibelleProf(null);
			newHistorique.setHistNbInscDeug(new Integer(0));
			newHistorique.setHistNumeroAlloc(null);
			newHistorique.setHistRedouble(EOHistorique.HIST_REDOUBLE_RIEN);
			newHistorique.setHistRemarques(null);
			newHistorique.setHistResteBu(new Integer(0));
			newHistorique.setHistSalarie("N");
			newHistorique.setToVEtablissementSalarieRelationship(null);
			newHistorique.setHistSalarieLibelle(null);
			newHistorique.setHistTeleens(new Integer(0));
			newHistorique.setHistTransfert(null);
			newHistorique.setHistTuteur1(null);
			newHistorique.setHistTuteur2(null);
			newHistorique.setToTypeRegimeSiseRelationship(EOTypeRegimeSise.getTypeFormationInitiale(ec));
			newHistorique.setHistVilleAutEtab(null);
			newHistorique.setToLangueRelationship(null);
			newHistorique.setToMutuelleMutRelationship(null);
			newHistorique.setToMutuelleOrgaRelationship(null);
			newHistorique.setToOrigineRessourcesRelationship(null);
			newHistorique.setProCodeEtud(null);
			newHistorique.setToQuotiteTravailRelationship(null);
			newHistorique.setToRneAutEtabRelationship(null);
			newHistorique.setToRneTransRelationship(null);
			newHistorique.setToTypeHandicapRelationship(FinderTypeHandicap.getTypeHandicapAucunHandicap(ec));

			if (preCandidat.isPreInscription()) {
				// Situation scol
				if (preCandidat.toFwkpers_Pays_DerEtab() == null || preCandidat.toFwkpers_Pays_DerEtab().isPaysDefaut() == false) {
					if (preCandidat.toRneDerEtab() != null && preCandidat.toRneDerEtab().tetabCode() != null
							&& preCandidat.toRneDerEtab().tetabCode().equalsIgnoreCase(TETAB_CODE_EFE)) {
						newHistorique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(ec, "A"));
					}
					else {
						newHistorique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(ec, "P"));
					}
				}
				else {
					if (FinderParametre.getAnneeCivile(ec) == false) {
						if (preCandidat.candAnbac() != null && (preCandidat.candAnbac().intValue() < anneeScol.intValue())) {
							newHistorique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(ec, "S"));
						}
						else {
							newHistorique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(ec, "A"));
						}
					}
					else {
						if (preCandidat.candAnbac() != null && (preCandidat.candAnbac().intValue() < anneeScol.intValue() - 1)) {
							newHistorique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(ec, "S"));
						}
						else {
							newHistorique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(ec, "A"));
						}
					}
				}
				// Type diplome
				if (preCandidat.toFwkpers_Pays_EtabBac() == null || preCandidat.toFwkpers_Pays_EtabBac().isPaysDefaut() == false) {
					if (preCandidat.toRneCodeBac() != null && preCandidat.toRneCodeBac().tetabCode() != null
							&& preCandidat.toRneCodeBac().tetabCode().equalsIgnoreCase(TETAB_CODE_EFE)) {
						newHistorique.setToTypeDiplomeRelationship(FinderTypeDiplome.getTypeDiplome(ec, "A"));
					}
					else {
						newHistorique.setToTypeDiplomeRelationship(FinderTypeDiplome.getTypeDiplome(ec, "X"));
					}
				}
				else {
					newHistorique.setToTypeDiplomeRelationship(FinderTypeDiplome.getTypeDiplome(ec, "A"));
				}

				// Dernier diplome
				newHistorique.setToFwkpers_Departement_DerDiplRelationship(preCandidat.toFwkpers_Departement_EtabBac());
				newHistorique.setHistAnneeDerDipl(preCandidat.candAnbac());
				if (preCandidat.toBac() != null) {
					newHistorique.setHistLibelleDerDipl(preCandidat.toBac().bacLibelle());
				}
				if (preCandidat.toRneCodeBac() != null) {
					if (preCandidat.toRneCodeBac().ville() != null) {
						newHistorique.setHistVilleDerDipl(preCandidat.toRneCodeBac().ville());
					}
					else {
						newHistorique.setHistVilleDerDipl(preCandidat.candVilleBac());
					}
				}
				else {
					newHistorique.setHistVilleDerDipl(preCandidat.candVilleBac());
				}
				newHistorique.setToFwkpers_Pays_DerDiplRelationship(preCandidat.toFwkpers_Pays_EtabBac());
				newHistorique.setToRneDerDiplRelationship(preCandidat.toRneCodeBac());
				newHistorique.setToDptSituationRelationship(null);

				// Dernier etablissement
				newHistorique.setHistAnneeDerEtab(new Integer(anneeScol.intValue() - 1));
				newHistorique.setHistEnsDerEtab(preCandidat.histEnsDerEtab());
				newHistorique.setHistLibelleDerEtab(preCandidat.histLibelleDerEtab());
				newHistorique.setHistVilleDerEtab(preCandidat.histVilleDerEtab());
				newHistorique.setToFwkpers_Departement_DerEtabRelationship(preCandidat.toFwkpers_Departement_DerEtab());
				newHistorique.setToFwkpers_Pays_DerEtabRelationship(preCandidat.toFwkpers_Pays_DerEtab());
				newHistorique.setToRneDerEtabRelationship(preCandidat.toRneDerEtab());
			}
		}

		return newHistorique;
	}

	/**
	 * Cree un nouvel EOHistorique a partir des donnees du dernier EOHistorique de l'etudiant.<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param lastHistorique
	 *            OBLIGATOIRE : Le EOHistorique a recopier (si NULL, retourne un EOHistorique vide).
	 * @param anneeScol
	 *            OBLIGATOIRE : L'annee universitaire pour laquelle on cree cet historique
	 * @return un EOHistorique.
	 */
	public static EOHistorique create(EOEditingContext ec, EOHistorique lastHistorique, Integer anneeScol) {
		EOHistorique newHistorique = createWithDefaultValues(ec, anneeScol);

		if (lastHistorique != null) {
			// Recopie des valeurs...
			newHistorique.setToActiviteProfessionnelleRelationship(lastHistorique.toActiviteProfessionnelle());
			newHistorique.setCotiCode(lastHistorique.cotiCode());
			newHistorique.setDactCode(lastHistorique.dactCode());
			newHistorique.setToFwkpers_Departement_AutEtabRelationship(lastHistorique.toFwkpers_Departement_AutEtab());
			newHistorique.setToDptSituationRelationship(lastHistorique.toDptSituation());
			newHistorique.setDspeCodeFutur(null);
			newHistorique.setEtudNumero(lastHistorique.etudNumero());
			newHistorique.setToEtudiantRelationship(lastHistorique.toEtudiant());
			newHistorique.setHist1InscDeugDut(new Integer(0));
			newHistorique.setHistAcadBourse(null);
			newHistorique.setHistAccesInfo(new Integer(0));
			newHistorique.setHistActivProf(new Integer(0));
			newHistorique.setHistAdhAgme(new Integer(0));
			newHistorique.setHistAffss(new Integer(0));
			newHistorique.setHistAnneeDiplFutur(null);
			newHistorique.setHistAnneeScol(anneeScol);
			newHistorique.setHistAssuranceCivile(lastHistorique.histAssuranceCivile());
			newHistorique.setHistAssuranceCivileCie(lastHistorique.histAssuranceCivileCie());
			newHistorique.setHistAssuranceCivileNum(lastHistorique.histAssuranceCivileNum());
			newHistorique.setHistAutreCasExo(null);
			newHistorique.setHistAyantDroit(new Integer(0));
			newHistorique.setHistBourse(new Integer(0));
			newHistorique.setHistDateTransfert(null);
			newHistorique.setHistDemEmpl(new Integer(0));
			newHistorique.setHistDureeInt(new Integer(0));
			newHistorique.setHistEchBourse(null);
			newHistorique.setHistForProf(new Integer(0));
			newHistorique.setHistIntEtud(new Integer(0));
			newHistorique.setHistLibelleAutEtab(lastHistorique.histLibelleAutEtab());
			newHistorique.setHistLibelleProf(null);
			newHistorique.setHistNbInscDeug(lastHistorique.histNbInscDeug());
			newHistorique.setHistNumeroAlloc(lastHistorique.histNumeroAlloc());
			newHistorique.setHistRedouble(EOHistorique.HIST_REDOUBLE_RIEN);
			newHistorique.setHistRemarques(null);
			newHistorique.setHistResteBu(lastHistorique.histResteBu());
			newHistorique.setHistSalarie("N");
			newHistorique.setToVEtablissementSalarieRelationship(null);
			newHistorique.setHistSalarieLibelle(null);
			newHistorique.setHistTeleens(new Integer(0));
			newHistorique.setHistTransfert(null);
			newHistorique.setHistTuteur1(lastHistorique.histTuteur1());
			newHistorique.setHistTuteur2(lastHistorique.histTuteur2());
			newHistorique.setToTypeRegimeSiseRelationship(lastHistorique.toTypeRegimeSise());
			newHistorique.setHistVilleAutEtab(lastHistorique.histVilleAutEtab());
			newHistorique.setHistTypeRegime(null);
			newHistorique.setToLangueRelationship(null);
			newHistorique.setToMutuelleMutRelationship(null);
			newHistorique.setToMutuelleOrgaRelationship(null);
			newHistorique.setToOrigineRessourcesRelationship(null);
			newHistorique.setProCodeEtud(null);
			newHistorique.setToQuotiteTravailRelationship(null);
			newHistorique.setToRneAutEtabRelationship(lastHistorique.toRneAutEtab());
			newHistorique.setToRneTransRelationship(null);
			newHistorique.setToTypeClassePreparatoireRelationship(lastHistorique.toTypeClassePreparatoire());
			if (lastHistorique.histAnneeScol().intValue() == anneeScol.intValue() - 1) {
				// determination de la situation scolaire (situCode) en fonction du type etablissement (tetabCode) si
				// possible, sinon situation UNIVERSITE (tetabCode='H') par defaut
				EORne rneLocal = FinderRne.getRneLocal(ec);
				if (rneLocal != null) {
					newHistorique.setToSituationScolRelationship(rneLocal.situationScol());
				}
				if (newHistorique.toSituationScol() == null) {
					newHistorique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(ec, "H"));
				}
			}
			else {
				newHistorique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(ec, "U"));
			}
			if (lastHistorique.toTypeHandicap() != null) {
				newHistorique.setToTypeHandicapRelationship(lastHistorique.toTypeHandicap());
			}
			else {
				newHistorique.setToTypeHandicapRelationship(FinderTypeHandicap.getTypeHandicapAucunHandicap(ec));
			}

			EOInscDipl inscriptionPrincipale = lastHistorique.inscriptionPrincipale();

			NSArray<EOScolInscriptionTitre> titres = FinderScolInscriptionTitre.getScolFormationTitres(ec, lastHistorique);
			if (titres != null && titres.count() > 0) {
				// bravo, il a reussi, il a un nouveau "dernier" diplome
				// recherche du titre reussi le plus haut
				EOScolInscriptionTitre titre = null;
				for (int i = 0; i < titres.count(); i++) {
					titre = titres.objectAtIndex(i);
					if (inscriptionPrincipale.toFwkScolarite_ScolFormationSpecialisation().equals(titre.toFwkScolarite_ScolFormationSpecialisation())
							&& inscriptionPrincipale.idiplAnneeSuivie().intValue() == titre.idiplAnneeSuivie().intValue()) {
						break;
					}
				}
				if (titre == null) {
					titre = titres.objectAtIndex(0);
				}
				String libelle = (titre.toFwkScolarite_ScolFormationTitre().filiereTitre() != null ? titre.toFwkScolarite_ScolFormationTitre()
						.filiereTitre().abreviation() : "?")
						+ " "
						+ (titre.toFwkScolarite_ScolFormationTitre().toFwkScolarite_ScolFormationDiplome().fdipAbreviation() != null ? titre
								.toFwkScolarite_ScolFormationTitre().toFwkScolarite_ScolFormationDiplome().fdipAbreviation() : "?");
				newHistorique.setHistLibelleDerDipl(libelle);
				newHistorique.setToRneDerDiplRelationship(titre.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome()
						.toFwkScolarix_VEtablissementScolarite().toRne());
				if (newHistorique.toRneDerDipl() != null) {
					newHistorique.setToFwkpers_Departement_DerDiplRelationship(newHistorique.toRneDerDipl().departement());
					newHistorique.setHistVilleDerDipl(newHistorique.toRneDerDipl().ville());
					if (newHistorique.toRneDerDipl().toAdresse() != null) {
						newHistorique.setToFwkpers_Pays_DerDiplRelationship(newHistorique.toRneDerDipl().toAdresse().toPays());
					}
				}
				newHistorique.setHistAnneeDerDipl(titre.toFwkScolarite_ScolFormationAnnee().fannFin());
				EOTypeDiplomeSise typeDiplomeSise = FinderTypeDiplomeSise
						.getTypeDiplomeSise(ec, titre.toFwkScolarite_ScolFormationTitre().fgraCode());
				if (typeDiplomeSise != null) {
					newHistorique.setToTypeDiplomeRelationship(typeDiplomeSise.toTypeDiplome());
				}
				else {
					newHistorique.setToTypeDiplomeRelationship(FinderTypeDiplome.getTypeDiplome(ec, "Y"));
				}
			}
			else {
				// pas d'obtention de nouveau diplome ==> les donnees sur le dernier diplome obtenu restent identiques a
				// celles du dernier historique
				newHistorique.setToFwkpers_Departement_DerDiplRelationship(lastHistorique.toFwkpers_Departement_DerDipl());
				newHistorique.setHistLibelleDerDipl(lastHistorique.histLibelleDerDipl());
				newHistorique.setHistVilleDerDipl(lastHistorique.histVilleDerDipl());
				newHistorique.setToFwkpers_Pays_DerDiplRelationship(lastHistorique.toFwkpers_Pays_DerDipl());
				newHistorique.setToRneDerDiplRelationship(lastHistorique.toRneDerDipl());
				newHistorique.setHistAnneeDerDipl(lastHistorique.histAnneeDerDipl());
				newHistorique.setToTypeDiplomeRelationship(lastHistorique.toTypeDiplome());
			}

			// infos sur le dernier etablissement connu en fonction de la derniere inscription principale connue
			newHistorique.setToRneDerEtabRelationship(inscriptionPrincipale.toFwkScolarite_ScolFormationSpecialisation()
					.toFwkScolarite_ScolFormationDiplome().toFwkScolarix_VEtablissementScolarite().toRne());
			if (inscriptionPrincipale != null && newHistorique.toRneDerEtab() != null) {
				newHistorique.setToFwkpers_Departement_DerEtabRelationship(newHistorique.toRneDerEtab().departement());
				newHistorique.setHistLibelleDerEtab(newHistorique.toRneDerEtab().llRne());
				newHistorique.setHistVilleDerEtab(newHistorique.toRneDerEtab().ville());
				if (newHistorique.toRneDerEtab().toAdresse() != null) {
					newHistorique.setToFwkpers_Pays_DerEtabRelationship(newHistorique.toRneDerEtab().toAdresse().toPays());
				}
				newHistorique.setHistAnneeDerEtab(lastHistorique.histAnneeScol());
				newHistorique.setHistEnsDerEtab(inscriptionPrincipale.libelleDiplomeAbrege());
			}
		}

		return newHistorique;
	}

	/**
	 * Cree un nouvel EOHistorique à partir du candidatAdmission
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param candidat
	 *            OBLIGATOIRE le EOCandidatGrhum à partir duquel on récupère les infos
	 * @return un EOHistorique
	 */
	public static EOHistorique create(EOEditingContext ec, EOCandidatGrhum candidat) {
		EOHistorique newHistorique = FactoryHistorique.createWithDefaultValues(ec, candidat.candAnneeScol());
		newHistorique.setHistAnneeDerEtab(candidat.candAnneeDerEtab());
		newHistorique.setHistEnsDerEtab(candidat.candEnsDerEtab());
		newHistorique.setToFwkpers_Departement_DerEtabRelationship(candidat.toFwkpers_Departement_DerEtab());
		newHistorique.setToFwkpers_Pays_DerEtabRelationship(candidat.toFwkpers_Pays_DerEtab());
		newHistorique.setToRneDerEtabRelationship(candidat.toRneDerEtab());
		newHistorique.setHistVilleDerEtab(candidat.candVilleDerEtab());
		newHistorique.setToSituationScolRelationship(candidat.toSituationScol());
		newHistorique.setToTypeClassePreparatoireRelationship(candidat.toTypeClassePreparatoire());
		newHistorique.addToToInscDiplsRelationship(FactoryInscDipl.createWithDefaultValues(ec,
				FinderTypeInscription.getTypeInscriptionPrincipale(ec), candidat.toFwkScolarite_ScolFormationSpecialisation(),
				candidat.candAnneeSuivie()));
		return newHistorique;
	}

}
