/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.factory;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.scolarix.serveur.finder.FinderCommune;
import org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryAdresse extends Factory {

	public static EOAdresse create(EOEditingContext ec) {
		EOAdresse newAdresse = (EOAdresse) Factory.instanceForEntity(ec, EOAdresse.ENTITY_NAME);
		ec.insertObject(newAdresse);
		newAdresse.setDCreation(new NSTimestamp());
		newAdresse.setDModification(new NSTimestamp());
		newAdresse.setDDebVal(new NSTimestamp());
		newAdresse.setAdrListeRouge(null);
		return newAdresse;
	}

	/**
	 * Cree un nouvel EOAdresse copie conforme du EOPreAdresse.
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param preAdresse
	 *            Le EOPreAdresse a recopier (si NULL, retourne un EOAdresse vide).
	 * @return le EOAdresse qui va bien.
	 */
	public static EOAdresse create(EOEditingContext ec, EOPreAdresse preAdresse) {
		EOAdresse newAdresse = create(ec);

		if (preAdresse != null) {
			newAdresse.setAdrAdresse1(preAdresse.adrAdresse1());
			newAdresse.setAdrAdresse2(preAdresse.adrAdresse2());
			newAdresse.setCodePostal(preAdresse.codePostal());
			newAdresse.setVille(preAdresse.ville());
			newAdresse.setToPaysRelationship(preAdresse.toFwkpers_Pays());
			FactoryAdresse.verifierEtCorrigerVille(newAdresse);

			newAdresse.setAdrBp(preAdresse.adrBp());
			newAdresse.setAdrListeRouge(preAdresse.adrListeRouge());
			newAdresse.setAdrUrlPere(preAdresse.adrUrlPere());
			newAdresse.setBisTer(preAdresse.bisTer());
			newAdresse.setCImplantation(preAdresse.cImplantation());
			newAdresse.setCpEtranger(preAdresse.cpEtranger());
			newAdresse.setCVoie(preAdresse.cVoie());
			newAdresse.setLocalite(preAdresse.localite());
			newAdresse.setNomVoie(preAdresse.nomVoie());
			newAdresse.setNoVoie(preAdresse.noVoie());
			newAdresse.setTemPayeUtil(preAdresse.temPayeUtil());
		}

		return newAdresse;
	}

	public static void recopierAdresseDansAdresse(EOAdresse adresseStable, EOAdresse adresseUniversitaire) {
		adresseUniversitaire.setToPaysRelationship(adresseStable.toPays());
		adresseUniversitaire.setAdrAdresse1(adresseStable.adrAdresse1());
		adresseUniversitaire.setAdrAdresse2(adresseStable.adrAdresse2());
		adresseUniversitaire.setCPCache(adresseStable.getCPCache());
		adresseUniversitaire.setVille(adresseStable.ville());
		adresseUniversitaire.setAdrBp(adresseStable.adrBp());
		//adresseUniversitaire.setCpEtranger(adresseStable.cpEtranger());
		adresseUniversitaire.setAdrListeRouge(adresseStable.adrListeRouge());
		adresseUniversitaire.setBisTer(adresseStable.bisTer());
		adresseUniversitaire.setCImplantation(adresseStable.cImplantation());
		adresseUniversitaire.setCVoie(adresseStable.cVoie());
		// adresseUniversitaire.setHabitantChez(adresseStable.habitantChez());
		adresseUniversitaire.setLocalite(adresseStable.localite());
		adresseUniversitaire.setNomVoie(adresseStable.nomVoie());
		adresseUniversitaire.setNoVoie(adresseStable.noVoie());
	}

	/**
	 * Corrige la ville (pour avoir l'orthographe exacte) si besoin et si possible par rapport a la vue TableCommunes, dans laquelle on doit
	 * trouver un enregistrement unique pour le binome cp/ville.<BR>
	 * Si l'adresse est en France, on verifie que le code postal/ville existe, sinon on essaye de deviner la ville qui a l'orthographe la
	 * plus rapprochante pour ce code postal.
	 */
	public static void verifierEtCorrigerVille(EOAdresse adresse) {
		if (isEnFranceAvecCpVille(adresse)) {
			NSArray<EOCommune> communes = FinderCommune.getCommunesExact(adresse.editingContext(), adresse.codePostal(), adresse.ville());
			if (communes == null || communes.count() == 0) {
				communes = FinderCommune.getCommunesLike(adresse.editingContext(), adresse.codePostal(), adresse.ville());
				if (communes != null && communes.count() > 0) {
					adresse.setVille(communes.lastObject().lcCom());
				}
			}
		}
	}

	private static boolean isEnFranceAvecCpVille(EOAdresse adresse) {
		return adresse.toPays() != null && adresse.toPays().cPays() != null && adresse.codePostal() != null && adresse.ville() != null
				&& adresse.toPays().cPays().equals(EOPays.CODE_PAYS_FRANCE);
	}

}
