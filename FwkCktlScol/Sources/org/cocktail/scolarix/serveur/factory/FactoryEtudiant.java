/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.factory;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.scolarix.serveur.finder.FinderMention;
import org.cocktail.scolarix.serveur.finder.FinderProfession;
import org.cocktail.scolarix.serveur.finder.FinderRne;
import org.cocktail.scolarix.serveur.finder.FinderSitFamEtudiant;
import org.cocktail.scolarix.serveur.finder.FinderTypeHebergement;
import org.cocktail.scolarix.serveur.finder.FinderTypeInscription;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOPreCandidat;
import org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOProfession;
import org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryEtudiant extends Factory {

	public static EOEtudiant create(EOEditingContext ec) {
		EOEtudiant newEtudiant = (EOEtudiant) Factory.instanceForEntity(ec, EOEtudiant.ENTITY_NAME);
		ec.insertObject(newEtudiant);
		newEtudiant.setDCreation(new NSTimestamp());
		newEtudiant.setDModification(new NSTimestamp());
		return newEtudiant;
	}

	/**
	 * Cree un nouvel etudiant from scratch avec les valeurs par defaut utiles et les relations minimum remplies. Cette methode est appelee
	 * lors de la saisie d'un nouveau dossier d'inscription administrative.
	 * 
	 * @param ec
	 * @param anneeUniversitaire
	 *            L'annee universitaire a laquelle on veut inscrire le nouvel etudiant que l'on cree.
	 * @param cRne
	 *            Le code rne de l'etablissement auquel on inscrit notre etudiant.
	 * @param individu
	 *            FACULTATIF. L'individu déjà existant à partir duquel on initialise un étudiant.
	 * @return un EOEtudiant initialise avec les valeurs de l'individu ou les valeurs minimum par defaut et les relations minimum par
	 *         defaut.
	 */
	public static EOEtudiant createForInscription(EOEditingContext ec, Integer anneeUniversitaire, String cRne, EOIndividu individu) {
		EOEtudiant newEtudiant = create(ec);
		newEtudiant.addToToHistoriquesRelationship(FactoryHistorique.createWithDefaultValues(ec, anneeUniversitaire));
		newEtudiant.setToFwkpers_IndividuRelationship(FactoryIndividuUlr.createForInscription(ec, individu));

		newEtudiant.setEtudType(EOEtudiant.ETUD_TYPE_INSCRIPTION);
		newEtudiant.setAnneeInscriptionEnCours(anneeUniversitaire);
		newEtudiant.setRne(FinderRne.getRne(ec, cRne));

		newEtudiant.setToSitFamEtudiantRelationship(FinderSitFamEtudiant.getSituation(ec, EOSitFamEtudiant.ETUD_SITFAM_DEFAUT));
		// Utile en Nouvelle-Caledonie ? newEtudiant.setDepartementParentRelationship(null);
		// Utile en Nouvelle-Caledonie ? newEtudiant.setTypeHebergementRelationship(null);

		newEtudiant.setToBacRelationship(null);

		newEtudiant.setEtudSportHn(new Integer(0));
		newEtudiant.setToProfessionRelationship(FinderProfession.getProfession(ec, EOProfession.PRO_CODE_DEFAUT));

		return newEtudiant;
	}

	/**
	 * 
	 * @param ec
	 * @param anneeUniversitaire
	 * @param cRne
	 * @param oldEtudiant
	 * @return
	 */
	public static EOEtudiant createForInscriptionWithEtudiant(EOEditingContext ec, Integer anneeUniversitaire, String cRne, EOEtudiant oldEtudiant,
			EOIndividu individuExistant) {

		EOEtudiant newEtudiant = oldEtudiant;
		EOHistorique histoExistant = null;
		if (newEtudiant == null) {
			EOEtudiant etudiantRes = FactoryEtudiant.createForInscription(ec, anneeUniversitaire, cRne, individuExistant);
			return etudiantRes;

		}
		else {
			// rechercher l'historique existant et le relier si necessaire
			NSArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			quals.add(new EOKeyValueQualifier(EOHistorique.ETUD_NUMERO_KEY, EOQualifier.QualifierOperatorEqual, newEtudiant.etudNumero()));
			quals.add(new EOKeyValueQualifier(EOHistorique.HIST_ANNEE_SCOL_KEY, EOQualifier.QualifierOperatorEqual, anneeUniversitaire));
			histoExistant = EOHistorique.fetchFirstByQualifier(ec, new EOAndQualifier(quals));
		}
		if (histoExistant == null) {
			histoExistant = FactoryHistorique.createWithDefaultValues(ec, anneeUniversitaire);
		}
		newEtudiant.addToToHistoriquesRelationship(histoExistant);
		newEtudiant.setEtudType(EOEtudiant.ETUD_TYPE_INSCRIPTION);
		newEtudiant.setAnneeInscriptionEnCours(anneeUniversitaire);
		newEtudiant.setRne(FinderRne.getRne(ec, cRne));

		return newEtudiant;
	}

	/**
	 * Cree une nouvelle admission from scratch avec les valeurs par defaut utiles et les relations minimum remplies. Cette methode est
	 * appelee lors de la saisie d'une nouvelle admission.
	 * 
	 * @param ec
	 * @param anneeUniversitaire
	 *            L'annee universitaire
	 * @param cRne
	 *            Le code rne de l'etablissement auquel on inscrit notre admission.
	 * @param individu
	 *            FACULTATIF. L'individu déjà existant à partir duquel on initialise certaines infos.
	 * @return un EOEtudiant initialise avec les valeurs de l'individu ou les valeurs minimum par defaut et les relations minimum par
	 *         defaut.
	 */
	public static EOEtudiant createForAdmission(EOEditingContext ec, Integer anneeUniversitaire, String cRne, EOIndividu individu) {
		EOEtudiant newEtudiant = null;
		if (individu != null) {
			newEtudiant = EOEtudiant.fetchByKeyValue(ec, EOEtudiant.NO_INDIVIDU_KEY, individu.noIndividu());
		}
		if (newEtudiant != null) {
			EOHistorique lastHistorique = newEtudiant.historiquePlusRecent(anneeUniversitaire);
			newEtudiant.addToToHistoriquesRelationship(FactoryHistorique.create(ec, lastHistorique, anneeUniversitaire));
			newEtudiant.setAnneeInscriptionEnCours(anneeUniversitaire);
			newEtudiant.setRne(FinderRne.getRne(ec, cRne));
		}
		else {
			newEtudiant = createForInscription(ec, anneeUniversitaire, cRne, individu);
		}
		newEtudiant.addToToCandidatGrhumsRelationship(EOCandidatGrhum.create(ec));
		return newEtudiant;
	}

	/**
	 * Cree un nouvel EOEtudiant copie conforme du EOPreEtudiant.<BR>
	 * Des relations sont egalement creees en cascade (si elles existent dans EOPreEtudiant) :
	 * <UL>
	 * <LI>IndividuUlr (depuis PreIndividu)
	 * <LI>Historique (depuis PreHistorique :-) )
	 * </UL>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param preEtudiant
	 *            OBLIGATOIRE : Le EOPreEtudiant a recopier.
	 * @param oldEtudiant
	 *            FACULTATIF : Un EOEtudiant qui existe deja, si non null on le met a jour et on renvoie celui-la.
	 * @return un EOEtudiant
	 */
	public static EOEtudiant create(EOEditingContext ec, EOPreEtudiant preEtudiant, EOEtudiant oldEtudiant) {
		EOEtudiant newEtudiant = oldEtudiant;
		if (newEtudiant == null) {
			newEtudiant = create(ec);
		}
		if (newEtudiant.editingContext() == null) {
			ec.insertObject(newEtudiant);
		}

		if (preEtudiant != null) {
			newEtudiant.setEtudNumero(preEtudiant.etudNumero());
			newEtudiant.setEtudCodeIne(preEtudiant.etudCodeIne());
			newEtudiant.setToFwkpers_AcademieRelationship(preEtudiant.toFwkpers_Academie());
			newEtudiant.setEtudAnbac(preEtudiant.etudAnbac());
			newEtudiant.setEtudAnnee1InscSup(preEtudiant.etudAnnee1InscSup());
			newEtudiant.setEtudAnnee1InscUniv(preEtudiant.etudAnnee1InscUniv());
			newEtudiant.setEtudAnnee1InscUlr(preEtudiant.etudAnnee1InscUlr());
			newEtudiant.setToBacRelationship(preEtudiant.toBac());
			newEtudiant.setCandNumero(preEtudiant.candNumero());
			newEtudiant.setCharte(preEtudiant.cptCharte());
			newEtudiant.setToFwkpers_Departement_EtabBacRelationship(preEtudiant.toFwkpers_Departement_EtabBac());
			newEtudiant.setToFwkpers_Departement_ParentRelationship(preEtudiant.toFwkpers_Departement_Parent());
			newEtudiant.setToFwkpers_Departement_EtabBacRelationship(preEtudiant.toFwkpers_Departement_EtabBac());
			newEtudiant.setToRneCodeBacRelationship(preEtudiant.toRneCodeBac());
			newEtudiant.setEtudReimmatriculation(preEtudiant.etudReimmatriculation());
			newEtudiant.setEtudSnAttestation(preEtudiant.etudSnAttestation());
			newEtudiant.setEtudSnCertification(preEtudiant.etudSnCertification());
			newEtudiant.setEtudSportHn(preEtudiant.etudSportHn());
			newEtudiant.setEtudVilleBac(preEtudiant.etudVilleBac());
			if (newEtudiant.etudVilleBac() == null && preEtudiant.toRneCodeBac() != null) {
				newEtudiant.setEtudVilleBac(preEtudiant.toRneCodeBac().ville());
			}
			newEtudiant.setToMentionBacRelationship(preEtudiant.toMentionBac());
			newEtudiant.setToFwkpers_Pays_EtabBacRelationship(preEtudiant.toFwkpers_Pays_EtabBac());
			newEtudiant.setToProfessionRelationship(preEtudiant.toProfession());
			newEtudiant.setToProfession2Relationship(preEtudiant.toProfession2());
			newEtudiant.setToRneCodeBacRelationship(preEtudiant.toRneCodeBac());
			newEtudiant.setToRneCodeSupRelationship(preEtudiant.toRneCodeSup());
			newEtudiant.setToSitFamEtudiantRelationship(preEtudiant.toSitFamEtudiant());
			newEtudiant.setUniversitePremiereInscription(preEtudiant.universitePremiereInscription());
			if (preEtudiant.isReInscription()) {
				newEtudiant.setEtudType(EOEtudiant.ETUD_TYPE_PRE_RE_INSCRIPTION);
			}
			else {
				newEtudiant.setEtudType(EOEtudiant.ETUD_TYPE_PRE_INSCRIPTION);
			}
			if (preEtudiant.toRdvPreEtudiant() != null && preEtudiant.toRdvPreEtudiant().convocDate() != null) {
				newEtudiant.setToRdvCandidatRelationship(preEtudiant.toRdvPreEtudiant());
			}
			else {
				newEtudiant.setToRdvCandidatRelationship(null);
			}

			// IndividuUlr depuis PreIndividu
			if (preEtudiant.toPreIndividu() != null) {
				newEtudiant.setToFwkpers_IndividuRelationship(FactoryIndividuUlr.create(ec, preEtudiant.toPreIndividu()));
			}
			// Historique depuis PreHistorique
			if (preEtudiant.preHistorique() != null) {
				newEtudiant.addToToHistoriquesRelationship(FactoryHistorique.create(ec, preEtudiant.preHistorique()));
			}
			newEtudiant.setToTypeHebergementRelationship(preEtudiant.toTypeHebergement());
		}
		return newEtudiant;
	}

	public static EOEtudiant create(EOEditingContext ec, EOCandidatGrhum candidat, EOEtudiant etudiant) {
		EOEtudiant newEtudiant = etudiant;
		if (newEtudiant == null) {
			newEtudiant = create(ec);
		}

		if (candidat != null) {
			newEtudiant.setAnneeInscriptionEnCours(candidat.candAnneeScol());
			newEtudiant.addToToCandidatGrhumsRelationship(candidat);
			newEtudiant.setEtudNumero(candidat.etudNumero());
			newEtudiant.setEtudCodeIne(candidat.candBea());
			newEtudiant.setCandNumero(new Integer(candidat.primaryKey()));

			// Titre accès
			newEtudiant.setAnneeObtentionTitreAcces(candidat.candAnbac());
			newEtudiant.setToBacRelationship(candidat.toBac());
			newEtudiant.setToFwkpers_Departement_EtabBacRelationship(candidat.toFwkpers_Departement_EtabBac());
			newEtudiant.setToFwkpers_Departement_ParentRelationship(candidat.toFwkpers_Departement_Parent());
			newEtudiant.setToRneCodeBacRelationship(candidat.toRneCodeBac());
			newEtudiant.setEtudVilleBac(candidat.candVilleBac());
			if (candidat.candVilleBac() == null && candidat.toRneCodeBac() != null) {
				newEtudiant.setEtudVilleBac(candidat.toRneCodeBac().ville());
			}
			newEtudiant.setToMentionBacRelationship(candidat.toMentionBac());
			newEtudiant.setToFwkpers_Pays_EtabBacRelationship(candidat.toFwkpers_Pays_EtabBac());
			newEtudiant.setToRneCodeBacRelationship(candidat.toRneCodeBac());
			newEtudiant.setToSitFamEtudiantRelationship(candidat.toSitFamEtudiant());
			newEtudiant.setEtudType(EOEtudiant.ETUD_TYPE_ADMISSION);
			// IndividuUlr
			if (candidat.toFwkpers_Individu() != null) {
				newEtudiant.setToFwkpers_IndividuRelationship(candidat.toFwkpers_Individu());
			}
			else {
				if (newEtudiant.toFwkpers_Individu() == null) {
					newEtudiant.setToFwkpers_IndividuRelationship(FactoryIndividuUlr.create(ec, candidat));
				}
			}
			newEtudiant.setEmail(candidat.candEmailScol());
			// Historique
			EOHistorique newHistorique;
			if (newEtudiant.historiquePlusRecent(newEtudiant.anneeInscriptionEnCours()) != null) {
				newHistorique = FactoryHistorique.create(ec, newEtudiant.historiquePlusRecent(newEtudiant.anneeInscriptionEnCours()),
						newEtudiant.anneeInscriptionEnCours());
				newHistorique.addToToInscDiplsRelationship(FactoryInscDipl.createWithDefaultValues(ec,
						FinderTypeInscription.getTypeInscriptionPrincipale(ec), candidat.toFwkScolarite_ScolFormationSpecialisation(),
						candidat.candAnneeSuivie()));
			}
			else {
				newHistorique = FactoryHistorique.create(ec, candidat);
			}
			newEtudiant.addToToHistoriquesRelationship(newHistorique);
		}
		return newEtudiant;
	}

	/**
	 * Cree un nouvel EOEtudiant a partir d'un EOPreCandidat. PreInscription UNIQUEMENT !<BR>
	 * Des relations sont egalement creees en cascade :
	 * <UL>
	 * <LI>IndividuUlr (depuis PreCandidat)
	 * <LI>Historique initialise par defaut pour l'annee de preinscription en cours
	 * </UL>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param preCandidat
	 *            OBLIGATOIRE : Le EOPreCandidat d'ou on recupere les infos.
	 * @param anneeScol
	 *            OBLIGATOIRE L'annee universitaire pour laquelle on inscrit ce candidat
	 * @param oldEtudiant
	 *            FACULTATIF : Un EOEtudiant qui existe deja, si non null on le met a jour et on renvoie celui-la.
	 * @return un EOEtudiant
	 */
	public static EOEtudiant create(EOEditingContext ec, EOPreCandidat preCandidat, Integer anneeScol, EOEtudiant oldEtudiant) {
		EOEtudiant newEtudiant = oldEtudiant;
		if (newEtudiant == null) {
			newEtudiant = create(ec);
		}
		if (newEtudiant.editingContext() == null) {
			ec.insertObject(newEtudiant);
		}

		if (preCandidat != null) {
			newEtudiant.setCandNumero(preCandidat.candNumero());
			newEtudiant.setEtudCodeIne(preCandidat.candBea());
			newEtudiant.setEtudSportHn(new Integer(0));

			// Infos relatives au Bac
			newEtudiant.setToFwkpers_AcademieRelationship(preCandidat.toFwkpers_Academie());
			newEtudiant.setEtudAnbac(preCandidat.candAnbac());
			newEtudiant.setEtudVilleBac(preCandidat.candVilleBac());

			newEtudiant.setEtudAnnee1InscSup(anneeScol);
			newEtudiant.setEtudAnnee1InscUniv(anneeScol);
			newEtudiant.setToRneCodeSupRelationship(FinderRne.getRneLocal(ec));
			newEtudiant.setEtudAnnee1InscUlr(anneeScol);

			newEtudiant.setToBacRelationship(preCandidat.toBac());
			newEtudiant.setToMentionBacRelationship(FinderMention.getMention(ec, "0", null));
			newEtudiant.setToRneCodeBacRelationship(preCandidat.toRneCodeBac());
			newEtudiant.setToFwkpers_Departement_EtabBacRelationship(preCandidat.toFwkpers_Departement_EtabBac());
			newEtudiant.setToFwkpers_Pays_EtabBacRelationship(preCandidat.toFwkpers_Pays_EtabBac());

			// IndividuUlr depuis PreCandidat
			if (newEtudiant.individu() == null) {
				newEtudiant.setToFwkpers_IndividuRelationship(FactoryIndividuUlr.create(ec, preCandidat));
				if (newEtudiant.adresseUniversitaire() != null
						&& (newEtudiant.adresseUniversitaire().toPays() == null || newEtudiant.adresseUniversitaire().toPays().cPays() == null)) {
					newEtudiant.setToTypeHebergementRelationship(FinderTypeHebergement.getTypeHebergementEnAttente(ec));
				}
			}

			// Historique
			newEtudiant.addToToHistoriquesRelationship(FactoryHistorique.create(ec, preCandidat, anneeScol));
		}
		return newEtudiant;
	}

	public static void setSituationFamiliale(EOEtudiant etudiant, String situation) {
		EOSitFamEtudiant situationFamiliale = FinderSitFamEtudiant.getSituation(etudiant.editingContext(), situation);
		etudiant.setToSitFamEtudiantRelationship(situationFamiliale);
	}

}
