/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.factory;

import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryPersonneTelephone extends Factory {

	public static EOPersonneTelephone create(EOEditingContext ec) {
		EOPersonneTelephone newPersonneTelephone = (EOPersonneTelephone) Factory.instanceForEntity(ec, EOPersonneTelephone.ENTITY_NAME);
		ec.insertObject(newPersonneTelephone);
		newPersonneTelephone.setDCreation(new NSTimestamp());
		newPersonneTelephone.setDModification(new NSTimestamp());
		newPersonneTelephone.setDDebVal(new NSTimestamp());
		return newPersonneTelephone;
	}

	/**
	 * Cree un nouvel EOPersonneTelephone copie conforme du EOPrePersonneTelephone.
	 * 
	 * @param ec
	 * @param prePersonneTelephone
	 *            Le EOPrePersonneTelephone a recopier (si NULL retourne un EOPersonneTelephone vide).
	 * @return Un EOPersonneTelephone qui va bien.
	 */
	public static EOPersonneTelephone create(EOEditingContext ec, EOPrePersonneTelephone prePersonneTelephone) {
		EOPersonneTelephone newPersonneTelephone = create(ec);
		if (prePersonneTelephone != null) {
			newPersonneTelephone.setDDebVal(prePersonneTelephone.dDebVal());
			newPersonneTelephone.setDFinVal(prePersonneTelephone.dFinVal());
			newPersonneTelephone.setIndicatif(prePersonneTelephone.indicatif());
			newPersonneTelephone.setListeRouge(prePersonneTelephone.listeRouge());
			newPersonneTelephone.setNoTelephone(StringCtrl.formatPhoneNumber(prePersonneTelephone.noTelephone()));
			newPersonneTelephone.setToTypeNoTelRelationship(prePersonneTelephone.toFwkpers_TypeNoTel());
			newPersonneTelephone.setToTypeTelRelationship(prePersonneTelephone.toFwkpers_TypeTel());
		}
		return newPersonneTelephone;
	}

	public static EOPersonneTelephone createTelephonePAR(EOEditingContext ec, String no) {
		EOPersonneTelephone newPersonneTelephone = create(ec);
		newPersonneTelephone.setToTypeTelRelationship(EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_PAR));
		newPersonneTelephone.setToTypeNoTelRelationship(EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL));

		newPersonneTelephone.setNoTelephone(StringCtrl.formatPhoneNumber(no));
		return newPersonneTelephone;
	}

	public static EOPersonneTelephone createTelephoneEtudiant(EOEditingContext ec, String no) {
		EOPersonneTelephone newPersonneTelephone = create(ec);
		newPersonneTelephone.setToTypeTelRelationship(EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_ETUD));
		newPersonneTelephone.setToTypeNoTelRelationship(EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL));

		newPersonneTelephone.setNoTelephone(StringCtrl.formatPhoneNumber(no));
		return newPersonneTelephone;
	}

	public static EOPersonneTelephone createTelephoneEtudiantMOB(EOEditingContext ec, String no) {
		EOPersonneTelephone newPersonneTelephone = create(ec);
		newPersonneTelephone.setToTypeTelRelationship(EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_ETUD));
		newPersonneTelephone.setToTypeNoTelRelationship(EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_MOB));

		newPersonneTelephone.setNoTelephone(StringCtrl.formatPhoneNumber(no));
		return newPersonneTelephone;
	}
}
