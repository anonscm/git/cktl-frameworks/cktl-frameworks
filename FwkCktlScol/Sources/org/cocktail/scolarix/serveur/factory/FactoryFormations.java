/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.factory;

import java.math.BigDecimal;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationProgression;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre;
import org.cocktail.scolarix.serveur.finder.FinderPreOuvertureDiplome;
import org.cocktail.scolarix.serveur.finder.FinderScolFormationProgression;
import org.cocktail.scolarix.serveur.finder.FinderScolFormationTitre;
import org.cocktail.scolarix.serveur.finder.FinderScolInscriptionTitre;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOResultat;
import org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class FactoryFormations {

	public static NSArray<EOScolFormationProgression> getProgressions(EOEditingContext ec, EOEtudiant etudiant) {
		Integer anneeEnCours = etudiant.anneeInscriptionEnCours();
		if (etudiant.formationsEnCours() == null || anneeEnCours == null) {
			return null;
		}
		NSArray<EOVWebInscriptionResultat> inscriptionsEnCours = new NSArray<EOVWebInscriptionResultat>(etudiant.formationsEnCours());
		NSMutableArray<NSDictionary<String, Object>> inscriptionsListe = new NSMutableArray<NSDictionary<String, Object>>();
		NSMutableArray<NSDictionary<String, Object>> redoublementsListe = new NSMutableArray<NSDictionary<String, Object>>();

		if (construireInscriptionsListe(ec, etudiant, inscriptionsListe, redoublementsListe) == false) {
			return NSArray.EmptyArray;
		}

		calculerSemestresEtCredits(inscriptionsEnCours, inscriptionsListe);

		calculerInscriptionsValides(inscriptionsListe);

		supprimerProgressionsImpossibles(ec, anneeEnCours, inscriptionsListe);

		epurerProgressionsNonValides(ec, anneeEnCours, inscriptionsListe);

		if (verifierTitreOK(ec, etudiant, inscriptionsListe, redoublementsListe) == false) {
			return NSArray.EmptyArray;
		}

		// Construction du tableau des progressions
		// On recupere toutes les formations qui ouvrent a progression
		NSMutableArray<NSArray<Integer>> tempProgressionProgressions = construirePremierTableauProgressions(inscriptionsListe);

		NSMutableArray<EOScolFormationProgression> progressionsListe = new NSMutableArray<EOScolFormationProgression>();
		int typeAssociation = 1;
		for (int i = 0; i < redoublementsListe.count(); i++) {
			NSMutableDictionary<String, Object> redoublement = (NSMutableDictionary<String, Object>) redoublementsListe.objectAtIndex(i);
			NSArray<EOScolFormationProgression> redoublementsInscription = FinderScolFormationProgression.getProgressionsForCriteres(ec,
					anneeEnCours, (Integer) redoublement.valueForKeyPath("fspnKey"), (Integer) redoublement.valueForKeyPath("fhabNiveau"),
					EOScolFormationProgression.TYPE_REDOUBLEMENT);
			if (redoublementsInscription != null && redoublementsInscription.count() > 0) {
				for (int j = 0; j < redoublementsInscription.count(); j++) {
					EOScolFormationProgression progressionRedoublement = redoublementsInscription.objectAtIndex(j);
					progressionRedoublement.setTypeAssociation(new Integer(typeAssociation));
					String minima = (String) redoublement.valueForKeyPath("passageAMinima");
					if (minima != null) {
						progressionRedoublement.setIsPassageAMinima(minima.equals("O"));
					}
					else {
						progressionRedoublement.setIsPassageAMinima(false);
					}
					progressionsListe.addObject(progressionRedoublement);
				}
				typeAssociation++;
			}
		}

		NSMutableArray<EOScolFormationProgression> progressions = new NSMutableArray<EOScolFormationProgression>();
		for (int i = 0; i < tempProgressionProgressions.count(); i++) {
			NSArray<Integer> progression = tempProgressionProgressions.objectAtIndex(i);
			NSArray<EOScolFormationProgression> progressionsInscription = FinderScolFormationProgression.getProgressionsForCriteres(ec, anneeEnCours,
					progression.objectAtIndex(0), progression.objectAtIndex(1), null);
			if (progressionsInscription != null && progressionsInscription.count() > 0) {
				for (int j = 0; j < progressionsInscription.count(); j++) {
					EOScolFormationProgression progressionProgression = progressionsInscription.objectAtIndex(j);
					progressionProgression.setTypeAssociation(new Integer(typeAssociation));
					progressions.addObject(progressionProgression);
				}
				typeAssociation++;
			}
		}

		// on enleve les progressions qui correspondent a des inscriptions en cours
		if (progressions != null && progressions.count() > 0) {
			for (int i = 0; i < inscriptionsListe.count(); i++) {
				NSMutableDictionary<String, Object> inscription = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(i);

				NSArray<EOScolFormationProgression> redoublementsInscription = FinderScolFormationProgression.getProgressionsForCriteres(ec,
						anneeEnCours, (Integer) inscription.valueForKeyPath("fspnKey"), (Integer) inscription.valueForKeyPath("fhabNiveau"),
						EOScolFormationProgression.TYPE_REDOUBLEMENT);

				for (int j = progressions.count() - 1; j >= 0; j--) {
					EOScolFormationProgression progression = (progressions.objectAtIndex(j));

					for (int k = 0; k < redoublementsInscription.count(); k++) {
						EOScolFormationProgression redoublement = redoublementsInscription.objectAtIndex(k);

						if ((progression.toFwkScolarite_ScolFormationSpecialisationProgression().fspnKey().intValue() == redoublement
								.toFwkScolarite_ScolFormationSpecialisationProgression().fspnKey().intValue())
								&& (progression.fproFhabNiveau().intValue() == redoublement.fproFhabNiveau().intValue())) {
							// a-til le titre ?
							progressions.removeObjectAtIndex(j);
							break;
						}
					}
				}
			}
			progressionsListe.addObjectsFromArray(progressions);
		}

		return progressionsListe;
	}

	/**
	 * On construit un tableau de travail : inscriptionsListe avec des objets definis : objectArray Ces objets seront le reflet des
	 * progressions permises (valide a "O")
	 * 
	 * @param ec
	 * @param etudiant
	 * @param inscriptionsListe
	 * @param redoublementsListe
	 */
	private static boolean construireInscriptionsListe(EOEditingContext ec, EOEtudiant etudiant,
			NSMutableArray<NSDictionary<String, Object>> inscriptionsListe, NSMutableArray<NSDictionary<String, Object>> redoublementsListe) {
		Integer anneeEnCours = etudiant.anneeInscriptionEnCours();
		NSArray<EOVWebInscriptionResultat> formationsEnCours = etudiant.formationsEnCours();
		for (int i = 0; i < formationsEnCours.count(); i++) {
			EOVWebInscriptionResultat formation = formationsEnCours.objectAtIndex(i);

			// On construit l'objet...
			Integer myKey = formation.toFwkScolarite_ScolFormationSpecialisation().fspnKey();
			Integer myNiveau = formation.idiplAnneeSuivie();
			String myGrade = formation.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().filiere().code();
			String myBlocage = FinderPreOuvertureDiplome.recupererBlocagePourCriteres(ec, anneeEnCours, myKey, myNiveau);

			// si au moins une de ses inscriptions n'a pas encore eu de résultat (jury pas statue), on ne lui propose
			// rien...
			if (myBlocage != null && myBlocage.equalsIgnoreCase("N") == false) {
				if (formation.etatType() != null && formation.etatType().intValue() == 0) {
					if (formation.niveauDetail() != null && formation.niveauDetail().equals("Année")) {
						etudiant.setMessageInterditDeConnexion("Les jurys n'ont pas statué, veuillez attendre les délibérations de jury pour vous réinscrire. Pour connaitre la date de délibération, veuillez contacter votre secrétariat pédagogique.");
					}
					else {
						etudiant.setMessageInterditDeConnexion("Vous suivez une formation dont au moins une session n'est pas validée, veuillez attendre les délibérations de jury pour vous réinscrire. Pour connaitre la date de délibération, veuillez contacter votre secrétariat pédagogique.");
					}
					return false;
				}
			}

			NSMutableDictionary<String, Object> objectArray = new NSMutableDictionary<String, Object>();
			objectArray.setObjectForKey(myKey, "fspnKey");
			objectArray.setObjectForKey(myNiveau, "fhabNiveau");
			objectArray.setObjectForKey(myGrade, "fgraCode");
			objectArray.setObjectForKey(myBlocage, "blocage");
			objectArray.setObjectForKey(formation.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipCode(),
					"fdipCode");
			objectArray.setObjectForKey(new Integer(0), "totalSemestres");
			objectArray.setObjectForKey(new Integer(0), "totalCredits");
			objectArray.setObjectForKey("O", "valide");
			//if (myGrade != null && (myGrade.equals("D") || myGrade.equals("DOCT"))) {
				objectArray.setObjectForKey("O", "passeDroit");
//			}
//			else {
//				objectArray.setObjectForKey("N", "passeDroit");
//			}
			if (formation.toInscDipl().toResultat() != null && formation.toInscDipl().toResultat().resCode().equals(EOResultat.RES_CODE_MINIMA)) {
				objectArray.setObjectForKey("O", "passageAMinima");
			}
			else {
				objectArray.setObjectForKey("N", "passageAMinima");
			}

			// On l'insere dans le tableau...
			if (inscriptionsListe.containsObject(objectArray) == false) {
				inscriptionsListe.addObject(objectArray);
			}

			// redoublements...
			if (formation.etat() != null && formation.etat().intValue() == 0) {
				// La formation est-elle deja dans le panier ?
				if (redoublementsListe.containsObject(objectArray) == false) {
					redoublementsListe.addObject(objectArray);
				}
			}
		}
		return true;
	}

	/**
	 * On parcours le tableau de travail pour positionner les valeurs "totalSemestres" et "totalCredits"
	 * 
	 * @param inscriptionsEnCours
	 * @param inscriptionsListe
	 */
	private static void calculerSemestresEtCredits(NSArray<EOVWebInscriptionResultat> inscriptionsEnCours,
			NSMutableArray<NSDictionary<String, Object>> inscriptionsListe) {
		for (int i = 0; i < inscriptionsListe.count(); i++) {
			NSMutableDictionary<String, Object> inscription = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(i);

			for (int j = 0; j < inscriptionsEnCours.count(); j++) {
				EOVWebInscriptionResultat myRec = inscriptionsEnCours.objectAtIndex(j);

				if ((myRec.toFwkScolarite_ScolFormationSpecialisation().fspnKey().intValue() == ((Integer) inscription.valueForKeyPath("fspnKey"))
						.intValue()) && (myRec.idiplAnneeSuivie().intValue() == ((Integer) inscription.valueForKeyPath("fhabNiveau")).intValue())) {
					// Les semestres...
					if (myRec.etat().intValue() > 0) {
						int mySemestre = ((Integer) inscription.valueForKeyPath("totalSemestres")).intValue() + 1;
						inscription.setObjectForKey(new Integer(mySemestre), "totalSemestres");
					}

					// Les credits...
					double myCredits = ((Number) inscription.valueForKeyPath("totalCredits")).doubleValue();
					myCredits = myCredits + myRec.credits().doubleValue();
					inscription.setObjectForKey(new BigDecimal(myCredits), "totalCredits");
				}
			}
		}
	}

	/**
	 * On parcours le tableau de travail pour positionner la valeur "valide"
	 * 
	 * @param inscriptionsListe
	 */
	private static void calculerInscriptionsValides(NSMutableArray<NSDictionary<String, Object>> inscriptionsListe) {
		for (int i = 0; i < inscriptionsListe.count(); i++) {
			NSMutableDictionary<String, Object> inscription = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(i);

			// Verification SEMESTRES
			if ((((String) inscription.valueForKeyPath("blocage")).equals("1") == true)
					&& (((Integer) inscription.valueForKeyPath("totalSemestres")).intValue() == 0)) {
				inscription.setObjectForKey("N", "valide");
			}

			// Verification CREDITS
			if ((((String) inscription.valueForKeyPath("blocage")).equals("2") == true)
					&& (((Number) inscription.valueForKeyPath("totalCredits")).doubleValue() < 30.00)) {
				inscription.setObjectForKey("N", "valide");
			}

			// Aucune Verification
			if ((((String) inscription.valueForKeyPath("blocage")).equals("3") == true)
					&& (((Number) inscription.valueForKeyPath("totalCredits")).doubleValue() < 30.00)) {
				inscription.setObjectForKey("N", "valide");
			}
		}
	}

	/**
	 * On parcours le tableau de travail pour epurer les progressions // On devalide les inscriptions qui correspondent a des progressions
	 * impossibles
	 * 
	 * @param ec
	 * @param anneeEnCours
	 * @param inscriptionsListe
	 */
	private static void supprimerProgressionsImpossibles(EOEditingContext ec, Integer anneeEnCours,
			NSMutableArray<NSDictionary<String, Object>> inscriptionsListe) {
		for (int i = 0; i < inscriptionsListe.count(); i++) {
			NSMutableDictionary<String, Object> inscription = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(i);

			if (((String) inscription.valueForKeyPath("valide")).equals("N") == true) {
				// On cherche la progression possible
				Integer myKey = (Integer) inscription.valueForKeyPath("fspnKey");
				Integer myNiveau = (Integer) inscription.valueForKeyPath("fhabNiveau");

				NSArray<EOScolFormationProgression> progressions = FinderScolFormationProgression.getProgressionsForCriteres(ec, anneeEnCours, myKey,
						myNiveau, EOScolFormationProgression.TYPE_PROGRESSION);

				for (int j = 0; j < progressions.count(); j++) {
					for (int k = 0; k < inscriptionsListe.count(); k++) {
						EOScolFormationProgression progression = progressions.objectAtIndex(j);
						NSMutableDictionary<String, Object> inscription2 = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(k);

						// comme on peut redoubler ou progresser dans une formation differente (chgt de maquette), il
						// faut chercher les redoublements possibles a partir de l'insc en cours (inscriptionListe)
						NSArray<EOScolFormationProgression> redoublements = FinderScolFormationProgression.getProgressionsForCriteres(ec,
								anneeEnCours, (Integer) inscription2.valueForKeyPath("fspnKey"),
								(Integer) inscription2.valueForKeyPath("fhabNiveau"), EOScolFormationProgression.TYPE_REDOUBLEMENT);

						for (int l = 0; l < redoublements.count(); l++) {
							EOScolFormationProgression redoublement = redoublements.objectAtIndex(l);
							if ((redoublement.toFwkScolarite_ScolFormationSpecialisationProgression().fspnKey().intValue() == progression
									.toFwkScolarite_ScolFormationSpecialisationProgression().fspnKey().intValue())
									&& (redoublement.fproFhabNiveau().intValue() == progression.fproFhabNiveau().intValue())) {
								inscription2.setObjectForKey("N", "valide");
							}
						}
					}
				}
			}
		}
	}

	/**
	 * On parcours le tableau de travail pour epurer les progressions<br>
	 * Pour les progressions de type 2 (sur les credits), on calcule le cumul en retard qui ne doit pas depasser 30.00 (1 semestre en fait)
	 * 
	 * @param ec
	 * @param anneeEnCours
	 * @param inscriptionsListe
	 */
	private static void epurerProgressionsNonValides(EOEditingContext ec, Integer anneeEnCours,
			NSMutableArray<NSDictionary<String, Object>> inscriptionsListe) {
		for (int i = 0; i < inscriptionsListe.count(); i++) {
			NSMutableDictionary<String, Object> inscription = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(i);
			double myRetard1 = 60.00 - ((Number) inscription.valueForKeyPath("totalCredits")).doubleValue();

			if ((((String) inscription.valueForKeyPath("valide")).equals("O") == true)
					&& (((String) inscription.valueForKeyPath("blocage")).equals("2") == true)) {
				// On cherche la progression possible
				Integer myKey = (Integer) inscription.valueForKeyPath("fspnKey");
				Integer myNiveau = (Integer) inscription.valueForKeyPath("fhabNiveau");

				NSArray<EOScolFormationProgression> progressions = FinderScolFormationProgression.getProgressionsForCriteres(ec, anneeEnCours, myKey,
						myNiveau, EOScolFormationProgression.TYPE_PROGRESSION);

				for (int j = 0; j < progressions.count(); j++) {
					EOScolFormationProgression progression = progressions.objectAtIndex(j);
					for (int k = 0; k < inscriptionsListe.count(); k++) {
						NSMutableDictionary<String, Object> inscription2 = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(k);

						// comme on peut redoubler ou progresser dans une formation differente (chgt de maquette), il
						// faut chercher les redoublements possibles a partir de l'insc en cours (inscriptionListe)
						NSArray<EOScolFormationProgression> redoublements = FinderScolFormationProgression.getProgressionsForCriteres(ec,
								anneeEnCours, (Integer) inscription2.valueForKeyPath("fspnKey"),
								(Integer) inscription2.valueForKeyPath("fhabNiveau"), EOScolFormationProgression.TYPE_REDOUBLEMENT);

						for (int l = 0; l < redoublements.count(); l++) {
							EOScolFormationProgression redoublement = redoublements.objectAtIndex(l);
							if ((redoublement.toFwkScolarite_ScolFormationSpecialisationProgression().fspnKey().intValue() == progression
									.toFwkScolarite_ScolFormationSpecialisationProgression().fspnKey().intValue())
									&& (redoublement.fproFhabNiveau().intValue() == progression.fproFhabNiveau().intValue())) {
								double myRetard2 = 60.00 - ((Number) inscription2.valueForKeyPath("totalCredits")).doubleValue();

								if ((myRetard1 + myRetard2) > 30.00) {
									inscription2.setObjectForKey("N", "valide");
								}
							}
						}
					}
				}
			}
		}
	}

	private static boolean verifierTitreOK(EOEditingContext ec, EOEtudiant etudiant, NSMutableArray<NSDictionary<String, Object>> inscriptionsListe,
			NSMutableArray<NSDictionary<String, Object>> redoublementsListe) {
		for (int i = 0; i < inscriptionsListe.count(); i++) {
			NSMutableDictionary<String, Object> inscription = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(i);
			if (inscription.valueForKeyPath("passeDroit").equals("N") && ((String) inscription.valueForKeyPath("valide")).equals("O") == true) {
				EOScolFormationTitre titre = FinderScolFormationTitre.getScolFormationTitre(ec, (String) inscription.valueForKeyPath("fdipCode"),
						(Integer) inscription.valueForKeyPath("fhabNiveau"));
				// On cherche le resultat au Titre s'il y a un titre
				if (titre != null && FinderScolInscriptionTitre.isRecuPourTitre(ec, etudiant.etudNumero(), titre) == false) {
					inscription.setObjectForKey("N", "valide");
					if (redoublementsListe.count() == 0) {
						etudiant.setMessageInterditDeConnexion("Vous êtes en attente de délibération de jury pour le titre "
								+ titre.filiere().abreviation()
								+ ". Vous pourrez vous réinscrire après cette délibération. Pour connaitre la date de délibération, veuillez contacter votre secrétariat pédagogique.");
						return false;
					}
				}
			}
		}
		return true;
	}

	private static NSMutableArray<NSArray<Integer>> construirePremierTableauProgressions(
			NSMutableArray<NSDictionary<String, Object>> inscriptionsListe) {
		NSMutableArray<NSArray<Integer>> tempProgressionProgressions = new NSMutableArray<NSArray<Integer>>();
		for (int i = 0; i < inscriptionsListe.count(); i++) {
			NSMutableDictionary<String, Object> inscription = (NSMutableDictionary<String, Object>) inscriptionsListe.objectAtIndex(i);
			if (((String) inscription.valueForKeyPath("valide")).equals("O") == true) {
				tempProgressionProgressions.addObject(new NSArray<Integer>(new Integer[] { (Integer) inscription.valueForKeyPath("fspnKey"),
						(Integer) inscription.valueForKeyPath("fhabNiveau") }));
			}
		}
		return tempProgressionProgressions;
	}

}