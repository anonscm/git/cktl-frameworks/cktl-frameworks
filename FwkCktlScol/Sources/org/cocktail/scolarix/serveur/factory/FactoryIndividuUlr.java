/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.factory;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOPreCandidat;
import org.cocktail.scolarix.serveur.metier.eos.EOPreIndividu;
import org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone;
import org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class FactoryIndividuUlr extends Factory {

	private static final EOKeyValueQualifier QUAL_TYPE_NO_TEL_MOBILE = new EOKeyValueQualifier(EOPersonneTelephone.TO_TYPE_NO_TEL_KEY + "."
			+ EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOKeyValueQualifier.QualifierOperatorEqual, EOTypeNoTel.TYPE_NO_TEL_MOB);
	private static final EOKeyValueQualifier QUAL_TYPE_TEL_ETUDIANT = new EOKeyValueQualifier(EOPersonneTelephone.TO_TYPE_TEL_KEY + "."
			+ EOTypeTel.C_TYPE_TEL_KEY, EOKeyValueQualifier.QualifierOperatorEqual, EOTypeTel.C_TYPE_TEL_ETUD);
	private static final EOKeyValueQualifier QUAL_TYPE_NO_TEL_TELEPHONE = new EOKeyValueQualifier(EOPersonneTelephone.TO_TYPE_NO_TEL_KEY + "."
			+ EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOKeyValueQualifier.QualifierOperatorEqual, EOTypeNoTel.TYPE_NO_TEL_TEL);
	private static final EOKeyValueQualifier QUAL_TYPE_TEL_PARENTS = new EOKeyValueQualifier(EOPersonneTelephone.TO_TYPE_TEL_KEY + "."
			+ EOTypeTel.C_TYPE_TEL_KEY, EOKeyValueQualifier.QualifierOperatorEqual, EOTypeTel.C_TYPE_TEL_PAR);

	private static final EOQualifier QUAL_TELEPHONE_PARENTS = ERXQ.and(QUAL_TYPE_TEL_PARENTS, QUAL_TYPE_NO_TEL_TELEPHONE);
	private static final EOQualifier QUAL_TELEPHONE_ETUDIANT = ERXQ.and(QUAL_TYPE_TEL_ETUDIANT, QUAL_TYPE_NO_TEL_TELEPHONE);
	private static final EOQualifier QUAL_MOBILE_ETUDIANT = ERXQ.and(QUAL_TYPE_TEL_ETUDIANT, QUAL_TYPE_NO_TEL_MOBILE);

	public static EOIndividu create(EOEditingContext ec) {
		EOIndividu newIndividu = (EOIndividu) Factory.instanceForEntity(ec, EOIndividu.ENTITY_NAME);
		ec.insertObject(newIndividu);
		newIndividu.setDCreation(DateCtrl.now());
		newIndividu.setDModification(DateCtrl.now());
		newIndividu.setTemValide("O");
		newIndividu.setPriseCptInsee("R");
		return newIndividu;
	}

	public static EOIndividu createForInscription(EOEditingContext ec, EOIndividu individu) {
		EOIndividu newIndividu = individu;
		boolean createAdrStable = false, createAdrEtud = false, createTelPar = false, createTelEtud = false, createTelEtudMob = false;
		if (newIndividu == null) {
			newIndividu = create(ec);
			createAdrStable = true;
			createAdrEtud = true;
			createTelPar = true;
			createTelEtud = true;
			createTelEtudMob = true;
		}
		else {
			// Vérification de ce qui peut exister déjà, à ne pas recréer si c'est le cas...
			NSArray<EORepartPersonneAdresse> adrStable = newIndividu.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_PAR);
			if (adrStable == null || adrStable.count() == 0) {
				createAdrStable = true;
			}
			NSArray<EORepartPersonneAdresse> adrEtud = newIndividu.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_ETUD);
			if (adrEtud == null || adrEtud.count() == 0) {
				createAdrEtud = true;
			}

			NSArray<EOPersonneTelephone> telPar = newIndividu.toPersonneTelephones(QUAL_TELEPHONE_PARENTS);
			if (telPar == null || telPar.count() == 0) {
				createTelPar = true;
			}
			NSArray<EOPersonneTelephone> telEtud = newIndividu.toPersonneTelephones(QUAL_TELEPHONE_ETUDIANT);
			if (telEtud == null || telEtud.count() == 0) {
				createTelEtud = true;
			}
			NSArray<EOPersonneTelephone> telEtudMob = newIndividu.toPersonneTelephones(QUAL_MOBILE_ETUDIANT);
			if (telEtudMob == null || telEtudMob.count() == 0) {
				createTelEtudMob = true;
			}
		}
		// RepartPersonneAdresse
		if (createAdrStable) {
			FactoryRepartPersonneAdresse.createAdresseStable(ec, (EOPreCandidat) null, newIndividu);
		}
		if (createAdrEtud) {
			FactoryRepartPersonneAdresse.createAdresseUniversitaire(ec, (EOPreCandidat) null, newIndividu);
		}

		// PersonneTelephone
		if (createTelPar) {
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephonePAR(ec, null));
		}
		if (createTelEtud) {
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephoneEtudiant(ec, null));
		}
		if (createTelEtudMob) {
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephoneEtudiantMOB(ec, null));
		}

		// Compte
		newIndividu.addToToComptesRelationship(FactoryCompte.create(ec));

		return newIndividu;
	}

	/**
	 * Cree un nouvel EOIndividuUlr copie conforme du EOPreIndividu.<BR>
	 * Des relations sont egalement creees en cascade (si elles existent dans EOPreIndividu) :
	 * <UL>
	 * <LI>RepartPersonneAdresse (depuis PreRepartAdresse)
	 * <LI>PersonneTelephone (depuis PrePersonneTelephone)
	 * </UL>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param preIndividu
	 *            OBLIGATOIRE : Le EOPreIndividu a recopier (si NULL, retourne un EOIndividuUlr vide).
	 * @return un EOIndividu
	 */
	public static EOIndividu create(EOEditingContext ec, EOPreIndividu preIndividu) {
		EOIndividu newIndividu = create(ec);

		if (preIndividu != null) {
			newIndividu.setDNaissance(preIndividu.dNaissance());
			newIndividu.setNomAffichage(preIndividu.nomAffichage());
			newIndividu.setNomPatronymique(preIndividu.nomPatronymique());
			newIndividu.setNomUsuel(preIndividu.nomUsuel());
			newIndividu.setNomPatronymiqueAffichage(preIndividu.nomPatronymiqueAffichage());
			newIndividu.setPrenom(preIndividu.prenom());
			newIndividu.setPrenom2(preIndividu.prenom2());
			newIndividu.setPrenomAffichage(preIndividu.prenomAffichage());

			newIndividu.setToCiviliteRelationship(preIndividu.toFwkpers_Civilite());
			newIndividu.setToDepartementRelationship(preIndividu.toFwkpers_Departement());
			newIndividu.setVilleDeNaissance(preIndividu.villeDeNaissance());
			newIndividu.setToPaysNaissanceRelationship(preIndividu.toFwkpers_Pays_Naissance());
			newIndividu.setToPaysNationaliteRelationship(preIndividu.toFwkpers_Pays_Nationalite());

			newIndividu.setToSituationFamilialeRelationship(preIndividu.toFwkpers_SituationFamiliale());
			newIndividu.setToSituationMilitaireRelationship(preIndividu.toFwkpers_SituationMilitaire());

			newIndividu.setIndActivite(preIndividu.indActivite());
			newIndividu.setIndCleInsee(preIndividu.indCleInsee());
			newIndividu.setIndNoInsee(preIndividu.indNoInsee());
			newIndividu.setIndCleInseeProv(preIndividu.indCleInseeProv());
			newIndividu.setIndNoInseeProv(preIndividu.indNoInseeProv());
			newIndividu.setPriseCptInsee(preIndividu.priseCptInsee());
			newIndividu.setIndPhoto(preIndividu.indPhoto());
			newIndividu.setIndQualite(preIndividu.indQualite());
			newIndividu.setTemValide(preIndividu.temValide());

			// RepartPersonneAdresse depuis PreRepartAdresse
			if (preIndividu.toPreRepartAdresses() != null) {
				for (int i = 0; i < preIndividu.toPreRepartAdresses().count(); i++) {
					EOPreRepartAdresse preRPA = (EOPreRepartAdresse) preIndividu.toPreRepartAdresses().objectAtIndex(i);
					FactoryRepartPersonneAdresse.create(ec, preRPA, newIndividu);
				}
			}

			// PersonneTelephone depuis PrePersonneTelephone
			if (preIndividu.toPrePersonneTelephones() != null) {
				for (int i = 0; i < preIndividu.toPrePersonneTelephones().count(); i++) {
					EOPrePersonneTelephone prePT = (EOPrePersonneTelephone) preIndividu.toPrePersonneTelephones().objectAtIndex(i);
					newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.create(ec, prePT));
				}
			}
		}
		return newIndividu;
	}

	/**
	 * Cree un nouvel EOIndividuUlr a partir du EOPreCandidat.<BR>
	 * Des relations sont egalement creees en cascade (si les infos existent dans EOPreCandidat) :
	 * <UL>
	 * <LI>RepartPersonneAdresse
	 * <LI>PersonneTelephone
	 * </UL>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param preCandidat
	 *            OBLIGATOIRE : Le EOPreCandidat d'ou on prend les valeurs.
	 * @return un EOIndividu
	 */
	public static EOIndividu create(EOEditingContext ec, EOPreCandidat preCandidat) {
		EOIndividu newIndividu = create(ec);

		if (preCandidat != null) {
			// newIndividu.setDNaissance(preCandidat.candDateNais().timestampByAddingGregorianUnits(0, 0, 0, 12, 0, 0));
			newIndividu.setDNaissance(preCandidat.candDateNais());
			newIndividu.setNomAffichage(preCandidat.candNom());
			newIndividu.setNomPatronymique(preCandidat.candNom());
			newIndividu.setNomUsuel(preCandidat.candNom());
			newIndividu.setNomPatronymiqueAffichage(preCandidat.candNom());
			newIndividu.setPrenom(preCandidat.candPrenom());
			newIndividu.setPrenom2(preCandidat.candPrenom2());
			newIndividu.setPrenomAffichage(preCandidat.candPrenom());
			newIndividu.setIndPhoto(null);

			newIndividu.setToCiviliteRelationship(preCandidat.toFwkpers_Civilite());
			newIndividu.setToDepartementRelationship(preCandidat.toFwkpers_Departement_Naissance());
			if (newIndividu.toDepartement() == null || newIndividu.toDepartement().cDepartement() == null) {
				newIndividu.setToDepartementRelationship(null);
			}
			newIndividu.setVilleDeNaissance(preCandidat.candComnais());
			newIndividu.setToPaysNaissanceRelationship(preCandidat.toFwkpers_Pays_Naissance());
			if (newIndividu.toPaysNaissance() == null || newIndividu.toPaysNaissance().cPays() == null) {
				newIndividu.setToPaysNaissanceRelationship(EOPays.getPaysDefaut(ec));
			}
			newIndividu.setToPaysNationaliteRelationship(preCandidat.toFwkpers_Pays_Nationalite());
			if (newIndividu.toPaysNationalite() == null || newIndividu.toPaysNationalite().cPays() == null) {
				newIndividu.setToPaysNationaliteRelationship(EOPays.getPaysDefaut(ec));
			}

			// RepartPersonneAdresse depuis les infos dans PreCandidat
			FactoryRepartPersonneAdresse.createAdresseStable(ec, preCandidat, newIndividu);
			FactoryRepartPersonneAdresse.createAdresseUniversitaire(ec, preCandidat, newIndividu);

			// PersonneTelephone depuis les infos dans PreCandidat
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephonePAR(ec, preCandidat.candTelParent()));
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephoneEtudiant(ec, preCandidat.candTelScol()));
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephoneEtudiantMOB(ec, preCandidat.candPortScol()));
		}
		return newIndividu;
	}

	/**
	 * Cree un nouvel EOIndividuUlr a partir du EOCandidatGrhum.<BR>
	 * Des relations sont egalement creees en cascade (si les infos existent dans EOCandidatGrhum) :
	 * <UL>
	 * <LI>RepartPersonneAdresse
	 * <LI>PersonneTelephone
	 * </UL>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param candidatGrhum
	 *            OBLIGATOIRE : Le EOCandidatGrhum d'ou on prend les valeurs.
	 * @return un EOIndividu
	 */
	public static EOIndividu create(EOEditingContext ec, EOCandidatGrhum candidatGrhum) {
		EOIndividu newIndividu = create(ec);

		if (candidatGrhum != null) {
			newIndividu.setDNaissance(candidatGrhum.candDateNais());
			newIndividu.setNomAffichage(candidatGrhum.candNom());
			newIndividu.setNomPatronymique(candidatGrhum.candNomMarital());
			newIndividu.setNomUsuel(candidatGrhum.candNom());
			newIndividu.setNomPatronymiqueAffichage(candidatGrhum.candNom());
			newIndividu.setPrenom(candidatGrhum.candPrenom());
			newIndividu.setPrenom2(candidatGrhum.candPrenom2());
			newIndividu.setPrenomAffichage(candidatGrhum.candPrenom());
			newIndividu.setIndPhoto(null);

			newIndividu.setToCiviliteRelationship(candidatGrhum.toFwkpers_Civilite());
			newIndividu.setToDepartementRelationship(candidatGrhum.toFwkpers_Departement_Naissance());
			if (newIndividu.toDepartement() == null || newIndividu.toDepartement().cDepartement() == null) {
				newIndividu.setToDepartementRelationship(null);
			}
			newIndividu.setVilleDeNaissance(candidatGrhum.candComnais());
			newIndividu.setToPaysNaissanceRelationship(candidatGrhum.toFwkpers_Pays_Naissance());
			if (newIndividu.toPaysNaissance() == null || newIndividu.toPaysNaissance().cPays() == null) {
				newIndividu.setToPaysNaissanceRelationship(EOPays.getPaysDefaut(ec));
			}
			newIndividu.setToPaysNationaliteRelationship(candidatGrhum.toFwkpers_Pays_Nationalite());
			if (newIndividu.toPaysNationalite() == null || newIndividu.toPaysNationalite().cPays() == null) {
				newIndividu.setToPaysNationaliteRelationship(EOPays.getPaysDefaut(ec));
			}

			// RepartPersonneAdresse depuis les infos dans CandidatGrhum
			FactoryRepartPersonneAdresse.createAdresseStable(ec, candidatGrhum, newIndividu);
			FactoryRepartPersonneAdresse.createAdresseUniversitaire(ec, candidatGrhum, newIndividu);

			// PersonneTelephone depuis les infos dans CandidatGrhum
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephonePAR(ec, candidatGrhum.candTelParent()));
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephoneEtudiant(ec, candidatGrhum.candTelScol()));
			newIndividu.addToToPersonneTelephonesRelationship(FactoryPersonneTelephone.createTelephoneEtudiantMOB(ec,
					candidatGrhum.candPortableScol()));
		}
		return newIndividu;
	}

	/**
	 * Donne l'adresse stable (adresse parents) de cet individu.
	 * 
	 * @return Un EOAdresse
	 */
	public static EOAdresse adresseStable(EOIndividu individu) {
		if (individu == null) {
			return null;
		}
		EOAdresse adresseStable = null;
		NSArray<EORepartPersonneAdresse> repartAdresses = individu.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_PAR);
		if (repartAdresses != null && repartAdresses.count() > 0) {
			adresseStable = repartAdresses.lastObject().toAdresse();
		}
		return adresseStable;
	}

	/**
	 * Donne l'adresse universitaire (adresse etudiante) de cet individu.
	 * 
	 * @return Un EOAdresse
	 */
	public static EOAdresse adresseUniversitaire(EOIndividu individu) {
		if (individu == null) {
			return null;
		}
		EOAdresse adresseUniversitaire = null;
		NSArray<EORepartPersonneAdresse> repartAdresses = individu.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_ETUD);
		if (repartAdresses != null && repartAdresses.count() > 0) {
			adresseUniversitaire = repartAdresses.lastObject().toAdresse();
		}
		return adresseUniversitaire;
	}

	/**
	 * Donne le telephone stable (telephone parents) de cet individu.
	 * 
	 * @return Un EOPersonneTelephone
	 */
	public static EOPersonneTelephone telephoneStable(EOIndividu individu) {
		if (individu == null) {
			return null;
		}
		EOPersonneTelephone telephoneStable = null;
		NSArray<EOPersonneTelephone> personneTelephones = individu.toPersonneTelephones(QUAL_TELEPHONE_PARENTS);

		if (personneTelephones != null && personneTelephones.count() > 0) {
			telephoneStable = personneTelephones.lastObject();
		}
		return telephoneStable;
	}

	/**
	 * Donne le telephone universitaire fixe (etudiant) de cet individu.
	 * 
	 * @return Un EOPersonneTelephone
	 */
	public static EOPersonneTelephone telephoneUniversitaireFixe(EOIndividu individu) {
		if (individu == null) {
			return null;
		}
		EOPersonneTelephone telephoneUniversitaireFixe = null;
		NSArray<EOPersonneTelephone> personneTelephones = individu.toPersonneTelephones(QUAL_TELEPHONE_ETUDIANT);

		if (personneTelephones != null && personneTelephones.count() > 0) {
			telephoneUniversitaireFixe = personneTelephones.lastObject();
		}
		return telephoneUniversitaireFixe;
	}

	/**
	 * Donne le telephone universitaire mobile (portable etudiant) de cet individu.
	 * 
	 * @return Un EOPersonneTelephone
	 */
	public static EOPersonneTelephone telephoneUniversitairePortable(EOIndividu individu) {
		if (individu == null) {
			return null;
		}
		EOPersonneTelephone telephoneUniversitairePortable = null;
		NSArray<EOPersonneTelephone> personneTelephones = individu.toPersonneTelephones(QUAL_MOBILE_ETUDIANT);

		if (personneTelephones != null && personneTelephones.count() > 0) {
			telephoneUniversitairePortable = personneTelephones.lastObject();
		}
		return telephoneUniversitairePortable;
	}

}
