/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.impression;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;
import org.cocktail.scolarix.serveur.finder.FinderParametre;
import org.cocktail.scolarix.serveur.metier.eos.EOVSituationDetail;
import org.cocktail.scolarix.serveur.metier.eos.EOVSituationGrhum;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSPathUtilities;
import com.webobjects.jdbcadaptor.JDBCContext;

public class PrintFactory {
	public static final String JASPER_DOSSIER_INSCRIPTION = "dossier_inscription.jasper";
	public static final String JASPER_DOSSIER_PRE_INSCRIPTION = "dossier_pre_inscription.jasper";
	public static final String JASPER_CERTIFICATS = "dossier_certificats.jasper";
	public static final String JASPER_CERTIFICATS_DUPLICATA = "dossier_certificats_duplicata.jasper";
	public static final String JASPER_SITUATION = "dossier_situation.jasper";
	public static final String JASPER_SITUATION_DETAIL = "dossier_situation_detail.jasper";
	public static final String JASPER_BOURSIERS_PAR_FILIERE = "scola_bourse_filiere.jasper";
	public static final String JASPER_BOURSIERS_PAR_DOMAINE = "scola_bourse_domaine.jasper";
	public static final String JASPER_BOURSIERS_GLOBALE = "scola_bourse_globale.jasper";
	public static final String JASPER_ADM_LISTE = "scola_admlist.jasper";
	public static final String JASPER_ADM_RESULTAT = "scola_admresult_admis.jasper";
	public static final String JASPER_RDV_LISTE = "scola_rdv_liste.jasper";
	public static final String JASPER_DOSSIER_ECHEANCIER = "dossier_echeancier.jasper";
	public static final String JASPER_LISTE_RESULTATS = "liste_resultats.jasper";

	public static NSData printDossierDePreInscription(CktlWebSession session, String cRne, Integer etudNumero) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		boolean isAnneeCivile = FinderParametre.getAnneeCivile(session.defaultEditingContext());
		aMap.put("P_GARNUCHE_ETUD_NUMERO", etudNumero);
		aMap.put("P_GARNUCHE_ANNEE_CIVILE", isAnneeCivile ? "OUI" : "NON");

		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_DOSSIER_PRE_INSCRIPTION), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printDossierInscription(CktlWebSession session, String cRne, Integer etudNumero, Integer anneeScolaire) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		boolean isAnneeCivile = FinderParametre.getAnneeCivile(session.defaultEditingContext());
		aMap.put("P_GARNUCHE_ETUD_NUMERO", etudNumero);
		aMap.put("P_GARNUCHE_ANNEE_CIVILE", isAnneeCivile ? "OUI" : "NON");
		aMap.put("P_HIST_ANNEE_SCOL", anneeScolaire);

		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_DOSSIER_INSCRIPTION), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printCertificats(CktlWebSession session, String cRne, Integer histNumero, Date dateInsc, Date dateEffetInsc,
			Date datEffetCouverture, BigDecimal cotisationMontant, String cumulatif, Integer signaturePersId) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("HIST_NUMERO", histNumero);
		aMap.put("DATE_INSC", dateInsc);
		aMap.put("DATE_EFFET_INSC", dateEffetInsc);
		aMap.put("DATE_EFFET_COUVERTURE", datEffetCouverture);
		aMap.put("COTISATION_MONTANT", cotisationMontant);
		aMap.put("CUMULATIF", cumulatif);
		aMap.put("SIGNATURE_PERS_ID", signaturePersId);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_CERTIFICATS), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printCertificatsDuplicata(CktlWebSession session, String cRne, Integer histNumero, Date dateInsc, Date dateEffetInsc,
			Date datEffetCouverture, BigDecimal cotisationMontant, String cumulatif, Integer signaturePersId) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("HIST_NUMERO", histNumero);
		aMap.put("DATE_INSC", dateInsc);
		aMap.put("DATE_EFFET_INSC", dateEffetInsc);
		aMap.put("DATE_EFFET_COUVERTURE", datEffetCouverture);
		aMap.put("COTISATION_MONTANT", cotisationMontant);
		aMap.put("CUMULATIF", cumulatif);
		aMap.put("SIGNATURE_PERS_ID", signaturePersId);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_CERTIFICATS_DUPLICATA), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printSituation(CktlWebSession session, String cRne, Integer etudNumero, String idiplNumeros,
			List<EOVSituationGrhum> objects, String universite, String ville, String scolAffichage, String responsable, String responsabilite)
			throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("ETUD_NUMERO", etudNumero);
		aMap.put("IDIPL_NUMEROS", idiplNumeros);
		aMap.put("OBJECTS", objects);
		aMap.put("UNIVERSITE", universite);
		aMap.put("VILLE", ville);
		aMap.put("SCOL_AFFICHAGE", scolAffichage);
		aMap.put("RESPONSABLE", responsable);
		aMap.put("RESPONSABILITE", responsabilite);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_SITUATION), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printSituationDetail(CktlWebSession session, String cRne, Integer etudNumero, String idiplNumeros,
			List<EOVSituationDetail> objects, String universite, String ville, String scolAffichage, String responsable, String responsabilite)
			throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("ETUD_NUMERO", etudNumero);
		aMap.put("IDIPL_NUMEROS", idiplNumeros);
		aMap.put("OBJECTS", objects);
		aMap.put("UNIVERSITE", universite);
		aMap.put("VILLE", ville);
		aMap.put("SCOL_AFFICHAGE", scolAffichage);
		aMap.put("RESPONSABLE", responsable);
		aMap.put("RESPONSABILITE", responsabilite);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_SITUATION_DETAIL), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printBoursiersParFiliere(CktlWebSession session, String cRne, Integer anneeScol) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("SCOL_EXE", anneeScol.longValue());
		aMap.put("CA_RNE", cRne);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_BOURSIERS_PAR_FILIERE), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printBoursiersParDomaine(CktlWebSession session, String cRne, Integer anneeScol, Date dateDeb, Date dateFin)
			throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("SCOL_EXE", anneeScol.longValue());
		aMap.put("CA_RNE", cRne);
		aMap.put("CA_DATE_DEBUT", dateDeb);
		aMap.put("CA_DATE_FIN", dateFin);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_BOURSIERS_PAR_DOMAINE), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printBoursiersGlobal(CktlWebSession session, String cRne, Integer anneeScol, Date dateDeb, Date dateFin) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("SCOL_EXE", anneeScol);
		aMap.put("CA_RNE", cRne);
		aMap.put("CA_DATE_DEBUT", dateDeb);
		aMap.put("CA_DATE_FIN", dateFin);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_BOURSIERS_GLOBALE), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printAdmListe(CktlWebSession session, String cRne, Integer anneeScol, String scolCycle) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("SCOL_EXE", anneeScol.longValue());
		aMap.put("SCOL_CYCLE", scolCycle);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_ADM_LISTE), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printAdmListeResultat(CktlWebSession session, String cRne, Integer anneeScol, String scolCycle) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("SCOL_EXE", anneeScol.longValue());
		aMap.put("SCOL_CYCLE", scolCycle);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_ADM_RESULTAT), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printRdvListe(CktlWebSession session, String cRne, Integer numOccPlanning, String convocDateString) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("NUM_OCC_PLANNING", numOccPlanning);
		aMap.put("CONVOC_DATE_STRING", convocDateString);
		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_RDV_LISTE), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printEcheancier(CktlWebSession session, String cRne, Integer paieNumero, Integer anneeScolaire) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		boolean isAnneeCivile = FinderParametre.getAnneeCivile(session.defaultEditingContext());
		aMap.put("PAIE_NUMERO", paieNumero);
		aMap.put("ANNEE_SCOLAIRE", anneeScolaire);
		aMap.put("IS_ANNEE_CIVILE", isAnneeCivile ? "OUI" : "NON");

		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_DOSSIER_ECHEANCIER), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	public static NSData printResultats(CktlWebSession session, String cRne, String universite, String ville, Integer dspeCode,
			Integer idiplAnneeSuivie, Integer histAnneeScol, String orderByKeys, String libelleDiplome) throws Throwable {
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("UNIVERSITE", universite);
		aMap.put("VILLE", ville);
		aMap.put("DSPE_CODE", dspeCode);
		aMap.put("IDIPL_ANNEE_SUIVIE", idiplAnneeSuivie);
		aMap.put("HIST_ANNEE_SCOL", histAnneeScol);
		aMap.put("ORDER_BY_KEYS", orderByKeys);
		aMap.put("LIBELLE_DIPLOME", libelleDiplome);

		JrxmlReporter reporter = new JrxmlReporter();
		return reporter.printNow(null, getJDBCConnection(), null, getReportLocation(session, cRne, JASPER_LISTE_RESULTATS), aMap,
				JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
	}

	//

	public static WOActionResults afficherPdf(NSData pdfData, String fileName) {
		CktlDataResponse resp = new CktlDataResponse();
		if (pdfData != null) {
			resp.setContent(pdfData, CktlDataResponse.MIME_PDF);
			if (fileName != null) {
				resp.setFileName(fileName);
			}
		}
		else {
			resp.setContent("");
			resp.setHeader("0", "Content-Length");
		}
		return resp.generateResponse();
	}

	public static String getReportLocation(CktlWebSession session, String cRne, String fileName) {
		CktlWebApplication app = ((CktlWebApplication) session.application());
		String customReportsLocation = app.config().stringForKey("REPORTS_LOCATION");
		String reportPath = null;
		if (!StringCtrl.isEmpty(customReportsLocation)) {
			reportPath = customReportsLocation + "/" + fileName;
			File reportSansRne = new File(reportPath);
			if (!reportSansRne.exists()) {
				reportPath = null;
			}
			if (cRne != null) {
				String fileNameWithRne = NSPathUtilities.stringByDeletingPathExtension(fileName) + "_" + cRne + ".jasper";
				String reportPathWithRne = customReportsLocation + "/" + fileNameWithRne;
				File reportWithRne = new File(reportPathWithRne);
				if (reportWithRne.exists()) {
					reportPath = reportPathWithRne;
				}
			}
		}
		if (reportPath == null) {
			URL url = urlForJasperReport("report/" + fileName);
			if (cRne != null) {
				fileName = NSPathUtilities.stringByDeletingPathExtension(fileName);
				String reportSpecifiqueName = fileName + "_" + cRne + ".jasper";
				java.net.URL urlSpecifique = urlForJasperReport("report/" + reportSpecifiqueName);
				if (urlSpecifique != null) {
					url = urlSpecifique;
				}
			}
			if (url != null) {
				reportPath = url.getPath();
			}
		}
		return reportPath;
	}

	private static java.net.URL urlForJasperReport(String reportName) {
		java.net.URL urlJasperReport = NSBundle.mainBundle().pathURLForResourcePath(reportName);
		NSArray<NSBundle> frameworkBundles = NSBundle.frameworkBundles();
		Enumeration<NSBundle> enumFrameworkBundles = frameworkBundles.objectEnumerator();
		while (enumFrameworkBundles.hasMoreElements() && urlJasperReport == null) {
			NSBundle bundle = enumFrameworkBundles.nextElement();
			urlJasperReport = bundle.pathURLForResourcePath(reportName);
		}

		return urlJasperReport;
	}

	private static Connection getJDBCConnection() {
		return ((JDBCContext) CktlDataBus.databaseContext().availableChannel().adaptorChannel().adaptorContext()).connection();
	}
}
