/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOFraudeur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureInsFraudeur {

	private static final String PROCEDURE_NAME = "FwkScolarix_insFraudeur";

	/**
	 * Appele la procedure de creation d'un fraudeur.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param fraudeur
	 *            EOFraudeur qui sera enregistre
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOFraudeur fraudeur) throws NSValidation.ValidationException {
		if (fraudeur == null) {
			throw new ScolarixFwkException("Fraudeur a enregistrer NULL !");
		}
		boolean ok;
		try {
			ok = dataBus.executeProcedure(ProcedureInsFraudeur.PROCEDURE_NAME,
					ProcedureInsFraudeur.construireDictionnaire(dataBus.editingContext(), fraudeur));
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir du fraudeur.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param fraudeur
	 *            EOFraudeur pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, EOFraudeur fraudeur) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (fraudeur == null) {
			return dico;
		}
		dico.takeValueForKey(fraudeur.frauNom(), "0010fraunom");
		dico.takeValueForKey(fraudeur.frauPrenom(), "0020frauprenom");
		dico.takeValueForKey(fraudeur.frauDateNais(), "0030fraudatenais");
		dico.takeValueForKey(fraudeur.frauBordereau(), "0040fraubordereau");
		dico.takeValueForKey(fraudeur.frauRaison(), "0050frauraison");
		dico.takeValueForKey(fraudeur.frauSanction(), "0060frausanction");
		dico.takeValueForKey(fraudeur.frauDateDebut(), "0070fraudatedebut");
		dico.takeValueForKey(fraudeur.frauDateFin(), "0080fraudatefin");
		if (fraudeur.toEtudiant() != null) {
			dico.takeValueForKey(fraudeur.toEtudiant().etudNumero(), "0090etudnumero");
		}
		if (fraudeur.toInscDipl() != null) {
			dico.takeValueForKey(fraudeur.toInscDipl().idiplNumero(), "0100idiplnumero");
		}
		return dico;
	}
}