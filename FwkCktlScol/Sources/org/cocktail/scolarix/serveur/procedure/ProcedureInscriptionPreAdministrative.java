/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.procedure;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureInscriptionPreAdministrative {

	private static final String PROCEDURE_NAME = "FwkScolarix_inscriptionPreAdministrative";

	/**
	 * Appele la procedure de creation/modification d'une pre-inscription/pre-re-inscription administrative.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param etudiant
	 *            EOEtudiant qui sera enregistre
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOEtudiant etudiant) throws NSValidation.ValidationException {
		if (etudiant == null) {
			throw new ScolarixFwkException("Etudiant a enregistrer NULL !");
		}
		if (!etudiant.isPreInscription() && !etudiant.isPreReInscription()) {
			throw new ScolarixFwkException("La procedure " + PROCEDURE_NAME
					+ " est reservee aux pre-inscriptions et pre-re-inscriptions (l'etudiant passe est en inscription)");
		}
		boolean ok;
		try {
			etudiant.validateForSave();
			ok = dataBus.executeProcedure(ProcedureInscriptionPreAdministrative.PROCEDURE_NAME,
					ProcedureInscriptionPreAdministrative.construireDictionnaire(dataBus.editingContext(), etudiant));
			if (ok && etudiant.etudNumero() == null) {
				etudiant.setEtudNumero((Integer) dataBus.executedProcResult().valueForKey("0010etudnumero"));
			}
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'etudiant.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param etudiant
	 *            EOEtudiant pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, EOEtudiant etudiant) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (etudiant == null) {
			return dico;
		}

		// recup de l'annee...
		Integer annee = etudiant.anneeInscriptionEnCours();
		if (annee == null) {
			throw new ScolarixFwkException(
					"ProcedureInscriptionPreAdministrative : Impossible de determiner l'annee de pre/re-inscription en cours !");
		}

		// recup de l'historique pour cette annee...
		EOHistorique historique = etudiant.historique(annee);
		if (historique == null) {
			throw new ScolarixFwkException(
					"ProcedureInscriptionPreAdministrative : Impossible de retrouver un historique pour l'annee de pre/re-inscription !");
		}

		// recup de l'individu...
		EOIndividu individu = etudiant.individu();
		if (individu == null) {
			throw new ScolarixFwkException("ProcedureInscriptionPreAdministrative : Impossible de retrouver un individu associe a l'etudiant !");
		}

		// recup de la ou des inscription(s) pour cette annee...
		NSArray inscriptions = etudiant.formationsEnvisagees();

		// Construction du dico...

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(etudiant.etudNumero(), "0010etudnumero");
		dico.takeValueForKey(annee, "0020histanneescol");
		dico.takeValueForKey(etudiant.etudCodeIne(), "0030etudcodeine");
		dico.takeValueForKey(etudiant.candNumero(), "0035candnumero");

		// Partie A...
		if (individu.toCivilite() != null) {
			dico.takeValueForKey(individu.toCivilite().cCivilite(), "0040adrcivilite");
		}
		dico.takeValueForKey(individu.nomPatronymiqueAffichage(), "0050adrnom");
		dico.takeValueForKey(individu.prenomAffichage(), "0060adrprenom");
		dico.takeValueForKey(individu.prenom2(), "0070adrprenom2");
		if (individu.nomAffichage() != null && individu.nomAffichage().equals(individu.nomPatronymiqueAffichage())) {
			dico.takeValueForKey(null, "0080etudnommarital");
		}
		else {
			dico.takeValueForKey(individu.nomAffichage(), "0080etudnommarital");
		}
		dico.takeValueForKey(individu.dNaissance(), "0090etuddatenaissance");
		dico.takeValueForKey(individu.villeDeNaissance(), "0100etudcomnais");
		if (individu.toDepartement() != null) {
			dico.takeValueForKey(individu.toDepartement().cDepartement(), "0110dptgcodenais");
		}
		if (individu.toPaysNaissance() != null) {
			dico.takeValueForKey(individu.toPaysNaissance().cPays(), "0120payscodenais");
		}
		if (individu.toPaysNationalite() != null) {
			dico.takeValueForKey(individu.toPaysNationalite().cPays(), "0130natordre");
		}
		if (etudiant.toSitFamEtudiant() != null) {
			dico.takeValueForKey(etudiant.toSitFamEtudiant().etudSitfam(), "0140etudsitfam");
		}
		dico.takeValueForKey(historique.histSalarie(), "0150histsalarie");
		if (historique.toVEtablissementSalarie() != null) {
			dico.takeValueForKey(historique.toVEtablissementSalarie().cRne(), "0153histsalarierne");
		}
		dico.takeValueForKey(historique.histSalarieLibelle(), "0156histsalarielibelle");

		// Partie B...
		if (etudiant.telephoneStable() != null) {
			dico.takeValueForKey(etudiant.telephoneStable().noTelephone(), "0170botnumeroparent");
		}
		if (etudiant.adresseStable() != null) {
			dico.takeValueForKey(etudiant.adresseStable().adrAdresse1(), "0180adradresse1parent");
			dico.takeValueForKey(etudiant.adresseStable().adrAdresse2(), "0190adradresse2parent");
			dico.takeValueForKey(etudiant.adresseStable().codePostal(), "0200adrcpparent");
			if (etudiant.adresseStable().toPays() != null) {
				dico.takeValueForKey(etudiant.adresseStable().toPays().cPays(), "0210adrpaysparent");
			}
			if (etudiant.toFwkpers_Departement_Parent() != null) {
				dico.takeValueForKey(etudiant.toFwkpers_Departement_Parent().cDepartement(), "0220dptgcodeparent");
			}
			dico.takeValueForKey(etudiant.adresseStable().ville(), "023adrvilleparent");
		}
		if (etudiant.telephoneUniversitaireFixe() != null) {
			dico.takeValueForKey(etudiant.telephoneUniversitaireFixe().noTelephone(), "0250botnumeroscol");
		}
		if (etudiant.telephoneUniversitairePortable() != null) {
			dico.takeValueForKey(etudiant.telephoneUniversitairePortable().noTelephone(), "0260botportablescol");
		}
		dico.takeValueForKey(etudiant.email(), "0270adremailscol");
		if (etudiant.adresseUniversitaire() != null) {
			dico.takeValueForKey(etudiant.adresseUniversitaire().adrAdresse1(), "0280adradresse1scol");
			dico.takeValueForKey(etudiant.adresseUniversitaire().adrAdresse2(), "0290adradresse2scol");
			dico.takeValueForKey(etudiant.adresseUniversitaire().codePostal(), "0300adrcpscol");
			if (etudiant.adresseUniversitaire().toPays() != null) {
				dico.takeValueForKey(etudiant.adresseUniversitaire().toPays().cPays(), "0310adrpaysscol");
			}
			dico.takeValueForKey(etudiant.adresseUniversitaire().ville(), "0320adrvillescol");
		}
		if (etudiant.toTypeHebergement() != null) {
			dico.takeValueForKey(etudiant.toTypeHebergement().thebCode(), "0330thebcode");
		}

		// Partie C...
		dico.takeValueForKey(historique.histRedouble(), "0340histredouble");
		dico.takeValueForKey("00", "0350cumcode");
		if (historique.toRneAutEtab() != null) {
			dico.takeValueForKey(historique.toRneAutEtab().cRne(), "0360etabcodeautetab");
		}
		dico.takeValueForKey(historique.histLibelleAutEtab(), "0370histlibelleautetab");
		if (historique.toFwkpers_Departement_AutEtab() != null) {
			dico.takeValueForKey(historique.toFwkpers_Departement_AutEtab().cDepartement(), "0380dptgcodeautetab");
		}
		dico.takeValueForKey(historique.histVilleAutEtab(), "0390histvilleautetab");

		// Partie C : 1er diplome (inscription principale)...
		if (inscriptions.count() > 0) {
			EOScolFormationHabilitation scolFormationHabilitation = (EOScolFormationHabilitation) inscriptions.objectAtIndex(0);
			dico.takeValueForKey(null, "0400idiplnumero");
			if (scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation() != null) {
				dico.takeValueForKey(scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation().fspnKey(), "0410dspecode");
			}
			dico.takeValueForKey(scolFormationHabilitation.fhabNiveau(), "0420idiplanneesuivie");
			dico.takeValueForKey(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_PRINCIPALE, "0430idipltypeinscription");
			dico.takeValueForKey(DateCtrl.now(), "0440idipldateinsc");
			dico.takeValueForKey((scolFormationHabilitation.isPassageConditionnel() != null
					&& scolFormationHabilitation.isPassageConditionnel().booleanValue() ? "O" : "N"), "0450idiplpassageconditionnel");
			dico.takeValueForKey(null, "0460mparkey");
			dico.takeValueForKey(null, "0470grpdnumero");
		}

		// Partie C : 2eme diplome (inscription complementaire 1)...
		if (inscriptions.count() > 1) {
			EOScolFormationHabilitation scolFormationHabilitation = (EOScolFormationHabilitation) inscriptions.objectAtIndex(1);
			dico.takeValueForKey(null, "0480idiplnumero2");
			if (scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation() != null) {
				dico.takeValueForKey(scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation().fspnKey(), "0490dspecode2");
			}
			dico.takeValueForKey(scolFormationHabilitation.fhabNiveau(), "0500idiplanneesuivie2");
			dico.takeValueForKey(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_COMPLEMENTAIRE, "0510idipltypeinscription2");
			dico.takeValueForKey(DateCtrl.now().timestampByAddingGregorianUnits(0, 0, 0, 0, 2, 0), "0520idipldateinsc2");
			dico.takeValueForKey((scolFormationHabilitation.isPassageConditionnel() != null
					&& scolFormationHabilitation.isPassageConditionnel().booleanValue() ? "O" : "N"), "0530idiplpassageconditionnel2");
			dico.takeValueForKey(null, "0540mparkey2");
			dico.takeValueForKey(null, "0550grpdnumero2");
		}

		// Partie C : 3eme diplome (inscription complementaire 2)...
		if (inscriptions.count() > 2) {
			EOScolFormationHabilitation scolFormationHabilitation = (EOScolFormationHabilitation) inscriptions.objectAtIndex(2);
			dico.takeValueForKey(null, "0560idiplnumero3");
			if (scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation() != null) {
				dico.takeValueForKey(scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation().fspnKey(), "0570dspecode3");
			}
			dico.takeValueForKey(scolFormationHabilitation.fhabNiveau(), "0580idiplanneesuivie3");
			dico.takeValueForKey(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_COMPLEMENTAIRE, "0590idipltypeinscription3");
			dico.takeValueForKey(DateCtrl.now().timestampByAddingGregorianUnits(0, 0, 0, 0, 4, 0), "0600idipldateinsc3");
			dico.takeValueForKey((scolFormationHabilitation.isPassageConditionnel() != null
					&& scolFormationHabilitation.isPassageConditionnel().booleanValue() ? "O" : "N"), "0610idiplpassageconditionnel3");
			dico.takeValueForKey(null, "0620mparkey3");
			dico.takeValueForKey(null, "0630grpdnumero3");
		}

		// Type d'echange... null par defaut dans notre cas...
		dico.takeValueForKey(null, "0640techcode");
		// Etudiant inscrit aux modules FLE ? Non=0, Oui=1 ... ici NON!
		dico.takeValueForKey(new Integer(0), "0650mstacode");

		// Partie D...
		if (etudiant.toBac() != null) {
			dico.takeValueForKey(etudiant.toBac().bacCode(), "0660baccode");
		}
		if (etudiant.toMentionBac() != null) {
			dico.takeValueForKey(etudiant.toMentionBac().mentCode(), "0670mentcode");
		}
		dico.takeValueForKey(etudiant.etudAnbac(), "0680etudanbac");
		if (etudiant.toRneCodeBac() != null) {
			dico.takeValueForKey(etudiant.toRneCodeBac().cRne(), "0690etabcodebac");
		}
		dico.takeValueForKey(etudiant.etudVilleBac(), "0700etudvillebac");
		if (etudiant.toFwkpers_Departement_EtabBac() != null) {
			dico.takeValueForKey(etudiant.toFwkpers_Departement_EtabBac().cDepartement(), "0710dptgetabbac");
		}
		if (etudiant.toFwkpers_Pays_EtabBac() != null) {
			dico.takeValueForKey(etudiant.toFwkpers_Pays_EtabBac().cPays(), "0720paysetabbac");
		}
		if (etudiant.toFwkpers_Academie() != null) {
			dico.takeValueForKey(etudiant.toFwkpers_Academie().cAcademie(), "0730acadcodebac");
		}

		// Partie E...
		dico.takeValueForKey(etudiant.etudAnnee1InscUniv(), "0740etudannee1inscuniv");
		if (etudiant.toRneCodeSup() != null) {
			dico.takeValueForKey(etudiant.toRneCodeSup().cRne(), "0750etabcodesup");
		}
		dico.takeValueForKey(etudiant.etudAnnee1InscUlr(), "0760etudannee1insculr");
		dico.takeValueForKey(etudiant.etudAnnee1InscSup(), "0770etudannee1inscsup");

		// Partie F...
		if (historique.toTypeDiplome() != null) {
			dico.takeValueForKey(historique.toTypeDiplome().tdiplCode(), "0780tdiplcodederdipl");
		}
		dico.takeValueForKey(historique.histAnneeDerDipl(), "0790histanneederdipl");
		dico.takeValueForKey(historique.histLibelleDerDipl(), "0800histlibellederdipl");
		if (historique.toRneDerDipl() != null) {
			dico.takeValueForKey(historique.toRneDerDipl().cRne(), "0810etabcodederdipl");
		}
		if (historique.toFwkpers_Departement_DerDipl() != null) {
			dico.takeValueForKey(historique.toFwkpers_Departement_DerDipl().cDepartement(), "0820dptgcodederdipl");
		}
		if (historique.toFwkpers_Pays_DerDipl() != null) {
			dico.takeValueForKey(historique.toFwkpers_Pays_DerDipl().cPays(), "0830payscodederdipl");
		}
		dico.takeValueForKey(historique.histVilleDerDipl(), "0840histvillederdipl");

		// Partie G...
		dico.takeValueForKey(historique.histAnneeDerEtab(), "0850histanneederetab");
		dico.takeValueForKey(historique.histEnsDerEtab(), "0860histensderetab");
		if (historique.toRneDerEtab() != null) {
			dico.takeValueForKey(historique.toRneDerEtab().cRne(), "0870etabcodederetab");
		}
		dico.takeValueForKey(historique.histLibelleDerEtab(), "0880histlibellederetab");
		dico.takeValueForKey(historique.histVilleDerEtab(), "0890histvillederetab");
		if (historique.toFwkpers_Departement_DerEtab() != null) {
			dico.takeValueForKey(historique.toFwkpers_Departement_DerEtab().cDepartement(), "0900dptgcodederetab");
		}
		if (historique.toFwkpers_Pays_DerEtab() != null) {
			dico.takeValueForKey(historique.toFwkpers_Pays_DerEtab().cPays(), "0910payscodederetab");
		}
		if (historique.toSituationScol() != null) {
			dico.takeValueForKey(historique.toSituationScol().situCode(), "0920situcode");
		}
		if (historique.toDptSituation() != null) {
			dico.takeValueForKey(historique.toDptSituation().dsituCode(), "0930dsitucode");
		}

		// Partie H...
		dico.takeValueForKey(historique.histBourse(), "0940histbourse");
		if (historique.histBourse() != null
				&& (historique.histBourse().intValue() == EOHistorique.NON_BOURSIER || historique.histBourse().intValue() == EOHistorique.BOURSIER_DEMANDE_EN_COURS)) {
			dico.takeValueForKey(null, "0950histnumeroalloc");
			dico.takeValueForKey(null, "0960histacadbourse");
			dico.takeValueForKey(null, "0970histautrecasexo");
			dico.takeValueForKey(null, "0980histechbourse");
		}
		else {
			dico.takeValueForKey(historique.histNumeroAlloc(), "0950histnumeroalloc");
			dico.takeValueForKey(historique.histAcadBourse(), "0960histacadbourse");
			dico.takeValueForKey(historique.histAutreCasExo(), "0970histautrecasexo");
			dico.takeValueForKey(historique.histEchBourse(), "0980histechbourse");
		}

		// Partie I...
		dico.takeValueForKey(individu.indNoInsee(), "0990indnoinsee");
		dico.takeValueForKey(individu.indCleInsee(), "1000indcleinsee");
		if (individu.indNoInsee() == null && individu.indNoInseeProv() == null) {
			dico.takeValueForKey("P", "1003prisecptinsee");
		}
		else {
			dico.takeValueForKey(individu.priseCptInsee(), "1003prisecptinsee");
		}
		dico.takeValueForKey(individu.indNoInseeProv(), "1005indnoinseeprov");
		dico.takeValueForKey(individu.indCleInseeProv(), "1007indcleinseeprov");
		dico.takeValueForKey(historique.histTuteur1(), "1008histtuteur1");
		dico.takeValueForKey(historique.histTuteur2(), "1009histtuteur2");
		dico.takeValueForKey(historique.histActivProf(), "1010histactivprof");
		dico.takeValueForKey(historique.histLibelleProf(), "1020histlibelleprof");
		dico.takeValueForKey(historique.histAffss(), "1030histaffss");
		if (historique.toMutuelleOrga() != null) {
			dico.takeValueForKey(historique.toMutuelleOrga().mutCode(), "1040mutcodeorga");
		}
		dico.takeValueForKey(historique.histAyantDroit(), "1050histayantdroit");
		dico.takeValueForKey(historique.cotiCode(), "1060coticode");
		if (historique.histAffss() == null || historique.histAffss().intValue() != 0) {
			dico.takeValueForKey(historique.histTypeRegime(), "1065histtyperegime");
		}
		if (historique.toOrigineRessources() != null) {
			dico.takeValueForKey(historique.toOrigineRessources().oresCode(), "1070orescode");
		}
		if (historique.toQuotiteTravail() != null) {
			dico.takeValueForKey(historique.toQuotiteTravail().qtraCode(), "1080qtracode");
		}
		dico.takeValueForKey(historique.proCodeEtud(), "1090procodeetud");
		if (historique.toActiviteProfessionnelle() != null) {
			dico.takeValueForKey(historique.toActiviteProfessionnelle().activCode(), "1100activcode");
		}
		dico.takeValueForKey(historique.dactCode(), "1110dactcode");

		// Partie J...
		dico.takeValueForKey(historique.histAdhAgme(), "1120histadhagme");
		dico.takeValueForKey(historique.histAccesInfo(), "1130histaccesinfo");
		if (historique.toMutuelleMut() != null) {
			dico.takeValueForKey(historique.toMutuelleMut().mutCode(), "1140mutcodemut");
		}

		// Partie K...
		if (historique.toLangue() != null) {
			dico.takeValueForKey(historique.toLangue().langCode(), "1150langcode");
		}
		dico.takeValueForKey(historique.histDemEmpl(), "1160histdemempl");
		dico.takeValueForKey(historique.histForProf(), "1170histforprof");
		if (historique.toTypeRegimeSise() != null) {
			dico.takeValueForKey(historique.toTypeRegimeSise().histTypeFormation(), "1180histtypeformation");
		}
		dico.takeValueForKey(historique.histIntEtud(), "1190histintetud");
		dico.takeValueForKey(historique.histDureeInt(), "1200histdureeint");
		dico.takeValueForKey(historique.histTeleens(), "1210histteleens");
		dico.takeValueForKey(etudiant.etudSportHn(), "1220etudsporthn");
		if (individu.toSituationMilitaire() != null) {
			dico.takeValueForKey(individu.toSituationMilitaire().cSitMilitaire(), "1230etudservmil");
		}
		dico.takeValueForKey(etudiant.etudSnAttestation(), "1240etudsnattestation");
		dico.takeValueForKey(etudiant.etudSnCertification(), "1250etudsncertification");
		if (etudiant.toProfession() != null) {
			dico.takeValueForKey(etudiant.toProfession().proCode(), "1260procode");
		}
		if (etudiant.toProfession2() != null) {
			dico.takeValueForKey(etudiant.toProfession2().proCode(), "1265procode2");
		}
		if (historique.toTypeHandicap() != null) {
			dico.takeValueForKey(historique.toTypeHandicap().thanCode(), "1270thancode");
		}
		dico.takeValueForKey(historique.histAssuranceCivile(), "1275histassurancecivile");
		dico.takeValueForKey(historique.histAssuranceCivileCie(), "1276histassurancecivilecie");
		dico.takeValueForKey(historique.histAssuranceCivileNum(), "1277histassurancecivilenum");

		// Infos complementaires (non tirees du dossier)...
		// dico.takeValueForKey(etudiant.charte() ? "O" : "N", "1280cptcharte");
		// dico.takeValueForKey(etudiant.listeRouge() ? "O" : "N", "1290adrlisterouge");
		// dico.takeValueForKey(etudiant.photo() ? "O" : "N", "1300adrphoto");
		dico.takeValueForKey(etudiant.charte(), "1280cptcharte");
		dico.takeValueForKey(etudiant.listeRouge(), "1290adrlisterouge");
		dico.takeValueForKey(etudiant.photo(), "1300adrphoto");
		dico.takeValueForKey(historique.histResteBu(), "1310histrestebu");
		dico.takeValueForKey(historique.histRemarques(), "1320idiplobservations");

		return dico;
	}
}