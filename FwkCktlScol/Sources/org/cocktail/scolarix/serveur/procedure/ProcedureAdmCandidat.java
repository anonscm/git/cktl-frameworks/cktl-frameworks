/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.procedure;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureAdmCandidat {

	private static final String PROCEDURE_NAME = "FwkScolarix_admCandidat";

	/**
	 * Appele la procedure de creation/modification d'une admission.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param etudiant
	 *            EOEtudiant qui sera enregistre en tant que candidat
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOEtudiant etudiant) throws NSValidation.ValidationException {
		if (etudiant == null) {
			throw new ScolarixFwkException("Etudiant a enregistrer NULL !");
		}
		if (!etudiant.isAdmission()) {
			throw new ScolarixFwkException("La procedure " + PROCEDURE_NAME + " est reservee aux admissions (l'etudiant passe est en... autre chose)");
		}
		boolean ok;
		try {
			ok = dataBus.executeProcedure(ProcedureAdmCandidat.PROCEDURE_NAME,
					ProcedureAdmCandidat.construireDictionnaire(dataBus.editingContext(), etudiant));
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			if (etudiant.etudNumero() == null) {
				Integer annee = etudiant.anneeInscriptionEnCours();
				EOHistorique historique = etudiant.historique(annee);
				historique.setHistNumero(null);
			}
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'etudiant.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param etudiant
	 *            EOEtudiant pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, EOEtudiant etudiant) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (etudiant == null) {
			return dico;
		}

		// recup de l'annee...
		Integer annee = etudiant.anneeInscriptionEnCours();
		if (annee == null) {
			throw new ScolarixFwkException("ProcedureAdmCandidat : Impossible de determiner l'annee d'admission en cours !");
		}

		// recup de l'historique pour cette annee...
		EOHistorique historique = etudiant.historique(annee);
		if (historique == null) {
			throw new ScolarixFwkException("ProcedureAdmCandidat : Impossible de retrouver un historique pour l'annee d'admission !");
		}

		// recup de l'individu...
		EOIndividu individu = etudiant.individu();
		if (individu == null) {
			throw new ScolarixFwkException("ProcedureAdmCandidat : Impossible de retrouver un individu associe a l'etudiant !");
		}

		EOCandidatGrhum candidat = etudiant.candidat();
		if (candidat == null) {
			throw new ScolarixFwkException("ProcedureAdmCandidat : Impossible de retrouver un candidat associe a l'etudiant !");
		}

		// Construction du dico...

		if (candidat.primaryKey() != null) {
			dico.takeValueForKey(new Integer(candidat.primaryKey()), "0010candnumero");
		}
		dico.takeValueForKey(candidat.candNumAdm(), "0020candnumadm");
		dico.takeValueForKey(annee, "0030candanneescol");
		if (candidat.toFwkScolarite_ScolFormationAdmission() != null) {
			dico.takeValueForKey(new Integer(candidat.toFwkScolarite_ScolFormationAdmission().primaryKey()), "0040candfadmkey");
		}
		if (candidat.toFwkScolarite_ScolFormationSpecialisation() != null) {
			dico.takeValueForKey(candidat.toFwkScolarite_ScolFormationSpecialisation().fspnKey(), "0050candfspnkey");
		}
		dico.takeValueForKey(candidat.candAnneeSuivie(), "0060candanneesuivie");

		dico.takeValueForKey(etudiant.etudCodeIne(), "0070candbea");
		dico.takeValueForKey(etudiant.etudNumero(), "0080etudnumero");
		if (individu.globalID().isTemporary()) {
			dico.takeValueForKey(null, "0090noindividu");
			dico.takeValueForKey(null, "0100persid");
		}
		else {
			dico.takeValueForKey(individu.noIndividu(), "0090noindividu");
			dico.takeValueForKey(individu.persId(), "0100persid");
		}

		// Partie A...
		if (individu.toCivilite() != null) {
			dico.takeValueForKey(individu.toCivilite().cCivilite(), "0110candcivilite");
		}
		dico.takeValueForKey(individu.nomPatronymiqueAffichage(), "0120candnom");
		dico.takeValueForKey(individu.prenomAffichage(), "0130candprenom");
		dico.takeValueForKey(individu.prenom2(), "0140candprenom2");
		if (individu.nomAffichage() != null && individu.nomAffichage().equals(individu.nomPatronymiqueAffichage())) {
			dico.takeValueForKey(null, "0150candnommarital");
		}
		else {
			dico.takeValueForKey(individu.nomAffichage(), "0150candnommarital");
		}
		dico.takeValueForKey(individu.dNaissance(), "0160canddatenaissance");
		dico.takeValueForKey(individu.villeDeNaissance(), "0170candcomnais");
		if (individu.toDepartement() != null) {
			dico.takeValueForKey(individu.toDepartement().cDepartement(), "0180dptgcodenais");
		}
		if (individu.toPaysNaissance() != null) {
			dico.takeValueForKey(individu.toPaysNaissance().cPays(), "0190payscodenais");
		}
		if (individu.toPaysNationalite() != null) {
			dico.takeValueForKey(new Integer(individu.toPaysNationalite().cPays()), "0200natordre");
		}
		if (etudiant.toSitFamEtudiant() != null) {
			dico.takeValueForKey(etudiant.toSitFamEtudiant().etudSitfam(), "0210candsitfam");
		}
		dico.takeValueForKey(historique.histSalarie(), "0220candsalarie");
		if (historique.toVEtablissementSalarie() != null) {
			dico.takeValueForKey(historique.toVEtablissementSalarie().cRne(), "0230candsalarierne");
		}
		dico.takeValueForKey(historique.histSalarieLibelle(), "0240candsalarielibelle");

		// Partie B...
		if (etudiant.telephoneStable() != null) {
			dico.takeValueForKey(etudiant.telephoneStable().noTelephone(), "0250candtelparent");
		}
		if (etudiant.adresseStable() != null) {
			dico.takeValueForKey(etudiant.adresseStable().adrAdresse1(), "0260candadr1parent");
			dico.takeValueForKey(etudiant.adresseStable().adrAdresse2(), "0270candadr2parent");
			dico.takeValueForKey(etudiant.adresseStable().codePostal(), "0280candcpparent");
			dico.takeValueForKey(etudiant.adresseStable().ville(), "0290candvilleparent");
			if (etudiant.adresseStable().toPays() != null) {
				dico.takeValueForKey(etudiant.adresseStable().toPays().cPays(), "0300candpaysparent");
			}
			if (etudiant.toFwkpers_Departement_Parent() != null) {
				dico.takeValueForKey(etudiant.toFwkpers_Departement_Parent().cDepartement(), "0310dptgcodeparent");
			}
		}
		if (etudiant.telephoneUniversitaireFixe() != null) {
			dico.takeValueForKey(etudiant.telephoneUniversitaireFixe().noTelephone(), "0320candtelscol");
		}
		if (etudiant.telephoneUniversitairePortable() != null) {
			dico.takeValueForKey(etudiant.telephoneUniversitairePortable().noTelephone(), "0330candportablescol");
		}
		dico.takeValueForKey(etudiant.email(), "0340candemailscol");
		if (etudiant.adresseUniversitaire() != null) {
			dico.takeValueForKey(etudiant.adresseUniversitaire().adrAdresse1(), "0350candadr1scol");
			dico.takeValueForKey(etudiant.adresseUniversitaire().adrAdresse2(), "0360candadr2scol");
			dico.takeValueForKey(etudiant.adresseUniversitaire().codePostal(), "0370candcpscol");
			dico.takeValueForKey(etudiant.adresseUniversitaire().ville(), "0380candvillescol");
			if (etudiant.adresseUniversitaire().toPays() != null) {
				dico.takeValueForKey(etudiant.adresseUniversitaire().toPays().cPays(), "0390candpaysscol");
			}
		}

		// Partie D...
		if (etudiant.toBac() != null) {
			dico.takeValueForKey(etudiant.toBac().bacCode(), "0400baccode");
		}
		if (etudiant.toMentionBac() != null) {
			dico.takeValueForKey(etudiant.toMentionBac().mentCode(), "0410mentcode");
		}
		dico.takeValueForKey(etudiant.etudAnbac(), "0420etudanbac");
		if (etudiant.toRneCodeBac() != null) {
			dico.takeValueForKey(etudiant.toRneCodeBac().cRne(), "0430etabcodebac");
		}
		dico.takeValueForKey(etudiant.etudVilleBac(), "0440candvillebac");
		if (etudiant.toFwkpers_Departement_EtabBac() != null) {
			dico.takeValueForKey(etudiant.toFwkpers_Departement_EtabBac().cDepartement(), "0450dptgetabbac");
		}
		if (etudiant.toFwkpers_Pays_EtabBac() != null) {
			dico.takeValueForKey(etudiant.toFwkpers_Pays_EtabBac().cPays(), "0460paysetabbac");
		}

		// Partie G...
		if (historique.toSituationScol() != null) {
			dico.takeValueForKey(historique.toSituationScol().situCode(), "0470situcode");
		}
		dico.takeValueForKey(historique.histAnneeDerEtab(), "0480candanneederetab");
		dico.takeValueForKey(historique.histEnsDerEtab(), "0490candensderetab");
		if (historique.toRneDerEtab() != null) {
			dico.takeValueForKey(historique.toRneDerEtab().cRne(), "0500etabcodederetab");
		}
		dico.takeValueForKey(historique.histLibelleDerEtab(), "0510candlibellederetab");
		dico.takeValueForKey(historique.histVilleDerEtab(), "0520candvillederetab");
		if (historique.toFwkpers_Departement_DerEtab() != null) {
			dico.takeValueForKey(historique.toFwkpers_Departement_DerEtab().cDepartement(), "0530dptgcodederetab");
		}
		if (historique.toFwkpers_Pays_DerEtab() != null) {
			dico.takeValueForKey(historique.toFwkpers_Pays_DerEtab().cPays(), "0540payscodederetab");
		}
		if (historique.toTypeClassePreparatoire() != null) {
			dico.takeValueForKey(historique.toTypeClassePreparatoire().tcpreCode(), "0550tcprecode");
		}

		// Infos complementaires (non tirees du dossier)...
		if (candidat.toDiplomePrepare() != null) {
			dico.takeValueForKey(candidat.toDiplomePrepare().dpreCode(), "0560dprecode");
		}
		if (candidat.toResultat() != null) {
			dico.takeValueForKey(candidat.toResultat().resCode(), "0570rescode");
		}
		dico.takeValueForKey(candidat.candListsup(), "0580candlistsup");
		dico.takeValueForKey(candidat.candRemarques(), "0590candremarques");
		dico.takeValueForKey(candidat.candEntretien(), "0600candentretien");
		dico.takeValueForKey(candidat.ocapiNumero(), "0610ocapinumero");

		return dico;
	}
}