/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureInsRemboursement {

	private static final String PROCEDURE_NAME = "FwkScolarix_insRemboursement";

	/**
	 * Appele la procedure de creation d'un remboursement.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param etudiantRembourse
	 *            EOEtudiantRembourse qui sera enregistre
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOEtudiantRembourse etudiantRembourse) throws NSValidation.ValidationException {
		if (etudiantRembourse == null) {
			throw new ScolarixFwkException("Remboursement a enregistrer NULL !");
		}
		boolean ok;
		try {
			// etudiantRembourse.validateForInsert();
			ok = dataBus.executeProcedure(ProcedureInsRemboursement.PROCEDURE_NAME,
					ProcedureInsRemboursement.construireDictionnaire(dataBus.editingContext(), etudiantRembourse));
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir du etudiantRembourse.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param etudiantRembourse
	 *            EOEtudiantRembourse pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, EOEtudiantRembourse etudiantRembourse) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (etudiantRembourse == null) {
			return dico;
		}

		if (etudiantRembourse.toHistorique() != null) {
			dico.takeValueForKey(etudiantRembourse.toHistorique().histNumero(), "0010histnumero");
		}
		dico.takeValueForKey(etudiantRembourse.toVEtablissementScolarite().cRne(), "0020etabcode");
		dico.takeValueForKey(etudiantRembourse.erembMotif(), "0030erembmotif");
		dico.takeValueForKey(etudiantRembourse.erembSomme(), "0040erembsomme");
		if (etudiantRembourse.toFwkpers_Rib() != null) {
			dico.takeValueForKey(Integer.valueOf(etudiantRembourse.toFwkpers_Rib().primaryKey()), "0050ribordre");
		}
		// ce paramètre n'est plus utilisé dans la procédure...
		dico.takeValueForKey(null, "0060idiplchaine");
		dico.takeValueForKey(etudiantRembourse.getDpaieChaine(), "0070dpaiechaine");

		return dico;
	}
}