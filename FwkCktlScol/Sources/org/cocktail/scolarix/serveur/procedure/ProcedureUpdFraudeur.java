/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOFraudeur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureUpdFraudeur {

	private static final String PROCEDURE_NAME = "FwkScolarix_updFraudeur";

	/**
	 * Appele la procedure de mise à jour d'un fraudeur.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param fraudeur
	 *            EOFraudeur qui sera modifié
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOFraudeur fraudeur) throws NSValidation.ValidationException {
		if (fraudeur == null) {
			throw new ScolarixFwkException("Fraudeur a modifier NULL !");
		}
		boolean ok;
		try {
			fraudeur.validateForUpdate();
			ok = dataBus.executeProcedure(ProcedureUpdFraudeur.PROCEDURE_NAME,
					ProcedureUpdFraudeur.construireDictionnaire(dataBus.editingContext(), fraudeur));
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir du fraudeur.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param fraudeur
	 *            EOFraudeur pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, EOFraudeur fraudeur) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (fraudeur == null) {
			return dico;
		}
		dico.takeValueForKey(fraudeur.getOldNomBeforeUpdate(), "0010fraunom");
		dico.takeValueForKey(fraudeur.getOldPrenomBeforeUpdate(), "0020frauprenom");
		dico.takeValueForKey(fraudeur.frauNom(), "0030newnom");
		dico.takeValueForKey(fraudeur.frauPrenom(), "0040newprenom");
		dico.takeValueForKey(fraudeur.frauDateNais(), "0050fraudatenais");
		dico.takeValueForKey(fraudeur.frauBordereau(), "0060fraubordereau");
		dico.takeValueForKey(fraudeur.frauRaison(), "0070frauraison");
		dico.takeValueForKey(fraudeur.frauSanction(), "0080frausanction");
		dico.takeValueForKey(fraudeur.frauDateDebut(), "0090fraudatedebut");
		dico.takeValueForKey(fraudeur.frauDateFin(), "0100fraudatefin");
		if (fraudeur.toEtudiant() != null) {
			dico.takeValueForKey(fraudeur.toEtudiant().etudNumero(), "0110etudnumero");
		}
		if (fraudeur.toInscDipl() != null) {
			dico.takeValueForKey(fraudeur.toInscDipl().idiplNumero(), "0120idiplnumero");
		}
		return dico;
	}
}