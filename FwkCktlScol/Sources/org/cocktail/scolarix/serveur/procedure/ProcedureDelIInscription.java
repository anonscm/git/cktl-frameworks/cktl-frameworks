package org.cocktail.scolarix.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureDelIInscription {
	private static final String PROCEDURE_NAME = "FwkScolarite_pLmdDelIInscription";

	/**
	 * Appele la procedure de suppression d'une Inscription.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param inscDipl
	 *            EOInscDipl qui sera supprimé
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOInscDipl inscDipl) throws NSValidation.ValidationException {
		if (inscDipl == null) {
			throw new ScolarixFwkException("InscDipl à supprimer NULL !");
		}
		boolean ok;
		try {
			inscDipl.validateForDelete();
			ok = dataBus.executeProcedure(ProcedureDelIInscription.PROCEDURE_NAME,
					ProcedureDelIInscription.construireDictionnaire(dataBus.editingContext(), inscDipl));
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir du inscDipl.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param inscDipl
	 *            EOInscDipl pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, EOInscDipl inscDipl) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (inscDipl == null) {
			return dico;
		}

		dico.takeValueForKey(inscDipl.idiplNumero(), "0010idiplnumero");
		dico.takeValueForKey(inscDipl.toHistorique().histAnneeScol(), "0020fannkey");

		return dico;
	}
}
