/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.procedure;

import java.util.Enumeration;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureUpdResultatAdmission {

	private static final String PROCEDURE_NAME = "FwkScolarix_updResultatAdmission";

	/**
	 * Appele la procedure de mise à jour des résultat admission.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param resultats
	 *            NSArray résultats à mettre à jour
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, NSArray<EOCandidatGrhum> resultats) throws NSValidation.ValidationException {
		if (resultats == null) {
			throw new ScolarixFwkException("Résultats à mettre à jour NULL !");
		}
		boolean ok;
		try {
			ok = dataBus.executeProcedure(ProcedureUpdResultatAdmission.PROCEDURE_NAME,
					ProcedureUpdResultatAdmission.construireDictionnaire(dataBus.editingContext(), resultats));
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir du paiement.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param resultats
	 *            NSArray résultats pour lesquels construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, NSArray<EOCandidatGrhum> resultats) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (resultats == null) {
			return dico;
		}

		dico.takeValueForKey(getChaine(resultats), "0010chaine1");

		return dico;
	}

	private static String getChaine(NSArray<EOCandidatGrhum> resultats) {
		// Construction de la chaine qui va bien...
		// Format : candnumero$rescode$candlistsup$ pour chaque candidat
		StringBuffer dpSB = new StringBuffer();
		if (resultats != null) {
			Enumeration<EOCandidatGrhum> res = resultats.objectEnumerator();
			while (res.hasMoreElements()) {
				EOCandidatGrhum cand = res.nextElement();
				// candnumero
				dpSB.append(cand.primaryKey());
				dpSB.append("$");
				// rescode
				if (cand.toResultat() != null) {
					dpSB.append(cand.toResultat().resCode());
				}
				dpSB.append("$");
				// candlistsup
				dpSB.append(cand.candListsup());
				dpSB.append("$");
			}
		}
		return dpSB.toString();
	}

}