/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureInsBordereauNew {

	private static final String PROCEDURE_NAME = "FwkScolarix_insBordereauNew";

	/**
	 * Appele la procedure de creation d'un bordereau.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param remboursements
	 *            NSArray de etudiantRembourse qui sera enregistre
	 * @param persId
	 *            le persId de l'agent qui réalise l'opération
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, NSArray<EOEtudiantRembourse> remboursements, Integer persId)
			throws NSValidation.ValidationException {
		if (remboursements == null) {
			throw new ScolarixFwkException("Bordereaux a enregistrer NULL !");
		}
		boolean ok;
		try {
			ok = dataBus.executeProcedure(ProcedureInsBordereauNew.PROCEDURE_NAME,
					ProcedureInsBordereauNew.construireDictionnaire(dataBus.editingContext(), remboursements, persId));
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir des remboursements.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param remboursements
	 *            NSArray de etudiantRembourse pour lequel construire le dictionnaire
	 * @param persId
	 *            le persId de l'agent qui réalise l'opération
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, NSArray<EOEtudiantRembourse> remboursements,
			Integer persId) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (remboursements == null || remboursements.count() == 0) {
			return dico;
		}

		dico.takeValueForKey(getErembChaine(ec, remboursements), "0010erembchaine");
		dico.takeValueForKey(remboursements.lastObject().toVEtablissementScolarite().cRne(), "0020etabcode");
		dico.takeValueForKey(persId, "0030persid");

		return dico;
	}

	private static String getErembChaine(EOEditingContext ec, NSArray<EOEtudiantRembourse> remboursements) {
		// Construction de la chaine des erembNumero qui va bien...
		// Format : erembnumero1$erembnumero2$erembnumero3... autant que d'étudiants à rembourser
		StringBuffer dpSB = new StringBuffer();
		if (remboursements != null) {
			for (EOEtudiantRembourse bordereau : remboursements) {
				dpSB.append(bordereau.primaryKey());
				dpSB.append("$");
			}
		}
		return dpSB.toString();
	}
}