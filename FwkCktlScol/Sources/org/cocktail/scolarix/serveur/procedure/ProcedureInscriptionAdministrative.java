/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.procedure;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureInscriptionAdministrative {

	private static final String PROCEDURE_NAME = "FwkScolarix_inscriptionAdministrative";

	/**
	 * Appele la procedure de creation/modification d'une inscription/re-inscription administrative.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param etudiant
	 *            EOEtudiant qui sera enregistre
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOEtudiant etudiant) throws NSValidation.ValidationException {
		if (etudiant == null) {
			throw new ScolarixFwkException("Etudiant a enregistrer NULL !");
		}
		if (!etudiant.isInscription() && !etudiant.isReInscription()) {
			throw new ScolarixFwkException("La procedure " + PROCEDURE_NAME
					+ " est reservee aux inscriptions et re-inscriptions (l'etudiant passe est en... autre chose)");
		}
		boolean ok;
		try {
			etudiant.validateForSave();
			ok = dataBus.executeProcedure(ProcedureInscriptionAdministrative.PROCEDURE_NAME,
					ProcedureInscriptionAdministrative.construireDictionnaire(dataBus.editingContext(), etudiant));
			if (ok && etudiant.etudNumero() == null) {
				Integer annee = etudiant.anneeInscriptionEnCours();
				EOHistorique historique = etudiant.historique(annee);
				EOIndividu individu = etudiant.individu();

				etudiant.setEtudCodeIne((String) dataBus.executedProcResult().valueForKey("0060etudcodeine"));
				historique.setHistNumero((Integer) dataBus.executedProcResult().valueForKey("0020histnumero"));
				individu.setNoIndividu((Integer) dataBus.executedProcResult().valueForKey("0080noindividu"));
				individu.setPersId((Integer) dataBus.executedProcResult().valueForKey("0090persid"));
			}
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			if (etudiant.etudNumero() == null) {
				Integer annee = etudiant.anneeInscriptionEnCours();
				EOHistorique historique = etudiant.historique(annee);
				historique.setHistNumero(null);
			}
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'etudiant.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param etudiant
	 *            EOEtudiant pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, EOEtudiant etudiant) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (etudiant == null) {
			return dico;
		}

		// recup de l'annee...
		Integer annee = etudiant.anneeInscriptionEnCours();
		if (annee == null) {
			throw new ScolarixFwkException(
					"ProcedureInscriptionAdministrative : Impossible de determiner l'annee de re-inscription/inscription en cours !");
		}

		// recup de l'historique pour cette annee...
		EOHistorique historique = etudiant.historique(annee);
		if (historique == null) {
			throw new ScolarixFwkException(
					"ProcedureInscriptionAdministrative : Impossible de retrouver un historique pour l'annee de re-inscription/inscription !");
		}

		// recup de l'individu...
		EOIndividu individu = etudiant.individu();
		if (individu == null) {
			throw new ScolarixFwkException("ProcedureInscriptionAdministrative : Impossible de retrouver un individu associe a l'etudiant !");
		}

		// recup de la ou des inscription(s) pour cette annee...
		NSArray inscriptions = etudiant.formationsEnvisagees();

		// Construction du dico...

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(etudiant.etudNumero(), "0010etudnumero");
		dico.takeValueForKey(historique.histNumero(), "0020histnumero");
		dico.takeValueForKey(etudiant.candNumero(), "0030candnumero");
		dico.takeValueForKey(annee, "0040histanneescol");
		dico.takeValueForKey(null, "0050ocapinumero");
		dico.takeValueForKey(etudiant.etudCodeIne(), "0060etudcodeine");
		dico.takeValueForKey(etudiant.etudReimmatriculation(), "0070etudreimmatriculation");
		// dans le cas de la ré-inscription (en nouvelle inscription) ou de la modification de dossier, on veut
		// récupérer les no_individu et pers_id existants, et ne pas utiliser ceux générés automatiquement par EOF...
		// donc on laisse la procédure les récupérer depuis etudiant...
		// dico.takeValueForKey(individu.noIndividu(), "0080noindividu");
		dico.takeValueForKey(null, "0080noindividu");
		// dico.takeValueForKey(individu.persId(), "0090persid");
		dico.takeValueForKey(null, "0090persid");

		// Partie A...
		if (individu.toCivilite() != null) {
			dico.takeValueForKey(individu.toCivilite().cCivilite(), "0100adrcivilite");
		}
		dico.takeValueForKey(individu.nomPatronymiqueAffichage(), "0110adrnom");
		dico.takeValueForKey(individu.prenomAffichage(), "0120adrprenom");
		dico.takeValueForKey(individu.prenom2(), "0130adrprenom2");
		if (individu.nomAffichage() != null && individu.nomAffichage().equals(individu.nomPatronymiqueAffichage())) {
			dico.takeValueForKey(null, "0140etudnommarital");
		}
		else {
			dico.takeValueForKey(individu.nomAffichage(), "0140etudnommarital");
		}
		dico.takeValueForKey(individu.dNaissance(), "0150etuddatenaissance");
		dico.takeValueForKey(individu.villeDeNaissance(), "0160etudcomnais");
		if (individu.toDepartement() != null) {
			dico.takeValueForKey(individu.toDepartement().cDepartement(), "0170dptgcodenais");
		}
		if (individu.toPaysNaissance() != null) {
			dico.takeValueForKey(individu.toPaysNaissance().cPays(), "0180payscodenais");
		}
		if (individu.toPaysNationalite() != null) {
			dico.takeValueForKey(individu.toPaysNationalite().cPays(), "0190natordre");
		}
		if (etudiant.toSitFamEtudiant() != null) {
			dico.takeValueForKey(etudiant.toSitFamEtudiant().etudSitfam(), "0200etudsitfam");
		}
		dico.takeValueForKey(historique.histSalarie(), "0210histsalarie");
		if (historique.toVEtablissementSalarie() != null) {
			dico.takeValueForKey(historique.toVEtablissementSalarie().cRne(), "0220histsalarierne");
		}
		dico.takeValueForKey(historique.histSalarieLibelle(), "0230histsalarielibelle");

		// Partie B...
		dico.takeValueForKey(null, "0240adrordre");
		if (etudiant.telephoneStable() != null) {
			dico.takeValueForKey(etudiant.telephoneStable().noTelephone(), "0250botnumeroparent");
		}
		if (etudiant.adresseStable() != null) {
			dico.takeValueForKey(etudiant.adresseStable().adrAdresse1(), "0260adradresse1parent");
			dico.takeValueForKey(etudiant.adresseStable().adrAdresse2(), "0270adradresse2parent");
			dico.takeValueForKey(etudiant.adresseStable().codePostal(), "0280adrcpparent");
			if (etudiant.adresseStable().toPays() != null) {
				dico.takeValueForKey(etudiant.adresseStable().toPays().cPays(), "0290adrpaysparent");
			}
			if (etudiant.toFwkpers_Departement_Parent() != null) {
				dico.takeValueForKey(etudiant.toFwkpers_Departement_Parent().cDepartement(), "0300dptgcodeparent");
			}
			dico.takeValueForKey(etudiant.adresseStable().ville(), "0310adrvilleparent");
		}
		if (etudiant.telephoneUniversitaireFixe() != null) {
			dico.takeValueForKey(etudiant.telephoneUniversitaireFixe().noTelephone(), "0320botnumeroscol");
		}
		if (etudiant.telephoneUniversitairePortable() != null) {
			dico.takeValueForKey(etudiant.telephoneUniversitairePortable().noTelephone(), "0330botportablescol");
		}
		dico.takeValueForKey(etudiant.email(), "0340adremailscol");
		if (etudiant.adresseUniversitaire() != null) {
			dico.takeValueForKey(etudiant.adresseUniversitaire().adrAdresse1(), "0350adradresse1scol");
			dico.takeValueForKey(etudiant.adresseUniversitaire().adrAdresse2(), "0360adradresse2scol");
			dico.takeValueForKey(etudiant.adresseUniversitaire().codePostal(), "0370adrcpscol");
			if (etudiant.adresseUniversitaire().toPays() != null) {
				dico.takeValueForKey(etudiant.adresseUniversitaire().toPays().cPays(), "0380adrpaysscol");
			}
			dico.takeValueForKey(etudiant.adresseUniversitaire().ville(), "0390adrvillescol");
		}
		if (etudiant.toTypeHebergement() != null) {
			dico.takeValueForKey(etudiant.toTypeHebergement().thebCode(), "0400thebcode");
		}

		// Partie C...
		dico.takeValueForKey(historique.histRedouble(), "0460histredouble");
		dico.takeValueForKey(null, "0470histnbinscdeug");
		dico.takeValueForKey(null, "0480hist1inscdeugdut");

		// les 4 valeurs suivantes sont renseignées plus bas après les 3 diplomes, selon si au moins une des
		// inscriptions
		// est cumulative ou non :
		// "0510etabcodeautetab", "0520histlibelleautetab", "0530dptgcodeautetab", "0540histvilleautetab"
		boolean isTypeInscriptionCumulative = false;

		// Etudiant inscrit aux modules FLE ? Non=0, Oui=1 ... ici NON!
		dico.takeValueForKey(new Integer(0), "0860mstacode");

		// Partie C : 1er diplome...
		if (inscriptions.count() > 0) {
			EOInscDipl inscription = (EOInscDipl) inscriptions.objectAtIndex(0);
			EOScolFormationHabilitation scolFormationHabilitation = null;
			if (inscription.scolFormationHabilitation() != null) {
				scolFormationHabilitation = inscription.scolFormationHabilitation();
			}

			dico.takeValueForKey(inscription.idiplNumero(), "0410idiplnumero");

			if (scolFormationHabilitation != null && scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation() != null) {
				dico.takeValueForKey(scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation().fspnKey(), "0420dspecode");
				dico.takeValueForKey(scolFormationHabilitation.fhabNiveau(), "0430idiplanneesuivie");
			}
			if (inscription.toResultat() != null) {
				dico.takeValueForKey(inscription.toResultat().resCode(), "0440rescode");
			}
			dico.takeValueForKey(inscription.idiplDateDemission(), "0450idipldatedemission");
			if (inscription.toTypeInscription() != null) {
				dico.takeValueForKey(inscription.toTypeInscription().idiplTypeInscription(), "0490idipltypeinscription");
				if (inscription.toTypeInscription().idiplTypeInscription().equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_CUMULATIF)) {
					isTypeInscriptionCumulative = true;
					if (inscription.toCumulatif() != null) {
						dico.takeValueForKey(inscription.toCumulatif().cumCode(), "0500cumcode");
					}
				}
			}
			dico.takeValueForKey(inscription.idiplDateInsc(), "0550idipldateinsc");

			dico.takeValueForKey((inscription.isPassageConditionnel() != null && inscription.isPassageConditionnel().booleanValue() ? "O" : "N"),
					"0560idiplpassageconditionnel");
			dico.takeValueForKey(null, "0570mparkey");
			dico.takeValueForKey(inscription.grpdNumero(), "0580grpdnumero");
			dico.takeValueForKey((inscription.isDiplomable() != null && inscription.isDiplomable().booleanValue() ? new Integer(1) : new Integer(0)),
					"0590idipldiplomable");
			dico.takeValueForKey(
					(inscription.isAmenagement() != null && inscription.isAmenagement().booleanValue() ? new Integer(1) : new Integer(0)),
					"0600idiplamenagement");
			// Type d'echange...
			if (inscription.toTypeEchange() != null) {
				dico.takeValueForKey(inscription.toTypeEchange().techCode(), "0850techcode");
			}
			// Etudiant inscrit aux modules FLE ? Non=0, Oui=1
			if (inscription.mstaCode() != null && inscription.mstaCode().intValue() > 0) {
				dico.takeValueForKey(inscription.mstaCode(), "0860mstacode");
			}
		}

		// Partie C : 2eme diplome...
		if (inscriptions.count() > 1) {
			EOInscDipl inscription = (EOInscDipl) inscriptions.objectAtIndex(1);
			EOScolFormationHabilitation scolFormationHabilitation = null;
			if (inscription.scolFormationHabilitation() != null) {
				scolFormationHabilitation = inscription.scolFormationHabilitation();
			}

			dico.takeValueForKey(inscription.idiplNumero(), "0610idiplnumero2");

			if (scolFormationHabilitation != null && scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation() != null) {
				dico.takeValueForKey(scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation().fspnKey(), "0620dspecode2");
				dico.takeValueForKey(scolFormationHabilitation.fhabNiveau(), "0630idiplanneesuivie2");
			}
			if (inscription.toResultat() != null) {
				dico.takeValueForKey(inscription.toResultat().resCode(), "0640rescode2");
			}
			dico.takeValueForKey(inscription.idiplDateDemission(), "0650idipldatedemission2");
			if (inscription.toTypeInscription() != null) {
				dico.takeValueForKey(inscription.toTypeInscription().idiplTypeInscription(), "0660idipltypeinscription2");
				if (inscription.toTypeInscription().idiplTypeInscription().equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_CUMULATIF)) {
					isTypeInscriptionCumulative = true;
					if (inscription.toCumulatif() != null) {
						dico.takeValueForKey(inscription.toCumulatif().cumCode(), "0500cumcode");
					}
				}
			}
			dico.takeValueForKey(inscription.idiplDateInsc(), "0670idipldateinsc2");

			dico.takeValueForKey((inscription.isPassageConditionnel() != null && inscription.isPassageConditionnel().booleanValue() ? "O" : "N"),
					"0680idiplpassageconditionnel2");
			dico.takeValueForKey(null, "0690mparkey2");
			dico.takeValueForKey(inscription.grpdNumero(), "0700grpdnumero2");
			dico.takeValueForKey((inscription.isDiplomable() != null && inscription.isDiplomable().booleanValue() ? new Integer(1) : new Integer(0)),
					"0710idipldiplomable2");
			dico.takeValueForKey(
					(inscription.isAmenagement() != null && inscription.isAmenagement().booleanValue() ? new Integer(1) : new Integer(0)),
					"0720idiplamenagement2");
			// Type d'echange...
			if (inscription.toTypeEchange() != null) {
				dico.takeValueForKey(inscription.toTypeEchange().techCode(), "0850techcode");
			}
			// Etudiant inscrit aux modules FLE ? Non=0, Oui=1
			if (inscription.mstaCode() != null && inscription.mstaCode().intValue() > 0) {
				dico.takeValueForKey(inscription.mstaCode(), "0860mstacode");
			}
		}

		// Partie C : 3eme diplome...
		if (inscriptions.count() > 2) {
			EOInscDipl inscription = (EOInscDipl) inscriptions.objectAtIndex(2);
			EOScolFormationHabilitation scolFormationHabilitation = null;
			if (inscription.scolFormationHabilitation() != null) {
				scolFormationHabilitation = inscription.scolFormationHabilitation();
			}

			dico.takeValueForKey(inscription.idiplNumero(), "0730idiplnumero3");

			if (scolFormationHabilitation != null && scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation() != null) {
				dico.takeValueForKey(scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation().fspnKey(), "0740dspecode3");
				dico.takeValueForKey(scolFormationHabilitation.fhabNiveau(), "0750idiplanneesuivie3");
			}
			if (inscription.toResultat() != null) {
				dico.takeValueForKey(inscription.toResultat().resCode(), "0760rescode3");
			}
			dico.takeValueForKey(inscription.idiplDateDemission(), "0770idipldatedemission3");
			if (inscription.toTypeInscription() != null) {
				dico.takeValueForKey(inscription.toTypeInscription().idiplTypeInscription(), "0780idipltypeinscription3");
				if (inscription.toTypeInscription().idiplTypeInscription().equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_CUMULATIF)) {
					isTypeInscriptionCumulative = true;
					if (inscription.toCumulatif() != null) {
						dico.takeValueForKey(inscription.toCumulatif().cumCode(), "0500cumcode");
					}
				}
			}
			dico.takeValueForKey(inscription.idiplDateInsc(), "0790idipldateinsc3");

			dico.takeValueForKey((inscription.isPassageConditionnel() != null && inscription.isPassageConditionnel().booleanValue() ? "O" : "N"),
					"0800idiplpassageconditionnel3");
			dico.takeValueForKey(null, "0810mparkey3");
			dico.takeValueForKey(inscription.grpdNumero(), "0820grpdnumero3");
			dico.takeValueForKey((inscription.isDiplomable() != null && inscription.isDiplomable().booleanValue() ? new Integer(1) : new Integer(0)),
					"0830idipldiplomable3");
			dico.takeValueForKey(
					(inscription.isAmenagement() != null && inscription.isAmenagement().booleanValue() ? new Integer(1) : new Integer(0)),
					"0840idiplamenagement3");
			// Type d'echange...
			if (inscription.toTypeEchange() != null) {
				dico.takeValueForKey(inscription.toTypeEchange().techCode(), "0850techcode");
			}
			// Etudiant inscrit aux modules FLE ? Non=0, Oui=1
			if (inscription.mstaCode() != null && inscription.mstaCode().intValue() > 0) {
				dico.takeValueForKey(inscription.mstaCode(), "0860mstacode");
			}
		}

		// cas spécifique des valeurs utilisées pour les infos supplémentaires liées à l'inscription cumulative...
		if (isTypeInscriptionCumulative) {
			if (historique.toRneAutEtab() != null) {
				dico.takeValueForKey(historique.toRneAutEtab().cRne(), "0510etabcodeautetab");
			}
			dico.takeValueForKey(historique.histLibelleAutEtab(), "0520histlibelleautetab");
			if (historique.toFwkpers_Departement_AutEtab() != null) {
				dico.takeValueForKey(historique.toFwkpers_Departement_AutEtab().cDepartement(), "0530dptgcodeautetab");
			}
			dico.takeValueForKey(historique.histVilleAutEtab(), "0540histvilleautetab");
		}

		// Partie D...
		if (etudiant.toBac() != null) {
			dico.takeValueForKey(etudiant.toBac().bacCode(), "0870baccode");
		}
		if (etudiant.toMentionBac() != null) {
			dico.takeValueForKey(etudiant.toMentionBac().mentCode(), "0880mentcode");
		}
		dico.takeValueForKey(etudiant.etudAnbac(), "0890etudanbac");
		if (etudiant.toRneCodeBac() != null) {
			dico.takeValueForKey(etudiant.toRneCodeBac().cRne(), "0900etabcodebac");
		}
		dico.takeValueForKey(etudiant.etudVilleBac(), "0910etudvillebac");
		if (etudiant.toFwkpers_Departement_EtabBac() != null) {
			dico.takeValueForKey(etudiant.toFwkpers_Departement_EtabBac().cDepartement(), "0920dptgetabbac");
		}
		if (etudiant.toFwkpers_Pays_EtabBac() != null) {
			dico.takeValueForKey(etudiant.toFwkpers_Pays_EtabBac().cPays(), "0930paysetabbac");
		}
		if (etudiant.toFwkpers_Departement_EtabBac() != null && etudiant.toFwkpers_Departement_EtabBac().toAcademie() != null) {
			dico.takeValueForKey(etudiant.toFwkpers_Departement_EtabBac().toAcademie().cAcademie(), "0940acadcodebac");
		}

		// Partie E...
		dico.takeValueForKey(etudiant.etudAnnee1InscUniv(), "0950etudannee1inscuniv");
		if (etudiant.toRneCodeSup() != null) {
			dico.takeValueForKey(etudiant.toRneCodeSup().cRne(), "0960etabcodesup");
		}
		dico.takeValueForKey(etudiant.etudAnnee1InscUlr(), "0970etudannee1insculr");
		dico.takeValueForKey(etudiant.etudAnnee1InscSup(), "0980etudannee1inscsup");

		// Partie F...
		if (historique.toTypeDiplome() != null) {
			dico.takeValueForKey(historique.toTypeDiplome().tdiplCode(), "0990tdiplcodederdipl");
		}
		dico.takeValueForKey(historique.histAnneeDerDipl(), "1000histanneederdipl");
		dico.takeValueForKey(historique.histLibelleDerDipl(), "1010histlibellederdipl");
		if (historique.toRneDerDipl() != null) {
			dico.takeValueForKey(historique.toRneDerDipl().cRne(), "1020etabcodederdipl");
		}
		if (historique.toFwkpers_Departement_DerDipl() != null) {
			dico.takeValueForKey(historique.toFwkpers_Departement_DerDipl().cDepartement(), "1030dptgcodederdipl");
		}
		if (historique.toFwkpers_Pays_DerDipl() != null) {
			dico.takeValueForKey(historique.toFwkpers_Pays_DerDipl().cPays(), "1040payscodederdipl");
		}
		dico.takeValueForKey(historique.histVilleDerDipl(), "1050histvillederdipl");

		// Partie G...
		dico.takeValueForKey(historique.histAnneeDerEtab(), "1060histanneederetab");
		dico.takeValueForKey(historique.histEnsDerEtab(), "1070histensderetab");
		if (historique.toRneDerEtab() != null) {
			dico.takeValueForKey(historique.toRneDerEtab().cRne(), "1080etabcodederetab");
		}
		dico.takeValueForKey(historique.histLibelleDerEtab(), "1090histlibellederetab");
		dico.takeValueForKey(historique.histVilleDerEtab(), "1100histvillederetab");
		if (historique.toFwkpers_Departement_DerEtab() != null) {
			dico.takeValueForKey(historique.toFwkpers_Departement_DerEtab().cDepartement(), "1110dptgcodederetab");
		}
		if (historique.toFwkpers_Pays_DerEtab() != null) {
			dico.takeValueForKey(historique.toFwkpers_Pays_DerEtab().cPays(), "1120payscodederetab");
		}
		if (historique.toSituationScol() != null) {
			dico.takeValueForKey(historique.toSituationScol().situCode(), "1130situcode");
		}
		if (historique.toDptSituation() != null) {
			dico.takeValueForKey(historique.toDptSituation().dsituCode(), "1140dsitucode");
		}
		if (historique.toTypeClassePreparatoire() != null) {
			dico.takeValueForKey(historique.toTypeClassePreparatoire().tcpreCode(), "1150tcprecode");
		}

		// Partie H...
		dico.takeValueForKey(historique.histBourse(), "1160histbourse");
		if (historique.histBourse() != null
				&& (historique.histBourse().intValue() == EOHistorique.NON_BOURSIER || historique.histBourse().intValue() == EOHistorique.BOURSIER_DEMANDE_EN_COURS)) {
			dico.takeValueForKey(null, "1170histnumeroalloc");
			dico.takeValueForKey(null, "1180histacadbourse");
			dico.takeValueForKey(null, "1190histautrecasexo");
			dico.takeValueForKey(null, "1200histechbourse");
		}
		else {
			dico.takeValueForKey(historique.histNumeroAlloc(), "1170histnumeroalloc");
			dico.takeValueForKey(historique.histAcadBourse(), "1180histacadbourse");
			dico.takeValueForKey(historique.histAutreCasExo(), "1190histautrecasexo");
			dico.takeValueForKey(historique.histEchBourse(), "1200histechbourse");
		}

		// Partie I...
		if (individu.priseCptInsee() == null || individu.priseCptInsee().equalsIgnoreCase("R")) {
			dico.takeValueForKey(individu.indNoInsee(), "1210indnoinsee");
			dico.takeValueForKey(individu.indCleInsee(), "1220indcleinsee");
		}
		else {
			dico.takeValueForKey(individu.indNoInseeProv(), "1210indnoinsee");
			dico.takeValueForKey(individu.indCleInseeProv(), "1220indcleinsee");
		}
		if (individu.indNoInsee() == null && individu.indNoInseeProv() == null) {
			dico.takeValueForKey("P", "1225prisecptinsee");
		}
		else {
			dico.takeValueForKey(individu.priseCptInsee(), "1225prisecptinsee");
		}
		dico.takeValueForKey(historique.histTuteur1(), "1227histtuteur1");
		dico.takeValueForKey(historique.histTuteur2(), "1228histtuteur2");
		dico.takeValueForKey(historique.histActivProf(), "1230histactivprof");
		dico.takeValueForKey(historique.histLibelleProf(), "1240histlibelleprof");
		dico.takeValueForKey(historique.histAffss(), "1250histaffss");
		if (historique.toMutuelleOrga() != null) {
			dico.takeValueForKey(historique.toMutuelleOrga().mutCode(), "1260mutcodeorga");
		}
		dico.takeValueForKey(historique.histAyantDroit(), "1270histayantdroit");
		dico.takeValueForKey(historique.cotiCode(), "1280coticode");
		if (historique.histAffss() == null || historique.histAffss().intValue() != 0) {
			dico.takeValueForKey(historique.histTypeRegime(), "1285histtyperegime");
		}
		if (historique.toOrigineRessources() != null) {
			dico.takeValueForKey(historique.toOrigineRessources().oresCode(), "1290orescode");
		}
		if (historique.toQuotiteTravail() != null) {
			dico.takeValueForKey(historique.toQuotiteTravail().qtraCode(), "1300qtracode");
		}
		dico.takeValueForKey(historique.proCodeEtud(), "1310procodeetud");
		if (historique.toActiviteProfessionnelle() != null) {
			dico.takeValueForKey(historique.toActiviteProfessionnelle().activCode(), "1320activcode");
		}
		dico.takeValueForKey(historique.dactCode(), "1330dactcode");

		// Partie J...
		dico.takeValueForKey(historique.histAdhAgme(), "1340histadhagme");
		dico.takeValueForKey(historique.histAccesInfo(), "1350histaccesinfo");
		if (historique.toMutuelleMut() != null) {
			dico.takeValueForKey(historique.toMutuelleMut().mutCode(), "1360mutcodemut");
		}

		// Partie K...
		if (historique.toLangue() != null) {
			dico.takeValueForKey(historique.toLangue().langCode(), "1370langcode");
		}
		dico.takeValueForKey(historique.histDemEmpl(), "1380histdemempl");
		dico.takeValueForKey(historique.histForProf(), "1390histforprof");
		if (historique.toTypeRegimeSise() != null) {
			dico.takeValueForKey(historique.toTypeRegimeSise().histTypeFormation(), "1400histtypeformation");
		}
		dico.takeValueForKey(historique.histIntEtud(), "1410histintetud");
		dico.takeValueForKey(historique.histDureeInt(), "1420histdureeint");
		dico.takeValueForKey(historique.histTeleens(), "1430histteleens");
		dico.takeValueForKey(etudiant.etudSportHn(), "1440etudsporthn");
		if (individu.toSituationMilitaire() != null) {
			dico.takeValueForKey(individu.toSituationMilitaire().cSitMilitaire(), "1450etudservmil");
		}
		dico.takeValueForKey(etudiant.etudSnAttestation(), "1460etudsnattestation");
		dico.takeValueForKey(etudiant.etudSnCertification(), "1470etudsncertification");
		if (etudiant.toProfession() != null) {
			dico.takeValueForKey(etudiant.toProfession().proCode(), "1480procode");
		}
		if (etudiant.toProfession2() != null) {
			dico.takeValueForKey(etudiant.toProfession2().proCode(), "1490procode2");
		}
		if (historique.toTypeHandicap() != null) {
			dico.takeValueForKey(historique.toTypeHandicap().thanCode(), "1500thancode");
		}
		if (historique.toTypeDispense() != null) {
			dico.takeValueForKey(historique.toTypeDispense().tdisCode(), "1510tdiscode");
		}
		dico.takeValueForKey(historique.histAssuranceCivile(), "1520histassurancecivile");
		dico.takeValueForKey(historique.histAssuranceCivileCie(), "1530histassurancecivilecie");
		dico.takeValueForKey(historique.histAssuranceCivileNum(), "1540histassurancecivilenum");

		// Infos complementaires (non tirees du dossier)...
		dico.takeValueForKey(etudiant.charte(), "1550cptcharte");
		dico.takeValueForKey("N", "1560cptconnexion");
		dico.takeValueForKey(etudiant.listeRouge(), "1570adrlisterouge");
		dico.takeValueForKey(etudiant.photo(), "1580adrphoto");
		dico.takeValueForKey(historique.histResteBu(), "1590histrestebu");
		dico.takeValueForKey(historique.histTransfert(), "1600histtransfert");
		dico.takeValueForKey(historique.histDateTransfert(), "1610histdatetransfert");
		dico.takeValueForKey(historique.dspeCodeFutur(), "1620dspecodefutur");
		dico.takeValueForKey(historique.histAnneeDiplFutur(), "1630histanneediplfutur");
		if (historique.toRneTrans() != null) {
			dico.takeValueForKey(historique.toRneTrans().cRne(), "1640etabcodetrans");
		}

		dico.takeValueForKey(historique.histRemarques(), "1650idiplobservations");

		return dico;
	}
}