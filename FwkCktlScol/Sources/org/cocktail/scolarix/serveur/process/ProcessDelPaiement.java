/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.process;

import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOPaiement;
import org.cocktail.scolarix.serveur.procedure.ProcedureDelPaiement;

import com.webobjects.eocontrol.EOEditingContext;

public class ProcessDelPaiement {

	/**
	 * Supprime un paiement.<BR>
	 * 
	 * @param databus
	 *            _CktlBasicDataBus permettant de gerer les transactions
	 * @param ec
	 *            editingContext dans lequel on enregistre
	 * @param paiement
	 *            Le paiement a supprimer
	 * @throws ScolarixFwkException
	 */
	public static void enregistrer(_CktlBasicDataBus databus, EOEditingContext ec, EOPaiement paiement) throws ScolarixFwkException {
		if (paiement == null) {
			throw new ScolarixFwkException("il faut passer un paiement a supprimer en parametre !");
		}
		if (databus == null || !CktlDataBus.isDatabaseConnected()) {
			throw new ScolarixFwkException("Probleme avec le dataBus");
		}

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode ProcessDelPaiement.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			boolean ok = ProcedureDelPaiement.enregistrer(databus, paiement);
			if (!ok) {
				throw new ScolarixFwkException((String) databus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			ec.revert();
			ec.invalidateAllObjects();
		}
		catch (ScolarixFwkException e) {
			e.printStackTrace();
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
	}

}
