/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.exception.HistoriqueException;
import org.cocktail.scolarix.serveur.exception.PreEtudiantException;
import org.cocktail.scolarix.serveur.factory.FactoryPreEtudiant;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPreEtudiant extends _EOPreEtudiant {

	public static final String PRE_TYPE_PRE_INSCRIPTION = "P";
	public static final String PRE_TYPE_RE_INSCRIPTION = "R";

	private EORne rne;

	public EOPreEtudiant() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public EORne rne() {
		if (rne == null) {
			EOPreInscription inscriptionPrincipale = inscriptionPrincipale();
			if (inscriptionPrincipale != null) {
				rne = inscriptionPrincipale.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome()
						.toFwkScolarix_VEtablissementScolarite().toRne();
			}
		}
		return rne;
	}

	public EOPreInscription inscriptionPrincipale() throws EtudiantException {
		try {
			EOPreInscription preInscription = null;
			EOPreHistorique preHistorique = preHistorique();
			if (preHistorique != null) {
				preInscription = preHistorique.inscriptionPrincipale();
			}
			return preInscription;
		}
		catch (HistoriqueException e) {
			throw new EtudiantException("Le pré-étudiant No " + this.numero() + " a plusieurs inscriptions principales, ce n'est pas cohérent !!");
		}
	}

	public void setRne(EORne rne) {
		this.rne = rne;
	}

	public Integer numero() {
		return etudNumero();
	}

	public String numeroINE() {
		return etudCodeIne();
	}

	public String prenomNom() {
		return toPreIndividu().prenomAffichage() + " " + toPreIndividu().nomAffichage();
	}

	public EOSitFamEtudiant situationFamiliale() {
		return toSitFamEtudiant();
	}

	public boolean isCelibataire() {
		boolean isCelibataire = false;
		EOSitFamEtudiant situation = toSitFamEtudiant();
		if (situation != null) {
			String libelle = situation.etudSitfamLibelle();
			if (libelle != null && libelle.toLowerCase().startsWith("seul")) {
				isCelibataire = true;
			}
		}
		return isCelibataire;
	}

	public void setIsCelibataire(Boolean isCelibataire) {
		String situation = "";
		if (isCelibataire.booleanValue() == true) {
			situation = "Seul";
		}
		else {
			situation = "En couple";
		}
		if (isEnfantsACharge()) {
			situation += " avec enfants";
		}
		else {
			situation += " sans enfants";
		}
		FactoryPreEtudiant.setSituationFamiliale(this, situation);
	}

	public boolean isEnCouple() {
		boolean isEnCouple = false;
		EOSitFamEtudiant situation = toSitFamEtudiant();
		if (situation != null) {
			String libelle = situation.etudSitfamLibelle();
			if (libelle != null && libelle.toLowerCase().startsWith("en couple")) {
				isEnCouple = true;
			}
		}
		return isEnCouple;
	}

	public void setIsEnCouple(Boolean isEnCouple) {
		String situation = "";
		if (isEnCouple.booleanValue() == false) {
			situation = "Seul";
		}
		else {
			situation = "En couple";
		}
		if (isEnfantsACharge()) {
			situation += " avec enfants";
		}
		else {
			situation += " sans enfants";
		}
		FactoryPreEtudiant.setSituationFamiliale(this, situation);
	}

	public boolean isEnfantsACharge() {
		boolean isEnfantsACharge = false;
		EOSitFamEtudiant situation = toSitFamEtudiant();
		if (situation != null) {
			String libelle = situation.etudSitfamLibelle();
			if (libelle != null && libelle.toLowerCase().endsWith("avec enfants")) {
				isEnfantsACharge = true;
			}
		}
		return isEnfantsACharge;
	}

	public void setIsEnfantsACharge(Boolean isEnfantsACharge) {
		String situation = "";
		if (isEnCouple() == false) {
			situation = "Seul";
		}
		else {
			situation = "En couple";
		}
		if (isEnfantsACharge) {
			situation += " avec enfants";
		}
		else {
			situation += " sans enfants";
		}
		FactoryPreEtudiant.setSituationFamiliale(this, situation);
	}

	public EOPreAdresse adresseStable() {
		return toPreIndividu().adresseStable();
	}

	public EOPreAdresse adresseUniversitaire() {
		return toPreIndividu().adresseUniversitaire();
	}

	public EOPrePersonneTelephone telephoneStable() {
		return toPreIndividu().telephoneStable();
	}

	public void setTelephoneStable(EOPrePersonneTelephone telephone) {
		toPreIndividu().addToToPrePersonneTelephonesRelationship(telephone);
	}

	public String noTelephoneStable() {
		String noTelephone = null;
		EOPrePersonneTelephone telephone = telephoneStable();
		if (telephone != null) {
			noTelephone = telephone.noTelephone();
		}
		return noTelephone;
	}

	public void setNoTelephoneStable(EOEditingContext ec, String noTelephoneStable) {
		EOPrePersonneTelephone telephone = telephoneStable();
		if (telephone != null) {
			telephone.setNoTelephone(noTelephoneStable);
		}
		else {
			EOTypeNoTel typeNoTel = EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL);
			EOTypeTel typeTel = EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_PAR);
			telephone = EOPrePersonneTelephone.create(ec, DateCtrl.now(), DateCtrl.now(), DateCtrl.now(), null, null, noTelephoneStable, "N",
					typeNoTel, typeTel);
			setTelephoneStable(telephone);
		}
	}

	public EOPrePersonneTelephone telephoneUniversitaireFixe() {
		return toPreIndividu().telephoneUniversitaireFixe();
	}

	public void setTelephoneUniversitaireFixe(EOPrePersonneTelephone telephone) {
		toPreIndividu().addToToPrePersonneTelephonesRelationship(telephone);
	}

	public String noTelephoneUniversitaireFixe() {
		String noTelephone = null;
		EOPrePersonneTelephone telephone = telephoneUniversitaireFixe();
		if (telephone != null) {
			noTelephone = telephone.noTelephone();
		}
		return noTelephone;
	}

	public void setNoTelephoneUniversitaireFixe(EOEditingContext ec, String noTelephoneUniversitaireFixe) {
		EOPrePersonneTelephone telephone = telephoneUniversitaireFixe();
		if (telephone != null) {
			telephone.setNoTelephone(noTelephoneUniversitaireFixe);
		}
		else {
			EOTypeNoTel typeNoTel = EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL);
			EOTypeTel typeTel = EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_ETUD);
			telephone = EOPrePersonneTelephone.create(ec, DateCtrl.now(), DateCtrl.now(), DateCtrl.now(), null, null, noTelephoneUniversitaireFixe,
					"O", typeNoTel, typeTel);
			setTelephoneUniversitaireFixe(telephone);
		}
	}

	public EOPrePersonneTelephone telephoneUniversitairePortable() {
		return toPreIndividu().telephoneUniversitairePortable();
	}

	public void setTelephoneUniversitairePortable(EOPrePersonneTelephone telephone) {
		toPreIndividu().addToToPrePersonneTelephonesRelationship(telephone);
	}

	public String noTelephoneUniversitairePortable() {
		String noTelephone = null;
		EOPrePersonneTelephone telephone = telephoneUniversitairePortable();
		if (telephone != null) {
			noTelephone = telephone.noTelephone();
		}
		return noTelephone;
	}

	public void setNoTelephoneUniversitairePortable(EOEditingContext ec, String noTelephoneUniversitairePortable) {
		EOPrePersonneTelephone telephone = telephoneUniversitairePortable();
		if (telephone != null) {
			telephone.setNoTelephone(noTelephoneUniversitairePortable);
		}
		else {
			EOTypeNoTel typeNoTel = EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_MOB);
			EOTypeTel typeTel = EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_ETUD);
			telephone = EOPrePersonneTelephone.create(ec, DateCtrl.now(), DateCtrl.now(), DateCtrl.now(), null, null,
					noTelephoneUniversitairePortable, "N", typeNoTel, typeTel);
			setTelephoneUniversitairePortable(telephone);
		}
	}

	public String email() {
		EOPreRepartAdresse rpa = repartPersonneAdresseEtud();
		if (rpa != null && rpa.eMail() != null) {
			return rpa.eMail();
		}
		return null;
	}

	public void setEmail(String email) {
		EOPreRepartAdresse rpa = repartPersonneAdresseEtud();
		if (rpa != null) {
			if (email == null || email.trim().length() == 0) {
				rpa.setEMail(null);
			}
			else {
				rpa.setEMail(email);
			}
		}
	}

	/**
	 * @return le EOPreRepartAdresse etudiant
	 */
	private EOPreRepartAdresse repartPersonneAdresseEtud() {
		EOPreRepartAdresse rpa = null;
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>(2);
		quals.addObject(new EOKeyValueQualifier(EOPreRepartAdresse.TO_FWKPERS__TYPE_ADRESSE_KEY + "." + EOTypeAdresse.TADR_CODE_KEY,
				EOKeyValueQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_ETUD));
		quals.addObject(new EOKeyValueQualifier(EOPreRepartAdresse.RPA_VALIDE_KEY, EOKeyValueQualifier.QualifierOperatorEqual, "O"));
		NSArray<EOPreRepartAdresse> repartAdresses = toPreIndividu().toPreRepartAdresses(new EOAndQualifier(quals));
		if (repartAdresses != null && repartAdresses.count() > 0) {
			rpa = repartAdresses.lastObject();
		}
		return rpa;

	}

	public EOMention mentionTitreAcces() {
		return toMentionBac();
	}

	public void setMentionTitreAcces(EOMention mention) {
		setToMentionBacRelationship(mention);
	}

	public EORne etablissementTitreAcces() {
		return toRneCodeBac();
	}

	public void setEtablissementTitreAcces(EORne etablissement) {
		setToRneCodeBacRelationship(etablissement);
	}

	public EODepartement departementTitreAcces() {
		return toFwkpers_Departement_EtabBac();
	}

	public void setDepartementTitreAcces(EODepartement departement) {
		setToFwkpers_Departement_EtabBacRelationship(departement);
	}

	public EOPays paysTitreAcces() {
		return toFwkpers_Pays_EtabBac();
	}

	public void setPaysTitreAcces(EOPays pays) {
		setToFwkpers_Pays_EtabBacRelationship(pays);
	}

	public EOBac titreAcces() {
		return toBac();
	}

	public void setTitreAcces(EOBac titreAcces) {
		setToBacRelationship(titreAcces);
	}

	public Integer anneeObtentionTitreAcces() {
		return etudAnbac();
	}

	public void setAnneeObtentionTitreAcces(Integer annee) {
		setEtudAnbac(annee);
	}

	public Integer anneePremiereInscriptionEnseignementSuperieur() {
		return etudAnnee1InscSup();
	}

	public void setAnneePremiereInscriptionEnseignementSuperieur(Integer annee) {
		setEtudAnnee1InscSup(annee);
	}

	public Integer anneePremiereInscriptionUniversiteFrancaise() {
		return etudAnnee1InscUniv();
	}

	public void setAnneePremiereInscriptionUniversiteFrancaise(Integer annee) {
		setEtudAnnee1InscUniv(annee);
	}

	public Integer anneePremiereInscriptionUniversiteLocale() {
		return etudAnnee1InscUlr();
	}

	public void setAnneePremiereInscriptionUniversiteLocale(Integer annee) {
		setEtudAnnee1InscUlr(annee);
	}

	public EORne universitePremiereInscription() {
		return toRneCodeSup();
	}

	public void setUniversitePremiereInscription(EORne universite) {
		setToRneCodeSupRelationship(universite);
	}

	/**
	 * Retourne le pre-historique associe au pre-etudiant, lequel doit etre unique.
	 * 
	 * @return un EOPreHistorique.
	 * @throws PreEtudiantException
	 *             Si aucun PreHistorique trouve, ou si plusieurs PreHistorique trouves.
	 */
	public EOPreHistorique preHistorique() throws PreEtudiantException {
		if (toPreHistoriques() == null || toPreHistoriques().count() == 0) {
			throw new PreEtudiantException("Problème : aucune pré-inscription récupérée !");
		}
		if (toPreHistoriques().count() > 1) {
			throw new PreEtudiantException("Problème : plusieurs pré-inscriptions existent, ce n'est pas normal !");
		}
		return (EOPreHistorique) toPreHistoriques().lastObject();
	}

	public boolean isPreInscription() {
		return preType() != null && preType().equals(PRE_TYPE_PRE_INSCRIPTION);
	}

	public boolean isReInscription() {
		return preType() != null && preType().equals(PRE_TYPE_RE_INSCRIPTION);
	}

	public EORdvCandidat rendezVous() {
		EORdvCandidat rendezVous = toRdvPreEtudiant();

		if (rendezVous == null || rendezVous.convocDate() == null) {
			setToRdvPreEtudiantRelationship(null);
			rendezVous = null;
		}

		return rendezVous;
	}

}
