/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVWebInscriptionResultat.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVWebInscriptionResultat extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VWebInscriptionResultat";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_WEB_INSCRIPTION_RESULTAT";

	//Attributes

	public static final ERXKey<Double> CREDITS = new ERXKey<Double>("credits");
	public static final ERXKey<Integer> ETAT = new ERXKey<Integer>("etat");
	public static final ERXKey<Integer> ETAT_TYPE = new ERXKey<Integer>("etatType");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<String> FDIP_ABREVIATION = new ERXKey<String>("fdipAbreviation");
	public static final ERXKey<String> FGRA_CODE = new ERXKey<String>("fgraCode");
	public static final ERXKey<Integer> HIST_NUMERO = new ERXKey<Integer>("histNumero");
	public static final ERXKey<Integer> IDIPL_ANNEE_SUIVIE = new ERXKey<Integer>("idiplAnneeSuivie");
	public static final ERXKey<String> NIVEAU_DETAIL = new ERXKey<String>("niveauDetail");

	public static final String CREDITS_KEY = "credits";
	public static final String ETAT_KEY = "etat";
	public static final String ETAT_TYPE_KEY = "etatType";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String FDIP_ABREVIATION_KEY = "fdipAbreviation";
	public static final String FGRA_CODE_KEY = "fgraCode";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String IDIPL_ANNEE_SUIVIE_KEY = "idiplAnneeSuivie";
	public static final String NIVEAU_DETAIL_KEY = "niveauDetail";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FDOM_CODE_KEY = "fdomCode";
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";

	// Colkeys
	public static final String CREDITS_COLKEY = "CREDITS";
	public static final String ETAT_COLKEY = "ETAT";
	public static final String ETAT_TYPE_COLKEY = "ETAT_TYPE";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String FDIP_ABREVIATION_COLKEY = "FDIP_ABREVIATION";
	public static final String FGRA_CODE_COLKEY = "FGRA_CODE";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String IDIPL_ANNEE_SUIVIE_COLKEY = "IDIPL_ANNEE_SUIVIE";
	public static final String NIVEAU_DETAIL_COLKEY = "NIVEAU";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FDOM_CODE_COLKEY = "FDOM_CODE";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl> TO_INSC_DIPL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl>("toInscDipl");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";
	public static final String TO_INSC_DIPL_KEY = "toInscDipl";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVWebInscriptionResultat with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param etudNumero
	 * @param histNumero
	 * @param idiplAnneeSuivie
	 * @param niveauDetail
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @param toInscDipl
	 * @return EOVWebInscriptionResultat
	 */
	public static EOVWebInscriptionResultat create(EOEditingContext editingContext, Integer etudNumero, Integer histNumero, Integer idiplAnneeSuivie, String niveauDetail, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation, org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl) {
		EOVWebInscriptionResultat eo = (EOVWebInscriptionResultat) createAndInsertInstance(editingContext);
		eo.setEtudNumero(etudNumero);
		eo.setHistNumero(histNumero);
		eo.setIdiplAnneeSuivie(idiplAnneeSuivie);
		eo.setNiveauDetail(niveauDetail);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		eo.setToInscDiplRelationship(toInscDipl);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVWebInscriptionResultat.
	 *
	 * @param editingContext
	 * @return EOVWebInscriptionResultat
	 */
	public static EOVWebInscriptionResultat create(EOEditingContext editingContext) {
		EOVWebInscriptionResultat eo = (EOVWebInscriptionResultat) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVWebInscriptionResultat localInstanceIn(EOEditingContext editingContext) {
		EOVWebInscriptionResultat localInstance = (EOVWebInscriptionResultat) localInstanceOfObject(editingContext, (EOVWebInscriptionResultat) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVWebInscriptionResultat localInstanceIn(EOEditingContext editingContext, EOVWebInscriptionResultat eo) {
		EOVWebInscriptionResultat localInstance = (eo == null) ? null : (EOVWebInscriptionResultat) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Double credits() {
		return (Double) storedValueForKey("credits");
	}

	public void setCredits(Double value) {
		takeStoredValueForKey(value, "credits");
	}
	public Integer etat() {
		return (Integer) storedValueForKey("etat");
	}

	public void setEtat(Integer value) {
		takeStoredValueForKey(value, "etat");
	}
	public Integer etatType() {
		return (Integer) storedValueForKey("etatType");
	}

	public void setEtatType(Integer value) {
		takeStoredValueForKey(value, "etatType");
	}
	public Integer etudNumero() {
		return (Integer) storedValueForKey("etudNumero");
	}

	public void setEtudNumero(Integer value) {
		takeStoredValueForKey(value, "etudNumero");
	}
	public String fdipAbreviation() {
		return (String) storedValueForKey("fdipAbreviation");
	}

	public void setFdipAbreviation(String value) {
		takeStoredValueForKey(value, "fdipAbreviation");
	}
	public String fgraCode() {
		return (String) storedValueForKey("fgraCode");
	}

	public void setFgraCode(String value) {
		takeStoredValueForKey(value, "fgraCode");
	}
	public Integer histNumero() {
		return (Integer) storedValueForKey("histNumero");
	}

	public void setHistNumero(Integer value) {
		takeStoredValueForKey(value, "histNumero");
	}
	public Integer idiplAnneeSuivie() {
		return (Integer) storedValueForKey("idiplAnneeSuivie");
	}

	public void setIdiplAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "idiplAnneeSuivie");
	}
	public String niveauDetail() {
		return (String) storedValueForKey("niveauDetail");
	}

	public void setNiveauDetail(String value) {
		takeStoredValueForKey(value, "niveauDetail");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOInscDipl)storedValueForKey("toInscDipl");
	}

	public void setToInscDiplRelationship(org.cocktail.scolarix.serveur.metier.eos.EOInscDipl value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOInscDipl oldValue = toInscDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toInscDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toInscDipl");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVWebInscriptionResultat.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVWebInscriptionResultat.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVWebInscriptionResultat)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVWebInscriptionResultat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVWebInscriptionResultat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVWebInscriptionResultat eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVWebInscriptionResultat)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVWebInscriptionResultat fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVWebInscriptionResultat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVWebInscriptionResultat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVWebInscriptionResultat eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVWebInscriptionResultat)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVWebInscriptionResultat fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVWebInscriptionResultat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVWebInscriptionResultat eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVWebInscriptionResultat ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVWebInscriptionResultat createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVWebInscriptionResultat.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVWebInscriptionResultat.ENTITY_NAME + "' !");
		}
		else {
			EOVWebInscriptionResultat object = (EOVWebInscriptionResultat) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVWebInscriptionResultat localInstanceOfObject(EOEditingContext ec, EOVWebInscriptionResultat object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVWebInscriptionResultat " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVWebInscriptionResultat) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
