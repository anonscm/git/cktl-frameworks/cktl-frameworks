/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEtudiant.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOEtudiant extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_Etudiant";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ETUDIANT";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "etudNumero";

	public static final ERXKey<Integer> CAND_NUMERO = new ERXKey<Integer>("candNumero");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> ETUD_ANBAC = new ERXKey<Integer>("etudAnbac");
	public static final ERXKey<Integer> ETUD_ANNEE1_INSC_SUP = new ERXKey<Integer>("etudAnnee1InscSup");
	public static final ERXKey<Integer> ETUD_ANNEE1_INSC_ULR = new ERXKey<Integer>("etudAnnee1InscUlr");
	public static final ERXKey<Integer> ETUD_ANNEE1_INSC_UNIV = new ERXKey<Integer>("etudAnnee1InscUniv");
	public static final ERXKey<String> ETUD_CODE_INE = new ERXKey<String>("etudCodeIne");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<String> ETUD_REIMMATRICULATION = new ERXKey<String>("etudReimmatriculation");
	public static final ERXKey<Integer> ETUD_SN_ATTESTATION = new ERXKey<Integer>("etudSnAttestation");
	public static final ERXKey<Integer> ETUD_SN_CERTIFICATION = new ERXKey<Integer>("etudSnCertification");
	public static final ERXKey<Integer> ETUD_SPORT_HN = new ERXKey<Integer>("etudSportHn");
	public static final ERXKey<String> ETUD_VILLE_BAC = new ERXKey<String>("etudVilleBac");

	public static final String CAND_NUMERO_KEY = "candNumero";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETUD_ANBAC_KEY = "etudAnbac";
	public static final String ETUD_ANNEE1_INSC_SUP_KEY = "etudAnnee1InscSup";
	public static final String ETUD_ANNEE1_INSC_ULR_KEY = "etudAnnee1InscUlr";
	public static final String ETUD_ANNEE1_INSC_UNIV_KEY = "etudAnnee1InscUniv";
	public static final String ETUD_CODE_INE_KEY = "etudCodeIne";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String ETUD_REIMMATRICULATION_KEY = "etudReimmatriculation";
	public static final String ETUD_SN_ATTESTATION_KEY = "etudSnAttestation";
	public static final String ETUD_SN_CERTIFICATION_KEY = "etudSnCertification";
	public static final String ETUD_SPORT_HN_KEY = "etudSportHn";
	public static final String ETUD_VILLE_BAC_KEY = "etudVilleBac";

	// Non visible attributes
	public static final String ACAD_CODE_BAC_KEY = "acadCodeBac";
	public static final String BAC_CODE_KEY = "bacCode";
	public static final String C_DEPARTEMENT_ETAB_BAC_KEY = "cDepartementEtabBac";
	public static final String C_DEPARTEMENT_PARENT_KEY = "cDepartementParent";
	public static final String C_PAYS_ETAB_BAC_KEY = "cPaysEtabBac";
	public static final String ETAB_CODE_BAC_KEY = "etabCodeBac";
	public static final String ETAB_CODE_SUP_KEY = "etabCodeSup";
	public static final String ETUD_SITFAM_KEY = "etudSitfam";
	public static final String MENT_CODE_KEY = "mentCode";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PRO_CODE_KEY = "proCode";
	public static final String THEB_CODE_KEY = "thebCode";
	public static final String PRO_CODE2_KEY = "proCode2";

	// Colkeys
	public static final String CAND_NUMERO_COLKEY = "CAND_NUMERO";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ETUD_ANBAC_COLKEY = "ETUD_ANBAC";
	public static final String ETUD_ANNEE1_INSC_SUP_COLKEY = "ETUD_ANNEE_1INSC_SUP";
	public static final String ETUD_ANNEE1_INSC_ULR_COLKEY = "ETUD_ANNEE_1INSC_ULR";
	public static final String ETUD_ANNEE1_INSC_UNIV_COLKEY = "ETUD_ANNEE_1INSC_UNIV";
	public static final String ETUD_CODE_INE_COLKEY = "ETUD_CODE_INE";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String ETUD_REIMMATRICULATION_COLKEY = "ETUD_REIMMATRICULATION";
	public static final String ETUD_SN_ATTESTATION_COLKEY = "ETUD_SN_ATTESTATION";
	public static final String ETUD_SN_CERTIFICATION_COLKEY = "ETUD_SN_CERTIFICATION";
	public static final String ETUD_SPORT_HN_COLKEY = "ETUD_SPORT_HN";
	public static final String ETUD_VILLE_BAC_COLKEY = "ETUD_VILLE_BAC";

	// Non visible colkeys
	public static final String ACAD_CODE_BAC_COLKEY = "ACAD_CODE_BAC";
	public static final String BAC_CODE_COLKEY = "BAC_CODE";
	public static final String C_DEPARTEMENT_ETAB_BAC_COLKEY = "C_DEPARTEMENT_ETAB_BAC";
	public static final String C_DEPARTEMENT_PARENT_COLKEY = "C_DEPARTEMENT_PARENT";
	public static final String C_PAYS_ETAB_BAC_COLKEY = "C_PAYS_ETAB_BAC";
	public static final String ETAB_CODE_BAC_COLKEY = "ETAB_CODE_BAC";
	public static final String ETAB_CODE_SUP_COLKEY = "ETAB_CODE_SUP";
	public static final String ETUD_SITFAM_COLKEY = "ETUD_SITFAM";
	public static final String MENT_CODE_COLKEY = "MENT_CODE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PRO_CODE_COLKEY = "PRO_CODE";
	public static final String THEB_CODE_COLKEY = "THEB_CODE";
	public static final String PRO_CODE2_COLKEY = "PRO_CODE2";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBac> TO_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBac>("toBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum> TO_CANDIDAT_GRHUMS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum>("toCandidatGrhums");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie> TO_FWKPERS__ACADEMIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie>("toFwkpers_Academie");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_EtabBac");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__PARENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_Parent");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_FWKPERS__INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toFwkpers_Individu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_EtabBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique> TO_HISTORIQUES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique>("toHistoriques");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMention> TO_MENTION_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMention>("toMentionBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOProfession> TO_PROFESSION = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOProfession>("toProfession");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOProfession> TO_PROFESSION2 = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOProfession>("toProfession2");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORdvCandidat> TO_RDV_CANDIDAT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORdvCandidat>("toRdvCandidat");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_CODE_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneCodeBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_CODE_SUP = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneCodeSup");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant> TO_SIT_FAM_ETUDIANT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant>("toSitFamEtudiant");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeHebergement> TO_TYPE_HEBERGEMENT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeHebergement>("toTypeHebergement");

	public static final String TO_BAC_KEY = "toBac";
	public static final String TO_CANDIDAT_GRHUMS_KEY = "toCandidatGrhums";
	public static final String TO_FWKPERS__ACADEMIE_KEY = "toFwkpers_Academie";
	public static final String TO_FWKPERS__DEPARTEMENT__ETAB_BAC_KEY = "toFwkpers_Departement_EtabBac";
	public static final String TO_FWKPERS__DEPARTEMENT__PARENT_KEY = "toFwkpers_Departement_Parent";
	public static final String TO_FWKPERS__INDIVIDU_KEY = "toFwkpers_Individu";
	public static final String TO_FWKPERS__PAYS__ETAB_BAC_KEY = "toFwkpers_Pays_EtabBac";
	public static final String TO_HISTORIQUES_KEY = "toHistoriques";
	public static final String TO_MENTION_BAC_KEY = "toMentionBac";
	public static final String TO_PROFESSION_KEY = "toProfession";
	public static final String TO_PROFESSION2_KEY = "toProfession2";
	public static final String TO_RDV_CANDIDAT_KEY = "toRdvCandidat";
	public static final String TO_RNE_CODE_BAC_KEY = "toRneCodeBac";
	public static final String TO_RNE_CODE_SUP_KEY = "toRneCodeSup";
	public static final String TO_SIT_FAM_ETUDIANT_KEY = "toSitFamEtudiant";
	public static final String TO_TYPE_HEBERGEMENT_KEY = "toTypeHebergement";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOEtudiant with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dCreation
	 * @param dModification
	 * @param etudNumero
	 * @param etudSportHn
	 * @param toBac
	 * @param toProfession
	 * @param toSitFamEtudiant
	 * @return EOEtudiant
	 */
	public static EOEtudiant create(EOEditingContext editingContext, NSTimestamp dCreation, NSTimestamp dModification, Integer etudNumero, Integer etudSportHn, org.cocktail.scolarix.serveur.metier.eos.EOBac toBac, org.cocktail.scolarix.serveur.metier.eos.EOProfession toProfession, org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant toSitFamEtudiant) {
		EOEtudiant eo = (EOEtudiant) createAndInsertInstance(editingContext);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setEtudNumero(etudNumero);
		eo.setEtudSportHn(etudSportHn);
		eo.setToBacRelationship(toBac);
		eo.setToProfessionRelationship(toProfession);
		eo.setToSitFamEtudiantRelationship(toSitFamEtudiant);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOEtudiant.
	 *
	 * @param editingContext
	 * @return EOEtudiant
	 */
	public static EOEtudiant create(EOEditingContext editingContext) {
		EOEtudiant eo = (EOEtudiant) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOEtudiant localInstanceIn(EOEditingContext editingContext) {
		EOEtudiant localInstance = (EOEtudiant) localInstanceOfObject(editingContext, (EOEtudiant) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOEtudiant localInstanceIn(EOEditingContext editingContext, EOEtudiant eo) {
		EOEtudiant localInstance = (eo == null) ? null : (EOEtudiant) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer candNumero() {
		return (Integer) storedValueForKey("candNumero");
	}

	public void setCandNumero(Integer value) {
		takeStoredValueForKey(value, "candNumero");
	}
	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}
	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	public Integer etudAnbac() {
		return (Integer) storedValueForKey("etudAnbac");
	}

	public void setEtudAnbac(Integer value) {
		takeStoredValueForKey(value, "etudAnbac");
	}
	public Integer etudAnnee1InscSup() {
		return (Integer) storedValueForKey("etudAnnee1InscSup");
	}

	public void setEtudAnnee1InscSup(Integer value) {
		takeStoredValueForKey(value, "etudAnnee1InscSup");
	}
	public Integer etudAnnee1InscUlr() {
		return (Integer) storedValueForKey("etudAnnee1InscUlr");
	}

	public void setEtudAnnee1InscUlr(Integer value) {
		takeStoredValueForKey(value, "etudAnnee1InscUlr");
	}
	public Integer etudAnnee1InscUniv() {
		return (Integer) storedValueForKey("etudAnnee1InscUniv");
	}

	public void setEtudAnnee1InscUniv(Integer value) {
		takeStoredValueForKey(value, "etudAnnee1InscUniv");
	}
	public String etudCodeIne() {
		return (String) storedValueForKey("etudCodeIne");
	}

	public void setEtudCodeIne(String value) {
		takeStoredValueForKey(value, "etudCodeIne");
	}
	public Integer etudNumero() {
		return (Integer) storedValueForKey("etudNumero");
	}

	public void setEtudNumero(Integer value) {
		takeStoredValueForKey(value, "etudNumero");
	}
	public String etudReimmatriculation() {
		return (String) storedValueForKey("etudReimmatriculation");
	}

	public void setEtudReimmatriculation(String value) {
		takeStoredValueForKey(value, "etudReimmatriculation");
	}
	public Integer etudSnAttestation() {
		return (Integer) storedValueForKey("etudSnAttestation");
	}

	public void setEtudSnAttestation(Integer value) {
		takeStoredValueForKey(value, "etudSnAttestation");
	}
	public Integer etudSnCertification() {
		return (Integer) storedValueForKey("etudSnCertification");
	}

	public void setEtudSnCertification(Integer value) {
		takeStoredValueForKey(value, "etudSnCertification");
	}
	public Integer etudSportHn() {
		return (Integer) storedValueForKey("etudSportHn");
	}

	public void setEtudSportHn(Integer value) {
		takeStoredValueForKey(value, "etudSportHn");
	}
	public String etudVilleBac() {
		return (String) storedValueForKey("etudVilleBac");
	}

	public void setEtudVilleBac(String value) {
		takeStoredValueForKey(value, "etudVilleBac");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOBac toBac() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOBac)storedValueForKey("toBac");
	}

	public void setToBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EOBac value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOBac oldValue = toBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toBac");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOAcademie toFwkpers_Academie() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAcademie)storedValueForKey("toFwkpers_Academie");
	}

	public void setToFwkpers_AcademieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAcademie value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAcademie oldValue = toFwkpers_Academie();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Academie");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Academie");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_EtabBac() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_EtabBac");
	}

	public void setToFwkpers_Departement_EtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_EtabBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_EtabBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_EtabBac");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_Parent() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_Parent");
	}

	public void setToFwkpers_Departement_ParentRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_Parent();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_Parent");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_Parent");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toFwkpers_Individu");
	}

	public void setToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toFwkpers_Individu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Individu");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Individu");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_EtabBac() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_EtabBac");
	}

	public void setToFwkpers_Pays_EtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_EtabBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_EtabBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_EtabBac");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOMention toMentionBac() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOMention)storedValueForKey("toMentionBac");
	}

	public void setToMentionBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EOMention value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOMention oldValue = toMentionBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMentionBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toMentionBac");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOProfession toProfession() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOProfession)storedValueForKey("toProfession");
	}

	public void setToProfessionRelationship(org.cocktail.scolarix.serveur.metier.eos.EOProfession value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOProfession oldValue = toProfession();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toProfession");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toProfession");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOProfession toProfession2() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOProfession)storedValueForKey("toProfession2");
	}

	public void setToProfession2Relationship(org.cocktail.scolarix.serveur.metier.eos.EOProfession value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOProfession oldValue = toProfession2();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toProfession2");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toProfession2");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORdvCandidat toRdvCandidat() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORdvCandidat)storedValueForKey("toRdvCandidat");
	}

	public void setToRdvCandidatRelationship(org.cocktail.scolarix.serveur.metier.eos.EORdvCandidat value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORdvCandidat oldValue = toRdvCandidat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRdvCandidat");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRdvCandidat");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneCodeBac() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneCodeBac");
	}

	public void setToRneCodeBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneCodeBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneCodeBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneCodeBac");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneCodeSup() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneCodeSup");
	}

	public void setToRneCodeSupRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneCodeSup();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneCodeSup");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneCodeSup");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant toSitFamEtudiant() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant)storedValueForKey("toSitFamEtudiant");
	}

	public void setToSitFamEtudiantRelationship(org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant oldValue = toSitFamEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSitFamEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toSitFamEtudiant");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOTypeHebergement toTypeHebergement() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeHebergement)storedValueForKey("toTypeHebergement");
	}

	public void setToTypeHebergementRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeHebergement value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeHebergement oldValue = toTypeHebergement();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeHebergement");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toTypeHebergement");
		}
	}
  
	public NSArray toCandidatGrhums() {
		return (NSArray)storedValueForKey("toCandidatGrhums");
	}

	public NSArray toCandidatGrhums(EOQualifier qualifier) {
		return toCandidatGrhums(qualifier, null);
	}

	public NSArray toCandidatGrhums(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toCandidatGrhums();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToCandidatGrhumsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toCandidatGrhums");
	}

	public void removeFromToCandidatGrhumsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toCandidatGrhums");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum createToCandidatGrhumsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_CandidatGrhum");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toCandidatGrhums");
		return (org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum) eo;
	}

	public void deleteToCandidatGrhumsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toCandidatGrhums");
				editingContext().deleteObject(object);
			}

	public void deleteAllToCandidatGrhumsRelationships() {
		Enumeration objects = toCandidatGrhums().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToCandidatGrhumsRelationship((org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum)objects.nextElement());
		}
	}
	public NSArray toHistoriques() {
		return (NSArray)storedValueForKey("toHistoriques");
	}

	public NSArray toHistoriques(EOQualifier qualifier) {
		return toHistoriques(qualifier, null, false);
	}

	public NSArray toHistoriques(EOQualifier qualifier, boolean fetch) {
		return toHistoriques(qualifier, null, fetch);
	}

	public NSArray toHistoriques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EOHistorique.TO_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EOHistorique.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toHistoriques();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToHistoriquesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOHistorique object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toHistoriques");
	}

	public void removeFromToHistoriquesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOHistorique object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toHistoriques");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOHistorique createToHistoriquesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_Historique");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toHistoriques");
		return (org.cocktail.scolarix.serveur.metier.eos.EOHistorique) eo;
	}

	public void deleteToHistoriquesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOHistorique object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toHistoriques");
				editingContext().deleteObject(object);
			}

	public void deleteAllToHistoriquesRelationships() {
		Enumeration objects = toHistoriques().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToHistoriquesRelationship((org.cocktail.scolarix.serveur.metier.eos.EOHistorique)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOEtudiant.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOEtudiant.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOEtudiant)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOEtudiant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEtudiant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOEtudiant eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOEtudiant)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOEtudiant fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOEtudiant eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOEtudiant)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOEtudiant fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOEtudiant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOEtudiant eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOEtudiant ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchRecherche(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("Recherche", "FwkScolarix_Etudiant");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EOEtudiant createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOEtudiant.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOEtudiant.ENTITY_NAME + "' !");
		}
		else {
			EOEtudiant object = (EOEtudiant) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOEtudiant localInstanceOfObject(EOEditingContext ec, EOEtudiant object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOEtudiant " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOEtudiant) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
