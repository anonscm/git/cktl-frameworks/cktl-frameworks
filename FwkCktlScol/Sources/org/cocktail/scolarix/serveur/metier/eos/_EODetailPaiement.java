/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODetailPaiement.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EODetailPaiement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_DetailPaiement";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.DETAIL_PAIEMENT";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "dpaieNumero";

	public static final ERXKey<NSTimestamp> DPAIE_DATE_OPERATION = new ERXKey<NSTimestamp>("dpaieDateOperation");
	public static final ERXKey<java.math.BigDecimal> DPAIE_MONTANT = new ERXKey<java.math.BigDecimal>("dpaieMontant");
	public static final ERXKey<Integer> DPAIE_MOUVEMENT = new ERXKey<Integer>("dpaieMouvement");
	public static final ERXKey<Integer> EREMB_NUMERO = new ERXKey<Integer>("erembNumero");
	public static final ERXKey<Integer> HIST_ANNEE_SCOL = new ERXKey<Integer>("histAnneeScol");

	public static final String DPAIE_DATE_OPERATION_KEY = "dpaieDateOperation";
	public static final String DPAIE_MONTANT_KEY = "dpaieMontant";
	public static final String DPAIE_MOUVEMENT_KEY = "dpaieMouvement";
	public static final String EREMB_NUMERO_KEY = "erembNumero";
	public static final String HIST_ANNEE_SCOL_KEY = "histAnneeScol";

	// Non visible attributes
	public static final String DPAIE_NUMERO_KEY = "dpaieNumero";
	public static final String ETAB_CODE_KEY = "etabCode";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String PAIE_NUMERO_KEY = "paieNumero";
	public static final String RCPT_CODE_KEY = "rcptCode";

	// Colkeys
	public static final String DPAIE_DATE_OPERATION_COLKEY = "DPAIE_DATE_OPERATION";
	public static final String DPAIE_MONTANT_COLKEY = "DPAIE_MONTANT";
	public static final String DPAIE_MOUVEMENT_COLKEY = "DPAIE_MOUVEMENT";
	public static final String EREMB_NUMERO_COLKEY = "EREMB_NUMERO";
	public static final String HIST_ANNEE_SCOL_COLKEY = "HIST_ANNEE_SCOL";

	// Non visible colkeys
	public static final String DPAIE_NUMERO_COLKEY = "DPAIE_NUMERO";
	public static final String ETAB_CODE_COLKEY = "ETAB_CODE";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String PAIE_NUMERO_COLKEY = "PAIE_NUMERO";
	public static final String RCPT_CODE_COLKEY = "RCPT_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse> TO_ETUDIANT_REMBOURSE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse>("toEtudiantRembourse");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl> TO_INSC_DIPL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl>("toInscDipl");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPaiement> TO_PAIEMENT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPaiement>("toPaiement");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte> TO_REPARTITION_COMPTE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte>("toRepartitionCompte");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite> TO_V_ETABLISSEMENT_SCOLARITE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite>("toVEtablissementScolarite");

	public static final String TO_ETUDIANT_REMBOURSE_KEY = "toEtudiantRembourse";
	public static final String TO_INSC_DIPL_KEY = "toInscDipl";
	public static final String TO_PAIEMENT_KEY = "toPaiement";
	public static final String TO_REPARTITION_COMPTE_KEY = "toRepartitionCompte";
	public static final String TO_V_ETABLISSEMENT_SCOLARITE_KEY = "toVEtablissementScolarite";

	// Create / Init methods

	/**
	 * Creates and inserts a new EODetailPaiement with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dpaieDateOperation
	 * @param dpaieMontant
	 * @param dpaieMouvement
	 * @param toInscDipl
	 * @param toRepartitionCompte
	 * @return EODetailPaiement
	 */
	public static EODetailPaiement create(EOEditingContext editingContext, NSTimestamp dpaieDateOperation, java.math.BigDecimal dpaieMontant, Integer dpaieMouvement, org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl, org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte) {
		EODetailPaiement eo = (EODetailPaiement) createAndInsertInstance(editingContext);
		eo.setDpaieDateOperation(dpaieDateOperation);
		eo.setDpaieMontant(dpaieMontant);
		eo.setDpaieMouvement(dpaieMouvement);
		eo.setToInscDiplRelationship(toInscDipl);
		eo.setToRepartitionCompteRelationship(toRepartitionCompte);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EODetailPaiement.
	 *
	 * @param editingContext
	 * @return EODetailPaiement
	 */
	public static EODetailPaiement create(EOEditingContext editingContext) {
		EODetailPaiement eo = (EODetailPaiement) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EODetailPaiement localInstanceIn(EOEditingContext editingContext) {
		EODetailPaiement localInstance = (EODetailPaiement) localInstanceOfObject(editingContext, (EODetailPaiement) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EODetailPaiement localInstanceIn(EOEditingContext editingContext, EODetailPaiement eo) {
		EODetailPaiement localInstance = (eo == null) ? null : (EODetailPaiement) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public NSTimestamp dpaieDateOperation() {
		return (NSTimestamp) storedValueForKey("dpaieDateOperation");
	}

	public void setDpaieDateOperation(NSTimestamp value) {
		takeStoredValueForKey(value, "dpaieDateOperation");
	}
	public java.math.BigDecimal dpaieMontant() {
		return (java.math.BigDecimal) storedValueForKey("dpaieMontant");
	}

	public void setDpaieMontant(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "dpaieMontant");
	}
	public Integer dpaieMouvement() {
		return (Integer) storedValueForKey("dpaieMouvement");
	}

	public void setDpaieMouvement(Integer value) {
		takeStoredValueForKey(value, "dpaieMouvement");
	}
	public Integer erembNumero() {
		return (Integer) storedValueForKey("erembNumero");
	}

	public void setErembNumero(Integer value) {
		takeStoredValueForKey(value, "erembNumero");
	}
	public Integer histAnneeScol() {
		return (Integer) storedValueForKey("histAnneeScol");
	}

	public void setHistAnneeScol(Integer value) {
		takeStoredValueForKey(value, "histAnneeScol");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse toEtudiantRembourse() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse)storedValueForKey("toEtudiantRembourse");
	}

	public void setToEtudiantRembourseRelationship(org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse oldValue = toEtudiantRembourse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEtudiantRembourse");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toEtudiantRembourse");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOInscDipl)storedValueForKey("toInscDipl");
	}

	public void setToInscDiplRelationship(org.cocktail.scolarix.serveur.metier.eos.EOInscDipl value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOInscDipl oldValue = toInscDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toInscDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toInscDipl");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOPaiement toPaiement() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOPaiement)storedValueForKey("toPaiement");
	}

	public void setToPaiementRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiement value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOPaiement oldValue = toPaiement();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPaiement");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toPaiement");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte)storedValueForKey("toRepartitionCompte");
	}

	public void setToRepartitionCompteRelationship(org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte oldValue = toRepartitionCompte();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartitionCompte");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRepartitionCompte");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite toVEtablissementScolarite() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite)storedValueForKey("toVEtablissementScolarite");
	}

	public void setToVEtablissementScolariteRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite oldValue = toVEtablissementScolarite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVEtablissementScolarite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toVEtablissementScolarite");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EODetailPaiement.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EODetailPaiement.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EODetailPaiement)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EODetailPaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODetailPaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EODetailPaiement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EODetailPaiement)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EODetailPaiement fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EODetailPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EODetailPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EODetailPaiement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EODetailPaiement)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EODetailPaiement fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EODetailPaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EODetailPaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EODetailPaiement ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EODetailPaiement createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EODetailPaiement.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EODetailPaiement.ENTITY_NAME + "' !");
		}
		else {
			EODetailPaiement object = (EODetailPaiement) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EODetailPaiement localInstanceOfObject(EOEditingContext ec, EODetailPaiement object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EODetailPaiement " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EODetailPaiement) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
