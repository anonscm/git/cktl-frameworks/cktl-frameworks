/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBac.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOBac extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_Bac";
	public static final String ENTITY_TABLE_NAME = "GRHUM.BAC";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "bacCode";

	public static final ERXKey<String> BAC_CODE = new ERXKey<String>("bacCode");
	public static final ERXKey<String> BAC_CODE_NATIONAL = new ERXKey<String>("bacCodeNational");
	public static final ERXKey<String> BAC_CODE_SISE = new ERXKey<String>("bacCodeSise");
	public static final ERXKey<Integer> BAC_DATE_INVALIDITE = new ERXKey<Integer>("bacDateInvalidite");
	public static final ERXKey<Integer> BAC_DATE_VALIDITE = new ERXKey<Integer>("bacDateValidite");
	public static final ERXKey<String> BAC_LIBELLE = new ERXKey<String>("bacLibelle");
	public static final ERXKey<String> BAC_TYPE = new ERXKey<String>("bacType");
	public static final ERXKey<String> BAC_VALIDITE = new ERXKey<String>("bacValidite");

	public static final String BAC_CODE_KEY = "bacCode";
	public static final String BAC_CODE_NATIONAL_KEY = "bacCodeNational";
	public static final String BAC_CODE_SISE_KEY = "bacCodeSise";
	public static final String BAC_DATE_INVALIDITE_KEY = "bacDateInvalidite";
	public static final String BAC_DATE_VALIDITE_KEY = "bacDateValidite";
	public static final String BAC_LIBELLE_KEY = "bacLibelle";
	public static final String BAC_TYPE_KEY = "bacType";
	public static final String BAC_VALIDITE_KEY = "bacValidite";

	// Non visible attributes

	// Colkeys
	public static final String BAC_CODE_COLKEY = "BAC_CODE";
	public static final String BAC_CODE_NATIONAL_COLKEY = "BAC_CODE_NATIONAL";
	public static final String BAC_CODE_SISE_COLKEY = "BAC_CODE_SISE";
	public static final String BAC_DATE_INVALIDITE_COLKEY = "BAC_DATE_INVALIDITE";
	public static final String BAC_DATE_VALIDITE_COLKEY = "BAC_DATE_VALIDITE";
	public static final String BAC_LIBELLE_COLKEY = "BAC_LIBELLE";
	public static final String BAC_TYPE_COLKEY = "BAC_TYPE";
	public static final String BAC_VALIDITE_COLKEY = "BAC_VALIDITE";

	// Non visible colkeys

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOBac with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param bacCode
	 * @param bacType
	 * @param bacValidite
	 * @return EOBac
	 */
	public static EOBac create(EOEditingContext editingContext, String bacCode, String bacType, String bacValidite) {
		EOBac eo = (EOBac) createAndInsertInstance(editingContext);
		eo.setBacCode(bacCode);
		eo.setBacType(bacType);
		eo.setBacValidite(bacValidite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOBac.
	 *
	 * @param editingContext
	 * @return EOBac
	 */
	public static EOBac create(EOEditingContext editingContext) {
		EOBac eo = (EOBac) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOBac localInstanceIn(EOEditingContext editingContext) {
		EOBac localInstance = (EOBac) localInstanceOfObject(editingContext, (EOBac) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOBac localInstanceIn(EOEditingContext editingContext, EOBac eo) {
		EOBac localInstance = (eo == null) ? null : (EOBac) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String bacCode() {
		return (String) storedValueForKey("bacCode");
	}

	public void setBacCode(String value) {
		takeStoredValueForKey(value, "bacCode");
	}
	public String bacCodeNational() {
		return (String) storedValueForKey("bacCodeNational");
	}

	public void setBacCodeNational(String value) {
		takeStoredValueForKey(value, "bacCodeNational");
	}
	public String bacCodeSise() {
		return (String) storedValueForKey("bacCodeSise");
	}

	public void setBacCodeSise(String value) {
		takeStoredValueForKey(value, "bacCodeSise");
	}
	public Integer bacDateInvalidite() {
		return (Integer) storedValueForKey("bacDateInvalidite");
	}

	public void setBacDateInvalidite(Integer value) {
		takeStoredValueForKey(value, "bacDateInvalidite");
	}
	public Integer bacDateValidite() {
		return (Integer) storedValueForKey("bacDateValidite");
	}

	public void setBacDateValidite(Integer value) {
		takeStoredValueForKey(value, "bacDateValidite");
	}
	public String bacLibelle() {
		return (String) storedValueForKey("bacLibelle");
	}

	public void setBacLibelle(String value) {
		takeStoredValueForKey(value, "bacLibelle");
	}
	public String bacType() {
		return (String) storedValueForKey("bacType");
	}

	public void setBacType(String value) {
		takeStoredValueForKey(value, "bacType");
	}
	public String bacValidite() {
		return (String) storedValueForKey("bacValidite");
	}

	public void setBacValidite(String value) {
		takeStoredValueForKey(value, "bacValidite");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOBac.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOBac.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOBac)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOBac fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBac fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOBac eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOBac)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOBac fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOBac fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOBac fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOBac eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOBac)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOBac fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOBac fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOBac eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOBac ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchRecherche(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("Recherche", "FwkScolarix_Bac");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EOBac createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOBac.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOBac.ENTITY_NAME + "' !");
		}
		else {
			EOBac object = (EOBac) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOBac localInstanceOfObject(EOEditingContext ec, EOBac object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOBac " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOBac) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
