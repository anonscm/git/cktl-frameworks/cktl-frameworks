/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateur.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOUtilisateur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_Utilisateur";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.UTILISATEUR";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "utiNumero";

	public static final ERXKey<Integer> UTI_ADM = new ERXKey<Integer>("utiAdm");
	public static final ERXKey<Integer> UTI_AJOUT = new ERXKey<Integer>("utiAjout");
	public static final ERXKey<Integer> UTI_BU = new ERXKey<Integer>("utiBu");
	public static final ERXKey<String> UTI_CHEQUE_SERVER_HOSTNAME = new ERXKey<String>("utiChequeServerHostname");
	public static final ERXKey<Integer> UTI_CHEQUE_SERVER_PORT = new ERXKey<Integer>("utiChequeServerPort");
	public static final ERXKey<Integer> UTI_CONSULT = new ERXKey<Integer>("utiConsult");
	public static final ERXKey<Integer> UTI_IDENTITE = new ERXKey<Integer>("utiIdentite");
	public static final ERXKey<Integer> UTI_INE = new ERXKey<Integer>("utiIne");
	public static final ERXKey<Integer> UTI_MODIF = new ERXKey<Integer>("utiModif");
	public static final ERXKey<Integer> UTI_PHOTO = new ERXKey<Integer>("utiPhoto");
	public static final ERXKey<Integer> UTI_SUPP = new ERXKey<Integer>("utiSupp");
	public static final ERXKey<String> UTI_USERNAME = new ERXKey<String>("utiUsername");

	public static final String UTI_ADM_KEY = "utiAdm";
	public static final String UTI_AJOUT_KEY = "utiAjout";
	public static final String UTI_BU_KEY = "utiBu";
	public static final String UTI_CHEQUE_SERVER_HOSTNAME_KEY = "utiChequeServerHostname";
	public static final String UTI_CHEQUE_SERVER_PORT_KEY = "utiChequeServerPort";
	public static final String UTI_CONSULT_KEY = "utiConsult";
	public static final String UTI_IDENTITE_KEY = "utiIdentite";
	public static final String UTI_INE_KEY = "utiIne";
	public static final String UTI_MODIF_KEY = "utiModif";
	public static final String UTI_PHOTO_KEY = "utiPhoto";
	public static final String UTI_SUPP_KEY = "utiSupp";
	public static final String UTI_USERNAME_KEY = "utiUsername";

	// Non visible attributes
	public static final String UTI_NUMERO_KEY = "utiNumero";

	// Colkeys
	public static final String UTI_ADM_COLKEY = "UTI_ADM";
	public static final String UTI_AJOUT_COLKEY = "UTI_AJOUT";
	public static final String UTI_BU_COLKEY = "UTI_BU";
	public static final String UTI_CHEQUE_SERVER_HOSTNAME_COLKEY = "UTI_CHEQUE_SERVER_HOSTNAME";
	public static final String UTI_CHEQUE_SERVER_PORT_COLKEY = "UTI_CHEQUE_SERVER_PORT";
	public static final String UTI_CONSULT_COLKEY = "UTI_CONSULT";
	public static final String UTI_IDENTITE_COLKEY = "UTI_IDENTITE";
	public static final String UTI_INE_COLKEY = "UTI_INE";
	public static final String UTI_MODIF_COLKEY = "UTI_MODIF";
	public static final String UTI_PHOTO_COLKEY = "UTI_PHOTO";
	public static final String UTI_SUPP_COLKEY = "UTI_SUPP";
	public static final String UTI_USERNAME_COLKEY = "UTI_USERNAME";

	// Non visible colkeys
	public static final String UTI_NUMERO_COLKEY = "UTI_NUMERO";

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOUtilisateur with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param utiAdm
	 * @param utiAjout
	 * @param utiBu
	 * @param utiChequeServerHostname
	 * @param utiChequeServerPort
	 * @param utiConsult
	 * @param utiIdentite
	 * @param utiIne
	 * @param utiModif
	 * @param utiPhoto
	 * @param utiSupp
	 * @param utiUsername
	 * @return EOUtilisateur
	 */
	public static EOUtilisateur create(EOEditingContext editingContext, Integer utiAdm, Integer utiAjout, Integer utiBu, String utiChequeServerHostname, Integer utiChequeServerPort, Integer utiConsult, Integer utiIdentite, Integer utiIne, Integer utiModif, Integer utiPhoto, Integer utiSupp, String utiUsername) {
		EOUtilisateur eo = (EOUtilisateur) createAndInsertInstance(editingContext);
		eo.setUtiAdm(utiAdm);
		eo.setUtiAjout(utiAjout);
		eo.setUtiBu(utiBu);
		eo.setUtiChequeServerHostname(utiChequeServerHostname);
		eo.setUtiChequeServerPort(utiChequeServerPort);
		eo.setUtiConsult(utiConsult);
		eo.setUtiIdentite(utiIdentite);
		eo.setUtiIne(utiIne);
		eo.setUtiModif(utiModif);
		eo.setUtiPhoto(utiPhoto);
		eo.setUtiSupp(utiSupp);
		eo.setUtiUsername(utiUsername);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOUtilisateur.
	 *
	 * @param editingContext
	 * @return EOUtilisateur
	 */
	public static EOUtilisateur create(EOEditingContext editingContext) {
		EOUtilisateur eo = (EOUtilisateur) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOUtilisateur localInstanceIn(EOEditingContext editingContext) {
		EOUtilisateur localInstance = (EOUtilisateur) localInstanceOfObject(editingContext, (EOUtilisateur) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOUtilisateur localInstanceIn(EOEditingContext editingContext, EOUtilisateur eo) {
		EOUtilisateur localInstance = (eo == null) ? null : (EOUtilisateur) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer utiAdm() {
		return (Integer) storedValueForKey("utiAdm");
	}

	public void setUtiAdm(Integer value) {
		takeStoredValueForKey(value, "utiAdm");
	}
	public Integer utiAjout() {
		return (Integer) storedValueForKey("utiAjout");
	}

	public void setUtiAjout(Integer value) {
		takeStoredValueForKey(value, "utiAjout");
	}
	public Integer utiBu() {
		return (Integer) storedValueForKey("utiBu");
	}

	public void setUtiBu(Integer value) {
		takeStoredValueForKey(value, "utiBu");
	}
	public String utiChequeServerHostname() {
		return (String) storedValueForKey("utiChequeServerHostname");
	}

	public void setUtiChequeServerHostname(String value) {
		takeStoredValueForKey(value, "utiChequeServerHostname");
	}
	public Integer utiChequeServerPort() {
		return (Integer) storedValueForKey("utiChequeServerPort");
	}

	public void setUtiChequeServerPort(Integer value) {
		takeStoredValueForKey(value, "utiChequeServerPort");
	}
	public Integer utiConsult() {
		return (Integer) storedValueForKey("utiConsult");
	}

	public void setUtiConsult(Integer value) {
		takeStoredValueForKey(value, "utiConsult");
	}
	public Integer utiIdentite() {
		return (Integer) storedValueForKey("utiIdentite");
	}

	public void setUtiIdentite(Integer value) {
		takeStoredValueForKey(value, "utiIdentite");
	}
	public Integer utiIne() {
		return (Integer) storedValueForKey("utiIne");
	}

	public void setUtiIne(Integer value) {
		takeStoredValueForKey(value, "utiIne");
	}
	public Integer utiModif() {
		return (Integer) storedValueForKey("utiModif");
	}

	public void setUtiModif(Integer value) {
		takeStoredValueForKey(value, "utiModif");
	}
	public Integer utiPhoto() {
		return (Integer) storedValueForKey("utiPhoto");
	}

	public void setUtiPhoto(Integer value) {
		takeStoredValueForKey(value, "utiPhoto");
	}
	public Integer utiSupp() {
		return (Integer) storedValueForKey("utiSupp");
	}

	public void setUtiSupp(Integer value) {
		takeStoredValueForKey(value, "utiSupp");
	}
	public String utiUsername() {
		return (String) storedValueForKey("utiUsername");
	}

	public void setUtiUsername(String value) {
		takeStoredValueForKey(value, "utiUsername");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOUtilisateur.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOUtilisateur.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOUtilisateur)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOUtilisateur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOUtilisateur eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOUtilisateur fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOUtilisateur eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOUtilisateur fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOUtilisateur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOUtilisateur eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOUtilisateur ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOUtilisateur createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOUtilisateur.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOUtilisateur.ENTITY_NAME + "' !");
		}
		else {
			EOUtilisateur object = (EOUtilisateur) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOUtilisateur localInstanceOfObject(EOEditingContext ec, EOUtilisateur object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOUtilisateur " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOUtilisateur) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
