// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPlancoGestion.java instead.
package org.cocktail.scolarix.serveur.metier.eos;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPlancoGestion extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PlancoGestion";

	// Attributes
	public static final String ETAB_CODE_KEY = "etabCode";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String ORG_UNIT_KEY = "orgUnit";
	public static final String PCO_NUM_KEY = "pcoNum";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOPlancoGestion.class);

  public EOPlancoGestion localInstanceIn(EOEditingContext editingContext) {
    EOPlancoGestion localInstance = (EOPlancoGestion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String etabCode() {
    return (String) storedValueForKey("etabCode");
  }

  public void setEtabCode(String value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating etabCode from " + etabCode() + " to " + value);
    }
    takeStoredValueForKey(value, "etabCode");
  }

  public String gesCode() {
    return (String) storedValueForKey("gesCode");
  }

  public void setGesCode(String value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating gesCode from " + gesCode() + " to " + value);
    }
    takeStoredValueForKey(value, "gesCode");
  }

  public String orgUnit() {
    return (String) storedValueForKey("orgUnit");
  }

  public void setOrgUnit(String value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating orgUnit from " + orgUnit() + " to " + value);
    }
    takeStoredValueForKey(value, "orgUnit");
  }

  public String pcoNum() {
    return (String) storedValueForKey("pcoNum");
  }

  public void setPcoNum(String value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating pcoNum from " + pcoNum() + " to " + value);
    }
    takeStoredValueForKey(value, "pcoNum");
  }


  public static EOPlancoGestion createFwkScolarix_PlancoGestion(EOEditingContext editingContext, String orgUnit
) {
    EOPlancoGestion eo = (EOPlancoGestion) EOUtilities.createAndInsertInstance(editingContext, _EOPlancoGestion.ENTITY_NAME);    
		eo.setOrgUnit(orgUnit);
    return eo;
  }

  public static NSArray<EOPlancoGestion> fetchAllFwkScolarix_PlancoGestions(EOEditingContext editingContext) {
    return _EOPlancoGestion.fetchAllFwkScolarix_PlancoGestions(editingContext, null);
  }

  public static NSArray<EOPlancoGestion> fetchAllFwkScolarix_PlancoGestions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPlancoGestion.fetchFwkScolarix_PlancoGestions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPlancoGestion> fetchFwkScolarix_PlancoGestions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPlancoGestion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPlancoGestion> eoObjects = (NSArray<EOPlancoGestion>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPlancoGestion fetchFwkScolarix_PlancoGestion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPlancoGestion.fetchFwkScolarix_PlancoGestion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPlancoGestion fetchFwkScolarix_PlancoGestion(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPlancoGestion> eoObjects = _EOPlancoGestion.fetchFwkScolarix_PlancoGestions(editingContext, qualifier, null);
    EOPlancoGestion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPlancoGestion)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkScolarix_PlancoGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPlancoGestion fetchRequiredFwkScolarix_PlancoGestion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPlancoGestion.fetchRequiredFwkScolarix_PlancoGestion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPlancoGestion fetchRequiredFwkScolarix_PlancoGestion(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPlancoGestion eoObject = _EOPlancoGestion.fetchFwkScolarix_PlancoGestion(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkScolarix_PlancoGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPlancoGestion localInstanceIn(EOEditingContext editingContext, EOPlancoGestion eo) {
    EOPlancoGestion localInstance = (eo == null) ? null : (EOPlancoGestion)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
