/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPaiementEcheance.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPaiementEcheance extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PaiementEcheance";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PAIEMENT_ECHEANCE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "pecheNumero";

	public static final ERXKey<NSTimestamp> PECHE_DATE = new ERXKey<NSTimestamp>("pecheDate");
	public static final ERXKey<java.math.BigDecimal> PECHE_MONTANT = new ERXKey<java.math.BigDecimal>("pecheMontant");
	public static final ERXKey<Integer> PECHE_ORDRE = new ERXKey<Integer>("pecheOrdre");
	public static final ERXKey<Integer> PECHR_NUMERO = new ERXKey<Integer>("pechrNumero");

	public static final String PECHE_DATE_KEY = "pecheDate";
	public static final String PECHE_MONTANT_KEY = "pecheMontant";
	public static final String PECHE_ORDRE_KEY = "pecheOrdre";
	public static final String PECHR_NUMERO_KEY = "pechrNumero";

	// Non visible attributes
	public static final String PECHE_NUMERO_KEY = "pecheNumero";

	// Colkeys
	public static final String PECHE_DATE_COLKEY = "PECHE_DATE";
	public static final String PECHE_MONTANT_COLKEY = "PECHE_MONTANT";
	public static final String PECHE_ORDRE_COLKEY = "PECHE_ORDRE";
	public static final String PECHR_NUMERO_COLKEY = "PECHR_NUMERO";

	// Non visible colkeys
	public static final String PECHE_NUMERO_COLKEY = "PECHE_NUMERO";

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOPaiementEcheance with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param pecheDate
	 * @param pecheMontant
	 * @param pecheOrdre
	 * @param pechrNumero
	 * @return EOPaiementEcheance
	 */
	public static EOPaiementEcheance create(EOEditingContext editingContext, NSTimestamp pecheDate, java.math.BigDecimal pecheMontant, Integer pecheOrdre, Integer pechrNumero) {
		EOPaiementEcheance eo = (EOPaiementEcheance) createAndInsertInstance(editingContext);
		eo.setPecheDate(pecheDate);
		eo.setPecheMontant(pecheMontant);
		eo.setPecheOrdre(pecheOrdre);
		eo.setPechrNumero(pechrNumero);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPaiementEcheance.
	 *
	 * @param editingContext
	 * @return EOPaiementEcheance
	 */
	public static EOPaiementEcheance create(EOEditingContext editingContext) {
		EOPaiementEcheance eo = (EOPaiementEcheance) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPaiementEcheance localInstanceIn(EOEditingContext editingContext) {
		EOPaiementEcheance localInstance = (EOPaiementEcheance) localInstanceOfObject(editingContext, (EOPaiementEcheance) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPaiementEcheance localInstanceIn(EOEditingContext editingContext, EOPaiementEcheance eo) {
		EOPaiementEcheance localInstance = (eo == null) ? null : (EOPaiementEcheance) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public NSTimestamp pecheDate() {
		return (NSTimestamp) storedValueForKey("pecheDate");
	}

	public void setPecheDate(NSTimestamp value) {
		takeStoredValueForKey(value, "pecheDate");
	}
	public java.math.BigDecimal pecheMontant() {
		return (java.math.BigDecimal) storedValueForKey("pecheMontant");
	}

	public void setPecheMontant(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "pecheMontant");
	}
	public Integer pecheOrdre() {
		return (Integer) storedValueForKey("pecheOrdre");
	}

	public void setPecheOrdre(Integer value) {
		takeStoredValueForKey(value, "pecheOrdre");
	}
	public Integer pechrNumero() {
		return (Integer) storedValueForKey("pechrNumero");
	}

	public void setPechrNumero(Integer value) {
		takeStoredValueForKey(value, "pechrNumero");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPaiementEcheance.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPaiementEcheance.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPaiementEcheance)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPaiementEcheance fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPaiementEcheance fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPaiementEcheance eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPaiementEcheance)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPaiementEcheance fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPaiementEcheance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPaiementEcheance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPaiementEcheance eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPaiementEcheance)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPaiementEcheance fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPaiementEcheance fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPaiementEcheance eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPaiementEcheance ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPaiementEcheance createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPaiementEcheance.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPaiementEcheance.ENTITY_NAME + "' !");
		}
		else {
			EOPaiementEcheance object = (EOPaiementEcheance) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPaiementEcheance localInstanceOfObject(EOEditingContext ec, EOPaiementEcheance object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPaiementEcheance " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPaiementEcheance) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
