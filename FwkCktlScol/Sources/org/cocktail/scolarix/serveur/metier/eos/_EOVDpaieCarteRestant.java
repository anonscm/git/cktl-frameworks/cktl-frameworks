/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVDpaieCarteRestant.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVDpaieCarteRestant extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VDpaieCarteRestant";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_DPAIE_CARTE_RESTANT";

	//Attributes

	public static final ERXKey<java.math.BigDecimal> DETAIL_MONTANT = new ERXKey<java.math.BigDecimal>("detailMontant");
	public static final ERXKey<String> LIBELLE_COMPTE = new ERXKey<String>("libelleCompte");
	public static final ERXKey<Integer> NUMERO_HISTORIQUE = new ERXKey<Integer>("numeroHistorique");
	public static final ERXKey<Integer> NUMERO_INSCRIPTION = new ERXKey<Integer>("numeroInscription");

	public static final String DETAIL_MONTANT_KEY = "detailMontant";
	public static final String LIBELLE_COMPTE_KEY = "libelleCompte";
	public static final String NUMERO_HISTORIQUE_KEY = "numeroHistorique";
	public static final String NUMERO_INSCRIPTION_KEY = "numeroInscription";

	// Non visible attributes
	public static final String ANNEE_SCOLAIRE_KEY = "anneeScolaire";
	public static final String CODE_COMPTE_KEY = "codeCompte";

	// Colkeys
	public static final String DETAIL_MONTANT_COLKEY = "DETAIL_MONTANT";
	public static final String LIBELLE_COMPTE_COLKEY = "LIBELLE_COMPTE";
	public static final String NUMERO_HISTORIQUE_COLKEY = "NUMERO_HISTORIQUE";
	public static final String NUMERO_INSCRIPTION_COLKEY = "NUMERO_INSCRIPTION";

	// Non visible colkeys
	public static final String ANNEE_SCOLAIRE_COLKEY = "ANNEE_SCOLAIRE";
	public static final String CODE_COMPTE_COLKEY = "CODE_COMPTE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique> TO_HISTORIQUE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique>("toHistorique");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl> TO_INSC_DIPL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl>("toInscDipl");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte> TO_REPARTITION_COMPTE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte>("toRepartitionCompte");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_HISTORIQUE_KEY = "toHistorique";
	public static final String TO_INSC_DIPL_KEY = "toInscDipl";
	public static final String TO_REPARTITION_COMPTE_KEY = "toRepartitionCompte";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVDpaieCarteRestant with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param numeroHistorique
	 * @param numeroInscription
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toHistorique
	 * @param toInscDipl
	 * @param toRepartitionCompte
	 * @return EOVDpaieCarteRestant
	 */
	public static EOVDpaieCarteRestant create(EOEditingContext editingContext, Integer numeroHistorique, Integer numeroInscription, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique, org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl, org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte) {
		EOVDpaieCarteRestant eo = (EOVDpaieCarteRestant) createAndInsertInstance(editingContext);
		eo.setNumeroHistorique(numeroHistorique);
		eo.setNumeroInscription(numeroInscription);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToHistoriqueRelationship(toHistorique);
		eo.setToInscDiplRelationship(toInscDipl);
		eo.setToRepartitionCompteRelationship(toRepartitionCompte);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVDpaieCarteRestant.
	 *
	 * @param editingContext
	 * @return EOVDpaieCarteRestant
	 */
	public static EOVDpaieCarteRestant create(EOEditingContext editingContext) {
		EOVDpaieCarteRestant eo = (EOVDpaieCarteRestant) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVDpaieCarteRestant localInstanceIn(EOEditingContext editingContext) {
		EOVDpaieCarteRestant localInstance = (EOVDpaieCarteRestant) localInstanceOfObject(editingContext, (EOVDpaieCarteRestant) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVDpaieCarteRestant localInstanceIn(EOEditingContext editingContext, EOVDpaieCarteRestant eo) {
		EOVDpaieCarteRestant localInstance = (eo == null) ? null : (EOVDpaieCarteRestant) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public java.math.BigDecimal detailMontant() {
		return (java.math.BigDecimal) storedValueForKey("detailMontant");
	}

	public void setDetailMontant(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "detailMontant");
	}
	public String libelleCompte() {
		return (String) storedValueForKey("libelleCompte");
	}

	public void setLibelleCompte(String value) {
		takeStoredValueForKey(value, "libelleCompte");
	}
	public Integer numeroHistorique() {
		return (Integer) storedValueForKey("numeroHistorique");
	}

	public void setNumeroHistorique(Integer value) {
		takeStoredValueForKey(value, "numeroHistorique");
	}
	public Integer numeroInscription() {
		return (Integer) storedValueForKey("numeroInscription");
	}

	public void setNumeroInscription(Integer value) {
		takeStoredValueForKey(value, "numeroInscription");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOHistorique)storedValueForKey("toHistorique");
	}

	public void setToHistoriqueRelationship(org.cocktail.scolarix.serveur.metier.eos.EOHistorique value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOHistorique oldValue = toHistorique();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toHistorique");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toHistorique");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOInscDipl)storedValueForKey("toInscDipl");
	}

	public void setToInscDiplRelationship(org.cocktail.scolarix.serveur.metier.eos.EOInscDipl value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOInscDipl oldValue = toInscDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toInscDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toInscDipl");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte)storedValueForKey("toRepartitionCompte");
	}

	public void setToRepartitionCompteRelationship(org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte oldValue = toRepartitionCompte();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartitionCompte");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRepartitionCompte");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVDpaieCarteRestant.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVDpaieCarteRestant.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVDpaieCarteRestant)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVDpaieCarteRestant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVDpaieCarteRestant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVDpaieCarteRestant eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVDpaieCarteRestant)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVDpaieCarteRestant fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVDpaieCarteRestant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVDpaieCarteRestant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVDpaieCarteRestant eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVDpaieCarteRestant)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVDpaieCarteRestant fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVDpaieCarteRestant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVDpaieCarteRestant eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVDpaieCarteRestant ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVDpaieCarteRestant createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVDpaieCarteRestant.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVDpaieCarteRestant.ENTITY_NAME + "' !");
		}
		else {
			EOVDpaieCarteRestant object = (EOVDpaieCarteRestant) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVDpaieCarteRestant localInstanceOfObject(EOEditingContext ec, EOVDpaieCarteRestant object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVDpaieCarteRestant " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVDpaieCarteRestant) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
