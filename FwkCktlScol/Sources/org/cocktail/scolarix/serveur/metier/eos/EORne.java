/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolarix.serveur.finder.FinderDepartement;
import org.cocktail.scolarix.serveur.finder.FinderSituationScol;

import com.webobjects.foundation.NSValidation;

public class EORne extends _EORne {

	public static final String TETAB_CODE_AUTRE_ETABLISSEMENT = "AE";
	public static final String TETAB_CODE_ECOLE_INGENIEUR = "EI";
	public static final String TETAB_CODE_ECOLE_MANAGEMENT = "EMA";
	public static final String TETAB_CODE_ENS_GRANDS_ETABLISSEMENTS = "ENS";
	public static final String TETAB_CODE_INSTITUT_CATHOLIQUE = "ICA";
	public static final String TETAB_CODE_IUFM = "IUF";
	public static final String TETAB_CODE_IUT = "IUT";
	public static final String TETAB_CODE_LYCEE_AGRICOLE = "LA";
	public static final String TETAB_CODE_LYCEE_GENERAL = "LG";
	public static final String TETAB_CODE_LYCEE_GENERAL_TECHNOLOGIQUE = "LGT";
	public static final String TETAB_CODE_LYCEE_PROFESSIONNEL = "LP";
	public static final String TETAB_CODE_LYCEE_POLYVALENT = "LPO";
	public static final String TETAB_CODE_LYCEE_TECHNOLOGIQUE = "LTO";
	public static final String TETAB_CODE_UNIVERSITE = "UNI";

	public EORne() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appel̩e.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public EODepartement departement() {
		EODepartement departement = null;
		// on traite a part le cas particulier de la Corse
		if (!StringCtrl.isEmpty(codePostal()) && codePostal().startsWith("20")) {
			// on regarde par rapport au cRne: 62 -> 02A ; 72 -> 02B
			if (cRne().startsWith("62")) {
				departement = FinderDepartement.getDepartementByKey(this.editingContext(), "02A");
			} else {
				departement = FinderDepartement.getDepartementByKey(this.editingContext(), "02B");
			}
		} else {
			departement = FinderDepartement.getDepartementByCodePostal(this.editingContext(), codePostal());
		}
		return departement;
	}

	/**
	 * Determination de la situation scolaire (situCode) en fonction du type etablissement (tetabCode).<BR>
	 * 
	 * @return Un EOSituationScol, default = situation scol UNIVERSITE (situCode = "H")
	 */
	public EOSituationScol situationScol() {
		String situCode = null;
		if (tetabCode() != null) {
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_UNIVERSITE)) {
				situCode = "H";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_IUT)) {
				situCode = "C";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_ECOLE_INGENIEUR)) {
				situCode = "E";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_IUFM)) {
				situCode = "F";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_ENS_GRANDS_ETABLISSEMENTS)) {
				situCode = "K";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_ECOLE_MANAGEMENT)) {
				situCode = "J";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_INSTITUT_CATHOLIQUE)) {
				situCode = "V";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_AUTRE_ETABLISSEMENT)) {
				situCode = "S";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_LYCEE_AGRICOLE)) {
				situCode = "A";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_LYCEE_GENERAL)) {
				situCode = "A";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_LYCEE_GENERAL_TECHNOLOGIQUE)) {
				situCode = "A";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_LYCEE_POLYVALENT)) {
				situCode = "A";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_LYCEE_PROFESSIONNEL)) {
				situCode = "A";
			}
			if (tetabCode().equalsIgnoreCase(TETAB_CODE_LYCEE_TECHNOLOGIQUE)) {
				situCode = "A";
			}
		}
		if (situCode != null) {
			return FinderSituationScol.getSituationScol(this.editingContext(), situCode);
		}
		return null;
	}
	
	public String libelleLong() {
	    return cRne() + " " + llRne();
	}
	
}
