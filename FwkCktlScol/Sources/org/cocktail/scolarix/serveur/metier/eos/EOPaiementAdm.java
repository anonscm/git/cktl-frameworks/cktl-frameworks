/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.metier.eos;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.IERXChainableQualifier;

public class EOPaiementAdm extends _EOPaiementAdm {

	public EOPaiementAdm() {
		super();
	}

	public static EOPaiementAdm create(EOEditingContext ec, EOCandidatGrhum candidat) {
		EOPaiementAdm p = candidat.createToPaiementAdmsRelationship();
		p.setCandNumero(new Integer(candidat.primaryKey()));
		p.setPadmAccompte(new Integer(0));
		p.setPadmAnneeScol(candidat.candAnneeScol());
		// initialisation du montant par défaut
		if (candidat.toFwkScolarite_ScolFormationSpecialisation() != null) {
			if (candidat.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome() != null) {
				String fdipTypeDroit = candidat.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipTypeDroit();
				if (fdipTypeDroit != null) {
					IERXChainableQualifier q = ERXQ.equals(EODroitsUniversite.DUNIV_ANNEE_SCOL_KEY, candidat.candAnneeScol());
					q = q.and(ERXQ.equals(EODroitsUniversite.RCPT_CODE_KEY, "ADM"));
					q = q.and(ERXQ.equals(EODroitsUniversite.DUNIV_TYPE_DROIT_KEY, fdipTypeDroit));
					EODroitsUniversite du = EODroitsUniversite.fetchFirstByQualifier(ec, (EOQualifier) q);
					if (du != null) {
						p.setPadmSomme(du.dunivMontant());
					}
				}
			}
		}
		return p;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele a partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
