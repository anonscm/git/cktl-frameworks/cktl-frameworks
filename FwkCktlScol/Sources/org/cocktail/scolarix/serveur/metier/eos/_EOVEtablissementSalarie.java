/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVEtablissementSalarie.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVEtablissementSalarie extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VEtablissementSalarie";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_ETABLISSEMENT_SALARIE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "cRne";

	public static final ERXKey<String> ADRESSE = new ERXKey<String>("adresse");
	public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
	public static final ERXKey<String> C_PAYS = new ERXKey<String>("cPays");
	public static final ERXKey<String> C_RNE = new ERXKey<String>("cRne");
	public static final ERXKey<String> LL_PAYS = new ERXKey<String>("llPays");
	public static final ERXKey<String> LL_RNE = new ERXKey<String>("llRne");
	public static final ERXKey<String> VILLE = new ERXKey<String>("ville");

	public static final String ADRESSE_KEY = "adresse";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String C_PAYS_KEY = "cPays";
	public static final String C_RNE_KEY = "cRne";
	public static final String LL_PAYS_KEY = "llPays";
	public static final String LL_RNE_KEY = "llRne";
	public static final String VILLE_KEY = "ville";

	// Non visible attributes

	// Colkeys
	public static final String ADRESSE_COLKEY = "ADRESSE";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String C_PAYS_COLKEY = "C_PAYS";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String LL_PAYS_COLKEY = "LL_PAYS";
	public static final String LL_RNE_COLKEY = "LL_RNE";
	public static final String VILLE_COLKEY = "VILLE";

	// Non visible colkeys

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOVEtablissementSalarie with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cRne
	 * @return EOVEtablissementSalarie
	 */
	public static EOVEtablissementSalarie create(EOEditingContext editingContext, String cRne) {
		EOVEtablissementSalarie eo = (EOVEtablissementSalarie) createAndInsertInstance(editingContext);
		eo.setCRne(cRne);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVEtablissementSalarie.
	 *
	 * @param editingContext
	 * @return EOVEtablissementSalarie
	 */
	public static EOVEtablissementSalarie create(EOEditingContext editingContext) {
		EOVEtablissementSalarie eo = (EOVEtablissementSalarie) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVEtablissementSalarie localInstanceIn(EOEditingContext editingContext) {
		EOVEtablissementSalarie localInstance = (EOVEtablissementSalarie) localInstanceOfObject(editingContext, (EOVEtablissementSalarie) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVEtablissementSalarie localInstanceIn(EOEditingContext editingContext, EOVEtablissementSalarie eo) {
		EOVEtablissementSalarie localInstance = (eo == null) ? null : (EOVEtablissementSalarie) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String adresse() {
		return (String) storedValueForKey("adresse");
	}

	public void setAdresse(String value) {
		takeStoredValueForKey(value, "adresse");
	}
	public String codePostal() {
		return (String) storedValueForKey("codePostal");
	}

	public void setCodePostal(String value) {
		takeStoredValueForKey(value, "codePostal");
	}
	public String cPays() {
		return (String) storedValueForKey("cPays");
	}

	public void setCPays(String value) {
		takeStoredValueForKey(value, "cPays");
	}
	public String cRne() {
		return (String) storedValueForKey("cRne");
	}

	public void setCRne(String value) {
		takeStoredValueForKey(value, "cRne");
	}
	public String llPays() {
		return (String) storedValueForKey("llPays");
	}

	public void setLlPays(String value) {
		takeStoredValueForKey(value, "llPays");
	}
	public String llRne() {
		return (String) storedValueForKey("llRne");
	}

	public void setLlRne(String value) {
		takeStoredValueForKey(value, "llRne");
	}
	public String ville() {
		return (String) storedValueForKey("ville");
	}

	public void setVille(String value) {
		takeStoredValueForKey(value, "ville");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVEtablissementSalarie.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVEtablissementSalarie.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVEtablissementSalarie)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVEtablissementSalarie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVEtablissementSalarie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVEtablissementSalarie eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVEtablissementSalarie)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVEtablissementSalarie fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVEtablissementSalarie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVEtablissementSalarie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVEtablissementSalarie eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVEtablissementSalarie)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVEtablissementSalarie fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVEtablissementSalarie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVEtablissementSalarie eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVEtablissementSalarie ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVEtablissementSalarie createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVEtablissementSalarie.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVEtablissementSalarie.ENTITY_NAME + "' !");
		}
		else {
			EOVEtablissementSalarie object = (EOVEtablissementSalarie) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVEtablissementSalarie localInstanceOfObject(EOEditingContext ec, EOVEtablissementSalarie object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVEtablissementSalarie " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVEtablissementSalarie) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
