/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreCandidat.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPreCandidat extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PreCandidat";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PRE_CANDIDAT";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "candKey";

	public static final ERXKey<String> CAND_ADR1_PARENT = new ERXKey<String>("candAdr1Parent");
	public static final ERXKey<String> CAND_ADR1_SCOL = new ERXKey<String>("candAdr1Scol");
	public static final ERXKey<String> CAND_ADR2_PARENT = new ERXKey<String>("candAdr2Parent");
	public static final ERXKey<String> CAND_ADR2_SCOL = new ERXKey<String>("candAdr2Scol");
	public static final ERXKey<Integer> CAND_ANBAC = new ERXKey<Integer>("candAnbac");
	public static final ERXKey<String> CAND_BEA = new ERXKey<String>("candBea");
	public static final ERXKey<String> CAND_CIVILITE = new ERXKey<String>("candCivilite");
	public static final ERXKey<String> CAND_COMNAIS = new ERXKey<String>("candComnais");
	public static final ERXKey<String> CAND_CP_PARENT = new ERXKey<String>("candCpParent");
	public static final ERXKey<String> CAND_CP_SCOL = new ERXKey<String>("candCpScol");
	public static final ERXKey<NSTimestamp> CAND_DATE_NAIS = new ERXKey<NSTimestamp>("candDateNais");
	public static final ERXKey<String> CAND_EMAIL_SCOL = new ERXKey<String>("candEmailScol");
	public static final ERXKey<String> CAND_NOM = new ERXKey<String>("candNom");
	public static final ERXKey<Integer> CAND_NUMERO = new ERXKey<Integer>("candNumero");
	public static final ERXKey<String> CAND_PAYS_PARENT = new ERXKey<String>("candPaysParent");
	public static final ERXKey<String> CAND_PAYS_SCOL = new ERXKey<String>("candPaysScol");
	public static final ERXKey<String> CAND_PORT_SCOL = new ERXKey<String>("candPortScol");
	public static final ERXKey<String> CAND_PRENOM = new ERXKey<String>("candPrenom");
	public static final ERXKey<String> CAND_PRENOM2 = new ERXKey<String>("candPrenom2");
	public static final ERXKey<String> CAND_TEL_PARENT = new ERXKey<String>("candTelParent");
	public static final ERXKey<String> CAND_TEL_SCOL = new ERXKey<String>("candTelScol");
	public static final ERXKey<String> CAND_VILLE_BAC = new ERXKey<String>("candVilleBac");
	public static final ERXKey<String> CAND_VILLE_PARENT = new ERXKey<String>("candVilleParent");
	public static final ERXKey<String> CAND_VILLE_SCOL = new ERXKey<String>("candVilleScol");
	public static final ERXKey<String> ETAB_LIBELLE_BAC = new ERXKey<String>("etabLibelleBac");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<String> HIST_ENS_DER_ETAB = new ERXKey<String>("histEnsDerEtab");
	public static final ERXKey<String> HIST_LIBELLE_DER_ETAB = new ERXKey<String>("histLibelleDerEtab");
	public static final ERXKey<String> HIST_VILLE_DER_ETAB = new ERXKey<String>("histVilleDerEtab");

	public static final String CAND_ADR1_PARENT_KEY = "candAdr1Parent";
	public static final String CAND_ADR1_SCOL_KEY = "candAdr1Scol";
	public static final String CAND_ADR2_PARENT_KEY = "candAdr2Parent";
	public static final String CAND_ADR2_SCOL_KEY = "candAdr2Scol";
	public static final String CAND_ANBAC_KEY = "candAnbac";
	public static final String CAND_BEA_KEY = "candBea";
	public static final String CAND_CIVILITE_KEY = "candCivilite";
	public static final String CAND_COMNAIS_KEY = "candComnais";
	public static final String CAND_CP_PARENT_KEY = "candCpParent";
	public static final String CAND_CP_SCOL_KEY = "candCpScol";
	public static final String CAND_DATE_NAIS_KEY = "candDateNais";
	public static final String CAND_EMAIL_SCOL_KEY = "candEmailScol";
	public static final String CAND_NOM_KEY = "candNom";
	public static final String CAND_NUMERO_KEY = "candNumero";
	public static final String CAND_PAYS_PARENT_KEY = "candPaysParent";
	public static final String CAND_PAYS_SCOL_KEY = "candPaysScol";
	public static final String CAND_PORT_SCOL_KEY = "candPortScol";
	public static final String CAND_PRENOM_KEY = "candPrenom";
	public static final String CAND_PRENOM2_KEY = "candPrenom2";
	public static final String CAND_TEL_PARENT_KEY = "candTelParent";
	public static final String CAND_TEL_SCOL_KEY = "candTelScol";
	public static final String CAND_VILLE_BAC_KEY = "candVilleBac";
	public static final String CAND_VILLE_PARENT_KEY = "candVilleParent";
	public static final String CAND_VILLE_SCOL_KEY = "candVilleScol";
	public static final String ETAB_LIBELLE_BAC_KEY = "etabLibelleBac";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String HIST_ENS_DER_ETAB_KEY = "histEnsDerEtab";
	public static final String HIST_LIBELLE_DER_ETAB_KEY = "histLibelleDerEtab";
	public static final String HIST_VILLE_DER_ETAB_KEY = "histVilleDerEtab";

	// Non visible attributes
	public static final String ACAD_ETAB_BAC_KEY = "acadEtabBac";
	public static final String BAC_CODE_KEY = "bacCode";
	public static final String CAND_KEY_KEY = "candKey";
	public static final String C_PAYS_ETAB_BAC_KEY = "cPaysEtabBac";
	public static final String DPTG_CODE_NAIS_KEY = "dptgCodeNais";
	public static final String DPTG_ETAB_BAC_KEY = "dptgEtabBac";
	public static final String ETAB_CODE_BAC_KEY = "etabCodeBac";
	public static final String NAT_ORDRE_KEY = "natOrdre";
	public static final String PAYS_CODE_NAIS_KEY = "paysCodeNais";
	public static final String C_RNE_KEY = "cRne";
	public static final String DPTG_CODE_DER_ETAB_KEY = "dptgCodeDerEtab";
	public static final String ETAB_CODE_DER_ETAB_KEY = "etabCodeDerEtab";
	public static final String PAYS_CODE_DER_ETAB_KEY = "paysCodeDerEtab";

	// Colkeys
	public static final String CAND_ADR1_PARENT_COLKEY = "CAND_ADR1_PARENT";
	public static final String CAND_ADR1_SCOL_COLKEY = "CAND_ADR1_SCOL";
	public static final String CAND_ADR2_PARENT_COLKEY = "CAND_ADR2_PARENT";
	public static final String CAND_ADR2_SCOL_COLKEY = "CAND_ADR2_SCOL";
	public static final String CAND_ANBAC_COLKEY = "CAND_ANBAC";
	public static final String CAND_BEA_COLKEY = "CAND_BEA";
	public static final String CAND_CIVILITE_COLKEY = "CAND_CIVILITE";
	public static final String CAND_COMNAIS_COLKEY = "CAND_COMNAIS";
	public static final String CAND_CP_PARENT_COLKEY = "CAND_CP_PARENT";
	public static final String CAND_CP_SCOL_COLKEY = "CAND_CP_SCOL";
	public static final String CAND_DATE_NAIS_COLKEY = "CAND_DATE_NAIS";
	public static final String CAND_EMAIL_SCOL_COLKEY = "CAND_EMAIL_SCOL";
	public static final String CAND_NOM_COLKEY = "CAND_NOM";
	public static final String CAND_NUMERO_COLKEY = "CAND_NUMERO";
	public static final String CAND_PAYS_PARENT_COLKEY = "CAND_PAYS_PARENT";
	public static final String CAND_PAYS_SCOL_COLKEY = "CAND_PAYS_SCOL";
	public static final String CAND_PORT_SCOL_COLKEY = "CAND_PORT_SCOL";
	public static final String CAND_PRENOM_COLKEY = "CAND_PRENOM";
	public static final String CAND_PRENOM2_COLKEY = "CAND_PRENOM2";
	public static final String CAND_TEL_PARENT_COLKEY = "CAND_TEL_PARENT";
	public static final String CAND_TEL_SCOL_COLKEY = "CAND_TEL_SCOL";
	public static final String CAND_VILLE_BAC_COLKEY = "CAND_VILLE_BAC";
	public static final String CAND_VILLE_PARENT_COLKEY = "CAND_VILLE_PARENT";
	public static final String CAND_VILLE_SCOL_COLKEY = "CAND_VILLE_SCOL";
	public static final String ETAB_LIBELLE_BAC_COLKEY = "ETAB_LIBELLE_BAC";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String HIST_ENS_DER_ETAB_COLKEY = "HIST_ENS_DER_ETAB";
	public static final String HIST_LIBELLE_DER_ETAB_COLKEY = "HIST_LIBELLE_DER_ETAB";
	public static final String HIST_VILLE_DER_ETAB_COLKEY = "HIST_VILLE_DER_ETAB";

	// Non visible colkeys
	public static final String ACAD_ETAB_BAC_COLKEY = "ACAD_ETAB_BAC";
	public static final String BAC_CODE_COLKEY = "BAC_CODE";
	public static final String CAND_KEY_COLKEY = "CAND_KEY";
	public static final String C_PAYS_ETAB_BAC_COLKEY = "PAYS_ETAB_BAC";
	public static final String DPTG_CODE_NAIS_COLKEY = "DPTG_CODE_NAIS";
	public static final String DPTG_ETAB_BAC_COLKEY = "DPTG_ETAB_BAC";
	public static final String ETAB_CODE_BAC_COLKEY = "ETAB_CODE_BAC";
	public static final String NAT_ORDRE_COLKEY = "NAT_ORDRE";
	public static final String PAYS_CODE_NAIS_COLKEY = "PAYS_CODE_NAIS";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String DPTG_CODE_DER_ETAB_COLKEY = "DPTG_CODE_DER_ETAB";
	public static final String ETAB_CODE_DER_ETAB_COLKEY = "ETAB_CODE_DER_ETAB";
	public static final String PAYS_CODE_DER_ETAB_COLKEY = "PAYS_CODE_DER_ETAB";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBac> TO_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBac>("toBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiant> TO_ETUDIANT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiant>("toEtudiant");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie> TO_FWKPERS__ACADEMIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie>("toFwkpers_Academie");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite> TO_FWKPERS__CIVILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite>("toFwkpers_Civilite");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__DER_ETAB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_DerEtab");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_EtabBac");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__NAISSANCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_Naissance");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__ADR_PARENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_AdrParent");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__ADR_SCOL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_AdrScol");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__DER_ETAB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_DerEtab");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_EtabBac");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__NAISSANCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_Naissance");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__NATIONALITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_Nationalite");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature> TO_PRE_CANDIDATURES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature>("toPreCandidatures");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant> TO_PRE_ETUDIANTS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant>("toPreEtudiants");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant> TO_PRE_ETUDIANTS_POST_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant>("toPreEtudiantsPostBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRne");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_CODE_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneCodeBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_DER_ETAB = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneDerEtab");

	public static final String TO_BAC_KEY = "toBac";
	public static final String TO_ETUDIANT_KEY = "toEtudiant";
	public static final String TO_FWKPERS__ACADEMIE_KEY = "toFwkpers_Academie";
	public static final String TO_FWKPERS__CIVILITE_KEY = "toFwkpers_Civilite";
	public static final String TO_FWKPERS__DEPARTEMENT__DER_ETAB_KEY = "toFwkpers_Departement_DerEtab";
	public static final String TO_FWKPERS__DEPARTEMENT__ETAB_BAC_KEY = "toFwkpers_Departement_EtabBac";
	public static final String TO_FWKPERS__DEPARTEMENT__NAISSANCE_KEY = "toFwkpers_Departement_Naissance";
	public static final String TO_FWKPERS__PAYS__ADR_PARENT_KEY = "toFwkpers_Pays_AdrParent";
	public static final String TO_FWKPERS__PAYS__ADR_SCOL_KEY = "toFwkpers_Pays_AdrScol";
	public static final String TO_FWKPERS__PAYS__DER_ETAB_KEY = "toFwkpers_Pays_DerEtab";
	public static final String TO_FWKPERS__PAYS__ETAB_BAC_KEY = "toFwkpers_Pays_EtabBac";
	public static final String TO_FWKPERS__PAYS__NAISSANCE_KEY = "toFwkpers_Pays_Naissance";
	public static final String TO_FWKPERS__PAYS__NATIONALITE_KEY = "toFwkpers_Pays_Nationalite";
	public static final String TO_PRE_CANDIDATURES_KEY = "toPreCandidatures";
	public static final String TO_PRE_ETUDIANTS_KEY = "toPreEtudiants";
	public static final String TO_PRE_ETUDIANTS_POST_BAC_KEY = "toPreEtudiantsPostBac";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_RNE_CODE_BAC_KEY = "toRneCodeBac";
	public static final String TO_RNE_DER_ETAB_KEY = "toRneDerEtab";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPreCandidat with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @return EOPreCandidat
	 */
	public static EOPreCandidat create(EOEditingContext editingContext) {
		EOPreCandidat eo = (EOPreCandidat) createAndInsertInstance(editingContext);
		return eo;
	}


	// Utilities methods

	public EOPreCandidat localInstanceIn(EOEditingContext editingContext) {
		EOPreCandidat localInstance = (EOPreCandidat) localInstanceOfObject(editingContext, (EOPreCandidat) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPreCandidat localInstanceIn(EOEditingContext editingContext, EOPreCandidat eo) {
		EOPreCandidat localInstance = (eo == null) ? null : (EOPreCandidat) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String candAdr1Parent() {
		return (String) storedValueForKey("candAdr1Parent");
	}

	public void setCandAdr1Parent(String value) {
		takeStoredValueForKey(value, "candAdr1Parent");
	}
	public String candAdr1Scol() {
		return (String) storedValueForKey("candAdr1Scol");
	}

	public void setCandAdr1Scol(String value) {
		takeStoredValueForKey(value, "candAdr1Scol");
	}
	public String candAdr2Parent() {
		return (String) storedValueForKey("candAdr2Parent");
	}

	public void setCandAdr2Parent(String value) {
		takeStoredValueForKey(value, "candAdr2Parent");
	}
	public String candAdr2Scol() {
		return (String) storedValueForKey("candAdr2Scol");
	}

	public void setCandAdr2Scol(String value) {
		takeStoredValueForKey(value, "candAdr2Scol");
	}
	public Integer candAnbac() {
		return (Integer) storedValueForKey("candAnbac");
	}

	public void setCandAnbac(Integer value) {
		takeStoredValueForKey(value, "candAnbac");
	}
	public String candBea() {
		return (String) storedValueForKey("candBea");
	}

	public void setCandBea(String value) {
		takeStoredValueForKey(value, "candBea");
	}
	public String candCivilite() {
		return (String) storedValueForKey("candCivilite");
	}

	public void setCandCivilite(String value) {
		takeStoredValueForKey(value, "candCivilite");
	}
	public String candComnais() {
		return (String) storedValueForKey("candComnais");
	}

	public void setCandComnais(String value) {
		takeStoredValueForKey(value, "candComnais");
	}
	public String candCpParent() {
		return (String) storedValueForKey("candCpParent");
	}

	public void setCandCpParent(String value) {
		takeStoredValueForKey(value, "candCpParent");
	}
	public String candCpScol() {
		return (String) storedValueForKey("candCpScol");
	}

	public void setCandCpScol(String value) {
		takeStoredValueForKey(value, "candCpScol");
	}
	public NSTimestamp candDateNais() {
		return (NSTimestamp) storedValueForKey("candDateNais");
	}

	public void setCandDateNais(NSTimestamp value) {
		takeStoredValueForKey(value, "candDateNais");
	}
	public String candEmailScol() {
		return (String) storedValueForKey("candEmailScol");
	}

	public void setCandEmailScol(String value) {
		takeStoredValueForKey(value, "candEmailScol");
	}
	public String candNom() {
		return (String) storedValueForKey("candNom");
	}

	public void setCandNom(String value) {
		takeStoredValueForKey(value, "candNom");
	}
	public Integer candNumero() {
		return (Integer) storedValueForKey("candNumero");
	}

	public void setCandNumero(Integer value) {
		takeStoredValueForKey(value, "candNumero");
	}
	public String candPaysParent() {
		return (String) storedValueForKey("candPaysParent");
	}

	public void setCandPaysParent(String value) {
		takeStoredValueForKey(value, "candPaysParent");
	}
	public String candPaysScol() {
		return (String) storedValueForKey("candPaysScol");
	}

	public void setCandPaysScol(String value) {
		takeStoredValueForKey(value, "candPaysScol");
	}
	public String candPortScol() {
		return (String) storedValueForKey("candPortScol");
	}

	public void setCandPortScol(String value) {
		takeStoredValueForKey(value, "candPortScol");
	}
	public String candPrenom() {
		return (String) storedValueForKey("candPrenom");
	}

	public void setCandPrenom(String value) {
		takeStoredValueForKey(value, "candPrenom");
	}
	public String candPrenom2() {
		return (String) storedValueForKey("candPrenom2");
	}

	public void setCandPrenom2(String value) {
		takeStoredValueForKey(value, "candPrenom2");
	}
	public String candTelParent() {
		return (String) storedValueForKey("candTelParent");
	}

	public void setCandTelParent(String value) {
		takeStoredValueForKey(value, "candTelParent");
	}
	public String candTelScol() {
		return (String) storedValueForKey("candTelScol");
	}

	public void setCandTelScol(String value) {
		takeStoredValueForKey(value, "candTelScol");
	}
	public String candVilleBac() {
		return (String) storedValueForKey("candVilleBac");
	}

	public void setCandVilleBac(String value) {
		takeStoredValueForKey(value, "candVilleBac");
	}
	public String candVilleParent() {
		return (String) storedValueForKey("candVilleParent");
	}

	public void setCandVilleParent(String value) {
		takeStoredValueForKey(value, "candVilleParent");
	}
	public String candVilleScol() {
		return (String) storedValueForKey("candVilleScol");
	}

	public void setCandVilleScol(String value) {
		takeStoredValueForKey(value, "candVilleScol");
	}
	public String etabLibelleBac() {
		return (String) storedValueForKey("etabLibelleBac");
	}

	public void setEtabLibelleBac(String value) {
		takeStoredValueForKey(value, "etabLibelleBac");
	}
	public Integer etudNumero() {
		return (Integer) storedValueForKey("etudNumero");
	}

	public void setEtudNumero(Integer value) {
		takeStoredValueForKey(value, "etudNumero");
	}
	public String histEnsDerEtab() {
		return (String) storedValueForKey("histEnsDerEtab");
	}

	public void setHistEnsDerEtab(String value) {
		takeStoredValueForKey(value, "histEnsDerEtab");
	}
	public String histLibelleDerEtab() {
		return (String) storedValueForKey("histLibelleDerEtab");
	}

	public void setHistLibelleDerEtab(String value) {
		takeStoredValueForKey(value, "histLibelleDerEtab");
	}
	public String histVilleDerEtab() {
		return (String) storedValueForKey("histVilleDerEtab");
	}

	public void setHistVilleDerEtab(String value) {
		takeStoredValueForKey(value, "histVilleDerEtab");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOBac toBac() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOBac)storedValueForKey("toBac");
	}

	public void setToBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EOBac value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOBac oldValue = toBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toBac");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOEtudiant toEtudiant() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOEtudiant)storedValueForKey("toEtudiant");
	}

	public void setToEtudiantRelationship(org.cocktail.scolarix.serveur.metier.eos.EOEtudiant value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOEtudiant oldValue = toEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toEtudiant");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOAcademie toFwkpers_Academie() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAcademie)storedValueForKey("toFwkpers_Academie");
	}

	public void setToFwkpers_AcademieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAcademie value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAcademie oldValue = toFwkpers_Academie();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Academie");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Academie");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toFwkpers_Civilite() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOCivilite)storedValueForKey("toFwkpers_Civilite");
	}

	public void setToFwkpers_CiviliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCivilite value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOCivilite oldValue = toFwkpers_Civilite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Civilite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Civilite");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_DerEtab() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_DerEtab");
	}

	public void setToFwkpers_Departement_DerEtabRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_DerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_DerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_DerEtab");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_EtabBac() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_EtabBac");
	}

	public void setToFwkpers_Departement_EtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_EtabBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_EtabBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_EtabBac");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_Naissance() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_Naissance");
	}

	public void setToFwkpers_Departement_NaissanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_Naissance();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_Naissance");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_Naissance");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_AdrParent() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_AdrParent");
	}

	public void setToFwkpers_Pays_AdrParentRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_AdrParent();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_AdrParent");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_AdrParent");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_AdrScol() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_AdrScol");
	}

	public void setToFwkpers_Pays_AdrScolRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_AdrScol();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_AdrScol");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_AdrScol");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_DerEtab() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_DerEtab");
	}

	public void setToFwkpers_Pays_DerEtabRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_DerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_DerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_DerEtab");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_EtabBac() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_EtabBac");
	}

	public void setToFwkpers_Pays_EtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_EtabBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_EtabBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_EtabBac");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Naissance() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_Naissance");
	}

	public void setToFwkpers_Pays_NaissanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_Naissance();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_Naissance");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_Naissance");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Nationalite() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_Nationalite");
	}

	public void setToFwkpers_Pays_NationaliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_Nationalite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_Nationalite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_Nationalite");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRne() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRne");
	}

	public void setToRneRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRne();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneCodeBac() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneCodeBac");
	}

	public void setToRneCodeBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneCodeBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneCodeBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneCodeBac");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneDerEtab() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneDerEtab");
	}

	public void setToRneDerEtabRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneDerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneDerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneDerEtab");
		}
	}
  
	public NSArray toPreCandidatures() {
		return (NSArray)storedValueForKey("toPreCandidatures");
	}

	public NSArray toPreCandidatures(EOQualifier qualifier) {
		return toPreCandidatures(qualifier, null);
	}

	public NSArray toPreCandidatures(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPreCandidatures();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPreCandidaturesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPreCandidatures");
	}

	public void removeFromToPreCandidaturesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreCandidatures");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature createToPreCandidaturesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PreCandidature");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPreCandidatures");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature) eo;
	}

	public void deleteToPreCandidaturesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreCandidatures");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPreCandidaturesRelationships() {
		Enumeration objects = toPreCandidatures().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPreCandidaturesRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPreCandidature)objects.nextElement());
		}
	}
	public NSArray toPreEtudiants() {
		return (NSArray)storedValueForKey("toPreEtudiants");
	}

	public NSArray toPreEtudiants(EOQualifier qualifier) {
		return toPreEtudiants(qualifier, null);
	}

	public NSArray toPreEtudiants(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPreEtudiants();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPreEtudiantsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPreEtudiants");
	}

	public void removeFromToPreEtudiantsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreEtudiants");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant createToPreEtudiantsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PreEtudiant");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPreEtudiants");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant) eo;
	}

	public void deleteToPreEtudiantsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreEtudiants");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPreEtudiantsRelationships() {
		Enumeration objects = toPreEtudiants().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPreEtudiantsRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant)objects.nextElement());
		}
	}
	public NSArray toPreEtudiantsPostBac() {
		return (NSArray)storedValueForKey("toPreEtudiantsPostBac");
	}

	public NSArray toPreEtudiantsPostBac(EOQualifier qualifier) {
		return toPreEtudiantsPostBac(qualifier, null);
	}

	public NSArray toPreEtudiantsPostBac(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPreEtudiantsPostBac();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPreEtudiantsPostBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPreEtudiantsPostBac");
	}

	public void removeFromToPreEtudiantsPostBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreEtudiantsPostBac");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant createToPreEtudiantsPostBacRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PreEtudiant");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPreEtudiantsPostBac");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant) eo;
	}

	public void deleteToPreEtudiantsPostBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreEtudiantsPostBac");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPreEtudiantsPostBacRelationships() {
		Enumeration objects = toPreEtudiantsPostBac().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPreEtudiantsPostBacRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPreCandidat.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPreCandidat.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPreCandidat)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPreCandidat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreCandidat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPreCandidat eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPreCandidat)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPreCandidat fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreCandidat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreCandidat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPreCandidat eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPreCandidat)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreCandidat fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreCandidat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPreCandidat eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPreCandidat ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPreCandidat createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPreCandidat.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPreCandidat.ENTITY_NAME + "' !");
		}
		else {
			EOPreCandidat object = (EOPreCandidat) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPreCandidat localInstanceOfObject(EOEditingContext ec, EOPreCandidat object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPreCandidat " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPreCandidat) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
