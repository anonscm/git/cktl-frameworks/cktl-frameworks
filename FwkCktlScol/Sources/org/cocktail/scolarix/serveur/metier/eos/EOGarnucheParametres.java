/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOGarnucheParametres extends _EOGarnucheParametres {

	public static final String GARNUCHE_ANNEE_CIVILE = "GARNUCHE_ANNEE_CIVILE";
	public static final String GARNUCHE_INTERRUPTION_MAXIMALE = "GARNUCHE_INTERRUPTION_MAXIMALE";
	public static final String GARNUCHE_PAIEMENT_SPECIAL = "GARNUCHE_PAIEMENT_SPECIAL";
	public static final String GARNUCHE_INIT_INDPHOTO = "GARNUCHE_INIT_INDPHOTO";
	public static final String GARNUCHE_UNIVERSITE = "GARNUCHE_UNIVERSITE";
	public static final String GARNUCHE_VILLE = "GARNUCHE_VILLE";
	public static final String GARNUCHE_SCOLARITE = "GARNUCHE_SCOLARITE";

	public EOGarnucheParametres() {
		super();
	}

	public static String getGarnucheUniversite(EOEditingContext ec) {
		return getParam(ec, GARNUCHE_UNIVERSITE);
	}

	public static String getGarnucheVille(EOEditingContext ec) {
		return getParam(ec, GARNUCHE_VILLE);
	}

	public static String getGarnucheServiceScolAffichage(EOEditingContext ec, String cRne) {
		EOStructure struct = getGarnucheServiceScolarite(ec, cRne);
		if (struct != null) {
			return struct.strAffichage();
		}
		return null;
	}

	public static String getGarnucheResponsableScol(EOEditingContext ec, String cRne) {
		EOStructure struct = getGarnucheServiceScolarite(ec, cRne);
		if (struct != null && struct.toResponsable() != null) {
			return StringCtrl.capitalizeWords(struct.toResponsable().prenomAffichage()) + " " + struct.toResponsable().nomAffichage();
		}
		return null;
	}

	public static String getGarnucheResponsabiliteScol(EOEditingContext ec, String cRne) {
		EOStructure struct = getGarnucheServiceScolarite(ec, cRne);
		if (struct != null && struct.toResponsable() != null) {
			return (struct.toResponsable().estHomme() ? "Le " : "La ") + struct.grpResponsabilite();
		}
		return null;
	}

	private static EOStructure getGarnucheServiceScolarite(EOEditingContext ec, String cRne) {
		String persId = getParam(ec, GARNUCHE_SCOLARITE, cRne);
		if (persId != null) {
			return EOStructure.fetchFirstByQualifier(ec, ERXQ.equals(EOStructure.PERS_ID_KEY, new Integer(persId)));
		}
		return null;
	}

	public static String getParam(EOEditingContext ec, String paramKey) {
		EOGarnucheParametres param = EOGarnucheParametres.fetchFirstByKeyValue(ec, EOGarnucheParametres.PARAM_KEY_KEY, paramKey);
		if (param != null) {
			return param.paramValue();
		}
		return null;
	}

	public static String getParam(EOEditingContext ec, String paramKey, String paramValue) {
		EOGarnucheParametres param = EOGarnucheParametres.fetchFirstByQualifier(ec,
				ERXQ.equals(EOGarnucheParametres.PARAM_KEY_KEY, paramKey).and(ERXQ.equals(EOGarnucheParametres.PARAM_VALUE_KEY, paramValue)));
		if (param != null) {
			return param.paramCommentaires();
		}
		return null;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
