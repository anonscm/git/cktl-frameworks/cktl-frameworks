/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreRepartAdresse.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPreRepartAdresse extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PreRepartAdresse";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PRE_REPART_ADRESSE";

	//Attributes

	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> E_MAIL = new ERXKey<String>("eMail");
	public static final ERXKey<String> RPA_PRINCIPAL = new ERXKey<String>("rpaPrincipal");
	public static final ERXKey<String> RPA_VALIDE = new ERXKey<String>("rpaValide");
	public static final ERXKey<String> TADR_CODE = new ERXKey<String>("tadrCode");

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String E_MAIL_KEY = "eMail";
	public static final String RPA_PRINCIPAL_KEY = "rpaPrincipal";
	public static final String RPA_VALIDE_KEY = "rpaValide";
	public static final String TADR_CODE_KEY = "tadrCode";

	// Non visible attributes
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String PERS_ID_KEY = "persId";

	// Colkeys
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String E_MAIL_COLKEY = "E_MAIL";
	public static final String RPA_PRINCIPAL_COLKEY = "RPA_PRINCIPAL";
	public static final String RPA_VALIDE_COLKEY = "RPA_VALIDE";
	public static final String TADR_CODE_COLKEY = "TADR_CODE";

	// Non visible colkeys
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse> TO_FWKPERS__TYPE_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse>("toFwkpers_TypeAdresse");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse> TO_PRE_ADRESSE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse>("toPreAdresse");

	public static final String TO_FWKPERS__TYPE_ADRESSE_KEY = "toFwkpers_TypeAdresse";
	public static final String TO_PRE_ADRESSE_KEY = "toPreAdresse";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPreRepartAdresse with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dCreation
	 * @param dModification
	 * @param rpaPrincipal
	 * @param rpaValide
	 * @param tadrCode
	 * @param toFwkpers_TypeAdresse
	 * @param toPreAdresse
	 * @return EOPreRepartAdresse
	 */
	public static EOPreRepartAdresse create(EOEditingContext editingContext, NSTimestamp dCreation, NSTimestamp dModification, String rpaPrincipal, String rpaValide, String tadrCode, org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse toFwkpers_TypeAdresse, org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse toPreAdresse) {
		EOPreRepartAdresse eo = (EOPreRepartAdresse) createAndInsertInstance(editingContext);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setRpaPrincipal(rpaPrincipal);
		eo.setRpaValide(rpaValide);
		eo.setTadrCode(tadrCode);
		eo.setToFwkpers_TypeAdresseRelationship(toFwkpers_TypeAdresse);
		eo.setToPreAdresseRelationship(toPreAdresse);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPreRepartAdresse.
	 *
	 * @param editingContext
	 * @return EOPreRepartAdresse
	 */
	public static EOPreRepartAdresse create(EOEditingContext editingContext) {
		EOPreRepartAdresse eo = (EOPreRepartAdresse) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPreRepartAdresse localInstanceIn(EOEditingContext editingContext) {
		EOPreRepartAdresse localInstance = (EOPreRepartAdresse) localInstanceOfObject(editingContext, (EOPreRepartAdresse) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPreRepartAdresse localInstanceIn(EOEditingContext editingContext, EOPreRepartAdresse eo) {
		EOPreRepartAdresse localInstance = (eo == null) ? null : (EOPreRepartAdresse) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}
	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	public String eMail() {
		return (String) storedValueForKey("eMail");
	}

	public void setEMail(String value) {
		takeStoredValueForKey(value, "eMail");
	}
	public String rpaPrincipal() {
		return (String) storedValueForKey("rpaPrincipal");
	}

	public void setRpaPrincipal(String value) {
		takeStoredValueForKey(value, "rpaPrincipal");
	}
	public String rpaValide() {
		return (String) storedValueForKey("rpaValide");
	}

	public void setRpaValide(String value) {
		takeStoredValueForKey(value, "rpaValide");
	}
	public String tadrCode() {
		return (String) storedValueForKey("tadrCode");
	}

	public void setTadrCode(String value) {
		takeStoredValueForKey(value, "tadrCode");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse toFwkpers_TypeAdresse() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse)storedValueForKey("toFwkpers_TypeAdresse");
	}

	public void setToFwkpers_TypeAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse oldValue = toFwkpers_TypeAdresse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_TypeAdresse");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_TypeAdresse");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse toPreAdresse() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse)storedValueForKey("toPreAdresse");
	}

	public void setToPreAdresseRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOPreAdresse oldValue = toPreAdresse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPreAdresse");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toPreAdresse");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPreRepartAdresse.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPreRepartAdresse.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPreRepartAdresse)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPreRepartAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreRepartAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPreRepartAdresse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPreRepartAdresse)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPreRepartAdresse fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreRepartAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreRepartAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPreRepartAdresse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPreRepartAdresse)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreRepartAdresse fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreRepartAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPreRepartAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPreRepartAdresse ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPreRepartAdresse createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPreRepartAdresse.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPreRepartAdresse.ENTITY_NAME + "' !");
		}
		else {
			EOPreRepartAdresse object = (EOPreRepartAdresse) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPreRepartAdresse localInstanceOfObject(EOEditingContext ec, EOPreRepartAdresse object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPreRepartAdresse " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPreRepartAdresse) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
