/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPreIndividu extends _EOPreIndividu {

	public EOPreIndividu() {
		super();
	}

	public String toString() {
		return nomAffichage()
				+ (nomPatronymiqueAffichage() != null && !nomPatronymiqueAffichage().equals(nomAffichage()) ? " (" + nomPatronymiqueAffichage() + ")"
						: "") + (prenomAffichage() != null ? " " + prenomAffichage() : "");
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Donne l'adresse stable (adresse parents) de ce pre-individu.
	 * 
	 * @return Un EOPreAdresse
	 */
	public EOPreAdresse adresseStable() {
		EOPreAdresse adresseStable = null;
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOPreRepartAdresse.TADR_CODE_KEY, EOKeyValueQualifier.QualifierOperatorEqual,
				EOTypeAdresse.TADR_CODE_PAR);
		NSArray<EOPreRepartAdresse> repartAdresses = toPreRepartAdresses(qualifier);
		if (repartAdresses != null && repartAdresses.count() > 0) {
			adresseStable = repartAdresses.lastObject().toPreAdresse();
		}
		return adresseStable;
	}

	/**
	 * Donne l'adresse universitaire (adresse etudiante) de ce pre-individu.
	 * 
	 * @return Un EOPreAdresse
	 */
	public EOPreAdresse adresseUniversitaire() {
		EOPreAdresse adresseUniversitaire = null;
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOPreRepartAdresse.TADR_CODE_KEY, EOKeyValueQualifier.QualifierOperatorEqual,
				EOTypeAdresse.TADR_CODE_ETUD);
		NSArray<EOPreRepartAdresse> repartAdresses = toPreRepartAdresses(qualifier);
		if (repartAdresses != null && repartAdresses.count() > 0) {
			adresseUniversitaire = repartAdresses.lastObject().toPreAdresse();
		}
		return adresseUniversitaire;
	}

	/**
	 * Donne le telephone stable (telephone parents) de ce pre-individu.
	 * 
	 * @return Un EOPrePersonneTelephone
	 */
	public EOPrePersonneTelephone telephoneStable() {
		EOPrePersonneTelephone telephoneStable = null;

		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOPrePersonneTelephone.TO_FWKPERS__TYPE_TEL_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY,
				EOKeyValueQualifier.QualifierOperatorEqual, EOTypeTel.C_TYPE_TEL_PAR);
		NSArray<EOPrePersonneTelephone> personneTelephones = toPrePersonneTelephones(qualifier);

		if (personneTelephones != null && personneTelephones.count() > 0) {
			telephoneStable = personneTelephones.lastObject();
		}

		return telephoneStable;
	}

	/**
	 * Donne le telephone universitaire fixe (etudiant) de ce pre-individu.
	 * 
	 * @return Un EOPrePersonneTelephone
	 */
	public EOPrePersonneTelephone telephoneUniversitaireFixe() {
		EOPrePersonneTelephone telephoneUniversitaireFixe = null;
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOPrePersonneTelephone.TO_FWKPERS__TYPE_TEL_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY,
				EOKeyValueQualifier.QualifierOperatorEqual, EOTypeTel.C_TYPE_TEL_ETUD);
		EOKeyValueQualifier qualifier1 = new EOKeyValueQualifier(EOPrePersonneTelephone.TO_FWKPERS__TYPE_NO_TEL_KEY + "."
				+ EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOKeyValueQualifier.QualifierOperatorEqual, EOTypeNoTel.TYPE_NO_TEL_TEL);
		NSArray<EOPrePersonneTelephone> personneTelephones = toPrePersonneTelephones(new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier, qualifier1 })));

		if (personneTelephones != null && personneTelephones.count() > 0) {
			telephoneUniversitaireFixe = personneTelephones.lastObject();
		}

		return telephoneUniversitaireFixe;
	}

	/**
	 * Donne le telephone universitaire mobile (portable etudiant) de ce pre-individu.
	 * 
	 * @return Un EOPrePersonneTelephone
	 */
	public EOPrePersonneTelephone telephoneUniversitairePortable() {
		EOPrePersonneTelephone telephoneUniversitairePortable = null;
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOPrePersonneTelephone.TO_FWKPERS__TYPE_TEL_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY,
				EOKeyValueQualifier.QualifierOperatorEqual, EOTypeTel.C_TYPE_TEL_ETUD);
		EOKeyValueQualifier qualifier1 = new EOKeyValueQualifier(EOPrePersonneTelephone.TO_FWKPERS__TYPE_NO_TEL_KEY + "."
				+ EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOKeyValueQualifier.QualifierOperatorEqual, EOTypeNoTel.TYPE_NO_TEL_MOB);
		NSArray<EOPrePersonneTelephone> personneTelephones = toPrePersonneTelephones(new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier, qualifier1 })));

		if (personneTelephones != null && personneTelephones.count() > 0) {
			telephoneUniversitairePortable = personneTelephones.lastObject();
		}

		return telephoneUniversitairePortable;
	}
}
