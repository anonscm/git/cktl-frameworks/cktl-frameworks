/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVSituationDetail.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVSituationDetail extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VSituationDetail";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_SITUATION_DETAIL";

	//Attributes

	public static final ERXKey<String> ADRESSE1 = new ERXKey<String>("adresse1");
	public static final ERXKey<String> ADRESSE2 = new ERXKey<String>("adresse2");
	public static final ERXKey<Integer> ANNEE1INSC_ULR = new ERXKey<Integer>("annee1inscUlr");
	public static final ERXKey<Integer> ANNEE1INSC_UNIV = new ERXKey<Integer>("annee1inscUniv");
	public static final ERXKey<Integer> ANNEE_BAC = new ERXKey<Integer>("anneeBac");
	public static final ERXKey<Integer> ANNEE_SCOLAIREN = new ERXKey<Integer>("anneeScolairen");
	public static final ERXKey<Integer> ANNEE_SCOLAIREN1 = new ERXKey<Integer>("anneeScolairen1");
	public static final ERXKey<Integer> ANNEE_SUIVIE = new ERXKey<Integer>("anneeSuivie");
	public static final ERXKey<String> CODE_BAC = new ERXKey<String>("codeBac");
	public static final ERXKey<String> CODE_ETAB1INSC_UNIV = new ERXKey<String>("codeEtab1inscUniv");
	public static final ERXKey<String> CODE_FILIERE = new ERXKey<String>("codeFiliere");
	public static final ERXKey<String> CODE_INE = new ERXKey<String>("codeIne");
	public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
	public static final ERXKey<String> CODE_RESULTAT = new ERXKey<String>("codeResultat");
	public static final ERXKey<String> COMMUNE_NAISSANCE = new ERXKey<String>("communeNaissance");
	public static final ERXKey<Integer> CREDITS = new ERXKey<Integer>("credits");
	public static final ERXKey<Integer> CREDITS2 = new ERXKey<Integer>("credits2");
	public static final ERXKey<String> CREDITS_AFFICHAGE = new ERXKey<String>("creditsAffichage");
	public static final ERXKey<Integer> CREDITS_SEMESTRE = new ERXKey<Integer>("creditsSemestre");
	public static final ERXKey<String> DATE_NAISSANCE = new ERXKey<String>("dateNaissance");
	public static final ERXKey<String> DATE_RESULTAT = new ERXKey<String>("dateResultat");
	public static final ERXKey<String> DATE_RESULTAT_STR = new ERXKey<String>("dateResultatStr");
	public static final ERXKey<String> DPT_NAISSANCE = new ERXKey<String>("dptNaissance");
	public static final ERXKey<Integer> ETAT_SEMESTRE = new ERXKey<Integer>("etatSemestre");
	public static final ERXKey<Integer> ETAT_TYPE_SEMESTRE = new ERXKey<Integer>("etatTypeSemestre");
	public static final ERXKey<Integer> IDIPL_NUMERO = new ERXKey<Integer>("idiplNumero");
	public static final ERXKey<String> INSCRIPTION_LIBELLE = new ERXKey<String>("inscriptionLibelle");
	public static final ERXKey<String> LIBELLE_BAC = new ERXKey<String>("libelleBac");
	public static final ERXKey<String> LIBELLE_DIPLOME = new ERXKey<String>("libelleDiplome");
	public static final ERXKey<String> LIBELLE_ETAB1INSC_UNIV = new ERXKey<String>("libelleEtab1inscUniv");
	public static final ERXKey<String> LIBELLE_FILIERE = new ERXKey<String>("libelleFiliere");
	public static final ERXKey<String> LIBELLE_RESULTAT = new ERXKey<String>("libelleResultat");
	public static final ERXKey<String> NIVEAU_SEMESTRE = new ERXKey<String>("niveauSemestre");
	public static final ERXKey<String> NO_INSEE = new ERXKey<String>("noInsee");
	public static final ERXKey<String> NOM = new ERXKey<String>("nom");
	public static final ERXKey<Integer> NUMERO_ETUDIANT = new ERXKey<Integer>("numeroEtudiant");
	public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
	public static final ERXKey<String> RESULTAT_LIBELLE = new ERXKey<String>("resultatLibelle");
	public static final ERXKey<String> SCOLARITE = new ERXKey<String>("scolarite");
	public static final ERXKey<Integer> TYPE_INSCRIPTION = new ERXKey<Integer>("typeInscription");
	public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
	public static final ERXKey<String> VILLE_BAC = new ERXKey<String>("villeBac");

	public static final String ADRESSE1_KEY = "adresse1";
	public static final String ADRESSE2_KEY = "adresse2";
	public static final String ANNEE1INSC_ULR_KEY = "annee1inscUlr";
	public static final String ANNEE1INSC_UNIV_KEY = "annee1inscUniv";
	public static final String ANNEE_BAC_KEY = "anneeBac";
	public static final String ANNEE_SCOLAIREN_KEY = "anneeScolairen";
	public static final String ANNEE_SCOLAIREN1_KEY = "anneeScolairen1";
	public static final String ANNEE_SUIVIE_KEY = "anneeSuivie";
	public static final String CODE_BAC_KEY = "codeBac";
	public static final String CODE_ETAB1INSC_UNIV_KEY = "codeEtab1inscUniv";
	public static final String CODE_FILIERE_KEY = "codeFiliere";
	public static final String CODE_INE_KEY = "codeIne";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String CODE_RESULTAT_KEY = "codeResultat";
	public static final String COMMUNE_NAISSANCE_KEY = "communeNaissance";
	public static final String CREDITS_KEY = "credits";
	public static final String CREDITS2_KEY = "credits2";
	public static final String CREDITS_AFFICHAGE_KEY = "creditsAffichage";
	public static final String CREDITS_SEMESTRE_KEY = "creditsSemestre";
	public static final String DATE_NAISSANCE_KEY = "dateNaissance";
	public static final String DATE_RESULTAT_KEY = "dateResultat";
	public static final String DATE_RESULTAT_STR_KEY = "dateResultatStr";
	public static final String DPT_NAISSANCE_KEY = "dptNaissance";
	public static final String ETAT_SEMESTRE_KEY = "etatSemestre";
	public static final String ETAT_TYPE_SEMESTRE_KEY = "etatTypeSemestre";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String INSCRIPTION_LIBELLE_KEY = "inscriptionLibelle";
	public static final String LIBELLE_BAC_KEY = "libelleBac";
	public static final String LIBELLE_DIPLOME_KEY = "libelleDiplome";
	public static final String LIBELLE_ETAB1INSC_UNIV_KEY = "libelleEtab1inscUniv";
	public static final String LIBELLE_FILIERE_KEY = "libelleFiliere";
	public static final String LIBELLE_RESULTAT_KEY = "libelleResultat";
	public static final String NIVEAU_SEMESTRE_KEY = "niveauSemestre";
	public static final String NO_INSEE_KEY = "noInsee";
	public static final String NOM_KEY = "nom";
	public static final String NUMERO_ETUDIANT_KEY = "numeroEtudiant";
	public static final String PRENOM_KEY = "prenom";
	public static final String RESULTAT_LIBELLE_KEY = "resultatLibelle";
	public static final String SCOLARITE_KEY = "scolarite";
	public static final String TYPE_INSCRIPTION_KEY = "typeInscription";
	public static final String VILLE_KEY = "ville";
	public static final String VILLE_BAC_KEY = "villeBac";

	// Non visible attributes

	// Colkeys
	public static final String ADRESSE1_COLKEY = "ADRESSE1";
	public static final String ADRESSE2_COLKEY = "ADRESSE2";
	public static final String ANNEE1INSC_ULR_COLKEY = "ANNEE_1INSC_ULR";
	public static final String ANNEE1INSC_UNIV_COLKEY = "ANNEE_1INSC_UNIV";
	public static final String ANNEE_BAC_COLKEY = "ANNEE_BAC";
	public static final String ANNEE_SCOLAIREN_COLKEY = "ANNEE_SCOLAIREN";
	public static final String ANNEE_SCOLAIREN1_COLKEY = "ANNEE_SCOLAIREN1";
	public static final String ANNEE_SUIVIE_COLKEY = "ANNEE_SUIVIE";
	public static final String CODE_BAC_COLKEY = "CODE_BAC";
	public static final String CODE_ETAB1INSC_UNIV_COLKEY = "CODE_ETAB_1INSC_UNIV";
	public static final String CODE_FILIERE_COLKEY = "CODE_FILIERE";
	public static final String CODE_INE_COLKEY = "CODE_INE";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String CODE_RESULTAT_COLKEY = "CODE_RESULTAT";
	public static final String COMMUNE_NAISSANCE_COLKEY = "COMMUNE_NAISSANCE";
	public static final String CREDITS_COLKEY = "CREDITS";
	public static final String CREDITS2_COLKEY = "CREDITS2";
	public static final String CREDITS_AFFICHAGE_COLKEY = "$attribute.columnName";
	public static final String CREDITS_SEMESTRE_COLKEY = "CREDITS_SEMESTRE";
	public static final String DATE_NAISSANCE_COLKEY = "DATE_NAISSANCE";
	public static final String DATE_RESULTAT_COLKEY = "DATE_RESULTAT";
	public static final String DATE_RESULTAT_STR_COLKEY = "$attribute.columnName";
	public static final String DPT_NAISSANCE_COLKEY = "DPT_NAISSANCE";
	public static final String ETAT_SEMESTRE_COLKEY = "ETAT_SEMESTRE";
	public static final String ETAT_TYPE_SEMESTRE_COLKEY = "ETAT_TYPE_SEMESTRE";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String INSCRIPTION_LIBELLE_COLKEY = "$attribute.columnName";
	public static final String LIBELLE_BAC_COLKEY = "LIBELLE_BAC";
	public static final String LIBELLE_DIPLOME_COLKEY = "LIBELLE_DIPLOME";
	public static final String LIBELLE_ETAB1INSC_UNIV_COLKEY = "LIBELLE_ETAB_1INSC_UNIV";
	public static final String LIBELLE_FILIERE_COLKEY = "LIBELLE_FILIERE";
	public static final String LIBELLE_RESULTAT_COLKEY = "LIBELLE_RESULTAT";
	public static final String NIVEAU_SEMESTRE_COLKEY = "NIVEAU_SEMESTRE";
	public static final String NO_INSEE_COLKEY = "NO_INSEE";
	public static final String NOM_COLKEY = "NOM";
	public static final String NUMERO_ETUDIANT_COLKEY = "NUMERO_ETUDIANT";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String RESULTAT_LIBELLE_COLKEY = "$attribute.columnName";
	public static final String SCOLARITE_COLKEY = "SCOLARITE";
	public static final String TYPE_INSCRIPTION_COLKEY = "TYPE_INSCRIPTION";
	public static final String VILLE_COLKEY = "VILLE";
	public static final String VILLE_BAC_COLKEY = "VILLE_BAC";

	// Non visible colkeys

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOVSituationDetail with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param creditsAffichage
	 * @param dateResultatStr
	 * @param idiplNumero
	 * @param inscriptionLibelle
	 * @param niveauSemestre
	 * @param resultatLibelle
	 * @return EOVSituationDetail
	 */
	public static EOVSituationDetail create(EOEditingContext editingContext, String creditsAffichage, String dateResultatStr, Integer idiplNumero, String inscriptionLibelle, String niveauSemestre, String resultatLibelle) {
		EOVSituationDetail eo = (EOVSituationDetail) createAndInsertInstance(editingContext);
		eo.setCreditsAffichage(creditsAffichage);
		eo.setDateResultatStr(dateResultatStr);
		eo.setIdiplNumero(idiplNumero);
		eo.setInscriptionLibelle(inscriptionLibelle);
		eo.setNiveauSemestre(niveauSemestre);
		eo.setResultatLibelle(resultatLibelle);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVSituationDetail.
	 *
	 * @param editingContext
	 * @return EOVSituationDetail
	 */
	public static EOVSituationDetail create(EOEditingContext editingContext) {
		EOVSituationDetail eo = (EOVSituationDetail) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVSituationDetail localInstanceIn(EOEditingContext editingContext) {
		EOVSituationDetail localInstance = (EOVSituationDetail) localInstanceOfObject(editingContext, (EOVSituationDetail) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVSituationDetail localInstanceIn(EOEditingContext editingContext, EOVSituationDetail eo) {
		EOVSituationDetail localInstance = (eo == null) ? null : (EOVSituationDetail) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String adresse1() {
		return (String) storedValueForKey("adresse1");
	}

	public void setAdresse1(String value) {
		takeStoredValueForKey(value, "adresse1");
	}
	public String adresse2() {
		return (String) storedValueForKey("adresse2");
	}

	public void setAdresse2(String value) {
		takeStoredValueForKey(value, "adresse2");
	}
	public Integer annee1inscUlr() {
		return (Integer) storedValueForKey("annee1inscUlr");
	}

	public void setAnnee1inscUlr(Integer value) {
		takeStoredValueForKey(value, "annee1inscUlr");
	}
	public Integer annee1inscUniv() {
		return (Integer) storedValueForKey("annee1inscUniv");
	}

	public void setAnnee1inscUniv(Integer value) {
		takeStoredValueForKey(value, "annee1inscUniv");
	}
	public Integer anneeBac() {
		return (Integer) storedValueForKey("anneeBac");
	}

	public void setAnneeBac(Integer value) {
		takeStoredValueForKey(value, "anneeBac");
	}
	public Integer anneeScolairen() {
		return (Integer) storedValueForKey("anneeScolairen");
	}

	public void setAnneeScolairen(Integer value) {
		takeStoredValueForKey(value, "anneeScolairen");
	}
	public Integer anneeScolairen1() {
		return (Integer) storedValueForKey("anneeScolairen1");
	}

	public void setAnneeScolairen1(Integer value) {
		takeStoredValueForKey(value, "anneeScolairen1");
	}
	public Integer anneeSuivie() {
		return (Integer) storedValueForKey("anneeSuivie");
	}

	public void setAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "anneeSuivie");
	}
	public String codeBac() {
		return (String) storedValueForKey("codeBac");
	}

	public void setCodeBac(String value) {
		takeStoredValueForKey(value, "codeBac");
	}
	public String codeEtab1inscUniv() {
		return (String) storedValueForKey("codeEtab1inscUniv");
	}

	public void setCodeEtab1inscUniv(String value) {
		takeStoredValueForKey(value, "codeEtab1inscUniv");
	}
	public String codeFiliere() {
		return (String) storedValueForKey("codeFiliere");
	}

	public void setCodeFiliere(String value) {
		takeStoredValueForKey(value, "codeFiliere");
	}
	public String codeIne() {
		return (String) storedValueForKey("codeIne");
	}

	public void setCodeIne(String value) {
		takeStoredValueForKey(value, "codeIne");
	}
	public String codePostal() {
		return (String) storedValueForKey("codePostal");
	}

	public void setCodePostal(String value) {
		takeStoredValueForKey(value, "codePostal");
	}
	public String codeResultat() {
		return (String) storedValueForKey("codeResultat");
	}

	public void setCodeResultat(String value) {
		takeStoredValueForKey(value, "codeResultat");
	}
	public String communeNaissance() {
		return (String) storedValueForKey("communeNaissance");
	}

	public void setCommuneNaissance(String value) {
		takeStoredValueForKey(value, "communeNaissance");
	}
	public Integer credits() {
		return (Integer) storedValueForKey("credits");
	}

	public void setCredits(Integer value) {
		takeStoredValueForKey(value, "credits");
	}
	public Integer credits2() {
		return (Integer) storedValueForKey("credits2");
	}

	public void setCredits2(Integer value) {
		takeStoredValueForKey(value, "credits2");
	}
	public String creditsAffichage() {
		return (String) storedValueForKey("creditsAffichage");
	}

	public void setCreditsAffichage(String value) {
		takeStoredValueForKey(value, "creditsAffichage");
	}
	public Integer creditsSemestre() {
		return (Integer) storedValueForKey("creditsSemestre");
	}

	public void setCreditsSemestre(Integer value) {
		takeStoredValueForKey(value, "creditsSemestre");
	}
	public String dateNaissance() {
		return (String) storedValueForKey("dateNaissance");
	}

	public void setDateNaissance(String value) {
		takeStoredValueForKey(value, "dateNaissance");
	}
	public String dateResultat() {
		return (String) storedValueForKey("dateResultat");
	}

	public void setDateResultat(String value) {
		takeStoredValueForKey(value, "dateResultat");
	}
	public String dateResultatStr() {
		return (String) storedValueForKey("dateResultatStr");
	}

	public void setDateResultatStr(String value) {
		takeStoredValueForKey(value, "dateResultatStr");
	}
	public String dptNaissance() {
		return (String) storedValueForKey("dptNaissance");
	}

	public void setDptNaissance(String value) {
		takeStoredValueForKey(value, "dptNaissance");
	}
	public Integer etatSemestre() {
		return (Integer) storedValueForKey("etatSemestre");
	}

	public void setEtatSemestre(Integer value) {
		takeStoredValueForKey(value, "etatSemestre");
	}
	public Integer etatTypeSemestre() {
		return (Integer) storedValueForKey("etatTypeSemestre");
	}

	public void setEtatTypeSemestre(Integer value) {
		takeStoredValueForKey(value, "etatTypeSemestre");
	}
	public Integer idiplNumero() {
		return (Integer) storedValueForKey("idiplNumero");
	}

	public void setIdiplNumero(Integer value) {
		takeStoredValueForKey(value, "idiplNumero");
	}
	public String inscriptionLibelle() {
		return (String) storedValueForKey("inscriptionLibelle");
	}

	public void setInscriptionLibelle(String value) {
		takeStoredValueForKey(value, "inscriptionLibelle");
	}
	public String libelleBac() {
		return (String) storedValueForKey("libelleBac");
	}

	public void setLibelleBac(String value) {
		takeStoredValueForKey(value, "libelleBac");
	}
	public String libelleDiplome() {
		return (String) storedValueForKey("libelleDiplome");
	}

	public void setLibelleDiplome(String value) {
		takeStoredValueForKey(value, "libelleDiplome");
	}
	public String libelleEtab1inscUniv() {
		return (String) storedValueForKey("libelleEtab1inscUniv");
	}

	public void setLibelleEtab1inscUniv(String value) {
		takeStoredValueForKey(value, "libelleEtab1inscUniv");
	}
	public String libelleFiliere() {
		return (String) storedValueForKey("libelleFiliere");
	}

	public void setLibelleFiliere(String value) {
		takeStoredValueForKey(value, "libelleFiliere");
	}
	public String libelleResultat() {
		return (String) storedValueForKey("libelleResultat");
	}

	public void setLibelleResultat(String value) {
		takeStoredValueForKey(value, "libelleResultat");
	}
	public String niveauSemestre() {
		return (String) storedValueForKey("niveauSemestre");
	}

	public void setNiveauSemestre(String value) {
		takeStoredValueForKey(value, "niveauSemestre");
	}
	public String noInsee() {
		return (String) storedValueForKey("noInsee");
	}

	public void setNoInsee(String value) {
		takeStoredValueForKey(value, "noInsee");
	}
	public String nom() {
		return (String) storedValueForKey("nom");
	}

	public void setNom(String value) {
		takeStoredValueForKey(value, "nom");
	}
	public Integer numeroEtudiant() {
		return (Integer) storedValueForKey("numeroEtudiant");
	}

	public void setNumeroEtudiant(Integer value) {
		takeStoredValueForKey(value, "numeroEtudiant");
	}
	public String prenom() {
		return (String) storedValueForKey("prenom");
	}

	public void setPrenom(String value) {
		takeStoredValueForKey(value, "prenom");
	}
	public String resultatLibelle() {
		return (String) storedValueForKey("resultatLibelle");
	}

	public void setResultatLibelle(String value) {
		takeStoredValueForKey(value, "resultatLibelle");
	}
	public String scolarite() {
		return (String) storedValueForKey("scolarite");
	}

	public void setScolarite(String value) {
		takeStoredValueForKey(value, "scolarite");
	}
	public Integer typeInscription() {
		return (Integer) storedValueForKey("typeInscription");
	}

	public void setTypeInscription(Integer value) {
		takeStoredValueForKey(value, "typeInscription");
	}
	public String ville() {
		return (String) storedValueForKey("ville");
	}

	public void setVille(String value) {
		takeStoredValueForKey(value, "ville");
	}
	public String villeBac() {
		return (String) storedValueForKey("villeBac");
	}

	public void setVilleBac(String value) {
		takeStoredValueForKey(value, "villeBac");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVSituationDetail.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVSituationDetail.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVSituationDetail)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVSituationDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVSituationDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVSituationDetail eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVSituationDetail)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVSituationDetail fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVSituationDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVSituationDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVSituationDetail eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVSituationDetail)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVSituationDetail fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVSituationDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVSituationDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVSituationDetail ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVSituationDetail createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVSituationDetail.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVSituationDetail.ENTITY_NAME + "' !");
		}
		else {
			EOVSituationDetail object = (EOVSituationDetail) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVSituationDetail localInstanceOfObject(EOEditingContext ec, EOVSituationDetail object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVSituationDetail " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVSituationDetail) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
