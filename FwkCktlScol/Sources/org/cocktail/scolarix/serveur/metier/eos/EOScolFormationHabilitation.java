/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre;

import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXKeyValueQualifier;

public class EOScolFormationHabilitation extends _EOScolFormationHabilitation {

	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_ASSOCIATION_KEY = "typeAssociation";

	public static final String TYPE_ACTION_REDOUBLEMENT = "R";
	public static final String TYPE_ACTION_PROGRESSION = "P";

	public static final String LIBELLE_KEY = "libelle";
	public static final String LIBELLE_ABREGE_KEY = "libelleAbrege";
	public static final String LIBELLE_DIPLOME_COMPLET_KEY = "libelleDiplomeComplet";
	public static final String GRADE_LIBELLE_KEY = "gradeLibelle";
	public static final String TRI_KEY = "tri";

	private Integer typeAssociation;
	private String typeAction;
	private Boolean isPassageConditionnel;
	private Boolean isDiplomable;
	private Boolean isAmenagement;
	private Boolean isSelectionnable;
	private Boolean hasTitre = null;
	private Boolean isPassageAMinima = Boolean.FALSE;

	public EOScolFormationHabilitation() {
		super();
	}

	public Boolean hasTitre() {
		if (hasTitre == null) {
			ERXKeyValueQualifier q1 = ERXQ.equals(EOScolFormationTitre.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY,
					toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome());
			ERXKeyValueQualifier q2 = ERXQ.equals(EOScolFormationTitre.FHAB_NIVEAU_KEY, fhabNiveau());
			EOScolFormationTitre titre = EOScolFormationTitre.fetchByQualifier(editingContext(), ERXQ.and(q1, q2));
			hasTitre = Boolean.valueOf(titre != null);
		}
		return hasTitre;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appel̩e.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public String typeAction() {
		return typeAction;
	}

	public void setTypeAction(String typeAction) {
		this.typeAction = typeAction;
	}

	public String tri() {
		return toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().filiere().tri();
	}

	/**
	 * Retourne le code de la filiere selon LMD or not.
	 * 
	 * @return String
	 */
	public String grade() {
		return toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().filiere().code();
	}

	/**
	 * Retourne l'abreviation de la filiere selon LMD or not.
	 * 
	 * @return String
	 */
	public String gradeLibelle() {
		return toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().filiere().abreviation();
	}

	/**
	 * Retourne l'annee/niveau de la formation (1, 2 ou 3 en L, 1 ou 2 en M, ...).
	 * 
	 * @return Integer
	 */
	public Integer niveau() {
		return fhabNiveau();
	}

	/**
	 * Le libelle simple, sans grade et niveau.
	 * 
	 * @return String
	 */
	public String libelle() {
		StringBuffer sb = new StringBuffer();
		// pour les tests, penser a commenter ces 3 lignes...
		// sb = sb.append(typeAssociation() + " - ");
		// sb = sb.append(typeAction() + " - ");
		// sb = sb.append((isSelectionnable().booleanValue() ? "O" : "N") + " - ");
		sb = sb.append(toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipLibelle());
		if (toFwkScolarite_ScolFormationSpecialisation().fspnLibelle() != null) {
			sb = sb.append(" ");
			sb = sb.append(toFwkScolarite_ScolFormationSpecialisation().fspnLibelle());
		}
		return sb.toString();
	}

	public String libelleAbrege() {
		StringBuffer sb = new StringBuffer();
		if (!EOScolFormationDiplome.FORMATION_CLASSIQUE.equals(toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome()
				.fdipType())) {
			sb = sb.append(toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().departement().libelle());
			sb = sb.append(" - ");
		}
		sb = sb.append(toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipAbreviation());
		if (toFwkScolarite_ScolFormationSpecialisation().fspnLibelle() != null) {
			sb = sb.append(" ");
			sb = sb.append(toFwkScolarite_ScolFormationSpecialisation().fspnLibelle());
		}
		return sb.toString();
	}

	/**
	 * Retourne le libelle abrege du diplome.
	 * 
	 * @return String
	 */
	public String libelleDiplomeAbrege() {
		return toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipAbreviation();
	}

	/**
	 * Retourne le libelle complet du diplome.
	 * 
	 * @return String
	 */
	public String libelleDiplomeComplet() {
		return toFwkScolarite_ScolFormationSpecialisation().libelleDiplomeComplet(fhabNiveau());
	}

	public Integer typeAssociation() {
		return typeAssociation;
	}

	public void setTypeAssociation(Integer typeAssociation) {
		this.typeAssociation = typeAssociation;
	}

	public Boolean isPassageConditionnel() {
		return isPassageConditionnel;
	}

	public void setIsPassageConditionnel(Boolean isPassageConditionnel) {
		this.isPassageConditionnel = isPassageConditionnel;
	}

	public Boolean isDiplomable() {
		return isDiplomable;
	}

	public void setIsDiplomable(Boolean isDiplomable) {
		this.isDiplomable = isDiplomable;
	}

	public Boolean isAmenagement() {
		return isAmenagement;
	}

	public void setIsAmenagement(Boolean isAmenagement) {
		this.isAmenagement = isAmenagement;
	}

	public Boolean isSelectionnable() {
		return isSelectionnable;
	}

	public void setIsSelectionnable(Boolean isSelectionnable) {
		this.isSelectionnable = isSelectionnable;
	}

	public String toString() {
		String s = super.toString();
		s = s + ", typeAction = " + typeAction();
		s = s + ", typeAssociation = " + typeAssociation();
		s = s + ", isSelectionnable = " + (isSelectionnable() != null && isSelectionnable().booleanValue() ? "TRUE" : "FALSE") + ";";
		s = s + ", isPassageConditionnel = " + (isPassageConditionnel() != null && isPassageConditionnel().booleanValue() ? "TRUE" : "FALSE") + ";";
		return s;
	}

	public Boolean isPassageAMinima() {
		return isPassageAMinima;
	}

	public void setIsPassageAMinima(Boolean isPassageAMinima) {
		this.isPassageAMinima = isPassageAMinima;
	}

}
