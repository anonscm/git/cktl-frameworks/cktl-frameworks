/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPreHistorique extends _EOPreHistorique {

	public EOPreHistorique() {
		super();
	}

	/**
	 * Retourne l'inscription principale si trouvee parmi les inscriptions , NULL si aucune trouvee.
	 * 
	 * @return un EOPreInscription de l'inscription principale, NULL si aucune inscription principale.
	 */
	public EOPreInscription inscriptionPrincipale() {
		EOPreInscription preInscription = inscriptionPrincipale(inscriptionsValides());

		if (preInscription == null) {
			// Si aucune inscription valide, on cherche l'inscription de plus bas type d'inscription
			EOSortOrdering ordering1 = EOSortOrdering.sortOrderingWithKey(EOPreInscription.TO_TYPE_INSCRIPTION_KEY + "."
					+ EOTypeInscription.IDIPL_TYPE_INSCRIPTION_KEY, EOSortOrdering.CompareAscending);
			EOSortOrdering ordering2 = EOSortOrdering.sortOrderingWithKey(EOPreInscription.IDIPL_DATE_INSC_KEY, EOSortOrdering.CompareDescending);
			NSArray<EOPreInscription> preInscriptions = toPreInscriptions(null, new NSArray<EOSortOrdering>(new EOSortOrdering[] { ordering1,
					ordering2 }));

			preInscription = inscriptionPrincipale(preInscriptions);
		}
		return preInscription;
	}

	/**
	 * Retourne l'inscription principale si trouvee parmi les inscriptions passees en parametre , NULL si aucune trouvee.
	 * 
	 * @return un EOPreInscription de l'inscription principale, NULL si aucune inscription principale.
	 */
	public EOPreInscription inscriptionPrincipale(NSArray<EOPreInscription> preInscriptions) {
		EOPreInscription preInscription = null;

		if (preInscriptions != null && preInscriptions.count() > 0) {
			for (int i = 0; i < preInscriptions.count(); i++) {
				preInscription = preInscriptions.objectAtIndex(i);
				if ((preInscription.toTypeInscription() != null) && (preInscription.toTypeInscription().idiplTypeInscription() != null)) {
					if ((preInscription.toTypeInscription().idiplTypeInscription().intValue() == 0)
							|| (preInscription.toTypeInscription().idiplTypeInscription().intValue() == 4)) {
						return preInscription;
					}
				}
			}
			// S'il n'y a pas d'inscription de type 0 (principale) ou 4 (échange), on prend alors la première (voir
			// classement)
			preInscription = preInscriptions.objectAtIndex(0);
		}
		return preInscription;
	}

	/**
	 * @return Un NSArray de EOPreInscription des inscriptions valides du gazier, triées par ordre croissant de type inscription
	 *         (l'inscription principale en premier)
	 */
	public NSArray<EOPreInscription> inscriptionsValides() {
		NSMutableArray<EOQualifier> andQuals = new NSMutableArray<EOQualifier>(EOResultat.RES_CODE_INVALIDES.length);
		for (int i = 0; i < EOResultat.RES_CODE_INVALIDES.length; i++) {
			andQuals.addObject(new EOKeyValueQualifier(EOPreInscription.TO_RESULTAT_KEY + "." + EOResultat.RES_CODE_KEY,
					EOKeyValueQualifier.QualifierOperatorNotEqual, EOResultat.RES_CODE_INVALIDES[i]));
		}
		NSMutableArray<EOQualifier> orQuals = new NSMutableArray<EOQualifier>(2);
		orQuals.addObject(new EOKeyValueQualifier(EOPreInscription.TO_RESULTAT_KEY, EOKeyValueQualifier.QualifierOperatorEqual,
				NSKeyValueCoding.NullValue));
		orQuals.addObject(new EOAndQualifier(andQuals));
		EOSortOrdering ordering1 = EOSortOrdering.sortOrderingWithKey(EOPreInscription.TO_TYPE_INSCRIPTION_KEY + "."
				+ EOTypeInscription.IDIPL_TYPE_INSCRIPTION_KEY, EOSortOrdering.CompareAscending);
		EOSortOrdering ordering2 = EOSortOrdering.sortOrderingWithKey(EOPreInscription.IDIPL_DATE_INSC_KEY, EOSortOrdering.CompareAscending);
		return toPreInscriptions(new EOOrQualifier(orQuals), new NSArray<EOSortOrdering>(new EOSortOrdering[] { ordering1, ordering2 }));
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
