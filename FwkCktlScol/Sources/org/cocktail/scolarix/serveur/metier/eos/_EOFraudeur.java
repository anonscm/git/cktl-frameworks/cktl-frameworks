/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFraudeur.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOFraudeur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_Fraudeur";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.FRAUDEUR";

	//Attributes

	public static final ERXKey<Integer> FRAU_BORDEREAU = new ERXKey<Integer>("frauBordereau");
	public static final ERXKey<NSTimestamp> FRAU_DATE_DEBUT = new ERXKey<NSTimestamp>("frauDateDebut");
	public static final ERXKey<NSTimestamp> FRAU_DATE_FIN = new ERXKey<NSTimestamp>("frauDateFin");
	public static final ERXKey<NSTimestamp> FRAU_DATE_NAIS = new ERXKey<NSTimestamp>("frauDateNais");
	public static final ERXKey<String> FRAU_NOM = new ERXKey<String>("frauNom");
	public static final ERXKey<String> FRAU_PRENOM = new ERXKey<String>("frauPrenom");
	public static final ERXKey<String> FRAU_RAISON = new ERXKey<String>("frauRaison");
	public static final ERXKey<String> FRAU_SANCTION = new ERXKey<String>("frauSanction");

	public static final String FRAU_BORDEREAU_KEY = "frauBordereau";
	public static final String FRAU_DATE_DEBUT_KEY = "frauDateDebut";
	public static final String FRAU_DATE_FIN_KEY = "frauDateFin";
	public static final String FRAU_DATE_NAIS_KEY = "frauDateNais";
	public static final String FRAU_NOM_KEY = "frauNom";
	public static final String FRAU_PRENOM_KEY = "frauPrenom";
	public static final String FRAU_RAISON_KEY = "frauRaison";
	public static final String FRAU_SANCTION_KEY = "frauSanction";

	// Non visible attributes
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";

	// Colkeys
	public static final String FRAU_BORDEREAU_COLKEY = "FRAU_BORDEREAU";
	public static final String FRAU_DATE_DEBUT_COLKEY = "FRAU_DATE_DEBUT";
	public static final String FRAU_DATE_FIN_COLKEY = "FRAU_DATE_FIN";
	public static final String FRAU_DATE_NAIS_COLKEY = "FRAU_DATE_NAIS";
	public static final String FRAU_NOM_COLKEY = "FRAU_NOM";
	public static final String FRAU_PRENOM_COLKEY = "FRAU_PRENOM";
	public static final String FRAU_RAISON_COLKEY = "FRAU_RAISON";
	public static final String FRAU_SANCTION_COLKEY = "FRAU_SANCTION";

	// Non visible colkeys
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiant> TO_ETUDIANT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiant>("toEtudiant");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl> TO_INSC_DIPL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl>("toInscDipl");

	public static final String TO_ETUDIANT_KEY = "toEtudiant";
	public static final String TO_INSC_DIPL_KEY = "toInscDipl";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOFraudeur with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param frauBordereau
	 * @param frauNom
	 * @param frauPrenom
	 * @return EOFraudeur
	 */
	public static EOFraudeur create(EOEditingContext editingContext, Integer frauBordereau, String frauNom, String frauPrenom) {
		EOFraudeur eo = (EOFraudeur) createAndInsertInstance(editingContext);
		eo.setFrauBordereau(frauBordereau);
		eo.setFrauNom(frauNom);
		eo.setFrauPrenom(frauPrenom);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOFraudeur.
	 *
	 * @param editingContext
	 * @return EOFraudeur
	 */
	public static EOFraudeur create(EOEditingContext editingContext) {
		EOFraudeur eo = (EOFraudeur) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOFraudeur localInstanceIn(EOEditingContext editingContext) {
		EOFraudeur localInstance = (EOFraudeur) localInstanceOfObject(editingContext, (EOFraudeur) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOFraudeur localInstanceIn(EOEditingContext editingContext, EOFraudeur eo) {
		EOFraudeur localInstance = (eo == null) ? null : (EOFraudeur) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer frauBordereau() {
		return (Integer) storedValueForKey("frauBordereau");
	}

	public void setFrauBordereau(Integer value) {
		takeStoredValueForKey(value, "frauBordereau");
	}
	public NSTimestamp frauDateDebut() {
		return (NSTimestamp) storedValueForKey("frauDateDebut");
	}

	public void setFrauDateDebut(NSTimestamp value) {
		takeStoredValueForKey(value, "frauDateDebut");
	}
	public NSTimestamp frauDateFin() {
		return (NSTimestamp) storedValueForKey("frauDateFin");
	}

	public void setFrauDateFin(NSTimestamp value) {
		takeStoredValueForKey(value, "frauDateFin");
	}
	public NSTimestamp frauDateNais() {
		return (NSTimestamp) storedValueForKey("frauDateNais");
	}

	public void setFrauDateNais(NSTimestamp value) {
		takeStoredValueForKey(value, "frauDateNais");
	}
	public String frauNom() {
		return (String) storedValueForKey("frauNom");
	}

	public void setFrauNom(String value) {
		takeStoredValueForKey(value, "frauNom");
	}
	public String frauPrenom() {
		return (String) storedValueForKey("frauPrenom");
	}

	public void setFrauPrenom(String value) {
		takeStoredValueForKey(value, "frauPrenom");
	}
	public String frauRaison() {
		return (String) storedValueForKey("frauRaison");
	}

	public void setFrauRaison(String value) {
		takeStoredValueForKey(value, "frauRaison");
	}
	public String frauSanction() {
		return (String) storedValueForKey("frauSanction");
	}

	public void setFrauSanction(String value) {
		takeStoredValueForKey(value, "frauSanction");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOEtudiant toEtudiant() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOEtudiant)storedValueForKey("toEtudiant");
	}

	public void setToEtudiantRelationship(org.cocktail.scolarix.serveur.metier.eos.EOEtudiant value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOEtudiant oldValue = toEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toEtudiant");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOInscDipl)storedValueForKey("toInscDipl");
	}

	public void setToInscDiplRelationship(org.cocktail.scolarix.serveur.metier.eos.EOInscDipl value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOInscDipl oldValue = toInscDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toInscDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toInscDipl");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOFraudeur.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOFraudeur.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOFraudeur)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOFraudeur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFraudeur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOFraudeur eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOFraudeur)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOFraudeur fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOFraudeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOFraudeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOFraudeur eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOFraudeur)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOFraudeur fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOFraudeur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOFraudeur eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOFraudeur ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOFraudeur createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOFraudeur.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOFraudeur.ENTITY_NAME + "' !");
		}
		else {
			EOFraudeur object = (EOFraudeur) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOFraudeur localInstanceOfObject(EOEditingContext ec, EOFraudeur object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOFraudeur " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOFraudeur) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
