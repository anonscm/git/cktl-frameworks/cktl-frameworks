/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVRdvHoraires.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVRdvHoraires extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VRdvHoraires";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_RDV_HORAIRES";

	//Attributes

	public static final ERXKey<String> DATE_PER = new ERXKey<String>("datePer");
	public static final ERXKey<String> DEB_AFFICHAGE = new ERXKey<String>("debAffichage");
	public static final ERXKey<String> DEB_PERIODE = new ERXKey<String>("debPeriode");
	public static final ERXKey<String> FIN_AFFICHAGE = new ERXKey<String>("finAffichage");
	public static final ERXKey<String> FIN_PERIODE = new ERXKey<String>("finPeriode");
	public static final ERXKey<Integer> NB_ETU_PER = new ERXKey<Integer>("nbEtuPer");
	public static final ERXKey<Integer> NB_RDV_POSES = new ERXKey<Integer>("nbRdvPoses");
	public static final ERXKey<Integer> NB_RDV_REST = new ERXKey<Integer>("nbRdvRest");
	public static final ERXKey<Integer> NB_RDV_VENUS = new ERXKey<Integer>("nbRdvVenus");
	public static final ERXKey<Integer> NUM_OCC_PERIODE = new ERXKey<Integer>("numOccPeriode");
	public static final ERXKey<Integer> NUM_OCC_PLANNING = new ERXKey<Integer>("numOccPlanning");

	public static final String DATE_PER_KEY = "datePer";
	public static final String DEB_AFFICHAGE_KEY = "debAffichage";
	public static final String DEB_PERIODE_KEY = "debPeriode";
	public static final String FIN_AFFICHAGE_KEY = "finAffichage";
	public static final String FIN_PERIODE_KEY = "finPeriode";
	public static final String NB_ETU_PER_KEY = "nbEtuPer";
	public static final String NB_RDV_POSES_KEY = "nbRdvPoses";
	public static final String NB_RDV_REST_KEY = "nbRdvRest";
	public static final String NB_RDV_VENUS_KEY = "nbRdvVenus";
	public static final String NUM_OCC_PERIODE_KEY = "numOccPeriode";
	public static final String NUM_OCC_PLANNING_KEY = "numOccPlanning";

	// Non visible attributes

	// Colkeys
	public static final String DATE_PER_COLKEY = "DATE_PER";
	public static final String DEB_AFFICHAGE_COLKEY = "$attribute.columnName";
	public static final String DEB_PERIODE_COLKEY = "DEB_PERIODE";
	public static final String FIN_AFFICHAGE_COLKEY = "$attribute.columnName";
	public static final String FIN_PERIODE_COLKEY = "FIN_PERIODE";
	public static final String NB_ETU_PER_COLKEY = "NB_ETU_PER";
	public static final String NB_RDV_POSES_COLKEY = "NB_RDV_POSES";
	public static final String NB_RDV_REST_COLKEY = "NB_RDV_REST";
	public static final String NB_RDV_VENUS_COLKEY = "NB_RDV_VENUS";
	public static final String NUM_OCC_PERIODE_COLKEY = "NUM_OCC_PERIODE";
	public static final String NUM_OCC_PLANNING_COLKEY = "NUM_OCC_PLANNING";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats> TO_V_RDV_CANDIDATSES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats>("toVRdvCandidatses");

	public static final String TO_V_RDV_CANDIDATSES_KEY = "toVRdvCandidatses";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVRdvHoraires with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param datePer
	 * @param debAffichage
	 * @param debPeriode
	 * @param finAffichage
	 * @param numOccPeriode
	 * @param numOccPlanning
	 * @return EOVRdvHoraires
	 */
	public static EOVRdvHoraires create(EOEditingContext editingContext, String datePer, String debAffichage, String debPeriode, String finAffichage, Integer numOccPeriode, Integer numOccPlanning) {
		EOVRdvHoraires eo = (EOVRdvHoraires) createAndInsertInstance(editingContext);
		eo.setDatePer(datePer);
		eo.setDebAffichage(debAffichage);
		eo.setDebPeriode(debPeriode);
		eo.setFinAffichage(finAffichage);
		eo.setNumOccPeriode(numOccPeriode);
		eo.setNumOccPlanning(numOccPlanning);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVRdvHoraires.
	 *
	 * @param editingContext
	 * @return EOVRdvHoraires
	 */
	public static EOVRdvHoraires create(EOEditingContext editingContext) {
		EOVRdvHoraires eo = (EOVRdvHoraires) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVRdvHoraires localInstanceIn(EOEditingContext editingContext) {
		EOVRdvHoraires localInstance = (EOVRdvHoraires) localInstanceOfObject(editingContext, (EOVRdvHoraires) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVRdvHoraires localInstanceIn(EOEditingContext editingContext, EOVRdvHoraires eo) {
		EOVRdvHoraires localInstance = (eo == null) ? null : (EOVRdvHoraires) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String datePer() {
		return (String) storedValueForKey("datePer");
	}

	public void setDatePer(String value) {
		takeStoredValueForKey(value, "datePer");
	}
	public String debAffichage() {
		return (String) storedValueForKey("debAffichage");
	}

	public void setDebAffichage(String value) {
		takeStoredValueForKey(value, "debAffichage");
	}
	public String debPeriode() {
		return (String) storedValueForKey("debPeriode");
	}

	public void setDebPeriode(String value) {
		takeStoredValueForKey(value, "debPeriode");
	}
	public String finAffichage() {
		return (String) storedValueForKey("finAffichage");
	}

	public void setFinAffichage(String value) {
		takeStoredValueForKey(value, "finAffichage");
	}
	public String finPeriode() {
		return (String) storedValueForKey("finPeriode");
	}

	public void setFinPeriode(String value) {
		takeStoredValueForKey(value, "finPeriode");
	}
	public Integer nbEtuPer() {
		return (Integer) storedValueForKey("nbEtuPer");
	}

	public void setNbEtuPer(Integer value) {
		takeStoredValueForKey(value, "nbEtuPer");
	}
	public Integer nbRdvPoses() {
		return (Integer) storedValueForKey("nbRdvPoses");
	}

	public void setNbRdvPoses(Integer value) {
		takeStoredValueForKey(value, "nbRdvPoses");
	}
	public Integer nbRdvRest() {
		return (Integer) storedValueForKey("nbRdvRest");
	}

	public void setNbRdvRest(Integer value) {
		takeStoredValueForKey(value, "nbRdvRest");
	}
	public Integer nbRdvVenus() {
		return (Integer) storedValueForKey("nbRdvVenus");
	}

	public void setNbRdvVenus(Integer value) {
		takeStoredValueForKey(value, "nbRdvVenus");
	}
	public Integer numOccPeriode() {
		return (Integer) storedValueForKey("numOccPeriode");
	}

	public void setNumOccPeriode(Integer value) {
		takeStoredValueForKey(value, "numOccPeriode");
	}
	public Integer numOccPlanning() {
		return (Integer) storedValueForKey("numOccPlanning");
	}

	public void setNumOccPlanning(Integer value) {
		takeStoredValueForKey(value, "numOccPlanning");
	}

	public NSArray toVRdvCandidatses() {
		return (NSArray)storedValueForKey("toVRdvCandidatses");
	}

	public NSArray toVRdvCandidatses(EOQualifier qualifier) {
		return toVRdvCandidatses(qualifier, null);
	}

	public NSArray toVRdvCandidatses(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toVRdvCandidatses();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToVRdvCandidatsesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toVRdvCandidatses");
	}

	public void removeFromToVRdvCandidatsesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toVRdvCandidatses");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats createToVRdvCandidatsesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_VRdvCandidats");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toVRdvCandidatses");
		return (org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats) eo;
	}

	public void deleteToVRdvCandidatsesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toVRdvCandidatses");
				editingContext().deleteObject(object);
			}

	public void deleteAllToVRdvCandidatsesRelationships() {
		Enumeration objects = toVRdvCandidatses().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToVRdvCandidatsesRelationship((org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVRdvHoraires.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVRdvHoraires.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVRdvHoraires)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVRdvHoraires fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVRdvHoraires fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVRdvHoraires eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVRdvHoraires)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVRdvHoraires fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVRdvHoraires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVRdvHoraires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVRdvHoraires eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVRdvHoraires)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVRdvHoraires fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVRdvHoraires fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVRdvHoraires eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVRdvHoraires ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVRdvHoraires createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVRdvHoraires.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVRdvHoraires.ENTITY_NAME + "' !");
		}
		else {
			EOVRdvHoraires object = (EOVRdvHoraires) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVRdvHoraires localInstanceOfObject(EOEditingContext ec, EOVRdvHoraires object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVRdvHoraires " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVRdvHoraires) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
