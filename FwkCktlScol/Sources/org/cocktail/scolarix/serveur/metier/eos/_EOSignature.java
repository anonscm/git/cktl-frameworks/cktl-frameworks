/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSignature.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOSignature extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_Signature";
	public static final String ENTITY_TABLE_NAME = "PHOTO.SIGNATURE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "signOrdre";

	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
	public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
	public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
	public static final ERXKey<String> SIGN_COMMENTAIRE = new ERXKey<String>("signCommentaire");
	public static final ERXKey<NSTimestamp> SIGN_DATE_CREATION = new ERXKey<NSTimestamp>("signDateCreation");
	public static final ERXKey<NSTimestamp> SIGN_DATE_MODIFICATION = new ERXKey<NSTimestamp>("signDateModification");
	public static final ERXKey<NSData> SIGN_IMG = new ERXKey<NSData>("signImg");
	public static final ERXKey<String> SIGN_TYPE = new ERXKey<String>("signType");

	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String SIGN_COMMENTAIRE_KEY = "signCommentaire";
	public static final String SIGN_DATE_CREATION_KEY = "signDateCreation";
	public static final String SIGN_DATE_MODIFICATION_KEY = "signDateModification";
	public static final String SIGN_IMG_KEY = "signImg";
	public static final String SIGN_TYPE_KEY = "signType";

	// Non visible attributes
	public static final String SIGN_ORDRE_KEY = "signOrdre";

	// Colkeys
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String SIGN_COMMENTAIRE_COLKEY = "SIGN_COMMENTAIRE";
	public static final String SIGN_DATE_CREATION_COLKEY = "SIGN_DATE_CREATION";
	public static final String SIGN_DATE_MODIFICATION_COLKEY = "SIGN_DATE_MODIFICATION";
	public static final String SIGN_IMG_COLKEY = "SIGN_IMG";
	public static final String SIGN_TYPE_COLKEY = "SIGN_TYPE";

	// Non visible colkeys
	public static final String SIGN_ORDRE_COLKEY = "SIGN_ORDRE";

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOSignature with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param persId
	 * @param signDateCreation
	 * @param signDateModification
	 * @param signImg
	 * @return EOSignature
	 */
	public static EOSignature create(EOEditingContext editingContext, Integer persId, NSTimestamp signDateCreation, NSTimestamp signDateModification, NSData signImg) {
		EOSignature eo = (EOSignature) createAndInsertInstance(editingContext);
		eo.setPersId(persId);
		eo.setSignDateCreation(signDateCreation);
		eo.setSignDateModification(signDateModification);
		eo.setSignImg(signImg);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOSignature.
	 *
	 * @param editingContext
	 * @return EOSignature
	 */
	public static EOSignature create(EOEditingContext editingContext) {
		EOSignature eo = (EOSignature) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOSignature localInstanceIn(EOEditingContext editingContext) {
		EOSignature localInstance = (EOSignature) localInstanceOfObject(editingContext, (EOSignature) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOSignature localInstanceIn(EOEditingContext editingContext, EOSignature eo) {
		EOSignature localInstance = (eo == null) ? null : (EOSignature) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer persId() {
		return (Integer) storedValueForKey("persId");
	}

	public void setPersId(Integer value) {
		takeStoredValueForKey(value, "persId");
	}
	public Integer persIdCreation() {
		return (Integer) storedValueForKey("persIdCreation");
	}

	public void setPersIdCreation(Integer value) {
		takeStoredValueForKey(value, "persIdCreation");
	}
	public Integer persIdModification() {
		return (Integer) storedValueForKey("persIdModification");
	}

	public void setPersIdModification(Integer value) {
		takeStoredValueForKey(value, "persIdModification");
	}
	public String signCommentaire() {
		return (String) storedValueForKey("signCommentaire");
	}

	public void setSignCommentaire(String value) {
		takeStoredValueForKey(value, "signCommentaire");
	}
	public NSTimestamp signDateCreation() {
		return (NSTimestamp) storedValueForKey("signDateCreation");
	}

	public void setSignDateCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "signDateCreation");
	}
	public NSTimestamp signDateModification() {
		return (NSTimestamp) storedValueForKey("signDateModification");
	}

	public void setSignDateModification(NSTimestamp value) {
		takeStoredValueForKey(value, "signDateModification");
	}
	public NSData signImg() {
		return (NSData) storedValueForKey("signImg");
	}

	public void setSignImg(NSData value) {
		takeStoredValueForKey(value, "signImg");
	}
	public String signType() {
		return (String) storedValueForKey("signType");
	}

	public void setSignType(String value) {
		takeStoredValueForKey(value, "signType");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOSignature.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOSignature.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOSignature)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOSignature fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSignature fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOSignature eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOSignature)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOSignature fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOSignature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOSignature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOSignature eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOSignature)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOSignature fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOSignature fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOSignature eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOSignature ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOSignature createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOSignature.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOSignature.ENTITY_NAME + "' !");
		}
		else {
			EOSignature object = (EOSignature) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOSignature localInstanceOfObject(EOEditingContext ec, EOSignature object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOSignature " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOSignature) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
