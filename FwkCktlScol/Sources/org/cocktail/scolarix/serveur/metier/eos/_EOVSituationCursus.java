/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVSituationCursus.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVSituationCursus extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VSituationCursus";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_SITUATION_CURSUS";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "numeroEtudiant";

	public static final String ANNEE_SCOLAIREN_KEY = "anneeScolairen";
	public static final String ANNEE_SCOLAIREN1_KEY = "anneeScolairen1";
	public static final String ANNEE_SUIVIE_KEY = "anneeSuivie";
	public static final String CODE_FILIERE_KEY = "codeFiliere";
	public static final String CODE_RESULTAT_KEY = "codeResultat";
	public static final String LIBELLE_DIPLOME_KEY = "libelleDiplome";
	public static final String LIBELLE_FILIERE_KEY = "libelleFiliere";
	public static final String LIBELLE_RESULTAT_KEY = "libelleResultat";
	public static final String TYPE_INSCRIPTION_KEY = "typeInscription";

	// Non visible attributes
	public static final String NUMERO_ETUDIANT_KEY = "numeroEtudiant";

	// Colkeys
	public static final String ANNEE_SCOLAIREN_COLKEY = "ANNEE_SCOLAIREN";
	public static final String ANNEE_SCOLAIREN1_COLKEY = "ANNEE_SCOLAIREN1";
	public static final String ANNEE_SUIVIE_COLKEY = "ANNEE_SUIVIE";
	public static final String CODE_FILIERE_COLKEY = "CODE_FILIERE";
	public static final String CODE_RESULTAT_COLKEY = "CODE_RESULTAT";
	public static final String LIBELLE_DIPLOME_COLKEY = "LIBELLE_DIPLOME";
	public static final String LIBELLE_FILIERE_COLKEY = "LIBELLE_FILIERE";
	public static final String LIBELLE_RESULTAT_COLKEY = "LIBELLE_RESULTAT";
	public static final String TYPE_INSCRIPTION_COLKEY = "TYPE_INSCRIPTION";

	// Non visible colkeys
	public static final String NUMERO_ETUDIANT_COLKEY = "NUMERO_ETUDIANT";

	// Relationships

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVSituationCursus with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param anneeScolairen
	 * @param anneeSuivie
	 * @param codeFiliere
	 * @param libelleDiplome
	 * @param libelleFiliere
	 * @param typeInscription
	 * @return EOVSituationCursus
	 */
	public static EOVSituationCursus create(EOEditingContext editingContext, Integer anneeScolairen, Integer anneeSuivie, String codeFiliere, String libelleDiplome, String libelleFiliere, Integer typeInscription) {
		EOVSituationCursus eo = (EOVSituationCursus) createAndInsertInstance(editingContext);
		eo.setAnneeScolairen(anneeScolairen);
		eo.setAnneeSuivie(anneeSuivie);
		eo.setCodeFiliere(codeFiliere);
		eo.setLibelleDiplome(libelleDiplome);
		eo.setLibelleFiliere(libelleFiliere);
		eo.setTypeInscription(typeInscription);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVSituationCursus.
	 *
	 * @param editingContext
	 * @return EOVSituationCursus
	 */
	public static EOVSituationCursus create(EOEditingContext editingContext) {
		EOVSituationCursus eo = (EOVSituationCursus) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVSituationCursus localInstanceIn(EOEditingContext editingContext) {
		EOVSituationCursus localInstance = (EOVSituationCursus) localInstanceOfObject(editingContext, (EOVSituationCursus) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVSituationCursus localInstanceIn(EOEditingContext editingContext, EOVSituationCursus eo) {
		EOVSituationCursus localInstance = (eo == null) ? null : (EOVSituationCursus) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer anneeScolairen() {
		return (Integer) storedValueForKey("anneeScolairen");
	}

	public void setAnneeScolairen(Integer value) {
		takeStoredValueForKey(value, "anneeScolairen");
	}
	public Integer anneeScolairen1() {
		return (Integer) storedValueForKey("anneeScolairen1");
	}

	public void setAnneeScolairen1(Integer value) {
		takeStoredValueForKey(value, "anneeScolairen1");
	}
	public Integer anneeSuivie() {
		return (Integer) storedValueForKey("anneeSuivie");
	}

	public void setAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "anneeSuivie");
	}
	public String codeFiliere() {
		return (String) storedValueForKey("codeFiliere");
	}

	public void setCodeFiliere(String value) {
		takeStoredValueForKey(value, "codeFiliere");
	}
	public String codeResultat() {
		return (String) storedValueForKey("codeResultat");
	}

	public void setCodeResultat(String value) {
		takeStoredValueForKey(value, "codeResultat");
	}
	public String libelleDiplome() {
		return (String) storedValueForKey("libelleDiplome");
	}

	public void setLibelleDiplome(String value) {
		takeStoredValueForKey(value, "libelleDiplome");
	}
	public String libelleFiliere() {
		return (String) storedValueForKey("libelleFiliere");
	}

	public void setLibelleFiliere(String value) {
		takeStoredValueForKey(value, "libelleFiliere");
	}
	public String libelleResultat() {
		return (String) storedValueForKey("libelleResultat");
	}

	public void setLibelleResultat(String value) {
		takeStoredValueForKey(value, "libelleResultat");
	}
	public Integer typeInscription() {
		return (Integer) storedValueForKey("typeInscription");
	}

	public void setTypeInscription(Integer value) {
		takeStoredValueForKey(value, "typeInscription");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVSituationCursus.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVSituationCursus.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVSituationCursus)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVSituationCursus fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVSituationCursus fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVSituationCursus eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVSituationCursus)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVSituationCursus fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVSituationCursus fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVSituationCursus fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVSituationCursus eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVSituationCursus)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVSituationCursus fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVSituationCursus fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVSituationCursus eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVSituationCursus ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVSituationCursus createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVSituationCursus.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVSituationCursus.ENTITY_NAME + "' !");
		}
		else {
			EOVSituationCursus object = (EOVSituationCursus) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVSituationCursus localInstanceOfObject(EOEditingContext ec, EOVSituationCursus object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVSituationCursus " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVSituationCursus) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
