/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreOuvertureDiplome.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPreOuvertureDiplome extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PreOuvertureDiplome";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PRE_OUVERTURE_DIPLOME";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "poudKey";

	public static final ERXKey<Integer> HIST_ANNEE_SCOL = new ERXKey<Integer>("histAnneeScol");
	public static final ERXKey<Integer> POUD_ANNEE_SUIVIE = new ERXKey<Integer>("poudAnneeSuivie");
	public static final ERXKey<String> POUD_BLOCAGE = new ERXKey<String>("poudBlocage");
	public static final ERXKey<String> POUD_OUVERTURE = new ERXKey<String>("poudOuverture");

	public static final String HIST_ANNEE_SCOL_KEY = "histAnneeScol";
	public static final String POUD_ANNEE_SUIVIE_KEY = "poudAnneeSuivie";
	public static final String POUD_BLOCAGE_KEY = "poudBlocage";
	public static final String POUD_OUVERTURE_KEY = "poudOuverture";

	// Non visible attributes
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String POUD_KEY_KEY = "poudKey";

	// Colkeys
	public static final String HIST_ANNEE_SCOL_COLKEY = "HIST_ANNEE_SCOL";
	public static final String POUD_ANNEE_SUIVIE_COLKEY = "POUD_ANNEE_SUIVIE";
	public static final String POUD_BLOCAGE_COLKEY = "POUD_BLOCAGE";
	public static final String POUD_OUVERTURE_COLKEY = "POUD_OUVERTURE";

	// Non visible colkeys
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String POUD_KEY_COLKEY = "POUD_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPreOuvertureDiplome with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param histAnneeScol
	 * @param poudAnneeSuivie
	 * @param poudBlocage
	 * @param poudOuverture
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @return EOPreOuvertureDiplome
	 */
	public static EOPreOuvertureDiplome create(EOEditingContext editingContext, Integer histAnneeScol, Integer poudAnneeSuivie, String poudBlocage, String poudOuverture, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation) {
		EOPreOuvertureDiplome eo = (EOPreOuvertureDiplome) createAndInsertInstance(editingContext);
		eo.setHistAnneeScol(histAnneeScol);
		eo.setPoudAnneeSuivie(poudAnneeSuivie);
		eo.setPoudBlocage(poudBlocage);
		eo.setPoudOuverture(poudOuverture);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPreOuvertureDiplome.
	 *
	 * @param editingContext
	 * @return EOPreOuvertureDiplome
	 */
	public static EOPreOuvertureDiplome create(EOEditingContext editingContext) {
		EOPreOuvertureDiplome eo = (EOPreOuvertureDiplome) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPreOuvertureDiplome localInstanceIn(EOEditingContext editingContext) {
		EOPreOuvertureDiplome localInstance = (EOPreOuvertureDiplome) localInstanceOfObject(editingContext, (EOPreOuvertureDiplome) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPreOuvertureDiplome localInstanceIn(EOEditingContext editingContext, EOPreOuvertureDiplome eo) {
		EOPreOuvertureDiplome localInstance = (eo == null) ? null : (EOPreOuvertureDiplome) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer histAnneeScol() {
		return (Integer) storedValueForKey("histAnneeScol");
	}

	public void setHistAnneeScol(Integer value) {
		takeStoredValueForKey(value, "histAnneeScol");
	}
	public Integer poudAnneeSuivie() {
		return (Integer) storedValueForKey("poudAnneeSuivie");
	}

	public void setPoudAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "poudAnneeSuivie");
	}
	public String poudBlocage() {
		return (String) storedValueForKey("poudBlocage");
	}

	public void setPoudBlocage(String value) {
		takeStoredValueForKey(value, "poudBlocage");
	}
	public String poudOuverture() {
		return (String) storedValueForKey("poudOuverture");
	}

	public void setPoudOuverture(String value) {
		takeStoredValueForKey(value, "poudOuverture");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPreOuvertureDiplome.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPreOuvertureDiplome.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPreOuvertureDiplome)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPreOuvertureDiplome fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreOuvertureDiplome fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPreOuvertureDiplome eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPreOuvertureDiplome)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPreOuvertureDiplome fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreOuvertureDiplome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreOuvertureDiplome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPreOuvertureDiplome eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPreOuvertureDiplome)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreOuvertureDiplome fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreOuvertureDiplome fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPreOuvertureDiplome eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPreOuvertureDiplome ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPreOuvertureDiplome createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPreOuvertureDiplome.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPreOuvertureDiplome.ENTITY_NAME + "' !");
		}
		else {
			EOPreOuvertureDiplome object = (EOPreOuvertureDiplome) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPreOuvertureDiplome localInstanceOfObject(EOEditingContext ec, EOPreOuvertureDiplome object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPreOuvertureDiplome " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPreOuvertureDiplome) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
