/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPaiement.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPaiement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_Paiement";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PAIEMENT";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "paieNumero";

	public static final ERXKey<NSTimestamp> PAIE_DATE = new ERXKey<NSTimestamp>("paieDate");
	public static final ERXKey<String> PAIE_NOM_TIREUR = new ERXKey<String>("paieNomTireur");
	public static final ERXKey<Integer> PAIE_NUMERO = new ERXKey<Integer>("paieNumero");
	public static final ERXKey<String> PAIE_NUMERO_CHEQUE = new ERXKey<String>("paieNumeroCheque");
	public static final ERXKey<String> PAIE_NUMERO_COMPTE = new ERXKey<String>("paieNumeroCompte");
	public static final ERXKey<String> PAIE_PRENOM_TIREUR = new ERXKey<String>("paiePrenomTireur");
	public static final ERXKey<java.math.BigDecimal> PAIE_SOMME = new ERXKey<java.math.BigDecimal>("paieSomme");
	public static final ERXKey<String> PAIE_TRANSACTION = new ERXKey<String>("paieTransaction");

	public static final String PAIE_DATE_KEY = "paieDate";
	public static final String PAIE_NOM_TIREUR_KEY = "paieNomTireur";
	public static final String PAIE_NUMERO_KEY = "paieNumero";
	public static final String PAIE_NUMERO_CHEQUE_KEY = "paieNumeroCheque";
	public static final String PAIE_NUMERO_COMPTE_KEY = "paieNumeroCompte";
	public static final String PAIE_PRENOM_TIREUR_KEY = "paiePrenomTireur";
	public static final String PAIE_SOMME_KEY = "paieSomme";
	public static final String PAIE_TRANSACTION_KEY = "paieTransaction";

	// Non visible attributes
	public static final String BANQ_CODE_KEY = "banqCode";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String RCPT_CODE_KEY = "rcptCode";

	// Colkeys
	public static final String PAIE_DATE_COLKEY = "PAIE_DATE";
	public static final String PAIE_NOM_TIREUR_COLKEY = "PAIE_NOM_TIREUR";
	public static final String PAIE_NUMERO_COLKEY = "PAIE_NUMERO";
	public static final String PAIE_NUMERO_CHEQUE_COLKEY = "PAIE_NUMERO_CHEQUE";
	public static final String PAIE_NUMERO_COMPTE_COLKEY = "PAIE_NUMERO_COMPTE";
	public static final String PAIE_PRENOM_TIREUR_COLKEY = "PAIE_PRENOM_TIREUR";
	public static final String PAIE_SOMME_COLKEY = "PAIE_SOMME";
	public static final String PAIE_TRANSACTION_COLKEY = "PAIE_TRANSACTION";

	// Non visible colkeys
	public static final String BANQ_CODE_COLKEY = "BANQ_CODE";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String RCPT_CODE_COLKEY = "RCPT_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBanque> TO_BANQUE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBanque>("toBanque");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement> TO_DETAIL_PAIEMENTS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement>("toDetailPaiements");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique> TO_HISTORIQUE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique>("toHistorique");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheancier> TO_PAIEMENT_ECHEANCIERS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheancier>("toPaiementEcheanciers");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte> TO_REPARTITION_COMPTE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte>("toRepartitionCompte");

	public static final String TO_BANQUE_KEY = "toBanque";
	public static final String TO_DETAIL_PAIEMENTS_KEY = "toDetailPaiements";
	public static final String TO_HISTORIQUE_KEY = "toHistorique";
	public static final String TO_PAIEMENT_ECHEANCIERS_KEY = "toPaiementEcheanciers";
	public static final String TO_REPARTITION_COMPTE_KEY = "toRepartitionCompte";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPaiement with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param paieDate
	 * @param paieNumero
	 * @param paieSomme
	 * @param paieTransaction
	 * @param toRepartitionCompte
	 * @return EOPaiement
	 */
	public static EOPaiement create(EOEditingContext editingContext, NSTimestamp paieDate, Integer paieNumero, java.math.BigDecimal paieSomme, String paieTransaction, org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte) {
		EOPaiement eo = (EOPaiement) createAndInsertInstance(editingContext);
		eo.setPaieDate(paieDate);
		eo.setPaieNumero(paieNumero);
		eo.setPaieSomme(paieSomme);
		eo.setPaieTransaction(paieTransaction);
		eo.setToRepartitionCompteRelationship(toRepartitionCompte);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPaiement.
	 *
	 * @param editingContext
	 * @return EOPaiement
	 */
	public static EOPaiement create(EOEditingContext editingContext) {
		EOPaiement eo = (EOPaiement) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPaiement localInstanceIn(EOEditingContext editingContext) {
		EOPaiement localInstance = (EOPaiement) localInstanceOfObject(editingContext, (EOPaiement) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPaiement localInstanceIn(EOEditingContext editingContext, EOPaiement eo) {
		EOPaiement localInstance = (eo == null) ? null : (EOPaiement) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public NSTimestamp paieDate() {
		return (NSTimestamp) storedValueForKey("paieDate");
	}

	public void setPaieDate(NSTimestamp value) {
		takeStoredValueForKey(value, "paieDate");
	}
	public String paieNomTireur() {
		return (String) storedValueForKey("paieNomTireur");
	}

	public void setPaieNomTireur(String value) {
		takeStoredValueForKey(value, "paieNomTireur");
	}
	public Integer paieNumero() {
		return (Integer) storedValueForKey("paieNumero");
	}

	public void setPaieNumero(Integer value) {
		takeStoredValueForKey(value, "paieNumero");
	}
	public String paieNumeroCheque() {
		return (String) storedValueForKey("paieNumeroCheque");
	}

	public void setPaieNumeroCheque(String value) {
		takeStoredValueForKey(value, "paieNumeroCheque");
	}
	public String paieNumeroCompte() {
		return (String) storedValueForKey("paieNumeroCompte");
	}

	public void setPaieNumeroCompte(String value) {
		takeStoredValueForKey(value, "paieNumeroCompte");
	}
	public String paiePrenomTireur() {
		return (String) storedValueForKey("paiePrenomTireur");
	}

	public void setPaiePrenomTireur(String value) {
		takeStoredValueForKey(value, "paiePrenomTireur");
	}
	public java.math.BigDecimal paieSomme() {
		return (java.math.BigDecimal) storedValueForKey("paieSomme");
	}

	public void setPaieSomme(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "paieSomme");
	}
	public String paieTransaction() {
		return (String) storedValueForKey("paieTransaction");
	}

	public void setPaieTransaction(String value) {
		takeStoredValueForKey(value, "paieTransaction");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOBanque toBanque() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOBanque)storedValueForKey("toBanque");
	}

	public void setToBanqueRelationship(org.cocktail.scolarix.serveur.metier.eos.EOBanque value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOBanque oldValue = toBanque();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBanque");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toBanque");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOHistorique)storedValueForKey("toHistorique");
	}

	public void setToHistoriqueRelationship(org.cocktail.scolarix.serveur.metier.eos.EOHistorique value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOHistorique oldValue = toHistorique();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toHistorique");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toHistorique");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte)storedValueForKey("toRepartitionCompte");
	}

	public void setToRepartitionCompteRelationship(org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte oldValue = toRepartitionCompte();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartitionCompte");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRepartitionCompte");
		}
	}
  
	public NSArray toDetailPaiements() {
		return (NSArray)storedValueForKey("toDetailPaiements");
	}

	public NSArray toDetailPaiements(EOQualifier qualifier) {
		return toDetailPaiements(qualifier, null, false);
	}

	public NSArray toDetailPaiements(EOQualifier qualifier, boolean fetch) {
		return toDetailPaiements(qualifier, null, fetch);
	}

	public NSArray toDetailPaiements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement.TO_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toDetailPaiements();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
	}

	public void removeFromToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement createToDetailPaiementsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_DetailPaiement");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toDetailPaiements");
		return (org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement) eo;
	}

	public void deleteToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
				editingContext().deleteObject(object);
			}

	public void deleteAllToDetailPaiementsRelationships() {
		Enumeration objects = toDetailPaiements().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToDetailPaiementsRelationship((org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement)objects.nextElement());
		}
	}
	public NSArray toPaiementEcheanciers() {
		return (NSArray)storedValueForKey("toPaiementEcheanciers");
	}

	public NSArray toPaiementEcheanciers(EOQualifier qualifier) {
		return toPaiementEcheanciers(qualifier, null);
	}

	public NSArray toPaiementEcheanciers(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPaiementEcheanciers();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPaiementEcheanciersRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheancier object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPaiementEcheanciers");
	}

	public void removeFromToPaiementEcheanciersRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheancier object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPaiementEcheanciers");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheancier createToPaiementEcheanciersRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PaiementEcheancier");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPaiementEcheanciers");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheancier) eo;
	}

	public void deleteToPaiementEcheanciersRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheancier object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPaiementEcheanciers");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPaiementEcheanciersRelationships() {
		Enumeration objects = toPaiementEcheanciers().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPaiementEcheanciersRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheancier)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPaiement.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPaiement.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPaiement)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPaiement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPaiement)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPaiement fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPaiement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPaiement)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPaiement fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPaiement ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPaiement createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPaiement.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPaiement.ENTITY_NAME + "' !");
		}
		else {
			EOPaiement object = (EOPaiement) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPaiement localInstanceOfObject(EOEditingContext ec, EOPaiement object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPaiement " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPaiement) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
