/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPaiementEcheancier.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPaiementEcheancier extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PaiementEcheancier";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PAIEMENT_ECHEANCIER";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "pechrNumero";

	public static final ERXKey<Integer> ECHE_ECHEANCIER_ORDRE = new ERXKey<Integer>("echeEcheancierOrdre");
	public static final ERXKey<Integer> HIST_ANNEE_SCOL = new ERXKey<Integer>("histAnneeScol");
	public static final ERXKey<Integer> PAIE_NUMERO = new ERXKey<Integer>("paieNumero");
	public static final ERXKey<String> PECHR_REFERENCE = new ERXKey<String>("pechrReference");
	public static final ERXKey<Integer> RIB_ORDRE = new ERXKey<Integer>("ribOrdre");

	public static final String ECHE_ECHEANCIER_ORDRE_KEY = "echeEcheancierOrdre";
	public static final String HIST_ANNEE_SCOL_KEY = "histAnneeScol";
	public static final String PAIE_NUMERO_KEY = "paieNumero";
	public static final String PECHR_REFERENCE_KEY = "pechrReference";
	public static final String RIB_ORDRE_KEY = "ribOrdre";

	// Non visible attributes
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String PECHR_NUMERO_KEY = "pechrNumero";

	// Colkeys
	public static final String ECHE_ECHEANCIER_ORDRE_COLKEY = "ECHE_ECHEANCIER_ORDRE";
	public static final String HIST_ANNEE_SCOL_COLKEY = "HIST_ANNEE_SCOL";
	public static final String PAIE_NUMERO_COLKEY = "PAIE_NUMERO";
	public static final String PECHR_REFERENCE_COLKEY = "PECHR_REFERENCE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";

	// Non visible colkeys
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String PECHR_NUMERO_COLKEY = "PECHR_NUMERO";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> TO_FWKPERS__FOURNIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>("toFwkpers_Fournis");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique> TO_HISTORIQUE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique>("toHistorique");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance> TO_PAIEMENT_ECHEANCES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance>("toPaiementEcheances");

	public static final String TO_FWKPERS__FOURNIS_KEY = "toFwkpers_Fournis";
	public static final String TO_HISTORIQUE_KEY = "toHistorique";
	public static final String TO_PAIEMENT_ECHEANCES_KEY = "toPaiementEcheances";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPaiementEcheancier with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param histAnneeScol
	 * @param paieNumero
	 * @param toFwkpers_Fournis
	 * @param toHistorique
	 * @return EOPaiementEcheancier
	 */
	public static EOPaiementEcheancier create(EOEditingContext editingContext, Integer histAnneeScol, Integer paieNumero, org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFwkpers_Fournis, org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique) {
		EOPaiementEcheancier eo = (EOPaiementEcheancier) createAndInsertInstance(editingContext);
		eo.setHistAnneeScol(histAnneeScol);
		eo.setPaieNumero(paieNumero);
		eo.setToFwkpers_FournisRelationship(toFwkpers_Fournis);
		eo.setToHistoriqueRelationship(toHistorique);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPaiementEcheancier.
	 *
	 * @param editingContext
	 * @return EOPaiementEcheancier
	 */
	public static EOPaiementEcheancier create(EOEditingContext editingContext) {
		EOPaiementEcheancier eo = (EOPaiementEcheancier) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPaiementEcheancier localInstanceIn(EOEditingContext editingContext) {
		EOPaiementEcheancier localInstance = (EOPaiementEcheancier) localInstanceOfObject(editingContext, (EOPaiementEcheancier) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPaiementEcheancier localInstanceIn(EOEditingContext editingContext, EOPaiementEcheancier eo) {
		EOPaiementEcheancier localInstance = (eo == null) ? null : (EOPaiementEcheancier) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer echeEcheancierOrdre() {
		return (Integer) storedValueForKey("echeEcheancierOrdre");
	}

	public void setEcheEcheancierOrdre(Integer value) {
		takeStoredValueForKey(value, "echeEcheancierOrdre");
	}
	public Integer histAnneeScol() {
		return (Integer) storedValueForKey("histAnneeScol");
	}

	public void setHistAnneeScol(Integer value) {
		takeStoredValueForKey(value, "histAnneeScol");
	}
	public Integer paieNumero() {
		return (Integer) storedValueForKey("paieNumero");
	}

	public void setPaieNumero(Integer value) {
		takeStoredValueForKey(value, "paieNumero");
	}
	public String pechrReference() {
		return (String) storedValueForKey("pechrReference");
	}

	public void setPechrReference(String value) {
		takeStoredValueForKey(value, "pechrReference");
	}
	public Integer ribOrdre() {
		return (Integer) storedValueForKey("ribOrdre");
	}

	public void setRibOrdre(Integer value) {
		takeStoredValueForKey(value, "ribOrdre");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFwkpers_Fournis() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis)storedValueForKey("toFwkpers_Fournis");
	}

	public void setToFwkpers_FournisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOFournis oldValue = toFwkpers_Fournis();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Fournis");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Fournis");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOHistorique)storedValueForKey("toHistorique");
	}

	public void setToHistoriqueRelationship(org.cocktail.scolarix.serveur.metier.eos.EOHistorique value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOHistorique oldValue = toHistorique();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toHistorique");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toHistorique");
		}
	}
  
	public NSArray toPaiementEcheances() {
		return (NSArray)storedValueForKey("toPaiementEcheances");
	}

	public NSArray toPaiementEcheances(EOQualifier qualifier) {
		return toPaiementEcheances(qualifier, null);
	}

	public NSArray toPaiementEcheances(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPaiementEcheances();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPaiementEcheancesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPaiementEcheances");
	}

	public void removeFromToPaiementEcheancesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPaiementEcheances");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance createToPaiementEcheancesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PaiementEcheance");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPaiementEcheances");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance) eo;
	}

	public void deleteToPaiementEcheancesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPaiementEcheances");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPaiementEcheancesRelationships() {
		Enumeration objects = toPaiementEcheances().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPaiementEcheancesRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPaiementEcheance)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPaiementEcheancier.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPaiementEcheancier.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPaiementEcheancier)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPaiementEcheancier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPaiementEcheancier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPaiementEcheancier eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPaiementEcheancier)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPaiementEcheancier fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPaiementEcheancier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPaiementEcheancier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPaiementEcheancier eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPaiementEcheancier)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPaiementEcheancier fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPaiementEcheancier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPaiementEcheancier eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPaiementEcheancier ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPaiementEcheancier createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPaiementEcheancier.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPaiementEcheancier.ENTITY_NAME + "' !");
		}
		else {
			EOPaiementEcheancier object = (EOPaiementEcheancier) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPaiementEcheancier localInstanceOfObject(EOEditingContext ec, EOPaiementEcheancier object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPaiementEcheancier " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPaiementEcheancier) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
