/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.metier.eos;

import com.webobjects.foundation.NSValidation;

public class EOVWebInscriptionResultat extends _EOVWebInscriptionResultat {

	public static final String LIBELLE_DIPLOME_COMPLET_KEY = "LIBELLE_DIPLOME_COMPLET";

	public EOVWebInscriptionResultat() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Retourne l'annee suivie (1, 2 ou 3 en L, 1 ou 2 en M, ...).
	 * 
	 * @return Integer
	 */
	public Integer niveau() {
		return idiplAnneeSuivie();
	}

	/**
	 * Retourne le semestre de cette inscription.
	 * 
	 * @return String
	 */
	public String semestre() {
		return niveauDetail();
	}

	/**
	 * Retourne les credits ECTS obtenus pour cette inscription.
	 * 
	 * @return Double
	 */
	public Double creditsObtenus() {
		return credits();
	}

	/**
	 * Retourne le libelle du resultat.
	 * 
	 * @return String
	 */
	public String resultatLibelle() {
		String resultat = "N/D";
		if (etat() != null && etat().intValue() == 0) {
			resultat = "Non obtenu";
		}
		if (etat() != null && etat().intValue() == 1) {
			resultat = "Obtenu en session 1";
		}
		if (etat() != null && etat().intValue() == 2) {
			resultat = "Obtenu en session 2";
		}
		return resultat;
	}

	/**
	 * Retourne l'etat du resultat.
	 * 
	 * @return String
	 */
	public String etatLibelle() {
		String etat = "N/D";
		if (etatType() != null && etatType().intValue() == 0) {
			etat = "En attente";
		}
		if (etatType() != null && etatType().intValue() == 1) {
			etat = "Validé";
		}
		return etat;
	}

	/**
	 * Retourne l'abreviation de la filiere selon LMD or not.
	 * 
	 * @return String
	 */
	public String filiere() {
		return toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().filiere().abreviation();
	}

	/**
	 * Retourne le libelle abrege du diplome.
	 * 
	 * @return String
	 */
	public String libelleDiplomeAbrege() {
		return toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipAbreviation();
	}

	/**
	 * Retourne le libelle complet du diplome.
	 * 
	 * @return String
	 */
	public String libelleDiplomeComplet() {
		return toFwkScolarite_ScolFormationSpecialisation().libelleDiplomeComplet(idiplAnneeSuivie());
	}

}
