/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.scolarix.serveur.exception.PreCandidatException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPreCandidat extends _EOPreCandidat {

	public EOPreCandidat() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Determine si ce pre-candidat est en PRE-inscription ou non.
	 * 
	 * @return true si c'est une PRE-inscription, false si c'est une RE-inscription.
	 */
	public boolean isPreInscription() {
		return (toEtudiant() == null);
	}

	/**
	 * Determine si ce pre-candidat est en RE-inscription ou non.
	 * 
	 * @return true si c'est une RE-inscription, false si c'est une PRE-inscription.
	 */
	public boolean isReInscription() {
		return !isPreInscription();
	}

	/**
	 * Determine si ce pre-candidat est deja pre-inscrit ou non
	 * 
	 * @return true si il est deja pre-inscrit, false sinon
	 */
	public boolean isPreInscrit() {
		return (preEtudiant() != null);
	}

	/**
	 * Determine si le pre-candidat est deja inscrit pour l'annee de pre ou pre-re inscription
	 * 
	 * @param anneePreInscription
	 * @param anneePreReInscription
	 * @return true si deja inscrit administrativement, false sinon
	 */
	public boolean isInscrit(Integer anneePreInscription, Integer anneePreReInscription) {
		if (isReInscription()) {
			if (toEtudiant().historique(anneePreReInscription) != null) {
				return true;
			}
		}
		else {
			IEtudiant myEtudiant = EOEtudiant.fetchByKeyValue(editingContext(), EOEtudiant.ETUD_CODE_INE_KEY, candBea());
			if (myEtudiant != null) {
				if (myEtudiant.historique(anneePreInscription) != null) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Retourne le PreEtudiant associe a ce PreCandidat.<BR>
	 * Le PreEtudiant est recherche en priorite par la relation preEtudiants() sur le numero INE/BEA (candBEA).<BR>
	 * S'il n'est pas trouve par cette relation, recherche par la relation preEtudiantsPostBac() sur le numero PostBac (candNumero).
	 * 
	 * @return Un EOPreEtudiant s'il existe (deja passe par la procedure de pre ou re inscription), NULL si c'est son premier acces.
	 * @throws PreCandidatException
	 *             Si plusieurs PreEtudiants existent pour ce PreCandidat, ce qui ne devrait jamais arriver.
	 */
	public EOPreEtudiant preEtudiant() throws PreCandidatException {
		EOPreEtudiant preEtudiant = null;
		NSArray<EOPreEtudiant> array = super.toPreEtudiants();
		if (candBea() != null && array != null && array.count() > 0) {
			if (array.count() > 1) {
				throw new PreCandidatException("Il y a un problème avec votre Pre-Inscription existante (numéro INE/BEA)");
			}
			preEtudiant = array.lastObject();
		}
		else {
			array = super.toPreEtudiantsPostBac();
			if (candNumero() != null && array != null && array.count() > 0) {
				if (array.count() > 1) {
					throw new PreCandidatException("Il y a un problème avec votre Pre-Inscription existante (numéro PostBac)");
				}
				preEtudiant = array.lastObject();
			}
		}
		return preEtudiant;
	}

}
