/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVBordereauEntete.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVBordereauEntete extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VBordereauEntete";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_BORDEREAU_ENTETE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "borId";

	public static final ERXKey<String> BOR_ETAT = new ERXKey<String>("borEtat");
	public static final ERXKey<Integer> EREMB_ANNEE = new ERXKey<Integer>("erembAnnee");
	public static final ERXKey<Integer> EREMB_NUM_BORDEREAU = new ERXKey<Integer>("erembNumBordereau");
	public static final ERXKey<String> ETAB_CODE = new ERXKey<String>("etabCode");
	public static final ERXKey<String> ETAB_LIBELLE = new ERXKey<String>("etabLibelle");

	public static final String BOR_ETAT_KEY = "borEtat";
	public static final String EREMB_ANNEE_KEY = "erembAnnee";
	public static final String EREMB_NUM_BORDEREAU_KEY = "erembNumBordereau";
	public static final String ETAB_CODE_KEY = "etabCode";
	public static final String ETAB_LIBELLE_KEY = "etabLibelle";

	// Non visible attributes
	public static final String BOR_ID_KEY = "borId";

	// Colkeys
	public static final String BOR_ETAT_COLKEY = "BOR_ETAT";
	public static final String EREMB_ANNEE_COLKEY = "EREMB_ANNEE";
	public static final String EREMB_NUM_BORDEREAU_COLKEY = "EREMB_NUM_BORDEREAU";
	public static final String ETAB_CODE_COLKEY = "ETAB_CODE";
	public static final String ETAB_LIBELLE_COLKEY = "ETAB_LIBELLE";

	// Non visible colkeys
	public static final String BOR_ID_COLKEY = "BOR_ID";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail> TO_V_BORDEREAU_DETAILS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail>("toVBordereauDetails");

	public static final String TO_V_BORDEREAU_DETAILS_KEY = "toVBordereauDetails";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVBordereauEntete with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @return EOVBordereauEntete
	 */
	public static EOVBordereauEntete create(EOEditingContext editingContext) {
		EOVBordereauEntete eo = (EOVBordereauEntete) createAndInsertInstance(editingContext);
		return eo;
	}


	// Utilities methods

	public EOVBordereauEntete localInstanceIn(EOEditingContext editingContext) {
		EOVBordereauEntete localInstance = (EOVBordereauEntete) localInstanceOfObject(editingContext, (EOVBordereauEntete) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVBordereauEntete localInstanceIn(EOEditingContext editingContext, EOVBordereauEntete eo) {
		EOVBordereauEntete localInstance = (eo == null) ? null : (EOVBordereauEntete) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String borEtat() {
		return (String) storedValueForKey("borEtat");
	}

	public void setBorEtat(String value) {
		takeStoredValueForKey(value, "borEtat");
	}
	public Integer erembAnnee() {
		return (Integer) storedValueForKey("erembAnnee");
	}

	public void setErembAnnee(Integer value) {
		takeStoredValueForKey(value, "erembAnnee");
	}
	public Integer erembNumBordereau() {
		return (Integer) storedValueForKey("erembNumBordereau");
	}

	public void setErembNumBordereau(Integer value) {
		takeStoredValueForKey(value, "erembNumBordereau");
	}
	public String etabCode() {
		return (String) storedValueForKey("etabCode");
	}

	public void setEtabCode(String value) {
		takeStoredValueForKey(value, "etabCode");
	}
	public String etabLibelle() {
		return (String) storedValueForKey("etabLibelle");
	}

	public void setEtabLibelle(String value) {
		takeStoredValueForKey(value, "etabLibelle");
	}

	public NSArray toVBordereauDetails() {
		return (NSArray)storedValueForKey("toVBordereauDetails");
	}

	public NSArray toVBordereauDetails(EOQualifier qualifier) {
		return toVBordereauDetails(qualifier, null);
	}

	public NSArray toVBordereauDetails(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toVBordereauDetails();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToVBordereauDetailsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toVBordereauDetails");
	}

	public void removeFromToVBordereauDetailsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toVBordereauDetails");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail createToVBordereauDetailsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_VBordereauDetail");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toVBordereauDetails");
		return (org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail) eo;
	}

	public void deleteToVBordereauDetailsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toVBordereauDetails");
				editingContext().deleteObject(object);
			}

	public void deleteAllToVBordereauDetailsRelationships() {
		Enumeration objects = toVBordereauDetails().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToVBordereauDetailsRelationship((org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVBordereauEntete.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVBordereauEntete.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVBordereauEntete)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVBordereauEntete fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVBordereauEntete fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVBordereauEntete eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVBordereauEntete)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVBordereauEntete fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVBordereauEntete fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVBordereauEntete fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVBordereauEntete eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVBordereauEntete)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVBordereauEntete fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVBordereauEntete fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVBordereauEntete eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVBordereauEntete ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVBordereauEntete createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVBordereauEntete.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVBordereauEntete.ENTITY_NAME + "' !");
		}
		else {
			EOVBordereauEntete object = (EOVBordereauEntete) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVBordereauEntete localInstanceOfObject(EOEditingContext ec, EOVBordereauEntete object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVBordereauEntete " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVBordereauEntete) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
