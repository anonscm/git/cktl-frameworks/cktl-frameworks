/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGarnucheApplication.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOGarnucheApplication extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_GarnucheApplication";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.GARNUCHE_APPLICATION";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "applKey";

	public static final ERXKey<String> APPL_CODE = new ERXKey<String>("applCode");
	public static final ERXKey<String> APPL_COMMENTAIRE = new ERXKey<String>("applCommentaire");
	public static final ERXKey<String> APPL_NOM = new ERXKey<String>("applNom");
	public static final ERXKey<String> C_RNE = new ERXKey<String>("cRne");

	public static final String APPL_CODE_KEY = "applCode";
	public static final String APPL_COMMENTAIRE_KEY = "applCommentaire";
	public static final String APPL_NOM_KEY = "applNom";
	public static final String C_RNE_KEY = "cRne";

	// Non visible attributes
	public static final String APPL_KEY_KEY = "applKey";

	// Colkeys
	public static final String APPL_CODE_COLKEY = "APPL_CODE";
	public static final String APPL_COMMENTAIRE_COLKEY = "APPL_COMMENTAIRE";
	public static final String APPL_NOM_COLKEY = "APPL_NOM";
	public static final String C_RNE_COLKEY = "C_RNE";

	// Non visible colkeys
	public static final String APPL_KEY_COLKEY = "APPL_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation> TO_GARNUCHE_APPLI_DOCUMENTATIONS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation>("toGarnucheAppliDocumentations");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliOuverture> TO_GARNUCHE_APPLI_OUVERTURES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliOuverture>("toGarnucheAppliOuvertures");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite> TO_V_COMPOSANTE_SCOLARITE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite>("toVComposanteScolarite");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite> TO_V_ETABLISSEMENT_SCOLARITE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite>("toVEtablissementScolarite");

	public static final String TO_GARNUCHE_APPLI_DOCUMENTATIONS_KEY = "toGarnucheAppliDocumentations";
	public static final String TO_GARNUCHE_APPLI_OUVERTURES_KEY = "toGarnucheAppliOuvertures";
	public static final String TO_V_COMPOSANTE_SCOLARITE_KEY = "toVComposanteScolarite";
	public static final String TO_V_ETABLISSEMENT_SCOLARITE_KEY = "toVEtablissementScolarite";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOGarnucheApplication with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cRne
	 * @param toVComposanteScolarite
	 * @param toVEtablissementScolarite
	 * @return EOGarnucheApplication
	 */
	public static EOGarnucheApplication create(EOEditingContext editingContext, String cRne, org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite toVComposanteScolarite, org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite toVEtablissementScolarite) {
		EOGarnucheApplication eo = (EOGarnucheApplication) createAndInsertInstance(editingContext);
		eo.setCRne(cRne);
		eo.setToVComposanteScolariteRelationship(toVComposanteScolarite);
		eo.setToVEtablissementScolariteRelationship(toVEtablissementScolarite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOGarnucheApplication.
	 *
	 * @param editingContext
	 * @return EOGarnucheApplication
	 */
	public static EOGarnucheApplication create(EOEditingContext editingContext) {
		EOGarnucheApplication eo = (EOGarnucheApplication) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOGarnucheApplication localInstanceIn(EOEditingContext editingContext) {
		EOGarnucheApplication localInstance = (EOGarnucheApplication) localInstanceOfObject(editingContext, (EOGarnucheApplication) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOGarnucheApplication localInstanceIn(EOEditingContext editingContext, EOGarnucheApplication eo) {
		EOGarnucheApplication localInstance = (eo == null) ? null : (EOGarnucheApplication) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String applCode() {
		return (String) storedValueForKey("applCode");
	}

	public void setApplCode(String value) {
		takeStoredValueForKey(value, "applCode");
	}
	public String applCommentaire() {
		return (String) storedValueForKey("applCommentaire");
	}

	public void setApplCommentaire(String value) {
		takeStoredValueForKey(value, "applCommentaire");
	}
	public String applNom() {
		return (String) storedValueForKey("applNom");
	}

	public void setApplNom(String value) {
		takeStoredValueForKey(value, "applNom");
	}
	public String cRne() {
		return (String) storedValueForKey("cRne");
	}

	public void setCRne(String value) {
		takeStoredValueForKey(value, "cRne");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite toVComposanteScolarite() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite)storedValueForKey("toVComposanteScolarite");
	}

	public void setToVComposanteScolariteRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite oldValue = toVComposanteScolarite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVComposanteScolarite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toVComposanteScolarite");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite toVEtablissementScolarite() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite)storedValueForKey("toVEtablissementScolarite");
	}

	public void setToVEtablissementScolariteRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite oldValue = toVEtablissementScolarite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVEtablissementScolarite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toVEtablissementScolarite");
		}
	}
  
	public NSArray toGarnucheAppliDocumentations() {
		return (NSArray)storedValueForKey("toGarnucheAppliDocumentations");
	}

	public NSArray toGarnucheAppliDocumentations(EOQualifier qualifier) {
		return toGarnucheAppliDocumentations(qualifier, null, false);
	}

	public NSArray toGarnucheAppliDocumentations(EOQualifier qualifier, boolean fetch) {
		return toGarnucheAppliDocumentations(qualifier, null, fetch);
	}

	public NSArray toGarnucheAppliDocumentations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation.TO_GARNUCHE_APPLICATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toGarnucheAppliDocumentations();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToGarnucheAppliDocumentationsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toGarnucheAppliDocumentations");
	}

	public void removeFromToGarnucheAppliDocumentationsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toGarnucheAppliDocumentations");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation createToGarnucheAppliDocumentationsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_GarnucheAppliDocumentation");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toGarnucheAppliDocumentations");
		return (org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation) eo;
	}

	public void deleteToGarnucheAppliDocumentationsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toGarnucheAppliDocumentations");
				editingContext().deleteObject(object);
			}

	public void deleteAllToGarnucheAppliDocumentationsRelationships() {
		Enumeration objects = toGarnucheAppliDocumentations().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToGarnucheAppliDocumentationsRelationship((org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliDocumentation)objects.nextElement());
		}
	}
	public NSArray toGarnucheAppliOuvertures() {
		return (NSArray)storedValueForKey("toGarnucheAppliOuvertures");
	}

	public NSArray toGarnucheAppliOuvertures(EOQualifier qualifier) {
		return toGarnucheAppliOuvertures(qualifier, null);
	}

	public NSArray toGarnucheAppliOuvertures(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toGarnucheAppliOuvertures();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToGarnucheAppliOuverturesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliOuverture object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toGarnucheAppliOuvertures");
	}

	public void removeFromToGarnucheAppliOuverturesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliOuverture object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toGarnucheAppliOuvertures");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliOuverture createToGarnucheAppliOuverturesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_GarnucheAppliOuverture");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toGarnucheAppliOuvertures");
		return (org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliOuverture) eo;
	}

	public void deleteToGarnucheAppliOuverturesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliOuverture object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toGarnucheAppliOuvertures");
				editingContext().deleteObject(object);
			}

	public void deleteAllToGarnucheAppliOuverturesRelationships() {
		Enumeration objects = toGarnucheAppliOuvertures().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToGarnucheAppliOuverturesRelationship((org.cocktail.scolarix.serveur.metier.eos.EOGarnucheAppliOuverture)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOGarnucheApplication.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOGarnucheApplication.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOGarnucheApplication)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOGarnucheApplication fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGarnucheApplication fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOGarnucheApplication eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOGarnucheApplication)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOGarnucheApplication fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOGarnucheApplication eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOGarnucheApplication)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheApplication fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheApplication fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOGarnucheApplication eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOGarnucheApplication ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchFetchAll(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", "FwkScolarix_GarnucheApplication");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EOGarnucheApplication createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOGarnucheApplication.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOGarnucheApplication.ENTITY_NAME + "' !");
		}
		else {
			EOGarnucheApplication object = (EOGarnucheApplication) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOGarnucheApplication localInstanceOfObject(EOEditingContext ec, EOGarnucheApplication object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOGarnucheApplication " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOGarnucheApplication) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
