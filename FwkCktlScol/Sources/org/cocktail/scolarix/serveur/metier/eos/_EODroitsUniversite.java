/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODroitsUniversite.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EODroitsUniversite extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_DroitsUniversite";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.DROITS_UNIVERSITE";

	//Attributes

	public static final ERXKey<Integer> DUNIV_ANNEE_SCOL = new ERXKey<Integer>("dunivAnneeScol");
	public static final ERXKey<java.math.BigDecimal> DUNIV_MONTANT = new ERXKey<java.math.BigDecimal>("dunivMontant");
	public static final ERXKey<String> DUNIV_TYPE_DROIT = new ERXKey<String>("dunivTypeDroit");
	public static final ERXKey<String> RCPT_CODE = new ERXKey<String>("rcptCode");

	public static final String DUNIV_ANNEE_SCOL_KEY = "dunivAnneeScol";
	public static final String DUNIV_MONTANT_KEY = "dunivMontant";
	public static final String DUNIV_TYPE_DROIT_KEY = "dunivTypeDroit";
	public static final String RCPT_CODE_KEY = "rcptCode";

	// Non visible attributes

	// Colkeys
	public static final String DUNIV_ANNEE_SCOL_COLKEY = "DUNIV_ANNEE_SCOL";
	public static final String DUNIV_MONTANT_COLKEY = "DUNIV_MONTANT";
	public static final String DUNIV_TYPE_DROIT_COLKEY = "DUNIV_TYPE_DROIT";
	public static final String RCPT_CODE_COLKEY = "RCPT_CODE";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte> TO_REPARTITION_COMPTE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte>("toRepartitionCompte");

	public static final String TO_REPARTITION_COMPTE_KEY = "toRepartitionCompte";

	// Create / Init methods

	/**
	 * Creates and inserts a new EODroitsUniversite with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dunivAnneeScol
	 * @param dunivMontant
	 * @param dunivTypeDroit
	 * @param rcptCode
	 * @param toRepartitionCompte
	 * @return EODroitsUniversite
	 */
	public static EODroitsUniversite create(EOEditingContext editingContext, Integer dunivAnneeScol, java.math.BigDecimal dunivMontant, String dunivTypeDroit, String rcptCode, org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte) {
		EODroitsUniversite eo = (EODroitsUniversite) createAndInsertInstance(editingContext);
		eo.setDunivAnneeScol(dunivAnneeScol);
		eo.setDunivMontant(dunivMontant);
		eo.setDunivTypeDroit(dunivTypeDroit);
		eo.setRcptCode(rcptCode);
		eo.setToRepartitionCompteRelationship(toRepartitionCompte);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EODroitsUniversite.
	 *
	 * @param editingContext
	 * @return EODroitsUniversite
	 */
	public static EODroitsUniversite create(EOEditingContext editingContext) {
		EODroitsUniversite eo = (EODroitsUniversite) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EODroitsUniversite localInstanceIn(EOEditingContext editingContext) {
		EODroitsUniversite localInstance = (EODroitsUniversite) localInstanceOfObject(editingContext, (EODroitsUniversite) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EODroitsUniversite localInstanceIn(EOEditingContext editingContext, EODroitsUniversite eo) {
		EODroitsUniversite localInstance = (eo == null) ? null : (EODroitsUniversite) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer dunivAnneeScol() {
		return (Integer) storedValueForKey("dunivAnneeScol");
	}

	public void setDunivAnneeScol(Integer value) {
		takeStoredValueForKey(value, "dunivAnneeScol");
	}
	public java.math.BigDecimal dunivMontant() {
		return (java.math.BigDecimal) storedValueForKey("dunivMontant");
	}

	public void setDunivMontant(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "dunivMontant");
	}
	public String dunivTypeDroit() {
		return (String) storedValueForKey("dunivTypeDroit");
	}

	public void setDunivTypeDroit(String value) {
		takeStoredValueForKey(value, "dunivTypeDroit");
	}
	public String rcptCode() {
		return (String) storedValueForKey("rcptCode");
	}

	public void setRcptCode(String value) {
		takeStoredValueForKey(value, "rcptCode");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte)storedValueForKey("toRepartitionCompte");
	}

	public void setToRepartitionCompteRelationship(org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte oldValue = toRepartitionCompte();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartitionCompte");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRepartitionCompte");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EODroitsUniversite.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EODroitsUniversite.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EODroitsUniversite)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EODroitsUniversite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODroitsUniversite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EODroitsUniversite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EODroitsUniversite)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EODroitsUniversite fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EODroitsUniversite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EODroitsUniversite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EODroitsUniversite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EODroitsUniversite)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EODroitsUniversite fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EODroitsUniversite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EODroitsUniversite eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EODroitsUniversite ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EODroitsUniversite createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EODroitsUniversite.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EODroitsUniversite.ENTITY_NAME + "' !");
		}
		else {
			EODroitsUniversite object = (EODroitsUniversite) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EODroitsUniversite localInstanceOfObject(EOEditingContext ec, EODroitsUniversite object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EODroitsUniversite " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EODroitsUniversite) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
