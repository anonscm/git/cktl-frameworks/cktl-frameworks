/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVRdvJournees.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVRdvJournees extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VRdvJournees";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_RDV_JOURNEES";

	//Attributes

	public static final ERXKey<NSTimestamp> DATE_COMPLETE = new ERXKey<NSTimestamp>("dateComplete");
	public static final ERXKey<String> DATE_PER = new ERXKey<String>("datePer");
	public static final ERXKey<Integer> NUM_OCC_PLANNING = new ERXKey<Integer>("numOccPlanning");

	public static final String DATE_COMPLETE_KEY = "dateComplete";
	public static final String DATE_PER_KEY = "datePer";
	public static final String NUM_OCC_PLANNING_KEY = "numOccPlanning";

	// Non visible attributes

	// Colkeys
	public static final String DATE_COMPLETE_COLKEY = "DATE_COMPLETE";
	public static final String DATE_PER_COLKEY = "DATE_PER";
	public static final String NUM_OCC_PLANNING_COLKEY = "NUM_OCC_PLANNING";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires> TO_V_RDV_HORAIRESES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires>("toVRdvHoraireses");

	public static final String TO_V_RDV_HORAIRESES_KEY = "toVRdvHoraireses";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVRdvJournees with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param datePer
	 * @param numOccPlanning
	 * @return EOVRdvJournees
	 */
	public static EOVRdvJournees create(EOEditingContext editingContext, String datePer, Integer numOccPlanning) {
		EOVRdvJournees eo = (EOVRdvJournees) createAndInsertInstance(editingContext);
		eo.setDatePer(datePer);
		eo.setNumOccPlanning(numOccPlanning);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVRdvJournees.
	 *
	 * @param editingContext
	 * @return EOVRdvJournees
	 */
	public static EOVRdvJournees create(EOEditingContext editingContext) {
		EOVRdvJournees eo = (EOVRdvJournees) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVRdvJournees localInstanceIn(EOEditingContext editingContext) {
		EOVRdvJournees localInstance = (EOVRdvJournees) localInstanceOfObject(editingContext, (EOVRdvJournees) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVRdvJournees localInstanceIn(EOEditingContext editingContext, EOVRdvJournees eo) {
		EOVRdvJournees localInstance = (eo == null) ? null : (EOVRdvJournees) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public NSTimestamp dateComplete() {
		return (NSTimestamp) storedValueForKey("dateComplete");
	}

	public void setDateComplete(NSTimestamp value) {
		takeStoredValueForKey(value, "dateComplete");
	}
	public String datePer() {
		return (String) storedValueForKey("datePer");
	}

	public void setDatePer(String value) {
		takeStoredValueForKey(value, "datePer");
	}
	public Integer numOccPlanning() {
		return (Integer) storedValueForKey("numOccPlanning");
	}

	public void setNumOccPlanning(Integer value) {
		takeStoredValueForKey(value, "numOccPlanning");
	}

	public NSArray toVRdvHoraireses() {
		return (NSArray)storedValueForKey("toVRdvHoraireses");
	}

	public NSArray toVRdvHoraireses(EOQualifier qualifier) {
		return toVRdvHoraireses(qualifier, null);
	}

	public NSArray toVRdvHoraireses(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toVRdvHoraireses();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToVRdvHorairesesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toVRdvHoraireses");
	}

	public void removeFromToVRdvHorairesesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toVRdvHoraireses");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires createToVRdvHorairesesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_VRdvHoraires");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toVRdvHoraireses");
		return (org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires) eo;
	}

	public void deleteToVRdvHorairesesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toVRdvHoraireses");
				editingContext().deleteObject(object);
			}

	public void deleteAllToVRdvHorairesesRelationships() {
		Enumeration objects = toVRdvHoraireses().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToVRdvHorairesesRelationship((org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVRdvJournees.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVRdvJournees.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVRdvJournees)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVRdvJournees fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVRdvJournees fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVRdvJournees eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVRdvJournees)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVRdvJournees fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVRdvJournees fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVRdvJournees fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVRdvJournees eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVRdvJournees)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVRdvJournees fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVRdvJournees fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVRdvJournees eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVRdvJournees ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVRdvJournees createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVRdvJournees.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVRdvJournees.ENTITY_NAME + "' !");
		}
		else {
			EOVRdvJournees object = (EOVRdvJournees) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVRdvJournees localInstanceOfObject(EOEditingContext ec, EOVRdvJournees object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVRdvJournees " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVRdvJournees) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
