/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrePersonneTelephone.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPrePersonneTelephone extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PrePersonneTelephone";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PRE_PERSONNE_TELEPHONE";

	//Attributes

	public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
	public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Integer> INDICATIF = new ERXKey<Integer>("indicatif");
	public static final ERXKey<String> LISTE_ROUGE = new ERXKey<String>("listeRouge");
	public static final ERXKey<String> NO_TELEPHONE = new ERXKey<String>("noTelephone");
	public static final ERXKey<String> TEL_PRINCIPAL = new ERXKey<String>("telPrincipal");

	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INDICATIF_KEY = "indicatif";
	public static final String LISTE_ROUGE_KEY = "listeRouge";
	public static final String NO_TELEPHONE_KEY = "noTelephone";
	public static final String TEL_PRINCIPAL_KEY = "telPrincipal";

	// Non visible attributes
	public static final String PERS_ID_KEY = "persId";
	public static final String TYPE_NO_KEY = "typeNo";
	public static final String TYPE_TEL_KEY = "typeTel";

	// Colkeys
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String INDICATIF_COLKEY = "INDICATIF";
	public static final String LISTE_ROUGE_COLKEY = "LISTE_ROUGE";
	public static final String NO_TELEPHONE_COLKEY = "NO_TELEPHONE";
	public static final String TEL_PRINCIPAL_COLKEY = "TEL_PRINCIPAL";

	// Non visible colkeys
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TYPE_NO_COLKEY = "TYPE_NO";
	public static final String TYPE_TEL_COLKEY = "TYPE_TEL";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel> TO_FWKPERS__TYPE_NO_TEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel>("toFwkpers_TypeNoTel");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel> TO_FWKPERS__TYPE_TEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel>("toFwkpers_TypeTel");

	public static final String TO_FWKPERS__TYPE_NO_TEL_KEY = "toFwkpers_TypeNoTel";
	public static final String TO_FWKPERS__TYPE_TEL_KEY = "toFwkpers_TypeTel";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPrePersonneTelephone with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dCreation
	 * @param dDebVal
	 * @param dModification
	 * @param indicatif
	 * @param listeRouge
	 * @param noTelephone
	 * @param telPrincipal
	 * @param toFwkpers_TypeNoTel
	 * @param toFwkpers_TypeTel
	 * @return EOPrePersonneTelephone
	 */
	public static EOPrePersonneTelephone create(EOEditingContext editingContext, NSTimestamp dCreation, NSTimestamp dDebVal, NSTimestamp dModification, Integer indicatif, String listeRouge, String noTelephone, String telPrincipal, org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel toFwkpers_TypeNoTel, org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel toFwkpers_TypeTel) {
		EOPrePersonneTelephone eo = (EOPrePersonneTelephone) createAndInsertInstance(editingContext);
		eo.setDCreation(dCreation);
		eo.setDDebVal(dDebVal);
		eo.setDModification(dModification);
		eo.setIndicatif(indicatif);
		eo.setListeRouge(listeRouge);
		eo.setNoTelephone(noTelephone);
		eo.setTelPrincipal(telPrincipal);
		eo.setToFwkpers_TypeNoTelRelationship(toFwkpers_TypeNoTel);
		eo.setToFwkpers_TypeTelRelationship(toFwkpers_TypeTel);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPrePersonneTelephone.
	 *
	 * @param editingContext
	 * @return EOPrePersonneTelephone
	 */
	public static EOPrePersonneTelephone create(EOEditingContext editingContext) {
		EOPrePersonneTelephone eo = (EOPrePersonneTelephone) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPrePersonneTelephone localInstanceIn(EOEditingContext editingContext) {
		EOPrePersonneTelephone localInstance = (EOPrePersonneTelephone) localInstanceOfObject(editingContext, (EOPrePersonneTelephone) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPrePersonneTelephone localInstanceIn(EOEditingContext editingContext, EOPrePersonneTelephone eo) {
		EOPrePersonneTelephone localInstance = (eo == null) ? null : (EOPrePersonneTelephone) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cStructure() {
		return (String) storedValueForKey("cStructure");
	}

	public void setCStructure(String value) {
		takeStoredValueForKey(value, "cStructure");
	}
	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}
	public NSTimestamp dDebVal() {
		return (NSTimestamp) storedValueForKey("dDebVal");
	}

	public void setDDebVal(NSTimestamp value) {
		takeStoredValueForKey(value, "dDebVal");
	}
	public NSTimestamp dFinVal() {
		return (NSTimestamp) storedValueForKey("dFinVal");
	}

	public void setDFinVal(NSTimestamp value) {
		takeStoredValueForKey(value, "dFinVal");
	}
	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	public Integer indicatif() {
		return (Integer) storedValueForKey("indicatif");
	}

	public void setIndicatif(Integer value) {
		takeStoredValueForKey(value, "indicatif");
	}
	public String listeRouge() {
		return (String) storedValueForKey("listeRouge");
	}

	public void setListeRouge(String value) {
		takeStoredValueForKey(value, "listeRouge");
	}
	public String noTelephone() {
		return (String) storedValueForKey("noTelephone");
	}

	public void setNoTelephone(String value) {
		takeStoredValueForKey(value, "noTelephone");
	}
	public String telPrincipal() {
		return (String) storedValueForKey("telPrincipal");
	}

	public void setTelPrincipal(String value) {
		takeStoredValueForKey(value, "telPrincipal");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel toFwkpers_TypeNoTel() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel)storedValueForKey("toFwkpers_TypeNoTel");
	}

	public void setToFwkpers_TypeNoTelRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel oldValue = toFwkpers_TypeNoTel();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_TypeNoTel");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_TypeNoTel");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel toFwkpers_TypeTel() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel)storedValueForKey("toFwkpers_TypeTel");
	}

	public void setToFwkpers_TypeTelRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel oldValue = toFwkpers_TypeTel();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_TypeTel");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_TypeTel");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPrePersonneTelephone.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPrePersonneTelephone.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPrePersonneTelephone)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPrePersonneTelephone fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrePersonneTelephone fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPrePersonneTelephone eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPrePersonneTelephone)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPrePersonneTelephone fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPrePersonneTelephone fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPrePersonneTelephone fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPrePersonneTelephone eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPrePersonneTelephone)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPrePersonneTelephone fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPrePersonneTelephone fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPrePersonneTelephone eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPrePersonneTelephone ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPrePersonneTelephone createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPrePersonneTelephone.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPrePersonneTelephone.ENTITY_NAME + "' !");
		}
		else {
			EOPrePersonneTelephone object = (EOPrePersonneTelephone) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPrePersonneTelephone localInstanceOfObject(EOEditingContext ec, EOPrePersonneTelephone object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPrePersonneTelephone " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPrePersonneTelephone) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
