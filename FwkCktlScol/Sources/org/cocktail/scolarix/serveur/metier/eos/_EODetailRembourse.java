/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODetailRembourse.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EODetailRembourse extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_DetailRembourse";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.DETAIL_REMBOURSE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "drembNumero";

	public static final ERXKey<NSTimestamp> DREMB_DATE_OPERATION = new ERXKey<NSTimestamp>("drembDateOperation");
	public static final ERXKey<String> DREMB_ETAT = new ERXKey<String>("drembEtat");
	public static final ERXKey<java.math.BigDecimal> DREMB_MONTANT = new ERXKey<java.math.BigDecimal>("drembMontant");
	public static final ERXKey<Integer> DREMB_MOUVEMENT = new ERXKey<Integer>("drembMouvement");

	public static final String DREMB_DATE_OPERATION_KEY = "drembDateOperation";
	public static final String DREMB_ETAT_KEY = "drembEtat";
	public static final String DREMB_MONTANT_KEY = "drembMontant";
	public static final String DREMB_MOUVEMENT_KEY = "drembMouvement";

	// Non visible attributes
	public static final String DREMB_NUMERO_KEY = "drembNumero";
	public static final String EREMB_NUMERO_KEY = "erembNumero";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String PAIE_NUMERO_KEY = "paieNumero";
	public static final String RCPT_CODE_KEY = "rcptCode";

	// Colkeys
	public static final String DREMB_DATE_OPERATION_COLKEY = "DREMB_DATE_OPERATION";
	public static final String DREMB_ETAT_COLKEY = "DREMB_ETAT";
	public static final String DREMB_MONTANT_COLKEY = "DREMB_MONTANT";
	public static final String DREMB_MOUVEMENT_COLKEY = "DREMB_MOUVEMENT";

	// Non visible colkeys
	public static final String DREMB_NUMERO_COLKEY = "DREMB_NUMERO";
	public static final String EREMB_NUMERO_COLKEY = "EREMB_NUMERO";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String PAIE_NUMERO_COLKEY = "PAIE_NUMERO";
	public static final String RCPT_CODE_COLKEY = "RCPT_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse> TO_ETUDIANT_REMBOURSE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse>("toEtudiantRembourse");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl> TO_INSC_DIPL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl>("toInscDipl");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte> TO_REPARTITION_COMPTE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte>("toRepartitionCompte");

	public static final String TO_ETUDIANT_REMBOURSE_KEY = "toEtudiantRembourse";
	public static final String TO_INSC_DIPL_KEY = "toInscDipl";
	public static final String TO_REPARTITION_COMPTE_KEY = "toRepartitionCompte";

	// Create / Init methods

	/**
	 * Creates and inserts a new EODetailRembourse with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param drembDateOperation
	 * @param drembEtat
	 * @param drembMontant
	 * @param drembMouvement
	 * @param toEtudiantRembourse
	 * @param toInscDipl
	 * @param toRepartitionCompte
	 * @return EODetailRembourse
	 */
	public static EODetailRembourse create(EOEditingContext editingContext, NSTimestamp drembDateOperation, String drembEtat, java.math.BigDecimal drembMontant, Integer drembMouvement, org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse toEtudiantRembourse, org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl, org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte) {
		EODetailRembourse eo = (EODetailRembourse) createAndInsertInstance(editingContext);
		eo.setDrembDateOperation(drembDateOperation);
		eo.setDrembEtat(drembEtat);
		eo.setDrembMontant(drembMontant);
		eo.setDrembMouvement(drembMouvement);
		eo.setToEtudiantRembourseRelationship(toEtudiantRembourse);
		eo.setToInscDiplRelationship(toInscDipl);
		eo.setToRepartitionCompteRelationship(toRepartitionCompte);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EODetailRembourse.
	 *
	 * @param editingContext
	 * @return EODetailRembourse
	 */
	public static EODetailRembourse create(EOEditingContext editingContext) {
		EODetailRembourse eo = (EODetailRembourse) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EODetailRembourse localInstanceIn(EOEditingContext editingContext) {
		EODetailRembourse localInstance = (EODetailRembourse) localInstanceOfObject(editingContext, (EODetailRembourse) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EODetailRembourse localInstanceIn(EOEditingContext editingContext, EODetailRembourse eo) {
		EODetailRembourse localInstance = (eo == null) ? null : (EODetailRembourse) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public NSTimestamp drembDateOperation() {
		return (NSTimestamp) storedValueForKey("drembDateOperation");
	}

	public void setDrembDateOperation(NSTimestamp value) {
		takeStoredValueForKey(value, "drembDateOperation");
	}
	public String drembEtat() {
		return (String) storedValueForKey("drembEtat");
	}

	public void setDrembEtat(String value) {
		takeStoredValueForKey(value, "drembEtat");
	}
	public java.math.BigDecimal drembMontant() {
		return (java.math.BigDecimal) storedValueForKey("drembMontant");
	}

	public void setDrembMontant(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "drembMontant");
	}
	public Integer drembMouvement() {
		return (Integer) storedValueForKey("drembMouvement");
	}

	public void setDrembMouvement(Integer value) {
		takeStoredValueForKey(value, "drembMouvement");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse toEtudiantRembourse() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse)storedValueForKey("toEtudiantRembourse");
	}

	public void setToEtudiantRembourseRelationship(org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse oldValue = toEtudiantRembourse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEtudiantRembourse");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toEtudiantRembourse");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toInscDipl() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOInscDipl)storedValueForKey("toInscDipl");
	}

	public void setToInscDiplRelationship(org.cocktail.scolarix.serveur.metier.eos.EOInscDipl value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOInscDipl oldValue = toInscDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toInscDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toInscDipl");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte)storedValueForKey("toRepartitionCompte");
	}

	public void setToRepartitionCompteRelationship(org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte oldValue = toRepartitionCompte();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartitionCompte");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRepartitionCompte");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EODetailRembourse.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EODetailRembourse.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EODetailRembourse)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EODetailRembourse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODetailRembourse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EODetailRembourse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EODetailRembourse)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EODetailRembourse fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EODetailRembourse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EODetailRembourse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EODetailRembourse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EODetailRembourse)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EODetailRembourse fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EODetailRembourse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EODetailRembourse eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EODetailRembourse ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EODetailRembourse createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EODetailRembourse.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EODetailRembourse.ENTITY_NAME + "' !");
		}
		else {
			EODetailRembourse object = (EODetailRembourse) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EODetailRembourse localInstanceOfObject(EOEditingContext ec, EODetailRembourse object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EODetailRembourse " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EODetailRembourse) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
