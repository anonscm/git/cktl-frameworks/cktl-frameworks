/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.metier.eos;

import com.webobjects.foundation.NSValidation;

public class EOUtilisateur extends _EOUtilisateur {

	public EOUtilisateur() {
		super();
	}

	public boolean isConsult() {
		return utiConsult() != null && utiConsult().intValue() == 1;
	}

	public boolean isConsultDisabled() {
		return !isConsult();
	}

	public boolean isModif() {
		return utiModif() != null && utiModif().intValue() == 1;
	}

	public boolean isModifDisabled() {
		return !isModif();
	}

	public boolean isAjout() {
		return utiAjout() != null && utiAjout().intValue() == 1;
	}

	public boolean isAjoutDisabled() {
		return !isAjout();
	}

	public boolean isSupp() {
		return utiSupp() != null && utiSupp().intValue() == 1;
	}

	public boolean isSuppDisabled() {
		return !isSupp();
	}

	public boolean isAdm() {
		return utiAdm() != null && utiAdm().intValue() == 1;
	}

	public boolean isAdmDisabled() {
		return !isAdm();
	}

	public boolean isBu() {
		return utiBu() != null && utiBu().intValue() == 1;
	}

	public boolean isBuDisabled() {
		return !isBu();
	}

	public boolean isIne() {
		return utiIne() != null && utiIne().intValue() == 1;
	}

	public boolean isIneDisabled() {
		return !isIne();
	}

	public boolean isIdentite() {
		return utiIdentite() != null && utiIdentite().intValue() == 1;
	}

	public boolean isIdentiteDisabled() {
		return !isIdentite();
	}

	public boolean isPhoto() {
		return utiPhoto() != null && utiPhoto().intValue() == 1;
	}

	public boolean isPhotoDisabled() {
		return !isPhoto();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
