/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORne.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EORne extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_Rne";
	public static final String ENTITY_TABLE_NAME = "GRHUM.RNE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "cRne";

	public static final ERXKey<String> ADRESSE = new ERXKey<String>("adresse");
	public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
	public static final ERXKey<String> C_RNE = new ERXKey<String>("cRne");
	public static final ERXKey<String> C_RNE_PERE = new ERXKey<String>("cRnePere");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
	public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> ETAB_ENQUETE = new ERXKey<String>("etabEnquete");
	public static final ERXKey<String> ETAB_STATUT = new ERXKey<String>("etabStatut");
	public static final ERXKey<String> LC_RNE = new ERXKey<String>("lcRne");
	public static final ERXKey<String> LL_RNE = new ERXKey<String>("llRne");
	public static final ERXKey<String> TETAB_CODE = new ERXKey<String>("tetabCode");
	public static final ERXKey<String> VILLE = new ERXKey<String>("ville");

	public static final String ADRESSE_KEY = "adresse";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_RNE_PERE_KEY = "cRnePere";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETAB_ENQUETE_KEY = "etabEnquete";
	public static final String ETAB_STATUT_KEY = "etabStatut";
	public static final String LC_RNE_KEY = "lcRne";
	public static final String LL_RNE_KEY = "llRne";
	public static final String TETAB_CODE_KEY = "tetabCode";
	public static final String VILLE_KEY = "ville";

	// Non visible attributes
	public static final String ACAD_CODE_KEY = "acadCode";
	public static final String ADR_ORDRE_KEY = "adrOrdre";

	// Colkeys
	public static final String ADRESSE_COLKEY = "ADRESSE";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_RNE_PERE_COLKEY = "C_RNE_PERE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ETAB_ENQUETE_COLKEY = "ETAB_ENQUETE";
	public static final String ETAB_STATUT_COLKEY = "ETAB_STATUT";
	public static final String LC_RNE_COLKEY = "LC_RNE";
	public static final String LL_RNE_COLKEY = "LL_RNE";
	public static final String TETAB_CODE_COLKEY = "TETAB_CODE";
	public static final String VILLE_COLKEY = "VILLE";

	// Non visible colkeys
	public static final String ACAD_CODE_COLKEY = "ACAD_CODE";
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie> TO_ACADEMIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie>("toAcademie");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresse");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_PERE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRnePere");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNES_FILS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRnesFils");

	public static final String TO_ACADEMIE_KEY = "toAcademie";
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_RNE_PERE_KEY = "toRnePere";
	public static final String TO_RNES_FILS_KEY = "toRnesFils";

	// Create / Init methods

	/**
	 * Creates and inserts a new EORne with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dCreation
	 * @param dModification
	 * @param etabStatut
	 * @return EORne
	 */
	public static EORne create(EOEditingContext editingContext, NSTimestamp dCreation, NSTimestamp dModification, String etabStatut) {
		EORne eo = (EORne) createAndInsertInstance(editingContext);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setEtabStatut(etabStatut);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EORne.
	 *
	 * @param editingContext
	 * @return EORne
	 */
	public static EORne create(EOEditingContext editingContext) {
		EORne eo = (EORne) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EORne localInstanceIn(EOEditingContext editingContext) {
		EORne localInstance = (EORne) localInstanceOfObject(editingContext, (EORne) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EORne localInstanceIn(EOEditingContext editingContext, EORne eo) {
		EORne localInstance = (eo == null) ? null : (EORne) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String adresse() {
		return (String) storedValueForKey("adresse");
	}

	public void setAdresse(String value) {
		takeStoredValueForKey(value, "adresse");
	}
	public String codePostal() {
		return (String) storedValueForKey("codePostal");
	}

	public void setCodePostal(String value) {
		takeStoredValueForKey(value, "codePostal");
	}
	public String cRne() {
		return (String) storedValueForKey("cRne");
	}

	public void setCRne(String value) {
		takeStoredValueForKey(value, "cRne");
	}
	public String cRnePere() {
		return (String) storedValueForKey("cRnePere");
	}

	public void setCRnePere(String value) {
		takeStoredValueForKey(value, "cRnePere");
	}
	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}
	public NSTimestamp dDebVal() {
		return (NSTimestamp) storedValueForKey("dDebVal");
	}

	public void setDDebVal(NSTimestamp value) {
		takeStoredValueForKey(value, "dDebVal");
	}
	public NSTimestamp dFinVal() {
		return (NSTimestamp) storedValueForKey("dFinVal");
	}

	public void setDFinVal(NSTimestamp value) {
		takeStoredValueForKey(value, "dFinVal");
	}
	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	public String etabEnquete() {
		return (String) storedValueForKey("etabEnquete");
	}

	public void setEtabEnquete(String value) {
		takeStoredValueForKey(value, "etabEnquete");
	}
	public String etabStatut() {
		return (String) storedValueForKey("etabStatut");
	}

	public void setEtabStatut(String value) {
		takeStoredValueForKey(value, "etabStatut");
	}
	public String lcRne() {
		return (String) storedValueForKey("lcRne");
	}

	public void setLcRne(String value) {
		takeStoredValueForKey(value, "lcRne");
	}
	public String llRne() {
		return (String) storedValueForKey("llRne");
	}

	public void setLlRne(String value) {
		takeStoredValueForKey(value, "llRne");
	}
	public String tetabCode() {
		return (String) storedValueForKey("tetabCode");
	}

	public void setTetabCode(String value) {
		takeStoredValueForKey(value, "tetabCode");
	}
	public String ville() {
		return (String) storedValueForKey("ville");
	}

	public void setVille(String value) {
		takeStoredValueForKey(value, "ville");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAcademie toAcademie() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAcademie)storedValueForKey("toAcademie");
	}

	public void setToAcademieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAcademie value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAcademie oldValue = toAcademie();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAcademie");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toAcademie");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresse() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey("toAdresse");
	}

	public void setToAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toAdresse");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toAdresse");
		}
	}
  
	public NSArray toRnePere() {
		return (NSArray)storedValueForKey("toRnePere");
	}

	public NSArray toRnePere(EOQualifier qualifier) {
		return toRnePere(qualifier, null, false);
	}

	public NSArray toRnePere(EOQualifier qualifier, boolean fetch) {
		return toRnePere(qualifier, null, fetch);
	}

	public NSArray toRnePere(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EORne.TO_RNES_FILS_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EORne.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toRnePere();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToRnePereRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toRnePere");
	}

	public void removeFromToRnePereRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toRnePere");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EORne createToRnePereRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_Rne");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toRnePere");
		return (org.cocktail.scolarix.serveur.metier.eos.EORne) eo;
	}

	public void deleteToRnePereRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toRnePere");
				editingContext().deleteObject(object);
			}

	public void deleteAllToRnePereRelationships() {
		Enumeration objects = toRnePere().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToRnePereRelationship((org.cocktail.scolarix.serveur.metier.eos.EORne)objects.nextElement());
		}
	}
	public NSArray toRnesFils() {
		return (NSArray)storedValueForKey("toRnesFils");
	}

	public NSArray toRnesFils(EOQualifier qualifier) {
		return toRnesFils(qualifier, null, false);
	}

	public NSArray toRnesFils(EOQualifier qualifier, boolean fetch) {
		return toRnesFils(qualifier, null, fetch);
	}

	public NSArray toRnesFils(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EORne.TO_RNE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EORne.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toRnesFils();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToRnesFilsRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toRnesFils");
	}

	public void removeFromToRnesFilsRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toRnesFils");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EORne createToRnesFilsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_Rne");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toRnesFils");
		return (org.cocktail.scolarix.serveur.metier.eos.EORne) eo;
	}

	public void deleteToRnesFilsRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toRnesFils");
				editingContext().deleteObject(object);
			}

	public void deleteAllToRnesFilsRelationships() {
		Enumeration objects = toRnesFils().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToRnesFilsRelationship((org.cocktail.scolarix.serveur.metier.eos.EORne)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EORne.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EORne.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EORne)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EORne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EORne eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EORne)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EORne fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EORne eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EORne)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORne fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EORne eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EORne ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchFetchAll(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", "FwkScolarix_Rne");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EORne createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EORne.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EORne.ENTITY_NAME + "' !");
		}
		else {
			EORne object = (EORne) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EORne localInstanceOfObject(EOEditingContext ec, EORne object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EORne " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EORne) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
