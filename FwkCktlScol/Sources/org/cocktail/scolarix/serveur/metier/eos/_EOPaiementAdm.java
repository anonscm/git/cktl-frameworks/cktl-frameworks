/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPaiementAdm.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPaiementAdm extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PaiementAdm";
	public static final String ENTITY_TABLE_NAME = "ADMISSION.PAIEMENT_ADM";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "padmNumero";

	public static final ERXKey<Integer> CAND_NUMERO = new ERXKey<Integer>("candNumero");
	public static final ERXKey<Integer> PADM_ACCOMPTE = new ERXKey<Integer>("padmAccompte");
	public static final ERXKey<Integer> PADM_ANNEE_SCOL = new ERXKey<Integer>("padmAnneeScol");
	public static final ERXKey<NSTimestamp> PADM_DATE_PAIEMENT = new ERXKey<NSTimestamp>("padmDatePaiement");
	public static final ERXKey<String> PADM_NOM_TIREUR = new ERXKey<String>("padmNomTireur");
	public static final ERXKey<Integer> PADM_NUMERO = new ERXKey<Integer>("padmNumero");
	public static final ERXKey<String> PADM_NUMERO_CHEQUE = new ERXKey<String>("padmNumeroCheque");
	public static final ERXKey<String> PADM_NUMERO_COMPTE = new ERXKey<String>("padmNumeroCompte");
	public static final ERXKey<String> PADM_PRENOM_TIREUR = new ERXKey<String>("padmPrenomTireur");
	public static final ERXKey<java.math.BigDecimal> PADM_SOMME = new ERXKey<java.math.BigDecimal>("padmSomme");

	public static final String CAND_NUMERO_KEY = "candNumero";
	public static final String PADM_ACCOMPTE_KEY = "padmAccompte";
	public static final String PADM_ANNEE_SCOL_KEY = "padmAnneeScol";
	public static final String PADM_DATE_PAIEMENT_KEY = "padmDatePaiement";
	public static final String PADM_NOM_TIREUR_KEY = "padmNomTireur";
	public static final String PADM_NUMERO_KEY = "padmNumero";
	public static final String PADM_NUMERO_CHEQUE_KEY = "padmNumeroCheque";
	public static final String PADM_NUMERO_COMPTE_KEY = "padmNumeroCompte";
	public static final String PADM_PRENOM_TIREUR_KEY = "padmPrenomTireur";
	public static final String PADM_SOMME_KEY = "padmSomme";

	// Non visible attributes
	public static final String BANQ_CODE_KEY = "banqCode";
	public static final String RCPT_CODE_KEY = "rcptCode";

	// Colkeys
	public static final String CAND_NUMERO_COLKEY = "CAND_NUMERO";
	public static final String PADM_ACCOMPTE_COLKEY = "PADM_ACCOMPTE";
	public static final String PADM_ANNEE_SCOL_COLKEY = "PADM_ANNEE_SCOL";
	public static final String PADM_DATE_PAIEMENT_COLKEY = "PADM_DATE_PAIEMENT";
	public static final String PADM_NOM_TIREUR_COLKEY = "PADM_NOM_TIREUR";
	public static final String PADM_NUMERO_COLKEY = "PADM_NUMERO";
	public static final String PADM_NUMERO_CHEQUE_COLKEY = "PADM_NUMERO_CHEQUE";
	public static final String PADM_NUMERO_COMPTE_COLKEY = "PADM_NUMERO_COMPTE";
	public static final String PADM_PRENOM_TIREUR_COLKEY = "PADM_PRENOM_TIREUR";
	public static final String PADM_SOMME_COLKEY = "PADM_SOMME";

	// Non visible colkeys
	public static final String BANQ_CODE_COLKEY = "BANQ_CODE";
	public static final String RCPT_CODE_COLKEY = "RCPT_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBanque> TO_BANQUE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBanque>("toBanque");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte> TO_REPARTITION_COMPTE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte>("toRepartitionCompte");

	public static final String TO_BANQUE_KEY = "toBanque";
	public static final String TO_REPARTITION_COMPTE_KEY = "toRepartitionCompte";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPaiementAdm with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param candNumero
	 * @param padmAccompte
	 * @param padmAnneeScol
	 * @param padmDatePaiement
	 * @param padmNumero
	 * @param padmSomme
	 * @param toRepartitionCompte
	 * @return EOPaiementAdm
	 */
	public static EOPaiementAdm create(EOEditingContext editingContext, Integer candNumero, Integer padmAccompte, Integer padmAnneeScol, NSTimestamp padmDatePaiement, Integer padmNumero, java.math.BigDecimal padmSomme, org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte) {
		EOPaiementAdm eo = (EOPaiementAdm) createAndInsertInstance(editingContext);
		eo.setCandNumero(candNumero);
		eo.setPadmAccompte(padmAccompte);
		eo.setPadmAnneeScol(padmAnneeScol);
		eo.setPadmDatePaiement(padmDatePaiement);
		eo.setPadmNumero(padmNumero);
		eo.setPadmSomme(padmSomme);
		eo.setToRepartitionCompteRelationship(toRepartitionCompte);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPaiementAdm.
	 *
	 * @param editingContext
	 * @return EOPaiementAdm
	 */
	public static EOPaiementAdm create(EOEditingContext editingContext) {
		EOPaiementAdm eo = (EOPaiementAdm) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPaiementAdm localInstanceIn(EOEditingContext editingContext) {
		EOPaiementAdm localInstance = (EOPaiementAdm) localInstanceOfObject(editingContext, (EOPaiementAdm) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPaiementAdm localInstanceIn(EOEditingContext editingContext, EOPaiementAdm eo) {
		EOPaiementAdm localInstance = (eo == null) ? null : (EOPaiementAdm) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer candNumero() {
		return (Integer) storedValueForKey("candNumero");
	}

	public void setCandNumero(Integer value) {
		takeStoredValueForKey(value, "candNumero");
	}
	public Integer padmAccompte() {
		return (Integer) storedValueForKey("padmAccompte");
	}

	public void setPadmAccompte(Integer value) {
		takeStoredValueForKey(value, "padmAccompte");
	}
	public Integer padmAnneeScol() {
		return (Integer) storedValueForKey("padmAnneeScol");
	}

	public void setPadmAnneeScol(Integer value) {
		takeStoredValueForKey(value, "padmAnneeScol");
	}
	public NSTimestamp padmDatePaiement() {
		return (NSTimestamp) storedValueForKey("padmDatePaiement");
	}

	public void setPadmDatePaiement(NSTimestamp value) {
		takeStoredValueForKey(value, "padmDatePaiement");
	}
	public String padmNomTireur() {
		return (String) storedValueForKey("padmNomTireur");
	}

	public void setPadmNomTireur(String value) {
		takeStoredValueForKey(value, "padmNomTireur");
	}
	public Integer padmNumero() {
		return (Integer) storedValueForKey("padmNumero");
	}

	public void setPadmNumero(Integer value) {
		takeStoredValueForKey(value, "padmNumero");
	}
	public String padmNumeroCheque() {
		return (String) storedValueForKey("padmNumeroCheque");
	}

	public void setPadmNumeroCheque(String value) {
		takeStoredValueForKey(value, "padmNumeroCheque");
	}
	public String padmNumeroCompte() {
		return (String) storedValueForKey("padmNumeroCompte");
	}

	public void setPadmNumeroCompte(String value) {
		takeStoredValueForKey(value, "padmNumeroCompte");
	}
	public String padmPrenomTireur() {
		return (String) storedValueForKey("padmPrenomTireur");
	}

	public void setPadmPrenomTireur(String value) {
		takeStoredValueForKey(value, "padmPrenomTireur");
	}
	public java.math.BigDecimal padmSomme() {
		return (java.math.BigDecimal) storedValueForKey("padmSomme");
	}

	public void setPadmSomme(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "padmSomme");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOBanque toBanque() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOBanque)storedValueForKey("toBanque");
	}

	public void setToBanqueRelationship(org.cocktail.scolarix.serveur.metier.eos.EOBanque value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOBanque oldValue = toBanque();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBanque");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toBanque");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte toRepartitionCompte() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte)storedValueForKey("toRepartitionCompte");
	}

	public void setToRepartitionCompteRelationship(org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORepartitionCompte oldValue = toRepartitionCompte();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRepartitionCompte");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRepartitionCompte");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPaiementAdm.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPaiementAdm.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPaiementAdm)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPaiementAdm fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPaiementAdm fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPaiementAdm eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPaiementAdm)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPaiementAdm fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPaiementAdm fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPaiementAdm fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPaiementAdm eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPaiementAdm)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPaiementAdm fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPaiementAdm fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPaiementAdm eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPaiementAdm ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPaiementAdm createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPaiementAdm.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPaiementAdm.ENTITY_NAME + "' !");
		}
		else {
			EOPaiementAdm object = (EOPaiementAdm) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPaiementAdm localInstanceOfObject(EOEditingContext ec, EOPaiementAdm object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPaiementAdm " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPaiementAdm) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
