/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionTitre;
import org.cocktail.scolarix.serveur.finder.FinderScolFormationHabilitation;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXKeyValueQualifier;

public class EOInscDipl extends _EOInscDipl {

	public final static String IDIPL_NUMERO_KEY = "idiplNumero";
	public final static String SCOL_FORMATION_HABILITATION_KEY = "scolFormationHabilitation";

	private String resultatInputValue, mentionInputValue, id;

	private EOScolFormationHabilitation scolFormationHabilitation = null;

	private Boolean isTitre;

	public EOInscDipl() {
		super();
	}

	public String toString() {
		return toHistorique().histAnneeScol() + " - " + toTypeInscription().libelleInscription() + " - "
				+ toFwkScolarite_ScolFormationSpecialisation().libelleDiplomeAbrege(idiplAnneeSuivie());
	}

	public String toStringLight() {
		return toFwkScolarite_ScolFormationSpecialisation().libelleDiplomeAbrege(idiplAnneeSuivie());
	}

	public boolean hasAutresInscriptions() {
		return toHistorique() != null && toHistorique().toInscDipls() != null && toHistorique().toInscDipls().count() > 1;
	}

	public boolean isInscriptionPrincipale() {
		return toTypeInscription() != null
				&& toTypeInscription().idiplTypeInscription() != null
				&& (toTypeInscription().idiplTypeInscription().equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_PRINCIPALE) || toTypeInscription()
						.idiplTypeInscription().equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_ECHANGE_INTERNATIONAL));
	}

	public EOScolFormationTitre getFormationTitre() {
		ERXKeyValueQualifier q1 = ERXQ.equals(EOScolFormationTitre.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY,
				toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome());
		ERXKeyValueQualifier q2 = ERXQ.equals(EOScolFormationTitre.FHAB_NIVEAU_KEY, idiplAnneeSuivie());
		return EOScolFormationTitre.fetchByQualifier(editingContext(), ERXQ.and(q1, q2));
	}

	public Boolean isTitre() {
		if (isTitre == null) {
			isTitre = getInscriptionTitre() != null;
		}
		return isTitre;
	}

	public void setIsTitre(Boolean isTitre) {
		this.isTitre = isTitre;
		if (isTitre != null) {
			if (isTitre) {
				if (getInscriptionTitre() == null) {
					EOScolInscriptionTitre titre = EOScolInscriptionTitre.create(editingContext());
					titre.setIdiplAnneeSuivie(idiplAnneeSuivie());
					titre.setIftitEtat(new Integer(2));
					titre.setIftitMoyenne(null);
					titre.setToFwkScolarite_ScolConstanteMentionRelationship(EOScolConstanteMention.fetchByKeyValue(editingContext(),
							EOScolConstanteMention.CMEN_KEY_KEY, new Integer(0)));
					titre.setToFwkScolarite_ScolFormationAnneeRelationship(EOScolFormationAnnee.fetchByKeyValue(editingContext(),
							EOScolFormationAnnee.FANN_DEBUT_KEY, toHistorique().histAnneeScol()));
					titre.setFannKey(titre.toFwkScolarite_ScolFormationAnnee().fannKey());
					titre.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation());
					titre.setToFwkScolarix_ResultatRelationship(EOResultat.fetchByKeyValue(editingContext(), EOResultat.RES_CODE_KEY, "C"));
					titre.setToFwkScolarite_ScolFormationTitreRelationship(getFormationTitre());
					titre.setFtitKey(new Integer(titre.toFwkScolarite_ScolFormationTitre().primaryKey()));
					titre.setToFwkScolarix_EtudiantRelationship(toHistorique().toEtudiant());
					titre.setToFwkScolarix_InscDiplRelationship(this);
					titre.setIdiplNumero(titre.toFwkScolarix_InscDipl().idiplNumero());
				}
			}
			else {
				if (getInscriptionTitre() != null) {
					editingContext().deleteObject(getInscriptionTitre());
				}
			}
		}
	}

	public EOScolInscriptionTitre getInscriptionTitre() {
		return EOScolInscriptionTitre.fetchByKeyValue(editingContext(), EOScolInscriptionTitre.TO_FWK_SCOLARIX__INSC_DIPL_KEY, this);
	}

	public String stringAutresInscriptions() {
		if (hasAutresInscriptions()) {
			String s = "";
			for (EOInscDipl inscDipl : (NSArray<EOInscDipl>) toHistorique().toInscDipls()) {
				if (inscDipl != this) {
					s += inscDipl.toStringLight() + " - " + inscDipl.toTypeInscription().libelleInscription() + " --- ";
				}
			}
			return s;
		}
		return null;
	}

	public String resultatInputValue() {
		if (resultatInputValue == null && toResultat() != null) {
			return toResultat().toString();
		}
		return resultatInputValue;
	}

	public void setResultatInputValue(String resultatInputValue) {
		this.resultatInputValue = resultatInputValue;
	}

	public String mentionInputValue() {
		if (mentionInputValue == null && toMention() != null) {
			return toMention().toString();
		}
		return mentionInputValue;
	}

	public void setMentionInputValue(String mentionInputValue) {
		this.mentionInputValue = mentionInputValue;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele a partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Retourne le libelle abrege du diplome.
	 * 
	 * @return String
	 */
	public String libelleDiplomeAbrege() {
		return toFwkScolarite_ScolFormationSpecialisation().libelleDiplomeAbrege(idiplAnneeSuivie());
	}

	/**
	 * Retourne le libelle complet du diplome.
	 * 
	 * @return String
	 */
	public String libelleDiplomeComplet() {
		return toFwkScolarite_ScolFormationSpecialisation().libelleDiplomeComplet(idiplAnneeSuivie());
	}

	public Boolean isPassageConditionnel() {
		return Boolean.valueOf(StringCtrl.toBool(idiplPassageConditionnel()));
	}

	public void setPassageConditionnel(Boolean passageConditionnel) {
		setIdiplPassageConditionnel(passageConditionnel.booleanValue() ? "O" : "N");
	}

	public Boolean isDiplomable() {
		if (idiplDiplomable() == null || idiplDiplomable().intValue() == 1) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public void setDiplomable(Boolean diplomable) {
		setIdiplDiplomable(diplomable.booleanValue() ? new Integer(1) : new Integer(0));
	}

	public String diplomableON() {
		return (isDiplomable().booleanValue() ? "O" : "N");
	}

	public Boolean isAmenagement() {
		if (idiplAmenagement() != null && idiplAmenagement().intValue() == 1) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public void setAmenagement(Boolean amenagement) {
		setIdiplDiplomable(amenagement.booleanValue() ? new Integer(1) : new Integer(0));
	}

	public void setIsSelectionnable(Boolean isSelectionnable) {
		EOScolFormationHabilitation scolFormationHabilitation = scolFormationHabilitation();
		if (scolFormationHabilitation != null) {
			scolFormationHabilitation.setIsSelectionnable(isSelectionnable);
		}
	}

	public Boolean isSelectionnable() {
		EOScolFormationHabilitation scolFormationHabilitation = scolFormationHabilitation();
		return ((scolFormationHabilitation != null) && (scolFormationHabilitation.isSelectionnable() != null && scolFormationHabilitation
				.isSelectionnable().booleanValue()));
	}

	/**
	 * Retourne l'inscription sous forme d'un EOScolFormationHabilitation.
	 * 
	 * @return EOScolFormationHabilitation
	 */
	public EOScolFormationHabilitation scolFormationHabilitation() {
		if (scolFormationHabilitation == null) {
			if ((toHistorique() == null) || (toHistorique().histAnneeScol() == null)) {
				return null;
			}
			if ((toFwkScolarite_ScolFormationSpecialisation() == null) || (toFwkScolarite_ScolFormationSpecialisation().fspnKey() == null)) {
				return null;
			}
			if (idiplAnneeSuivie() == null) {
				return null;
			}
			scolFormationHabilitation = FinderScolFormationHabilitation.getScolFormationHabilitation(editingContext(),
					toHistorique().histAnneeScol(), toFwkScolarite_ScolFormationSpecialisation(), idiplAnneeSuivie());
		}
		return scolFormationHabilitation;
	}

	public void setScolFormationHabilitation(EOScolFormationHabilitation scolFormationHabilitation) {
		this.scolFormationHabilitation = scolFormationHabilitation;
	}

	public String id() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
