/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGarnucheAppliDocumentation.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOGarnucheAppliDocumentation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_GarnucheAppliDocumentation";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.GARNUCHE_APPLI_DOCUMENTATION";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "adocKey";

	public static final ERXKey<String> ADOC_LIEN = new ERXKey<String>("adocLien");
	public static final ERXKey<String> ADOC_TEXTE = new ERXKey<String>("adocTexte");
	public static final ERXKey<String> ADOC_TYPE = new ERXKey<String>("adocType");
	public static final ERXKey<Integer> APPL_KEY = new ERXKey<Integer>("applKey");

	public static final String ADOC_LIEN_KEY = "adocLien";
	public static final String ADOC_TEXTE_KEY = "adocTexte";
	public static final String ADOC_TYPE_KEY = "adocType";
	public static final String APPL_KEY_KEY = "applKey";

	// Non visible attributes
	public static final String ADOC_KEY_KEY = "adocKey";

	// Colkeys
	public static final String ADOC_LIEN_COLKEY = "ADOC_LIEN";
	public static final String ADOC_TEXTE_COLKEY = "ADOC_TEXTE";
	public static final String ADOC_TYPE_COLKEY = "ADOC_TYPE";
	public static final String APPL_KEY_COLKEY = "APPL_KEY";

	// Non visible colkeys
	public static final String ADOC_KEY_COLKEY = "ADOC_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication> TO_GARNUCHE_APPLICATION = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication>("toGarnucheApplication");

	public static final String TO_GARNUCHE_APPLICATION_KEY = "toGarnucheApplication";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOGarnucheAppliDocumentation with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param adocType
	 * @param applKey
	 * @param toGarnucheApplication
	 * @return EOGarnucheAppliDocumentation
	 */
	public static EOGarnucheAppliDocumentation create(EOEditingContext editingContext, String adocType, Integer applKey, org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication toGarnucheApplication) {
		EOGarnucheAppliDocumentation eo = (EOGarnucheAppliDocumentation) createAndInsertInstance(editingContext);
		eo.setAdocType(adocType);
		eo.setApplKey(applKey);
		eo.setToGarnucheApplicationRelationship(toGarnucheApplication);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOGarnucheAppliDocumentation.
	 *
	 * @param editingContext
	 * @return EOGarnucheAppliDocumentation
	 */
	public static EOGarnucheAppliDocumentation create(EOEditingContext editingContext) {
		EOGarnucheAppliDocumentation eo = (EOGarnucheAppliDocumentation) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOGarnucheAppliDocumentation localInstanceIn(EOEditingContext editingContext) {
		EOGarnucheAppliDocumentation localInstance = (EOGarnucheAppliDocumentation) localInstanceOfObject(editingContext, (EOGarnucheAppliDocumentation) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOGarnucheAppliDocumentation localInstanceIn(EOEditingContext editingContext, EOGarnucheAppliDocumentation eo) {
		EOGarnucheAppliDocumentation localInstance = (eo == null) ? null : (EOGarnucheAppliDocumentation) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String adocLien() {
		return (String) storedValueForKey("adocLien");
	}

	public void setAdocLien(String value) {
		takeStoredValueForKey(value, "adocLien");
	}
	public String adocTexte() {
		return (String) storedValueForKey("adocTexte");
	}

	public void setAdocTexte(String value) {
		takeStoredValueForKey(value, "adocTexte");
	}
	public String adocType() {
		return (String) storedValueForKey("adocType");
	}

	public void setAdocType(String value) {
		takeStoredValueForKey(value, "adocType");
	}
	public Integer applKey() {
		return (Integer) storedValueForKey("applKey");
	}

	public void setApplKey(Integer value) {
		takeStoredValueForKey(value, "applKey");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication toGarnucheApplication() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication)storedValueForKey("toGarnucheApplication");
	}

	public void setToGarnucheApplicationRelationship(org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOGarnucheApplication oldValue = toGarnucheApplication();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toGarnucheApplication");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toGarnucheApplication");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOGarnucheAppliDocumentation.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOGarnucheAppliDocumentation.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOGarnucheAppliDocumentation)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOGarnucheAppliDocumentation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGarnucheAppliDocumentation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOGarnucheAppliDocumentation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOGarnucheAppliDocumentation)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOGarnucheAppliDocumentation fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheAppliDocumentation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOGarnucheAppliDocumentation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOGarnucheAppliDocumentation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOGarnucheAppliDocumentation)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheAppliDocumentation fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOGarnucheAppliDocumentation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOGarnucheAppliDocumentation eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOGarnucheAppliDocumentation ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOGarnucheAppliDocumentation createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOGarnucheAppliDocumentation.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOGarnucheAppliDocumentation.ENTITY_NAME + "' !");
		}
		else {
			EOGarnucheAppliDocumentation object = (EOGarnucheAppliDocumentation) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOGarnucheAppliDocumentation localInstanceOfObject(EOEditingContext ec, EOGarnucheAppliDocumentation object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOGarnucheAppliDocumentation " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOGarnucheAppliDocumentation) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
