/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORdvPlanningInfo.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EORdvPlanningInfo extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_RdvPlanningInfo";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.RDV_PLANNING_INFO";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "numOccPlanning";

	public static final ERXKey<String> COD_CGE = new ERXKey<String>("codCge");
	public static final ERXKey<String> COD_ETB = new ERXKey<String>("codEtb");
	public static final ERXKey<String> COD_UFR = new ERXKey<String>("codUfr");
	public static final ERXKey<String> LIB_PLANNING = new ERXKey<String>("libPlanning");
	public static final ERXKey<String> LIEU1_CONVOC = new ERXKey<String>("lieu1Convoc");
	public static final ERXKey<String> LIEU2_CONVOC = new ERXKey<String>("lieu2Convoc");
	public static final ERXKey<String> NOM_CGE = new ERXKey<String>("nomCge");
	public static final ERXKey<String> NOM_UFR = new ERXKey<String>("nomUfr");
	public static final ERXKey<Integer> NUM_OCC_PLANNING = new ERXKey<Integer>("numOccPlanning");
	public static final ERXKey<String> TELE_CONVOC = new ERXKey<String>("teleConvoc");

	public static final String COD_CGE_KEY = "codCge";
	public static final String COD_ETB_KEY = "codEtb";
	public static final String COD_UFR_KEY = "codUfr";
	public static final String LIB_PLANNING_KEY = "libPlanning";
	public static final String LIEU1_CONVOC_KEY = "lieu1Convoc";
	public static final String LIEU2_CONVOC_KEY = "lieu2Convoc";
	public static final String NOM_CGE_KEY = "nomCge";
	public static final String NOM_UFR_KEY = "nomUfr";
	public static final String NUM_OCC_PLANNING_KEY = "numOccPlanning";
	public static final String TELE_CONVOC_KEY = "teleConvoc";

	// Non visible attributes

	// Colkeys
	public static final String COD_CGE_COLKEY = "COD_CGE";
	public static final String COD_ETB_COLKEY = "COD_ETB";
	public static final String COD_UFR_COLKEY = "COD_UFR";
	public static final String LIB_PLANNING_COLKEY = "LIB_PLANNING";
	public static final String LIEU1_CONVOC_COLKEY = "LIEU1_CONVOC";
	public static final String LIEU2_CONVOC_COLKEY = "LIEU2_CONVOC";
	public static final String NOM_CGE_COLKEY = "NOM_CGE";
	public static final String NOM_UFR_COLKEY = "NOM_UFR";
	public static final String NUM_OCC_PLANNING_COLKEY = "NUM_OCC_PLANNING";
	public static final String TELE_CONVOC_COLKEY = "TELE_CONVOC";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningMulti> TO_RDV_PLANNING_MULTIS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningMulti>("toRdvPlanningMultis");

	public static final String TO_RDV_PLANNING_MULTIS_KEY = "toRdvPlanningMultis";

	// Create / Init methods

	/**
	 * Creates and inserts a new EORdvPlanningInfo with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param numOccPlanning
	 * @return EORdvPlanningInfo
	 */
	public static EORdvPlanningInfo create(EOEditingContext editingContext, Integer numOccPlanning) {
		EORdvPlanningInfo eo = (EORdvPlanningInfo) createAndInsertInstance(editingContext);
		eo.setNumOccPlanning(numOccPlanning);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EORdvPlanningInfo.
	 *
	 * @param editingContext
	 * @return EORdvPlanningInfo
	 */
	public static EORdvPlanningInfo create(EOEditingContext editingContext) {
		EORdvPlanningInfo eo = (EORdvPlanningInfo) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EORdvPlanningInfo localInstanceIn(EOEditingContext editingContext) {
		EORdvPlanningInfo localInstance = (EORdvPlanningInfo) localInstanceOfObject(editingContext, (EORdvPlanningInfo) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EORdvPlanningInfo localInstanceIn(EOEditingContext editingContext, EORdvPlanningInfo eo) {
		EORdvPlanningInfo localInstance = (eo == null) ? null : (EORdvPlanningInfo) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String codCge() {
		return (String) storedValueForKey("codCge");
	}

	public void setCodCge(String value) {
		takeStoredValueForKey(value, "codCge");
	}
	public String codEtb() {
		return (String) storedValueForKey("codEtb");
	}

	public void setCodEtb(String value) {
		takeStoredValueForKey(value, "codEtb");
	}
	public String codUfr() {
		return (String) storedValueForKey("codUfr");
	}

	public void setCodUfr(String value) {
		takeStoredValueForKey(value, "codUfr");
	}
	public String libPlanning() {
		return (String) storedValueForKey("libPlanning");
	}

	public void setLibPlanning(String value) {
		takeStoredValueForKey(value, "libPlanning");
	}
	public String lieu1Convoc() {
		return (String) storedValueForKey("lieu1Convoc");
	}

	public void setLieu1Convoc(String value) {
		takeStoredValueForKey(value, "lieu1Convoc");
	}
	public String lieu2Convoc() {
		return (String) storedValueForKey("lieu2Convoc");
	}

	public void setLieu2Convoc(String value) {
		takeStoredValueForKey(value, "lieu2Convoc");
	}
	public String nomCge() {
		return (String) storedValueForKey("nomCge");
	}

	public void setNomCge(String value) {
		takeStoredValueForKey(value, "nomCge");
	}
	public String nomUfr() {
		return (String) storedValueForKey("nomUfr");
	}

	public void setNomUfr(String value) {
		takeStoredValueForKey(value, "nomUfr");
	}
	public Integer numOccPlanning() {
		return (Integer) storedValueForKey("numOccPlanning");
	}

	public void setNumOccPlanning(Integer value) {
		takeStoredValueForKey(value, "numOccPlanning");
	}
	public String teleConvoc() {
		return (String) storedValueForKey("teleConvoc");
	}

	public void setTeleConvoc(String value) {
		takeStoredValueForKey(value, "teleConvoc");
	}

	public NSArray toRdvPlanningMultis() {
		return (NSArray)storedValueForKey("toRdvPlanningMultis");
	}

	public NSArray toRdvPlanningMultis(EOQualifier qualifier) {
		return toRdvPlanningMultis(qualifier, null);
	}

	public NSArray toRdvPlanningMultis(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toRdvPlanningMultis();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToRdvPlanningMultisRelationship(org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningMulti object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toRdvPlanningMultis");
	}

	public void removeFromToRdvPlanningMultisRelationship(org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningMulti object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toRdvPlanningMultis");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningMulti createToRdvPlanningMultisRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_RdvPlanningMulti");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toRdvPlanningMultis");
		return (org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningMulti) eo;
	}

	public void deleteToRdvPlanningMultisRelationship(org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningMulti object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toRdvPlanningMultis");
				editingContext().deleteObject(object);
			}

	public void deleteAllToRdvPlanningMultisRelationships() {
		Enumeration objects = toRdvPlanningMultis().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToRdvPlanningMultisRelationship((org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningMulti)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EORdvPlanningInfo.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EORdvPlanningInfo.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EORdvPlanningInfo)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EORdvPlanningInfo fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORdvPlanningInfo fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EORdvPlanningInfo eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EORdvPlanningInfo)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EORdvPlanningInfo fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORdvPlanningInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORdvPlanningInfo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EORdvPlanningInfo eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EORdvPlanningInfo)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORdvPlanningInfo fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORdvPlanningInfo fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EORdvPlanningInfo eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EORdvPlanningInfo ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EORdvPlanningInfo createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EORdvPlanningInfo.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EORdvPlanningInfo.ENTITY_NAME + "' !");
		}
		else {
			EORdvPlanningInfo object = (EORdvPlanningInfo) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EORdvPlanningInfo localInstanceOfObject(EOEditingContext ec, EORdvPlanningInfo object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EORdvPlanningInfo " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EORdvPlanningInfo) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
