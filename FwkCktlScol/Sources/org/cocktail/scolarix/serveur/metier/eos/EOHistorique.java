/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolarix.serveur.finder.FinderParametre;
import org.cocktail.scolarix.serveur.finder.FinderRdvEtape;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.qualifiers.ERXOrQualifier;

public class EOHistorique extends _EOHistorique {

	public static final String SALARIE_NON = "N";
	public static final String SALARIE_FONCTIONNAIRE = "O";
	public static final String SALARIE_AUTRE = "A";

	public static final Integer NON_BOURSIER = Integer.valueOf(0);
	public static final Integer BOURSIER = Integer.valueOf(1);
	public static final Integer EXONERE = Integer.valueOf(4);
	public static final Integer BOURSIER_DEMANDE_EN_COURS = Integer.valueOf(-1);;

	public static final Integer HIST_REDOUBLE_RIEN = null;
	public static final Integer HIST_REDOUBLE_REDOUBLEMENT = Integer.valueOf(1);
	public static final Integer HIST_REDOUBLE_REORIENTATION = Integer.valueOf(2);
	
	private NSMutableArray<EODroitsUniversite> droitsCalcules = null;

	public EOHistorique() {
		super();
	}

	public NSArray<EODetailPaiement> detailPaiements(boolean avecRemboursements) {
		NSMutableArray<EODetailPaiement> res = new NSMutableArray<EODetailPaiement>();
		if (toPaiements() != null) {
			Enumeration<EOPaiement> e = toPaiements().objectEnumerator();
			while (e.hasMoreElements()) {
				res.addObjectsFromArray(e.nextElement().toDetailPaiements());
			}
		}
		if (avecRemboursements && toEtudiantRembourses() != null) {
			Enumeration<EOEtudiantRembourse> e = toEtudiantRembourses().objectEnumerator();
			while (e.hasMoreElements()) {
				res.addObjectsFromArray(e.nextElement().toDetailPaiements());
			}
		}
		ERXSortOrdering so1 = ERXS.asc(EODetailPaiement.TO_REPARTITION_COMPTE_KEY + "." + EORepartitionCompte.TO_PLANCO_KEY + "."
				+ EOPlanco.PCO_LIB_KEY);
		ERXSortOrdering so2 = ERXS.desc(EODetailPaiement.DPAIE_MONTANT_KEY);
		ERXS.sort(res, so1, so2);
		return res;
	}

	public EOPaiementEcheancier getEcheancierEnCours() {
		NSArray<EOPaiement> paiements = toPaiements(ERXQ
				.equals(EOPaiement.TO_REPARTITION_COMPTE_KEY + "." + EORepartitionCompte.RCPT_CODE_KEY, "ECH"));
		if (paiements != null) {
			for (EOPaiement p : paiements) {
				for (EOPaiementEcheancier echeancier : (NSArray<EOPaiementEcheancier>) p.toPaiementEcheanciers()) {
					for (EOPaiementEcheance echeance : (NSArray<EOPaiementEcheance>) echeancier.toPaiementEcheances()) {
						if (echeance.pecheDate().after(DateCtrl.now())) {
							return echeancier;
						}
					}
				}
			}
		}
		return null;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appel̩e.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public void setHistBourse(Integer value) {
		super.setHistBourse(value);
		droitsCalcules = null;
	}

	/**
	 * @return Un NSArray de EOInscDipl des inscriptions valides du gazier, triées par ordre croissant de type inscription (l'inscription
	 *         principale en premier)
	 */
	public NSArray<EOInscDipl> inscriptionsValides() {
		ERXOrQualifier qual = ERXQ.isNull(EOInscDipl.TO_RESULTAT_KEY).or(
				ERXQ.notIn(EOInscDipl.TO_RESULTAT_KEY + "." + EOResultat.RES_CODE_KEY, new NSArray<String>(EOResultat.RES_CODE_INVALIDES)));
		return toInscDipls(qual,
				ERXS.ascs(EOInscDipl.TO_TYPE_INSCRIPTION_KEY + "." + EOTypeInscription.IDIPL_TYPE_INSCRIPTION_KEY, EOInscDipl.IDIPL_DATE_INSC_KEY),
				false);
	}

	/**
	 * @return Un NSArray de EOInscDipl des inscriptions du gazier, triées par ordre croissant de type inscription (l'inscription principale
	 *         en premier)
	 */
	public NSArray<EOInscDipl> inscriptions() {
		return toInscDipls(null,
				ERXS.ascs(EOInscDipl.TO_TYPE_INSCRIPTION_KEY + "." + EOTypeInscription.IDIPL_TYPE_INSCRIPTION_KEY, EOInscDipl.IDIPL_DATE_INSC_KEY),
				false);
	}

	/**
	 * Retourne l'inscription principale si trouvee parmi les inscriptions passees en parametre , NULL si aucune trouvee.
	 * 
	 * @return un EOInscDipl de l'inscription principale, NULL si aucune inscription principale.
	 */
	private EOInscDipl inscriptionPrincipale(NSArray<EOInscDipl> inscDipls) {
		EOInscDipl inscDipl = null;

		if (inscDipls != null && inscDipls.count() > 0) {
			for (int i = 0; i < inscDipls.count(); i++) {
				inscDipl = inscDipls.objectAtIndex(i);
				if (inscDipl.isInscriptionPrincipale()) {
					return inscDipl;
				}
			}
			// S'il n'y a pas d'inscription de type 0 (principale) ou 4 (échange), on prend alors la première (voir
			// classement)
			inscDipl = inscDipls.objectAtIndex(0);
		}
		return inscDipl;
	}

	/**
	 * Retourne l'inscription principale si trouvee parmi les inscriptions , NULL si aucune trouvee.
	 * 
	 * @return un EOInscDipl de l'inscription principale, NULL si aucune inscription principale.
	 */
	public EOInscDipl inscriptionPrincipale() {
		EOInscDipl inscDipl = inscriptionPrincipale(inscriptionsValides());

		if (inscDipl == null) {
			// Si aucune inscription valide, on cherche l'inscription de plus bas type d'inscription
			NSArray<EOInscDipl> inscDipls = toInscDipls(
					null,
					ERXS.asc(EOInscDipl.TO_TYPE_INSCRIPTION_KEY + "." + EOTypeInscription.IDIPL_TYPE_INSCRIPTION_KEY).then(
							ERXS.desc(EOInscDipl.IDIPL_DATE_INSC_KEY)), false);
			inscDipl = inscriptionPrincipale(inscDipls);
		}
		return inscDipl;
	}

	public Date getDateDerniereInscription() {
		Date dateInsc = null;
		NSArray<EOInscDipl> inscs = ERXS.sorted(inscriptionsValides(), ERXS.descs(EOInscDipl.IDIPL_DATE_INSC_KEY));
		if (inscs != null && inscs.count() > 0) {
			dateInsc = inscs.objectAtIndex(0).idiplDateInsc();
		}
		return dateInsc;
	}

	public BigDecimal getCotisationSecuMontant() {
		NSArray<EOVDpaieCarteRestant> a = EOVDpaieCarteRestant.fetchAll(editingContext(), ERXQ.equals(EOVDpaieCarteRestant.CODE_COMPTE_KEY, "SEC")
				.and(ERXQ.equals(EOVDpaieCarteRestant.NUMERO_HISTORIQUE_KEY, histNumero())));
		if (a == null || a.count() == 0) {
			return new BigDecimal(0);
		}
		return a.objectAtIndex(0).detailMontant();
	}

	public boolean isCumulatif() {
		for (EOInscDipl inscDipl : inscriptionsValides()) {
			if (inscDipl.toTypeInscription().idiplTypeInscription().equals(EOTypeInscription.IDIPL_TYPE_INSCRIPTION_CUMULATIF)) {
				return true;
			}
		}
		return false;
	}

	public Boolean isRedoublement() {
		if (histRedouble() != null && histRedouble().intValue() == 1) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public void setRedoublement(Boolean redoublement) {
		setHistRedouble(redoublement.booleanValue() ? new Integer(1) : null);
	}

	public void setHistAffss(Integer value) {
		super.setHistAffss(value);
		if (value == null || value.intValue() == 1) {
			setCotiCode(null);
		}
		if (value == null || value.intValue() == 0) {
			setToMutuelleOrgaRelationship(null);
			setHistAyantDroit(new Integer(0));
		}
	}

	public EOCotisation cotisation() {
		EOCotisation cotisation = null;
		String cotiCode = cotiCode();
		if (cotiCode != null) {
			NSMutableDictionary<String, String> bindings = new NSMutableDictionary<String, String>();
			bindings.setObjectForKey("O", EOCotisation.COTI_VALIDITE_KEY);
			bindings.setObjectForKey(cotiCode, EOCotisation.COTI_CODE_KEY);
			EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(bindings);
			cotisation = EOCotisation.fetchByQualifier(editingContext(), qualifier);
		}
		return cotisation;
	}

	public void setCotisation(EOCotisation cotisation) {
		if (cotisation != null) {
			setCotiCode(cotisation.cotiCode());
		}
		else {
			setCotiCode(null);
		}
	}

	public String histLibelleDerEtab() {
		return super.histLibelleDerEtab();
	}

	public void setHistLibelleDerEtab(String value) {
		super.setHistLibelleDerEtab(value);
		// EOPays pays = toFwkpers_Pays_DerEtab();
		// if (pays != null && !(pays.cPays().startsWith("100"))) {
		// setToRneDerEtabRelationship(null);
		// }
	}

	public boolean isRendezVousPossible() {
		return (rdvEtape() != null);
	}

	public EORdvEtape rdvEtape() {
		EORdvEtape rdvEtape = null;
		EOInscDipl inscriptionPrincipale = inscriptionPrincipale();
		if (inscriptionPrincipale != null && inscriptionPrincipale.toFwkScolarite_ScolFormationSpecialisation() != null
				&& inscriptionPrincipale.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome() != null) {
			rdvEtape = FinderRdvEtape.getRdvEtape(editingContext(), inscriptionPrincipale.toFwkScolarite_ScolFormationSpecialisation()
					.toFwkScolarite_ScolFormationDiplome().toFwkScolarix_VEtablissementScolarite().toRne(),
					inscriptionPrincipale.toFwkScolarite_ScolFormationSpecialisation(), inscriptionPrincipale.idiplAnneeSuivie());
		}
		return rdvEtape;
	}

	/**
	 * Renvoie la liste complete des droits universitaires
	 * 
	 * @return Un NSArray de EODroitsUniversite
	 */
	public NSArray<EODroitsUniversite> droitsUniversitairesComplets() {
		if (inscriptionsValides() != null) {
			NSMutableArray<String> types = new NSMutableArray<String>((NSArray<String>) inscriptionsValides().valueForKeyPath(
					EOInscDipl.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
							+ EOScolFormationDiplome.FDIP_TYPE_DROIT_KEY));
			types.addObjects("F", "N");
			return EODroitsUniversite.fetchAll(editingContext(),
					ERXQ.equals(EODroitsUniversite.DUNIV_ANNEE_SCOL_KEY, histAnneeScol())
							.and(ERXQ.in(EODroitsUniversite.DUNIV_TYPE_DROIT_KEY, types)), null);
		}
		return null;
	}

	/**
	 * Renvoie une proposition des droits universitaires a regler pour son ou ses inscription(s), amputee des droits deja payes (dans
	 * DetailPaiement par rapport au rcptCode).
	 * 
	 * @return Un NSArray de EODroitsUniversite
	 */
	public NSArray<EODroitsUniversite> droitsUniversitairesSpecifiques() {
		if (droitsCalcules != null) {
			return droitsCalcules;
		}

		NSArray<EOInscDipl> inscriptions = inscriptionsValides();

		if (inscriptions == null || inscriptions.count() == 0) {
			return droitsCalcules;
		}

		// 1) Recherche de l'inscription principale
		EOInscDipl inscPrincipale = inscriptionPrincipale(inscriptions);
		if (inscPrincipale == null) {
			return droitsCalcules;
		}

		// 2) Traitement de toutes les inscriptions
		for (int i = 0; i < inscriptions.count(); i++) {
			EOInscDipl inscDipl = inscriptions.objectAtIndex(i);

			if (inscDipl == inscPrincipale) {
				// 2-1) Si inscription en cours = 1) alors traitement des droits "INSCRIPTION PRINCIPALE"
				initialiserLesDroitsInscriptionPrincipale(inscDipl);
			}
			else {
				// 2-2) Si inscription en cours != 1) alors traitement des droits "INSCRIPTION COMPLEMENTAIRE"
				initialiserLesDroitsComplementaires(inscDipl);
			}
		}

		return droitsCalcules;
	}

	public void resetDroitsUniversitairesSpecifiques() {
		droitsCalcules = null;
	}

	/**
	 * Initialise la liste de droits universitaires avec ceux non encore regles pour l'inscription principale
	 * 
	 * @param inscDipl
	 *            Inscription a une formation a laquelle se rattache les droits (EOInscDipl).
	 */
	public void initialiserLesDroitsInscriptionPrincipale(EOInscDipl inscDipl) {
		NSArray<EODroitsUniversite> droitsUniversitaires = droitsUniversitairesComplets();
		String droitSpecialValue = null;
		String typeDroitDiplome = null;
		int typeInscription = 666;

		// Recherche du type d'inscription
		if (inscDipl.toTypeInscription() != null && inscDipl.toTypeInscription().idiplTypeInscription() != null) {
			typeInscription = inscDipl.toTypeInscription().idiplTypeInscription().intValue();
		}

		// Recherche du type droit du diplome
		if (inscDipl.toFwkScolarite_ScolFormationSpecialisation() != null
				&& inscDipl.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome() != null
				&& inscDipl.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipTypeDroit() != null) {
			typeDroitDiplome = inscDipl.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipTypeDroit();
		}

		// Recherche d'un droit SPECIAL
		if (FinderParametre.getPaiementSpecial(editingContext()) == true) {
			String droitSpecialCode = "GARNUCHE_PAIEMENT_" + typeInscription;
			droitSpecialValue = FinderParametre.getParam(editingContext(), droitSpecialCode);
		}

		// Traitement des droits
		for (int j = 0; j < droitsUniversitaires.count(); j++) {
			EODroitsUniversite unDroit = droitsUniversitaires.objectAtIndex(j);
			if (unDroit != null && (unDroit.rcptCode() != null && unDroit.toRepartitionCompte() != null)) {
				if (unDroit.rcptCode().equals(EORepartitionCompte.DROIT_AGME) == true) {
					if (histAdhAgme() != null && histAdhAgme().intValue() == 1) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
				if (unDroit.rcptCode().equals(EORepartitionCompte.DROIT_INSCRIPTION_AUDITEUR) == true) {
					if (typeInscription == 2) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
				if (unDroit.toRepartitionCompte().isDroitMutualiste() == true) {
					if (typeInscription == 2) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
				if (unDroit.rcptCode().equals(EORepartitionCompte.DROIT_SECURITE_SOCIALE) == true) {
					if (histAffss() != null
							&& histAyantDroit() != null
							&& histBourse() != null
							&& ((histAffss().intValue() == 1 && histAyantDroit().intValue() == 0) || (histAffss().intValue() == 1 && histAyantDroit()
									.intValue() == -1)) && (histBourse().intValue() != 1 && histBourse().intValue() != 2)) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
				if (unDroit.rcptCode().equals(EORepartitionCompte.DROIT_MEDECINE_PREVENTIVE) == true) {
					ajouterUnDroit(unDroit, inscDipl);
				}
				if (unDroit.rcptCode().equals(EORepartitionCompte.DROIT_BIBLIOTHEQUE_UNIVERSITAIRE) == true) {
					if (histBourse() != null && histBourse().intValue() != 1 && histBourse().intValue() != 2) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
				if (unDroit.rcptCode().equals(EORepartitionCompte.DROIT_ACTIVITE_SPORTIVE) == true) {
					ajouterUnDroit(unDroit, inscDipl);
				}
				if (unDroit.rcptCode().equals(EORepartitionCompte.DROIT_SERVICES_INFORMATIQUES) == true) {
					ajouterUnDroit(unDroit, inscDipl);
				}
				if (unDroit.toRepartitionCompte().isDroitInscriptionPrincipale() == true && unDroit.dunivTypeDroit().equals(typeDroitDiplome) == true) {
					if (histBourse() != null && (histBourse().intValue() != 1 && histBourse().intValue() != 2)
							&& (typeInscription == 0 || typeInscription == 4)) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
				if (unDroit.toRepartitionCompte().isDroitFonds() == true) {
					if (histBourse() != null && (histBourse().intValue() != 1 && histBourse().intValue() != 2) && (typeInscription == 0)) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
				if (unDroit.rcptCode().equals(EORepartitionCompte.DROIT_INSCRIPTION_CUMULATIVE) == true
						&& unDroit.dunivTypeDroit().equals(typeDroitDiplome) == true) {
					if (histBourse() != null && (histBourse().intValue() != 1 && histBourse().intValue() != 2) && (typeInscription == 1)) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
				if (droitSpecialValue != null && unDroit.rcptCode().equals(droitSpecialValue) == true
						&& unDroit.dunivTypeDroit().equals(typeDroitDiplome) == true) {
					if (histBourse() != null && (histBourse().intValue() != 1 && histBourse().intValue() != 2)) {
						ajouterUnDroit(unDroit, inscDipl);
					}
				}
			}
		}
	}

	/**
	 * Initialise la liste de droits universitaires avec ceux non encore regles pour les inscriptions autres que principale
	 * 
	 * @param inscDipl
	 *            Inscription a une formation a laquelle se rattache les droits (EOInscDipl).
	 */
	public void initialiserLesDroitsComplementaires(EOInscDipl inscDipl) {
		NSArray<EODroitsUniversite> droitsUniversitaires = droitsUniversitairesComplets();
		String typeDroitDiplome = null;
		int typeInscription = 666;

		// Recherche du type d'inscription
		if (inscDipl.toTypeInscription() != null && inscDipl.toTypeInscription().idiplTypeInscription() != null) {
			typeInscription = inscDipl.toTypeInscription().idiplTypeInscription().intValue();
		}

		// Recherche du type droit du diplome
		if (inscDipl.toFwkScolarite_ScolFormationSpecialisation() != null
				&& inscDipl.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome() != null
				&& inscDipl.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipTypeDroit() != null) {
			typeDroitDiplome = inscDipl.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipTypeDroit();
		}

		// Traitement des droits
		for (int j = 0; j < droitsUniversitaires.count(); j++) {
		    EODroitsUniversite unDroit = droitsUniversitaires.objectAtIndex(j);
		    if (correspondAuTypeDroitDiplomeEtDroitInscriptionComplementaire(typeDroitDiplome, unDroit) 
		            && nonBoursierEtInscriptionComplementaire(typeInscription)) {
		        ajouterUnDroit(unDroit, inscDipl);
		    }
		}
	}

    private boolean correspondAuTypeDroitDiplomeEtDroitInscriptionComplementaire(String typeDroitDiplome, EODroitsUniversite unDroit) {
        return unDroit != null 
                && unDroit.toRepartitionCompte() != null 
                && unDroit.toRepartitionCompte().isDroitInscriptionComplementaire() 
                && unDroit.aMemeTypeDroit(typeDroitDiplome);
    }
	
    private boolean nonBoursierEtInscriptionComplementaire(int typeInscription) {
        return nonBoursier() && isTypeInscriptionComplementaire(typeInscription);
    }

    private boolean isTypeInscriptionComplementaire(int typeInscription) {
        return EOTypeInscription.IDIPL_TYPE_INSCRIPTION_COMPLEMENTAIRE == typeInscription;
    }
    
    private boolean nonBoursier() {
        return histBourse() != null && histBourse().intValue() != 1 && histBourse().intValue() != 2;
    }

	/**
	 * Ajoute un droit universitaire dans une liste de droits, si et seulement si il n'est pas deja acquitte et inclus
	 * 
	 * @param unDroit
	 *            Un droit universitaire a ajouter (EODroitsUniversite).
	 * @param inscDipl
	 *            Inscription a une formation a laquelle se rattache ce droit (EOInscDipl).
	 */
	private void ajouterUnDroit(EODroitsUniversite unDroit, EOInscDipl inscDipl) {
		if (verifierPossibiliteAjoutDuDroit(unDroit, inscDipl) == true) {
			if (droitsCalcules == null || droitsCalcules.count() == 0) {
				droitsCalcules = new NSMutableArray<EODroitsUniversite>();
			}
			unDroit.setInscDipl(inscDipl);
			if (droitsCalcules.containsObject(unDroit) == false) {
				droitsCalcules.addObject(unDroit);
			}
		}
	}

	/**
	 * Verifie que le droit n'est pas deja acquitte pour l'inscription consideree.
	 * 
	 * @param unDroit
	 *            Un droit universitaire a verifier (EODroitsUniversite).
	 * @param inscDipl
	 *            Inscription a une formation a laquelle se rattachera ce droit (EOInscDipl).
	 * 
	 * @return Un boolean pour donner la reponse a la possibilite d'ajouter.
	 */
	private boolean verifierPossibiliteAjoutDuDroit(EODroitsUniversite unDroit, EOInscDipl inscDipl) {
		NSDictionary<String, Object> inscDico = EOUtilities.primaryKeyForObject(editingContext(), inscDipl);
		Integer idiplNumero = (Integer) inscDico.objectForKey(EOInscDipl.IDIPL_NUMERO_KEY);

		NSMutableArray<EOQualifier> andQualifier = new NSMutableArray<EOQualifier>(3);
		andQualifier.addObject(ERXQ.equals(EOVDpaieCarteRestant.NUMERO_INSCRIPTION_KEY, idiplNumero));
		andQualifier.addObject(ERXQ.equals(EOVDpaieCarteRestant.TO_REPARTITION_COMPTE_KEY + "." + EORepartitionCompte.RCPT_CODE_KEY,
				unDroit.rcptCode()));
		andQualifier.addObject(ERXQ.greaterThan(EOVDpaieCarteRestant.DETAIL_MONTANT_KEY, new BigDecimal(0.00)));
		NSArray<EOVDpaieCarteRestant> detailPaiements = EOVDpaieCarteRestant.fetchAll(editingContext(), ERXQ.and(andQualifier));

		if (detailPaiements != null && detailPaiements.count() > 0) {
			return false;
		}
		return true;
	}

}
