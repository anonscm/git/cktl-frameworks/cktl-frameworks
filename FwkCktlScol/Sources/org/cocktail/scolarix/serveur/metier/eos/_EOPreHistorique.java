/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreHistorique.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPreHistorique extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PreHistorique";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PRE_HISTORIQUE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "histNumero";

	public static final ERXKey<String> COTI_CODE = new ERXKey<String>("cotiCode");
	public static final ERXKey<String> DACT_CODE = new ERXKey<String>("dactCode");
	public static final ERXKey<Integer> DSPE_CODE_FUTUR = new ERXKey<Integer>("dspeCodeFutur");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<Integer> HIST1INSC_DEUG_DUT = new ERXKey<Integer>("hist1inscDeugDut");
	public static final ERXKey<String> HIST_ACAD_BOURSE = new ERXKey<String>("histAcadBourse");
	public static final ERXKey<Integer> HIST_ACCES_INFO = new ERXKey<Integer>("histAccesInfo");
	public static final ERXKey<Integer> HIST_ACTIV_PROF = new ERXKey<Integer>("histActivProf");
	public static final ERXKey<Integer> HIST_ADH_AGME = new ERXKey<Integer>("histAdhAgme");
	public static final ERXKey<Integer> HIST_AFFSS = new ERXKey<Integer>("histAffss");
	public static final ERXKey<Integer> HIST_ANNEE_DER_DIPL = new ERXKey<Integer>("histAnneeDerDipl");
	public static final ERXKey<Integer> HIST_ANNEE_DER_ETAB = new ERXKey<Integer>("histAnneeDerEtab");
	public static final ERXKey<Integer> HIST_ANNEE_DIPL_FUTUR = new ERXKey<Integer>("histAnneeDiplFutur");
	public static final ERXKey<Integer> HIST_ANNEE_SCOL = new ERXKey<Integer>("histAnneeScol");
	public static final ERXKey<Integer> HIST_ASSURANCE_CIVILE = new ERXKey<Integer>("histAssuranceCivile");
	public static final ERXKey<String> HIST_ASSURANCE_CIVILE_CIE = new ERXKey<String>("histAssuranceCivileCie");
	public static final ERXKey<String> HIST_ASSURANCE_CIVILE_NUM = new ERXKey<String>("histAssuranceCivileNum");
	public static final ERXKey<String> HIST_AUTRE_CAS_EXO = new ERXKey<String>("histAutreCasExo");
	public static final ERXKey<Integer> HIST_AYANT_DROIT = new ERXKey<Integer>("histAyantDroit");
	public static final ERXKey<Integer> HIST_BOURSE = new ERXKey<Integer>("histBourse");
	public static final ERXKey<NSTimestamp> HIST_DATE_TRANSFERT = new ERXKey<NSTimestamp>("histDateTransfert");
	public static final ERXKey<Integer> HIST_DEM_EMPL = new ERXKey<Integer>("histDemEmpl");
	public static final ERXKey<Integer> HIST_DUREE_INT = new ERXKey<Integer>("histDureeInt");
	public static final ERXKey<Integer> HIST_ECH_BOURSE = new ERXKey<Integer>("histEchBourse");
	public static final ERXKey<String> HIST_ENS_DER_ETAB = new ERXKey<String>("histEnsDerEtab");
	public static final ERXKey<Integer> HIST_FOR_PROF = new ERXKey<Integer>("histForProf");
	public static final ERXKey<Integer> HIST_INT_ETUD = new ERXKey<Integer>("histIntEtud");
	public static final ERXKey<String> HIST_LIBELLE_AUT_ETAB = new ERXKey<String>("histLibelleAutEtab");
	public static final ERXKey<String> HIST_LIBELLE_DER_DIPL = new ERXKey<String>("histLibelleDerDipl");
	public static final ERXKey<String> HIST_LIBELLE_DER_ETAB = new ERXKey<String>("histLibelleDerEtab");
	public static final ERXKey<String> HIST_LIBELLE_PROF = new ERXKey<String>("histLibelleProf");
	public static final ERXKey<Integer> HIST_NB_INSC_DEUG = new ERXKey<Integer>("histNbInscDeug");
	public static final ERXKey<String> HIST_NUMERO_ALLOC = new ERXKey<String>("histNumeroAlloc");
	public static final ERXKey<Integer> HIST_REDOUBLE = new ERXKey<Integer>("histRedouble");
	public static final ERXKey<String> HIST_REMARQUES = new ERXKey<String>("histRemarques");
	public static final ERXKey<Integer> HIST_RESTE_BU = new ERXKey<Integer>("histResteBu");
	public static final ERXKey<String> HIST_SALARIE = new ERXKey<String>("histSalarie");
	public static final ERXKey<String> HIST_SALARIE_LIBELLE = new ERXKey<String>("histSalarieLibelle");
	public static final ERXKey<Integer> HIST_TELEENS = new ERXKey<Integer>("histTeleens");
	public static final ERXKey<String> HIST_TRANSFERT = new ERXKey<String>("histTransfert");
	public static final ERXKey<String> HIST_TUTEUR1 = new ERXKey<String>("histTuteur1");
	public static final ERXKey<String> HIST_TUTEUR2 = new ERXKey<String>("histTuteur2");
	public static final ERXKey<String> HIST_TYPE_REGIME = new ERXKey<String>("histTypeRegime");
	public static final ERXKey<String> HIST_VILLE_AUT_ETAB = new ERXKey<String>("histVilleAutEtab");
	public static final ERXKey<String> HIST_VILLE_DER_DIPL = new ERXKey<String>("histVilleDerDipl");
	public static final ERXKey<String> HIST_VILLE_DER_ETAB = new ERXKey<String>("histVilleDerEtab");
	public static final ERXKey<String> PRO_CODE_ETUD = new ERXKey<String>("proCodeEtud");

	public static final String COTI_CODE_KEY = "cotiCode";
	public static final String DACT_CODE_KEY = "dactCode";
	public static final String DSPE_CODE_FUTUR_KEY = "dspeCodeFutur";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String HIST1INSC_DEUG_DUT_KEY = "hist1inscDeugDut";
	public static final String HIST_ACAD_BOURSE_KEY = "histAcadBourse";
	public static final String HIST_ACCES_INFO_KEY = "histAccesInfo";
	public static final String HIST_ACTIV_PROF_KEY = "histActivProf";
	public static final String HIST_ADH_AGME_KEY = "histAdhAgme";
	public static final String HIST_AFFSS_KEY = "histAffss";
	public static final String HIST_ANNEE_DER_DIPL_KEY = "histAnneeDerDipl";
	public static final String HIST_ANNEE_DER_ETAB_KEY = "histAnneeDerEtab";
	public static final String HIST_ANNEE_DIPL_FUTUR_KEY = "histAnneeDiplFutur";
	public static final String HIST_ANNEE_SCOL_KEY = "histAnneeScol";
	public static final String HIST_ASSURANCE_CIVILE_KEY = "histAssuranceCivile";
	public static final String HIST_ASSURANCE_CIVILE_CIE_KEY = "histAssuranceCivileCie";
	public static final String HIST_ASSURANCE_CIVILE_NUM_KEY = "histAssuranceCivileNum";
	public static final String HIST_AUTRE_CAS_EXO_KEY = "histAutreCasExo";
	public static final String HIST_AYANT_DROIT_KEY = "histAyantDroit";
	public static final String HIST_BOURSE_KEY = "histBourse";
	public static final String HIST_DATE_TRANSFERT_KEY = "histDateTransfert";
	public static final String HIST_DEM_EMPL_KEY = "histDemEmpl";
	public static final String HIST_DUREE_INT_KEY = "histDureeInt";
	public static final String HIST_ECH_BOURSE_KEY = "histEchBourse";
	public static final String HIST_ENS_DER_ETAB_KEY = "histEnsDerEtab";
	public static final String HIST_FOR_PROF_KEY = "histForProf";
	public static final String HIST_INT_ETUD_KEY = "histIntEtud";
	public static final String HIST_LIBELLE_AUT_ETAB_KEY = "histLibelleAutEtab";
	public static final String HIST_LIBELLE_DER_DIPL_KEY = "histLibelleDerDipl";
	public static final String HIST_LIBELLE_DER_ETAB_KEY = "histLibelleDerEtab";
	public static final String HIST_LIBELLE_PROF_KEY = "histLibelleProf";
	public static final String HIST_NB_INSC_DEUG_KEY = "histNbInscDeug";
	public static final String HIST_NUMERO_ALLOC_KEY = "histNumeroAlloc";
	public static final String HIST_REDOUBLE_KEY = "histRedouble";
	public static final String HIST_REMARQUES_KEY = "histRemarques";
	public static final String HIST_RESTE_BU_KEY = "histResteBu";
	public static final String HIST_SALARIE_KEY = "histSalarie";
	public static final String HIST_SALARIE_LIBELLE_KEY = "histSalarieLibelle";
	public static final String HIST_TELEENS_KEY = "histTeleens";
	public static final String HIST_TRANSFERT_KEY = "histTransfert";
	public static final String HIST_TUTEUR1_KEY = "histTuteur1";
	public static final String HIST_TUTEUR2_KEY = "histTuteur2";
	public static final String HIST_TYPE_REGIME_KEY = "histTypeRegime";
	public static final String HIST_VILLE_AUT_ETAB_KEY = "histVilleAutEtab";
	public static final String HIST_VILLE_DER_DIPL_KEY = "histVilleDerDipl";
	public static final String HIST_VILLE_DER_ETAB_KEY = "histVilleDerEtab";
	public static final String PRO_CODE_ETUD_KEY = "proCodeEtud";

	// Non visible attributes
	public static final String ACTIV_CODE_KEY = "activCode";
	public static final String DPTG_CODE_AUT_ETAB_KEY = "dptgCodeAutEtab";
	public static final String DPTG_CODE_DER_DIPL_KEY = "dptgCodeDerDipl";
	public static final String DPTG_CODE_DER_ETAB_KEY = "dptgCodeDerEtab";
	public static final String DSITU_CODE_KEY = "dsituCode";
	public static final String ETAB_CODE_AUT_ETAB_KEY = "etabCodeAutEtab";
	public static final String ETAB_CODE_DER_DIPL_KEY = "etabCodeDerDipl";
	public static final String ETAB_CODE_DER_ETAB_KEY = "etabCodeDerEtab";
	public static final String ETAB_CODE_TRANS_KEY = "etabCodeTrans";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String HIST_SALARIE_RNE_KEY = "histSalarieRne";
	public static final String HIST_TYPE_FORMATION_KEY = "histTypeFormation";
	public static final String LANG_CODE_KEY = "langCode";
	public static final String MUT_CODE_MUT_KEY = "mutCodeMut";
	public static final String MUT_CODE_ORGA_KEY = "mutCodeOrga";
	public static final String ORES_CODE_KEY = "oresCode";
	public static final String PAYS_CODE_DER_DIPL_KEY = "paysCodeDerDipl";
	public static final String PAYS_CODE_DER_ETAB_KEY = "paysCodeDerEtab";
	public static final String QTRA_CODE_KEY = "qtraCode";
	public static final String SITU_CODE_KEY = "situCode";
	public static final String TDIPL_CODE_DER_DIPL_KEY = "tdiplCodeDerDipl";
	public static final String THAN_CODE_KEY = "thanCode";

	// Colkeys
	public static final String COTI_CODE_COLKEY = "COTI_CODE";
	public static final String DACT_CODE_COLKEY = "DACT_CODE";
	public static final String DSPE_CODE_FUTUR_COLKEY = "DSPE_CODE_FUTUR";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String HIST1INSC_DEUG_DUT_COLKEY = "HIST_1INSC_DEUG_DUT";
	public static final String HIST_ACAD_BOURSE_COLKEY = "HIST_ACAD_BOURSE";
	public static final String HIST_ACCES_INFO_COLKEY = "HIST_ACCES_INFO";
	public static final String HIST_ACTIV_PROF_COLKEY = "HIST_ACTIV_PROF";
	public static final String HIST_ADH_AGME_COLKEY = "HIST_ADH_AGME";
	public static final String HIST_AFFSS_COLKEY = "HIST_AFFSS";
	public static final String HIST_ANNEE_DER_DIPL_COLKEY = "HIST_ANNEE_DER_DIPL";
	public static final String HIST_ANNEE_DER_ETAB_COLKEY = "HIST_ANNEE_DER_ETAB";
	public static final String HIST_ANNEE_DIPL_FUTUR_COLKEY = "HIST_ANNEE_DIPL_FUTUR";
	public static final String HIST_ANNEE_SCOL_COLKEY = "HIST_ANNEE_SCOL";
	public static final String HIST_ASSURANCE_CIVILE_COLKEY = "HIST_ASSURANCE_CIVILE";
	public static final String HIST_ASSURANCE_CIVILE_CIE_COLKEY = "HIST_ASSURANCE_CIVILE_CIE";
	public static final String HIST_ASSURANCE_CIVILE_NUM_COLKEY = "HIST_ASSURANCE_CIVILE_NUM";
	public static final String HIST_AUTRE_CAS_EXO_COLKEY = "HIST_AUTRE_CAS_EXO";
	public static final String HIST_AYANT_DROIT_COLKEY = "HIST_AYANT_DROIT";
	public static final String HIST_BOURSE_COLKEY = "HIST_BOURSE";
	public static final String HIST_DATE_TRANSFERT_COLKEY = "HIST_DATE_TRANSFERT";
	public static final String HIST_DEM_EMPL_COLKEY = "HIST_DEM_EMPL";
	public static final String HIST_DUREE_INT_COLKEY = "HIST_DUREE_INT";
	public static final String HIST_ECH_BOURSE_COLKEY = "HIST_ECH_BOURSE";
	public static final String HIST_ENS_DER_ETAB_COLKEY = "HIST_ENS_DER_ETAB";
	public static final String HIST_FOR_PROF_COLKEY = "HIST_FOR_PROF";
	public static final String HIST_INT_ETUD_COLKEY = "HIST_INT_ETUD";
	public static final String HIST_LIBELLE_AUT_ETAB_COLKEY = "HIST_LIBELLE_AUT_ETAB";
	public static final String HIST_LIBELLE_DER_DIPL_COLKEY = "HIST_LIBELLE_DER_DIPL";
	public static final String HIST_LIBELLE_DER_ETAB_COLKEY = "HIST_LIBELLE_DER_ETAB";
	public static final String HIST_LIBELLE_PROF_COLKEY = "HIST_LIBELLE_PROF";
	public static final String HIST_NB_INSC_DEUG_COLKEY = "HIST_NB_INSC_DEUG";
	public static final String HIST_NUMERO_ALLOC_COLKEY = "HIST_NUMERO_ALLOC";
	public static final String HIST_REDOUBLE_COLKEY = "HIST_REDOUBLE";
	public static final String HIST_REMARQUES_COLKEY = "HIST_REMARQUES";
	public static final String HIST_RESTE_BU_COLKEY = "HIST_RESTE_BU";
	public static final String HIST_SALARIE_COLKEY = "HIST_SALARIE";
	public static final String HIST_SALARIE_LIBELLE_COLKEY = "HIST_SALARIE_LIBELLE";
	public static final String HIST_TELEENS_COLKEY = "HIST_TELEENS";
	public static final String HIST_TRANSFERT_COLKEY = "HIST_TRANSFERT";
	public static final String HIST_TUTEUR1_COLKEY = "HIST_TUTEUR1";
	public static final String HIST_TUTEUR2_COLKEY = "HIST_TUTEUR2";
	public static final String HIST_TYPE_REGIME_COLKEY = "HIST_TYPE_REGIME";
	public static final String HIST_VILLE_AUT_ETAB_COLKEY = "HIST_VILLE_AUT_ETAB";
	public static final String HIST_VILLE_DER_DIPL_COLKEY = "HIST_VILLE_DER_DIPL";
	public static final String HIST_VILLE_DER_ETAB_COLKEY = "HIST_VILLE_DER_ETAB";
	public static final String PRO_CODE_ETUD_COLKEY = "PRO_CODE_ETUD";

	// Non visible colkeys
	public static final String ACTIV_CODE_COLKEY = "ACTIV_CODE";
	public static final String DPTG_CODE_AUT_ETAB_COLKEY = "DPTG_CODE_AUT_ETAB";
	public static final String DPTG_CODE_DER_DIPL_COLKEY = "DPTG_CODE_DER_DIPL";
	public static final String DPTG_CODE_DER_ETAB_COLKEY = "DPTG_CODE_DER_ETAB";
	public static final String DSITU_CODE_COLKEY = "DSITU_CODE";
	public static final String ETAB_CODE_AUT_ETAB_COLKEY = "ETAB_CODE_AUT_ETAB";
	public static final String ETAB_CODE_DER_DIPL_COLKEY = "ETAB_CODE_DER_DIPL";
	public static final String ETAB_CODE_DER_ETAB_COLKEY = "ETAB_CODE_DER_ETAB";
	public static final String ETAB_CODE_TRANS_COLKEY = "ETAB_CODE_TRANS";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String HIST_SALARIE_RNE_COLKEY = "HIST_SALARIE_RNE";
	public static final String HIST_TYPE_FORMATION_COLKEY = "HIST_TYPE_FORMATION";
	public static final String LANG_CODE_COLKEY = "LANG_CODE";
	public static final String MUT_CODE_MUT_COLKEY = "MUT_CODE_MUT";
	public static final String MUT_CODE_ORGA_COLKEY = "MUT_CODE_ORGA";
	public static final String ORES_CODE_COLKEY = "ORES_CODE";
	public static final String PAYS_CODE_DER_DIPL_COLKEY = "PAYS_CODE_DER_DIPL";
	public static final String PAYS_CODE_DER_ETAB_COLKEY = "PAYS_CODE_DER_ETAB";
	public static final String QTRA_CODE_COLKEY = "QTRA_CODE";
	public static final String SITU_CODE_COLKEY = "SITU_CODE";
	public static final String TDIPL_CODE_DER_DIPL_COLKEY = "TDIPL_CODE_DER_DIPL";
	public static final String THAN_CODE_COLKEY = "THAN_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOActiviteProfessionnelle> TO_ACTIVITE_PROFESSIONNELLE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOActiviteProfessionnelle>("toActiviteProfessionnelle");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODptSituation> TO_DPT_SITUATION = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODptSituation>("toDptSituation");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__AUT_ETAB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_AutEtab");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__DER_DIPL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_DerDipl");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__DER_ETAB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_DerEtab");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__DER_DIPL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_DerDipl");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__DER_ETAB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_DerEtab");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOLangue> TO_LANGUE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOLangue>("toLangue");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMutuelle> TO_MUTUELLE_MUT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMutuelle>("toMutuelleMut");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMutuelle> TO_MUTUELLE_ORGA = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMutuelle>("toMutuelleOrga");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOOrigineRessources> TO_ORIGINE_RESSOURCES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOOrigineRessources>("toOrigineRessources");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreInscription> TO_PRE_INSCRIPTIONS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreInscription>("toPreInscriptions");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOQuotiteTravail> TO_QUOTITE_TRAVAIL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOQuotiteTravail>("toQuotiteTravail");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_AUT_ETAB = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneAutEtab");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_DER_DIPL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneDerDipl");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_DER_ETAB = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneDerEtab");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_TRANS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneTrans");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOSituationScol> TO_SITUATION_SCOL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOSituationScol>("toSituationScol");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome> TO_TYPE_DIPLOME = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome>("toTypeDiplome");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeHandicap> TO_TYPE_HANDICAP = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeHandicap>("toTypeHandicap");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeRegimeSise> TO_TYPE_REGIME_SISE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeRegimeSise>("toTypeRegimeSise");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie> TO_V_ETABLISSEMENT_SALARIE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie>("toVEtablissementSalarie");

	public static final String TO_ACTIVITE_PROFESSIONNELLE_KEY = "toActiviteProfessionnelle";
	public static final String TO_DPT_SITUATION_KEY = "toDptSituation";
	public static final String TO_FWKPERS__DEPARTEMENT__AUT_ETAB_KEY = "toFwkpers_Departement_AutEtab";
	public static final String TO_FWKPERS__DEPARTEMENT__DER_DIPL_KEY = "toFwkpers_Departement_DerDipl";
	public static final String TO_FWKPERS__DEPARTEMENT__DER_ETAB_KEY = "toFwkpers_Departement_DerEtab";
	public static final String TO_FWKPERS__PAYS__DER_DIPL_KEY = "toFwkpers_Pays_DerDipl";
	public static final String TO_FWKPERS__PAYS__DER_ETAB_KEY = "toFwkpers_Pays_DerEtab";
	public static final String TO_LANGUE_KEY = "toLangue";
	public static final String TO_MUTUELLE_MUT_KEY = "toMutuelleMut";
	public static final String TO_MUTUELLE_ORGA_KEY = "toMutuelleOrga";
	public static final String TO_ORIGINE_RESSOURCES_KEY = "toOrigineRessources";
	public static final String TO_PRE_INSCRIPTIONS_KEY = "toPreInscriptions";
	public static final String TO_QUOTITE_TRAVAIL_KEY = "toQuotiteTravail";
	public static final String TO_RNE_AUT_ETAB_KEY = "toRneAutEtab";
	public static final String TO_RNE_DER_DIPL_KEY = "toRneDerDipl";
	public static final String TO_RNE_DER_ETAB_KEY = "toRneDerEtab";
	public static final String TO_RNE_TRANS_KEY = "toRneTrans";
	public static final String TO_SITUATION_SCOL_KEY = "toSituationScol";
	public static final String TO_TYPE_DIPLOME_KEY = "toTypeDiplome";
	public static final String TO_TYPE_HANDICAP_KEY = "toTypeHandicap";
	public static final String TO_TYPE_REGIME_SISE_KEY = "toTypeRegimeSise";
	public static final String TO_V_ETABLISSEMENT_SALARIE_KEY = "toVEtablissementSalarie";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPreHistorique with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param etudNumero
	 * @param hist1inscDeugDut
	 * @param histAccesInfo
	 * @param histActivProf
	 * @param histAdhAgme
	 * @param histAffss
	 * @param histAnneeScol
	 * @param histAssuranceCivile
	 * @param histAyantDroit
	 * @param histBourse
	 * @param histDemEmpl
	 * @param histForProf
	 * @param histIntEtud
	 * @param histNbInscDeug
	 * @param histResteBu
	 * @param histSalarie
	 * @param histSalarieLibelle
	 * @param histTeleens
	 * @return EOPreHistorique
	 */
	public static EOPreHistorique create(EOEditingContext editingContext, Integer etudNumero, Integer hist1inscDeugDut, Integer histAccesInfo, Integer histActivProf, Integer histAdhAgme, Integer histAffss, Integer histAnneeScol, Integer histAssuranceCivile, Integer histAyantDroit, Integer histBourse, Integer histDemEmpl, Integer histForProf, Integer histIntEtud, Integer histNbInscDeug, Integer histResteBu, String histSalarie, String histSalarieLibelle, Integer histTeleens) {
		EOPreHistorique eo = (EOPreHistorique) createAndInsertInstance(editingContext);
		eo.setEtudNumero(etudNumero);
		eo.setHist1inscDeugDut(hist1inscDeugDut);
		eo.setHistAccesInfo(histAccesInfo);
		eo.setHistActivProf(histActivProf);
		eo.setHistAdhAgme(histAdhAgme);
		eo.setHistAffss(histAffss);
		eo.setHistAnneeScol(histAnneeScol);
		eo.setHistAssuranceCivile(histAssuranceCivile);
		eo.setHistAyantDroit(histAyantDroit);
		eo.setHistBourse(histBourse);
		eo.setHistDemEmpl(histDemEmpl);
		eo.setHistForProf(histForProf);
		eo.setHistIntEtud(histIntEtud);
		eo.setHistNbInscDeug(histNbInscDeug);
		eo.setHistResteBu(histResteBu);
		eo.setHistSalarie(histSalarie);
		eo.setHistSalarieLibelle(histSalarieLibelle);
		eo.setHistTeleens(histTeleens);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPreHistorique.
	 *
	 * @param editingContext
	 * @return EOPreHistorique
	 */
	public static EOPreHistorique create(EOEditingContext editingContext) {
		EOPreHistorique eo = (EOPreHistorique) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPreHistorique localInstanceIn(EOEditingContext editingContext) {
		EOPreHistorique localInstance = (EOPreHistorique) localInstanceOfObject(editingContext, (EOPreHistorique) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPreHistorique localInstanceIn(EOEditingContext editingContext, EOPreHistorique eo) {
		EOPreHistorique localInstance = (eo == null) ? null : (EOPreHistorique) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cotiCode() {
		return (String) storedValueForKey("cotiCode");
	}

	public void setCotiCode(String value) {
		takeStoredValueForKey(value, "cotiCode");
	}
	public String dactCode() {
		return (String) storedValueForKey("dactCode");
	}

	public void setDactCode(String value) {
		takeStoredValueForKey(value, "dactCode");
	}
	public Integer dspeCodeFutur() {
		return (Integer) storedValueForKey("dspeCodeFutur");
	}

	public void setDspeCodeFutur(Integer value) {
		takeStoredValueForKey(value, "dspeCodeFutur");
	}
	public Integer etudNumero() {
		return (Integer) storedValueForKey("etudNumero");
	}

	public void setEtudNumero(Integer value) {
		takeStoredValueForKey(value, "etudNumero");
	}
	public Integer hist1inscDeugDut() {
		return (Integer) storedValueForKey("hist1inscDeugDut");
	}

	public void setHist1inscDeugDut(Integer value) {
		takeStoredValueForKey(value, "hist1inscDeugDut");
	}
	public String histAcadBourse() {
		return (String) storedValueForKey("histAcadBourse");
	}

	public void setHistAcadBourse(String value) {
		takeStoredValueForKey(value, "histAcadBourse");
	}
	public Integer histAccesInfo() {
		return (Integer) storedValueForKey("histAccesInfo");
	}

	public void setHistAccesInfo(Integer value) {
		takeStoredValueForKey(value, "histAccesInfo");
	}
	public Integer histActivProf() {
		return (Integer) storedValueForKey("histActivProf");
	}

	public void setHistActivProf(Integer value) {
		takeStoredValueForKey(value, "histActivProf");
	}
	public Integer histAdhAgme() {
		return (Integer) storedValueForKey("histAdhAgme");
	}

	public void setHistAdhAgme(Integer value) {
		takeStoredValueForKey(value, "histAdhAgme");
	}
	public Integer histAffss() {
		return (Integer) storedValueForKey("histAffss");
	}

	public void setHistAffss(Integer value) {
		takeStoredValueForKey(value, "histAffss");
	}
	public Integer histAnneeDerDipl() {
		return (Integer) storedValueForKey("histAnneeDerDipl");
	}

	public void setHistAnneeDerDipl(Integer value) {
		takeStoredValueForKey(value, "histAnneeDerDipl");
	}
	public Integer histAnneeDerEtab() {
		return (Integer) storedValueForKey("histAnneeDerEtab");
	}

	public void setHistAnneeDerEtab(Integer value) {
		takeStoredValueForKey(value, "histAnneeDerEtab");
	}
	public Integer histAnneeDiplFutur() {
		return (Integer) storedValueForKey("histAnneeDiplFutur");
	}

	public void setHistAnneeDiplFutur(Integer value) {
		takeStoredValueForKey(value, "histAnneeDiplFutur");
	}
	public Integer histAnneeScol() {
		return (Integer) storedValueForKey("histAnneeScol");
	}

	public void setHistAnneeScol(Integer value) {
		takeStoredValueForKey(value, "histAnneeScol");
	}
	public Integer histAssuranceCivile() {
		return (Integer) storedValueForKey("histAssuranceCivile");
	}

	public void setHistAssuranceCivile(Integer value) {
		takeStoredValueForKey(value, "histAssuranceCivile");
	}
	public String histAssuranceCivileCie() {
		return (String) storedValueForKey("histAssuranceCivileCie");
	}

	public void setHistAssuranceCivileCie(String value) {
		takeStoredValueForKey(value, "histAssuranceCivileCie");
	}
	public String histAssuranceCivileNum() {
		return (String) storedValueForKey("histAssuranceCivileNum");
	}

	public void setHistAssuranceCivileNum(String value) {
		takeStoredValueForKey(value, "histAssuranceCivileNum");
	}
	public String histAutreCasExo() {
		return (String) storedValueForKey("histAutreCasExo");
	}

	public void setHistAutreCasExo(String value) {
		takeStoredValueForKey(value, "histAutreCasExo");
	}
	public Integer histAyantDroit() {
		return (Integer) storedValueForKey("histAyantDroit");
	}

	public void setHistAyantDroit(Integer value) {
		takeStoredValueForKey(value, "histAyantDroit");
	}
	public Integer histBourse() {
		return (Integer) storedValueForKey("histBourse");
	}

	public void setHistBourse(Integer value) {
		takeStoredValueForKey(value, "histBourse");
	}
	public NSTimestamp histDateTransfert() {
		return (NSTimestamp) storedValueForKey("histDateTransfert");
	}

	public void setHistDateTransfert(NSTimestamp value) {
		takeStoredValueForKey(value, "histDateTransfert");
	}
	public Integer histDemEmpl() {
		return (Integer) storedValueForKey("histDemEmpl");
	}

	public void setHistDemEmpl(Integer value) {
		takeStoredValueForKey(value, "histDemEmpl");
	}
	public Integer histDureeInt() {
		return (Integer) storedValueForKey("histDureeInt");
	}

	public void setHistDureeInt(Integer value) {
		takeStoredValueForKey(value, "histDureeInt");
	}
	public Integer histEchBourse() {
		return (Integer) storedValueForKey("histEchBourse");
	}

	public void setHistEchBourse(Integer value) {
		takeStoredValueForKey(value, "histEchBourse");
	}
	public String histEnsDerEtab() {
		return (String) storedValueForKey("histEnsDerEtab");
	}

	public void setHistEnsDerEtab(String value) {
		takeStoredValueForKey(value, "histEnsDerEtab");
	}
	public Integer histForProf() {
		return (Integer) storedValueForKey("histForProf");
	}

	public void setHistForProf(Integer value) {
		takeStoredValueForKey(value, "histForProf");
	}
	public Integer histIntEtud() {
		return (Integer) storedValueForKey("histIntEtud");
	}

	public void setHistIntEtud(Integer value) {
		takeStoredValueForKey(value, "histIntEtud");
	}
	public String histLibelleAutEtab() {
		return (String) storedValueForKey("histLibelleAutEtab");
	}

	public void setHistLibelleAutEtab(String value) {
		takeStoredValueForKey(value, "histLibelleAutEtab");
	}
	public String histLibelleDerDipl() {
		return (String) storedValueForKey("histLibelleDerDipl");
	}

	public void setHistLibelleDerDipl(String value) {
		takeStoredValueForKey(value, "histLibelleDerDipl");
	}
	public String histLibelleDerEtab() {
		return (String) storedValueForKey("histLibelleDerEtab");
	}

	public void setHistLibelleDerEtab(String value) {
		takeStoredValueForKey(value, "histLibelleDerEtab");
	}
	public String histLibelleProf() {
		return (String) storedValueForKey("histLibelleProf");
	}

	public void setHistLibelleProf(String value) {
		takeStoredValueForKey(value, "histLibelleProf");
	}
	public Integer histNbInscDeug() {
		return (Integer) storedValueForKey("histNbInscDeug");
	}

	public void setHistNbInscDeug(Integer value) {
		takeStoredValueForKey(value, "histNbInscDeug");
	}
	public String histNumeroAlloc() {
		return (String) storedValueForKey("histNumeroAlloc");
	}

	public void setHistNumeroAlloc(String value) {
		takeStoredValueForKey(value, "histNumeroAlloc");
	}
	public Integer histRedouble() {
		return (Integer) storedValueForKey("histRedouble");
	}

	public void setHistRedouble(Integer value) {
		takeStoredValueForKey(value, "histRedouble");
	}
	public String histRemarques() {
		return (String) storedValueForKey("histRemarques");
	}

	public void setHistRemarques(String value) {
		takeStoredValueForKey(value, "histRemarques");
	}
	public Integer histResteBu() {
		return (Integer) storedValueForKey("histResteBu");
	}

	public void setHistResteBu(Integer value) {
		takeStoredValueForKey(value, "histResteBu");
	}
	public String histSalarie() {
		return (String) storedValueForKey("histSalarie");
	}

	public void setHistSalarie(String value) {
		takeStoredValueForKey(value, "histSalarie");
	}
	public String histSalarieLibelle() {
		return (String) storedValueForKey("histSalarieLibelle");
	}

	public void setHistSalarieLibelle(String value) {
		takeStoredValueForKey(value, "histSalarieLibelle");
	}
	public Integer histTeleens() {
		return (Integer) storedValueForKey("histTeleens");
	}

	public void setHistTeleens(Integer value) {
		takeStoredValueForKey(value, "histTeleens");
	}
	public String histTransfert() {
		return (String) storedValueForKey("histTransfert");
	}

	public void setHistTransfert(String value) {
		takeStoredValueForKey(value, "histTransfert");
	}
	public String histTuteur1() {
		return (String) storedValueForKey("histTuteur1");
	}

	public void setHistTuteur1(String value) {
		takeStoredValueForKey(value, "histTuteur1");
	}
	public String histTuteur2() {
		return (String) storedValueForKey("histTuteur2");
	}

	public void setHistTuteur2(String value) {
		takeStoredValueForKey(value, "histTuteur2");
	}
	public String histTypeRegime() {
		return (String) storedValueForKey("histTypeRegime");
	}

	public void setHistTypeRegime(String value) {
		takeStoredValueForKey(value, "histTypeRegime");
	}
	public String histVilleAutEtab() {
		return (String) storedValueForKey("histVilleAutEtab");
	}

	public void setHistVilleAutEtab(String value) {
		takeStoredValueForKey(value, "histVilleAutEtab");
	}
	public String histVilleDerDipl() {
		return (String) storedValueForKey("histVilleDerDipl");
	}

	public void setHistVilleDerDipl(String value) {
		takeStoredValueForKey(value, "histVilleDerDipl");
	}
	public String histVilleDerEtab() {
		return (String) storedValueForKey("histVilleDerEtab");
	}

	public void setHistVilleDerEtab(String value) {
		takeStoredValueForKey(value, "histVilleDerEtab");
	}
	public String proCodeEtud() {
		return (String) storedValueForKey("proCodeEtud");
	}

	public void setProCodeEtud(String value) {
		takeStoredValueForKey(value, "proCodeEtud");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOActiviteProfessionnelle toActiviteProfessionnelle() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOActiviteProfessionnelle)storedValueForKey("toActiviteProfessionnelle");
	}

	public void setToActiviteProfessionnelleRelationship(org.cocktail.scolarix.serveur.metier.eos.EOActiviteProfessionnelle value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOActiviteProfessionnelle oldValue = toActiviteProfessionnelle();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toActiviteProfessionnelle");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toActiviteProfessionnelle");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EODptSituation toDptSituation() {
		return (org.cocktail.scolarix.serveur.metier.eos.EODptSituation)storedValueForKey("toDptSituation");
	}

	public void setToDptSituationRelationship(org.cocktail.scolarix.serveur.metier.eos.EODptSituation value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EODptSituation oldValue = toDptSituation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDptSituation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toDptSituation");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_AutEtab() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_AutEtab");
	}

	public void setToFwkpers_Departement_AutEtabRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_AutEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_AutEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_AutEtab");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_DerDipl() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_DerDipl");
	}

	public void setToFwkpers_Departement_DerDiplRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_DerDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_DerDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_DerDipl");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_DerEtab() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_DerEtab");
	}

	public void setToFwkpers_Departement_DerEtabRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_DerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_DerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_DerEtab");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_DerDipl() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_DerDipl");
	}

	public void setToFwkpers_Pays_DerDiplRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_DerDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_DerDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_DerDipl");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_DerEtab() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_DerEtab");
	}

	public void setToFwkpers_Pays_DerEtabRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_DerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_DerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_DerEtab");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOLangue toLangue() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOLangue)storedValueForKey("toLangue");
	}

	public void setToLangueRelationship(org.cocktail.scolarix.serveur.metier.eos.EOLangue value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOLangue oldValue = toLangue();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLangue");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toLangue");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOMutuelle toMutuelleMut() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOMutuelle)storedValueForKey("toMutuelleMut");
	}

	public void setToMutuelleMutRelationship(org.cocktail.scolarix.serveur.metier.eos.EOMutuelle value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOMutuelle oldValue = toMutuelleMut();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMutuelleMut");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toMutuelleMut");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOMutuelle toMutuelleOrga() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOMutuelle)storedValueForKey("toMutuelleOrga");
	}

	public void setToMutuelleOrgaRelationship(org.cocktail.scolarix.serveur.metier.eos.EOMutuelle value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOMutuelle oldValue = toMutuelleOrga();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMutuelleOrga");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toMutuelleOrga");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOOrigineRessources toOrigineRessources() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOOrigineRessources)storedValueForKey("toOrigineRessources");
	}

	public void setToOrigineRessourcesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOOrigineRessources value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOOrigineRessources oldValue = toOrigineRessources();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toOrigineRessources");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toOrigineRessources");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOQuotiteTravail toQuotiteTravail() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOQuotiteTravail)storedValueForKey("toQuotiteTravail");
	}

	public void setToQuotiteTravailRelationship(org.cocktail.scolarix.serveur.metier.eos.EOQuotiteTravail value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOQuotiteTravail oldValue = toQuotiteTravail();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toQuotiteTravail");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toQuotiteTravail");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneAutEtab() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneAutEtab");
	}

	public void setToRneAutEtabRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneAutEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneAutEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneAutEtab");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneDerDipl() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneDerDipl");
	}

	public void setToRneDerDiplRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneDerDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneDerDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneDerDipl");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneDerEtab() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneDerEtab");
	}

	public void setToRneDerEtabRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneDerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneDerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneDerEtab");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneTrans() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneTrans");
	}

	public void setToRneTransRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneTrans();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneTrans");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneTrans");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOSituationScol toSituationScol() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOSituationScol)storedValueForKey("toSituationScol");
	}

	public void setToSituationScolRelationship(org.cocktail.scolarix.serveur.metier.eos.EOSituationScol value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOSituationScol oldValue = toSituationScol();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSituationScol");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toSituationScol");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome toTypeDiplome() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome)storedValueForKey("toTypeDiplome");
	}

	public void setToTypeDiplomeRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome oldValue = toTypeDiplome();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeDiplome");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toTypeDiplome");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOTypeHandicap toTypeHandicap() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeHandicap)storedValueForKey("toTypeHandicap");
	}

	public void setToTypeHandicapRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeHandicap value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeHandicap oldValue = toTypeHandicap();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeHandicap");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toTypeHandicap");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOTypeRegimeSise toTypeRegimeSise() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeRegimeSise)storedValueForKey("toTypeRegimeSise");
	}

	public void setToTypeRegimeSiseRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeRegimeSise value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeRegimeSise oldValue = toTypeRegimeSise();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeRegimeSise");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toTypeRegimeSise");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie toVEtablissementSalarie() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie)storedValueForKey("toVEtablissementSalarie");
	}

	public void setToVEtablissementSalarieRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementSalarie oldValue = toVEtablissementSalarie();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVEtablissementSalarie");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toVEtablissementSalarie");
		}
	}
  
	public NSArray toPreInscriptions() {
		return (NSArray)storedValueForKey("toPreInscriptions");
	}

	public NSArray toPreInscriptions(EOQualifier qualifier) {
		return toPreInscriptions(qualifier, null);
	}

	public NSArray toPreInscriptions(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPreInscriptions();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPreInscriptionsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreInscription object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPreInscriptions");
	}

	public void removeFromToPreInscriptionsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreInscription object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreInscriptions");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPreInscription createToPreInscriptionsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PreInscription");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPreInscriptions");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPreInscription) eo;
	}

	public void deleteToPreInscriptionsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreInscription object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreInscriptions");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPreInscriptionsRelationships() {
		Enumeration objects = toPreInscriptions().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPreInscriptionsRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPreInscription)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPreHistorique.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPreHistorique.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPreHistorique)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPreHistorique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreHistorique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPreHistorique eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPreHistorique)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPreHistorique fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreHistorique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreHistorique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPreHistorique eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPreHistorique)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreHistorique fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreHistorique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPreHistorique eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPreHistorique ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPreHistorique createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPreHistorique.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPreHistorique.ENTITY_NAME + "' !");
		}
		else {
			EOPreHistorique object = (EOPreHistorique) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPreHistorique localInstanceOfObject(EOEditingContext ec, EOPreHistorique object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPreHistorique " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPreHistorique) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
