/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVBordereauDetail.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVBordereauDetail extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VBordereauDetail";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_BORDEREAU_DETAIL";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "erembNumero";

	public static final ERXKey<java.math.BigDecimal> AFF = new ERXKey<java.math.BigDecimal>("aff");
	public static final ERXKey<java.math.BigDecimal> AUD = new ERXKey<java.math.BigDecimal>("aud");
	public static final ERXKey<java.math.BigDecimal> BU = new ERXKey<java.math.BigDecimal>("bu");
	public static final ERXKey<java.math.BigDecimal> CM = new ERXKey<java.math.BigDecimal>("cm");
	public static final ERXKey<java.math.BigDecimal> CMP = new ERXKey<java.math.BigDecimal>("cmp");
	public static final ERXKey<java.math.BigDecimal> CUM = new ERXKey<java.math.BigDecimal>("cum");
	public static final ERXKey<Integer> EREMB_ANNEE = new ERXKey<Integer>("erembAnnee");
	public static final ERXKey<Integer> EREMB_NUM_BORDEREAU = new ERXKey<Integer>("erembNumBordereau");
	public static final ERXKey<String> ETAB_CODE = new ERXKey<String>("etabCode");
	public static final ERXKey<String> ETAB_LIBELLE = new ERXKey<String>("etabLibelle");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<java.math.BigDecimal> FAV = new ERXKey<java.math.BigDecimal>("fav");
	public static final ERXKey<Integer> HIST_NUMERO = new ERXKey<Integer>("histNumero");
	public static final ERXKey<java.math.BigDecimal> MED = new ERXKey<java.math.BigDecimal>("med");
	public static final ERXKey<java.math.BigDecimal> MNA = new ERXKey<java.math.BigDecimal>("mna");
	public static final ERXKey<java.math.BigDecimal> MNB = new ERXKey<java.math.BigDecimal>("mnb");
	public static final ERXKey<java.math.BigDecimal> MNC = new ERXKey<java.math.BigDecimal>("mnc");
	public static final ERXKey<java.math.BigDecimal> MND = new ERXKey<java.math.BigDecimal>("mnd");
	public static final ERXKey<java.math.BigDecimal> MNE = new ERXKey<java.math.BigDecimal>("mne");
	public static final ERXKey<String> NOM = new ERXKey<String>("nom");
	public static final ERXKey<String> NOM_MARITAL = new ERXKey<String>("nomMarital");
	public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
	public static final ERXKey<java.math.BigDecimal> SCO = new ERXKey<java.math.BigDecimal>("sco");
	public static final ERXKey<java.math.BigDecimal> SEC = new ERXKey<java.math.BigDecimal>("sec");
	public static final ERXKey<java.math.BigDecimal> SMA = new ERXKey<java.math.BigDecimal>("sma");
	public static final ERXKey<java.math.BigDecimal> SMB = new ERXKey<java.math.BigDecimal>("smb");
	public static final ERXKey<java.math.BigDecimal> SMC = new ERXKey<java.math.BigDecimal>("smc");
	public static final ERXKey<java.math.BigDecimal> SMD = new ERXKey<java.math.BigDecimal>("smd");
	public static final ERXKey<java.math.BigDecimal> SME = new ERXKey<java.math.BigDecimal>("sme");
	public static final ERXKey<java.math.BigDecimal> SMF = new ERXKey<java.math.BigDecimal>("smf");
	public static final ERXKey<java.math.BigDecimal> SOMME = new ERXKey<java.math.BigDecimal>("somme");
	public static final ERXKey<java.math.BigDecimal> SOMME_CALC = new ERXKey<java.math.BigDecimal>("sommeCalc");
	public static final ERXKey<java.math.BigDecimal> SPO = new ERXKey<java.math.BigDecimal>("spo");
	public static final ERXKey<String> TYPE_MOUVEMENT = new ERXKey<String>("typeMouvement");

	public static final String AFF_KEY = "aff";
	public static final String AUD_KEY = "aud";
	public static final String BU_KEY = "bu";
	public static final String CM_KEY = "cm";
	public static final String CMP_KEY = "cmp";
	public static final String CUM_KEY = "cum";
	public static final String EREMB_ANNEE_KEY = "erembAnnee";
	public static final String EREMB_NUM_BORDEREAU_KEY = "erembNumBordereau";
	public static final String ETAB_CODE_KEY = "etabCode";
	public static final String ETAB_LIBELLE_KEY = "etabLibelle";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String FAV_KEY = "fav";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String MED_KEY = "med";
	public static final String MNA_KEY = "mna";
	public static final String MNB_KEY = "mnb";
	public static final String MNC_KEY = "mnc";
	public static final String MND_KEY = "mnd";
	public static final String MNE_KEY = "mne";
	public static final String NOM_KEY = "nom";
	public static final String NOM_MARITAL_KEY = "nomMarital";
	public static final String PRENOM_KEY = "prenom";
	public static final String SCO_KEY = "sco";
	public static final String SEC_KEY = "sec";
	public static final String SMA_KEY = "sma";
	public static final String SMB_KEY = "smb";
	public static final String SMC_KEY = "smc";
	public static final String SMD_KEY = "smd";
	public static final String SME_KEY = "sme";
	public static final String SMF_KEY = "smf";
	public static final String SOMME_KEY = "somme";
	public static final String SOMME_CALC_KEY = "sommeCalc";
	public static final String SPO_KEY = "spo";
	public static final String TYPE_MOUVEMENT_KEY = "typeMouvement";

	// Non visible attributes
	public static final String EREMB_NUMERO_KEY = "erembNumero";

	// Colkeys
	public static final String AFF_COLKEY = "AFF";
	public static final String AUD_COLKEY = "AUD";
	public static final String BU_COLKEY = "BU";
	public static final String CM_COLKEY = "CM";
	public static final String CMP_COLKEY = "CMP";
	public static final String CUM_COLKEY = "CUM";
	public static final String EREMB_ANNEE_COLKEY = "EREMB_ANNEE";
	public static final String EREMB_NUM_BORDEREAU_COLKEY = "EREMB_NUM_BORDEREAU";
	public static final String ETAB_CODE_COLKEY = "ETAB_CODE";
	public static final String ETAB_LIBELLE_COLKEY = "ETAB_LIBELLE";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String FAV_COLKEY = "FAV";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String MED_COLKEY = "MED";
	public static final String MNA_COLKEY = "MNA";
	public static final String MNB_COLKEY = "MNB";
	public static final String MNC_COLKEY = "MNC";
	public static final String MND_COLKEY = "MND";
	public static final String MNE_COLKEY = "MNE";
	public static final String NOM_COLKEY = "NOM";
	public static final String NOM_MARITAL_COLKEY = "NOM_MARITAL";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String SCO_COLKEY = "SCO";
	public static final String SEC_COLKEY = "SEC";
	public static final String SMA_COLKEY = "SMA";
	public static final String SMB_COLKEY = "SMB";
	public static final String SMC_COLKEY = "SMC";
	public static final String SMD_COLKEY = "SMD";
	public static final String SME_COLKEY = "SME";
	public static final String SMF_COLKEY = "SMF";
	public static final String SOMME_COLKEY = "SOMME";
	public static final String SOMME_CALC_COLKEY = "SOMME_CALC";
	public static final String SPO_COLKEY = "SPO";
	public static final String TYPE_MOUVEMENT_COLKEY = "TYPE_MOUVEMENT";

	// Non visible colkeys
	public static final String EREMB_NUMERO_COLKEY = "EREMB_NUMERO";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse> TO_ETUDIANT_REMBOURSE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse>("toEtudiantRembourse");

	public static final String TO_ETUDIANT_REMBOURSE_KEY = "toEtudiantRembourse";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVBordereauDetail with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param toEtudiantRembourse
	 * @return EOVBordereauDetail
	 */
	public static EOVBordereauDetail create(EOEditingContext editingContext, org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse toEtudiantRembourse) {
		EOVBordereauDetail eo = (EOVBordereauDetail) createAndInsertInstance(editingContext);
		eo.setToEtudiantRembourseRelationship(toEtudiantRembourse);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVBordereauDetail.
	 *
	 * @param editingContext
	 * @return EOVBordereauDetail
	 */
	public static EOVBordereauDetail create(EOEditingContext editingContext) {
		EOVBordereauDetail eo = (EOVBordereauDetail) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVBordereauDetail localInstanceIn(EOEditingContext editingContext) {
		EOVBordereauDetail localInstance = (EOVBordereauDetail) localInstanceOfObject(editingContext, (EOVBordereauDetail) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVBordereauDetail localInstanceIn(EOEditingContext editingContext, EOVBordereauDetail eo) {
		EOVBordereauDetail localInstance = (eo == null) ? null : (EOVBordereauDetail) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public java.math.BigDecimal aff() {
		return (java.math.BigDecimal) storedValueForKey("aff");
	}

	public void setAff(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "aff");
	}
	public java.math.BigDecimal aud() {
		return (java.math.BigDecimal) storedValueForKey("aud");
	}

	public void setAud(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "aud");
	}
	public java.math.BigDecimal bu() {
		return (java.math.BigDecimal) storedValueForKey("bu");
	}

	public void setBu(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "bu");
	}
	public java.math.BigDecimal cm() {
		return (java.math.BigDecimal) storedValueForKey("cm");
	}

	public void setCm(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "cm");
	}
	public java.math.BigDecimal cmp() {
		return (java.math.BigDecimal) storedValueForKey("cmp");
	}

	public void setCmp(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "cmp");
	}
	public java.math.BigDecimal cum() {
		return (java.math.BigDecimal) storedValueForKey("cum");
	}

	public void setCum(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "cum");
	}
	public Integer erembAnnee() {
		return (Integer) storedValueForKey("erembAnnee");
	}

	public void setErembAnnee(Integer value) {
		takeStoredValueForKey(value, "erembAnnee");
	}
	public Integer erembNumBordereau() {
		return (Integer) storedValueForKey("erembNumBordereau");
	}

	public void setErembNumBordereau(Integer value) {
		takeStoredValueForKey(value, "erembNumBordereau");
	}
	public String etabCode() {
		return (String) storedValueForKey("etabCode");
	}

	public void setEtabCode(String value) {
		takeStoredValueForKey(value, "etabCode");
	}
	public String etabLibelle() {
		return (String) storedValueForKey("etabLibelle");
	}

	public void setEtabLibelle(String value) {
		takeStoredValueForKey(value, "etabLibelle");
	}
	public Integer etudNumero() {
		return (Integer) storedValueForKey("etudNumero");
	}

	public void setEtudNumero(Integer value) {
		takeStoredValueForKey(value, "etudNumero");
	}
	public java.math.BigDecimal fav() {
		return (java.math.BigDecimal) storedValueForKey("fav");
	}

	public void setFav(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "fav");
	}
	public Integer histNumero() {
		return (Integer) storedValueForKey("histNumero");
	}

	public void setHistNumero(Integer value) {
		takeStoredValueForKey(value, "histNumero");
	}
	public java.math.BigDecimal med() {
		return (java.math.BigDecimal) storedValueForKey("med");
	}

	public void setMed(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "med");
	}
	public java.math.BigDecimal mna() {
		return (java.math.BigDecimal) storedValueForKey("mna");
	}

	public void setMna(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mna");
	}
	public java.math.BigDecimal mnb() {
		return (java.math.BigDecimal) storedValueForKey("mnb");
	}

	public void setMnb(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mnb");
	}
	public java.math.BigDecimal mnc() {
		return (java.math.BigDecimal) storedValueForKey("mnc");
	}

	public void setMnc(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mnc");
	}
	public java.math.BigDecimal mnd() {
		return (java.math.BigDecimal) storedValueForKey("mnd");
	}

	public void setMnd(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mnd");
	}
	public java.math.BigDecimal mne() {
		return (java.math.BigDecimal) storedValueForKey("mne");
	}

	public void setMne(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mne");
	}
	public String nom() {
		return (String) storedValueForKey("nom");
	}

	public void setNom(String value) {
		takeStoredValueForKey(value, "nom");
	}
	public String nomMarital() {
		return (String) storedValueForKey("nomMarital");
	}

	public void setNomMarital(String value) {
		takeStoredValueForKey(value, "nomMarital");
	}
	public String prenom() {
		return (String) storedValueForKey("prenom");
	}

	public void setPrenom(String value) {
		takeStoredValueForKey(value, "prenom");
	}
	public java.math.BigDecimal sco() {
		return (java.math.BigDecimal) storedValueForKey("sco");
	}

	public void setSco(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "sco");
	}
	public java.math.BigDecimal sec() {
		return (java.math.BigDecimal) storedValueForKey("sec");
	}

	public void setSec(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "sec");
	}
	public java.math.BigDecimal sma() {
		return (java.math.BigDecimal) storedValueForKey("sma");
	}

	public void setSma(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "sma");
	}
	public java.math.BigDecimal smb() {
		return (java.math.BigDecimal) storedValueForKey("smb");
	}

	public void setSmb(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "smb");
	}
	public java.math.BigDecimal smc() {
		return (java.math.BigDecimal) storedValueForKey("smc");
	}

	public void setSmc(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "smc");
	}
	public java.math.BigDecimal smd() {
		return (java.math.BigDecimal) storedValueForKey("smd");
	}

	public void setSmd(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "smd");
	}
	public java.math.BigDecimal sme() {
		return (java.math.BigDecimal) storedValueForKey("sme");
	}

	public void setSme(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "sme");
	}
	public java.math.BigDecimal smf() {
		return (java.math.BigDecimal) storedValueForKey("smf");
	}

	public void setSmf(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "smf");
	}
	public java.math.BigDecimal somme() {
		return (java.math.BigDecimal) storedValueForKey("somme");
	}

	public void setSomme(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "somme");
	}
	public java.math.BigDecimal sommeCalc() {
		return (java.math.BigDecimal) storedValueForKey("sommeCalc");
	}

	public void setSommeCalc(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "sommeCalc");
	}
	public java.math.BigDecimal spo() {
		return (java.math.BigDecimal) storedValueForKey("spo");
	}

	public void setSpo(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "spo");
	}
	public String typeMouvement() {
		return (String) storedValueForKey("typeMouvement");
	}

	public void setTypeMouvement(String value) {
		takeStoredValueForKey(value, "typeMouvement");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse toEtudiantRembourse() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse)storedValueForKey("toEtudiantRembourse");
	}

	public void setToEtudiantRembourseRelationship(org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse oldValue = toEtudiantRembourse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toEtudiantRembourse");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toEtudiantRembourse");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVBordereauDetail.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVBordereauDetail.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVBordereauDetail)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVBordereauDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVBordereauDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVBordereauDetail eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVBordereauDetail)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVBordereauDetail fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVBordereauDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVBordereauDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVBordereauDetail eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVBordereauDetail)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVBordereauDetail fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVBordereauDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVBordereauDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVBordereauDetail ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVBordereauDetail createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVBordereauDetail.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVBordereauDetail.ENTITY_NAME + "' !");
		}
		else {
			EOVBordereauDetail object = (EOVBordereauDetail) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVBordereauDetail localInstanceOfObject(EOEditingContext ec, EOVBordereauDetail object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVBordereauDetail " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVBordereauDetail) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
