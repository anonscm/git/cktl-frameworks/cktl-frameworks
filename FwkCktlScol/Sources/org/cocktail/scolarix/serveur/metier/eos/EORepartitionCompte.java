/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.metier.eos;

import com.webobjects.foundation.NSValidation;

public class EORepartitionCompte extends _EORepartitionCompte {

	public static final String DROIT_INSCRIPTION_ADMISSION = "ADM";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_00 = "SCO";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_01 = "SC1";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_02 = "SC2";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_03 = "SC3";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_04 = "SC4";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_05 = "SC5";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_06 = "SC6";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_07 = "SC7";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_08 = "SC8";
	public static final String DROIT_INSCRIPTION_PRINCIPALE_09 = "SC9";
	public static final String DROIT_INSCRIPTION_COMPLEMENTAIRE = "CMP";
	public static final String DROIT_INSCRIPTION_CUMULATIVE = "CUM";
	public static final String DROIT_INSCRIPTION_AUDITEUR = "AUD";
	public static final String DROIT_SECURITE_SOCIALE = "SEC";
	public static final String DROIT_BIBLIOTHEQUE_UNIVERSITAIRE = "BU";
	public static final String DROIT_ACTIVITE_SPORTIVE = "SPO";
	public static final String DROIT_MEDECINE_PREVENTIVE = "MED";
	public static final String DROIT_FONDS_AIDE = "FAV";
	public static final String DROIT_FONDS_SOCIAL = "FSD";
	public static final String DROIT_SERVICES_INFORMATIQUES = "AFF";
	public static final String DROIT_AGME = "AGM";
	public static final String DROIT_MUTUALISTE_11 = "MU1";
	public static final String DROIT_MUTUALISTE_12 = "MU2";
	public static final String DROIT_MUTUALISTE_13 = "MU3";
	public static final String DROIT_MUTUALISTE_14 = "MU4";
	public static final String DROIT_MUTUALISTE_15 = "MU5";
	public static final String DROIT_MUTUALISTE_21 = "SM1";
	public static final String DROIT_MUTUALISTE_22 = "SM2";
	public static final String DROIT_MUTUALISTE_23 = "SM3";
	public static final String DROIT_MUTUALISTE_24 = "SM4";
	public static final String DROIT_MUTUALISTE_25 = "SM5";
	public static final String DROIT_SPECIAL_01 = "SP1";
	public static final String DROIT_SPECIAL_02 = "SP2";
	public static final String DROIT_SPECIAL_03 = "SP3";
	public static final String DROIT_SPECIAL_04 = "SP4";
	public static final String DROIT_SPECIAL_05 = "SP5";

	public EORepartitionCompte() {
		super();
	}

	public String toString() {
		return rcptCode() + " - " + toPlanco().pcoLib();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public boolean isDroitInscriptionComplementaire() {
	    return DROIT_INSCRIPTION_COMPLEMENTAIRE.equals(rcptCode());
	}
	
	public boolean isDroitInscriptionPrincipale() {
		if (this != null
				&& this.rcptCode() != null
				&& (this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_00) == true
						|| this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_01) == true
						|| this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_02) == true
						|| this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_03) == true
						|| this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_04) == true
						|| this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_05) == true
						|| this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_06) == true
						|| this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_07) == true
						|| this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_08) == true || this.rcptCode().equals(DROIT_INSCRIPTION_PRINCIPALE_09) == true)) {
			return true;
		}
		return false;
	}

	public boolean isDroitMutualiste() {
		if (this != null
				&& this.rcptCode() != null
				&& (this.rcptCode().equals(DROIT_MUTUALISTE_11) == true || this.rcptCode().equals(DROIT_MUTUALISTE_12) == true
						|| this.rcptCode().equals(DROIT_MUTUALISTE_13) == true || this.rcptCode().equals(DROIT_MUTUALISTE_14) == true
						|| this.rcptCode().equals(DROIT_MUTUALISTE_15) == true || this.rcptCode().equals(DROIT_MUTUALISTE_21) == true
						|| this.rcptCode().equals(DROIT_MUTUALISTE_22) == true || this.rcptCode().equals(DROIT_MUTUALISTE_23) == true
						|| this.rcptCode().equals(DROIT_MUTUALISTE_24) == true || this.rcptCode().equals(DROIT_MUTUALISTE_25) == true)) {
			return true;
		}
		return false;
	}

	public boolean isDroitFonds() {
		if (this != null && this.rcptCode() != null
				&& (this.rcptCode().equals(DROIT_FONDS_AIDE) == true || this.rcptCode().equals(DROIT_FONDS_SOCIAL) == true)) {
			return true;
		}
		return false;
	}

	public boolean isDroitSpecial() {
		if (this != null
				&& this.rcptCode() != null
				&& (this.rcptCode().equals(DROIT_SPECIAL_01) == true || this.rcptCode().equals(DROIT_SPECIAL_02) == true
						|| this.rcptCode().equals(DROIT_SPECIAL_03) == true || this.rcptCode().equals(DROIT_SPECIAL_04) == true || this.rcptCode()
						.equals(DROIT_SPECIAL_05) == true)) {
			return true;
		}
		return false;
	}

}
