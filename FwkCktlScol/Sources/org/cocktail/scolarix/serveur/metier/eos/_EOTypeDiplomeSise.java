/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeDiplomeSise.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOTypeDiplomeSise extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_TypeDiplomeSise";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.TYPE_DIPLOME_SISE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "fgraCode";

	public static final ERXKey<String> FGRA_CODE = new ERXKey<String>("fgraCode");
	public static final ERXKey<String> TDIPL_CODE = new ERXKey<String>("tdiplCode");

	public static final String FGRA_CODE_KEY = "fgraCode";
	public static final String TDIPL_CODE_KEY = "tdiplCode";

	// Non visible attributes

	// Colkeys
	public static final String FGRA_CODE_COLKEY = "FGRA_CODE";
	public static final String TDIPL_CODE_COLKEY = "TDIPL_CODE";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome> TO_TYPE_DIPLOME = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome>("toTypeDiplome");

	public static final String TO_TYPE_DIPLOME_KEY = "toTypeDiplome";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOTypeDiplomeSise with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fgraCode
	 * @param tdiplCode
	 * @param toTypeDiplome
	 * @return EOTypeDiplomeSise
	 */
	public static EOTypeDiplomeSise create(EOEditingContext editingContext, String fgraCode, String tdiplCode, org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome toTypeDiplome) {
		EOTypeDiplomeSise eo = (EOTypeDiplomeSise) createAndInsertInstance(editingContext);
		eo.setFgraCode(fgraCode);
		eo.setTdiplCode(tdiplCode);
		eo.setToTypeDiplomeRelationship(toTypeDiplome);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOTypeDiplomeSise.
	 *
	 * @param editingContext
	 * @return EOTypeDiplomeSise
	 */
	public static EOTypeDiplomeSise create(EOEditingContext editingContext) {
		EOTypeDiplomeSise eo = (EOTypeDiplomeSise) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOTypeDiplomeSise localInstanceIn(EOEditingContext editingContext) {
		EOTypeDiplomeSise localInstance = (EOTypeDiplomeSise) localInstanceOfObject(editingContext, (EOTypeDiplomeSise) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOTypeDiplomeSise localInstanceIn(EOEditingContext editingContext, EOTypeDiplomeSise eo) {
		EOTypeDiplomeSise localInstance = (eo == null) ? null : (EOTypeDiplomeSise) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String fgraCode() {
		return (String) storedValueForKey("fgraCode");
	}

	public void setFgraCode(String value) {
		takeStoredValueForKey(value, "fgraCode");
	}
	public String tdiplCode() {
		return (String) storedValueForKey("tdiplCode");
	}

	public void setTdiplCode(String value) {
		takeStoredValueForKey(value, "tdiplCode");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome toTypeDiplome() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome)storedValueForKey("toTypeDiplome");
	}

	public void setToTypeDiplomeRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeDiplome oldValue = toTypeDiplome();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeDiplome");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toTypeDiplome");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOTypeDiplomeSise.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOTypeDiplomeSise.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOTypeDiplomeSise)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOTypeDiplomeSise fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeDiplomeSise fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOTypeDiplomeSise eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOTypeDiplomeSise)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOTypeDiplomeSise fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOTypeDiplomeSise fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOTypeDiplomeSise fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOTypeDiplomeSise eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOTypeDiplomeSise)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOTypeDiplomeSise fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOTypeDiplomeSise fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOTypeDiplomeSise eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOTypeDiplomeSise ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchFetchAll(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", "FwkScolarix_TypeDiplomeSise");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EOTypeDiplomeSise createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOTypeDiplomeSise.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOTypeDiplomeSise.ENTITY_NAME + "' !");
		}
		else {
			EOTypeDiplomeSise object = (EOTypeDiplomeSise) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOTypeDiplomeSise localInstanceOfObject(EOEditingContext ec, EOTypeDiplomeSise object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOTypeDiplomeSise " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOTypeDiplomeSise) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
