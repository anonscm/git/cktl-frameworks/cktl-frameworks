/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolarix.serveur.exception.ParametreException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOGarnucheApplication extends _EOGarnucheApplication {

	public static final String APPL_CODE_PREINSCRIPTION = "PREINSCAPP";
	public static final String APPL_CODE_PREREINSCRIPTION = "REINSCAPP";
	public static final String APPL_CODE_INSCRIPTION = "INSCAPP";
	public static final String APPL_CODE_ADMISSION = "ADMISSION";
	public static final String APPL_CODE_ADMIN_INSCRIPTION = "ADMIN_INSCAPP";
	public static final String APPL_CODE_ADMIN_PRE_PRERE_INSCRIPTION = "ADMIN_PREINSCAPP";

	public EOGarnucheApplication() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public EOGarnucheAppliOuverture garnucheAppliOuvertureEnCours() {
		EOGarnucheAppliOuverture garnucheAppliOuverture = null;
		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
		NSTimestamp now = DateCtrl.now();
		// les 2 dates null
		EOKeyValueQualifier qual1 = new EOKeyValueQualifier(EOGarnucheAppliOuverture.AOUV_DATE_DEBUT_KEY, EOQualifier.QualifierOperatorEqual,
				EOKeyValueCoding.NullValue);
		EOKeyValueQualifier qual2 = new EOKeyValueQualifier(EOGarnucheAppliOuverture.AOUV_DATE_FIN_KEY, EOQualifier.QualifierOperatorEqual,
				EOKeyValueCoding.NullValue);
		orQualifiers.addObject(new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qual1, qual2 })));
		// premiere date null
		qual1 = new EOKeyValueQualifier(EOGarnucheAppliOuverture.AOUV_DATE_DEBUT_KEY, EOQualifier.QualifierOperatorEqual, EOKeyValueCoding.NullValue);
		qual2 = new EOKeyValueQualifier(EOGarnucheAppliOuverture.AOUV_DATE_FIN_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, now);
		orQualifiers.addObject(new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qual1, qual2 })));
		// seconde date null
		qual1 = new EOKeyValueQualifier(EOGarnucheAppliOuverture.AOUV_DATE_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, now);
		qual2 = new EOKeyValueQualifier(EOGarnucheAppliOuverture.AOUV_DATE_FIN_KEY, EOQualifier.QualifierOperatorEqual, EOKeyValueCoding.NullValue);
		orQualifiers.addObject(new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qual1, qual2 })));
		// aucune date null
		qual1 = new EOKeyValueQualifier(EOGarnucheAppliOuverture.AOUV_DATE_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, now);
		qual2 = new EOKeyValueQualifier(EOGarnucheAppliOuverture.AOUV_DATE_FIN_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, now);
		orQualifiers.addObject(new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qual1, qual2 })));

		NSArray<EOGarnucheAppliOuverture> garnucheAppliOuvertures = toGarnucheAppliOuvertures(new EOOrQualifier(orQualifiers));
		if (garnucheAppliOuvertures != null && garnucheAppliOuvertures.count() > 1) {
			throw new ParametreException("Plusieurs dates d'ouverture de l'application " + applNom() + " (" + applCode()
					+ ") se chevauchent, revoir le parametrage !");
		}
		if (garnucheAppliOuvertures != null && garnucheAppliOuvertures.count() == 1) {
			garnucheAppliOuverture = garnucheAppliOuvertures.objectAtIndex(0);
		}
		return garnucheAppliOuverture;
	}

	public EOScolFormationAnnee scolFormationAnneeEnCours() {
		EOScolFormationAnnee anneeEnCours = null;
		EOGarnucheAppliOuverture garnucheAppliOuvertureEnCours = garnucheAppliOuvertureEnCours();
		if (garnucheAppliOuvertureEnCours != null) {
			anneeEnCours = garnucheAppliOuvertureEnCours.toFwkScolarite_ScolFormationAnnee();
		}
		return anneeEnCours;
	}

	public Integer anneeEnCours() {
		Integer anneeEnCours = null;
		EOGarnucheAppliOuverture garnucheAppliOuvertureEnCours = garnucheAppliOuvertureEnCours();
		if (garnucheAppliOuvertureEnCours != null) {
			anneeEnCours = garnucheAppliOuvertureEnCours.histAnneeScol();
		}
		return anneeEnCours;
	}

	public boolean isOuverte() {
		return anneeEnCours() != null;
	}
}
