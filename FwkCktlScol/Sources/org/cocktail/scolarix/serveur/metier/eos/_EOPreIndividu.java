/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreIndividu.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPreIndividu extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PreIndividu";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PRE_INDIVIDU";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "noIndividu";

	public static final ERXKey<Integer> CATEGORIE_PRINC = new ERXKey<Integer>("categoriePrinc");
	public static final ERXKey<String> C_CIVILITE = new ERXKey<String>("cCivilite");
	public static final ERXKey<String> C_DEPT_NAISSANCE = new ERXKey<String>("cDeptNaissance");
	public static final ERXKey<String> C_PAYS_NAISSANCE = new ERXKey<String>("cPaysNaissance");
	public static final ERXKey<String> C_PAYS_NATIONALITE = new ERXKey<String>("cPaysNationalite");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DECES = new ERXKey<NSTimestamp>("dDeces");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<NSTimestamp> D_NAISSANCE = new ERXKey<NSTimestamp>("dNaissance");
	public static final ERXKey<NSTimestamp> D_NATURALISATION = new ERXKey<NSTimestamp>("dNaturalisation");
	public static final ERXKey<String> IND_ACTIVITE = new ERXKey<String>("indActivite");
	public static final ERXKey<String> IND_AGENDA = new ERXKey<String>("indAgenda");
	public static final ERXKey<Integer> IND_CLE_INSEE = new ERXKey<Integer>("indCleInsee");
	public static final ERXKey<Integer> IND_CLE_INSEE_PROV = new ERXKey<Integer>("indCleInseeProv");
	public static final ERXKey<String> IND_C_SIT_MILITAIRE = new ERXKey<String>("indCSitMilitaire");
	public static final ERXKey<String> IND_C_SITUATION_FAMILLE = new ERXKey<String>("indCSituationFamille");
	public static final ERXKey<String> IND_NO_INSEE = new ERXKey<String>("indNoInsee");
	public static final ERXKey<String> IND_NO_INSEE_PROV = new ERXKey<String>("indNoInseeProv");
	public static final ERXKey<String> IND_ORIGINE = new ERXKey<String>("indOrigine");
	public static final ERXKey<String> IND_PHOTO = new ERXKey<String>("indPhoto");
	public static final ERXKey<String> IND_QUALITE = new ERXKey<String>("indQualite");
	public static final ERXKey<String> LANGUE_PREF = new ERXKey<String>("languePref");
	public static final ERXKey<String> LISTE_ROUGE = new ERXKey<String>("listeRouge");
	public static final ERXKey<Integer> NO_INDIVIDU_CREATEUR = new ERXKey<Integer>("noIndividuCreateur");
	public static final ERXKey<String> NOM_AFFICHAGE = new ERXKey<String>("nomAffichage");
	public static final ERXKey<String> NOM_PATRONYMIQUE = new ERXKey<String>("nomPatronymique");
	public static final ERXKey<String> NOM_PATRONYMIQUE_AFFICHAGE = new ERXKey<String>("nomPatronymiqueAffichage");
	public static final ERXKey<String> NOM_USUEL = new ERXKey<String>("nomUsuel");
	public static final ERXKey<String> ORGA_CODE = new ERXKey<String>("orgaCode");
	public static final ERXKey<String> ORGA_LIBELLE = new ERXKey<String>("orgaLibelle");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
	public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
	public static final ERXKey<String> PRENOM2 = new ERXKey<String>("prenom2");
	public static final ERXKey<String> PRENOM_AFFICHAGE = new ERXKey<String>("prenomAffichage");
	public static final ERXKey<String> PRISE_CPT_INSEE = new ERXKey<String>("priseCptInsee");
	public static final ERXKey<String> TEM_SS_DIPLOME = new ERXKey<String>("temSsDiplome");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
	public static final ERXKey<String> VILLE_DE_NAISSANCE = new ERXKey<String>("villeDeNaissance");

	public static final String CATEGORIE_PRINC_KEY = "categoriePrinc";
	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String C_DEPT_NAISSANCE_KEY = "cDeptNaissance";
	public static final String C_PAYS_NAISSANCE_KEY = "cPaysNaissance";
	public static final String C_PAYS_NATIONALITE_KEY = "cPaysNationalite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String D_NATURALISATION_KEY = "dNaturalisation";
	public static final String IND_ACTIVITE_KEY = "indActivite";
	public static final String IND_AGENDA_KEY = "indAgenda";
	public static final String IND_CLE_INSEE_KEY = "indCleInsee";
	public static final String IND_CLE_INSEE_PROV_KEY = "indCleInseeProv";
	public static final String IND_C_SIT_MILITAIRE_KEY = "indCSitMilitaire";
	public static final String IND_C_SITUATION_FAMILLE_KEY = "indCSituationFamille";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String IND_NO_INSEE_PROV_KEY = "indNoInseeProv";
	public static final String IND_ORIGINE_KEY = "indOrigine";
	public static final String IND_PHOTO_KEY = "indPhoto";
	public static final String IND_QUALITE_KEY = "indQualite";
	public static final String LANGUE_PREF_KEY = "languePref";
	public static final String LISTE_ROUGE_KEY = "listeRouge";
	public static final String NO_INDIVIDU_CREATEUR_KEY = "noIndividuCreateur";
	public static final String NOM_AFFICHAGE_KEY = "nomAffichage";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_PATRONYMIQUE_AFFICHAGE_KEY = "nomPatronymiqueAffichage";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String ORGA_CODE_KEY = "orgaCode";
	public static final String ORGA_LIBELLE_KEY = "orgaLibelle";
	public static final String PERS_ID_KEY = "persId";
	public static final String PRENOM_KEY = "prenom";
	public static final String PRENOM2_KEY = "prenom2";
	public static final String PRENOM_AFFICHAGE_KEY = "prenomAffichage";
	public static final String PRISE_CPT_INSEE_KEY = "priseCptInsee";
	public static final String TEM_SS_DIPLOME_KEY = "temSsDiplome";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String VILLE_DE_NAISSANCE_KEY = "villeDeNaissance";

	// Non visible attributes
	public static final String NO_INDIVIDU_KEY = "noIndividu";

	// Colkeys
	public static final String CATEGORIE_PRINC_COLKEY = "CATEGORIE_PRINC";
	public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
	public static final String C_DEPT_NAISSANCE_COLKEY = "C_DEPT_NAISSANCE";
	public static final String C_PAYS_NAISSANCE_COLKEY = "C_PAYS_NAISSANCE";
	public static final String C_PAYS_NATIONALITE_COLKEY = "C_PAYS_NATIONALITE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DECES_COLKEY = "D_DECES";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String D_NATURALISATION_COLKEY = "D_NATURALISATION";
	public static final String IND_ACTIVITE_COLKEY = "IND_ACTIVITE";
	public static final String IND_AGENDA_COLKEY = "IND_AGENDA";
	public static final String IND_CLE_INSEE_COLKEY = "IND_CLE_INSEE";
	public static final String IND_CLE_INSEE_PROV_COLKEY = "IND_CLE_INSEE_PROV";
	public static final String IND_C_SIT_MILITAIRE_COLKEY = "IND_C_SIT_MILITAIRE";
	public static final String IND_C_SITUATION_FAMILLE_COLKEY = "IND_C_SITUATION_FAMILLE";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String IND_NO_INSEE_PROV_COLKEY = "IND_NO_INSEE_PROV";
	public static final String IND_ORIGINE_COLKEY = "IND_ORIGINE";
	public static final String IND_PHOTO_COLKEY = "IND_PHOTO";
	public static final String IND_QUALITE_COLKEY = "IND_QUALITE";
	public static final String LANGUE_PREF_COLKEY = "LANGUE_PREF";
	public static final String LISTE_ROUGE_COLKEY = "LISTE_ROUGE";
	public static final String NO_INDIVIDU_CREATEUR_COLKEY = "NO_INDIVIDU_CREATEUR";
	public static final String NOM_AFFICHAGE_COLKEY = "NOM_AFFICHAGE";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String NOM_PATRONYMIQUE_AFFICHAGE_COLKEY = "NOM_PATRONYMIQUE_AFFICHAGE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String ORGA_CODE_COLKEY = "ORGA_CODE";
	public static final String ORGA_LIBELLE_COLKEY = "ORGA_LIBELLE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String PRENOM2_COLKEY = "PRENOM2";
	public static final String PRENOM_AFFICHAGE_COLKEY = "PRENOM_AFFICHAGE";
	public static final String PRISE_CPT_INSEE_COLKEY = "PRISE_CPT_INSEE";
	public static final String TEM_SS_DIPLOME_COLKEY = "TEM_SS_DIPLOME";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";
	public static final String VILLE_DE_NAISSANCE_COLKEY = "VILLE_DE_NAISSANCE";

	// Non visible colkeys
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite> TO_FWKPERS__CIVILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite>("toFwkpers_Civilite");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__NAISSANCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_Naissance");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__NATIONALITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_Nationalite");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale> TO_FWKPERS__SITUATION_FAMILIALE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale>("toFwkpers_SituationFamiliale");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire> TO_FWKPERS__SITUATION_MILITAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire>("toFwkpers_SituationMilitaire");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone> TO_PRE_PERSONNE_TELEPHONES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone>("toPrePersonneTelephones");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse> TO_PRE_REPART_ADRESSES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse>("toPreRepartAdresses");

	public static final String TO_FWKPERS__CIVILITE_KEY = "toFwkpers_Civilite";
	public static final String TO_FWKPERS__DEPARTEMENT_KEY = "toFwkpers_Departement";
	public static final String TO_FWKPERS__PAYS__NAISSANCE_KEY = "toFwkpers_Pays_Naissance";
	public static final String TO_FWKPERS__PAYS__NATIONALITE_KEY = "toFwkpers_Pays_Nationalite";
	public static final String TO_FWKPERS__SITUATION_FAMILIALE_KEY = "toFwkpers_SituationFamiliale";
	public static final String TO_FWKPERS__SITUATION_MILITAIRE_KEY = "toFwkpers_SituationMilitaire";
	public static final String TO_PRE_PERSONNE_TELEPHONES_KEY = "toPrePersonneTelephones";
	public static final String TO_PRE_REPART_ADRESSES_KEY = "toPreRepartAdresses";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPreIndividu with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cPaysNaissance
	 * @param cPaysNationalite
	 * @param dCreation
	 * @param dModification
	 * @param indAgenda
	 * @param indPhoto
	 * @param languePref
	 * @param listeRouge
	 * @param nomUsuel
	 * @param orgaCode
	 * @param orgaLibelle
	 * @param persId
	 * @param prenom
	 * @param temSsDiplome
	 * @param temValide
	 * @param toFwkpers_Pays_Naissance
	 * @param toFwkpers_Pays_Nationalite
	 * @return EOPreIndividu
	 */
	public static EOPreIndividu create(EOEditingContext editingContext, String cPaysNaissance, String cPaysNationalite, NSTimestamp dCreation, NSTimestamp dModification, String indAgenda, String indPhoto, String languePref, String listeRouge, String nomUsuel, String orgaCode, String orgaLibelle, Integer persId, String prenom, String temSsDiplome, String temValide, org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Naissance, org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Nationalite) {
		EOPreIndividu eo = (EOPreIndividu) createAndInsertInstance(editingContext);
		eo.setCPaysNaissance(cPaysNaissance);
		eo.setCPaysNationalite(cPaysNationalite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIndAgenda(indAgenda);
		eo.setIndPhoto(indPhoto);
		eo.setLanguePref(languePref);
		eo.setListeRouge(listeRouge);
		eo.setNomUsuel(nomUsuel);
		eo.setOrgaCode(orgaCode);
		eo.setOrgaLibelle(orgaLibelle);
		eo.setPersId(persId);
		eo.setPrenom(prenom);
		eo.setTemSsDiplome(temSsDiplome);
		eo.setTemValide(temValide);
		eo.setToFwkpers_Pays_NaissanceRelationship(toFwkpers_Pays_Naissance);
		eo.setToFwkpers_Pays_NationaliteRelationship(toFwkpers_Pays_Nationalite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPreIndividu.
	 *
	 * @param editingContext
	 * @return EOPreIndividu
	 */
	public static EOPreIndividu create(EOEditingContext editingContext) {
		EOPreIndividu eo = (EOPreIndividu) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPreIndividu localInstanceIn(EOEditingContext editingContext) {
		EOPreIndividu localInstance = (EOPreIndividu) localInstanceOfObject(editingContext, (EOPreIndividu) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPreIndividu localInstanceIn(EOEditingContext editingContext, EOPreIndividu eo) {
		EOPreIndividu localInstance = (eo == null) ? null : (EOPreIndividu) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer categoriePrinc() {
		return (Integer) storedValueForKey("categoriePrinc");
	}

	public void setCategoriePrinc(Integer value) {
		takeStoredValueForKey(value, "categoriePrinc");
	}
	public String cCivilite() {
		return (String) storedValueForKey("cCivilite");
	}

	public void setCCivilite(String value) {
		takeStoredValueForKey(value, "cCivilite");
	}
	public String cDeptNaissance() {
		return (String) storedValueForKey("cDeptNaissance");
	}

	public void setCDeptNaissance(String value) {
		takeStoredValueForKey(value, "cDeptNaissance");
	}
	public String cPaysNaissance() {
		return (String) storedValueForKey("cPaysNaissance");
	}

	public void setCPaysNaissance(String value) {
		takeStoredValueForKey(value, "cPaysNaissance");
	}
	public String cPaysNationalite() {
		return (String) storedValueForKey("cPaysNationalite");
	}

	public void setCPaysNationalite(String value) {
		takeStoredValueForKey(value, "cPaysNationalite");
	}
	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}
	public NSTimestamp dDeces() {
		return (NSTimestamp) storedValueForKey("dDeces");
	}

	public void setDDeces(NSTimestamp value) {
		takeStoredValueForKey(value, "dDeces");
	}
	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	public NSTimestamp dNaissance() {
		return (NSTimestamp) storedValueForKey("dNaissance");
	}

	public void setDNaissance(NSTimestamp value) {
		takeStoredValueForKey(value, "dNaissance");
	}
	public NSTimestamp dNaturalisation() {
		return (NSTimestamp) storedValueForKey("dNaturalisation");
	}

	public void setDNaturalisation(NSTimestamp value) {
		takeStoredValueForKey(value, "dNaturalisation");
	}
	public String indActivite() {
		return (String) storedValueForKey("indActivite");
	}

	public void setIndActivite(String value) {
		takeStoredValueForKey(value, "indActivite");
	}
	public String indAgenda() {
		return (String) storedValueForKey("indAgenda");
	}

	public void setIndAgenda(String value) {
		takeStoredValueForKey(value, "indAgenda");
	}
	public Integer indCleInsee() {
		return (Integer) storedValueForKey("indCleInsee");
	}

	public void setIndCleInsee(Integer value) {
		takeStoredValueForKey(value, "indCleInsee");
	}
	public Integer indCleInseeProv() {
		return (Integer) storedValueForKey("indCleInseeProv");
	}

	public void setIndCleInseeProv(Integer value) {
		takeStoredValueForKey(value, "indCleInseeProv");
	}
	public String indCSitMilitaire() {
		return (String) storedValueForKey("indCSitMilitaire");
	}

	public void setIndCSitMilitaire(String value) {
		takeStoredValueForKey(value, "indCSitMilitaire");
	}
	public String indCSituationFamille() {
		return (String) storedValueForKey("indCSituationFamille");
	}

	public void setIndCSituationFamille(String value) {
		takeStoredValueForKey(value, "indCSituationFamille");
	}
	public String indNoInsee() {
		return (String) storedValueForKey("indNoInsee");
	}

	public void setIndNoInsee(String value) {
		takeStoredValueForKey(value, "indNoInsee");
	}
	public String indNoInseeProv() {
		return (String) storedValueForKey("indNoInseeProv");
	}

	public void setIndNoInseeProv(String value) {
		takeStoredValueForKey(value, "indNoInseeProv");
	}
	public String indOrigine() {
		return (String) storedValueForKey("indOrigine");
	}

	public void setIndOrigine(String value) {
		takeStoredValueForKey(value, "indOrigine");
	}
	public String indPhoto() {
		return (String) storedValueForKey("indPhoto");
	}

	public void setIndPhoto(String value) {
		takeStoredValueForKey(value, "indPhoto");
	}
	public String indQualite() {
		return (String) storedValueForKey("indQualite");
	}

	public void setIndQualite(String value) {
		takeStoredValueForKey(value, "indQualite");
	}
	public String languePref() {
		return (String) storedValueForKey("languePref");
	}

	public void setLanguePref(String value) {
		takeStoredValueForKey(value, "languePref");
	}
	public String listeRouge() {
		return (String) storedValueForKey("listeRouge");
	}

	public void setListeRouge(String value) {
		takeStoredValueForKey(value, "listeRouge");
	}
	public Integer noIndividuCreateur() {
		return (Integer) storedValueForKey("noIndividuCreateur");
	}

	public void setNoIndividuCreateur(Integer value) {
		takeStoredValueForKey(value, "noIndividuCreateur");
	}
	public String nomAffichage() {
		return (String) storedValueForKey("nomAffichage");
	}

	public void setNomAffichage(String value) {
		takeStoredValueForKey(value, "nomAffichage");
	}
	public String nomPatronymique() {
		return (String) storedValueForKey("nomPatronymique");
	}

	public void setNomPatronymique(String value) {
		takeStoredValueForKey(value, "nomPatronymique");
	}
	public String nomPatronymiqueAffichage() {
		return (String) storedValueForKey("nomPatronymiqueAffichage");
	}

	public void setNomPatronymiqueAffichage(String value) {
		takeStoredValueForKey(value, "nomPatronymiqueAffichage");
	}
	public String nomUsuel() {
		return (String) storedValueForKey("nomUsuel");
	}

	public void setNomUsuel(String value) {
		takeStoredValueForKey(value, "nomUsuel");
	}
	public String orgaCode() {
		return (String) storedValueForKey("orgaCode");
	}

	public void setOrgaCode(String value) {
		takeStoredValueForKey(value, "orgaCode");
	}
	public String orgaLibelle() {
		return (String) storedValueForKey("orgaLibelle");
	}

	public void setOrgaLibelle(String value) {
		takeStoredValueForKey(value, "orgaLibelle");
	}
	public Integer persId() {
		return (Integer) storedValueForKey("persId");
	}

	public void setPersId(Integer value) {
		takeStoredValueForKey(value, "persId");
	}
	public String prenom() {
		return (String) storedValueForKey("prenom");
	}

	public void setPrenom(String value) {
		takeStoredValueForKey(value, "prenom");
	}
	public String prenom2() {
		return (String) storedValueForKey("prenom2");
	}

	public void setPrenom2(String value) {
		takeStoredValueForKey(value, "prenom2");
	}
	public String prenomAffichage() {
		return (String) storedValueForKey("prenomAffichage");
	}

	public void setPrenomAffichage(String value) {
		takeStoredValueForKey(value, "prenomAffichage");
	}
	public String priseCptInsee() {
		return (String) storedValueForKey("priseCptInsee");
	}

	public void setPriseCptInsee(String value) {
		takeStoredValueForKey(value, "priseCptInsee");
	}
	public String temSsDiplome() {
		return (String) storedValueForKey("temSsDiplome");
	}

	public void setTemSsDiplome(String value) {
		takeStoredValueForKey(value, "temSsDiplome");
	}
	public String temValide() {
		return (String) storedValueForKey("temValide");
	}

	public void setTemValide(String value) {
		takeStoredValueForKey(value, "temValide");
	}
	public String villeDeNaissance() {
		return (String) storedValueForKey("villeDeNaissance");
	}

	public void setVilleDeNaissance(String value) {
		takeStoredValueForKey(value, "villeDeNaissance");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toFwkpers_Civilite() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOCivilite)storedValueForKey("toFwkpers_Civilite");
	}

	public void setToFwkpers_CiviliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCivilite value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOCivilite oldValue = toFwkpers_Civilite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Civilite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Civilite");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement");
	}

	public void setToFwkpers_DepartementRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Naissance() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_Naissance");
	}

	public void setToFwkpers_Pays_NaissanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_Naissance();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_Naissance");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_Naissance");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Nationalite() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_Nationalite");
	}

	public void setToFwkpers_Pays_NationaliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_Nationalite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_Nationalite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_Nationalite");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale toFwkpers_SituationFamiliale() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale)storedValueForKey("toFwkpers_SituationFamiliale");
	}

	public void setToFwkpers_SituationFamilialeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale oldValue = toFwkpers_SituationFamiliale();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_SituationFamiliale");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_SituationFamiliale");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire toFwkpers_SituationMilitaire() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire)storedValueForKey("toFwkpers_SituationMilitaire");
	}

	public void setToFwkpers_SituationMilitaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire oldValue = toFwkpers_SituationMilitaire();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_SituationMilitaire");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_SituationMilitaire");
		}
	}
  
	public NSArray toPrePersonneTelephones() {
		return (NSArray)storedValueForKey("toPrePersonneTelephones");
	}

	public NSArray toPrePersonneTelephones(EOQualifier qualifier) {
		return toPrePersonneTelephones(qualifier, null);
	}

	public NSArray toPrePersonneTelephones(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPrePersonneTelephones();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPrePersonneTelephonesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPrePersonneTelephones");
	}

	public void removeFromToPrePersonneTelephonesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPrePersonneTelephones");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone createToPrePersonneTelephonesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PrePersonneTelephone");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPrePersonneTelephones");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone) eo;
	}

	public void deleteToPrePersonneTelephonesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPrePersonneTelephones");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPrePersonneTelephonesRelationships() {
		Enumeration objects = toPrePersonneTelephones().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPrePersonneTelephonesRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPrePersonneTelephone)objects.nextElement());
		}
	}
	public NSArray toPreRepartAdresses() {
		return (NSArray)storedValueForKey("toPreRepartAdresses");
	}

	public NSArray toPreRepartAdresses(EOQualifier qualifier) {
		return toPreRepartAdresses(qualifier, null);
	}

	public NSArray toPreRepartAdresses(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPreRepartAdresses();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPreRepartAdressesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPreRepartAdresses");
	}

	public void removeFromToPreRepartAdressesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreRepartAdresses");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse createToPreRepartAdressesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PreRepartAdresse");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPreRepartAdresses");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse) eo;
	}

	public void deleteToPreRepartAdressesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPreRepartAdresses");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPreRepartAdressesRelationships() {
		Enumeration objects = toPreRepartAdresses().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPreRepartAdressesRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPreRepartAdresse)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPreIndividu.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPreIndividu.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPreIndividu)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPreIndividu fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreIndividu fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPreIndividu eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPreIndividu)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPreIndividu fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreIndividu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreIndividu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPreIndividu eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPreIndividu)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreIndividu fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreIndividu fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPreIndividu eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPreIndividu ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPreIndividu createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPreIndividu.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPreIndividu.ENTITY_NAME + "' !");
		}
		else {
			EOPreIndividu object = (EOPreIndividu) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPreIndividu localInstanceOfObject(EOEditingContext ec, EOPreIndividu object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPreIndividu " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPreIndividu) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
