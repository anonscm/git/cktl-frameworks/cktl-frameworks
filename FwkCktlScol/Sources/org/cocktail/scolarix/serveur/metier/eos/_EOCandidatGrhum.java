/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCandidatGrhum.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOCandidatGrhum extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_CandidatGrhum";
	public static final String ENTITY_TABLE_NAME = "ADMISSION.CANDIDAT_GRHUM";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "candNumero";

	public static final ERXKey<String> CAND_ADR1_PARENT = new ERXKey<String>("candAdr1Parent");
	public static final ERXKey<String> CAND_ADR1_SCOL = new ERXKey<String>("candAdr1Scol");
	public static final ERXKey<String> CAND_ADR2_PARENT = new ERXKey<String>("candAdr2Parent");
	public static final ERXKey<String> CAND_ADR2_SCOL = new ERXKey<String>("candAdr2Scol");
	public static final ERXKey<Integer> CAND_ANBAC = new ERXKey<Integer>("candAnbac");
	public static final ERXKey<Integer> CAND_ANNEE_DER_ETAB = new ERXKey<Integer>("candAnneeDerEtab");
	public static final ERXKey<Integer> CAND_ANNEE_SCOL = new ERXKey<Integer>("candAnneeScol");
	public static final ERXKey<Integer> CAND_ANNEE_SUIVIE = new ERXKey<Integer>("candAnneeSuivie");
	public static final ERXKey<String> CAND_BEA = new ERXKey<String>("candBea");
	public static final ERXKey<String> CAND_COMNAIS = new ERXKey<String>("candComnais");
	public static final ERXKey<String> CAND_CP_PARENT = new ERXKey<String>("candCpParent");
	public static final ERXKey<String> CAND_CP_SCOL = new ERXKey<String>("candCpScol");
	public static final ERXKey<NSTimestamp> CAND_DATE_ADM = new ERXKey<NSTimestamp>("candDateAdm");
	public static final ERXKey<NSTimestamp> CAND_DATE_NAIS = new ERXKey<NSTimestamp>("candDateNais");
	public static final ERXKey<String> CAND_EMAIL_SCOL = new ERXKey<String>("candEmailScol");
	public static final ERXKey<String> CAND_ENS_DER_ETAB = new ERXKey<String>("candEnsDerEtab");
	public static final ERXKey<Integer> CAND_ENTRETIEN = new ERXKey<Integer>("candEntretien");
	public static final ERXKey<String> CAND_LIBELLE_DER_ETAB = new ERXKey<String>("candLibelleDerEtab");
	public static final ERXKey<Integer> CAND_LISTSUP = new ERXKey<Integer>("candListsup");
	public static final ERXKey<String> CAND_NOM = new ERXKey<String>("candNom");
	public static final ERXKey<String> CAND_NOM_MARITAL = new ERXKey<String>("candNomMarital");
	public static final ERXKey<Integer> CAND_NUM_ADM = new ERXKey<Integer>("candNumAdm");
	public static final ERXKey<String> CAND_PORTABLE_SCOL = new ERXKey<String>("candPortableScol");
	public static final ERXKey<String> CAND_PRENOM = new ERXKey<String>("candPrenom");
	public static final ERXKey<String> CAND_PRENOM2 = new ERXKey<String>("candPrenom2");
	public static final ERXKey<String> CAND_REMARQUES = new ERXKey<String>("candRemarques");
	public static final ERXKey<String> CAND_SALARIE = new ERXKey<String>("candSalarie");
	public static final ERXKey<String> CAND_SALARIE_LIBELLE = new ERXKey<String>("candSalarieLibelle");
	public static final ERXKey<String> CAND_SALARIE_RNE = new ERXKey<String>("candSalarieRne");
	public static final ERXKey<String> CAND_TEL_PARENT = new ERXKey<String>("candTelParent");
	public static final ERXKey<String> CAND_TEL_SCOL = new ERXKey<String>("candTelScol");
	public static final ERXKey<String> CAND_VILLE_BAC = new ERXKey<String>("candVilleBac");
	public static final ERXKey<String> CAND_VILLE_DER_ETAB = new ERXKey<String>("candVilleDerEtab");
	public static final ERXKey<String> CAND_VILLE_PARENT = new ERXKey<String>("candVilleParent");
	public static final ERXKey<String> CAND_VILLE_SCOL = new ERXKey<String>("candVilleScol");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<String> OCAPI_NUMERO = new ERXKey<String>("ocapiNumero");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");

	public static final String CAND_ADR1_PARENT_KEY = "candAdr1Parent";
	public static final String CAND_ADR1_SCOL_KEY = "candAdr1Scol";
	public static final String CAND_ADR2_PARENT_KEY = "candAdr2Parent";
	public static final String CAND_ADR2_SCOL_KEY = "candAdr2Scol";
	public static final String CAND_ANBAC_KEY = "candAnbac";
	public static final String CAND_ANNEE_DER_ETAB_KEY = "candAnneeDerEtab";
	public static final String CAND_ANNEE_SCOL_KEY = "candAnneeScol";
	public static final String CAND_ANNEE_SUIVIE_KEY = "candAnneeSuivie";
	public static final String CAND_BEA_KEY = "candBea";
	public static final String CAND_COMNAIS_KEY = "candComnais";
	public static final String CAND_CP_PARENT_KEY = "candCpParent";
	public static final String CAND_CP_SCOL_KEY = "candCpScol";
	public static final String CAND_DATE_ADM_KEY = "candDateAdm";
	public static final String CAND_DATE_NAIS_KEY = "candDateNais";
	public static final String CAND_EMAIL_SCOL_KEY = "candEmailScol";
	public static final String CAND_ENS_DER_ETAB_KEY = "candEnsDerEtab";
	public static final String CAND_ENTRETIEN_KEY = "candEntretien";
	public static final String CAND_LIBELLE_DER_ETAB_KEY = "candLibelleDerEtab";
	public static final String CAND_LISTSUP_KEY = "candListsup";
	public static final String CAND_NOM_KEY = "candNom";
	public static final String CAND_NOM_MARITAL_KEY = "candNomMarital";
	public static final String CAND_NUM_ADM_KEY = "candNumAdm";
	public static final String CAND_PORTABLE_SCOL_KEY = "candPortableScol";
	public static final String CAND_PRENOM_KEY = "candPrenom";
	public static final String CAND_PRENOM2_KEY = "candPrenom2";
	public static final String CAND_REMARQUES_KEY = "candRemarques";
	public static final String CAND_SALARIE_KEY = "candSalarie";
	public static final String CAND_SALARIE_LIBELLE_KEY = "candSalarieLibelle";
	public static final String CAND_SALARIE_RNE_KEY = "candSalarieRne";
	public static final String CAND_TEL_PARENT_KEY = "candTelParent";
	public static final String CAND_TEL_SCOL_KEY = "candTelScol";
	public static final String CAND_VILLE_BAC_KEY = "candVilleBac";
	public static final String CAND_VILLE_DER_ETAB_KEY = "candVilleDerEtab";
	public static final String CAND_VILLE_PARENT_KEY = "candVilleParent";
	public static final String CAND_VILLE_SCOL_KEY = "candVilleScol";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String OCAPI_NUMERO_KEY = "ocapiNumero";
	public static final String PERS_ID_KEY = "persId";

	// Non visible attributes
	public static final String BAC_CODE_KEY = "bacCode";
	public static final String CAND_CIVILITE_KEY = "candCivilite";
	public static final String CAND_FADM_KEY_KEY = "candFadmKey";
	public static final String CAND_FSPN_KEY_KEY = "candFspnKey";
	public static final String CAND_NUMERO_KEY = "candNumero";
	public static final String CAND_PAYS_PARENT_KEY = "candPaysParent";
	public static final String CAND_PAYS_SCOL_KEY = "candPaysScol";
	public static final String CAND_SITFAM_KEY = "candSitfam";
	public static final String DPRE_CODE_KEY = "dpreCode";
	public static final String DPTG_CODE_DER_ETAB_KEY = "dptgCodeDerEtab";
	public static final String DPTG_CODE_NAIS_KEY = "dptgCodeNais";
	public static final String DPTG_CODE_PARENT_KEY = "dptgCodeParent";
	public static final String DPTG_ETAB_BAC_KEY = "dptgEtabBac";
	public static final String ETAB_CODE_BAC_KEY = "etabCodeBac";
	public static final String ETAB_CODE_DER_ETAB_KEY = "etabCodeDerEtab";
	public static final String MENT_CODE_KEY = "mentCode";
	public static final String NAT_ORDRE_KEY = "natOrdre";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PAYS_CODE_DER_ETAB_KEY = "paysCodeDerEtab";
	public static final String PAYS_CODE_NAIS_KEY = "paysCodeNais";
	public static final String PAYS_CODE_NAT_KEY = "paysCodeNat";
	public static final String PAYS_ETAB_BAC_KEY = "paysEtabBac";
	public static final String RES_CODE_KEY = "resCode";
	public static final String SITU_CODE_KEY = "situCode";
	public static final String TCPRE_CODE_KEY = "tcpreCode";

	// Colkeys
	public static final String CAND_ADR1_PARENT_COLKEY = "CAND_ADR1_PARENT";
	public static final String CAND_ADR1_SCOL_COLKEY = "CAND_ADR1_SCOL";
	public static final String CAND_ADR2_PARENT_COLKEY = "CAND_ADR2_PARENT";
	public static final String CAND_ADR2_SCOL_COLKEY = "CAND_ADR2_SCOL";
	public static final String CAND_ANBAC_COLKEY = "CAND_ANBAC";
	public static final String CAND_ANNEE_DER_ETAB_COLKEY = "CAND_ANNEE_DER_ETAB";
	public static final String CAND_ANNEE_SCOL_COLKEY = "CAND_ANNEE_SCOL";
	public static final String CAND_ANNEE_SUIVIE_COLKEY = "CAND_ANNEE_SUIVIE";
	public static final String CAND_BEA_COLKEY = "CAND_BEA";
	public static final String CAND_COMNAIS_COLKEY = "CAND_COMNAIS";
	public static final String CAND_CP_PARENT_COLKEY = "CAND_CP_PARENT";
	public static final String CAND_CP_SCOL_COLKEY = "CAND_CP_SCOL";
	public static final String CAND_DATE_ADM_COLKEY = "CAND_DATE_ADM";
	public static final String CAND_DATE_NAIS_COLKEY = "CAND_DATE_NAIS";
	public static final String CAND_EMAIL_SCOL_COLKEY = "CAND_EMAIL_SCOL";
	public static final String CAND_ENS_DER_ETAB_COLKEY = "CAND_ENS_DER_ETAB";
	public static final String CAND_ENTRETIEN_COLKEY = "CAND_ENTRETIEN";
	public static final String CAND_LIBELLE_DER_ETAB_COLKEY = "CAND_LIBELLE_DER_ETAB";
	public static final String CAND_LISTSUP_COLKEY = "CAND_LISTSUP";
	public static final String CAND_NOM_COLKEY = "CAND_NOM";
	public static final String CAND_NOM_MARITAL_COLKEY = "CAND_NOM_MARITAL";
	public static final String CAND_NUM_ADM_COLKEY = "CAND_NUM_ADM";
	public static final String CAND_PORTABLE_SCOL_COLKEY = "CAND_PORTABLE_SCOL";
	public static final String CAND_PRENOM_COLKEY = "CAND_PRENOM";
	public static final String CAND_PRENOM2_COLKEY = "CAND_PRENOM2";
	public static final String CAND_REMARQUES_COLKEY = "CAND_REMARQUES";
	public static final String CAND_SALARIE_COLKEY = "CAND_SALARIE";
	public static final String CAND_SALARIE_LIBELLE_COLKEY = "CAND_SALARIE_LIBELLE";
	public static final String CAND_SALARIE_RNE_COLKEY = "CAND_SALARIE_RNE";
	public static final String CAND_TEL_PARENT_COLKEY = "CAND_TEL_PARENT";
	public static final String CAND_TEL_SCOL_COLKEY = "CAND_TEL_SCOL";
	public static final String CAND_VILLE_BAC_COLKEY = "CAND_VILLE_BAC";
	public static final String CAND_VILLE_DER_ETAB_COLKEY = "CAND_VILLE_DER_ETAB";
	public static final String CAND_VILLE_PARENT_COLKEY = "CAND_VILLE_PARENT";
	public static final String CAND_VILLE_SCOL_COLKEY = "CAND_VILLE_SCOL";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String OCAPI_NUMERO_COLKEY = "OCAPI_NUMERO";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	// Non visible colkeys
	public static final String BAC_CODE_COLKEY = "BAC_CODE";
	public static final String CAND_CIVILITE_COLKEY = "CAND_CIVILITE";
	public static final String CAND_FADM_KEY_COLKEY = "CAND_FADM_KEY";
	public static final String CAND_FSPN_KEY_COLKEY = "CAND_FSPN_KEY";
	public static final String CAND_NUMERO_COLKEY = "CAND_NUMERO";
	public static final String CAND_PAYS_PARENT_COLKEY = "CAND_PAYS_PARENT";
	public static final String CAND_PAYS_SCOL_COLKEY = "CAND_PAYS_SCOL";
	public static final String CAND_SITFAM_COLKEY = "CAND_SITFAM";
	public static final String DPRE_CODE_COLKEY = "DPRE_CODE";
	public static final String DPTG_CODE_DER_ETAB_COLKEY = "DPTG_CODE_DER_ETAB";
	public static final String DPTG_CODE_NAIS_COLKEY = "DPTG_CODE_NAIS";
	public static final String DPTG_CODE_PARENT_COLKEY = "DPTG_CODE_PARENT";
	public static final String DPTG_ETAB_BAC_COLKEY = "DPTG_ETAB_BAC";
	public static final String ETAB_CODE_BAC_COLKEY = "ETAB_CODE_BAC";
	public static final String ETAB_CODE_DER_ETAB_COLKEY = "ETAB_CODE_DER_ETAB";
	public static final String MENT_CODE_COLKEY = "MENT_CODE";
	public static final String NAT_ORDRE_COLKEY = "NAT_ORDRE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PAYS_CODE_DER_ETAB_COLKEY = "PAYS_CODE_DER_ETAB";
	public static final String PAYS_CODE_NAIS_COLKEY = "PAYS_CODE_NAIS";
	public static final String PAYS_CODE_NAT_COLKEY = "PAYS_CODE_NAT";
	public static final String PAYS_ETAB_BAC_COLKEY = "PAYS_ETAB_BAC";
	public static final String RES_CODE_COLKEY = "RES_CODE";
	public static final String SITU_CODE_COLKEY = "SITU_CODE";
	public static final String TCPRE_CODE_COLKEY = "TCPRE_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBac> TO_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOBac>("toBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODiplomePrepare> TO_DIPLOME_PREPARE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODiplomePrepare>("toDiplomePrepare");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite> TO_FWKPERS__CIVILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite>("toFwkpers_Civilite");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__DER_ETAB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_DerEtab");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_EtabBac");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__NAISSANCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_Naissance");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_FWKPERS__DEPARTEMENT__PARENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toFwkpers_Departement_Parent");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_FWKPERS__INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toFwkpers_Individu");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__ADR_PARENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_AdrParent");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__ADR_SCOL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_AdrScol");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__DER_ETAB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_DerEtab");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_EtabBac");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__NAISSANCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_Naissance");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS__NATIONALITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays_Nationalite");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAdmission> TO_FWK_SCOLARITE__SCOL_FORMATION_ADMISSION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAdmission>("toFwkScolarite_ScolFormationAdmission");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMention> TO_MENTION_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMention>("toMentionBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm> TO_PAIEMENT_ADMS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm>("toPaiementAdms");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOResultat> TO_RESULTAT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOResultat>("toResultat");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_CODE_BAC = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneCodeBac");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE_DER_ETAB = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRneDerEtab");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant> TO_SIT_FAM_ETUDIANT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant>("toSitFamEtudiant");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOSituationScol> TO_SITUATION_SCOL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOSituationScol>("toSituationScol");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeClassePreparatoire> TO_TYPE_CLASSE_PREPARATOIRE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeClassePreparatoire>("toTypeClassePreparatoire");

	public static final String TO_BAC_KEY = "toBac";
	public static final String TO_DIPLOME_PREPARE_KEY = "toDiplomePrepare";
	public static final String TO_FWKPERS__CIVILITE_KEY = "toFwkpers_Civilite";
	public static final String TO_FWKPERS__DEPARTEMENT__DER_ETAB_KEY = "toFwkpers_Departement_DerEtab";
	public static final String TO_FWKPERS__DEPARTEMENT__ETAB_BAC_KEY = "toFwkpers_Departement_EtabBac";
	public static final String TO_FWKPERS__DEPARTEMENT__NAISSANCE_KEY = "toFwkpers_Departement_Naissance";
	public static final String TO_FWKPERS__DEPARTEMENT__PARENT_KEY = "toFwkpers_Departement_Parent";
	public static final String TO_FWKPERS__INDIVIDU_KEY = "toFwkpers_Individu";
	public static final String TO_FWKPERS__PAYS__ADR_PARENT_KEY = "toFwkpers_Pays_AdrParent";
	public static final String TO_FWKPERS__PAYS__ADR_SCOL_KEY = "toFwkpers_Pays_AdrScol";
	public static final String TO_FWKPERS__PAYS__DER_ETAB_KEY = "toFwkpers_Pays_DerEtab";
	public static final String TO_FWKPERS__PAYS__ETAB_BAC_KEY = "toFwkpers_Pays_EtabBac";
	public static final String TO_FWKPERS__PAYS__NAISSANCE_KEY = "toFwkpers_Pays_Naissance";
	public static final String TO_FWKPERS__PAYS__NATIONALITE_KEY = "toFwkpers_Pays_Nationalite";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ADMISSION_KEY = "toFwkScolarite_ScolFormationAdmission";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";
	public static final String TO_MENTION_BAC_KEY = "toMentionBac";
	public static final String TO_PAIEMENT_ADMS_KEY = "toPaiementAdms";
	public static final String TO_RESULTAT_KEY = "toResultat";
	public static final String TO_RNE_CODE_BAC_KEY = "toRneCodeBac";
	public static final String TO_RNE_DER_ETAB_KEY = "toRneDerEtab";
	public static final String TO_SIT_FAM_ETUDIANT_KEY = "toSitFamEtudiant";
	public static final String TO_SITUATION_SCOL_KEY = "toSituationScol";
	public static final String TO_TYPE_CLASSE_PREPARATOIRE_KEY = "toTypeClassePreparatoire";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOCandidatGrhum with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param candAnneeScol
	 * @param candDateAdm
	 * @param candDateNais
	 * @param candNumAdm
	 * @param candSalarie
	 * @param toBac
	 * @param toFwkpers_Pays_Naissance
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @param toSitFamEtudiant
	 * @return EOCandidatGrhum
	 */
	public static EOCandidatGrhum create(EOEditingContext editingContext, Integer candAnneeScol, NSTimestamp candDateAdm, NSTimestamp candDateNais, Integer candNumAdm, String candSalarie, org.cocktail.scolarix.serveur.metier.eos.EOBac toBac, org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Naissance, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation, org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant toSitFamEtudiant) {
		EOCandidatGrhum eo = (EOCandidatGrhum) createAndInsertInstance(editingContext);
		eo.setCandAnneeScol(candAnneeScol);
		eo.setCandDateAdm(candDateAdm);
		eo.setCandDateNais(candDateNais);
		eo.setCandNumAdm(candNumAdm);
		eo.setCandSalarie(candSalarie);
		eo.setToBacRelationship(toBac);
		eo.setToFwkpers_Pays_NaissanceRelationship(toFwkpers_Pays_Naissance);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		eo.setToSitFamEtudiantRelationship(toSitFamEtudiant);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOCandidatGrhum.
	 *
	 * @param editingContext
	 * @return EOCandidatGrhum
	 */
	public static EOCandidatGrhum create(EOEditingContext editingContext) {
		EOCandidatGrhum eo = (EOCandidatGrhum) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOCandidatGrhum localInstanceIn(EOEditingContext editingContext) {
		EOCandidatGrhum localInstance = (EOCandidatGrhum) localInstanceOfObject(editingContext, (EOCandidatGrhum) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOCandidatGrhum localInstanceIn(EOEditingContext editingContext, EOCandidatGrhum eo) {
		EOCandidatGrhum localInstance = (eo == null) ? null : (EOCandidatGrhum) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String candAdr1Parent() {
		return (String) storedValueForKey("candAdr1Parent");
	}

	public void setCandAdr1Parent(String value) {
		takeStoredValueForKey(value, "candAdr1Parent");
	}
	public String candAdr1Scol() {
		return (String) storedValueForKey("candAdr1Scol");
	}

	public void setCandAdr1Scol(String value) {
		takeStoredValueForKey(value, "candAdr1Scol");
	}
	public String candAdr2Parent() {
		return (String) storedValueForKey("candAdr2Parent");
	}

	public void setCandAdr2Parent(String value) {
		takeStoredValueForKey(value, "candAdr2Parent");
	}
	public String candAdr2Scol() {
		return (String) storedValueForKey("candAdr2Scol");
	}

	public void setCandAdr2Scol(String value) {
		takeStoredValueForKey(value, "candAdr2Scol");
	}
	public Integer candAnbac() {
		return (Integer) storedValueForKey("candAnbac");
	}

	public void setCandAnbac(Integer value) {
		takeStoredValueForKey(value, "candAnbac");
	}
	public Integer candAnneeDerEtab() {
		return (Integer) storedValueForKey("candAnneeDerEtab");
	}

	public void setCandAnneeDerEtab(Integer value) {
		takeStoredValueForKey(value, "candAnneeDerEtab");
	}
	public Integer candAnneeScol() {
		return (Integer) storedValueForKey("candAnneeScol");
	}

	public void setCandAnneeScol(Integer value) {
		takeStoredValueForKey(value, "candAnneeScol");
	}
	public Integer candAnneeSuivie() {
		return (Integer) storedValueForKey("candAnneeSuivie");
	}

	public void setCandAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "candAnneeSuivie");
	}
	public String candBea() {
		return (String) storedValueForKey("candBea");
	}

	public void setCandBea(String value) {
		takeStoredValueForKey(value, "candBea");
	}
	public String candComnais() {
		return (String) storedValueForKey("candComnais");
	}

	public void setCandComnais(String value) {
		takeStoredValueForKey(value, "candComnais");
	}
	public String candCpParent() {
		return (String) storedValueForKey("candCpParent");
	}

	public void setCandCpParent(String value) {
		takeStoredValueForKey(value, "candCpParent");
	}
	public String candCpScol() {
		return (String) storedValueForKey("candCpScol");
	}

	public void setCandCpScol(String value) {
		takeStoredValueForKey(value, "candCpScol");
	}
	public NSTimestamp candDateAdm() {
		return (NSTimestamp) storedValueForKey("candDateAdm");
	}

	public void setCandDateAdm(NSTimestamp value) {
		takeStoredValueForKey(value, "candDateAdm");
	}
	public NSTimestamp candDateNais() {
		return (NSTimestamp) storedValueForKey("candDateNais");
	}

	public void setCandDateNais(NSTimestamp value) {
		takeStoredValueForKey(value, "candDateNais");
	}
	public String candEmailScol() {
		return (String) storedValueForKey("candEmailScol");
	}

	public void setCandEmailScol(String value) {
		takeStoredValueForKey(value, "candEmailScol");
	}
	public String candEnsDerEtab() {
		return (String) storedValueForKey("candEnsDerEtab");
	}

	public void setCandEnsDerEtab(String value) {
		takeStoredValueForKey(value, "candEnsDerEtab");
	}
	public Integer candEntretien() {
		return (Integer) storedValueForKey("candEntretien");
	}

	public void setCandEntretien(Integer value) {
		takeStoredValueForKey(value, "candEntretien");
	}
	public String candLibelleDerEtab() {
		return (String) storedValueForKey("candLibelleDerEtab");
	}

	public void setCandLibelleDerEtab(String value) {
		takeStoredValueForKey(value, "candLibelleDerEtab");
	}
	public Integer candListsup() {
		return (Integer) storedValueForKey("candListsup");
	}

	public void setCandListsup(Integer value) {
		takeStoredValueForKey(value, "candListsup");
	}
	public String candNom() {
		return (String) storedValueForKey("candNom");
	}

	public void setCandNom(String value) {
		takeStoredValueForKey(value, "candNom");
	}
	public String candNomMarital() {
		return (String) storedValueForKey("candNomMarital");
	}

	public void setCandNomMarital(String value) {
		takeStoredValueForKey(value, "candNomMarital");
	}
	public Integer candNumAdm() {
		return (Integer) storedValueForKey("candNumAdm");
	}

	public void setCandNumAdm(Integer value) {
		takeStoredValueForKey(value, "candNumAdm");
	}
	public String candPortableScol() {
		return (String) storedValueForKey("candPortableScol");
	}

	public void setCandPortableScol(String value) {
		takeStoredValueForKey(value, "candPortableScol");
	}
	public String candPrenom() {
		return (String) storedValueForKey("candPrenom");
	}

	public void setCandPrenom(String value) {
		takeStoredValueForKey(value, "candPrenom");
	}
	public String candPrenom2() {
		return (String) storedValueForKey("candPrenom2");
	}

	public void setCandPrenom2(String value) {
		takeStoredValueForKey(value, "candPrenom2");
	}
	public String candRemarques() {
		return (String) storedValueForKey("candRemarques");
	}

	public void setCandRemarques(String value) {
		takeStoredValueForKey(value, "candRemarques");
	}
	public String candSalarie() {
		return (String) storedValueForKey("candSalarie");
	}

	public void setCandSalarie(String value) {
		takeStoredValueForKey(value, "candSalarie");
	}
	public String candSalarieLibelle() {
		return (String) storedValueForKey("candSalarieLibelle");
	}

	public void setCandSalarieLibelle(String value) {
		takeStoredValueForKey(value, "candSalarieLibelle");
	}
	public String candSalarieRne() {
		return (String) storedValueForKey("candSalarieRne");
	}

	public void setCandSalarieRne(String value) {
		takeStoredValueForKey(value, "candSalarieRne");
	}
	public String candTelParent() {
		return (String) storedValueForKey("candTelParent");
	}

	public void setCandTelParent(String value) {
		takeStoredValueForKey(value, "candTelParent");
	}
	public String candTelScol() {
		return (String) storedValueForKey("candTelScol");
	}

	public void setCandTelScol(String value) {
		takeStoredValueForKey(value, "candTelScol");
	}
	public String candVilleBac() {
		return (String) storedValueForKey("candVilleBac");
	}

	public void setCandVilleBac(String value) {
		takeStoredValueForKey(value, "candVilleBac");
	}
	public String candVilleDerEtab() {
		return (String) storedValueForKey("candVilleDerEtab");
	}

	public void setCandVilleDerEtab(String value) {
		takeStoredValueForKey(value, "candVilleDerEtab");
	}
	public String candVilleParent() {
		return (String) storedValueForKey("candVilleParent");
	}

	public void setCandVilleParent(String value) {
		takeStoredValueForKey(value, "candVilleParent");
	}
	public String candVilleScol() {
		return (String) storedValueForKey("candVilleScol");
	}

	public void setCandVilleScol(String value) {
		takeStoredValueForKey(value, "candVilleScol");
	}
	public Integer etudNumero() {
		return (Integer) storedValueForKey("etudNumero");
	}

	public void setEtudNumero(Integer value) {
		takeStoredValueForKey(value, "etudNumero");
	}
	public String ocapiNumero() {
		return (String) storedValueForKey("ocapiNumero");
	}

	public void setOcapiNumero(String value) {
		takeStoredValueForKey(value, "ocapiNumero");
	}
	public Integer persId() {
		return (Integer) storedValueForKey("persId");
	}

	public void setPersId(Integer value) {
		takeStoredValueForKey(value, "persId");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOBac toBac() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOBac)storedValueForKey("toBac");
	}

	public void setToBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EOBac value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOBac oldValue = toBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toBac");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EODiplomePrepare toDiplomePrepare() {
		return (org.cocktail.scolarix.serveur.metier.eos.EODiplomePrepare)storedValueForKey("toDiplomePrepare");
	}

	public void setToDiplomePrepareRelationship(org.cocktail.scolarix.serveur.metier.eos.EODiplomePrepare value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EODiplomePrepare oldValue = toDiplomePrepare();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toDiplomePrepare");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toDiplomePrepare");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toFwkpers_Civilite() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOCivilite)storedValueForKey("toFwkpers_Civilite");
	}

	public void setToFwkpers_CiviliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCivilite value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOCivilite oldValue = toFwkpers_Civilite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Civilite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Civilite");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_DerEtab() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_DerEtab");
	}

	public void setToFwkpers_Departement_DerEtabRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_DerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_DerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_DerEtab");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_EtabBac() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_EtabBac");
	}

	public void setToFwkpers_Departement_EtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_EtabBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_EtabBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_EtabBac");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_Naissance() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_Naissance");
	}

	public void setToFwkpers_Departement_NaissanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_Naissance();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_Naissance");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_Naissance");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toFwkpers_Departement_Parent() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey("toFwkpers_Departement_Parent");
	}

	public void setToFwkpers_Departement_ParentRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toFwkpers_Departement_Parent();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Departement_Parent");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Departement_Parent");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toFwkpers_Individu");
	}

	public void setToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toFwkpers_Individu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Individu");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Individu");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_AdrParent() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_AdrParent");
	}

	public void setToFwkpers_Pays_AdrParentRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_AdrParent();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_AdrParent");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_AdrParent");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_AdrScol() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_AdrScol");
	}

	public void setToFwkpers_Pays_AdrScolRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_AdrScol();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_AdrScol");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_AdrScol");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_DerEtab() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_DerEtab");
	}

	public void setToFwkpers_Pays_DerEtabRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_DerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_DerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_DerEtab");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_EtabBac() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_EtabBac");
	}

	public void setToFwkpers_Pays_EtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_EtabBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_EtabBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_EtabBac");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Naissance() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_Naissance");
	}

	public void setToFwkpers_Pays_NaissanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_Naissance();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_Naissance");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_Naissance");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays_Nationalite() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays_Nationalite");
	}

	public void setToFwkpers_Pays_NationaliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays_Nationalite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays_Nationalite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays_Nationalite");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAdmission toFwkScolarite_ScolFormationAdmission() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAdmission)storedValueForKey("toFwkScolarite_ScolFormationAdmission");
	}

	public void setToFwkScolarite_ScolFormationAdmissionRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAdmission value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAdmission oldValue = toFwkScolarite_ScolFormationAdmission();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAdmission");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAdmission");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOMention toMentionBac() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOMention)storedValueForKey("toMentionBac");
	}

	public void setToMentionBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EOMention value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOMention oldValue = toMentionBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMentionBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toMentionBac");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOResultat toResultat() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOResultat)storedValueForKey("toResultat");
	}

	public void setToResultatRelationship(org.cocktail.scolarix.serveur.metier.eos.EOResultat value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOResultat oldValue = toResultat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toResultat");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toResultat");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneCodeBac() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneCodeBac");
	}

	public void setToRneCodeBacRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneCodeBac();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneCodeBac");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneCodeBac");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EORne toRneDerEtab() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRneDerEtab");
	}

	public void setToRneDerEtabRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRneDerEtab();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRneDerEtab");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRneDerEtab");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant toSitFamEtudiant() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant)storedValueForKey("toSitFamEtudiant");
	}

	public void setToSitFamEtudiantRelationship(org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOSitFamEtudiant oldValue = toSitFamEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSitFamEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toSitFamEtudiant");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOSituationScol toSituationScol() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOSituationScol)storedValueForKey("toSituationScol");
	}

	public void setToSituationScolRelationship(org.cocktail.scolarix.serveur.metier.eos.EOSituationScol value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOSituationScol oldValue = toSituationScol();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSituationScol");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toSituationScol");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOTypeClassePreparatoire toTypeClassePreparatoire() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeClassePreparatoire)storedValueForKey("toTypeClassePreparatoire");
	}

	public void setToTypeClassePreparatoireRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeClassePreparatoire value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeClassePreparatoire oldValue = toTypeClassePreparatoire();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeClassePreparatoire");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toTypeClassePreparatoire");
		}
	}
  
	public NSArray toPaiementAdms() {
		return (NSArray)storedValueForKey("toPaiementAdms");
	}

	public NSArray toPaiementAdms(EOQualifier qualifier) {
		return toPaiementAdms(qualifier, null);
	}

	public NSArray toPaiementAdms(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toPaiementAdms();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToPaiementAdmsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toPaiementAdms");
	}

	public void removeFromToPaiementAdmsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPaiementAdms");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm createToPaiementAdmsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_PaiementAdm");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toPaiementAdms");
		return (org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm) eo;
	}

	public void deleteToPaiementAdmsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toPaiementAdms");
				editingContext().deleteObject(object);
			}

	public void deleteAllToPaiementAdmsRelationships() {
		Enumeration objects = toPaiementAdms().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToPaiementAdmsRelationship((org.cocktail.scolarix.serveur.metier.eos.EOPaiementAdm)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOCandidatGrhum.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOCandidatGrhum.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOCandidatGrhum)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOCandidatGrhum fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCandidatGrhum fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOCandidatGrhum eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOCandidatGrhum)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOCandidatGrhum fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOCandidatGrhum fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOCandidatGrhum fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOCandidatGrhum eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOCandidatGrhum)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOCandidatGrhum fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOCandidatGrhum fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOCandidatGrhum eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOCandidatGrhum ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOCandidatGrhum createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOCandidatGrhum.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOCandidatGrhum.ENTITY_NAME + "' !");
		}
		else {
			EOCandidatGrhum object = (EOCandidatGrhum) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOCandidatGrhum localInstanceOfObject(EOEditingContext ec, EOCandidatGrhum object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOCandidatGrhum " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOCandidatGrhum) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
