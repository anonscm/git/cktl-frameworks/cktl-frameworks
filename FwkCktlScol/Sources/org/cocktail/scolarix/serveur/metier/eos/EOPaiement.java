/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.metier.eos;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.List;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOPaiement extends _EOPaiement {

	private static final List<String> RCPT_CODE_CHEQUES = new NSArray<String>(new String[] { "CCP", "TG", "CBE", "CBD", "CB", "CEU" });

	public EOPaiement() {
		super();
	}

	public static EOPaiement create(EOEditingContext ec, EOHistorique historique, NSArray<EODroitsUniversite> droitsUniversite) {
		EOPaiement p = create(ec, new NSTimestamp(), null, null, null, null);
		p.setToHistoriqueRelationship(historique);
		p.setPaieTransaction("N"); // FIXME
		if (droitsUniversite != null) {
			Enumeration<EODroitsUniversite> e = droitsUniversite.objectEnumerator();
			while (e.hasMoreElements()) {
				EODroitsUniversite du = e.nextElement();
				EODetailPaiement dp = p.createToDetailPaiementsRelationship();
				dp.setDpaieDateOperation(p.paieDate());
				dp.setDpaieMontant(du.dunivMontantLocal());
				dp.setDpaieMouvement(new Integer(0)); // plus utilisé
				dp.setErembNumero(null);
				dp.setToVEtablissementScolariteRelationship(historique.inscriptionPrincipale().toFwkScolarite_ScolFormationSpecialisation()
						.toFwkScolarite_ScolFormationDiplome().toFwkScolarix_VEtablissementScolarite()); // FIXME
				dp.setHistAnneeScol(historique.histAnneeScol());
				dp.setToInscDiplRelationship(historique.inscriptionPrincipale()); // FIXME
				dp.setToPaiementRelationship(p);
				dp.setToRepartitionCompteRelationship(du.toRepartitionCompte());
			}
			p.setPaieSomme((BigDecimal) p.toDetailPaiements().valueForKey("@sum." + EODetailPaiement.DPAIE_MONTANT_KEY));
		}
		return p;
	}

	public String getDpaieChaine() {
		// Construction de la chaine des détails paiement qui va bien...
		// Format : idiplnumero$drcptcode$dpaiemontant$ pour chaque detail_paiement
		StringBuffer dpSB = new StringBuffer();
		if (toDetailPaiements() != null) {
			Enumeration<EODetailPaiement> detailPaiements = toDetailPaiements().objectEnumerator();
			while (detailPaiements.hasMoreElements()) {
				EODetailPaiement detailPaiement = detailPaiements.nextElement();
				// idiplnumero
				if (detailPaiement.toInscDipl() != null) {
					NSDictionary<String, Object> dicoPK = EOUtilities.primaryKeyForObject(editingContext(), detailPaiement.toInscDipl());
					if (dicoPK != null) {
						dpSB.append(dicoPK.objectForKey(EOInscDipl.IDIPL_NUMERO_KEY));
					}
				}
				dpSB.append("$");
				// drcptcode
				if (detailPaiement.toRepartitionCompte() != null) {
					dpSB.append(detailPaiement.toRepartitionCompte().rcptCode());
				}
				dpSB.append("$");
				// dpaiemontant
				dpSB.append(detailPaiement.dpaieMontant());
				dpSB.append("$");
			}
		}
		return dpSB.toString();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (toRepartitionCompte() != null && toRepartitionCompte().rcptCode() != null) {
			if (RCPT_CODE_CHEQUES.contains(toRepartitionCompte().rcptCode())) {
				if (toBanque() == null) {
					throw new ValidationException("Pour un paiement par chèque, il faut renseigner une banque.");
				}
				if (paieNumeroCheque() == null) {
					throw new ValidationException("Pour un paiement par chèque, il faut renseigner un numéro de chèque.");
				}
				if (paieNumeroCompte() == null) {
					throw new ValidationException("Pour un paiement par chèque, il faut renseigner un numéro de compte.");
				}
			}
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
