/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolarix.serveur.metier.eos;

import org.cocktail.scolarix.serveur.finder.Finder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOCandidatGrhum extends _EOCandidatGrhum {

	public EOCandidatGrhum() {
		super();
	}

	public static final NSArray<EOCandidatGrhum> getCandidats(EOEditingContext ec, Integer annee, String numeroIne, String nom, String prenom,
			NSTimestamp dateDeNaissance, NSArray<EOSortOrdering> sortOrderings) {
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (annee != null) {
			quals.addObject(ERXQ.equals(EOCandidatGrhum.CAND_ANNEE_SCOL_KEY, annee));
		}
		if (numeroIne != null) {
			quals.addObject(ERXQ.likeInsensitive(EOCandidatGrhum.CAND_BEA_KEY, numeroIne));
		}
		if (nom != null) {
			quals.addObject(ERXQ.likeInsensitive(EOCandidatGrhum.CAND_NOM_KEY, "*" + nom + "*"));
		}
		if (prenom != null) {
			quals.addObject(ERXQ.likeInsensitive(EOCandidatGrhum.CAND_PRENOM_KEY, "*" + prenom + "*"));
		}
		if (dateDeNaissance != null) {
			quals.addObject(Finder.qualifierForDayDate(EOCandidatGrhum.CAND_DATE_NAIS_KEY, dateDeNaissance));
		}
		if (sortOrderings == null) {
			sortOrderings = ERXS.ascs(EOCandidatGrhum.CAND_NOM_KEY, EOCandidatGrhum.CAND_PRENOM_KEY);
		}
		return EOCandidatGrhum.fetchAll(ec, ERXQ.and(quals), sortOrderings);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele a partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
