/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolarix.serveur.metier.eos;

import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolaritefwk.serveur.interfaces.IScolFormationDomaine;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationProgression;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.exception.HistoriqueException;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.factory.FactoryEtudiant;
import org.cocktail.scolarix.serveur.factory.FactoryFormations;
import org.cocktail.scolarix.serveur.factory.FactoryIndividuUlr;
import org.cocktail.scolarix.serveur.finder.FinderCommune;
import org.cocktail.scolarix.serveur.finder.FinderEtudiant;
import org.cocktail.scolarix.serveur.finder.FinderParametre;
import org.cocktail.scolarix.serveur.finder.FinderPreCandidat;
import org.cocktail.scolarix.serveur.finder.FinderPreLangueException;
import org.cocktail.scolarix.serveur.finder.FinderRne;
import org.cocktail.scolarix.serveur.finder.FinderScolFormationHabilitation;
import org.cocktail.scolarix.serveur.finder.FinderSituationScol;
import org.cocktail.scolarix.serveur.finder.FinderTypeDiplome;
import org.cocktail.scolarix.serveur.finder.FinderTypeHebergement;
import org.cocktail.scolarix.serveur.finder.FinderTypeInscription;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.process.ProcessAdmCandidat;
import org.cocktail.scolarix.serveur.process.ProcessInscriptionAdministrative;
import org.cocktail.scolarix.serveur.process.ProcessInscriptionPreAdministrative;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.qualifiers.ERXKeyValueQualifier;

public class EOEtudiant extends _EOEtudiant implements IEtudiant {

	public static final String ETUD_TYPE_PRE_INSCRIPTION = "PI";
	public static final String ETUD_TYPE_PRE_RE_INSCRIPTION = "PRI";
	public static final String ETUD_TYPE_INSCRIPTION = "I";
	public static final String ETUD_TYPE_RE_INSCRIPTION = "RI";
	public static final String ETUD_TYPE_ADMISSION = "A";
	private String etudType;
	private String charte;
	private boolean isPreInscrit;
	private Integer anneeInscriptionEnCours = null;
	private NSArray<EOPreCandidature> preCandidatures = null;
	private EORne rne;

	private NSMutableArray<EOScolFormationHabilitation> formationsEnvisageables = null;
	private NSMutableArray<EOScolFormationHabilitation> formationsPossibles = null;
	private NSMutableArray formationsEnvisagees = null;

	private boolean resetFormationsEnvisagees = false;

	private String messageInterditDeConnexion = null;
	private NSMutableArray<EtudiantException> userInfos = null;

	public EOEtudiant() {
		super();
	}

	public EOEditingContext edc() {
		return editingContext();
	}

	public EOEtudiant etudiant() {
		return this;
	}

	public void reset() {
		setFormationsEnvisageables(null);
		setFormationsPossibles(null);
		setFormationsEnvisagees(null);
	}

	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	@Override
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Recherche d'un etudiant par son login/password <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel on opere
	 * @param login
	 *            OBLIGATOIRE : Identifiant de l'etudiant (caseInsensitive)
	 * @param pwd
	 *            FACULTATIF : Password de l'etudiant (caseSensitive)
	 * @return un EOEtudiant, sinon NULL
	 */
	public static EOEtudiant getEtudiantLogin(EOEditingContext ec, String login, String pwd) {
		if (login == null) {
			return null;
		}
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_LOGIN_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, login));
		if (pwd != null) {
			NSMutableArray<EOQualifier> passwdQuals = new NSMutableArray<EOQualifier>();
			passwdQuals.addObject(new EOKeyValueQualifier(EOCompte.CPT_PASSWD_CLAIR_KEY, EOQualifier.QualifierOperatorEqual, pwd));
			passwdQuals.addObject(new EOKeyValueQualifier(EOCompte.CPT_PASSWD_KEY, EOQualifier.QualifierOperatorEqual, pwd));
			quals.addObject(new EOOrQualifier(passwdQuals));
		}
		EOCompte compte = EOCompte.fetchByQualifier(ec, new EOAndQualifier(quals));
		if (compte == null) {
			return null;
		}
		EOQualifier qual = new EOKeyValueQualifier(EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.TO_COMPTES_KEY,
				EOQualifier.QualifierOperatorEqual, compte);
		return EOEtudiant.fetchByQualifier(ec, qual);
	}

	/**
	 * Valide la presence et la coherence des donnees.<BR>
	 * Peut etre appele a partir des factories.
	 * 
	 * @throws EtudiantException
	 */
	public void validateObjectMetier() throws EtudiantException {
		// champs obligatoires
		if (individu() == null) {
			throw new EtudiantException("L'étudiant n'a pas d'identité (individu)");
		}
		if (individu().toCivilite() == null) {
			throw new EtudiantException("L'étudiant n'a pas de civilité");
		}
		if (individu().nomPatronymiqueAffichage() == null) {
			throw new EtudiantException("Nom patronymique obligatoire");
		}
		if (individu().prenomAffichage() == null) {
			throw new EtudiantException("Prénom obligatoire");
		}
		if (individu().dNaissance() == null) {
			throw new EtudiantException("Date de naissance obligatoire");
		}
		if (individu().villeDeNaissance() == null) {
			throw new EtudiantException("Ville de naissance obligatoire");
		}
		if (individu().toPaysNaissance() == null) {
			throw new EtudiantException("Pays de naissance obligatoire");
		}
		if (individu().toPaysNationalite() == null) {
			throw new EtudiantException("Nationalité obligatoire");
		}
		EOHistorique historique = historique(anneeInscriptionEnCours());
		if (historique == null) {
			throw new EtudiantException("L'étudiant a un problème de dossier (pas d'historique)");
		}

		if (adresseStable() == null) {
			throw new EtudiantException("L'étudiant n'a pas d'adresse stable (parents)");
		}
		if (adresseStable().adrAdresse1() == null) {
			throw new EtudiantException("Première ligne de l'adresse obligatoire");
		}
		if (adresseStable().toPays() == null) {
			throw new EtudiantException("Pays de l'adresse obligatoire");
		}
		if (adresseStable().ville() == null) {
			throw new EtudiantException("Ville de l'adresse obligatoire");
		}
		if (toTypeHebergement() == null || toTypeHebergement().equals(FinderTypeHebergement.getTypeHebergementEnAttente(editingContext())) == false) {
			if (adresseUniversitaire() == null) {
				throw new EtudiantException("L'étudiant n'a pas d'adresse universitaire");
			}
			if (adresseUniversitaire().adrAdresse1() == null) {
				throw new EtudiantException("Première ligne de l'adresse universitaire obligatoire");
			}
			if (adresseUniversitaire().toPays() == null) {
				throw new EtudiantException("Pays de l'adresse universitaire obligatoire");
			}
			if (adresseUniversitaire().ville() == null) {
				throw new EtudiantException("Ville de l'adresse universitaire obligatoire");
			}
		}

		if (formationsEnvisagees() == null || formationsEnvisagees().count() == 0) {
			throw new EtudiantException("Vous devez choisir au moins une formation");
		}

		// champs obligatoires conditionnels
		if (individu().toCivilite().cCivilite().equals(EOCivilite.C_CIVILITE_MADAME) && individu().nomAffichage() == null) {
			throw new EtudiantException("Nom d'usage obligatoire pour les femmes mariées");
		}
		if (individu().toPaysNaissance().isPaysDefaut() && individu().toDepartement() == null) {
			throw new EtudiantException("Département de naissance obligatoire");
		}

		if (adresseStable().toPays().isPaysDefaut() && EOAdresse.isCodePostalObligatoire(edc()) && adresseStable().codePostal() == null) {
			throw new EtudiantException("Code postal obligatoire pour une adresse stable");
		}
		if (toTypeHebergement() == null || toTypeHebergement().equals(FinderTypeHebergement.getTypeHebergementEnAttente(editingContext())) == false) {
			if (adresseUniversitaire().toPays().isPaysDefaut() && EOAdresse.isCodePostalObligatoire(edc())
					&& adresseUniversitaire().codePostal() == null) {
				throw new EtudiantException("Code postal obligatoire pour une adresse universitaire");
			}
		}

		// verifications
		if (individu().dNaissance().after(DateCtrl.now())) {
			throw new EtudiantException("Date de naissance incorrecte");
		}

		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOCommune.ENTITY_NAME, null, null);
		fetchSpec.setFetchLimit(1);
		NSArray<EOCommune> objects = edc().objectsWithFetchSpecification(fetchSpec);
		boolean useCommunes = (objects != null && objects.count() > 0);
		if (adresseStable().toPays().isPaysDefaut() && adresseStable().codePostal() != null && adresseStable().ville() != null && useCommunes) {
			NSArray<EOCommune> communes = FinderCommune.getCommunesExact(adresseStable().editingContext(), adresseStable().codePostal(),
					adresseStable().ville());
			if (communes == null || communes.count() == 0) {
				throw new EtudiantException("Problème sur l'adresse stable, commune inconnue");
			}
			if (communes.count() > 1) {
				throw new EtudiantException("Problème sur l'adresse stable, plusieurs communes correspondent");
			}
		}
		if (toTypeHebergement() == null || toTypeHebergement().equals(FinderTypeHebergement.getTypeHebergementEnAttente(editingContext())) == false) {
			if (adresseUniversitaire().toPays().isPaysDefaut() && adresseUniversitaire().codePostal() != null
					&& adresseUniversitaire().ville() != null && useCommunes) {
				NSArray<EOCommune> communes = FinderCommune.getCommunesExact(adresseUniversitaire().editingContext(), adresseUniversitaire()
						.codePostal(), adresseUniversitaire().ville());
				if (communes == null || communes.count() == 0) {
					throw new EtudiantException("Problème sur l'adresse universitaire, commune inconnue");
				}
				if (communes.count() > 1) {
					throw new EtudiantException("Problème sur l'adresse universitaire, plusieurs communes correspondent");
				}
			}
		}

		if (toBac() != null && toBac().bacType() != null && toBac().bacType().equals(EOBac.BAC_TYPE_EQUIVALENCE)) {
			// Capacite, DAEU A ou DAEU B
			if (toBac().bacCode().equals("0030") || toBac().bacCode().equals("0033") || toBac().bacCode().equals("0034")) {
				if (etudAnbac() == null) {
					throw new EtudiantException("L'année d'obtention du titre d'accès est obligatoire");
				}
				if (toFwkpers_Pays_EtabBac() == null) {
					throw new EtudiantException("Le pays d'obtention du titre d'accès est obligatoire");
				}
				if (etudVilleBac() == null) {
					throw new EtudiantException("La ville d'obtention du titre d'accès est obligatoire");
				}
				if (toFwkpers_Pays_EtabBac() != null && toFwkpers_Pays_EtabBac().isPaysDefaut()) {
					if (toFwkpers_Departement_EtabBac() == null) {
						throw new EtudiantException("Le département d'obtention du titre d'accès est obligatoire");
					}
					if (isPreInscription() && toRneCodeBac() == null) {
						throw new EtudiantException("L'établissement d'obtention du titre d'accès est obligatoire");
					}
				}
			}
			// Titre Etranger admis nationalement en Equivalence
			if (toBac().bacCode().equals("0031")) {
				if (etudAnbac() == null) {
					throw new EtudiantException("L'année d'obtention du titre d'accès est obligatoire.");
				}
				if (isPreInscription() && toFwkpers_Pays_EtabBac() == null) {
					throw new EtudiantException("Le pays d'obtention du titre d'accès est obligatoire.");
				}
				if (toFwkpers_Pays_EtabBac() != null && toFwkpers_Pays_EtabBac().isPaysDefaut()) {
					throw new EtudiantException("Le pays d'obtention de ce titre d'accès ne peut pas être le pays local");
				}
			}
			// Titre Français admis nationalement en Equivalence
			if (toBac().bacCode().equals("0032")) {
				if (etudAnbac() == null) {
					throw new EtudiantException("L'année d'obtention du titre d'accès est obligatoire !");
				}
				if (toFwkpers_Pays_EtabBac() != null && !toFwkpers_Pays_EtabBac().isPaysDefaut()) {
					throw new EtudiantException("Le pays d'obtention de ce titre d'accès ne peut être que le pays local");
				}
			}
			// PROMOTION SOCIALE, VALIDATION DES ACQUIS
			if (toBac().bacCode().equals("0035") || toBac().bacCode().equals("0036")) {
				if (etudAnbac() == null) {
					throw new EtudiantException("L'année d'obtention du titre d'accès est obligatoire...");
				}
			}
			// AUTRES CAS DE NON BACHELIERS
			if (toBac().bacCode().equals("0037")) {
				// pas de controles...
			}
		}
		if (etudAnbac() != null) {
			// PO 2010 - UNC : Attention, modif car les tests précédents ne fonctionnaient
			// pas dans le cas de la préinscription en fin N-1 pour l'année N !!!
			// (on peut avoir son bac en 2009 et se préinscrire en novembre 2009 pour la rentée de février 2010... c'est
			// ça, aussi !)

			boolean anneeCivile = FinderParametre.getAnneeCivile(edc());
			if (anneeCivile == false && etudAnbac().intValue() > anneeInscriptionEnCours().intValue()) {
				throw new EtudiantException("L'année d'obtention du titre d'accès est incorrecte");
			}
			if (anneeCivile == true && etudAnbac().intValue() >= anneeInscriptionEnCours().intValue()) {
				throw new EtudiantException("L'année d'obtention du titre d'accès est incorrecte.");
			}
			if (anneeCivile == false && etudAnbac().intValue() > DateCtrl.nowDay().get(GregorianCalendar.YEAR)) {
				throw new EtudiantException("L'année d'obtention du titre d'accès est incorrecte...");
			}
			// modif UNC -
			// TO DO = pour être plus précis, il faudrait aussi vérifier dans quel cas on se trouve (préinsc), voir quel
			// mois on est, etc...
			// on change >= par >
			if (anneeCivile == true && etudAnbac().intValue() > DateCtrl.nowDay().get(GregorianCalendar.YEAR)) {
				throw new EtudiantException("L'année d'obtention du titre d'accès est incorrecte !");
			}
		}

		if (etudAnnee1InscUniv() == null && toRneCodeSup() != null) {
			throw new EtudiantException(
					"L'année de première inscription dans une université locale et le premier établissement sont à renseigner conjointement");
		}
		if (etudAnnee1InscUniv() != null && toRneCodeSup() == null) {
			throw new EtudiantException(
					"L'année de première inscription dans une université locale et le premier établissement sont à renseigner conjointement.");
		}
		if (etudAnnee1InscUniv() != null && etudAnnee1InscSup().intValue() > etudAnnee1InscUniv().intValue()) {
			throw new EtudiantException(
					"L'année de première inscription dans l'enseignement supérieur est postérieure à l'année de première inscription dans une université locale");
		}
		// PO : A l'UNC, on peut avoir des dossier étudiants en réinscription n'ayant pas de date de 1ere insc° à l'UNC
		// (on l'avait enlevé du dossier - suite à modif SISE)
		if (etudAnnee1InscUniv() != null && etudAnnee1InscUlr() != null && etudAnnee1InscUniv().intValue() > etudAnnee1InscUlr().intValue()) {
			throw new EtudiantException(
					"L'année de première inscription dans une université locale est postérieure à l'année de première inscription dans l'établissement");
		}

		if (historique.histAnneeDerEtab() != null && historique.histAnneeScol() != null
				&& historique.histAnneeDerEtab().intValue() < historique.histAnneeScol().intValue() - 1) {
			if (historique.toSituationScol() != null && !historique.toSituationScol().situCode().equals(EOSituationScol.SITU_CODE_JAMAIS_SCOLARISE)
					&& !historique.toSituationScol().situCode().equals(EOSituationScol.SITU_CODE_NON_SCOLARISE)) {
				throw new EtudiantException(
						"La situation scolaire de l'année précédente est incompatible avec l'année de fréquentation du dernier établissement");
			}
		}

	}

	/**
	 * Fixe/corrige les valeurs des champs qui peuvent/doivent etre determines/calcules/deduits automatiquement ou qui doivent etre vides.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (individu() == null) {
			throw new EtudiantException("L'étudiant n'a pas d'identité (individu)");
		}

		// Individu
		// passage en majuscules des noms/prenoms affichage...
		if (individu().nomPatronymiqueAffichage() != null) {
			individu().setNomPatronymiqueAffichage(individu().nomPatronymiqueAffichage().toUpperCase());
		}
		if (individu().prenomAffichage() != null) {
			individu().setPrenomAffichage(individu().prenomAffichage().toUpperCase());
		}
		if (individu().nomAffichage() != null) {
			individu().setNomAffichage(individu().nomAffichage().toUpperCase());
		}

		// remplissage des noms / prenom avec les nomsAffichage et prenomAffichage, sans accents et en majuscules
		if (individu().nomPatronymique() == null && individu().nomPatronymiqueAffichage() != null) {
			individu().setNomPatronymique(StringCtrl.chaineSansAccents(individu().nomPatronymiqueAffichage()).toUpperCase());
		}
		if (individu().prenom() == null && individu().prenomAffichage() != null) {
			individu().setPrenom(StringCtrl.chaineSansAccents(individu().prenomAffichage()).toUpperCase());
		}
		String nomUsuel = null;
		EOCivilite civilite = individu().toCivilite();
		if (civilite != null && civilite.lCivilite().equalsIgnoreCase("Madame")) {
			nomUsuel = individu().nomUsuel();
		}

		if (nomUsuel == null && individu().nomAffichage() != null) {
			individu().setNomUsuel(StringCtrl.chaineSansAccents(individu().nomAffichage()).toUpperCase());
		}
		// Premiere inscription
		if (etudAnnee1InscSup() != null && isPreInscription() && etudAnnee1InscSup().intValue() == anneeInscriptionEnCours().intValue()) {
			setEtudAnnee1InscUniv(etudAnnee1InscSup());
			setToRneCodeSupRelationship(FinderRne.getRne(edc(), FinderParametre.getParam(edc(), "GRHUM_DEFAULT_RNE")));
			setEtudAnnee1InscUlr(etudAnnee1InscSup());
		}

		// Dernier diplome obtenu
		if (isPreInscription() || isPreReInscription()) {
			EOHistorique historique = historiquePlusRecent(null);
			if (historique != null && historique.toTypeDiplome() != null
					&& (historique.toTypeDiplome().tdiplCode().equalsIgnoreCase(EOTypeDiplome.TDIPL_CODE_AUCUN_DIPLOME))) {
				historique.setHistAnneeDerDipl(null);
				historique.setHistLibelleDerDipl(null);
				historique.setToFwkpers_Pays_DerDiplRelationship(null);
				historique.setToFwkpers_Departement_DerDiplRelationship(null);
				historique.setToRneDerDiplRelationship(null);
				historique.setHistVilleDerDipl(null);
			}
		}

		// Situation sociale
		if (isPreInscription() || isPreReInscription()) {
			EOHistorique historique = historiquePlusRecent(null);
			if (historique.histAffss() != null && historique.histAffss().intValue() == 0) {
				historique.setToMutuelleOrgaRelationship(null);
				historique.setHistAyantDroit(new Integer(0));
			}
			if (historique.histAffss() != null && historique.histAffss().intValue() == 1) {
				historique.setCotiCode(null);
			}
		}

	}

	public void validateIdentite() throws EtudiantException {
		EOHistorique historique = historique(anneeInscriptionEnCours());
		// champs obligatoires
		if (individu() == null) {
			throw new EtudiantException("L'étudiant n'a pas d'identité (individu)");
		}
		if (individu().toCivilite() == null) {
			throw new EtudiantException("L'étudiant n'a pas de civilité");
		}
		if (individu().nomPatronymiqueAffichage() == null) {
			throw new EtudiantException("Nom patronymique obligatoire");
		}
		if (individu().prenomAffichage() == null) {
			throw new EtudiantException("Prénom obligatoire");
		}
		if (individu().dNaissance() == null) {
			throw new EtudiantException("Date de naissance obligatoire");
		}
		if (individu().villeDeNaissance() == null) {
			throw new EtudiantException("Ville de naissance obligatoire");
		}
		if (individu().toPaysNaissance() == null) {
			throw new EtudiantException("Pays de naissance obligatoire");
		}
		if (individu().toPaysNationalite() == null) {
			throw new EtudiantException("Nationalité obligatoire");
		}
		if (toSitFamEtudiant() == null) {
			throw new EtudiantException("Situation familiale obligatoire");
		}
		if (historique == null) {
			throw new EtudiantException("L'étudiant a un problème de dossier (pas d'historique)");
		}
		if (historique.histSalarie() == null) {
			throw new EtudiantException("Statut salarié obligatoire");
		}
		// champs obligatoires conditionnels
		if (individu().toCivilite().cCivilite().equals(EOCivilite.C_CIVILITE_MADAME) && individu().nomAffichage() == null) {
			throw new EtudiantException("Nom d'usage obligatoire pour les femmes mariées");
		}
		if (individu().toPaysNaissance().isPaysDefaut() && individu().toDepartement() == null) {
			throw new EtudiantException("Département de naissance obligatoire");
		}
		if ((historique.histSalarie().equals(EOHistorique.SALARIE_FONCTIONNAIRE)) && (historique.toVEtablissementSalarie() == null)) {
			throw new EtudiantException("Etablissement d'affectation obligatoire");
		}
		// if ((historique.histSalarie().equals(EOHistorique.SALARIE_NON) == false) && (historique.histSalarieLibelle()
		// == null)) {
		// throw new EtudiantException("Précision sur la position du salarié obligatoire");
		// }
		// verifications
		if (individu().dNaissance().after(DateCtrl.now())) {
			throw new EtudiantException("Date de naissance incorrecte");
		}
		// particularites, actions
	}

	public void validateAdresses() throws EtudiantException {
		// champs obligatoires
		if (adresseStable() == null) {
			throw new EtudiantException("L'étudiant n'a pas d'adresse stable (parents)");
		}
		if (adresseStable().adrAdresse1() == null) {
			throw new EtudiantException("Première ligne de l'adresse obligatoire");
		}
		if (adresseStable().toPays() == null) {
			throw new EtudiantException("Pays de l'adresse obligatoire");
		}
		if (adresseStable().ville() == null) {
			throw new EtudiantException("Ville de l'adresse obligatoire");
		}
		if (toTypeHebergement() == null || toTypeHebergement().equals(FinderTypeHebergement.getTypeHebergementEnAttente(editingContext())) == false) {
			if (adresseUniversitaire() == null) {
				throw new EtudiantException("L'étudiant n'a pas d'adresse universitaire");
			}
			if (adresseUniversitaire().adrAdresse1() == null) {
				throw new EtudiantException("Première ligne de l'adresse universitaire obligatoire");
			}
			if (adresseUniversitaire().toPays() == null) {
				throw new EtudiantException("Pays de l'adresse universitaire obligatoire");
			}
			if (adresseUniversitaire().ville() == null) {
				throw new EtudiantException("Ville de l'adresse universitaire obligatoire");
			}
		}
		// champs obligatoires conditionnels
		if (adresseStable().toPays().isPaysDefaut() && EOAdresse.isCodePostalObligatoire(edc()) && adresseStable().codePostal() == null) {
			throw new EtudiantException("Code postal obligatoire pour une adresse stable");
		}
		if (toTypeHebergement() == null || toTypeHebergement().equals(FinderTypeHebergement.getTypeHebergementEnAttente(editingContext())) == false) {
			if (adresseUniversitaire().toPays().isPaysDefaut() && EOAdresse.isCodePostalObligatoire(edc())
					&& adresseUniversitaire().codePostal() == null) {
				throw new EtudiantException("Code postal obligatoire pour une adresse universitaire");
			}
		}
		// verifications
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOCommune.ENTITY_NAME, null, null);
		fetchSpec.setFetchLimit(1);
		NSArray<EOCommune> objects = edc().objectsWithFetchSpecification(fetchSpec);
		boolean useCommunes = (objects != null && objects.count() > 0);
		if (adresseStable().toPays().isPaysDefaut() && adresseStable().codePostal() != null && adresseStable().ville() != null && useCommunes) {
			NSArray<EOCommune> communes = FinderCommune.getCommunesExact(adresseStable().editingContext(), adresseStable().codePostal(),
					adresseStable().ville());
			if (communes == null || communes.count() == 0) {
				throw new EtudiantException("Problème sur l'adresse stable, commune inconnue");
			}
			if (communes.count() > 1) {
				throw new EtudiantException("Problème sur l'adresse stable, plusieurs communes correspondent");
			}
		}
		if (toTypeHebergement() == null || toTypeHebergement().equals(FinderTypeHebergement.getTypeHebergementEnAttente(editingContext())) == false) {
			if (adresseUniversitaire().toPays().isPaysDefaut() && adresseUniversitaire().codePostal() != null
					&& adresseUniversitaire().ville() != null && useCommunes) {
				NSArray<EOCommune> communes = FinderCommune.getCommunesExact(adresseUniversitaire().editingContext(), adresseUniversitaire()
						.codePostal(), adresseUniversitaire().ville());
				if (communes == null || communes.count() == 0) {
					throw new EtudiantException("Problème sur l'adresse universitaire, commune inconnue");
				}
				if (communes.count() > 1) {
					throw new EtudiantException("Problème sur l'adresse universitaire, plusieurs communes correspondent");
				}
			}
		}
		// particularites, actions
	}

	public void validateDiplomes() throws EtudiantException {
		// champs obligatoires
		if (formationsEnvisagees() == null || formationsEnvisagees().count() == 0) {
			throw new EtudiantException("Vous devez choisir au moins une formation");
		}
		// champs obligatoires conditionnels
		if (languesVivantes() != null && languesVivantes().count() > 0) {
			EOHistorique historique = historique(anneeInscriptionEnCours());
			if (historique == null) {
				throw new EtudiantException("Il y a un problème avec votre dossier administratif.");
			}
			if (historique.toLangue() == null) {
				throw new EtudiantException("Vous devez choisir votre langue vivante 1");
			}
		}
		// verifications
		// particularites, actions
	}

	public void validateTitreAcces() throws EtudiantException {
		// champs obligatoires
		if (toBac() == null || toBac().bacType() == null) {
			throw new EtudiantException("Le titre d'accès est obligatoire");
		}
		// champs obligatoires conditionnels
		if (!toBac().bacType().equals(EOBac.BAC_TYPE_EQUIVALENCE)) {
			if (etudAnbac() == null) {
				throw new EtudiantException("L'année d'obtention du bac est obligatoire");
			}
			if (isPreInscription() && toMentionBac() == null) {
				throw new EtudiantException("La mention du bac est obligatoire");
			}
			if (toFwkpers_Pays_EtabBac() == null) {
				throw new EtudiantException("Le pays d'obtention du bac est obligatoire");
			}
			if (etudVilleBac() == null && toRneCodeBac() == null) {
				throw new EtudiantException("La ville d'obtention du bac est obligatoire");
			}
			if (toFwkpers_Pays_EtabBac() != null && toFwkpers_Pays_EtabBac().isPaysDefaut()) {
				if (toFwkpers_Departement_EtabBac() == null) {
					throw new EtudiantException("Le département d'obtention du bac est obligatoire");
				}
				if (isPreInscription() && toRneCodeBac() == null) {
					throw new EtudiantException("L'établissement d'obtention du bac est obligatoire");
				}
			}
		}
		// verifications
		if (toBac().bacType().equals(EOBac.BAC_TYPE_EQUIVALENCE)) {
			// Capacite, DAEU A ou DAEU B
			if (toBac().bacCode().equals("0030") || toBac().bacCode().equals("0033") || toBac().bacCode().equals("0034")) {
				if (etudAnbac() == null) {
					throw new EtudiantException("L'année d'obtention du titre d'accès est obligatoire");
				}
				if (toFwkpers_Pays_EtabBac() == null) {
					throw new EtudiantException("Le pays d'obtention du titre d'accès est obligatoire");
				}
				if (etudVilleBac() == null) {
					throw new EtudiantException("La ville d'obtention du titre d'accès est obligatoire");
				}
				if (toFwkpers_Pays_EtabBac() != null && toFwkpers_Pays_EtabBac().isPaysDefaut()) {
					if (toFwkpers_Departement_EtabBac() == null) {
						throw new EtudiantException("Le département d'obtention du titre d'accès est obligatoire");
					}
					if (isPreInscription() && toRneCodeBac() == null) {
						throw new EtudiantException("L'établissement d'obtention du titre d'accès est obligatoire");
					}
				}
			}
			// Titre Etranger admis nationalement en Equivalence
			if (toBac().bacCode().equals("0031")) {
				if (etudAnbac() == null) {
					throw new EtudiantException("L'année d'obtention du titre d'accès est obligatoire.");
				}
				if (isPreInscription() && toFwkpers_Pays_EtabBac() == null) {
					throw new EtudiantException("Le pays d'obtention du titre d'accès est obligatoire.");
				}
				if (toFwkpers_Pays_EtabBac() != null && toFwkpers_Pays_EtabBac().isPaysDefaut()) {
					throw new EtudiantException("Le pays d'obtention de ce titre d'accès ne peut pas être le pays local");
				}
			}
			// Titre Français admis nationalement en Equivalence
			if (toBac().bacCode().equals("0032")) {
				if (etudAnbac() == null) {
					throw new EtudiantException("L'année d'obtention du titre d'accès est obligatoire !");
				}
				if (toFwkpers_Pays_EtabBac() != null && !toFwkpers_Pays_EtabBac().isPaysDefaut()) {
					throw new EtudiantException("Le pays d'obtention de ce titre d'accès ne peut être que le pays local");
				}
			}
			// PROMOTION SOCIALE, VALIDATION DES ACQUIS
			if (toBac().bacCode().equals("0035") || toBac().bacCode().equals("0036")) {
				if (etudAnbac() == null) {
					throw new EtudiantException("L'année d'obtention du titre d'accès est obligatoire...");
				}
			}
			// AUTRES CAS DE NON BACHELIERS
			if (toBac().bacCode().equals("0037")) {
				// pas de controles...
			}
		}
		if (etudAnbac() != null) {
			// PO 2010 - UNC : Attention, modif car les tests précédents ne fonctionnaient
			// pas dans le cas de la préinscription en fin N-1 pour l'année N !!!
			// (on peut avoir son bac en 2009 et se préinscrire en novembre 2009 pour la rentée de février 2010... c'est
			// ça, aussi !)

			boolean anneeCivile = FinderParametre.getAnneeCivile(edc());
			if (anneeCivile == false && etudAnbac().intValue() > anneeInscriptionEnCours().intValue()) {
				throw new EtudiantException("L'année d'obtention du titre d'accès est incorrecte");
			}
			if (anneeCivile == true && etudAnbac().intValue() >= anneeInscriptionEnCours().intValue()) {
				throw new EtudiantException("L'année d'obtention du titre d'accès est incorrecte.");
			}
			if (anneeCivile == false && etudAnbac().intValue() > DateCtrl.nowDay().get(GregorianCalendar.YEAR)) {
				throw new EtudiantException("L'année d'obtention du titre d'accès est incorrecte...");
			}
			// modif UNC -
			// TO DO = pour être plus précis, il faudrait aussi vérifier dans quel cas on se trouve (préinsc), voir quel
			// mois on est, etc...
			// on change >= par >
			if (anneeCivile == true && etudAnbac().intValue() > DateCtrl.nowDay().get(GregorianCalendar.YEAR)) {
				throw new EtudiantException("L'année d'obtention du titre d'accès est incorrecte !");
			}
		}
		// particularites, actions
	}

	public void validatePremiereInscription() throws EtudiantException {
		// champs obligatoires
		if (etudAnnee1InscSup() == null) {
			throw new EtudiantException("L'année de première inscription dans l'enseignement supérieur est obligatoire");
		}
		// non obligatoires pour les ENS par exemple...
		// if (etudAnnee1InscUniv() == null) {
		// throw new EtudiantException("L'année de première inscription dans une université française est obligatoire");
		// }
		// if (rneCodeSup() == null) {
		// throw new EtudiantException("Le premier établissement d'enseignement supérieur est obligatoire");
		// }
		if (etudAnnee1InscUlr() == null) {
			throw new EtudiantException("L'année de première inscription dans l'établissement est obligatoire");
		}
		// champs obligatoires conditionnels
		// verifications
		if (etudAnnee1InscUniv() == null && toRneCodeSup() != null) {
			throw new EtudiantException(
					"L'année de première inscription dans une université locale et le premier établissement sont à renseigner conjointement");
		}
		if (etudAnnee1InscUniv() != null && toRneCodeSup() == null) {
			throw new EtudiantException(
					"L'année de première inscription dans une université locale et le premier établissement sont à renseigner conjointement.");
		}
		if (etudAnnee1InscUniv() != null && etudAnnee1InscSup().intValue() > etudAnnee1InscUniv().intValue()) {
			throw new EtudiantException(
					"L'année de première inscription dans l'enseignement supérieur est postérieure à l'année de première inscription dans une université locale");
		}
		if (etudAnnee1InscUniv() != null && etudAnnee1InscUniv().intValue() > etudAnnee1InscUlr().intValue()) {
			throw new EtudiantException(
					"L'année de première inscription dans une université locale est postérieure à l'année de première inscription dans l'établissement");
		}
		// particularites, actions
	}

	/**
	 * Verifie les donnees concernant le dernier diplome obtenu.
	 * 
	 * @throws EtudiantException
	 */
	public void validateDernierDiplome() throws EtudiantException {
		EOHistorique historique = historique(anneeInscriptionEnCours());
		if (historique == null) {
			throw new EtudiantException("Les informations concernant le dernier diplôme obtenu sont obligatoires.");
		}
		// champs obligatoires
		if (historique.toTypeDiplome() == null) {
			throw new EtudiantException("Le type du dernier diplôme obtenu est obligatoire");
		}
		// champs obligatoires conditionnels
		if (!historique.toTypeDiplome().tdiplCode().equalsIgnoreCase(EOTypeDiplome.TDIPL_CODE_AUCUN_DIPLOME)) {
			if (historique.histAnneeDerDipl() == null) {
				throw new EtudiantException("L'année d'obtention du dernier diplôme est obligatoire");
			}
			if (historique.toFwkpers_Pays_DerDipl() == null) {
				throw new EtudiantException("Le pays d'obtention du dernier diplôme est obligatoire");
			}
			if (historique.histLibelleDerDipl() == null) {
				throw new EtudiantException("Le libellé du dernier diplôme est obligatoire");
			}
			if (historique.toFwkpers_Pays_DerDipl() != null && historique.toFwkpers_Pays_DerDipl().isPaysDefaut()) {
				if (historique.toFwkpers_Departement_DerDipl() == null) {
					throw new EtudiantException("Le département d'obtention du dernier diplôme est obligatoire");
				}
				if (isPreInscription() && historique.toRneDerDipl() == null) {
					throw new EtudiantException("L'établissement d'obtention du dernier diplôme est obligatoire");
				}
				if (historique.histVilleDerDipl() == null) {
					throw new EtudiantException("La ville d obtention du dernier diplôme est obligatoire");
				}
			}
		}
		// verifications
		if (historique.histEnsDerEtab().length() > 80) {
			throw new EtudiantException("Les informations concernant le dernier enseignement suivi ne peuvent pas faire plus de 80 caractères. Actuellement longueur : " + historique.histEnsDerEtab().length());
		}
		
		
		// particularites, actions
	}

	/**
	 * Verifie les donnees concernant le dernier établissement fréquenté.
	 * 
	 * @throws EtudiantException
	 */
	public void validateDernierEtablissement() throws EtudiantException {
		EOHistorique historique = historique(anneeInscriptionEnCours());
		if (historique == null) {
			throw new EtudiantException("Les informations concernant le dernier établissement fréquenté sont obligatoires.");
		}
		// champs obligatoires
		if (historique.toSituationScol() == null) {
			throw new EtudiantException("La situation scolaire année précédente est obligatoire");
		}
		// champs obligatoires conditionnels
		if (!historique.toSituationScol().situCode().equalsIgnoreCase(EOSituationScol.SITU_CODE_JAMAIS_SCOLARISE)) {
			if (historique.histAnneeDerEtab() == null) {
				throw new EtudiantException("Année de fréquentation du dernier établissement enseignement supérieur obligatoire");
			}
			if (historique.toFwkpers_Pays_DerEtab() == null) {
				throw new EtudiantException("Le pays du dernier établissement enseignement supérieur fréquenté est obligatoire");
			}
			if (historique.toFwkpers_Pays_DerEtab() != null && historique.toFwkpers_Pays_DerEtab().isPaysDefaut()) {
				if (historique.toFwkpers_Departement_DerEtab() == null) {
					throw new EtudiantException("Le département du dernier établissement enseignement supérieur fréquenté est obligatoire");
				}
				// if (!isPreInscription()) {
				// if (historique.toRneDerEtab() == null) {
				// throw new
				// EtudiantException("Le dernier établissement d'enseignement supérieur fréquenté est obligatoire");
				// }
				// }
				if (historique.histVilleDerEtab() == null) {
					throw new EtudiantException("La ville du dernier établissement enseignement supérieur fréquenté est obligatoire");
				}
			}
			if (historique.toSituationScol() != null && historique.toSituationScol().situCode().equals(EOSituationScol.SITU_CODE_CPGE)) {
				EORne rneLocal = FinderRne.getRneLocal(edc());
				if (rneLocal != null && rneLocal.tetabCode() != null && rneLocal.tetabCode().equals(EORne.TETAB_CODE_ECOLE_INGENIEUR) && !isPreInscription() && !isReInscription()) {
					if (historique.toTypeClassePreparatoire() == null) {
						throw new EtudiantException("Le type de classe préparatoire du dernier établissement fréquenté est obligatoire");
					}
				}
			}
		}
		// verifications
		if (historique.histAnneeDerEtab() != null && historique.histAnneeScol() != null
				&& historique.histAnneeDerEtab().intValue() < historique.histAnneeScol().intValue() - 1) {
			if (historique.toSituationScol() != null && !historique.toSituationScol().situCode().equals(EOSituationScol.SITU_CODE_JAMAIS_SCOLARISE)
					&& !historique.toSituationScol().situCode().equals(EOSituationScol.SITU_CODE_NON_SCOLARISE)) {
				throw new EtudiantException("Situation scolaire année précédente incompatible avec année de fréquentation du dernier établissement");
			}
		}
		// particularites, actions
	}

	/**
	 * Verifie les donnees concernant les bourses et exonerations.
	 * 
	 * @throws EtudiantException
	 */
	public void validateBoursesEtExonerations() throws EtudiantException {
		EOHistorique historique = historique(anneeInscriptionEnCours());
		// champs obligatoires
		if (historique == null) {
			throw new EtudiantException("Les informations concernant les bourses et exonérations sont obligatoires.");
		}
		if (historique.histBourse() == null) {
			throw new EtudiantException("Le type de bourse 1 est obligatoire");
		}
		// champs obligatoires conditionnels
		if (historique.histBourse() != null && historique.histBourse().intValue() == 1) {
			if (historique.histAcadBourse() == null) {
				throw new EtudiantException("Académie donnant la bourse obligatoire");
			}
			// if (historique.histNumeroAlloc() == null) {
			// throw new EtudiantException("Le numéro d'allocataire de bourse est obligatoire");
			// }
			// if (historique.histEchBourse() == null) {
			// throw new EtudiantException("L'échelon de bourse est obligatoire");
			// }
		}
		if (historique.histAyantDroit() != null && historique.histAyantDroit().intValue() == -2) {
			if (historique.histBourse().compareTo(EOHistorique.NON_BOURSIER) == 0) {
				throw new EtudiantException("Si vous êtes exonéré de cotisation, vous devez être boursier !");
			}
		}
		// FIXME quand y'aura histBourse2
		// if (historique.histBourse2() != null && historique.histBourse2().intValue() == 1) {
		// if (historique.histAcadBourse() == null) {
		// throw new EtudiantException("L'académie donnant la bourse est obligatoire");
		// }
		// if (historique.histNumeroAlloc() == null) {
		// throw new EtudiantException("Le numéro d'allocataire de bourse est obligatoire");
		// }
		// if (historique.histEchBourse() == null) {
		// throw new EtudiantException("L'échelon de bourse est obligatoire");
		// }
		// }
		if (historique.histBourse() != null && historique.histBourse().intValue() == 4) {
			if (historique.histAutreCasExo() == null) {
				throw new EtudiantException("Le motif exonération est obligatoire");
			}
		}
		// FIXME quand y'aura histBourse2
		// if (historique.histBourse2() != null && historique.histBourse2().intValue() == 4) {
		// if (historique.histAutreCasExo() == null) {
		// throw new EtudiantException("Le motif d'exonération est obligatoire");
		// }
		// }

		// verifications
		// particularites, actions
	}

	/**
	 * Verifie les donnees concernant la situation sociale.
	 * 
	 * @throws EtudiantException
	 */
	public void validateSituationSociale() throws EtudiantException {
		EOHistorique historique = historique(anneeInscriptionEnCours());
		if (historique == null) {
			throw new EtudiantException("Les informations concernant la situation sociale sont obligatoires.");
		}
		// champs obligatoires
		if (historique.histAffss() == null) {
			throw new EtudiantException("Affiliation au régime assurance étudiant obligatoire");
		}
		// champs obligatoires conditionnels
		if (historique.histAffss() != null && historique.histAffss().intValue() == 0) {
			if (historique.cotiCode() == null) {
				throw new EtudiantException("Le motif de non affiliation au régime assurance étudiant est obligatoire");
			}
		}
		if (historique.histAffss() != null && historique.histAffss().intValue() == 1) {
			if (historique.toMutuelleOrga() == null) {
				throw new EtudiantException("Organisme affiliation au régime assurance étudiant obligatoire");
			}
			if (historique.cotiCode() == null && historique.histAyantDroit() == null) {
				throw new EtudiantException("Le cotisant au régime assurance étudiant est obligatoire");
			}
		}
		// verifications
		if (historique.histAffss() != null && historique.histAffss().intValue() == 0) {
			if (historique.cotiCode() == null || Integer.valueOf(historique.cotiCode()).intValue() <= 0) {
				throw new EtudiantException("Le motif de non affiliation au régime assurance étudiant est obligatoire.");
			}
		}
		if (historique.histAffss() != null && historique.histAffss().intValue() == 1) {
			if (historique.cotiCode() != null && Integer.valueOf(historique.cotiCode()).intValue() > 0) {
				throw new EtudiantException("Le motif de non affiliation au régime assurance étudiant est incorrect");
			}
		}
		// autres
	}

	/**
	 * Verifie les donnees des renseignements divers.
	 * 
	 * @throws EtudiantException
	 */
	public void validateRenseignementsDivers() throws EtudiantException {
		// controle des champs non dependants de l'historique
		// champs obligatoires
		if (etudSportHn() == null) {
			throw new EtudiantException("Le choix sportif de haut niveau est obligatoire");
		}
		if (etudSnAttestation() == null) {
			throw new EtudiantException("Attestation de recensement obligatoire");
		}
		if (etudSnCertification() == null) {
			throw new EtudiantException("La certification de participation à l appel est obligatoire");
		}
		if (toProfession() == null) {
			throw new EtudiantException("La profession du parent 1 est obligatoire");
		}

		EOHistorique historique = historique(anneeInscriptionEnCours());
		if (historique == null) {
			throw new EtudiantException("Les informations diverses (Demandeur emploi?, Formation financée?, Handicapé?) sont obligatoires.");
		}
		// champs obligatoires
		if (historique.histDemEmpl() == null) {
			throw new EtudiantException("Le choix demandeur emploi est obligatoire");
		}
		if (historique.histForProf() == null) {
			throw new EtudiantException("Le choix de formation financée est obligatoire");
		}
		if (historique.toTypeHandicap() == null) {
			throw new EtudiantException("Le choix handicapé est obligatoire");
		}
		if (historique.histAssuranceCivile() == null) {
			throw new EtudiantException("Le choix assurance responsabilité civile est obligatoire");
		}
		// champs obligatoires conditionnels
		if ((historique.histAssuranceCivile().intValue() == 1) && (historique.histAssuranceCivileCie() == null)) {
			throw new EtudiantException("La compagnie assurance est obligatoire en cas de souscription");
		}
		if ((historique.histAssuranceCivile().intValue() == 1) && (historique.histAssuranceCivileNum() == null)) {
			throw new EtudiantException("Le numéro de sociétaire assurance est obligatoire en cas de souscription");
		}
		// verifications
		// autres
	}

	public void validateAutres() throws EtudiantException {
		// champs obligatoires
		if (StringCtrl.isEmpty(charte())) {
			throw new EtudiantException("Le choix acceptation ou refus de la charte informatique est obligatoire");
		}
		if (adresseUniversitaire() == null || StringCtrl.isEmpty(adresseUniversitaire().adrListeRouge())) {
			throw new EtudiantException("Le choix acceptation ou refus de la diffusion de l adresse électronique est obligatoire");
		}
		if (individu() == null || StringCtrl.isEmpty(individu().indPhoto())) {
			throw new EtudiantException("Le choix acceptation ou refus de la diffusion de la photo dans l annuaire est obligatoire");
		}
		// champs obligatoires conditionnels
		// verifications
		// particularites, actions
	}

	public void onTitreAccesUpdate() {
		// Si titre d'acces = annee en cours d'inscription (ou annee precedente??), on recopie les valeurs dans :
		// - Cadre E - Premieres inscriptions (si vide)
		// - Cadre F - Dernier diplome obtenu (si vide)
		// - Cadre G - Dernier etablissement (si vide)

		if (etudAnbac() != null && toBac() != null) {
			boolean anneeCivile = FinderParametre.getAnneeCivile(edc());
			if ((anneeCivile == false && etudAnbac().intValue() == anneeInscriptionEnCours().intValue())
					|| (anneeCivile == true && etudAnbac().intValue() == (anneeInscriptionEnCours().intValue() - 1))) {
				EOHistorique historique = historique(anneeInscriptionEnCours());
				// Cadre E
				if (toBac() != null && etudAnnee1InscSup() == null) {
					EORne rneLocal = FinderRne.getRneLocal(edc());
					if (toBac().bacType().equals(EOBac.BAC_TYPE_EQUIVALENCE) == false) {
						this.setEtudAnnee1InscSup(anneeInscriptionEnCours());
						this.setEtudAnnee1InscUlr(anneeInscriptionEnCours());
						if ((rneLocal != null && rneLocal.tetabCode() != null)
								&& (rneLocal.tetabCode().equalsIgnoreCase(EORne.TETAB_CODE_UNIVERSITE) == true
										|| rneLocal.tetabCode().equalsIgnoreCase(EORne.TETAB_CODE_IUT) == true || rneLocal.tetabCode()
										.equalsIgnoreCase(EORne.TETAB_CODE_IUFM) == true)) {
							this.setEtudAnnee1InscUniv(anneeInscriptionEnCours());
							this.setToRneCodeSupRelationship(FinderRne.getRneLocal(edc()));
						}
					}
					if (toBac().bacType().equals(EOBac.BAC_TYPE_EQUIVALENCE) == true && toBac().bacCode().equals("0030") == false
							&& toBac().bacCode().equals("0033") == false && toBac().bacCode().equals("0034") == false) {
						this.setEtudAnnee1InscSup(anneeInscriptionEnCours());
						this.setEtudAnnee1InscUlr(anneeInscriptionEnCours());
						if ((rneLocal != null && rneLocal.tetabCode() != null)
								&& (rneLocal.tetabCode().equalsIgnoreCase(EORne.TETAB_CODE_UNIVERSITE) == true
										|| rneLocal.tetabCode().equalsIgnoreCase(EORne.TETAB_CODE_IUT) == true || rneLocal.tetabCode()
										.equalsIgnoreCase(EORne.TETAB_CODE_IUFM) == true)) {
							this.setEtudAnnee1InscUniv(anneeInscriptionEnCours());
							this.setToRneCodeSupRelationship(FinderRne.getRneLocal(edc()));
						}
					}
				}
				// Cadre F
				if (historique.toTypeDiplome() == null) {
					if (toBac().bacType().equals(EOBac.BAC_TYPE_EQUIVALENCE) == false) {
						if (toRneCodeBac() != null && toFwkpers_Departement_EtabBac() != null && toFwkpers_Pays_EtabBac() != null
								&& etudVilleBac() != null) {
							historique.setToTypeDiplomeRelationship(FinderTypeDiplome.getTypeDiplome(edc(), "A"));
							historique.setToRneDerDiplRelationship(toRneCodeBac());
							historique.setToFwkpers_Departement_DerDiplRelationship(toFwkpers_Departement_EtabBac());
							historique.setToFwkpers_Pays_DerDiplRelationship(toFwkpers_Pays_EtabBac());
							historique.setHistAnneeDerDipl(anneeInscriptionEnCours());
							historique.setHistLibelleDerDipl(toBac().bacLibelle());
							historique.setHistVilleDerDipl(etudVilleBac());
						}
					}
				}
				// Cadre G
				if (historique.histAnneeDerEtab() == null) {
					if (toBac().bacType().equals(EOBac.BAC_TYPE_EQUIVALENCE) == false) {
						if (toRneCodeBac() != null && toFwkpers_Departement_EtabBac() != null && toFwkpers_Pays_EtabBac() != null
								&& etudVilleBac() != null) {
							historique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(edc(), "A"));
							historique.setToRneDerEtabRelationship(toRneCodeBac());
							historique.setToFwkpers_Departement_DerEtabRelationship(toFwkpers_Departement_EtabBac());
							historique.setToFwkpers_Pays_DerEtabRelationship(toFwkpers_Pays_EtabBac());
							historique.setHistAnneeDerEtab(new Integer(anneeInscriptionEnCours().intValue() - 1));
							historique.setHistEnsDerEtab(toBac().bacLibelle());
							historique.setHistLibelleDerEtab(toRneCodeBac().llRne());
							historique.setHistVilleDerEtab(etudVilleBac());
						}
					}
				}
			}
		}
	}

	public void onDernierDiplomeUpdate() {
		// Si obtention dernier diplome = annee en cours d'inscription (ou annee precedente??), on recopie les
		// valeurs dans dernier etablissement obtenu (si celui-ci est vide)
		EOHistorique historique = historique(anneeInscriptionEnCours());
		if (historique.histAnneeDerEtab() == null) {
			if (historique.histAnneeDerDipl() != null && historique.toRneDerDipl() != null && historique.toFwkpers_Departement_DerDipl() != null
					&& historique.toFwkpers_Pays_DerDipl() != null && historique.histLibelleDerDipl() != null
					&& historique.histVilleDerDipl() != null) {
				boolean anneeCivile = FinderParametre.getAnneeCivile(edc());
				if ((anneeCivile == false && historique.histAnneeDerDipl().intValue() == anneeInscriptionEnCours().intValue())
						|| (anneeCivile == true && historique.histAnneeDerDipl().intValue() == (anneeInscriptionEnCours().intValue() - 1))) {
					// Cadre G
					historique.setToRneDerEtabRelationship(historique.toRneDerDipl());
					historique.setToFwkpers_Departement_DerEtabRelationship(historique.toFwkpers_Departement_DerDipl());
					historique.setToFwkpers_Pays_DerEtabRelationship(historique.toFwkpers_Pays_DerDipl());
					historique.setHistAnneeDerEtab(new Integer(anneeInscriptionEnCours().intValue() - 1));
					historique.setHistEnsDerEtab(historique.histLibelleDerDipl());
					historique.setHistLibelleDerEtab(historique.toRneDerDipl().llRne());
					historique.setHistVilleDerEtab(historique.histVilleDerDipl());

					if (historique.toRneDerEtab() != null && historique.toRneDerEtab().situationScol() != null) {
						historique.setToSituationScolRelationship(historique.toRneDerEtab().situationScol());
					}
					else {
						if (historique.toFwkpers_Pays_DerEtab() != null && historique.toFwkpers_Pays_DerEtab().isPaysDefaut() == false) {
							historique.setToSituationScolRelationship(FinderSituationScol.getSituationScol(edc(), "P"));
						}
					}
				}
			}
		}
	}

	//

	public Integer numero() {
		return etudNumero();
	}

	public void setNumero(Integer numero) {
		setEtudNumero(numero);
	}

	public String prenomNom() {
		String prenomNom = "";

		if (individu() != null) {
			prenomNom = StringCtrl.normalize(individu().prenomAffichage()) + " " + StringCtrl.normalize(individu().nomPatronymiqueAffichage());
		}
		return prenomNom;
	}

	public String numeroINE() {
		return etudCodeIne();
	}

	public void setNumeroINE(String numeroIne) {
		setEtudCodeIne(numeroIne);
	}

	public EOIndividu individu() {
		return toFwkpers_Individu();
	}

	public void setIndividu(EOIndividu individu) {
		setToFwkpers_IndividuRelationship(individu);
	}

	public EOCandidatGrhum candidat() {
		if (toCandidatGrhums() == null || toCandidatGrhums().count() == 0) {
			return null;
		}
		return (EOCandidatGrhum) toCandidatGrhums().objectAtIndex(0);
	}

	public boolean isCelibataire() {
		boolean isCelibataire = false;
		EOSitFamEtudiant situation = toSitFamEtudiant();
		if (situation != null) {
			String libelle = situation.etudSitfamLibelle();
			if (libelle != null && libelle.toLowerCase().startsWith("seul")) {
				isCelibataire = true;
			}
		}
		return isCelibataire;
	}

	public void setIsCelibataire(Boolean isCelibataire) {
		String situation = "";
		if (isCelibataire.booleanValue() == true) {
			situation = "Seul";
		}
		else {
			situation = "En couple";
		}
		if (isEnfantsACharge()) {
			situation += " avec enfants";
		}
		else {
			situation += " sans enfants";
		}
		FactoryEtudiant.setSituationFamiliale(this, situation);
	}

	public boolean isEnCouple() {
		boolean isEnCouple = false;
		EOSitFamEtudiant situation = toSitFamEtudiant();
		if (situation != null) {
			String libelle = situation.etudSitfamLibelle();
			if (libelle != null && libelle.toLowerCase().startsWith("en couple")) {
				isEnCouple = true;
			}
		}
		return isEnCouple;
	}

	public void setIsEnCouple(Boolean isEnCouple) {
		String situation = "";
		if (isEnCouple.booleanValue() == false) {
			situation = "Seul";
		}
		else {
			situation = "En couple";
		}
		if (isEnfantsACharge()) {
			situation += " avec enfants";
		}
		else {
			situation += " sans enfants";
		}
		FactoryEtudiant.setSituationFamiliale(this, situation);
	}

	public boolean isEnfantsACharge() {
		boolean isEnfantsACharge = false;
		EOSitFamEtudiant situation = toSitFamEtudiant();
		if (situation != null) {
			String libelle = situation.etudSitfamLibelle();
			if (libelle != null && libelle.toLowerCase().endsWith("avec enfants")) {
				isEnfantsACharge = true;
			}
		}
		return isEnfantsACharge;
	}

	public void setIsEnfantsACharge(Boolean isEnfantsACharge) {
		String situation = "";
		if (isEnCouple() == false) {
			situation = "Seul";
		}
		else {
			situation = "En couple";
		}
		if (isEnfantsACharge) {
			situation += " avec enfants";
		}
		else {
			situation += " sans enfants";
		}
		FactoryEtudiant.setSituationFamiliale(this, situation);
	}

	public EOAdresse adresseStable() {
		return FactoryIndividuUlr.adresseStable(individu());
	}

	public EOAdresse adresseUniversitaire() {
		return FactoryIndividuUlr.adresseUniversitaire(individu());
	}

	public EOPersonneTelephone telephoneStable() {
		return FactoryIndividuUlr.telephoneStable(individu());
	}

	public void setTelephoneStable(EOPersonneTelephone telephone) {
		individu().addToToPersonneTelephonesRelationship(telephone);
	}

	public String noTelephoneStable() {
		String noTelephone = null;
		EOPersonneTelephone telephone = telephoneStable();
		if (telephone != null) {
			noTelephone = telephone.noTelephone();
		}
		return noTelephone;
	}

	public void setNoTelephoneStable(EOEditingContext ec, String noTelephoneStable) {
		EOPersonneTelephone telephone = telephoneStable();
		if (telephone != null) {
			telephone.setNoTelephone(StringCtrl.formatPhoneNumber(noTelephoneStable));
		}
		else {
			EOTypeNoTel typeNoTel = EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL);
			EOTypeTel typeTel = EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_PAR);
			telephone = EOPersonneTelephone.createEOPersonneTelephone(ec, DateCtrl.now(), DateCtrl.now(), noTelephoneStable, individu().persId(),
					null, null, null, typeNoTel, typeTel);
			setTelephoneStable(telephone);
		}
	}

	public EOPersonneTelephone telephoneUniversitaireFixe() {
		return FactoryIndividuUlr.telephoneUniversitaireFixe(individu());
	}

	public void setTelephoneUniversitaireFixe(EOPersonneTelephone telephone) {
		individu().addToToPersonneTelephonesRelationship(telephone);
	}

	public String noTelephoneUniversitaireFixe() {
		String noTelephone = null;
		EOPersonneTelephone telephone = telephoneUniversitaireFixe();
		if (telephone != null) {
			noTelephone = telephone.noTelephone();
		}
		return noTelephone;
	}

	public void setNoTelephoneUniversitaireFixe(EOEditingContext ec, String noTelephoneUniversitaireFixe) {
		EOPersonneTelephone telephone = telephoneUniversitaireFixe();
		if (telephone != null) {
			telephone.setNoTelephone(StringCtrl.formatPhoneNumber(noTelephoneUniversitaireFixe));
		}
		else {
			EOTypeNoTel typeNoTel = EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL);
			EOTypeTel typeTel = EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_ETUD);
			telephone = EOPersonneTelephone.createEOPersonneTelephone(ec, DateCtrl.now(), DateCtrl.now(), noTelephoneUniversitaireFixe, individu()
					.persId(), null, null, null, typeNoTel, typeTel);
			setTelephoneUniversitaireFixe(telephone);
		}
	}

	public EOPersonneTelephone telephoneUniversitairePortable() {
		return FactoryIndividuUlr.telephoneUniversitairePortable(individu());
	}

	public void setTelephoneUniversitairePortable(EOPersonneTelephone telephone) {
		individu().addToToPersonneTelephonesRelationship(telephone);
	}

	public String noTelephoneUniversitairePortable() {
		String noTelephone = null;
		EOPersonneTelephone telephone = telephoneUniversitairePortable();
		if (telephone != null) {
			noTelephone = telephone.noTelephone();
		}
		return noTelephone;
	}

	public void setNoTelephoneUniversitairePortable(EOEditingContext ec, String noTelephoneUniversitairePortable) {
		EOPersonneTelephone telephone = telephoneUniversitairePortable();
		if (telephone != null) {
			telephone.setNoTelephone(StringCtrl.formatPhoneNumber(noTelephoneUniversitairePortable));
		}
		else {
			EOTypeNoTel typeNoTel = EOTypeNoTel.fetchByKeyValue(ec, EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_MOB);
			EOTypeTel typeTel = EOTypeTel.fetchByQualifier(ec, EOTypeTel.QUAL_C_TYPE_TEL_ETUD);
			telephone = EOPersonneTelephone.createEOPersonneTelephone(ec, DateCtrl.now(), DateCtrl.now(), noTelephoneUniversitairePortable,
					individu().persId(), null, null, null, typeNoTel, typeTel);
			setTelephoneUniversitairePortable(telephone);
		}
	}

	public String email() {
		EORepartPersonneAdresse rpa = repartPersonneAdresseEtud();
		if (rpa != null && rpa.eMail() != null) {
			return rpa.eMail();
		}
		return null;
	}

	public void setEmail(String email) {
		EORepartPersonneAdresse rpa = repartPersonneAdresseEtud();
		if (rpa != null) {
			if (email == null || email.trim().length() == 0) {
				rpa.setEMail(null);
			}
			else {
				rpa.setEMail(email);
			}
		}
	}

	/**
	 * @return le EORepartPersonneAdresse etudiant
	 */
	private EORepartPersonneAdresse repartPersonneAdresseEtud() {
		if (individu() == null) {
			return null;
		}
		EORepartPersonneAdresse rpa = null;
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>(2);
		quals.addObject(new EOKeyValueQualifier(EORepartPersonneAdresse.TO_TYPE_ADRESSE_KEY + "." + EOTypeAdresse.TADR_CODE_KEY,
				EOKeyValueQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_ETUD));
		quals.addObject(new EOKeyValueQualifier(EORepartPersonneAdresse.RPA_VALIDE_KEY, EOKeyValueQualifier.QualifierOperatorEqual, "O"));
		NSArray<EORepartPersonneAdresse> repartAdresses = individu().toRepartPersonneAdresses(new EOAndQualifier(quals));
		if (repartAdresses != null && repartAdresses.count() > 0) {
			rpa = repartAdresses.lastObject();
		}
		return rpa;

	}

	public EOBac titreAcces() {
		return toBac();
	}

	public void setTitreAcces(EOBac titreAcces) {
		setToBacRelationship(titreAcces);
		onTitreAccesUpdate();
	}

	public Integer anneeObtentionTitreAcces() {
		return etudAnbac();
	}

	public void setAnneeObtentionTitreAcces(Integer annee) {
		setEtudAnbac(annee);
		onTitreAccesUpdate();
	}

	public EOMention mentionTitreAcces() {
		return toMentionBac();
	}

	public void setMentionTitreAcces(EOMention mention) {
		setToMentionBacRelationship(mention);
	}

	public EORne etablissementTitreAcces() {
		return toRneCodeBac();
	}

	public void setEtablissementTitreAcces(EORne etablissement) {
		setToRneCodeBacRelationship(etablissement);
		if (etablissement != null) {
			setEtudVilleBac(etablissement.ville());
		}
		onTitreAccesUpdate();
	}

	public void setVilleTitreAcces(String ville) {
		setEtudVilleBac(ville);
		onTitreAccesUpdate();
	}

	public String villeTitreAcces() {
		String villeTitreAcces = etudVilleBac();
		// if (etablissementTitreAcces() != null && !StringCtrl.isEmpty(etablissementTitreAcces().ville())) {
		// villeTitreAcces = etablissementTitreAcces().ville();
		// }
		return villeTitreAcces;
	}

	public EODepartement departementTitreAcces() {
		return toFwkpers_Departement_EtabBac();
	}

	public void setDepartementTitreAcces(EODepartement departement) {
		setToFwkpers_Departement_EtabBacRelationship(departement);
		onTitreAccesUpdate();
	}

	public EOPays paysTitreAcces() {
		return toFwkpers_Pays_EtabBac();
	}

	public void setPaysTitreAcces(EOPays pays) {
		setToFwkpers_Pays_EtabBacRelationship(pays);
		onTitreAccesUpdate();
	}

	public Integer anneePremiereInscriptionEnseignementSuperieur() {
		return etudAnnee1InscSup();
	}

	public void setAnneePremiereInscriptionEnseignementSuperieur(Integer annee) {
		setEtudAnnee1InscSup(annee);
	}

	public Integer anneePremiereInscriptionUniversiteFrancaise() {
		return etudAnnee1InscUniv();
	}

	public void setAnneePremiereInscriptionUniversiteFrancaise(Integer annee) {
		setEtudAnnee1InscUniv(annee);
	}

	public Integer anneePremiereInscriptionUniversiteLocale() {
		return etudAnnee1InscUlr();
	}

	public void setAnneePremiereInscriptionUniversiteLocale(Integer annee) {
		setEtudAnnee1InscUlr(annee);
	}

	public EORne universitePremiereInscription() {
		return toRneCodeSup();
	}

	public void setUniversitePremiereInscription(EORne universite) {
		setToRneCodeSupRelationship(universite);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#historique(java.lang.Integer)
	 */
	public EOHistorique historique(Integer anneeScol) {
		if (anneeScol == null) {
			return null;
		}
		EOHistorique historique = null;
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOHistorique.HIST_ANNEE_SCOL_KEY, EOKeyValueQualifier.QualifierOperatorEqual,
				anneeScol);
		NSArray<EOHistorique> historiques = toHistoriques(qualifier);
		if (historiques != null && historiques.count() > 0) {
			historique = historiques.lastObject();
		}
		return historique;
		// return ERXQ.first(toHistoriques(), ERXQ.equals(EOHistorique.HIST_ANNEE_SCOL_KEY, anneeScol));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#historiquePlusRecent(java.lang.Integer)
	 */
	public EOHistorique historiquePlusRecent(Integer anneeEnCours) {
		EOHistorique historique = null;
		ERXKeyValueQualifier qualifier = null;
		if (anneeEnCours != null) {
			qualifier = ERXQ.lessThan(EOHistorique.HIST_ANNEE_SCOL_KEY, anneeEnCours);
		}
		NSArray<EOHistorique> historiques = toHistoriques(qualifier, ERXS.ascs(EOHistorique.HIST_ANNEE_SCOL_KEY), false);
		if (historiques != null && historiques.count() > 0) {
			historique = historiques.lastObject();
		}
		return historique;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#formationsEnCours()
	 */
	public NSArray<EOVWebInscriptionResultat> formationsEnCours() {
		NSMutableArray<EOVWebInscriptionResultat> vWebInscriptionResultat = null;
		if ((isPreReInscription() || isReInscription()) && anneeInscriptionEnCours() != null) {
			EOHistorique historique = historiquePlusRecent(anneeInscriptionEnCours());
			if (historique != null) {
				NSArray<EOInscDipl> inscDipls = historique.inscriptionsValides();
				if (inscDipls != null) {
					NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(
							EOVWebInscriptionResultat.NIVEAU_DETAIL_KEY, EOSortOrdering.CompareAscending));
					vWebInscriptionResultat = new NSMutableArray<EOVWebInscriptionResultat>();
					for (int i = 0; i < inscDipls.count(); i++) {
						vWebInscriptionResultat
								.addObjectsFromArray(inscDipls.objectAtIndex(i).toVWebInscriptionResultats(null, sortOrderings, false));
					}
				}
			}
		}
		return vWebInscriptionResultat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#formationsEnvisageables()
	 */
	public NSMutableArray<EOScolFormationHabilitation> formationsEnvisageables() {
		if (formationsEnvisageables == null) {
			initFormations();
		}
		return formationsEnvisageables;
	}

	public void setFormationsEnvisageables(NSMutableArray<EOScolFormationHabilitation> formations) {
		formationsEnvisageables = formations;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#formationsPossibles()
	 */
	public NSMutableArray<EOScolFormationHabilitation> formationsPossibles() {
		if (formationsPossibles == null) {
			initFormations();
		}
		return formationsPossibles;
	}

	public void setFormationsPossibles(NSMutableArray<EOScolFormationHabilitation> formations) {
		formationsPossibles = formations;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#formationsEnvisagees()
	 */
	public NSMutableArray formationsEnvisagees() {
		if (formationsEnvisagees == null) {
			initFormations();
		}
		return formationsEnvisagees;
	}

	public void setFormationsEnvisagees(NSMutableArray formations) {
		formationsEnvisagees = formations;
	}

	public void alerteFormationsEnvisagees() throws EtudiantException {
		formationsEnvisagees();
		if (resetFormationsEnvisagees) {
			resetFormationsEnvisagees = false;
			throw new EtudiantException("Votre situation a évolué, veuillez resaisir vos voeux d'inscription.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#languesVivantes()
	 */
	public NSArray<EOLangue> languesVivantes() {
		NSArray<EOLangue> langues = null;
		if (formationsEnvisagees() != null && formationsEnvisagees().count() > 0) {
			boolean langueObligatoire = false;
			if (isPreInscription() || isPreReInscription()) {
				for (int i = 0; i < formationsEnvisagees().count() && langueObligatoire == false; i++) {
					EOScolFormationHabilitation scolFormationHabilitation = (EOScolFormationHabilitation) formationsEnvisagees().objectAtIndex(i);
					langueObligatoire = FinderPreLangueException.isLangueObligatoire(edc(), anneeInscriptionEnCours,
							scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation(), scolFormationHabilitation.fhabNiveau());
				}
			}
			if (isInscription() || isReInscription()) {
				for (int i = 0; i < formationsEnvisagees().count() && langueObligatoire == false; i++) {
					EOScolFormationHabilitation scolFormationHabilitation = ((EOInscDipl) formationsEnvisagees().objectAtIndex(i))
							.scolFormationHabilitation();
					if (scolFormationHabilitation != null) {
						langueObligatoire = FinderPreLangueException.isLangueObligatoire(edc(), anneeInscriptionEnCours,
								scolFormationHabilitation.toFwkScolarite_ScolFormationSpecialisation(), scolFormationHabilitation.fhabNiveau());
					}
				}
			}
			if (langueObligatoire) {
				EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOLangue.LANG_LIBELLE_KEY, EOSortOrdering.CompareAscending);
				langues = EOLangue.fetchAll(edc(), new NSArray<EOSortOrdering>(sortOrdering));
			}
		}
		return langues;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#salarieEtablissement(java.lang.Integer)
	 */
	public String salarieEtablissement(Integer anneeScol) {
		String salarieEtablissement = EOHistorique.SALARIE_NON;
		if (anneeScol != null) {
			EOHistorique historique = historique(anneeScol);
			if (historique != null) {
				salarieEtablissement = historique.histSalarie();
			}
		}
		return salarieEtablissement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#setSalarieEtablissement(java.lang.String, java.lang.Integer)
	 */
	public void setSalarieEtablissement(String histSalarie, Integer anneeScol) {
		if (histSalarie != null && anneeScol != null) {
			EOHistorique historique = historique(anneeScol);
			if (historique != null) {
				historique.setHistSalarie(histSalarie);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#inscriptionPrincipale(java.lang.Integer)
	 */
	public EOInscDipl inscriptionPrincipale(Integer anneeScol) throws EtudiantException {
		try {
			EOInscDipl inscDipl = null;
			EOHistorique historique = historique(anneeScol);
			if (historique != null) {
				inscDipl = historique.inscriptionPrincipale();
			}
			return inscDipl;
		}
		catch (HistoriqueException e) {
			throw new EtudiantException("L'étudiant No " + this.numero() + " a plusieurs inscriptions principales pour l'année " + anneeScol
					+ ", ce n'est pas cohérent !!");
		}
	}

	public Integer anneeDernierEtabFrequenteAvantAnnee(Integer anneeScol) {
		Integer annee = null;
		EOHistorique historique = historique(anneeScol);
		if (historique != null) {
			annee = historique.histAnneeDerEtab();
		}
		return annee;
	}

	public EODepartement departementDernierEtabFrequenteAvantAnnee(Integer anneeScol) {
		EODepartement departement = null;
		EOHistorique historique = historique(anneeScol);
		if (historique != null) {
			departement = historique.toFwkpers_Departement_DerEtab();
		}
		return departement;
	}

	public EORne dernierEtabFrequenteAvantAnnee(Integer anneeScol) {
		EORne etablissement = null;
		EOHistorique historique = historique(anneeScol);
		if (historique != null) {
			etablissement = historique.toRneDerEtab();
		}
		return etablissement;
	}

	public String enseignementDernierEtabFrequenteAvantAnnee(Integer anneeScol) {
		String enseignement = null;
		EOHistorique historique = historique(anneeScol);
		if (historique != null) {
			enseignement = historique.histEnsDerEtab();
		}
		return enseignement;
	}

	public EOPays paysDernierEtabFrequenteAvantAnnee(Integer anneeScol) {
		EOPays pays = null;
		EOHistorique historique = historique(anneeScol);
		if (historique != null) {
			pays = historique.toFwkpers_Pays_DerEtab();
		}
		return pays;
	}

	public EOSituationScol situationAnneePrecedenteDernierEtabFrequenteAvantAnnee(Integer anneeScol) {
		EOSituationScol situation = null;
		EOHistorique historique = historique(anneeScol);
		if (historique != null) {
			situation = historique.toSituationScol();
		}
		return situation;
	}

	public void setSportifHautNiveau(Integer value) {
		super.setEtudSportHn(value);
	}

	public Integer sportifHautNiveau() {
		return super.etudSportHn();
	}

	public void setAttestationRecensement(Integer value) {
		super.setEtudSnAttestation(value);
	}

	public Integer attestationRecensement() {
		return super.etudSnAttestation();
	}

	public void setCertificationAppel(Integer value) {
		super.setEtudSnCertification(value);
	}

	public Integer certificationAppel() {
		return super.etudSnCertification();
	}

	public void setProfessionDuParent1(EOProfession profession) {
		super.setToProfessionRelationship(profession);
	}

	public EOProfession professionDuParent1() {
		return super.toProfession();
	}

	public void setProfessionDuParent2(EOProfession profession) {
		super.setToProfession2Relationship(profession);
	}

	public EOProfession professionDuParent2() {
		return super.toProfession2();
	}

	public String etudType() {
		return etudType;
	}

	public void setEtudType(String etudType) {
		this.etudType = etudType;
	}

	public boolean isPreInscription() {
		return etudType() != null && etudType().equals(EOEtudiant.ETUD_TYPE_PRE_INSCRIPTION);
	}

	public boolean isPreReInscription() {
		return etudType() != null && etudType().equals(EOEtudiant.ETUD_TYPE_PRE_RE_INSCRIPTION);
	}

	public boolean isInscription() {
		return etudType() != null && etudType().equals(EOEtudiant.ETUD_TYPE_INSCRIPTION);
	}

	public boolean isReInscription() {
		return etudType() != null && etudType().equals(EOEtudiant.ETUD_TYPE_RE_INSCRIPTION);
	}

	public boolean isAdmission() {
		return etudType() != null && etudType().equals(EOEtudiant.ETUD_TYPE_ADMISSION);
	}

	/**
	 * @return le EOCompte type etudiant correspondant au compte de l'etudiant, sinon NULL
	 */
	public EOCompte compte() {
		EOCompte compte = null;
		if (toFwkpers_Individu() != null) {
			NSArray<EOCompte> comptes = toFwkpers_Individu().toComptes(EOCompte.QUAL_CPT_VLAN_E);
			if (comptes != null && comptes.count() > 0) {
				compte = comptes.lastObject();
			}
		}
		return compte;
	}

	public void setCharte(String charte) {
		this.charte = charte;
		if (compte() != null) {
			compte().setCptCharte(charte);
		}
	}

	public String charte() {
		if (compte() != null) {
			return compte().cptCharte();
		}
		if (charte != null) {
			return charte;
		}
		if (numero() != null) {
			return "N";
		}
		return null;
	}

	public void setListeRouge(String listeRouge) {
		if (adresseUniversitaire() != null) {
			adresseUniversitaire().setAdrListeRouge(listeRouge);
		}
		if (adresseStable() != null) {
			adresseStable().setAdrListeRouge(listeRouge);
		}
	}

	public String listeRouge() {
		String listeRouge = null;
		if (adresseUniversitaire() != null) {
			listeRouge = adresseUniversitaire().adrListeRouge();
		}
		if (adresseStable() != null) {
			listeRouge = adresseStable().adrListeRouge();
		}
		return listeRouge;
	}

	public void setPhoto(String photo) {
		if (individu() != null) {
			individu().setIndPhoto(photo);
		}
	}

	public String photo() {
		String photo = null;
		if (individu() != null) {
			photo = individu().indPhoto();
		}
		return photo;
	}

	public EOTypeHebergement toTypeHebergement() {
		return super.toTypeHebergement();
	}

	public void setToTypeHebergementRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeHebergement value) {
		super.setToTypeHebergementRelationship(value);
		if (toTypeHebergement() != null && toTypeHebergement().equals(FinderTypeHebergement.getTypeHebergementEnAttente(editingContext()))) {
			EOAdresse adresseUniversitaire = adresseUniversitaire();
			if (adresseUniversitaire != null) {
				adresseUniversitaire.setAdrAdresse1(null);
				adresseUniversitaire.setAdrAdresse2(null);
				adresseUniversitaire.setCodePostal(null);
				adresseUniversitaire.setVille(null);
				adresseUniversitaire.setAdrBp(null);
				adresseUniversitaire.setToPaysRelationship(null);
			}
		}
	}

	public boolean isPreInscrit() {
		return this.isPreInscrit;
	}

	public void setIsPreInscrit(boolean isPreInscrit) {
		this.isPreInscrit = isPreInscrit;
	}

	public EORdvCandidat rendezVous() {
		return toRdvCandidat();
	}

	public boolean isRendezVousPossible() {
		if (anneeInscriptionEnCours() != null) {
			EOHistorique historique = historique(anneeInscriptionEnCours());
			if (historique != null) {
				return historique.isRendezVousPossible();
			}
		}
		return false;
	}

	public Integer anneeInscriptionEnCours() {
		return anneeInscriptionEnCours;
	}

	public void setAnneeInscriptionEnCours(Integer anneeInscriptionEnCours) {
		this.anneeInscriptionEnCours = anneeInscriptionEnCours;
		setFormationsEnvisageables(null);
		setFormationsPossibles(null);
		setFormationsEnvisagees(null);
	}

	public NSArray<EOPreCandidature> preCandidatures() {
		return preCandidatures;
	}

	public void setPreCandidatures(NSArray<EOPreCandidature> preCandidatures) {
		this.preCandidatures = preCandidatures;
	}

	public EORne rne() {
		if (rne == null) {
			EOInscDipl inscriptionPrincipale = inscriptionPrincipale(anneeInscriptionEnCours);
			if (inscriptionPrincipale == null) {
				NSArray<EOHistorique> a = toHistoriques(
						null,
						new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOHistorique.HIST_ANNEE_SCOL_KEY,
								EOSortOrdering.CompareDescending)), false);
				if (a != null) {
					inscriptionPrincipale = a.objectAtIndex(0).inscriptionPrincipale();
				}
			}
			if (inscriptionPrincipale != null) {
				rne = inscriptionPrincipale.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome()
						.toFwkScolarix_VEtablissementScolarite().toRne();
			}
		}
		return rne;
	}

	public void setRne(EORne rne) {
		this.rne = rne;
	}

	public String messageInterditDeConnexion() {
		return messageInterditDeConnexion;
	}

	public void setMessageInterditDeConnexion(String messageInterditDeConnexion) {
		this.messageInterditDeConnexion = messageInterditDeConnexion;
	}

	public NSMutableArray<EtudiantException> userInfos() {
		return userInfos;
	}

	public void setUserInfos(NSMutableArray<EtudiantException> userInfos) {
		this.userInfos = userInfos;
	}

	/*
	 * Cette procedure initialise le tableau des formations envisageables et envisagees par : - soit des EOScolFormationHabilitation (cas
	 * des pre-inscriptions) - soit des EOInscDipl (cas des inscriptions)
	 */
	private void initFormations() {
		boolean verifierSiChangementsInscs = false;
		if (formationsEnvisageables == null) {
			verifierSiChangementsInscs = true;
			initFormationsEnvisageables();
		}
		if (formationsPossibles == null) {
			initFormationsPossibles();
		}
		if (formationsEnvisagees == null) {
			verifierSiChangementsInscs = true;
			initFormationsEnvisagees();
		}
		resetFormationsEnvisagees = false;
		// traitements specifiques dans le cas de la reinscription...
		if (verifierSiChangementsInscs && isPreReInscription()) {
			// En cas de changement apres jury, des formations envisagees enregistrees avant, peuvent ne plus etre
			// valides (ne plus etre dans les formations envisageables). Dans ce cas, on vide le "panier" des formations
			// envisagees pour obliger l'etudiant a refaire ses choix.
			if (formationsEnvisagees != null && formationsEnvisagees.count() > 0 && formationsEnvisageables != null
					&& formationsEnvisageables.count() > 0) {
				for (int i = 0; i < formationsEnvisagees.count(); i++) {
					EOScolFormationHabilitation formationEnvisagee = (EOScolFormationHabilitation) formationsEnvisagees.objectAtIndex(i);
					if (formationsEnvisageables.contains(formationEnvisagee) == false) {
						resetFormationsEnvisagees = true;
						formationsEnvisagees.removeAllObjects();
						break;
					}
				}
			}
		}
		postInitFormations();
	}

	private void initFormationsEnvisageables() {
		formationsEnvisageables = new NSMutableArray<EOScolFormationHabilitation>();
		if (isPreInscription() && anneeInscriptionEnCours() != null && preCandidatures() != null && preCandidatures().count() > 0) {
			for (int i = 0; i < preCandidatures().count(); i++) {
				EOPreCandidature preCandidature = preCandidatures().objectAtIndex(i);
				EOScolFormationHabilitation scolFormationHabilitation = FinderScolFormationHabilitation.getScolFormationHabilitation(edc(),
						anneeInscriptionEnCours(), preCandidature.toFwkScolarite_ScolFormationSpecialisation(), preCandidature.candAnneeSuivie());
				if (scolFormationHabilitation != null) {
					scolFormationHabilitation.setTypeAction(EOScolFormationHabilitation.TYPE_ACTION_PROGRESSION);
					scolFormationHabilitation.setTypeAssociation(new Integer(i));
					scolFormationHabilitation.setIsSelectionnable(Boolean.TRUE);
					formationsEnvisageables.addObject(scolFormationHabilitation);
				}
			}
		}
		if (isPreReInscription() || isReInscription()) {
			NSArray<EOScolFormationProgression> progressions = FactoryFormations.getProgressions(edc(), this);
			if (progressions != null && progressions.count() > 0) {
				for (int i = 0; i < progressions.count(); i++) {
					EOScolFormationProgression scolFormationProgression = progressions.objectAtIndex(i);
					EOScolFormationHabilitation scolFormationHabilitation = FinderScolFormationHabilitation.getScolFormationHabilitation(edc(),
							anneeInscriptionEnCours(), scolFormationProgression.toFwkScolarite_ScolFormationSpecialisationProgression(),
							scolFormationProgression.fproFhabNiveau());
					if (EOScolFormationProgression.TYPE_PROGRESSION.equals(scolFormationProgression.fproType())) {
						scolFormationHabilitation.setTypeAction(EOScolFormationHabilitation.TYPE_ACTION_PROGRESSION);
						scolFormationHabilitation.setIsSelectionnable(Boolean.FALSE);
						scolFormationHabilitation.setIsPassageAMinima(Boolean.FALSE);
					}
					else {
						scolFormationHabilitation.setTypeAction(EOScolFormationHabilitation.TYPE_ACTION_REDOUBLEMENT);
						scolFormationHabilitation.setIsSelectionnable(Boolean.TRUE);
						scolFormationHabilitation.setIsPassageAMinima(scolFormationProgression.isPassageAMinima());
					}
					scolFormationHabilitation.setTypeAssociation(scolFormationProgression.typeAssociation());
					if (formationsEnvisageables.containsObject(scolFormationHabilitation) == false) {
						formationsEnvisageables.addObject(scolFormationHabilitation);
					}
				}
			}
		}
		if (isInscription()) {
			NSArray<EOScolFormationHabilitation> a = FinderScolFormationHabilitation.getScolFormationHabilitations(edc(), anneeInscriptionEnCours(),
					null);
			if (a != null) {
				Enumeration<EOScolFormationHabilitation> e = a.objectEnumerator();
				int i = 0;
				while (e.hasMoreElements()) {
					EOScolFormationHabilitation scolFormationHabilitation = e.nextElement();
					scolFormationHabilitation.setTypeAction(EOScolFormationHabilitation.TYPE_ACTION_PROGRESSION);
					scolFormationHabilitation.setTypeAssociation(new Integer(i++));
					scolFormationHabilitation.setIsSelectionnable(Boolean.TRUE);
					formationsEnvisageables.addObject(scolFormationHabilitation);
				}
			}
		}
	}

	private void initFormationsPossibles() {
		formationsPossibles = new NSMutableArray<EOScolFormationHabilitation>();
		if (isPreInscription() && anneeInscriptionEnCours() != null) {
			NSArray<EOScolFormationHabilitation> a = FinderScolFormationHabilitation.getScolFormationHabilitationPostBacs(edc(),
					anneeInscriptionEnCours(), rne().cRne());
			if (a != null) {
				formationsPossibles = new NSMutableArray<EOScolFormationHabilitation>(a);
			}
		}
		if (isReInscription() && anneeInscriptionEnCours() != null) {
			NSArray<EOScolFormationHabilitation> a = FinderScolFormationHabilitation.getScolFormationHabilitations(edc(), anneeInscriptionEnCours(),
					null);
			if (a != null) {
				formationsPossibles = new NSMutableArray<EOScolFormationHabilitation>(a);
			}
		}
		// on ote des formations possibles les formations envisageables
		// if (formationsPossibles != null && formationsPossibles.count() > 0 && formationsEnvisageables != null &&
		// formationsEnvisageables.count() > 0) {
		// formationsPossibles.removeObjectsInArray(formationsEnvisageables);
		// }
	}

	private void initFormationsEnvisagees() {
		formationsEnvisagees = new NSMutableArray();
		// Initialisation des EOScolFormationHabilitation
		if ((isPreInscription() || isPreReInscription()) && anneeInscriptionEnCours() != null) {
			NSArray<EOInscDipl> inscDipls = historique(anneeInscriptionEnCours()).toInscDipls();
			if (inscDipls != null && inscDipls.count() > 0) {
				for (int i = 0; i < inscDipls.count(); i++) {
					EOInscDipl inscDipl = inscDipls.objectAtIndex(i);
					EOScolFormationHabilitation scolFormationHabilitation = inscDipl.scolFormationHabilitation();
					if (scolFormationHabilitation != null) {
						// l'objet scolFormationHabilitation est déjà initialisé avec les bons types action et
						// association puisque c'est l'instance de l'objet qu'on avait initialisée pour les
						// formations envisageables
						formationsEnvisagees.addObject(scolFormationHabilitation);
					}
				}
			}
		}
		// Initialisation des EOInscDipl
		if ((isReInscription() || isInscription()) && anneeInscriptionEnCours() != null) {
			NSArray<EOInscDipl> inscDipls = historique(anneeInscriptionEnCours()).toInscDipls();
			if (inscDipls != null && inscDipls.count() > 0) {
				formationsEnvisagees.addObjectsFromArray(inscDipls);
			}
		}
	}

	private void postInitFormations() {
		if (isPreReInscription()) {
			oterFormationsDejaEnvisageesEnPreInscription();
		}
		if (isReInscription() || isInscription()) {
			oterFormationsDejaEnvisageesEnInscription();
		}
		trierFormationsEnvisagees();

		if (isPreReInscription() || isReInscription()) {
			positionnerMarqueurEnvisageablesEnReInscription();
		}
		if (formationsEnvisagees != null && formationsEnvisagees.count() > 0) {
			positionnerMarqueurEnvisagees();
		}

		if (formationsEnvisagees != null && formationsEnvisagees.count() > 0) {
			if (isPreReInscription() || isPreInscription()) {
				positionnerTagMinimaEnvisageesEnPreInsc();
			}
			if (isReInscription() || isInscription()) {
				positionnerTagMinimaEnvisageesEnInsc();
			}
		}
	}

	private void oterFormationsDejaEnvisageesEnPreInscription() {
		// on ote des formations envisageables les formations deja envisagees (dans le cas d'un etudiant deja
		// pre-inscrit)
		if (formationsEnvisagees != null && formationsEnvisagees.count() > 0 && formationsEnvisageables != null
				&& formationsEnvisageables.count() > 0) {
			formationsEnvisageables.removeObjectsInArray(formationsEnvisagees);
		}
		// on ote des formations envisageables les formations de meme typeAction+typeAssociation qui sont deja dans
		// les formations envisagees
		if (formationsEnvisagees != null && formationsEnvisagees.count() > 0 && formationsEnvisageables != null
				&& formationsEnvisageables.count() > 0) {
			for (int i = 0; i < formationsEnvisagees.count(); i++) {
				EOScolFormationHabilitation formationEnvisagee = (EOScolFormationHabilitation) formationsEnvisagees.objectAtIndex(i);
				String typeAction = formationEnvisagee.typeAction();
				Integer typeAssociation = formationEnvisagee.typeAssociation();
				if (typeAction != null && typeAssociation != null) {
					EOKeyValueQualifier qualTypeAction = new EOKeyValueQualifier(EOScolFormationHabilitation.TYPE_ACTION_KEY,
							EOKeyValueQualifier.QualifierOperatorEqual, typeAction);
					EOKeyValueQualifier qualTypeAssociation = new EOKeyValueQualifier(EOScolFormationHabilitation.TYPE_ASSOCIATION_KEY,
							EOKeyValueQualifier.QualifierOperatorEqual, typeAssociation);
					EOAndQualifier qual = new EOAndQualifier(new NSArray<EOQualifier>(
							new EOKeyValueQualifier[] { qualTypeAction, qualTypeAssociation }));
					NSArray<EOScolFormationHabilitation> formationsEnvisageablesASupprimer = EOQualifier.filteredArrayWithQualifier(
							formationsEnvisageables, qual);
					formationsEnvisageables.removeObjectsInArray(formationsEnvisageablesASupprimer);
				}
			}
		}
	}

	private void oterFormationsDejaEnvisageesEnInscription() {
		// on ote des formations envisageables les formations deja envisagees
		if (formationsEnvisagees != null && formationsEnvisagees.count() > 0 && formationsEnvisageables != null
				&& formationsEnvisageables.count() > 0) {
			formationsEnvisageables.removeObjectsInArray((NSArray<EOScolFormationHabilitation>) formationsEnvisagees
					.valueForKey(EOInscDipl.SCOL_FORMATION_HABILITATION_KEY));
		}
		// on ote des formations possibles les formations deja envisagees (dans le cas d'un etudiant deja
		// pre-inscrit)
		if (formationsEnvisagees != null && formationsEnvisagees.count() > 0 && formationsPossibles != null && formationsPossibles.count() > 0) {
			formationsPossibles.removeObjectsInArray((NSArray<EOScolFormationHabilitation>) formationsEnvisagees
					.valueForKey(EOInscDipl.SCOL_FORMATION_HABILITATION_KEY));
		}
		// on ote des formations envisageables les formations de meme typeAction+typeAssociation qui sont deja dans
		// les formations envisagees
		if (formationsEnvisagees != null && formationsEnvisagees.count() > 0 && formationsEnvisageables != null
				&& formationsEnvisageables.count() > 0) {
			for (int i = 0; i < formationsEnvisagees.count(); i++) {
				EOScolFormationHabilitation formationEnvisagee = ((EOInscDipl) formationsEnvisagees.objectAtIndex(i)).scolFormationHabilitation();
				String typeAction = formationEnvisagee.typeAction();
				Integer typeAssociation = formationEnvisagee.typeAssociation();
				if (typeAction != null && typeAssociation != null) {
					EOKeyValueQualifier qualTypeAction = new EOKeyValueQualifier(EOScolFormationHabilitation.TYPE_ACTION_KEY,
							EOKeyValueQualifier.QualifierOperatorEqual, typeAction);
					EOKeyValueQualifier qualTypeAssociation = new EOKeyValueQualifier(EOScolFormationHabilitation.TYPE_ASSOCIATION_KEY,
							EOKeyValueQualifier.QualifierOperatorEqual, typeAssociation);
					EOAndQualifier qual = new EOAndQualifier(new NSArray<EOQualifier>(
							new EOKeyValueQualifier[] { qualTypeAction, qualTypeAssociation }));
					NSArray<EOScolFormationHabilitation> formationsEnvisageablesASupprimer = EOQualifier.filteredArrayWithQualifier(
							formationsEnvisageables, qual);
					formationsEnvisageables.removeObjectsInArray(formationsEnvisageablesASupprimer);
				}
			}
		}
	}

	private void trierFormationsEnvisagees() {
		// on trie par niveau descendant pour placer en haut de la liste l'inscription principale
		if (isPreReInscription() || isPreInscription()) {
			if (formationsEnvisagees != null && formationsEnvisagees.count() > 0) {
				EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOScolFormationHabilitation.FHAB_NIVEAU_KEY,
						EOSortOrdering.CompareDescending);
				EOSortOrdering.sortArrayUsingKeyOrderArray(formationsEnvisagees, new NSArray<EOSortOrdering>(sortOrdering));
			}
		}
		if (isReInscription() || isInscription()) {
			if (formationsEnvisagees != null && formationsEnvisagees.count() > 0) {
				EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOInscDipl.SCOL_FORMATION_HABILITATION_KEY + "."
						+ EOScolFormationHabilitation.FHAB_NIVEAU_KEY, EOSortOrdering.CompareDescending);
				EOSortOrdering.sortArrayUsingKeyOrderArray(formationsEnvisagees, new NSArray<EOSortOrdering>(sortOrdering));
			}
		}
	}

	private void positionnerMarqueurEnvisageablesEnReInscription() {
		// Dans le cas de la reinscription, on positionne le marqueur selectionnable des envisageables pour indiquer si
		// on peut ajouter cette formation ou non, fonction des formations envisageables encore presentes (pour forcer a
		// prendre d'abord les redoublements, puis les progressions)
		if (formationsEnvisageables != null && formationsEnvisageables.count() > 0) {
			// si y'a des type action redoublement, les types actions progression ne doivent pas etre valides
			boolean yaRedoublement = ((NSArray<String>) formationsEnvisageables.valueForKeyPath(EOScolFormationHabilitation.TYPE_ACTION_KEY))
					.contains(EOScolFormationHabilitation.TYPE_ACTION_REDOUBLEMENT);
			for (int i = 0; i < formationsEnvisageables.count(); i++) {
				EOScolFormationHabilitation formationEnvisageable = formationsEnvisageables.objectAtIndex(i);
				if (EOScolFormationHabilitation.TYPE_ACTION_PROGRESSION.equals(formationEnvisageable.typeAction())) {

					formationEnvisageable.setIsSelectionnable(Boolean.valueOf(!yaRedoublement));
				}
				else {
					formationEnvisageable.setIsSelectionnable(Boolean.TRUE);
				}
			}
		}
	}

	private void positionnerMarqueurEnvisagees() {
		// on positionne le marqueur selectionnable des envisagees pour indiquer si on peut retirer cette formation ou
		// non, fonction des formations envisagees presentes (pour forcer a retirer dans l'ordre d'abord les
		// progressions, puis les redoublements)
		// si y'a des type action progression, les types actions redoublement ne doivent pas pouvoir etre retires
		if (isPreReInscription() || isPreInscription()) {
			boolean yaProgression = ((NSArray<String>) formationsEnvisagees.valueForKeyPath(EOScolFormationHabilitation.TYPE_ACTION_KEY))
					.contains(EOScolFormationHabilitation.TYPE_ACTION_PROGRESSION);
			for (int i = 0; i < formationsEnvisagees.count(); i++) {
				EOScolFormationHabilitation formationEnvisagee = (EOScolFormationHabilitation) formationsEnvisagees.objectAtIndex(i);
				if (EOScolFormationHabilitation.TYPE_ACTION_REDOUBLEMENT.equals(formationEnvisagee.typeAction())) {
					formationEnvisagee.setIsSelectionnable(Boolean.valueOf(!yaProgression));
				}
				else {
					formationEnvisagee.setIsSelectionnable(Boolean.TRUE);
				}
			}
		}
		if (isReInscription() || isInscription()) {
			NSMutableArray ma = new NSMutableArray(formationsEnvisagees.count());
			for (int i = 0; i < formationsEnvisagees.count(); i++) {
				EOInscDipl inscDipl = (EOInscDipl) formationsEnvisagees.objectAtIndex(i);
				EOScolFormationHabilitation fhab = inscDipl.scolFormationHabilitation();
				if (fhab != null) {
					String typeAction = fhab.typeAction();
					if (typeAction != null) {
						ma.addObject(typeAction);
					}
				}
			}
			boolean yaProgression = ma.contains(EOScolFormationHabilitation.TYPE_ACTION_PROGRESSION);
			for (int i = 0; i < formationsEnvisagees.count(); i++) {
				EOScolFormationHabilitation formationEnvisagee = ((EOInscDipl) formationsEnvisagees.objectAtIndex(i)).scolFormationHabilitation();
				if (formationEnvisagee != null) {
					if (EOScolFormationHabilitation.TYPE_ACTION_REDOUBLEMENT.equals(formationEnvisagee.typeAction())) {
						formationEnvisagee.setIsSelectionnable(Boolean.valueOf(!yaProgression));
					}
					else {
						formationEnvisagee.setIsSelectionnable(Boolean.TRUE);
					}
				}
			}
		}
	}

	private void positionnerTagMinimaEnvisageesEnPreInsc() {
		// positionnement du tag isPassageConditionnel...
		// si une formation a une formation inferieure (en niveau) de meme domaine, c'est du passage conditionnel...
		// ndlr: le tableau des formations envisagees doit etre deja trie par niveau desc...
		for (int i = 0; i < formationsEnvisagees.count(); i++) {
			EOScolFormationHabilitation formationEnvisagee = (EOScolFormationHabilitation) formationsEnvisagees.objectAtIndex(i);
			formationEnvisagee.setIsPassageConditionnel(Boolean.FALSE);
			if (formationEnvisagee.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().departement() != null) {
				IScolFormationDomaine idep = formationEnvisagee.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome()
						.departement();
				if (idep != null) {
					for (int j = i + 1; j < formationsEnvisagees.count(); j++) {
						EOScolFormationHabilitation formationEnvisageePlusUn = (EOScolFormationHabilitation) formationsEnvisagees.objectAtIndex(j);
						if (formationEnvisageePlusUn.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().departement() != null) {
							IScolFormationDomaine idepPlusUn = formationEnvisageePlusUn.toFwkScolarite_ScolFormationSpecialisation()
									.toFwkScolarite_ScolFormationDiplome().departement();
							if (idep.equals(idepPlusUn)) {
								formationEnvisagee.setIsPassageConditionnel(Boolean.TRUE);
								break;
							}
						}
					}
				}
			}
		}
	}

	private void positionnerTagMinimaEnvisageesEnInsc() {
		// positionnement du tag isPassageConditionnel...
		// si une formation a une formation inferieure (en niveau) de meme domaine, c'est du passage conditionnel...
		// ndlr: le tableau des formations envisagees doit etre deja trie par niveau desc...
		for (int i = 0; i < formationsEnvisagees.count(); i++) {
			EOScolFormationHabilitation formationEnvisagee = ((EOInscDipl) formationsEnvisagees.objectAtIndex(i)).scolFormationHabilitation();
			if (formationEnvisagee != null) {
				formationEnvisagee.setIsPassageConditionnel(Boolean.FALSE);
				if (formationEnvisagee.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().departement() != null) {
					IScolFormationDomaine idep = formationEnvisagee.toFwkScolarite_ScolFormationSpecialisation()
							.toFwkScolarite_ScolFormationDiplome().departement();
					if (idep != null) {
						for (int j = i + 1; j < formationsEnvisagees.count(); j++) {
							EOScolFormationHabilitation formationEnvisageePlusUn = ((EOInscDipl) formationsEnvisagees.objectAtIndex(j))
									.scolFormationHabilitation();
							if (formationEnvisageePlusUn.toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome()
									.departement() != null) {
								IScolFormationDomaine idepPlusUn = formationEnvisageePlusUn.toFwkScolarite_ScolFormationSpecialisation()
										.toFwkScolarite_ScolFormationDiplome().departement();
								if (idep.equals(idepPlusUn)) {
									formationEnvisagee.setIsPassageConditionnel(Boolean.TRUE);
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#addFormationEnvisagee(org.cocktail.scolarix.serveur.metier
	 * .eos.EOScolFormationHabilitation)
	 */
	public boolean addFormationEnvisagee(EOScolFormationHabilitation formation, Integer idiplNumero) throws EtudiantException {
		// verification qu'il n'y a pas de pb/changements qui necessiteraient une re-initialisation du tableau...
		boolean ok = false;
		if (formation != null) {
			if (isPreInscription() || isPreReInscription()) {
				if (formationsEnvisagees().containsObject(formation) == true) {
					throw new EtudiantException("Cette formation est déjà choisie dans les formations envisagées.");
				}
			}
			if (isInscription() || isReInscription()) {
				if (((NSArray<EOScolFormationHabilitation>) formationsEnvisagees().valueForKeyPath(EOInscDipl.SCOL_FORMATION_HABILITATION_KEY))
						.containsObject(formation) == true) {
					throw new EtudiantException("Cette formation est déjà choisie dans les formations envisagées.");
				}
			}
			// if (isPreInscription() && formationsEnvisagees().count() == 1) {
			// throw new EtudiantException("Une seule formation envisagée possible.");
			// }
			if ((isPreReInscription() || isReInscription() || isInscription()) && formationsEnvisagees().count() == 3) {
				throw new EtudiantException("Maximum 3 formations envisagées possibles.");
			}
			ok = true;
			if (isPreReInscription() || isReInscription() || isInscription()) {
				ok = formationsEnvisageables().removeObject(formation);
				if (!ok) {
					ok = formationsPossibles().removeObject(formation);
				}
			}
			if (ok) {
				if (isPreInscription() && formationsEnvisagees().count() == 1) {
					formationsEnvisagees.removeAllObjects();
				}
				if (isPreInscription() || isPreReInscription()) {
					formationsEnvisagees().addObject(formation);
				}
				if (isInscription() || isReInscription()) {
					EOInscDipl inscDipl = historique(anneeInscriptionEnCours()).createToInscDiplsRelationship();
					if (idiplNumero != null) {
						inscDipl.setIdiplNumero(idiplNumero);
					}
					inscDipl.setToFwkScolarite_ScolFormationSpecialisationRelationship(formation.toFwkScolarite_ScolFormationSpecialisation());
					inscDipl.setIdiplAnneeSuivie(formation.fhabNiveau());
					inscDipl.setIdiplDateInsc(DateCtrl.now());
					inscDipl.setIdiplPassageConditionnel("N");
					inscDipl.setIdiplDiplomable(new Integer(1));
					inscDipl.setIdiplAmenagement(new Integer(0));
					inscDipl.setToMentionRelationship(null);
					if (formationsEnvisagees().count() == 0) {
						inscDipl.setToTypeInscriptionRelationship(FinderTypeInscription.getTypeInscriptionPrincipale(edc()));
					}
					else {
						inscDipl.setToTypeInscriptionRelationship(FinderTypeInscription.getTypeInscriptionComplementaire(edc()));
					}
					formationsEnvisagees().addObject(inscDipl);
				}
				postInitFormations();
			}
		}
		return ok;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#removeFormationEnvisagee(org.cocktail.scolarix.serveur.metier
	 * .eos.EOScolFormationHabilitation)
	 */
	public boolean removeFormationEnvisagee(EOScolFormationHabilitation formation) {
		// verification qu'il n'y a pas de pb/changements qui necessiteraient une re-initialisation du tableau...
		boolean ok = false;
		if (formation != null) {
			ok = formationsEnvisagees().removeObject(formation);
			if (ok) {
				if (isPreReInscription() || isReInscription()) {
					setFormationsEnvisageables(null);
					initFormations();
				}
			}
		}
		return ok;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.scolarix.serveur.interfaces.IEtudiant#removeFormationEnvisagee(org.cocktail.scolarix.serveur.metier
	 * .eos.EOInscDipl)
	 */
	public boolean removeFormationEnvisagee(EOInscDipl inscDipl) {
		// verification qu'il n'y a pas de pb/changements qui necessiteraient une re-initialisation du tableau...
		boolean ok = false;
		if (inscDipl != null) {
			ok = formationsEnvisagees().removeObject(inscDipl);
			if (ok) {
				if (isPreReInscription() || isReInscription() || isInscription()) {
					historique(anneeInscriptionEnCours()).removeFromToInscDiplsRelationship(inscDipl);
					inscDipl.setScolFormationHabilitation(null);
					edc().deleteObject(inscDipl);
					setFormationsEnvisageables(null);
					if (isReInscription()) {
						setFormationsPossibles(null);
					}
					initFormations();
				}
			}
		}
		return ok;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.cocktail.scolarix.serveur.interfaces.IEtudiant#enregistrer(org.cocktail.fwkcktlwebapp.server.database. _CktlBasicDataBus,
	 * com.webobjects.eocontrol.EOEditingContext)
	 */
	public IEtudiant enregistrer(_CktlBasicDataBus databus, EOEditingContext ec) throws ScolarixFwkException, EtudiantException {
		if (isPreInscription() || isPreReInscription()) {
			String numeroIne = etudCodeIne();
			String cRne = rne().cRne();

			ProcessInscriptionPreAdministrative.enregistrer(databus, ec, this);

			IEtudiant etudiant = null;
			try {
				if (numeroIne == null) {
					throw new EtudiantException("Impossible de récupérer le numéro INE/BEA (etudCodeIne) après enregistrement !");
				}
				etudiant = FinderEtudiant.getEtudiantPreCandidat(ec, FinderPreCandidat.getPreCandidatIne(ec, numeroIne, null), cRne, this);
				if (etudiant != null && etudiant.userInfos() != null && etudiant.userInfos().count() > 0) {
					throw (EtudiantException) etudiant.userInfos().objectAtIndex(0);
				}
			}
			catch (EtudiantException e) {
				if (editingContext() == null) {
					ec.insertObject(this);
				}
				throw new RuntimeException(e);
			}
			return etudiant;
		}
		if (isReInscription() || isInscription()) {
			Integer etudNumero = etudNumero();
			boolean isReinscription = isReInscription();

			String numeroIne = ProcessInscriptionAdministrative.enregistrer(databus, ec, this);

			IEtudiant etudiant = null;
			try {
				if (etudNumero != null) {
					etudiant = FinderEtudiant.getEtudiant(ec, etudNumero, null, null);
				}
				else {
					etudiant = EOEtudiant.fetchByKeyValue(ec, EOEtudiant.ETUD_CODE_INE_KEY, numeroIne);
				}
				etudiant.setEtudType(isReinscription ? EOEtudiant.ETUD_TYPE_RE_INSCRIPTION : EOEtudiant.ETUD_TYPE_INSCRIPTION);
			}
			catch (EtudiantException e) {
				if (editingContext() == null) {
					ec.insertObject(this);
				}
				throw new RuntimeException(e);
			}

			return etudiant;
		}
		if (isAdmission()) {
			Integer numAdm = candidat().candNumAdm();
			ProcessAdmCandidat.enregistrer(databus, ec, this);
			IEtudiant etudiant = null;
			try {
				if (numAdm == null) {
					throw new EtudiantException("Impossible de récupérer le numéro admission (numAdm) après enregistrement !");
				}
				etudiant = FinderEtudiant.getEtudiantAdmission(ec, numAdm);
				if (etudiant != null && etudiant.userInfos() != null && etudiant.userInfos().count() > 0) {
					throw (EtudiantException) etudiant.userInfos().objectAtIndex(0);
				}
			}
			catch (EtudiantException e) {
				if (editingContext() == null) {
					ec.insertObject(this);
				}
				throw new RuntimeException(e);
			}
			return etudiant;
		}
		throw new ScolarixFwkException(
				"Impossible de déterminer le type d'inscription (Pré-Insc, Pré-Ré-Insc, Ré-Insc, Insc ou Admission), impossible d'enregistrer !");
	}

}
