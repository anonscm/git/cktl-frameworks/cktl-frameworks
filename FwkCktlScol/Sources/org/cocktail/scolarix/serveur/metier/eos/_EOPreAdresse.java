/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreAdresse.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOPreAdresse extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_PreAdresse";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.PRE_ADRESSE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "adrOrdre";

	public static final ERXKey<String> ADR_ADRESSE1 = new ERXKey<String>("adrAdresse1");
	public static final ERXKey<String> ADR_ADRESSE2 = new ERXKey<String>("adrAdresse2");
	public static final ERXKey<String> ADR_BP = new ERXKey<String>("adrBp");
	public static final ERXKey<String> ADR_LISTE_ROUGE = new ERXKey<String>("adrListeRouge");
	public static final ERXKey<String> ADR_URL_PERE = new ERXKey<String>("adrUrlPere");
	public static final ERXKey<String> ADR_URL_RELATIVE = new ERXKey<String>("adrUrlRelative");
	public static final ERXKey<String> ADR_URL_TEMPLATE = new ERXKey<String>("adrUrlTemplate");
	public static final ERXKey<String> BIS_TER = new ERXKey<String>("bisTer");
	public static final ERXKey<String> C_IMPLANTATION = new ERXKey<String>("cImplantation");
	public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
	public static final ERXKey<String> C_PAYS = new ERXKey<String>("cPays");
	public static final ERXKey<String> CP_ETRANGER = new ERXKey<String>("cpEtranger");
	public static final ERXKey<String> C_VOIE = new ERXKey<String>("cVoie");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
	public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> HABITANT_CHEZ = new ERXKey<String>("habitantChez");
	public static final ERXKey<String> LOCALITE = new ERXKey<String>("localite");
	public static final ERXKey<String> NOM_VOIE = new ERXKey<String>("nomVoie");
	public static final ERXKey<String> NO_VOIE = new ERXKey<String>("noVoie");
	public static final ERXKey<String> TEM_PAYE_UTIL = new ERXKey<String>("temPayeUtil");
	public static final ERXKey<String> VILLE = new ERXKey<String>("ville");

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_BP_KEY = "adrBp";
	public static final String ADR_LISTE_ROUGE_KEY = "adrListeRouge";
	public static final String ADR_URL_PERE_KEY = "adrUrlPere";
	public static final String ADR_URL_RELATIVE_KEY = "adrUrlRelative";
	public static final String ADR_URL_TEMPLATE_KEY = "adrUrlTemplate";
	public static final String BIS_TER_KEY = "bisTer";
	public static final String C_IMPLANTATION_KEY = "cImplantation";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String C_PAYS_KEY = "cPays";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String C_VOIE_KEY = "cVoie";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String HABITANT_CHEZ_KEY = "habitantChez";
	public static final String LOCALITE_KEY = "localite";
	public static final String NOM_VOIE_KEY = "nomVoie";
	public static final String NO_VOIE_KEY = "noVoie";
	public static final String TEM_PAYE_UTIL_KEY = "temPayeUtil";
	public static final String VILLE_KEY = "ville";

	// Non visible attributes
	public static final String ADR_ORDRE_KEY = "adrOrdre";

	// Colkeys
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_BP_COLKEY = "ADR_BP";
	public static final String ADR_LISTE_ROUGE_COLKEY = "ADR_LISTE_ROUGE";
	public static final String ADR_URL_PERE_COLKEY = "ADR_URL_PERE";
	public static final String ADR_URL_RELATIVE_COLKEY = "ADR_URL_RELATIVE";
	public static final String ADR_URL_TEMPLATE_COLKEY = "ADR_URL_TEMPLATE";
	public static final String BIS_TER_COLKEY = "BIS_TER";
	public static final String C_IMPLANTATION_COLKEY = "C_IMPLANTATION";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String C_PAYS_COLKEY = "C_PAYS";
	public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
	public static final String C_VOIE_COLKEY = "C_VOIE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String HABITANT_CHEZ_COLKEY = "HABITANT_CHEZ";
	public static final String LOCALITE_COLKEY = "LOCALITE";
	public static final String NOM_VOIE_COLKEY = "NOM_VOIE";
	public static final String NO_VOIE_COLKEY = "NO_VOIE";
	public static final String TEM_PAYE_UTIL_COLKEY = "TEM_PAYE_UTIL";
	public static final String VILLE_COLKEY = "VILLE";

	// Non visible colkeys
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays");

	public static final String TO_FWKPERS__PAYS_KEY = "toFwkpers_Pays";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOPreAdresse with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param adrListeRouge
	 * @param cPays
	 * @param dCreation
	 * @param dDebVal
	 * @param dModification
	 * @param temPayeUtil
	 * @param toFwkpers_Pays
	 * @return EOPreAdresse
	 */
	public static EOPreAdresse create(EOEditingContext editingContext, String adrListeRouge, String cPays, NSTimestamp dCreation, NSTimestamp dDebVal, NSTimestamp dModification, String temPayeUtil, org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays) {
		EOPreAdresse eo = (EOPreAdresse) createAndInsertInstance(editingContext);
		eo.setAdrListeRouge(adrListeRouge);
		eo.setCPays(cPays);
		eo.setDCreation(dCreation);
		eo.setDDebVal(dDebVal);
		eo.setDModification(dModification);
		eo.setTemPayeUtil(temPayeUtil);
		eo.setToFwkpers_PaysRelationship(toFwkpers_Pays);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOPreAdresse.
	 *
	 * @param editingContext
	 * @return EOPreAdresse
	 */
	public static EOPreAdresse create(EOEditingContext editingContext) {
		EOPreAdresse eo = (EOPreAdresse) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOPreAdresse localInstanceIn(EOEditingContext editingContext) {
		EOPreAdresse localInstance = (EOPreAdresse) localInstanceOfObject(editingContext, (EOPreAdresse) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOPreAdresse localInstanceIn(EOEditingContext editingContext, EOPreAdresse eo) {
		EOPreAdresse localInstance = (eo == null) ? null : (EOPreAdresse) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String adrAdresse1() {
		return (String) storedValueForKey("adrAdresse1");
	}

	public void setAdrAdresse1(String value) {
		takeStoredValueForKey(value, "adrAdresse1");
	}
	public String adrAdresse2() {
		return (String) storedValueForKey("adrAdresse2");
	}

	public void setAdrAdresse2(String value) {
		takeStoredValueForKey(value, "adrAdresse2");
	}
	public String adrBp() {
		return (String) storedValueForKey("adrBp");
	}

	public void setAdrBp(String value) {
		takeStoredValueForKey(value, "adrBp");
	}
	public String adrListeRouge() {
		return (String) storedValueForKey("adrListeRouge");
	}

	public void setAdrListeRouge(String value) {
		takeStoredValueForKey(value, "adrListeRouge");
	}
	public String adrUrlPere() {
		return (String) storedValueForKey("adrUrlPere");
	}

	public void setAdrUrlPere(String value) {
		takeStoredValueForKey(value, "adrUrlPere");
	}
	public String adrUrlRelative() {
		return (String) storedValueForKey("adrUrlRelative");
	}

	public void setAdrUrlRelative(String value) {
		takeStoredValueForKey(value, "adrUrlRelative");
	}
	public String adrUrlTemplate() {
		return (String) storedValueForKey("adrUrlTemplate");
	}

	public void setAdrUrlTemplate(String value) {
		takeStoredValueForKey(value, "adrUrlTemplate");
	}
	public String bisTer() {
		return (String) storedValueForKey("bisTer");
	}

	public void setBisTer(String value) {
		takeStoredValueForKey(value, "bisTer");
	}
	public String cImplantation() {
		return (String) storedValueForKey("cImplantation");
	}

	public void setCImplantation(String value) {
		takeStoredValueForKey(value, "cImplantation");
	}
	public String codePostal() {
		return (String) storedValueForKey("codePostal");
	}

	public void setCodePostal(String value) {
		takeStoredValueForKey(value, "codePostal");
	}
	public String cPays() {
		return (String) storedValueForKey("cPays");
	}

	public void setCPays(String value) {
		takeStoredValueForKey(value, "cPays");
	}
	public String cpEtranger() {
		return (String) storedValueForKey("cpEtranger");
	}

	public void setCpEtranger(String value) {
		takeStoredValueForKey(value, "cpEtranger");
	}
	public String cVoie() {
		return (String) storedValueForKey("cVoie");
	}

	public void setCVoie(String value) {
		takeStoredValueForKey(value, "cVoie");
	}
	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}
	public NSTimestamp dDebVal() {
		return (NSTimestamp) storedValueForKey("dDebVal");
	}

	public void setDDebVal(NSTimestamp value) {
		takeStoredValueForKey(value, "dDebVal");
	}
	public NSTimestamp dFinVal() {
		return (NSTimestamp) storedValueForKey("dFinVal");
	}

	public void setDFinVal(NSTimestamp value) {
		takeStoredValueForKey(value, "dFinVal");
	}
	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	public String habitantChez() {
		return (String) storedValueForKey("habitantChez");
	}

	public void setHabitantChez(String value) {
		takeStoredValueForKey(value, "habitantChez");
	}
	public String localite() {
		return (String) storedValueForKey("localite");
	}

	public void setLocalite(String value) {
		takeStoredValueForKey(value, "localite");
	}
	public String nomVoie() {
		return (String) storedValueForKey("nomVoie");
	}

	public void setNomVoie(String value) {
		takeStoredValueForKey(value, "nomVoie");
	}
	public String noVoie() {
		return (String) storedValueForKey("noVoie");
	}

	public void setNoVoie(String value) {
		takeStoredValueForKey(value, "noVoie");
	}
	public String temPayeUtil() {
		return (String) storedValueForKey("temPayeUtil");
	}

	public void setTemPayeUtil(String value) {
		takeStoredValueForKey(value, "temPayeUtil");
	}
	public String ville() {
		return (String) storedValueForKey("ville");
	}

	public void setVille(String value) {
		takeStoredValueForKey(value, "ville");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays");
	}

	public void setToFwkpers_PaysRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOPreAdresse.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOPreAdresse.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOPreAdresse)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOPreAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOPreAdresse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOPreAdresse)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOPreAdresse fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOPreAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOPreAdresse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOPreAdresse)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreAdresse fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOPreAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOPreAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOPreAdresse ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOPreAdresse createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOPreAdresse.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOPreAdresse.ENTITY_NAME + "' !");
		}
		else {
			EOPreAdresse object = (EOPreAdresse) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOPreAdresse localInstanceOfObject(EOEditingContext ec, EOPreAdresse object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOPreAdresse " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOPreAdresse) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
