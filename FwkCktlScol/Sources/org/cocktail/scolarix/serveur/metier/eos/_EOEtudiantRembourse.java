/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEtudiantRembourse.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOEtudiantRembourse extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_EtudiantRembourse";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.ETUDIANT_REMBOURSE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "erembNumero";

	public static final ERXKey<Integer> BOR_ORDRE = new ERXKey<Integer>("borOrdre");
	public static final ERXKey<String> EREMB_ANNEE = new ERXKey<String>("erembAnnee");
	public static final ERXKey<String> EREMB_ETAT = new ERXKey<String>("erembEtat");
	public static final ERXKey<String> EREMB_MOTIF = new ERXKey<String>("erembMotif");
	public static final ERXKey<Integer> EREMB_NUM_BORDEREAU = new ERXKey<Integer>("erembNumBordereau");
	public static final ERXKey<Integer> EREMB_NUMERO = new ERXKey<Integer>("erembNumero");
	public static final ERXKey<java.math.BigDecimal> EREMB_SOMME = new ERXKey<java.math.BigDecimal>("erembSomme");
	public static final ERXKey<Integer> JOU_ORDRE = new ERXKey<Integer>("jouOrdre");

	public static final String BOR_ORDRE_KEY = "borOrdre";
	public static final String EREMB_ANNEE_KEY = "erembAnnee";
	public static final String EREMB_ETAT_KEY = "erembEtat";
	public static final String EREMB_MOTIF_KEY = "erembMotif";
	public static final String EREMB_NUM_BORDEREAU_KEY = "erembNumBordereau";
	public static final String EREMB_NUMERO_KEY = "erembNumero";
	public static final String EREMB_SOMME_KEY = "erembSomme";
	public static final String JOU_ORDRE_KEY = "jouOrdre";

	// Non visible attributes
	public static final String ETAB_CODE_KEY = "etabCode";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String RIB_ORDRE_KEY = "ribOrdre";

	// Colkeys
	public static final String BOR_ORDRE_COLKEY = "BOR_ORDRE";
	public static final String EREMB_ANNEE_COLKEY = "EREMB_ANNEE";
	public static final String EREMB_ETAT_COLKEY = "EREMB_ETAT";
	public static final String EREMB_MOTIF_COLKEY = "EREMB_MOTIF";
	public static final String EREMB_NUM_BORDEREAU_COLKEY = "EREMB_NUM_BORDEREAU";
	public static final String EREMB_NUMERO_COLKEY = "EREMB_NUMERO";
	public static final String EREMB_SOMME_COLKEY = "EREMB_SOMME";
	public static final String JOU_ORDRE_COLKEY = "JOU_ORDRE";

	// Non visible colkeys
	public static final String ETAB_CODE_COLKEY = "ETAB_CODE";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement> TO_DETAIL_PAIEMENTS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement>("toDetailPaiements");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse> TO_DETAIL_REMBOURSES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse>("toDetailRembourses");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib> TO_FWKPERS__RIB = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib>("toFwkpers_Rib");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique> TO_HISTORIQUE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique>("toHistorique");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite> TO_V_ETABLISSEMENT_SCOLARITE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite>("toVEtablissementScolarite");

	public static final String TO_DETAIL_PAIEMENTS_KEY = "toDetailPaiements";
	public static final String TO_DETAIL_REMBOURSES_KEY = "toDetailRembourses";
	public static final String TO_FWKPERS__RIB_KEY = "toFwkpers_Rib";
	public static final String TO_HISTORIQUE_KEY = "toHistorique";
	public static final String TO_V_ETABLISSEMENT_SCOLARITE_KEY = "toVEtablissementScolarite";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOEtudiantRembourse with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param erembAnnee
	 * @param erembNumero
	 * @param erembSomme
	 * @param toHistorique
	 * @param toVEtablissementScolarite
	 * @return EOEtudiantRembourse
	 */
	public static EOEtudiantRembourse create(EOEditingContext editingContext, String erembAnnee, Integer erembNumero, java.math.BigDecimal erembSomme, org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique, org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite toVEtablissementScolarite) {
		EOEtudiantRembourse eo = (EOEtudiantRembourse) createAndInsertInstance(editingContext);
		eo.setErembAnnee(erembAnnee);
		eo.setErembNumero(erembNumero);
		eo.setErembSomme(erembSomme);
		eo.setToHistoriqueRelationship(toHistorique);
		eo.setToVEtablissementScolariteRelationship(toVEtablissementScolarite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOEtudiantRembourse.
	 *
	 * @param editingContext
	 * @return EOEtudiantRembourse
	 */
	public static EOEtudiantRembourse create(EOEditingContext editingContext) {
		EOEtudiantRembourse eo = (EOEtudiantRembourse) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOEtudiantRembourse localInstanceIn(EOEditingContext editingContext) {
		EOEtudiantRembourse localInstance = (EOEtudiantRembourse) localInstanceOfObject(editingContext, (EOEtudiantRembourse) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOEtudiantRembourse localInstanceIn(EOEditingContext editingContext, EOEtudiantRembourse eo) {
		EOEtudiantRembourse localInstance = (eo == null) ? null : (EOEtudiantRembourse) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer borOrdre() {
		return (Integer) storedValueForKey("borOrdre");
	}

	public void setBorOrdre(Integer value) {
		takeStoredValueForKey(value, "borOrdre");
	}
	public String erembAnnee() {
		return (String) storedValueForKey("erembAnnee");
	}

	public void setErembAnnee(String value) {
		takeStoredValueForKey(value, "erembAnnee");
	}
	public String erembEtat() {
		return (String) storedValueForKey("erembEtat");
	}

	public void setErembEtat(String value) {
		takeStoredValueForKey(value, "erembEtat");
	}
	public String erembMotif() {
		return (String) storedValueForKey("erembMotif");
	}

	public void setErembMotif(String value) {
		takeStoredValueForKey(value, "erembMotif");
	}
	public Integer erembNumBordereau() {
		return (Integer) storedValueForKey("erembNumBordereau");
	}

	public void setErembNumBordereau(Integer value) {
		takeStoredValueForKey(value, "erembNumBordereau");
	}
	public Integer erembNumero() {
		return (Integer) storedValueForKey("erembNumero");
	}

	public void setErembNumero(Integer value) {
		takeStoredValueForKey(value, "erembNumero");
	}
	public java.math.BigDecimal erembSomme() {
		return (java.math.BigDecimal) storedValueForKey("erembSomme");
	}

	public void setErembSomme(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "erembSomme");
	}
	public Integer jouOrdre() {
		return (Integer) storedValueForKey("jouOrdre");
	}

	public void setJouOrdre(Integer value) {
		takeStoredValueForKey(value, "jouOrdre");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EORib toFwkpers_Rib() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EORib)storedValueForKey("toFwkpers_Rib");
	}

	public void setToFwkpers_RibRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EORib oldValue = toFwkpers_Rib();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Rib");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Rib");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOHistorique)storedValueForKey("toHistorique");
	}

	public void setToHistoriqueRelationship(org.cocktail.scolarix.serveur.metier.eos.EOHistorique value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOHistorique oldValue = toHistorique();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toHistorique");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toHistorique");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite toVEtablissementScolarite() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite)storedValueForKey("toVEtablissementScolarite");
	}

	public void setToVEtablissementScolariteRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite oldValue = toVEtablissementScolarite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVEtablissementScolarite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toVEtablissementScolarite");
		}
	}
  
	public NSArray toDetailPaiements() {
		return (NSArray)storedValueForKey("toDetailPaiements");
	}

	public NSArray toDetailPaiements(EOQualifier qualifier) {
		return toDetailPaiements(qualifier, null, false);
	}

	public NSArray toDetailPaiements(EOQualifier qualifier, boolean fetch) {
		return toDetailPaiements(qualifier, null, fetch);
	}

	public NSArray toDetailPaiements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement.TO_ETUDIANT_REMBOURSE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toDetailPaiements();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
	}

	public void removeFromToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement createToDetailPaiementsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_DetailPaiement");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toDetailPaiements");
		return (org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement) eo;
	}

	public void deleteToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
				editingContext().deleteObject(object);
			}

	public void deleteAllToDetailPaiementsRelationships() {
		Enumeration objects = toDetailPaiements().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToDetailPaiementsRelationship((org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement)objects.nextElement());
		}
	}
	public NSArray toDetailRembourses() {
		return (NSArray)storedValueForKey("toDetailRembourses");
	}

	public NSArray toDetailRembourses(EOQualifier qualifier) {
		return toDetailRembourses(qualifier, null, false);
	}

	public NSArray toDetailRembourses(EOQualifier qualifier, boolean fetch) {
		return toDetailRembourses(qualifier, null, fetch);
	}

	public NSArray toDetailRembourses(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse.TO_ETUDIANT_REMBOURSE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toDetailRembourses();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToDetailRemboursesRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toDetailRembourses");
	}

	public void removeFromToDetailRemboursesRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailRembourses");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse createToDetailRemboursesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_DetailRembourse");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toDetailRembourses");
		return (org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse) eo;
	}

	public void deleteToDetailRemboursesRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailRembourses");
				editingContext().deleteObject(object);
			}

	public void deleteAllToDetailRemboursesRelationships() {
		Enumeration objects = toDetailRembourses().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToDetailRemboursesRelationship((org.cocktail.scolarix.serveur.metier.eos.EODetailRembourse)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOEtudiantRembourse.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOEtudiantRembourse.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOEtudiantRembourse)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOEtudiantRembourse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEtudiantRembourse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOEtudiantRembourse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOEtudiantRembourse)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOEtudiantRembourse fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOEtudiantRembourse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOEtudiantRembourse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOEtudiantRembourse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOEtudiantRembourse)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOEtudiantRembourse fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOEtudiantRembourse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOEtudiantRembourse eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOEtudiantRembourse ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOEtudiantRembourse createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOEtudiantRembourse.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOEtudiantRembourse.ENTITY_NAME + "' !");
		}
		else {
			EOEtudiantRembourse object = (EOEtudiantRembourse) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOEtudiantRembourse localInstanceOfObject(EOEditingContext ec, EOEtudiantRembourse object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOEtudiantRembourse " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOEtudiantRembourse) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
