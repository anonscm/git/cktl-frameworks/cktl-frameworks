/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORdvCandidat.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EORdvCandidat extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_RdvCandidat";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.RDV_CANDIDAT";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "codInd";

	public static final ERXKey<String> COD_ETB = new ERXKey<String>("codEtb");
	public static final ERXKey<String> COD_ETP = new ERXKey<String>("codEtp");
	public static final ERXKey<Integer> COD_VRS_VET = new ERXKey<Integer>("codVrsVet");
	public static final ERXKey<NSTimestamp> CONVOC_DATE = new ERXKey<NSTimestamp>("convocDate");
	public static final ERXKey<String> CONVOC_HEURE = new ERXKey<String>("convocHeure");
	public static final ERXKey<NSTimestamp> DATE_EDITION = new ERXKey<NSTimestamp>("dateEdition");
	public static final ERXKey<String> HEURE_EDITION = new ERXKey<String>("heureEdition");
	public static final ERXKey<String> TEM_RDV = new ERXKey<String>("temRdv");

	public static final String COD_ETB_KEY = "codEtb";
	public static final String COD_ETP_KEY = "codEtp";
	public static final String COD_VRS_VET_KEY = "codVrsVet";
	public static final String CONVOC_DATE_KEY = "convocDate";
	public static final String CONVOC_HEURE_KEY = "convocHeure";
	public static final String DATE_EDITION_KEY = "dateEdition";
	public static final String HEURE_EDITION_KEY = "heureEdition";
	public static final String TEM_RDV_KEY = "temRdv";

	// Non visible attributes
	public static final String COD_IND_KEY = "codInd";
	public static final String NUM_OCC_PLANNING_KEY = "numOccPlanning";

	// Colkeys
	public static final String COD_ETB_COLKEY = "COD_ETB";
	public static final String COD_ETP_COLKEY = "COD_ETP";
	public static final String COD_VRS_VET_COLKEY = "COD_VRS_VET";
	public static final String CONVOC_DATE_COLKEY = "CONVOC_DATE";
	public static final String CONVOC_HEURE_COLKEY = "CONVOC_HEURE";
	public static final String DATE_EDITION_COLKEY = "DATE_EDITION";
	public static final String HEURE_EDITION_COLKEY = "HEURE_EDITION";
	public static final String TEM_RDV_COLKEY = "TEM_RDV";

	// Non visible colkeys
	public static final String COD_IND_COLKEY = "COD_IND";
	public static final String NUM_OCC_PLANNING_COLKEY = "NUM_OCC_PLANNING";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningInfo> TO_RDV_PLANNING_INFO = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningInfo>("toRdvPlanningInfo");

	public static final String TO_RDV_PLANNING_INFO_KEY = "toRdvPlanningInfo";

	// Create / Init methods

	/**
	 * Creates and inserts a new EORdvCandidat with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @return EORdvCandidat
	 */
	public static EORdvCandidat create(EOEditingContext editingContext) {
		EORdvCandidat eo = (EORdvCandidat) createAndInsertInstance(editingContext);
		return eo;
	}


	// Utilities methods

	public EORdvCandidat localInstanceIn(EOEditingContext editingContext) {
		EORdvCandidat localInstance = (EORdvCandidat) localInstanceOfObject(editingContext, (EORdvCandidat) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EORdvCandidat localInstanceIn(EOEditingContext editingContext, EORdvCandidat eo) {
		EORdvCandidat localInstance = (eo == null) ? null : (EORdvCandidat) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String codEtb() {
		return (String) storedValueForKey("codEtb");
	}

	public void setCodEtb(String value) {
		takeStoredValueForKey(value, "codEtb");
	}
	public String codEtp() {
		return (String) storedValueForKey("codEtp");
	}

	public void setCodEtp(String value) {
		takeStoredValueForKey(value, "codEtp");
	}
	public Integer codVrsVet() {
		return (Integer) storedValueForKey("codVrsVet");
	}

	public void setCodVrsVet(Integer value) {
		takeStoredValueForKey(value, "codVrsVet");
	}
	public NSTimestamp convocDate() {
		return (NSTimestamp) storedValueForKey("convocDate");
	}

	public void setConvocDate(NSTimestamp value) {
		takeStoredValueForKey(value, "convocDate");
	}
	public String convocHeure() {
		return (String) storedValueForKey("convocHeure");
	}

	public void setConvocHeure(String value) {
		takeStoredValueForKey(value, "convocHeure");
	}
	public NSTimestamp dateEdition() {
		return (NSTimestamp) storedValueForKey("dateEdition");
	}

	public void setDateEdition(NSTimestamp value) {
		takeStoredValueForKey(value, "dateEdition");
	}
	public String heureEdition() {
		return (String) storedValueForKey("heureEdition");
	}

	public void setHeureEdition(String value) {
		takeStoredValueForKey(value, "heureEdition");
	}
	public String temRdv() {
		return (String) storedValueForKey("temRdv");
	}

	public void setTemRdv(String value) {
		takeStoredValueForKey(value, "temRdv");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningInfo toRdvPlanningInfo() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningInfo)storedValueForKey("toRdvPlanningInfo");
	}

	public void setToRdvPlanningInfoRelationship(org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningInfo value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningInfo oldValue = toRdvPlanningInfo();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRdvPlanningInfo");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRdvPlanningInfo");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EORdvCandidat.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EORdvCandidat.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EORdvCandidat)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EORdvCandidat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORdvCandidat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EORdvCandidat eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EORdvCandidat)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EORdvCandidat fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORdvCandidat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORdvCandidat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EORdvCandidat eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EORdvCandidat)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORdvCandidat fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORdvCandidat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EORdvCandidat eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EORdvCandidat ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EORdvCandidat createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EORdvCandidat.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EORdvCandidat.ENTITY_NAME + "' !");
		}
		else {
			EORdvCandidat object = (EORdvCandidat) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EORdvCandidat localInstanceOfObject(EOEditingContext ec, EORdvCandidat object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EORdvCandidat " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EORdvCandidat) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
