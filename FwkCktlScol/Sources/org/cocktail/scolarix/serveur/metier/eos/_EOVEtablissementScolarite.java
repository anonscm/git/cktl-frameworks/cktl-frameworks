/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVEtablissementScolarite.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVEtablissementScolarite extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VEtablissementScolarite";
	public static final String ENTITY_TABLE_NAME = "GRHUM.V_ETABLISSEMENT_SCOLARITE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "cRne";

	public static final ERXKey<String> C_RNE = new ERXKey<String>("cRne");
	public static final ERXKey<String> C_RNE_PERE = new ERXKey<String>("cRnePere");
	public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
	public static final ERXKey<String> C_STRUCTURE_PERE = new ERXKey<String>("cStructurePere");
	public static final ERXKey<String> LC_STRUCTURE = new ERXKey<String>("lcStructure");
	public static final ERXKey<String> LC_STRUCTURE_PERE = new ERXKey<String>("lcStructurePere");
	public static final ERXKey<String> LL_STRUCTURE = new ERXKey<String>("llStructure");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");

	public static final String C_RNE_KEY = "cRne";
	public static final String C_RNE_PERE_KEY = "cRnePere";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LC_STRUCTURE_PERE_KEY = "lcStructurePere";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String PERS_ID_KEY = "persId";

	// Non visible attributes

	// Colkeys
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_RNE_PERE_COLKEY = "C_RNE_PERE";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LC_STRUCTURE_PERE_COLKEY = "LC_STRUCTURE_PERE";
	public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne> TO_RNE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EORne>("toRne");

	public static final String TO_RNE_KEY = "toRne";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVEtablissementScolarite with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cRne
	 * @param toRne
	 * @return EOVEtablissementScolarite
	 */
	public static EOVEtablissementScolarite create(EOEditingContext editingContext, String cRne, org.cocktail.scolarix.serveur.metier.eos.EORne toRne) {
		EOVEtablissementScolarite eo = (EOVEtablissementScolarite) createAndInsertInstance(editingContext);
		eo.setCRne(cRne);
		eo.setToRneRelationship(toRne);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVEtablissementScolarite.
	 *
	 * @param editingContext
	 * @return EOVEtablissementScolarite
	 */
	public static EOVEtablissementScolarite create(EOEditingContext editingContext) {
		EOVEtablissementScolarite eo = (EOVEtablissementScolarite) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVEtablissementScolarite localInstanceIn(EOEditingContext editingContext) {
		EOVEtablissementScolarite localInstance = (EOVEtablissementScolarite) localInstanceOfObject(editingContext, (EOVEtablissementScolarite) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVEtablissementScolarite localInstanceIn(EOEditingContext editingContext, EOVEtablissementScolarite eo) {
		EOVEtablissementScolarite localInstance = (eo == null) ? null : (EOVEtablissementScolarite) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cRne() {
		return (String) storedValueForKey("cRne");
	}

	public void setCRne(String value) {
		takeStoredValueForKey(value, "cRne");
	}
	public String cRnePere() {
		return (String) storedValueForKey("cRnePere");
	}

	public void setCRnePere(String value) {
		takeStoredValueForKey(value, "cRnePere");
	}
	public String cStructure() {
		return (String) storedValueForKey("cStructure");
	}

	public void setCStructure(String value) {
		takeStoredValueForKey(value, "cStructure");
	}
	public String cStructurePere() {
		return (String) storedValueForKey("cStructurePere");
	}

	public void setCStructurePere(String value) {
		takeStoredValueForKey(value, "cStructurePere");
	}
	public String lcStructure() {
		return (String) storedValueForKey("lcStructure");
	}

	public void setLcStructure(String value) {
		takeStoredValueForKey(value, "lcStructure");
	}
	public String lcStructurePere() {
		return (String) storedValueForKey("lcStructurePere");
	}

	public void setLcStructurePere(String value) {
		takeStoredValueForKey(value, "lcStructurePere");
	}
	public String llStructure() {
		return (String) storedValueForKey("llStructure");
	}

	public void setLlStructure(String value) {
		takeStoredValueForKey(value, "llStructure");
	}
	public Integer persId() {
		return (Integer) storedValueForKey("persId");
	}

	public void setPersId(Integer value) {
		takeStoredValueForKey(value, "persId");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EORne toRne() {
		return (org.cocktail.scolarix.serveur.metier.eos.EORne)storedValueForKey("toRne");
	}

	public void setToRneRelationship(org.cocktail.scolarix.serveur.metier.eos.EORne value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EORne oldValue = toRne();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toRne");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toRne");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVEtablissementScolarite.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVEtablissementScolarite.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVEtablissementScolarite)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVEtablissementScolarite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVEtablissementScolarite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVEtablissementScolarite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVEtablissementScolarite)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVEtablissementScolarite fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVEtablissementScolarite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVEtablissementScolarite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVEtablissementScolarite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVEtablissementScolarite)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVEtablissementScolarite fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVEtablissementScolarite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVEtablissementScolarite eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVEtablissementScolarite ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVEtablissementScolarite createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVEtablissementScolarite.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVEtablissementScolarite.ENTITY_NAME + "' !");
		}
		else {
			EOVEtablissementScolarite object = (EOVEtablissementScolarite) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVEtablissementScolarite localInstanceOfObject(EOEditingContext ec, EOVEtablissementScolarite object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVEtablissementScolarite " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVEtablissementScolarite) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
