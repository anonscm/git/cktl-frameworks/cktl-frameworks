/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOInscDipl.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOInscDipl extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_InscDipl";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.INSC_DIPL";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "idiplNumero";

	public static final ERXKey<Integer> GRPD_NUMERO = new ERXKey<Integer>("grpdNumero");
	public static final ERXKey<Integer> IDIPL_AMENAGEMENT = new ERXKey<Integer>("idiplAmenagement");
	public static final ERXKey<Integer> IDIPL_ANNEE_SUIVIE = new ERXKey<Integer>("idiplAnneeSuivie");
	public static final ERXKey<NSTimestamp> IDIPL_DATE_DEMISSION = new ERXKey<NSTimestamp>("idiplDateDemission");
	public static final ERXKey<NSTimestamp> IDIPL_DATE_INSC = new ERXKey<NSTimestamp>("idiplDateInsc");
	public static final ERXKey<Integer> IDIPL_DIPLOMABLE = new ERXKey<Integer>("idiplDiplomable");
	public static final ERXKey<java.math.BigDecimal> IDIPL_MOY_ETU = new ERXKey<java.math.BigDecimal>("idiplMoyEtu");
	public static final ERXKey<java.math.BigDecimal> IDIPL_NOTE_PONDERATION = new ERXKey<java.math.BigDecimal>("idiplNotePonderation");
	public static final ERXKey<Integer> IDIPL_NUMERO = new ERXKey<Integer>("idiplNumero");
	public static final ERXKey<Integer> IDIPL_NUMERO_DIPL = new ERXKey<Integer>("idiplNumeroDipl");
	public static final ERXKey<String> IDIPL_PASSAGE_CONDITIONNEL = new ERXKey<String>("idiplPassageConditionnel");
	public static final ERXKey<Integer> IDIPL_RANG = new ERXKey<Integer>("idiplRang");
	public static final ERXKey<Integer> IDIPL_SEMESTRE = new ERXKey<Integer>("idiplSemestre");
	public static final ERXKey<Integer> MSTA_CODE = new ERXKey<Integer>("mstaCode");

	public static final String GRPD_NUMERO_KEY = "grpdNumero";
	public static final String IDIPL_AMENAGEMENT_KEY = "idiplAmenagement";
	public static final String IDIPL_ANNEE_SUIVIE_KEY = "idiplAnneeSuivie";
	public static final String IDIPL_DATE_DEMISSION_KEY = "idiplDateDemission";
	public static final String IDIPL_DATE_INSC_KEY = "idiplDateInsc";
	public static final String IDIPL_DIPLOMABLE_KEY = "idiplDiplomable";
	public static final String IDIPL_MOY_ETU_KEY = "idiplMoyEtu";
	public static final String IDIPL_NOTE_PONDERATION_KEY = "idiplNotePonderation";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String IDIPL_NUMERO_DIPL_KEY = "idiplNumeroDipl";
	public static final String IDIPL_PASSAGE_CONDITIONNEL_KEY = "idiplPassageConditionnel";
	public static final String IDIPL_RANG_KEY = "idiplRang";
	public static final String IDIPL_SEMESTRE_KEY = "idiplSemestre";
	public static final String MSTA_CODE_KEY = "mstaCode";

	// Non visible attributes
	public static final String CUM_CODE_KEY = "cumCode";
	public static final String DSPE_CODE_KEY = "dspeCode";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String IDIPL_TYPE_INSCRIPTION_KEY = "idiplTypeInscription";
	public static final String MENT_CODE_KEY = "mentCode";
	public static final String RES_CODE_KEY = "resCode";
	public static final String TECH_CODE_KEY = "techCode";

	// Colkeys
	public static final String GRPD_NUMERO_COLKEY = "GRPD_NUMERO";
	public static final String IDIPL_AMENAGEMENT_COLKEY = "IDIPL_AMENAGEMENT";
	public static final String IDIPL_ANNEE_SUIVIE_COLKEY = "IDIPL_ANNEE_SUIVIE";
	public static final String IDIPL_DATE_DEMISSION_COLKEY = "IDIPL_DATE_DEMISSION";
	public static final String IDIPL_DATE_INSC_COLKEY = "IDIPL_DATE_INSC";
	public static final String IDIPL_DIPLOMABLE_COLKEY = "IDIPL_DIPLOMABLE";
	public static final String IDIPL_MOY_ETU_COLKEY = "IDIPL_MOY_ETU";
	public static final String IDIPL_NOTE_PONDERATION_COLKEY = "IDIPL_NOTE_PONDERATION";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String IDIPL_NUMERO_DIPL_COLKEY = "IDIPL_NUMERO_DIPL";
	public static final String IDIPL_PASSAGE_CONDITIONNEL_COLKEY = "IDIPL_PASSAGE_CONDITIONNEL";
	public static final String IDIPL_RANG_COLKEY = "IDIPL_RANG";
	public static final String IDIPL_SEMESTRE_COLKEY = "IDIPL_SEMESTRE";
	public static final String MSTA_CODE_COLKEY = "MSTA_CODE";

	// Non visible colkeys
	public static final String CUM_CODE_COLKEY = "CUM_CODE";
	public static final String DSPE_CODE_COLKEY = "DSPE_CODE";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String IDIPL_TYPE_INSCRIPTION_COLKEY = "IDIPL_TYPE_INSCRIPTION";
	public static final String MENT_CODE_COLKEY = "MENT_CODE";
	public static final String RES_CODE_COLKEY = "RES_CODE";
	public static final String TECH_CODE_COLKEY = "TECH_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOCumulatif> TO_CUMULATIF = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOCumulatif>("toCumulatif");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement> TO_DETAIL_PAIEMENTS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement>("toDetailPaiements");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique> TO_HISTORIQUE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOHistorique>("toHistorique");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMention> TO_MENTION = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOMention>("toMention");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOResultat> TO_RESULTAT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOResultat>("toResultat");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeEchange> TO_TYPE_ECHANGE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeEchange>("toTypeEchange");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription> TO_TYPE_INSCRIPTION = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription>("toTypeInscription");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat> TO_V_WEB_INSCRIPTION_RESULTATS = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat>("toVWebInscriptionResultats");

	public static final String TO_CUMULATIF_KEY = "toCumulatif";
	public static final String TO_DETAIL_PAIEMENTS_KEY = "toDetailPaiements";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";
	public static final String TO_HISTORIQUE_KEY = "toHistorique";
	public static final String TO_MENTION_KEY = "toMention";
	public static final String TO_RESULTAT_KEY = "toResultat";
	public static final String TO_TYPE_ECHANGE_KEY = "toTypeEchange";
	public static final String TO_TYPE_INSCRIPTION_KEY = "toTypeInscription";
	public static final String TO_V_WEB_INSCRIPTION_RESULTATS_KEY = "toVWebInscriptionResultats";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOInscDipl with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param idiplAmenagement
	 * @param idiplAnneeSuivie
	 * @param idiplDateInsc
	 * @param idiplDiplomable
	 * @param idiplNumero
	 * @param idiplPassageConditionnel
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @param toHistorique
	 * @param toTypeInscription
	 * @return EOInscDipl
	 */
	public static EOInscDipl create(EOEditingContext editingContext, Integer idiplAmenagement, Integer idiplAnneeSuivie, NSTimestamp idiplDateInsc, Integer idiplDiplomable, Integer idiplNumero, String idiplPassageConditionnel, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation, org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique, org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription toTypeInscription) {
		EOInscDipl eo = (EOInscDipl) createAndInsertInstance(editingContext);
		eo.setIdiplAmenagement(idiplAmenagement);
		eo.setIdiplAnneeSuivie(idiplAnneeSuivie);
		eo.setIdiplDateInsc(idiplDateInsc);
		eo.setIdiplDiplomable(idiplDiplomable);
		eo.setIdiplNumero(idiplNumero);
		eo.setIdiplPassageConditionnel(idiplPassageConditionnel);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		eo.setToHistoriqueRelationship(toHistorique);
		eo.setToTypeInscriptionRelationship(toTypeInscription);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOInscDipl.
	 *
	 * @param editingContext
	 * @return EOInscDipl
	 */
	public static EOInscDipl create(EOEditingContext editingContext) {
		EOInscDipl eo = (EOInscDipl) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOInscDipl localInstanceIn(EOEditingContext editingContext) {
		EOInscDipl localInstance = (EOInscDipl) localInstanceOfObject(editingContext, (EOInscDipl) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOInscDipl localInstanceIn(EOEditingContext editingContext, EOInscDipl eo) {
		EOInscDipl localInstance = (eo == null) ? null : (EOInscDipl) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer grpdNumero() {
		return (Integer) storedValueForKey("grpdNumero");
	}

	public void setGrpdNumero(Integer value) {
		takeStoredValueForKey(value, "grpdNumero");
	}
	public Integer idiplAmenagement() {
		return (Integer) storedValueForKey("idiplAmenagement");
	}

	public void setIdiplAmenagement(Integer value) {
		takeStoredValueForKey(value, "idiplAmenagement");
	}
	public Integer idiplAnneeSuivie() {
		return (Integer) storedValueForKey("idiplAnneeSuivie");
	}

	public void setIdiplAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "idiplAnneeSuivie");
	}
	public NSTimestamp idiplDateDemission() {
		return (NSTimestamp) storedValueForKey("idiplDateDemission");
	}

	public void setIdiplDateDemission(NSTimestamp value) {
		takeStoredValueForKey(value, "idiplDateDemission");
	}
	public NSTimestamp idiplDateInsc() {
		return (NSTimestamp) storedValueForKey("idiplDateInsc");
	}

	public void setIdiplDateInsc(NSTimestamp value) {
		takeStoredValueForKey(value, "idiplDateInsc");
	}
	public Integer idiplDiplomable() {
		return (Integer) storedValueForKey("idiplDiplomable");
	}

	public void setIdiplDiplomable(Integer value) {
		takeStoredValueForKey(value, "idiplDiplomable");
	}
	public java.math.BigDecimal idiplMoyEtu() {
		return (java.math.BigDecimal) storedValueForKey("idiplMoyEtu");
	}

	public void setIdiplMoyEtu(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "idiplMoyEtu");
	}
	public java.math.BigDecimal idiplNotePonderation() {
		return (java.math.BigDecimal) storedValueForKey("idiplNotePonderation");
	}

	public void setIdiplNotePonderation(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "idiplNotePonderation");
	}
	public Integer idiplNumero() {
		return (Integer) storedValueForKey("idiplNumero");
	}

	public void setIdiplNumero(Integer value) {
		takeStoredValueForKey(value, "idiplNumero");
	}
	public Integer idiplNumeroDipl() {
		return (Integer) storedValueForKey("idiplNumeroDipl");
	}

	public void setIdiplNumeroDipl(Integer value) {
		takeStoredValueForKey(value, "idiplNumeroDipl");
	}
	public String idiplPassageConditionnel() {
		return (String) storedValueForKey("idiplPassageConditionnel");
	}

	public void setIdiplPassageConditionnel(String value) {
		takeStoredValueForKey(value, "idiplPassageConditionnel");
	}
	public Integer idiplRang() {
		return (Integer) storedValueForKey("idiplRang");
	}

	public void setIdiplRang(Integer value) {
		takeStoredValueForKey(value, "idiplRang");
	}
	public Integer idiplSemestre() {
		return (Integer) storedValueForKey("idiplSemestre");
	}

	public void setIdiplSemestre(Integer value) {
		takeStoredValueForKey(value, "idiplSemestre");
	}
	public Integer mstaCode() {
		return (Integer) storedValueForKey("mstaCode");
	}

	public void setMstaCode(Integer value) {
		takeStoredValueForKey(value, "mstaCode");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOCumulatif toCumulatif() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOCumulatif)storedValueForKey("toCumulatif");
	}

	public void setToCumulatifRelationship(org.cocktail.scolarix.serveur.metier.eos.EOCumulatif value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOCumulatif oldValue = toCumulatif();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toCumulatif");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toCumulatif");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOHistorique toHistorique() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOHistorique)storedValueForKey("toHistorique");
	}

	public void setToHistoriqueRelationship(org.cocktail.scolarix.serveur.metier.eos.EOHistorique value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOHistorique oldValue = toHistorique();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toHistorique");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toHistorique");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOMention toMention() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOMention)storedValueForKey("toMention");
	}

	public void setToMentionRelationship(org.cocktail.scolarix.serveur.metier.eos.EOMention value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOMention oldValue = toMention();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toMention");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toMention");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOResultat toResultat() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOResultat)storedValueForKey("toResultat");
	}

	public void setToResultatRelationship(org.cocktail.scolarix.serveur.metier.eos.EOResultat value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOResultat oldValue = toResultat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toResultat");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toResultat");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOTypeEchange toTypeEchange() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeEchange)storedValueForKey("toTypeEchange");
	}

	public void setToTypeEchangeRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeEchange value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeEchange oldValue = toTypeEchange();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeEchange");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toTypeEchange");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription toTypeInscription() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription)storedValueForKey("toTypeInscription");
	}

	public void setToTypeInscriptionRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeInscription oldValue = toTypeInscription();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeInscription");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toTypeInscription");
		}
	}
  
	public NSArray toDetailPaiements() {
		return (NSArray)storedValueForKey("toDetailPaiements");
	}

	public NSArray toDetailPaiements(EOQualifier qualifier) {
		return toDetailPaiements(qualifier, null, false);
	}

	public NSArray toDetailPaiements(EOQualifier qualifier, boolean fetch) {
		return toDetailPaiements(qualifier, null, fetch);
	}

	public NSArray toDetailPaiements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement.TO_INSC_DIPL_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toDetailPaiements();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
	}

	public void removeFromToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement createToDetailPaiementsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_DetailPaiement");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toDetailPaiements");
		return (org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement) eo;
	}

	public void deleteToDetailPaiementsRelationship(org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPaiements");
				editingContext().deleteObject(object);
			}

	public void deleteAllToDetailPaiementsRelationships() {
		Enumeration objects = toDetailPaiements().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToDetailPaiementsRelationship((org.cocktail.scolarix.serveur.metier.eos.EODetailPaiement)objects.nextElement());
		}
	}
	public NSArray toVWebInscriptionResultats() {
		return (NSArray)storedValueForKey("toVWebInscriptionResultats");
	}

	public NSArray toVWebInscriptionResultats(EOQualifier qualifier) {
		return toVWebInscriptionResultats(qualifier, null, false);
	}

	public NSArray toVWebInscriptionResultats(EOQualifier qualifier, boolean fetch) {
		return toVWebInscriptionResultats(qualifier, null, fetch);
	}

	public NSArray toVWebInscriptionResultats(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat.TO_INSC_DIPL_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toVWebInscriptionResultats();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToVWebInscriptionResultatsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toVWebInscriptionResultats");
	}

	public void removeFromToVWebInscriptionResultatsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toVWebInscriptionResultats");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat createToVWebInscriptionResultatsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarix_VWebInscriptionResultat");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toVWebInscriptionResultats");
		return (org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat) eo;
	}

	public void deleteToVWebInscriptionResultatsRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toVWebInscriptionResultats");
				editingContext().deleteObject(object);
			}

	public void deleteAllToVWebInscriptionResultatsRelationships() {
		Enumeration objects = toVWebInscriptionResultats().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToVWebInscriptionResultatsRelationship((org.cocktail.scolarix.serveur.metier.eos.EOVWebInscriptionResultat)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOInscDipl.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOInscDipl.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOInscDipl)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOInscDipl fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOInscDipl fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOInscDipl eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOInscDipl)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOInscDipl fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOInscDipl fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOInscDipl fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOInscDipl eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOInscDipl)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOInscDipl fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOInscDipl fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOInscDipl eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOInscDipl ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOInscDipl createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOInscDipl.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOInscDipl.ENTITY_NAME + "' !");
		}
		else {
			EOInscDipl object = (EOInscDipl) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOInscDipl localInstanceOfObject(EOEditingContext ec, EOInscDipl object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOInscDipl " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOInscDipl) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
