/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCotisation.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOCotisation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_Cotisation";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.COTISATION";

	//Attributes

	public static final ERXKey<String> COTI_CODE = new ERXKey<String>("cotiCode");
	public static final ERXKey<Integer> COTI_CODE_NUMBER = new ERXKey<Integer>("cotiCodeNumber");
	public static final ERXKey<Integer> COTI_DEBUT_VALIDITE = new ERXKey<Integer>("cotiDebutValidite");
	public static final ERXKey<Integer> COTI_FIN_VALIDITE = new ERXKey<Integer>("cotiFinValidite");
	public static final ERXKey<String> COTI_LIBELLE = new ERXKey<String>("cotiLibelle");
	public static final ERXKey<String> COTI_VALIDITE = new ERXKey<String>("cotiValidite");

	public static final String COTI_CODE_KEY = "cotiCode";
	public static final String COTI_CODE_NUMBER_KEY = "cotiCodeNumber";
	public static final String COTI_DEBUT_VALIDITE_KEY = "cotiDebutValidite";
	public static final String COTI_FIN_VALIDITE_KEY = "cotiFinValidite";
	public static final String COTI_LIBELLE_KEY = "cotiLibelle";
	public static final String COTI_VALIDITE_KEY = "cotiValidite";

	// Non visible attributes

	// Colkeys
	public static final String COTI_CODE_COLKEY = "COTI_CODE";
	public static final String COTI_CODE_NUMBER_COLKEY = "COTICODENUMBER";
	public static final String COTI_DEBUT_VALIDITE_COLKEY = "COTI_DEBUT_VALIDITE";
	public static final String COTI_FIN_VALIDITE_COLKEY = "COTI_FIN_VALIDITE";
	public static final String COTI_LIBELLE_COLKEY = "COTI_LIBELLE";
	public static final String COTI_VALIDITE_COLKEY = "COTI_VALIDITE";

	// Non visible colkeys

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOCotisation with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cotiCode
	 * @param cotiCodeNumber
	 * @param cotiDebutValidite
	 * @param cotiLibelle
	 * @param cotiValidite
	 * @return EOCotisation
	 */
	public static EOCotisation create(EOEditingContext editingContext, String cotiCode, Integer cotiCodeNumber, Integer cotiDebutValidite, String cotiLibelle, String cotiValidite) {
		EOCotisation eo = (EOCotisation) createAndInsertInstance(editingContext);
		eo.setCotiCode(cotiCode);
		eo.setCotiCodeNumber(cotiCodeNumber);
		eo.setCotiDebutValidite(cotiDebutValidite);
		eo.setCotiLibelle(cotiLibelle);
		eo.setCotiValidite(cotiValidite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOCotisation.
	 *
	 * @param editingContext
	 * @return EOCotisation
	 */
	public static EOCotisation create(EOEditingContext editingContext) {
		EOCotisation eo = (EOCotisation) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOCotisation localInstanceIn(EOEditingContext editingContext) {
		EOCotisation localInstance = (EOCotisation) localInstanceOfObject(editingContext, (EOCotisation) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOCotisation localInstanceIn(EOEditingContext editingContext, EOCotisation eo) {
		EOCotisation localInstance = (eo == null) ? null : (EOCotisation) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cotiCode() {
		return (String) storedValueForKey("cotiCode");
	}

	public void setCotiCode(String value) {
		takeStoredValueForKey(value, "cotiCode");
	}
	public Integer cotiCodeNumber() {
		return (Integer) storedValueForKey("cotiCodeNumber");
	}

	public void setCotiCodeNumber(Integer value) {
		takeStoredValueForKey(value, "cotiCodeNumber");
	}
	public Integer cotiDebutValidite() {
		return (Integer) storedValueForKey("cotiDebutValidite");
	}

	public void setCotiDebutValidite(Integer value) {
		takeStoredValueForKey(value, "cotiDebutValidite");
	}
	public Integer cotiFinValidite() {
		return (Integer) storedValueForKey("cotiFinValidite");
	}

	public void setCotiFinValidite(Integer value) {
		takeStoredValueForKey(value, "cotiFinValidite");
	}
	public String cotiLibelle() {
		return (String) storedValueForKey("cotiLibelle");
	}

	public void setCotiLibelle(String value) {
		takeStoredValueForKey(value, "cotiLibelle");
	}
	public String cotiValidite() {
		return (String) storedValueForKey("cotiValidite");
	}

	public void setCotiValidite(String value) {
		takeStoredValueForKey(value, "cotiValidite");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOCotisation.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOCotisation.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOCotisation)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOCotisation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCotisation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOCotisation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOCotisation)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOCotisation fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOCotisation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOCotisation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOCotisation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOCotisation)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOCotisation fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOCotisation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOCotisation eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOCotisation ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	
	public static NSArray fetchFetchAll(EOEditingContext editingContext, NSDictionary bindings) {
		EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", "FwkScolarix_Cotisation");
		fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
		return (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	}


	// Internal utilities methods for common use (server AND client)...

	private static EOCotisation createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOCotisation.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOCotisation.ENTITY_NAME + "' !");
		}
		else {
			EOCotisation object = (EOCotisation) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOCotisation localInstanceOfObject(EOEditingContext ec, EOCotisation object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOCotisation " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOCotisation) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
