/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVRdvCandidats.java instead.
package org.cocktail.scolarix.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVRdvCandidats extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarix_VRdvCandidats";
	public static final String ENTITY_TABLE_NAME = "GARNUCHE.V_RDV_CANDIDATS";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "codInd";

	public static final ERXKey<String> ADR_NOM = new ERXKey<String>("adrNom");
	public static final ERXKey<String> ADR_PRENOM = new ERXKey<String>("adrPrenom");
	public static final ERXKey<String> COD_ETB = new ERXKey<String>("codEtb");
	public static final ERXKey<String> COD_ETP = new ERXKey<String>("codEtp");
	public static final ERXKey<Integer> COD_IND = new ERXKey<Integer>("codInd");
	public static final ERXKey<Integer> COD_VRS_VET = new ERXKey<Integer>("codVrsVet");
	public static final ERXKey<NSTimestamp> CONVOC_DATE = new ERXKey<NSTimestamp>("convocDate");
	public static final ERXKey<String> CONVOC_DATE_STRING = new ERXKey<String>("convocDateString");
	public static final ERXKey<String> CONVOC_DIPLOME = new ERXKey<String>("convocDiplome");
	public static final ERXKey<String> CONVOC_HEURE = new ERXKey<String>("convocHeure");
	public static final ERXKey<String> CONVOC_HEURE_AFFICHAGE = new ERXKey<String>("convocHeureAffichage");
	public static final ERXKey<NSTimestamp> DATE_EDITION = new ERXKey<NSTimestamp>("dateEdition");
	public static final ERXKey<String> ETUD_CODE_INE = new ERXKey<String>("etudCodeIne");
	public static final ERXKey<NSTimestamp> ETUD_DATE_NAISSANCE = new ERXKey<NSTimestamp>("etudDateNaissance");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<String> FDIP_ABREVIATION = new ERXKey<String>("fdipAbreviation");
	public static final ERXKey<String> FGRA_CODE = new ERXKey<String>("fgraCode");
	public static final ERXKey<Integer> FHAB_NIVEAU = new ERXKey<Integer>("fhabNiveau");
	public static final ERXKey<String> FSPN_LIBELLE = new ERXKey<String>("fspnLibelle");
	public static final ERXKey<String> HEURE_EDITION = new ERXKey<String>("heureEdition");
	public static final ERXKey<Integer> NUM_OCC_PLANNING = new ERXKey<Integer>("numOccPlanning");
	public static final ERXKey<String> TEM_RDV = new ERXKey<String>("temRdv");

	public static final String ADR_NOM_KEY = "adrNom";
	public static final String ADR_PRENOM_KEY = "adrPrenom";
	public static final String COD_ETB_KEY = "codEtb";
	public static final String COD_ETP_KEY = "codEtp";
	public static final String COD_IND_KEY = "codInd";
	public static final String COD_VRS_VET_KEY = "codVrsVet";
	public static final String CONVOC_DATE_KEY = "convocDate";
	public static final String CONVOC_DATE_STRING_KEY = "convocDateString";
	public static final String CONVOC_DIPLOME_KEY = "convocDiplome";
	public static final String CONVOC_HEURE_KEY = "convocHeure";
	public static final String CONVOC_HEURE_AFFICHAGE_KEY = "convocHeureAffichage";
	public static final String DATE_EDITION_KEY = "dateEdition";
	public static final String ETUD_CODE_INE_KEY = "etudCodeIne";
	public static final String ETUD_DATE_NAISSANCE_KEY = "etudDateNaissance";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String FDIP_ABREVIATION_KEY = "fdipAbreviation";
	public static final String FGRA_CODE_KEY = "fgraCode";
	public static final String FHAB_NIVEAU_KEY = "fhabNiveau";
	public static final String FSPN_LIBELLE_KEY = "fspnLibelle";
	public static final String HEURE_EDITION_KEY = "heureEdition";
	public static final String NUM_OCC_PLANNING_KEY = "numOccPlanning";
	public static final String TEM_RDV_KEY = "temRdv";

	// Non visible attributes

	// Colkeys
	public static final String ADR_NOM_COLKEY = "ADR_NOM";
	public static final String ADR_PRENOM_COLKEY = "ADR_PRENOM";
	public static final String COD_ETB_COLKEY = "COD_ETB";
	public static final String COD_ETP_COLKEY = "COD_ETP";
	public static final String COD_IND_COLKEY = "COD_IND";
	public static final String COD_VRS_VET_COLKEY = "COD_VRS_VET";
	public static final String CONVOC_DATE_COLKEY = "CONVOC_DATE";
	public static final String CONVOC_DATE_STRING_COLKEY = "CONVOC_DATE_STRING";
	public static final String CONVOC_DIPLOME_COLKEY = "$attribute.columnName";
	public static final String CONVOC_HEURE_COLKEY = "CONVOC_HEURE";
	public static final String CONVOC_HEURE_AFFICHAGE_COLKEY = "$attribute.columnName";
	public static final String DATE_EDITION_COLKEY = "DATE_EDITION";
	public static final String ETUD_CODE_INE_COLKEY = "ETUD_CODE_INE";
	public static final String ETUD_DATE_NAISSANCE_COLKEY = "ETUD_DATE_NAISSANCE";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String FDIP_ABREVIATION_COLKEY = "FDIP_ABREVIATION";
	public static final String FGRA_CODE_COLKEY = "FGRA_CODE";
	public static final String FHAB_NIVEAU_COLKEY = "FHAB_NIVEAU";
	public static final String FSPN_LIBELLE_COLKEY = "FSPN_LIBELLE";
	public static final String HEURE_EDITION_COLKEY = "HEURE_EDITION";
	public static final String NUM_OCC_PLANNING_COLKEY = "NUM_OCC_PLANNING";
	public static final String TEM_RDV_COLKEY = "TEM_RDV";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant> TO_PRE_ETUDIANT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant>("toPreEtudiant");

	public static final String TO_PRE_ETUDIANT_KEY = "toPreEtudiant";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVRdvCandidats with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param adrPrenom
	 * @param codInd
	 * @param convocDiplome
	 * @param convocHeureAffichage
	 * @param etudNumero
	 * @param fgraCode
	 * @param toPreEtudiant
	 * @return EOVRdvCandidats
	 */
	public static EOVRdvCandidats create(EOEditingContext editingContext, String adrPrenom, Integer codInd, String convocDiplome, String convocHeureAffichage, Integer etudNumero, String fgraCode, org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant toPreEtudiant) {
		EOVRdvCandidats eo = (EOVRdvCandidats) createAndInsertInstance(editingContext);
		eo.setAdrPrenom(adrPrenom);
		eo.setCodInd(codInd);
		eo.setConvocDiplome(convocDiplome);
		eo.setConvocHeureAffichage(convocHeureAffichage);
		eo.setEtudNumero(etudNumero);
		eo.setFgraCode(fgraCode);
		eo.setToPreEtudiantRelationship(toPreEtudiant);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVRdvCandidats.
	 *
	 * @param editingContext
	 * @return EOVRdvCandidats
	 */
	public static EOVRdvCandidats create(EOEditingContext editingContext) {
		EOVRdvCandidats eo = (EOVRdvCandidats) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVRdvCandidats localInstanceIn(EOEditingContext editingContext) {
		EOVRdvCandidats localInstance = (EOVRdvCandidats) localInstanceOfObject(editingContext, (EOVRdvCandidats) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVRdvCandidats localInstanceIn(EOEditingContext editingContext, EOVRdvCandidats eo) {
		EOVRdvCandidats localInstance = (eo == null) ? null : (EOVRdvCandidats) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String adrNom() {
		return (String) storedValueForKey("adrNom");
	}

	public void setAdrNom(String value) {
		takeStoredValueForKey(value, "adrNom");
	}
	public String adrPrenom() {
		return (String) storedValueForKey("adrPrenom");
	}

	public void setAdrPrenom(String value) {
		takeStoredValueForKey(value, "adrPrenom");
	}
	public String codEtb() {
		return (String) storedValueForKey("codEtb");
	}

	public void setCodEtb(String value) {
		takeStoredValueForKey(value, "codEtb");
	}
	public String codEtp() {
		return (String) storedValueForKey("codEtp");
	}

	public void setCodEtp(String value) {
		takeStoredValueForKey(value, "codEtp");
	}
	public Integer codInd() {
		return (Integer) storedValueForKey("codInd");
	}

	public void setCodInd(Integer value) {
		takeStoredValueForKey(value, "codInd");
	}
	public Integer codVrsVet() {
		return (Integer) storedValueForKey("codVrsVet");
	}

	public void setCodVrsVet(Integer value) {
		takeStoredValueForKey(value, "codVrsVet");
	}
	public NSTimestamp convocDate() {
		return (NSTimestamp) storedValueForKey("convocDate");
	}

	public void setConvocDate(NSTimestamp value) {
		takeStoredValueForKey(value, "convocDate");
	}
	public String convocDateString() {
		return (String) storedValueForKey("convocDateString");
	}

	public void setConvocDateString(String value) {
		takeStoredValueForKey(value, "convocDateString");
	}
	public String convocDiplome() {
		return (String) storedValueForKey("convocDiplome");
	}

	public void setConvocDiplome(String value) {
		takeStoredValueForKey(value, "convocDiplome");
	}
	public String convocHeure() {
		return (String) storedValueForKey("convocHeure");
	}

	public void setConvocHeure(String value) {
		takeStoredValueForKey(value, "convocHeure");
	}
	public String convocHeureAffichage() {
		return (String) storedValueForKey("convocHeureAffichage");
	}

	public void setConvocHeureAffichage(String value) {
		takeStoredValueForKey(value, "convocHeureAffichage");
	}
	public NSTimestamp dateEdition() {
		return (NSTimestamp) storedValueForKey("dateEdition");
	}

	public void setDateEdition(NSTimestamp value) {
		takeStoredValueForKey(value, "dateEdition");
	}
	public String etudCodeIne() {
		return (String) storedValueForKey("etudCodeIne");
	}

	public void setEtudCodeIne(String value) {
		takeStoredValueForKey(value, "etudCodeIne");
	}
	public NSTimestamp etudDateNaissance() {
		return (NSTimestamp) storedValueForKey("etudDateNaissance");
	}

	public void setEtudDateNaissance(NSTimestamp value) {
		takeStoredValueForKey(value, "etudDateNaissance");
	}
	public Integer etudNumero() {
		return (Integer) storedValueForKey("etudNumero");
	}

	public void setEtudNumero(Integer value) {
		takeStoredValueForKey(value, "etudNumero");
	}
	public String fdipAbreviation() {
		return (String) storedValueForKey("fdipAbreviation");
	}

	public void setFdipAbreviation(String value) {
		takeStoredValueForKey(value, "fdipAbreviation");
	}
	public String fgraCode() {
		return (String) storedValueForKey("fgraCode");
	}

	public void setFgraCode(String value) {
		takeStoredValueForKey(value, "fgraCode");
	}
	public Integer fhabNiveau() {
		return (Integer) storedValueForKey("fhabNiveau");
	}

	public void setFhabNiveau(Integer value) {
		takeStoredValueForKey(value, "fhabNiveau");
	}
	public String fspnLibelle() {
		return (String) storedValueForKey("fspnLibelle");
	}

	public void setFspnLibelle(String value) {
		takeStoredValueForKey(value, "fspnLibelle");
	}
	public String heureEdition() {
		return (String) storedValueForKey("heureEdition");
	}

	public void setHeureEdition(String value) {
		takeStoredValueForKey(value, "heureEdition");
	}
	public Integer numOccPlanning() {
		return (Integer) storedValueForKey("numOccPlanning");
	}

	public void setNumOccPlanning(Integer value) {
		takeStoredValueForKey(value, "numOccPlanning");
	}
	public String temRdv() {
		return (String) storedValueForKey("temRdv");
	}

	public void setTemRdv(String value) {
		takeStoredValueForKey(value, "temRdv");
	}

	public org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant toPreEtudiant() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant)storedValueForKey("toPreEtudiant");
	}

	public void setToPreEtudiantRelationship(org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOPreEtudiant oldValue = toPreEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toPreEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toPreEtudiant");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVRdvCandidats.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVRdvCandidats.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVRdvCandidats)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVRdvCandidats fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVRdvCandidats fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVRdvCandidats eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVRdvCandidats)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVRdvCandidats fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVRdvCandidats fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVRdvCandidats fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVRdvCandidats eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVRdvCandidats)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVRdvCandidats fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVRdvCandidats fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVRdvCandidats eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVRdvCandidats ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVRdvCandidats createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVRdvCandidats.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVRdvCandidats.ENTITY_NAME + "' !");
		}
		else {
			EOVRdvCandidats object = (EOVRdvCandidats) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVRdvCandidats localInstanceOfObject(EOEditingContext ec, EOVRdvCandidats object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVRdvCandidats " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVRdvCandidats) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
