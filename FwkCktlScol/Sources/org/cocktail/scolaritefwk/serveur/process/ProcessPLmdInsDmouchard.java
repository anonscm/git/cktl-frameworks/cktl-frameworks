/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.process;

import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolaritefwk.serveur.exception.ScolariteFwkException;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitMouchard;
import org.cocktail.scolaritefwk.serveur.procedure.ProcedurePLmdInsDmouchard;

import com.webobjects.eocontrol.EOEditingContext;

public class ProcessPLmdInsDmouchard {

	/**
	 * Enregistre un "mouchard" : action faite au travers d'une procedure sur le user SCOLARITE
	 * Les infos a enregistrer sont passees par l'intermediaire d'un EOScolDroitMouchard 
	 * 
	 * @param databus
	 *            _CktlBasicDataBus permettant de gerer les transactions
	 * @param ec
	 *            editingContext dans lequel on enregistre
	 * @param etudiant
	 *            mouchard a enregistrer
	 * @throws ScolariteFwkException
	 */
	public static void enregistrer(_CktlBasicDataBus databus, EOEditingContext ec, EOScolDroitMouchard mouchard) throws ScolariteFwkException {
		if (mouchard == null) {
			throw new ScolariteFwkException("Il faut un mouchard en parametre (ProcessPLmdInsDmouchard)");
		}
		if (databus == null || !CktlDataBus.isDatabaseConnected()) {
			throw new ScolariteFwkException("Probleme avec le dataBus");
		}

		try {
			// debut de transaction...
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode ProcessPLmdInsDmouchard.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}
			databus.beginTransaction();

			boolean ok = ProcedurePLmdInsDmouchard.enregistrer(databus, mouchard);
			if (!ok) {
				throw new ScolariteFwkException((String) databus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			// commit...
			databus.commitTransaction();

			ec.invalidateAllObjects();
			
		} catch (ScolariteFwkException e) {
			databus.rollbackTransaction();
			throw e;
		} catch (Exception e) {
			databus.rollbackTransaction();
			e.printStackTrace();
			throw new ScolariteFwkException(e.getMessage());
		}
	}

}
