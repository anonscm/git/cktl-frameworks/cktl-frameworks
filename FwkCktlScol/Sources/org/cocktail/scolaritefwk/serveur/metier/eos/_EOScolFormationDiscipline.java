/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolFormationDiscipline.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolFormationDiscipline extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolFormationDiscipline";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_FORMATION_DISCIPLINE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "fdscKey";

	public static final ERXKey<Integer> FANN_DEBUT = new ERXKey<Integer>("fannDebut");
	public static final ERXKey<Integer> FANN_FIN = new ERXKey<Integer>("fannFin");
	public static final ERXKey<String> FDSC_ABREVIATION = new ERXKey<String>("fdscAbreviation");
	public static final ERXKey<String> FDSC_CODE = new ERXKey<String>("fdscCode");
	public static final ERXKey<String> FDSC_LIBELLE = new ERXKey<String>("fdscLibelle");
	public static final ERXKey<Integer> FDSC_NIVEAU = new ERXKey<Integer>("fdscNiveau");
	public static final ERXKey<Integer> FDSC_PERE = new ERXKey<Integer>("fdscPere");
	public static final ERXKey<String> FDSC_VALIDITE = new ERXKey<String>("fdscValidite");

	public static final String FANN_DEBUT_KEY = "fannDebut";
	public static final String FANN_FIN_KEY = "fannFin";
	public static final String FDSC_ABREVIATION_KEY = "fdscAbreviation";
	public static final String FDSC_CODE_KEY = "fdscCode";
	public static final String FDSC_LIBELLE_KEY = "fdscLibelle";
	public static final String FDSC_NIVEAU_KEY = "fdscNiveau";
	public static final String FDSC_PERE_KEY = "fdscPere";
	public static final String FDSC_VALIDITE_KEY = "fdscValidite";

	// Non visible attributes
	public static final String FDSC_KEY_KEY = "fdscKey";

	// Colkeys
	public static final String FANN_DEBUT_COLKEY = "FANN_DEBUT";
	public static final String FANN_FIN_COLKEY = "FANN_FIN";
	public static final String FDSC_ABREVIATION_COLKEY = "FDSC_ABREVIATION";
	public static final String FDSC_CODE_COLKEY = "FDSC_CODE";
	public static final String FDSC_LIBELLE_COLKEY = "FDSC_LIBELLE";
	public static final String FDSC_NIVEAU_COLKEY = "FDSC_NIVEAU";
	public static final String FDSC_PERE_COLKEY = "FDSC_PERE";
	public static final String FDSC_VALIDITE_COLKEY = "FDSC_VALIDITE";

	// Non visible colkeys
	public static final String FDSC_KEY_COLKEY = "FDSC_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire> TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire>("toFwkScolarite_ScolFormationDisciplinaires");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline> TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline>("toFwkScolarite_ScolFormationDiscipline");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline> TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline>("toFwkScolarite_ScolFormationDisciplines");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES_KEY = "toFwkScolarite_ScolFormationDisciplinaires";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE_KEY = "toFwkScolarite_ScolFormationDiscipline";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINES_KEY = "toFwkScolarite_ScolFormationDisciplines";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolFormationDiscipline with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fannDebut
	 * @param fannFin
	 * @param fdscCode
	 * @param fdscLibelle
	 * @param fdscNiveau
	 * @param fdscPere
	 * @param fdscValidite
	 * @param toFwkScolarite_ScolFormationDiscipline
	 * @return EOScolFormationDiscipline
	 */
	public static EOScolFormationDiscipline create(EOEditingContext editingContext, Integer fannDebut, Integer fannFin, String fdscCode, String fdscLibelle, Integer fdscNiveau, Integer fdscPere, String fdscValidite, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline toFwkScolarite_ScolFormationDiscipline) {
		EOScolFormationDiscipline eo = (EOScolFormationDiscipline) createAndInsertInstance(editingContext);
		eo.setFannDebut(fannDebut);
		eo.setFannFin(fannFin);
		eo.setFdscCode(fdscCode);
		eo.setFdscLibelle(fdscLibelle);
		eo.setFdscNiveau(fdscNiveau);
		eo.setFdscPere(fdscPere);
		eo.setFdscValidite(fdscValidite);
		eo.setToFwkScolarite_ScolFormationDisciplineRelationship(toFwkScolarite_ScolFormationDiscipline);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolFormationDiscipline.
	 *
	 * @param editingContext
	 * @return EOScolFormationDiscipline
	 */
	public static EOScolFormationDiscipline create(EOEditingContext editingContext) {
		EOScolFormationDiscipline eo = (EOScolFormationDiscipline) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolFormationDiscipline localInstanceIn(EOEditingContext editingContext) {
		EOScolFormationDiscipline localInstance = (EOScolFormationDiscipline) localInstanceOfObject(editingContext, (EOScolFormationDiscipline) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolFormationDiscipline localInstanceIn(EOEditingContext editingContext, EOScolFormationDiscipline eo) {
		EOScolFormationDiscipline localInstance = (eo == null) ? null : (EOScolFormationDiscipline) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer fannDebut() {
		return (Integer) storedValueForKey("fannDebut");
	}

	public void setFannDebut(Integer value) {
		takeStoredValueForKey(value, "fannDebut");
	}
	public Integer fannFin() {
		return (Integer) storedValueForKey("fannFin");
	}

	public void setFannFin(Integer value) {
		takeStoredValueForKey(value, "fannFin");
	}
	public String fdscAbreviation() {
		return (String) storedValueForKey("fdscAbreviation");
	}

	public void setFdscAbreviation(String value) {
		takeStoredValueForKey(value, "fdscAbreviation");
	}
	public String fdscCode() {
		return (String) storedValueForKey("fdscCode");
	}

	public void setFdscCode(String value) {
		takeStoredValueForKey(value, "fdscCode");
	}
	public String fdscLibelle() {
		return (String) storedValueForKey("fdscLibelle");
	}

	public void setFdscLibelle(String value) {
		takeStoredValueForKey(value, "fdscLibelle");
	}
	public Integer fdscNiveau() {
		return (Integer) storedValueForKey("fdscNiveau");
	}

	public void setFdscNiveau(Integer value) {
		takeStoredValueForKey(value, "fdscNiveau");
	}
	public Integer fdscPere() {
		return (Integer) storedValueForKey("fdscPere");
	}

	public void setFdscPere(Integer value) {
		takeStoredValueForKey(value, "fdscPere");
	}
	public String fdscValidite() {
		return (String) storedValueForKey("fdscValidite");
	}

	public void setFdscValidite(String value) {
		takeStoredValueForKey(value, "fdscValidite");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline toFwkScolarite_ScolFormationDiscipline() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline)storedValueForKey("toFwkScolarite_ScolFormationDiscipline");
	}

	public void setToFwkScolarite_ScolFormationDisciplineRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline oldValue = toFwkScolarite_ScolFormationDiscipline();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDiscipline");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDiscipline");
		}
	}
  
	public NSArray toFwkScolarite_ScolFormationDisciplinaires() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolFormationDisciplinaires");
	}

	public NSArray toFwkScolarite_ScolFormationDisciplinaires(EOQualifier qualifier) {
		return toFwkScolarite_ScolFormationDisciplinaires(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolFormationDisciplinaires(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolFormationDisciplinaires(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolFormationDisciplinaires(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolFormationDisciplinaires();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolFormationDisciplinairesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplinaires");
	}

	public void removeFromToFwkScolarite_ScolFormationDisciplinairesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplinaires");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire createToFwkScolarite_ScolFormationDisciplinairesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolFormationDisciplinaire");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolFormationDisciplinaires");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire) eo;
	}

	public void deleteToFwkScolarite_ScolFormationDisciplinairesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplinaires");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolFormationDisciplinairesRelationships() {
		Enumeration objects = toFwkScolarite_ScolFormationDisciplinaires().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolFormationDisciplinairesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolFormationDisciplines() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolFormationDisciplines");
	}

	public NSArray toFwkScolarite_ScolFormationDisciplines(EOQualifier qualifier) {
		return toFwkScolarite_ScolFormationDisciplines(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolFormationDisciplines(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolFormationDisciplines(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolFormationDisciplines(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolFormationDisciplines();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolFormationDisciplinesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplines");
	}

	public void removeFromToFwkScolarite_ScolFormationDisciplinesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplines");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline createToFwkScolarite_ScolFormationDisciplinesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolFormationDiscipline");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolFormationDisciplines");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline) eo;
	}

	public void deleteToFwkScolarite_ScolFormationDisciplinesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplines");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolFormationDisciplinesRelationships() {
		Enumeration objects = toFwkScolarite_ScolFormationDisciplines().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolFormationDisciplinesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolFormationDiscipline.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolFormationDiscipline.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolFormationDiscipline)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolFormationDiscipline fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolFormationDiscipline fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolFormationDiscipline eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolFormationDiscipline)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolFormationDiscipline fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationDiscipline fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationDiscipline fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolFormationDiscipline eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolFormationDiscipline)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationDiscipline fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationDiscipline fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolFormationDiscipline eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolFormationDiscipline ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolFormationDiscipline createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolFormationDiscipline.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolFormationDiscipline.ENTITY_NAME + "' !");
		}
		else {
			EOScolFormationDiscipline object = (EOScolFormationDiscipline) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolFormationDiscipline localInstanceOfObject(EOEditingContext ec, EOScolFormationDiscipline object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolFormationDiscipline " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolFormationDiscipline) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
