/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteRepartitionEc.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteRepartitionEc extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteRepartitionEc";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_REPARTITION_EC";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mrecKey";

	public static final ERXKey<Integer> MREC_BONIFIABLE = new ERXKey<Integer>("mrecBonifiable");
	public static final ERXKey<java.math.BigDecimal> MREC_COEFFICIENT = new ERXKey<java.math.BigDecimal>("mrecCoefficient");
	public static final ERXKey<Integer> MREC_COMPTABILISABLE = new ERXKey<Integer>("mrecComptabilisable");
	public static final ERXKey<String> MREC_NIVEAU = new ERXKey<String>("mrecNiveau");
	public static final ERXKey<java.math.BigDecimal> MREC_NOTE_BASE = new ERXKey<java.math.BigDecimal>("mrecNoteBase");
	public static final ERXKey<java.math.BigDecimal> MREC_NOTE_ELIMINATION = new ERXKey<java.math.BigDecimal>("mrecNoteElimination");
	public static final ERXKey<java.math.BigDecimal> MREC_NOTE_OBTENTION = new ERXKey<java.math.BigDecimal>("mrecNoteObtention");
	public static final ERXKey<Long> MREC_ORDRE = new ERXKey<Long>("mrecOrdre");

	public static final String MREC_BONIFIABLE_KEY = "mrecBonifiable";
	public static final String MREC_COEFFICIENT_KEY = "mrecCoefficient";
	public static final String MREC_COMPTABILISABLE_KEY = "mrecComptabilisable";
	public static final String MREC_NIVEAU_KEY = "mrecNiveau";
	public static final String MREC_NOTE_BASE_KEY = "mrecNoteBase";
	public static final String MREC_NOTE_ELIMINATION_KEY = "mrecNoteElimination";
	public static final String MREC_NOTE_OBTENTION_KEY = "mrecNoteObtention";
	public static final String MREC_ORDRE_KEY = "mrecOrdre";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MEC_KEY_KEY = "mecKey";
	public static final String MREC_KEY_KEY = "mrecKey";
	public static final String MTEC_CODE_KEY = "mtecCode";
	public static final String MUE_KEY_KEY = "mueKey";

	// Colkeys
	public static final String MREC_BONIFIABLE_COLKEY = "MREC_BONIFIABLE";
	public static final String MREC_COEFFICIENT_COLKEY = "MREC_COEFFICIENT";
	public static final String MREC_COMPTABILISABLE_COLKEY = "MREC_COMPTABILISABLE";
	public static final String MREC_NIVEAU_COLKEY = "MREC_NIVEAU";
	public static final String MREC_NOTE_BASE_COLKEY = "MREC_NOTE_BASE";
	public static final String MREC_NOTE_ELIMINATION_COLKEY = "MREC_NOTE_ELIMINATION";
	public static final String MREC_NOTE_OBTENTION_COLKEY = "MREC_NOTE_OBTENTION";
	public static final String MREC_ORDRE_COLKEY = "MREC_ORDRE";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MEC_KEY_COLKEY = "MEC_KEY";
	public static final String MREC_KEY_COLKEY = "MREC_KEY";
	public static final String MTEC_CODE_COLKEY = "MTEC_CODE";
	public static final String MUE_KEY_COLKEY = "MUE_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc> TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ECS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc>("toFwkScolarite_ScolInscriptionEcs");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc> TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc>("toFwkScolarite_ScolMaquetteEc");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc> TO_FWK_SCOLARITE__SCOL_MAQUETTE_TYPE_EC = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc>("toFwkScolarite_ScolMaquetteTypeEc");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe> TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe>("toFwkScolarite_ScolMaquetteUe");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ECS_KEY = "toFwkScolarite_ScolInscriptionEcs";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY = "toFwkScolarite_ScolMaquetteEc";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_TYPE_EC_KEY = "toFwkScolarite_ScolMaquetteTypeEc";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY = "toFwkScolarite_ScolMaquetteUe";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteRepartitionEc with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mrecBonifiable
	 * @param mrecCoefficient
	 * @param mrecComptabilisable
	 * @param mrecNoteBase
	 * @param mrecNoteObtention
	 * @param mrecOrdre
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteEc
	 * @param toFwkScolarite_ScolMaquetteTypeEc
	 * @param toFwkScolarite_ScolMaquetteUe
	 * @return EOScolMaquetteRepartitionEc
	 */
	public static EOScolMaquetteRepartitionEc create(EOEditingContext editingContext, Integer mrecBonifiable, java.math.BigDecimal mrecCoefficient, Integer mrecComptabilisable, java.math.BigDecimal mrecNoteBase, java.math.BigDecimal mrecNoteObtention, Long mrecOrdre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc toFwkScolarite_ScolMaquetteEc, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc toFwkScolarite_ScolMaquetteTypeEc, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe toFwkScolarite_ScolMaquetteUe) {
		EOScolMaquetteRepartitionEc eo = (EOScolMaquetteRepartitionEc) createAndInsertInstance(editingContext);
		eo.setMrecBonifiable(mrecBonifiable);
		eo.setMrecCoefficient(mrecCoefficient);
		eo.setMrecComptabilisable(mrecComptabilisable);
		eo.setMrecNoteBase(mrecNoteBase);
		eo.setMrecNoteObtention(mrecNoteObtention);
		eo.setMrecOrdre(mrecOrdre);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteEcRelationship(toFwkScolarite_ScolMaquetteEc);
		eo.setToFwkScolarite_ScolMaquetteTypeEcRelationship(toFwkScolarite_ScolMaquetteTypeEc);
		eo.setToFwkScolarite_ScolMaquetteUeRelationship(toFwkScolarite_ScolMaquetteUe);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteRepartitionEc.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteRepartitionEc
	 */
	public static EOScolMaquetteRepartitionEc create(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionEc eo = (EOScolMaquetteRepartitionEc) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteRepartitionEc localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionEc localInstance = (EOScolMaquetteRepartitionEc) localInstanceOfObject(editingContext, (EOScolMaquetteRepartitionEc) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteRepartitionEc localInstanceIn(EOEditingContext editingContext, EOScolMaquetteRepartitionEc eo) {
		EOScolMaquetteRepartitionEc localInstance = (eo == null) ? null : (EOScolMaquetteRepartitionEc) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mrecBonifiable() {
		return (Integer) storedValueForKey("mrecBonifiable");
	}

	public void setMrecBonifiable(Integer value) {
		takeStoredValueForKey(value, "mrecBonifiable");
	}
	public java.math.BigDecimal mrecCoefficient() {
		return (java.math.BigDecimal) storedValueForKey("mrecCoefficient");
	}

	public void setMrecCoefficient(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrecCoefficient");
	}
	public Integer mrecComptabilisable() {
		return (Integer) storedValueForKey("mrecComptabilisable");
	}

	public void setMrecComptabilisable(Integer value) {
		takeStoredValueForKey(value, "mrecComptabilisable");
	}
	public String mrecNiveau() {
		return (String) storedValueForKey("mrecNiveau");
	}

	public void setMrecNiveau(String value) {
		takeStoredValueForKey(value, "mrecNiveau");
	}
	public java.math.BigDecimal mrecNoteBase() {
		return (java.math.BigDecimal) storedValueForKey("mrecNoteBase");
	}

	public void setMrecNoteBase(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrecNoteBase");
	}
	public java.math.BigDecimal mrecNoteElimination() {
		return (java.math.BigDecimal) storedValueForKey("mrecNoteElimination");
	}

	public void setMrecNoteElimination(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrecNoteElimination");
	}
	public java.math.BigDecimal mrecNoteObtention() {
		return (java.math.BigDecimal) storedValueForKey("mrecNoteObtention");
	}

	public void setMrecNoteObtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrecNoteObtention");
	}
	public Long mrecOrdre() {
		return (Long) storedValueForKey("mrecOrdre");
	}

	public void setMrecOrdre(Long value) {
		takeStoredValueForKey(value, "mrecOrdre");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc toFwkScolarite_ScolMaquetteEc() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc)storedValueForKey("toFwkScolarite_ScolMaquetteEc");
	}

	public void setToFwkScolarite_ScolMaquetteEcRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc oldValue = toFwkScolarite_ScolMaquetteEc();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteEc");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteEc");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc toFwkScolarite_ScolMaquetteTypeEc() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc)storedValueForKey("toFwkScolarite_ScolMaquetteTypeEc");
	}

	public void setToFwkScolarite_ScolMaquetteTypeEcRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteTypeEc oldValue = toFwkScolarite_ScolMaquetteTypeEc();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteTypeEc");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteTypeEc");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe toFwkScolarite_ScolMaquetteUe() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe)storedValueForKey("toFwkScolarite_ScolMaquetteUe");
	}

	public void setToFwkScolarite_ScolMaquetteUeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe oldValue = toFwkScolarite_ScolMaquetteUe();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteUe");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteUe");
		}
	}
  
	public NSArray toFwkScolarite_ScolInscriptionEcs() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolInscriptionEcs");
	}

	public NSArray toFwkScolarite_ScolInscriptionEcs(EOQualifier qualifier) {
		return toFwkScolarite_ScolInscriptionEcs(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolInscriptionEcs(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolInscriptionEcs(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolInscriptionEcs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_EC_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolInscriptionEcs();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolInscriptionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionEcs");
	}

	public void removeFromToFwkScolarite_ScolInscriptionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionEcs");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc createToFwkScolarite_ScolInscriptionEcsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolInscriptionEc");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolInscriptionEcs");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc) eo;
	}

	public void deleteToFwkScolarite_ScolInscriptionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionEcs");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolInscriptionEcsRelationships() {
		Enumeration objects = toFwkScolarite_ScolInscriptionEcs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolInscriptionEcsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteRepartitionEc.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteRepartitionEc.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteRepartitionEc)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteRepartitionEc fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteRepartitionEc fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteRepartitionEc eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteRepartitionEc)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionEc fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionEc fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionEc fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteRepartitionEc eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteRepartitionEc)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionEc fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionEc fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteRepartitionEc eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteRepartitionEc ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteRepartitionEc createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteRepartitionEc.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteRepartitionEc.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteRepartitionEc object = (EOScolMaquetteRepartitionEc) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteRepartitionEc localInstanceOfObject(EOEditingContext ec, EOScolMaquetteRepartitionEc object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteRepartitionEc " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteRepartitionEc) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
