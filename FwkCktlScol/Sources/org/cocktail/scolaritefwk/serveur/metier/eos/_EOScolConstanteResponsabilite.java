/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolConstanteResponsabilite.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolConstanteResponsabilite extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolConstanteResponsabilite";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_CONSTANTE_RESPONSABILITE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "cresCode";

	public static final ERXKey<String> CRES_LIBELLE = new ERXKey<String>("cresLibelle");
	public static final ERXKey<Integer> CRES_PRIORITE = new ERXKey<Integer>("cresPriorite");

	public static final String CRES_LIBELLE_KEY = "cresLibelle";
	public static final String CRES_PRIORITE_KEY = "cresPriorite";

	// Non visible attributes
	public static final String CRES_CODE_KEY = "cresCode";

	// Colkeys
	public static final String CRES_LIBELLE_COLKEY = "CRES_LIBELLE";
	public static final String CRES_PRIORITE_COLKEY = "CRES_PRIORITE";

	// Non visible colkeys
	public static final String CRES_CODE_COLKEY = "CRES_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite> TO_FWK_SCOLARITE__SCOL_FORMATION_RESPONSABILITES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite>("toFwkScolarite_ScolFormationResponsabilites");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_RESPONSABILITES_KEY = "toFwkScolarite_ScolFormationResponsabilites";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolConstanteResponsabilite with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cresPriorite
	 * @return EOScolConstanteResponsabilite
	 */
	public static EOScolConstanteResponsabilite create(EOEditingContext editingContext, Integer cresPriorite) {
		EOScolConstanteResponsabilite eo = (EOScolConstanteResponsabilite) createAndInsertInstance(editingContext);
		eo.setCresPriorite(cresPriorite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolConstanteResponsabilite.
	 *
	 * @param editingContext
	 * @return EOScolConstanteResponsabilite
	 */
	public static EOScolConstanteResponsabilite create(EOEditingContext editingContext) {
		EOScolConstanteResponsabilite eo = (EOScolConstanteResponsabilite) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolConstanteResponsabilite localInstanceIn(EOEditingContext editingContext) {
		EOScolConstanteResponsabilite localInstance = (EOScolConstanteResponsabilite) localInstanceOfObject(editingContext, (EOScolConstanteResponsabilite) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolConstanteResponsabilite localInstanceIn(EOEditingContext editingContext, EOScolConstanteResponsabilite eo) {
		EOScolConstanteResponsabilite localInstance = (eo == null) ? null : (EOScolConstanteResponsabilite) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cresLibelle() {
		return (String) storedValueForKey("cresLibelle");
	}

	public void setCresLibelle(String value) {
		takeStoredValueForKey(value, "cresLibelle");
	}
	public Integer cresPriorite() {
		return (Integer) storedValueForKey("cresPriorite");
	}

	public void setCresPriorite(Integer value) {
		takeStoredValueForKey(value, "cresPriorite");
	}

	public NSArray toFwkScolarite_ScolFormationResponsabilites() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolFormationResponsabilites");
	}

	public NSArray toFwkScolarite_ScolFormationResponsabilites(EOQualifier qualifier) {
		return toFwkScolarite_ScolFormationResponsabilites(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolFormationResponsabilites(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolFormationResponsabilites(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolFormationResponsabilites(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_CONSTANTE_RESPONSABILITE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolFormationResponsabilites();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolFormationResponsabilitesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationResponsabilites");
	}

	public void removeFromToFwkScolarite_ScolFormationResponsabilitesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationResponsabilites");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite createToFwkScolarite_ScolFormationResponsabilitesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolFormationResponsabilite");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolFormationResponsabilites");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite) eo;
	}

	public void deleteToFwkScolarite_ScolFormationResponsabilitesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationResponsabilites");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolFormationResponsabilitesRelationships() {
		Enumeration objects = toFwkScolarite_ScolFormationResponsabilites().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolFormationResponsabilitesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolConstanteResponsabilite.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolConstanteResponsabilite.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolConstanteResponsabilite)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolConstanteResponsabilite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolConstanteResponsabilite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolConstanteResponsabilite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolConstanteResponsabilite)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolConstanteResponsabilite fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolConstanteResponsabilite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolConstanteResponsabilite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolConstanteResponsabilite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolConstanteResponsabilite)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolConstanteResponsabilite fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolConstanteResponsabilite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolConstanteResponsabilite eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolConstanteResponsabilite ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolConstanteResponsabilite createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolConstanteResponsabilite.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolConstanteResponsabilite.ENTITY_NAME + "' !");
		}
		else {
			EOScolConstanteResponsabilite object = (EOScolConstanteResponsabilite) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolConstanteResponsabilite localInstanceOfObject(EOEditingContext ec, EOScolConstanteResponsabilite object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolConstanteResponsabilite " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolConstanteResponsabilite) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
