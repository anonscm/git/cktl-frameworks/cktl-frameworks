/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.metier.eos;

import com.webobjects.foundation.NSValidation;

public class EOScolParametres extends _EOScolParametres {

	public static final String SCOL_ANNEE_EXPLOITATION = "SCOL_ANNEE_EXPLOITATION";
	public static final String SCOL_CONSTANTE_ANNEE_CIVILE = "SCOL_CONSTANTE_ANNEE_CIVILE";
	public static final String SCOL_CONSTANTE_PAYS = "SCOL_CONSTANTE_PAYS";
	public static final String SCOL_CONSTANTE_LANGUE = "SCOL_CONSTANTE_LANGUE";
	public static final String SCOL_CONSTANTE_SEMESTRE = "SCOL_CONSTANTE_SEMESTRE";
	public static final String SCOL_MAQUETTE_SEPARATEUR = "SCOL_MAQUETTE_SEPARATEUR";
	public static final String SCOL_UNIVERSITE_LIBELLE = "SCOL_UNIVERSITE_LIBELLE";
	public static final String SCOL_UNIVERSITE_VILLE = "SCOL_UNIVERSITE_VILLE";
	public static final String SCOL_UNIVERSITE_IDENTITE = "SCOL_UNIVERSITE_IDENTITE";
	public static final String SCOL_UNIVERSITE_TITRE = "SCOL_UNIVERSITE_TITRE";
	public static final String SCOL_UNIVERSITE_ABREVIATION = "SCOL_UNIVERSITE_ABREVIATION";
	public static final String SCOL_DEFAULT_STRING = "SCOL_DEFAULT_STRING";
	public static final String SCOL_DEFAULT_NUMBER = "SCOL_DEFAULT_NUMBER";

    public EOScolParametres() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
