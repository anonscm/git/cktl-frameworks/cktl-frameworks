/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteEc.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteEc extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteEc";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_EC";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mecKey";

	public static final ERXKey<Integer> MEC_BASE = new ERXKey<Integer>("mecBase");
	public static final ERXKey<String> MEC_CODE = new ERXKey<String>("mecCode");
	public static final ERXKey<java.math.BigDecimal> MEC_HORAIRE_ETU = new ERXKey<java.math.BigDecimal>("mecHoraireEtu");
	public static final ERXKey<String> MEC_LIBELLE = new ERXKey<String>("mecLibelle");
	public static final ERXKey<String> MEC_LIBELLE_COURT = new ERXKey<String>("mecLibelleCourt");
	public static final ERXKey<java.math.BigDecimal> MEC_POINTS = new ERXKey<java.math.BigDecimal>("mecPoints");
	public static final ERXKey<String> MEC_SESSION1 = new ERXKey<String>("mecSession1");
	public static final ERXKey<String> MEC_SESSION2 = new ERXKey<String>("mecSession2");

	public static final String MEC_BASE_KEY = "mecBase";
	public static final String MEC_CODE_KEY = "mecCode";
	public static final String MEC_HORAIRE_ETU_KEY = "mecHoraireEtu";
	public static final String MEC_LIBELLE_KEY = "mecLibelle";
	public static final String MEC_LIBELLE_COURT_KEY = "mecLibelleCourt";
	public static final String MEC_POINTS_KEY = "mecPoints";
	public static final String MEC_SESSION1_KEY = "mecSession1";
	public static final String MEC_SESSION2_KEY = "mecSession2";

	// Non visible attributes
	public static final String CBUD_KEY_KEY = "cbudKey";
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FDSC_KEY_KEY = "fdscKey";
	public static final String MEC_KEY_KEY = "mecKey";

	// Colkeys
	public static final String MEC_BASE_COLKEY = "MEC_BASE";
	public static final String MEC_CODE_COLKEY = "MEC_CODE";
	public static final String MEC_HORAIRE_ETU_COLKEY = "MEC_HORAIRE_ETU";
	public static final String MEC_LIBELLE_COLKEY = "MEC_LIBELLE";
	public static final String MEC_LIBELLE_COURT_COLKEY = "MEC_LIBELLE_COURT";
	public static final String MEC_POINTS_COLKEY = "MEC_POINTS";
	public static final String MEC_SESSION1_COLKEY = "MEC_SESSION1";
	public static final String MEC_SESSION2_COLKEY = "MEC_SESSION2";

	// Non visible colkeys
	public static final String CBUD_KEY_COLKEY = "CBUD_KEY";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FDSC_KEY_COLKEY = "FDSC_KEY";
	public static final String MEC_KEY_COLKEY = "MEC_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteBudget> TO_FWK_SCOLARITE__SCOL_CONSTANTE_BUDGET = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteBudget>("toFwkScolarite_ScolConstanteBudget");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline> TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline>("toFwkScolarite_ScolFormationDiscipline");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_APS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp>("toFwkScolarite_ScolMaquetteRepartitionAps");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc>("toFwkScolarite_ScolMaquetteRepartitionEcs");

	public static final String TO_FWK_SCOLARITE__SCOL_CONSTANTE_BUDGET_KEY = "toFwkScolarite_ScolConstanteBudget";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE_KEY = "toFwkScolarite_ScolFormationDiscipline";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_APS_KEY = "toFwkScolarite_ScolMaquetteRepartitionAps";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY = "toFwkScolarite_ScolMaquetteRepartitionEcs";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteEc with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mecBase
	 * @param mecCode
	 * @param mecHoraireEtu
	 * @param mecPoints
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationDiscipline
	 * @return EOScolMaquetteEc
	 */
	public static EOScolMaquetteEc create(EOEditingContext editingContext, Integer mecBase, String mecCode, java.math.BigDecimal mecHoraireEtu, java.math.BigDecimal mecPoints, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline toFwkScolarite_ScolFormationDiscipline) {
		EOScolMaquetteEc eo = (EOScolMaquetteEc) createAndInsertInstance(editingContext);
		eo.setMecBase(mecBase);
		eo.setMecCode(mecCode);
		eo.setMecHoraireEtu(mecHoraireEtu);
		eo.setMecPoints(mecPoints);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationDisciplineRelationship(toFwkScolarite_ScolFormationDiscipline);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteEc.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteEc
	 */
	public static EOScolMaquetteEc create(EOEditingContext editingContext) {
		EOScolMaquetteEc eo = (EOScolMaquetteEc) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteEc localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteEc localInstance = (EOScolMaquetteEc) localInstanceOfObject(editingContext, (EOScolMaquetteEc) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteEc localInstanceIn(EOEditingContext editingContext, EOScolMaquetteEc eo) {
		EOScolMaquetteEc localInstance = (eo == null) ? null : (EOScolMaquetteEc) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mecBase() {
		return (Integer) storedValueForKey("mecBase");
	}

	public void setMecBase(Integer value) {
		takeStoredValueForKey(value, "mecBase");
	}
	public String mecCode() {
		return (String) storedValueForKey("mecCode");
	}

	public void setMecCode(String value) {
		takeStoredValueForKey(value, "mecCode");
	}
	public java.math.BigDecimal mecHoraireEtu() {
		return (java.math.BigDecimal) storedValueForKey("mecHoraireEtu");
	}

	public void setMecHoraireEtu(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mecHoraireEtu");
	}
	public String mecLibelle() {
		return (String) storedValueForKey("mecLibelle");
	}

	public void setMecLibelle(String value) {
		takeStoredValueForKey(value, "mecLibelle");
	}
	public String mecLibelleCourt() {
		return (String) storedValueForKey("mecLibelleCourt");
	}

	public void setMecLibelleCourt(String value) {
		takeStoredValueForKey(value, "mecLibelleCourt");
	}
	public java.math.BigDecimal mecPoints() {
		return (java.math.BigDecimal) storedValueForKey("mecPoints");
	}

	public void setMecPoints(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mecPoints");
	}
	public String mecSession1() {
		return (String) storedValueForKey("mecSession1");
	}

	public void setMecSession1(String value) {
		takeStoredValueForKey(value, "mecSession1");
	}
	public String mecSession2() {
		return (String) storedValueForKey("mecSession2");
	}

	public void setMecSession2(String value) {
		takeStoredValueForKey(value, "mecSession2");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteBudget toFwkScolarite_ScolConstanteBudget() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteBudget)storedValueForKey("toFwkScolarite_ScolConstanteBudget");
	}

	public void setToFwkScolarite_ScolConstanteBudgetRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteBudget value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteBudget oldValue = toFwkScolarite_ScolConstanteBudget();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolConstanteBudget");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolConstanteBudget");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline toFwkScolarite_ScolFormationDiscipline() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline)storedValueForKey("toFwkScolarite_ScolFormationDiscipline");
	}

	public void setToFwkScolarite_ScolFormationDisciplineRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline oldValue = toFwkScolarite_ScolFormationDiscipline();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDiscipline");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDiscipline");
		}
	}
  
	public NSArray toFwkScolarite_ScolMaquetteRepartitionAps() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionAps");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionAps(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionAps(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionAps(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionAps(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionAps(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionAps();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionAps");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionAps");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp createToFwkScolarite_ScolMaquetteRepartitionApsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionAp");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionAps");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionAps");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionApsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionAps().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionApsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteRepartitionEcs() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionEcs");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionEcs(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionEcs(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionEcs(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionEcs(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionEcs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionEcs();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionEcs");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionEcs");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc createToFwkScolarite_ScolMaquetteRepartitionEcsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionEc");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionEcs");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionEcs");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionEcsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionEcs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionEcsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteEc.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteEc.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteEc)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteEc fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteEc fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteEc eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteEc)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteEc fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteEc fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteEc fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteEc eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteEc)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteEc fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteEc fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteEc eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteEc ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteEc createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteEc.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteEc.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteEc object = (EOScolMaquetteEc) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteEc localInstanceOfObject(EOEditingContext ec, EOScolMaquetteEc object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteEc " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteEc) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
