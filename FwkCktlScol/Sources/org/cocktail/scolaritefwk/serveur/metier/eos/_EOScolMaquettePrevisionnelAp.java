/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquettePrevisionnelAp.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquettePrevisionnelAp extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquettePrevisionnelAp";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_PREVISIONNEL_AP";

	//Attributes

	public static final ERXKey<java.math.BigDecimal> MPAP_VALEUR = new ERXKey<java.math.BigDecimal>("mpapValeur");

	public static final String MPAP_VALEUR_KEY = "mpapValeur";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MAP_KEY_KEY = "mapKey";
	public static final String PERS_ID_KEY = "persId";

	// Colkeys
	public static final String MPAP_VALEUR_COLKEY = "MPAP_VALEUR";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MAP_KEY_COLKEY = "MAP_KEY";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_FWKPERS__INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toFwkpers_Individu");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp> TO_FWK_SCOLARITE__SCOL_MAQUETTE_AP = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp>("toFwkScolarite_ScolMaquetteAp");

	public static final String TO_FWKPERS__INDIVIDU_KEY = "toFwkpers_Individu";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_AP_KEY = "toFwkScolarite_ScolMaquetteAp";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquettePrevisionnelAp with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mpapValeur
	 * @param toFwkpers_Individu
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteAp
	 * @return EOScolMaquettePrevisionnelAp
	 */
	public static EOScolMaquettePrevisionnelAp create(EOEditingContext editingContext, java.math.BigDecimal mpapValeur, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp toFwkScolarite_ScolMaquetteAp) {
		EOScolMaquettePrevisionnelAp eo = (EOScolMaquettePrevisionnelAp) createAndInsertInstance(editingContext);
		eo.setMpapValeur(mpapValeur);
		eo.setToFwkpers_IndividuRelationship(toFwkpers_Individu);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteApRelationship(toFwkScolarite_ScolMaquetteAp);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquettePrevisionnelAp.
	 *
	 * @param editingContext
	 * @return EOScolMaquettePrevisionnelAp
	 */
	public static EOScolMaquettePrevisionnelAp create(EOEditingContext editingContext) {
		EOScolMaquettePrevisionnelAp eo = (EOScolMaquettePrevisionnelAp) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquettePrevisionnelAp localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquettePrevisionnelAp localInstance = (EOScolMaquettePrevisionnelAp) localInstanceOfObject(editingContext, (EOScolMaquettePrevisionnelAp) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquettePrevisionnelAp localInstanceIn(EOEditingContext editingContext, EOScolMaquettePrevisionnelAp eo) {
		EOScolMaquettePrevisionnelAp localInstance = (eo == null) ? null : (EOScolMaquettePrevisionnelAp) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public java.math.BigDecimal mpapValeur() {
		return (java.math.BigDecimal) storedValueForKey("mpapValeur");
	}

	public void setMpapValeur(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mpapValeur");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toFwkpers_Individu");
	}

	public void setToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toFwkpers_Individu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Individu");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Individu");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp toFwkScolarite_ScolMaquetteAp() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp)storedValueForKey("toFwkScolarite_ScolMaquetteAp");
	}

	public void setToFwkScolarite_ScolMaquetteApRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp oldValue = toFwkScolarite_ScolMaquetteAp();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteAp");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteAp");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquettePrevisionnelAp.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquettePrevisionnelAp.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquettePrevisionnelAp)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquettePrevisionnelAp fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquettePrevisionnelAp fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquettePrevisionnelAp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquettePrevisionnelAp)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquettePrevisionnelAp fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquettePrevisionnelAp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquettePrevisionnelAp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquettePrevisionnelAp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquettePrevisionnelAp)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquettePrevisionnelAp fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquettePrevisionnelAp fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquettePrevisionnelAp eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquettePrevisionnelAp ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquettePrevisionnelAp createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquettePrevisionnelAp.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquettePrevisionnelAp.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquettePrevisionnelAp object = (EOScolMaquettePrevisionnelAp) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquettePrevisionnelAp localInstanceOfObject(EOEditingContext ec, EOScolMaquettePrevisionnelAp object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquettePrevisionnelAp " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquettePrevisionnelAp) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
