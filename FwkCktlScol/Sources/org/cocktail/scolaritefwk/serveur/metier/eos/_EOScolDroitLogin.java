/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolDroitLogin.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolDroitLogin extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolDroitLogin";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_DROIT_LOGIN";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "dlogKey";

	public static final ERXKey<String> DLOG_LOGIN = new ERXKey<String>("dlogLogin");
	public static final ERXKey<String> DLOG_NOM = new ERXKey<String>("dlogNom");
	public static final ERXKey<String> DLOG_PRENOM = new ERXKey<String>("dlogPrenom");
	public static final ERXKey<String> DLOG_VALIDE = new ERXKey<String>("dlogValide");

	public static final String DLOG_LOGIN_KEY = "dlogLogin";
	public static final String DLOG_NOM_KEY = "dlogNom";
	public static final String DLOG_PRENOM_KEY = "dlogPrenom";
	public static final String DLOG_VALIDE_KEY = "dlogValide";

	// Non visible attributes
	public static final String DLOG_KEY_KEY = "dlogKey";

	// Colkeys
	public static final String DLOG_LOGIN_COLKEY = "DLOG_LOGIN";
	public static final String DLOG_NOM_COLKEY = "DLOG_NOM";
	public static final String DLOG_PRENOM_COLKEY = "DLOG_PRENOM";
	public static final String DLOG_VALIDE_COLKEY = "DLOG_VALIDE";

	// Non visible colkeys
	public static final String DLOG_KEY_COLKEY = "DLOG_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference> TO_FWK_SCOLARITE__SCOL_DROIT_PREFERENCES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference>("toFwkScolarite_ScolDroitPreferences");

	public static final String TO_FWK_SCOLARITE__SCOL_DROIT_PREFERENCES_KEY = "toFwkScolarite_ScolDroitPreferences";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolDroitLogin with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dlogLogin
	 * @param dlogValide
	 * @return EOScolDroitLogin
	 */
	public static EOScolDroitLogin create(EOEditingContext editingContext, String dlogLogin, String dlogValide) {
		EOScolDroitLogin eo = (EOScolDroitLogin) createAndInsertInstance(editingContext);
		eo.setDlogLogin(dlogLogin);
		eo.setDlogValide(dlogValide);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolDroitLogin.
	 *
	 * @param editingContext
	 * @return EOScolDroitLogin
	 */
	public static EOScolDroitLogin create(EOEditingContext editingContext) {
		EOScolDroitLogin eo = (EOScolDroitLogin) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolDroitLogin localInstanceIn(EOEditingContext editingContext) {
		EOScolDroitLogin localInstance = (EOScolDroitLogin) localInstanceOfObject(editingContext, (EOScolDroitLogin) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolDroitLogin localInstanceIn(EOEditingContext editingContext, EOScolDroitLogin eo) {
		EOScolDroitLogin localInstance = (eo == null) ? null : (EOScolDroitLogin) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String dlogLogin() {
		return (String) storedValueForKey("dlogLogin");
	}

	public void setDlogLogin(String value) {
		takeStoredValueForKey(value, "dlogLogin");
	}
	public String dlogNom() {
		return (String) storedValueForKey("dlogNom");
	}

	public void setDlogNom(String value) {
		takeStoredValueForKey(value, "dlogNom");
	}
	public String dlogPrenom() {
		return (String) storedValueForKey("dlogPrenom");
	}

	public void setDlogPrenom(String value) {
		takeStoredValueForKey(value, "dlogPrenom");
	}
	public String dlogValide() {
		return (String) storedValueForKey("dlogValide");
	}

	public void setDlogValide(String value) {
		takeStoredValueForKey(value, "dlogValide");
	}

	public NSArray toFwkScolarite_ScolDroitPreferences() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolDroitPreferences");
	}

	public NSArray toFwkScolarite_ScolDroitPreferences(EOQualifier qualifier) {
		return toFwkScolarite_ScolDroitPreferences(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolDroitPreferences(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolDroitPreferences(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolDroitPreferences(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference.TO_FWK_SCOLARITE__SCOL_DROIT_LOGIN_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolDroitPreferences();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolDroitPreferencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolDroitPreferences");
	}

	public void removeFromToFwkScolarite_ScolDroitPreferencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolDroitPreferences");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference createToFwkScolarite_ScolDroitPreferencesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolDroitPreference");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolDroitPreferences");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference) eo;
	}

	public void deleteToFwkScolarite_ScolDroitPreferencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolDroitPreferences");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolDroitPreferencesRelationships() {
		Enumeration objects = toFwkScolarite_ScolDroitPreferences().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolDroitPreferencesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitPreference)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolDroitLogin.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolDroitLogin.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolDroitLogin)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolDroitLogin fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolDroitLogin fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolDroitLogin eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolDroitLogin)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolDroitLogin fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolDroitLogin fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolDroitLogin fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolDroitLogin eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolDroitLogin)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolDroitLogin fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolDroitLogin fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolDroitLogin eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolDroitLogin ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolDroitLogin createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolDroitLogin.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolDroitLogin.ENTITY_NAME + "' !");
		}
		else {
			EOScolDroitLogin object = (EOScolDroitLogin) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolDroitLogin localInstanceOfObject(EOEditingContext ec, EOScolDroitLogin object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolDroitLogin " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolDroitLogin) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
