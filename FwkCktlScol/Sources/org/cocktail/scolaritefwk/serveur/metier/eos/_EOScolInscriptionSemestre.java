/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolInscriptionSemestre.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolInscriptionSemestre extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolInscriptionSemestre";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_INSCRIPTION_SEMESTRE";

	//Attributes

	public static final ERXKey<Integer> FANN_KEY = new ERXKey<Integer>("fannKey");
	public static final ERXKey<Integer> IDIPL_NUMERO = new ERXKey<Integer>("idiplNumero");
	public static final ERXKey<Integer> IMRSEM_ABSENCE1 = new ERXKey<Integer>("imrsemAbsence1");
	public static final ERXKey<Integer> IMRSEM_ABSENCE2 = new ERXKey<Integer>("imrsemAbsence2");
	public static final ERXKey<Integer> IMRSEM_BLOCAGE = new ERXKey<Integer>("imrsemBlocage");
	public static final ERXKey<Integer> IMRSEM_DISPENSE = new ERXKey<Integer>("imrsemDispense");
	public static final ERXKey<Integer> IMRSEM_ETAT = new ERXKey<Integer>("imrsemEtat");
	public static final ERXKey<Integer> IMRSEM_MENTION1 = new ERXKey<Integer>("imrsemMention1");
	public static final ERXKey<Integer> IMRSEM_MENTION2 = new ERXKey<Integer>("imrsemMention2");
	public static final ERXKey<java.math.BigDecimal> IMRSEM_NOTE_BASE = new ERXKey<java.math.BigDecimal>("imrsemNoteBase");
	public static final ERXKey<String> IMRSEM_OBTENTION = new ERXKey<String>("imrsemObtention");
	public static final ERXKey<java.math.BigDecimal> IMRSEM_POINT_JURY = new ERXKey<java.math.BigDecimal>("imrsemPointJury");
	public static final ERXKey<java.math.BigDecimal> IMRSEM_POINTS1 = new ERXKey<java.math.BigDecimal>("imrsemPoints1");
	public static final ERXKey<java.math.BigDecimal> IMRSEM_POINTS2 = new ERXKey<java.math.BigDecimal>("imrsemPoints2");
	public static final ERXKey<java.math.BigDecimal> IMRSEM_PONDERATION = new ERXKey<java.math.BigDecimal>("imrsemPonderation");
	public static final ERXKey<java.math.BigDecimal> IMRSEM_SESSION1 = new ERXKey<java.math.BigDecimal>("imrsemSession1");
	public static final ERXKey<java.math.BigDecimal> IMRSEM_SESSION2 = new ERXKey<java.math.BigDecimal>("imrsemSession2");
	public static final ERXKey<Integer> MRSEM_KEY = new ERXKey<Integer>("mrsemKey");

	public static final String FANN_KEY_KEY = "fannKey";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String IMRSEM_ABSENCE1_KEY = "imrsemAbsence1";
	public static final String IMRSEM_ABSENCE2_KEY = "imrsemAbsence2";
	public static final String IMRSEM_BLOCAGE_KEY = "imrsemBlocage";
	public static final String IMRSEM_DISPENSE_KEY = "imrsemDispense";
	public static final String IMRSEM_ETAT_KEY = "imrsemEtat";
	public static final String IMRSEM_MENTION1_KEY = "imrsemMention1";
	public static final String IMRSEM_MENTION2_KEY = "imrsemMention2";
	public static final String IMRSEM_NOTE_BASE_KEY = "imrsemNoteBase";
	public static final String IMRSEM_OBTENTION_KEY = "imrsemObtention";
	public static final String IMRSEM_POINT_JURY_KEY = "imrsemPointJury";
	public static final String IMRSEM_POINTS1_KEY = "imrsemPoints1";
	public static final String IMRSEM_POINTS2_KEY = "imrsemPoints2";
	public static final String IMRSEM_PONDERATION_KEY = "imrsemPonderation";
	public static final String IMRSEM_SESSION1_KEY = "imrsemSession1";
	public static final String IMRSEM_SESSION2_KEY = "imrsemSession2";
	public static final String MRSEM_KEY_KEY = "mrsemKey";

	// Non visible attributes

	// Colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String IMRSEM_ABSENCE1_COLKEY = "IMRSEM_ABSENCE1";
	public static final String IMRSEM_ABSENCE2_COLKEY = "IMRSEM_ABSENCE2";
	public static final String IMRSEM_BLOCAGE_COLKEY = "IMRSEM_BLOCAGE";
	public static final String IMRSEM_DISPENSE_COLKEY = "IMRSEM_DISPENSE";
	public static final String IMRSEM_ETAT_COLKEY = "IMRSEM_ETAT";
	public static final String IMRSEM_MENTION1_COLKEY = "IMRSEM_MENTION1";
	public static final String IMRSEM_MENTION2_COLKEY = "IMRSEM_MENTION2";
	public static final String IMRSEM_NOTE_BASE_COLKEY = "IMRSEM_NOTE_BASE";
	public static final String IMRSEM_OBTENTION_COLKEY = "IMRSEM_OBTENTION";
	public static final String IMRSEM_POINT_JURY_COLKEY = "IMRSEM_POINT_JURY";
	public static final String IMRSEM_POINTS1_COLKEY = "IMRSEM_POINTS1";
	public static final String IMRSEM_POINTS2_COLKEY = "IMRSEM_POINTS2";
	public static final String IMRSEM_PONDERATION_COLKEY = "IMRSEM_PONDERATION";
	public static final String IMRSEM_SESSION1_COLKEY = "IMRSEM_SESSION1";
	public static final String IMRSEM_SESSION2_COLKEY = "IMRSEM_SESSION2";
	public static final String MRSEM_KEY_COLKEY = "MRSEM_KEY";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant> TO_SCOL_INSCRIPTION_ETUDIANT = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant>("toScolInscriptionEtudiant");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem> TO_SCOL_MAQUETTE_REPARTITION_SEM = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem>("toScolMaquetteRepartitionSem");

	public static final String TO_SCOL_FORMATION_ANNEE_KEY = "toScolFormationAnnee";
	public static final String TO_SCOL_INSCRIPTION_ETUDIANT_KEY = "toScolInscriptionEtudiant";
	public static final String TO_SCOL_MAQUETTE_REPARTITION_SEM_KEY = "toScolMaquetteRepartitionSem";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolInscriptionSemestre with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fannKey
	 * @param idiplNumero
	 * @param imrsemAbsence1
	 * @param imrsemAbsence2
	 * @param imrsemBlocage
	 * @param imrsemDispense
	 * @param imrsemEtat
	 * @param imrsemMention1
	 * @param imrsemMention2
	 * @param mrsemKey
	 * @param toScolFormationAnnee
	 * @param toScolInscriptionEtudiant
	 * @param toScolMaquetteRepartitionSem
	 * @return EOScolInscriptionSemestre
	 */
	public static EOScolInscriptionSemestre create(EOEditingContext editingContext, Integer fannKey, Integer idiplNumero, Integer imrsemAbsence1, Integer imrsemAbsence2, Integer imrsemBlocage, Integer imrsemDispense, Integer imrsemEtat, Integer imrsemMention1, Integer imrsemMention2, Integer mrsemKey, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant toScolInscriptionEtudiant, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem toScolMaquetteRepartitionSem) {
		EOScolInscriptionSemestre eo = (EOScolInscriptionSemestre) createAndInsertInstance(editingContext);
		eo.setFannKey(fannKey);
		eo.setIdiplNumero(idiplNumero);
		eo.setImrsemAbsence1(imrsemAbsence1);
		eo.setImrsemAbsence2(imrsemAbsence2);
		eo.setImrsemBlocage(imrsemBlocage);
		eo.setImrsemDispense(imrsemDispense);
		eo.setImrsemEtat(imrsemEtat);
		eo.setImrsemMention1(imrsemMention1);
		eo.setImrsemMention2(imrsemMention2);
		eo.setMrsemKey(mrsemKey);
		eo.setToScolFormationAnneeRelationship(toScolFormationAnnee);
		eo.setToScolInscriptionEtudiantRelationship(toScolInscriptionEtudiant);
		eo.setToScolMaquetteRepartitionSemRelationship(toScolMaquetteRepartitionSem);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolInscriptionSemestre.
	 *
	 * @param editingContext
	 * @return EOScolInscriptionSemestre
	 */
	public static EOScolInscriptionSemestre create(EOEditingContext editingContext) {
		EOScolInscriptionSemestre eo = (EOScolInscriptionSemestre) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolInscriptionSemestre localInstanceIn(EOEditingContext editingContext) {
		EOScolInscriptionSemestre localInstance = (EOScolInscriptionSemestre) localInstanceOfObject(editingContext, (EOScolInscriptionSemestre) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolInscriptionSemestre localInstanceIn(EOEditingContext editingContext, EOScolInscriptionSemestre eo) {
		EOScolInscriptionSemestre localInstance = (eo == null) ? null : (EOScolInscriptionSemestre) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer fannKey() {
		return (Integer) storedValueForKey("fannKey");
	}

	public void setFannKey(Integer value) {
		takeStoredValueForKey(value, "fannKey");
	}
	public Integer idiplNumero() {
		return (Integer) storedValueForKey("idiplNumero");
	}

	public void setIdiplNumero(Integer value) {
		takeStoredValueForKey(value, "idiplNumero");
	}
	public Integer imrsemAbsence1() {
		return (Integer) storedValueForKey("imrsemAbsence1");
	}

	public void setImrsemAbsence1(Integer value) {
		takeStoredValueForKey(value, "imrsemAbsence1");
	}
	public Integer imrsemAbsence2() {
		return (Integer) storedValueForKey("imrsemAbsence2");
	}

	public void setImrsemAbsence2(Integer value) {
		takeStoredValueForKey(value, "imrsemAbsence2");
	}
	public Integer imrsemBlocage() {
		return (Integer) storedValueForKey("imrsemBlocage");
	}

	public void setImrsemBlocage(Integer value) {
		takeStoredValueForKey(value, "imrsemBlocage");
	}
	public Integer imrsemDispense() {
		return (Integer) storedValueForKey("imrsemDispense");
	}

	public void setImrsemDispense(Integer value) {
		takeStoredValueForKey(value, "imrsemDispense");
	}
	public Integer imrsemEtat() {
		return (Integer) storedValueForKey("imrsemEtat");
	}

	public void setImrsemEtat(Integer value) {
		takeStoredValueForKey(value, "imrsemEtat");
	}
	public Integer imrsemMention1() {
		return (Integer) storedValueForKey("imrsemMention1");
	}

	public void setImrsemMention1(Integer value) {
		takeStoredValueForKey(value, "imrsemMention1");
	}
	public Integer imrsemMention2() {
		return (Integer) storedValueForKey("imrsemMention2");
	}

	public void setImrsemMention2(Integer value) {
		takeStoredValueForKey(value, "imrsemMention2");
	}
	public java.math.BigDecimal imrsemNoteBase() {
		return (java.math.BigDecimal) storedValueForKey("imrsemNoteBase");
	}

	public void setImrsemNoteBase(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrsemNoteBase");
	}
	public String imrsemObtention() {
		return (String) storedValueForKey("imrsemObtention");
	}

	public void setImrsemObtention(String value) {
		takeStoredValueForKey(value, "imrsemObtention");
	}
	public java.math.BigDecimal imrsemPointJury() {
		return (java.math.BigDecimal) storedValueForKey("imrsemPointJury");
	}

	public void setImrsemPointJury(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrsemPointJury");
	}
	public java.math.BigDecimal imrsemPoints1() {
		return (java.math.BigDecimal) storedValueForKey("imrsemPoints1");
	}

	public void setImrsemPoints1(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrsemPoints1");
	}
	public java.math.BigDecimal imrsemPoints2() {
		return (java.math.BigDecimal) storedValueForKey("imrsemPoints2");
	}

	public void setImrsemPoints2(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrsemPoints2");
	}
	public java.math.BigDecimal imrsemPonderation() {
		return (java.math.BigDecimal) storedValueForKey("imrsemPonderation");
	}

	public void setImrsemPonderation(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrsemPonderation");
	}
	public java.math.BigDecimal imrsemSession1() {
		return (java.math.BigDecimal) storedValueForKey("imrsemSession1");
	}

	public void setImrsemSession1(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrsemSession1");
	}
	public java.math.BigDecimal imrsemSession2() {
		return (java.math.BigDecimal) storedValueForKey("imrsemSession2");
	}

	public void setImrsemSession2(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrsemSession2");
	}
	public Integer mrsemKey() {
		return (Integer) storedValueForKey("mrsemKey");
	}

	public void setMrsemKey(Integer value) {
		takeStoredValueForKey(value, "mrsemKey");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toScolFormationAnnee");
	}

	public void setToScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant toScolInscriptionEtudiant() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant)storedValueForKey("toScolInscriptionEtudiant");
	}

	public void setToScolInscriptionEtudiantRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant oldValue = toScolInscriptionEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolInscriptionEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toScolInscriptionEtudiant");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem toScolMaquetteRepartitionSem() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem)storedValueForKey("toScolMaquetteRepartitionSem");
	}

	public void setToScolMaquetteRepartitionSemRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem oldValue = toScolMaquetteRepartitionSem();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolMaquetteRepartitionSem");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toScolMaquetteRepartitionSem");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolInscriptionSemestre.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolInscriptionSemestre.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolInscriptionSemestre)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolInscriptionSemestre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolInscriptionSemestre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolInscriptionSemestre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolInscriptionSemestre)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolInscriptionSemestre fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionSemestre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionSemestre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolInscriptionSemestre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolInscriptionSemestre)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionSemestre fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionSemestre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolInscriptionSemestre eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolInscriptionSemestre ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolInscriptionSemestre createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolInscriptionSemestre.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolInscriptionSemestre.ENTITY_NAME + "' !");
		}
		else {
			EOScolInscriptionSemestre object = (EOScolInscriptionSemestre) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolInscriptionSemestre localInstanceOfObject(EOEditingContext ec, EOScolInscriptionSemestre object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolInscriptionSemestre " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolInscriptionSemestre) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
