/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolFormationDiplome.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolFormationDiplome extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolFormationDiplome";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_FORMATION_DIPLOME";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "fdipCode";

	public static final ERXKey<String> FDIP_ABREVIATION = new ERXKey<String>("fdipAbreviation");
	public static final ERXKey<Integer> FDIP_ARRIVEE = new ERXKey<Integer>("fdipArrivee");
	public static final ERXKey<String> FDIP_CODE = new ERXKey<String>("fdipCode");
	public static final ERXKey<Integer> FDIP_CYCLE = new ERXKey<Integer>("fdipCycle");
	public static final ERXKey<String> FDIP_DELIBERATION = new ERXKey<String>("fdipDeliberation");
	public static final ERXKey<Integer> FDIP_DEPART = new ERXKey<Integer>("fdipDepart");
	public static final ERXKey<String> FDIP_LIBELLE = new ERXKey<String>("fdipLibelle");
	public static final ERXKey<String> FDIP_MENTION = new ERXKey<String>("fdipMention");
	public static final ERXKey<String> FDIP_MODELE = new ERXKey<String>("fdipModele");
	public static final ERXKey<Integer> FDIP_MONO_POSITION = new ERXKey<Integer>("fdipMonoPosition");
	public static final ERXKey<String> FDIP_MONO_SEMESTRE = new ERXKey<String>("fdipMonoSemestre");
	public static final ERXKey<String> FDIP_SEMESTRIALISATION = new ERXKey<String>("fdipSemestrialisation");
	public static final ERXKey<String> FDIP_SPECIALITE = new ERXKey<String>("fdipSpecialite");
	public static final ERXKey<String> FDIP_TYPE = new ERXKey<String>("fdipType");
	public static final ERXKey<String> FDIP_TYPE_DROIT = new ERXKey<String>("fdipTypeDroit");
	public static final ERXKey<String> SREMO_CODE = new ERXKey<String>("sremoCode");

	public static final String FDIP_ABREVIATION_KEY = "fdipAbreviation";
	public static final String FDIP_ARRIVEE_KEY = "fdipArrivee";
	public static final String FDIP_CODE_KEY = "fdipCode";
	public static final String FDIP_CYCLE_KEY = "fdipCycle";
	public static final String FDIP_DELIBERATION_KEY = "fdipDeliberation";
	public static final String FDIP_DEPART_KEY = "fdipDepart";
	public static final String FDIP_LIBELLE_KEY = "fdipLibelle";
	public static final String FDIP_MENTION_KEY = "fdipMention";
	public static final String FDIP_MODELE_KEY = "fdipModele";
	public static final String FDIP_MONO_POSITION_KEY = "fdipMonoPosition";
	public static final String FDIP_MONO_SEMESTRE_KEY = "fdipMonoSemestre";
	public static final String FDIP_SEMESTRIALISATION_KEY = "fdipSemestrialisation";
	public static final String FDIP_SPECIALITE_KEY = "fdipSpecialite";
	public static final String FDIP_TYPE_KEY = "fdipType";
	public static final String FDIP_TYPE_DROIT_KEY = "fdipTypeDroit";
	public static final String SREMO_CODE_KEY = "sremoCode";

	// Non visible attributes
	public static final String COMP_CODE_KEY = "compCode";
	public static final String ETAB_CODE_KEY = "etabCode";
	public static final String FDOM_CODE_KEY = "fdomCode";
	public static final String FGRA_CODE_KEY = "fgraCode";
	public static final String FVOC_CODE_KEY = "fvocCode";

	// Colkeys
	public static final String FDIP_ABREVIATION_COLKEY = "FDIP_ABREVIATION";
	public static final String FDIP_ARRIVEE_COLKEY = "FDIP_ARRIVEE";
	public static final String FDIP_CODE_COLKEY = "FDIP_CODE";
	public static final String FDIP_CYCLE_COLKEY = "FDIP_CYCLE";
	public static final String FDIP_DELIBERATION_COLKEY = "FDIP_DELIBERATION";
	public static final String FDIP_DEPART_COLKEY = "FDIP_DEPART";
	public static final String FDIP_LIBELLE_COLKEY = "FDIP_LIBELLE";
	public static final String FDIP_MENTION_COLKEY = "FDIP_MENTION";
	public static final String FDIP_MODELE_COLKEY = "FDIP_MODELE";
	public static final String FDIP_MONO_POSITION_COLKEY = "FDIP_MONO_POSITION";
	public static final String FDIP_MONO_SEMESTRE_COLKEY = "FDIP_MONO_SEMESTRE";
	public static final String FDIP_SEMESTRIALISATION_COLKEY = "FDIP_SEMESTRIALISATION";
	public static final String FDIP_SPECIALITE_COLKEY = "FDIP_SPECIALITE";
	public static final String FDIP_TYPE_COLKEY = "FDIP_TYPE";
	public static final String FDIP_TYPE_DROIT_COLKEY = "FDIP_TYPE_DROIT";
	public static final String SREMO_CODE_COLKEY = "SREMO_CODE";

	// Non visible colkeys
	public static final String COMP_CODE_COLKEY = "COMP_CODE";
	public static final String ETAB_CODE_COLKEY = "ETAB_CODE";
	public static final String FDOM_CODE_COLKEY = "FDOM_CODE";
	public static final String FGRA_CODE_COLKEY = "FGRA_CODE";
	public static final String FVOC_CODE_COLKEY = "FVOC_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement> TO_FWK_SCOLARITE__SCOL_FORMATION_DEPARTEMENT = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement>("toFwkScolarite_ScolFormationDepartement");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise> TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_SISE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise>("toFwkScolarite_ScolFormationDiplomeSise");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire> TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire>("toFwkScolarite_ScolFormationDisciplinaires");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine> TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine>("toFwkScolarite_ScolFormationDomaine");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere> TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere>("toFwkScolarite_ScolFormationFiliere");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade> TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade>("toFwkScolarite_ScolFormationGrade");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisations");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationVocation> TO_FWK_SCOLARITE__SCOL_FORMATION_VOCATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationVocation>("toFwkScolarite_ScolFormationVocation");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite> TO_FWK_SCOLARIX_V_COMPOSANTE_SCOLARITE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite>("toFwkScolarix_VComposanteScolarite");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite> TO_FWK_SCOLARIX_V_ETABLISSEMENT_SCOLARITE = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite>("toFwkScolarix_VEtablissementScolarite");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DEPARTEMENT_KEY = "toFwkScolarite_ScolFormationDepartement";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_SISE_KEY = "toFwkScolarite_ScolFormationDiplomeSise";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES_KEY = "toFwkScolarite_ScolFormationDisciplinaires";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY = "toFwkScolarite_ScolFormationDomaine";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE_KEY = "toFwkScolarite_ScolFormationFiliere";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY = "toFwkScolarite_ScolFormationGrade";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY = "toFwkScolarite_ScolFormationSpecialisations";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_VOCATION_KEY = "toFwkScolarite_ScolFormationVocation";
	public static final String TO_FWK_SCOLARIX_V_COMPOSANTE_SCOLARITE_KEY = "toFwkScolarix_VComposanteScolarite";
	public static final String TO_FWK_SCOLARIX_V_ETABLISSEMENT_SCOLARITE_KEY = "toFwkScolarix_VEtablissementScolarite";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolFormationDiplome with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fdipArrivee
	 * @param fdipCode
	 * @param fdipCycle
	 * @param fdipDeliberation
	 * @param fdipDepart
	 * @param fdipLibelle
	 * @param fdipMention
	 * @param fdipMonoPosition
	 * @param fdipMonoSemestre
	 * @param fdipSemestrialisation
	 * @param fdipType
	 * @param fdipTypeDroit
	 * @param toFwkScolarite_ScolFormationDiplomeSise
	 * @param toFwkScolarite_ScolFormationVocation
	 * @param toFwkScolarix_VComposanteScolarite
	 * @param toFwkScolarix_VEtablissementScolarite
	 * @return EOScolFormationDiplome
	 */
	public static EOScolFormationDiplome create(EOEditingContext editingContext, Integer fdipArrivee, String fdipCode, Integer fdipCycle, String fdipDeliberation, Integer fdipDepart, String fdipLibelle, String fdipMention, Integer fdipMonoPosition, String fdipMonoSemestre, String fdipSemestrialisation, String fdipType, String fdipTypeDroit, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise toFwkScolarite_ScolFormationDiplomeSise, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationVocation toFwkScolarite_ScolFormationVocation, org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite toFwkScolarix_VComposanteScolarite, org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite toFwkScolarix_VEtablissementScolarite) {
		EOScolFormationDiplome eo = (EOScolFormationDiplome) createAndInsertInstance(editingContext);
		eo.setFdipArrivee(fdipArrivee);
		eo.setFdipCode(fdipCode);
		eo.setFdipCycle(fdipCycle);
		eo.setFdipDeliberation(fdipDeliberation);
		eo.setFdipDepart(fdipDepart);
		eo.setFdipLibelle(fdipLibelle);
		eo.setFdipMention(fdipMention);
		eo.setFdipMonoPosition(fdipMonoPosition);
		eo.setFdipMonoSemestre(fdipMonoSemestre);
		eo.setFdipSemestrialisation(fdipSemestrialisation);
		eo.setFdipType(fdipType);
		eo.setFdipTypeDroit(fdipTypeDroit);
		eo.setToFwkScolarite_ScolFormationDiplomeSiseRelationship(toFwkScolarite_ScolFormationDiplomeSise);
		eo.setToFwkScolarite_ScolFormationVocationRelationship(toFwkScolarite_ScolFormationVocation);
		eo.setToFwkScolarix_VComposanteScolariteRelationship(toFwkScolarix_VComposanteScolarite);
		eo.setToFwkScolarix_VEtablissementScolariteRelationship(toFwkScolarix_VEtablissementScolarite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolFormationDiplome.
	 *
	 * @param editingContext
	 * @return EOScolFormationDiplome
	 */
	public static EOScolFormationDiplome create(EOEditingContext editingContext) {
		EOScolFormationDiplome eo = (EOScolFormationDiplome) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolFormationDiplome localInstanceIn(EOEditingContext editingContext) {
		EOScolFormationDiplome localInstance = (EOScolFormationDiplome) localInstanceOfObject(editingContext, (EOScolFormationDiplome) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolFormationDiplome localInstanceIn(EOEditingContext editingContext, EOScolFormationDiplome eo) {
		EOScolFormationDiplome localInstance = (eo == null) ? null : (EOScolFormationDiplome) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String fdipAbreviation() {
		return (String) storedValueForKey("fdipAbreviation");
	}

	public void setFdipAbreviation(String value) {
		takeStoredValueForKey(value, "fdipAbreviation");
	}
	public Integer fdipArrivee() {
		return (Integer) storedValueForKey("fdipArrivee");
	}

	public void setFdipArrivee(Integer value) {
		takeStoredValueForKey(value, "fdipArrivee");
	}
	public String fdipCode() {
		return (String) storedValueForKey("fdipCode");
	}

	public void setFdipCode(String value) {
		takeStoredValueForKey(value, "fdipCode");
	}
	public Integer fdipCycle() {
		return (Integer) storedValueForKey("fdipCycle");
	}

	public void setFdipCycle(Integer value) {
		takeStoredValueForKey(value, "fdipCycle");
	}
	public String fdipDeliberation() {
		return (String) storedValueForKey("fdipDeliberation");
	}

	public void setFdipDeliberation(String value) {
		takeStoredValueForKey(value, "fdipDeliberation");
	}
	public Integer fdipDepart() {
		return (Integer) storedValueForKey("fdipDepart");
	}

	public void setFdipDepart(Integer value) {
		takeStoredValueForKey(value, "fdipDepart");
	}
	public String fdipLibelle() {
		return (String) storedValueForKey("fdipLibelle");
	}

	public void setFdipLibelle(String value) {
		takeStoredValueForKey(value, "fdipLibelle");
	}
	public String fdipMention() {
		return (String) storedValueForKey("fdipMention");
	}

	public void setFdipMention(String value) {
		takeStoredValueForKey(value, "fdipMention");
	}
	public String fdipModele() {
		return (String) storedValueForKey("fdipModele");
	}

	public void setFdipModele(String value) {
		takeStoredValueForKey(value, "fdipModele");
	}
	public Integer fdipMonoPosition() {
		return (Integer) storedValueForKey("fdipMonoPosition");
	}

	public void setFdipMonoPosition(Integer value) {
		takeStoredValueForKey(value, "fdipMonoPosition");
	}
	public String fdipMonoSemestre() {
		return (String) storedValueForKey("fdipMonoSemestre");
	}

	public void setFdipMonoSemestre(String value) {
		takeStoredValueForKey(value, "fdipMonoSemestre");
	}
	public String fdipSemestrialisation() {
		return (String) storedValueForKey("fdipSemestrialisation");
	}

	public void setFdipSemestrialisation(String value) {
		takeStoredValueForKey(value, "fdipSemestrialisation");
	}
	public String fdipSpecialite() {
		return (String) storedValueForKey("fdipSpecialite");
	}

	public void setFdipSpecialite(String value) {
		takeStoredValueForKey(value, "fdipSpecialite");
	}
	public String fdipType() {
		return (String) storedValueForKey("fdipType");
	}

	public void setFdipType(String value) {
		takeStoredValueForKey(value, "fdipType");
	}
	public String fdipTypeDroit() {
		return (String) storedValueForKey("fdipTypeDroit");
	}

	public void setFdipTypeDroit(String value) {
		takeStoredValueForKey(value, "fdipTypeDroit");
	}
	public String sremoCode() {
		return (String) storedValueForKey("sremoCode");
	}

	public void setSremoCode(String value) {
		takeStoredValueForKey(value, "sremoCode");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement toFwkScolarite_ScolFormationDepartement() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement)storedValueForKey("toFwkScolarite_ScolFormationDepartement");
	}

	public void setToFwkScolarite_ScolFormationDepartementRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement oldValue = toFwkScolarite_ScolFormationDepartement();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDepartement");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDepartement");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise toFwkScolarite_ScolFormationDiplomeSise() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise)storedValueForKey("toFwkScolarite_ScolFormationDiplomeSise");
	}

	public void setToFwkScolarite_ScolFormationDiplomeSiseRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise oldValue = toFwkScolarite_ScolFormationDiplomeSise();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDiplomeSise");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDiplomeSise");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine toFwkScolarite_ScolFormationDomaine() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine)storedValueForKey("toFwkScolarite_ScolFormationDomaine");
	}

	public void setToFwkScolarite_ScolFormationDomaineRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine oldValue = toFwkScolarite_ScolFormationDomaine();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDomaine");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDomaine");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere toFwkScolarite_ScolFormationFiliere() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere)storedValueForKey("toFwkScolarite_ScolFormationFiliere");
	}

	public void setToFwkScolarite_ScolFormationFiliereRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere oldValue = toFwkScolarite_ScolFormationFiliere();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationFiliere");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationFiliere");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade toFwkScolarite_ScolFormationGrade() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade)storedValueForKey("toFwkScolarite_ScolFormationGrade");
	}

	public void setToFwkScolarite_ScolFormationGradeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade oldValue = toFwkScolarite_ScolFormationGrade();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationGrade");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationGrade");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationVocation toFwkScolarite_ScolFormationVocation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationVocation)storedValueForKey("toFwkScolarite_ScolFormationVocation");
	}

	public void setToFwkScolarite_ScolFormationVocationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationVocation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationVocation oldValue = toFwkScolarite_ScolFormationVocation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationVocation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationVocation");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite toFwkScolarix_VComposanteScolarite() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite)storedValueForKey("toFwkScolarix_VComposanteScolarite");
	}

	public void setToFwkScolarix_VComposanteScolariteRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite oldValue = toFwkScolarix_VComposanteScolarite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarix_VComposanteScolarite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarix_VComposanteScolarite");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite toFwkScolarix_VEtablissementScolarite() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite)storedValueForKey("toFwkScolarix_VEtablissementScolarite");
	}

	public void setToFwkScolarix_VEtablissementScolariteRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite oldValue = toFwkScolarix_VEtablissementScolarite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarix_VEtablissementScolarite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarix_VEtablissementScolarite");
		}
	}
  
	public NSArray toFwkScolarite_ScolFormationDisciplinaires() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolFormationDisciplinaires");
	}

	public NSArray toFwkScolarite_ScolFormationDisciplinaires(EOQualifier qualifier) {
		return toFwkScolarite_ScolFormationDisciplinaires(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolFormationDisciplinaires(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolFormationDisciplinaires(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolFormationDisciplinaires(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolFormationDisciplinaires();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolFormationDisciplinairesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplinaires");
	}

	public void removeFromToFwkScolarite_ScolFormationDisciplinairesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplinaires");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire createToFwkScolarite_ScolFormationDisciplinairesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolFormationDisciplinaire");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolFormationDisciplinaires");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire) eo;
	}

	public void deleteToFwkScolarite_ScolFormationDisciplinairesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDisciplinaires");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolFormationDisciplinairesRelationships() {
		Enumeration objects = toFwkScolarite_ScolFormationDisciplinaires().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolFormationDisciplinairesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolFormationSpecialisations() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolFormationSpecialisations");
	}

	public NSArray toFwkScolarite_ScolFormationSpecialisations(EOQualifier qualifier) {
		return toFwkScolarite_ScolFormationSpecialisations(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolFormationSpecialisations(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolFormationSpecialisations(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolFormationSpecialisations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolFormationSpecialisations();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolFormationSpecialisationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationSpecialisations");
	}

	public void removeFromToFwkScolarite_ScolFormationSpecialisationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationSpecialisations");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation createToFwkScolarite_ScolFormationSpecialisationsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolFormationSpecialisation");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolFormationSpecialisations");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation) eo;
	}

	public void deleteToFwkScolarite_ScolFormationSpecialisationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationSpecialisations");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolFormationSpecialisationsRelationships() {
		Enumeration objects = toFwkScolarite_ScolFormationSpecialisations().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolFormationSpecialisationsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolFormationDiplome.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolFormationDiplome.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolFormationDiplome)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolFormationDiplome fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolFormationDiplome fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolFormationDiplome eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolFormationDiplome)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolFormationDiplome fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationDiplome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationDiplome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolFormationDiplome eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolFormationDiplome)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationDiplome fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationDiplome fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolFormationDiplome eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolFormationDiplome ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolFormationDiplome createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolFormationDiplome.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolFormationDiplome.ENTITY_NAME + "' !");
		}
		else {
			EOScolFormationDiplome object = (EOScolFormationDiplome) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolFormationDiplome localInstanceOfObject(EOEditingContext ec, EOScolFormationDiplome object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolFormationDiplome " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolFormationDiplome) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
