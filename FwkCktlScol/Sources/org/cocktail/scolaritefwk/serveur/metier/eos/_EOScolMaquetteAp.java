/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteAp.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteAp extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteAp";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_AP";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mapKey";

	public static final ERXKey<Integer> MAP_GROUPE_PREVU = new ERXKey<Integer>("mapGroupePrevu");
	public static final ERXKey<Integer> MAP_GROUPE_REEL = new ERXKey<Integer>("mapGroupeReel");
	public static final ERXKey<String> MAP_LIBELLE = new ERXKey<String>("mapLibelle");
	public static final ERXKey<Integer> MAP_SEUIL = new ERXKey<Integer>("mapSeuil");
	public static final ERXKey<java.math.BigDecimal> MAP_VALEUR = new ERXKey<java.math.BigDecimal>("mapValeur");

	public static final String MAP_GROUPE_PREVU_KEY = "mapGroupePrevu";
	public static final String MAP_GROUPE_REEL_KEY = "mapGroupeReel";
	public static final String MAP_LIBELLE_KEY = "mapLibelle";
	public static final String MAP_SEUIL_KEY = "mapSeuil";
	public static final String MAP_VALEUR_KEY = "mapValeur";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MAP_KEY_KEY = "mapKey";
	public static final String MHCO_CODE_KEY = "mhcoCode";
	public static final String MHMO_CODE_KEY = "mhmoCode";

	// Colkeys
	public static final String MAP_GROUPE_PREVU_COLKEY = "MAP_GROUPE_PREVU";
	public static final String MAP_GROUPE_REEL_COLKEY = "MAP_GROUPE_REEL";
	public static final String MAP_LIBELLE_COLKEY = "MAP_LIBELLE";
	public static final String MAP_SEUIL_COLKEY = "MAP_SEUIL";
	public static final String MAP_VALEUR_COLKEY = "MAP_VALEUR";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MAP_KEY_COLKEY = "MAP_KEY";
	public static final String MHCO_CODE_COLKEY = "MHCO_CODE";
	public static final String MHMO_CODE_COLKEY = "MHMO_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet> TO_FWK_SCOLARITE__SCOL_GROUPE_OBJETS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet>("toFwkScolarite_ScolGroupeObjets");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp> TO_FWK_SCOLARITE__SCOL_MAQUETTE_CHARGES_APS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp>("toFwkScolarite_ScolMaquetteChargesAps");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode> TO_FWK_SCOLARITE__SCOL_MAQUETTE_HORAIRE_CODE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode>("toFwkScolarite_ScolMaquetteHoraireCode");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireMode> TO_FWK_SCOLARITE__SCOL_MAQUETTE_HORAIRE_MODE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireMode>("toFwkScolarite_ScolMaquetteHoraireMode");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_APS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp>("toFwkScolarite_ScolMaquetteRepartitionAps");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_OBJETS_KEY = "toFwkScolarite_ScolGroupeObjets";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_CHARGES_APS_KEY = "toFwkScolarite_ScolMaquetteChargesAps";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_HORAIRE_CODE_KEY = "toFwkScolarite_ScolMaquetteHoraireCode";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_HORAIRE_MODE_KEY = "toFwkScolarite_ScolMaquetteHoraireMode";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_APS_KEY = "toFwkScolarite_ScolMaquetteRepartitionAps";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteAp with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mapGroupePrevu
	 * @param mapGroupeReel
	 * @param mapLibelle
	 * @param mapSeuil
	 * @param mapValeur
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteHoraireCode
	 * @param toFwkScolarite_ScolMaquetteHoraireMode
	 * @return EOScolMaquetteAp
	 */
	public static EOScolMaquetteAp create(EOEditingContext editingContext, Integer mapGroupePrevu, Integer mapGroupeReel, String mapLibelle, Integer mapSeuil, java.math.BigDecimal mapValeur, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode toFwkScolarite_ScolMaquetteHoraireCode, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireMode toFwkScolarite_ScolMaquetteHoraireMode) {
		EOScolMaquetteAp eo = (EOScolMaquetteAp) createAndInsertInstance(editingContext);
		eo.setMapGroupePrevu(mapGroupePrevu);
		eo.setMapGroupeReel(mapGroupeReel);
		eo.setMapLibelle(mapLibelle);
		eo.setMapSeuil(mapSeuil);
		eo.setMapValeur(mapValeur);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteHoraireCodeRelationship(toFwkScolarite_ScolMaquetteHoraireCode);
		eo.setToFwkScolarite_ScolMaquetteHoraireModeRelationship(toFwkScolarite_ScolMaquetteHoraireMode);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteAp.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteAp
	 */
	public static EOScolMaquetteAp create(EOEditingContext editingContext) {
		EOScolMaquetteAp eo = (EOScolMaquetteAp) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteAp localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteAp localInstance = (EOScolMaquetteAp) localInstanceOfObject(editingContext, (EOScolMaquetteAp) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteAp localInstanceIn(EOEditingContext editingContext, EOScolMaquetteAp eo) {
		EOScolMaquetteAp localInstance = (eo == null) ? null : (EOScolMaquetteAp) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mapGroupePrevu() {
		return (Integer) storedValueForKey("mapGroupePrevu");
	}

	public void setMapGroupePrevu(Integer value) {
		takeStoredValueForKey(value, "mapGroupePrevu");
	}
	public Integer mapGroupeReel() {
		return (Integer) storedValueForKey("mapGroupeReel");
	}

	public void setMapGroupeReel(Integer value) {
		takeStoredValueForKey(value, "mapGroupeReel");
	}
	public String mapLibelle() {
		return (String) storedValueForKey("mapLibelle");
	}

	public void setMapLibelle(String value) {
		takeStoredValueForKey(value, "mapLibelle");
	}
	public Integer mapSeuil() {
		return (Integer) storedValueForKey("mapSeuil");
	}

	public void setMapSeuil(Integer value) {
		takeStoredValueForKey(value, "mapSeuil");
	}
	public java.math.BigDecimal mapValeur() {
		return (java.math.BigDecimal) storedValueForKey("mapValeur");
	}

	public void setMapValeur(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mapValeur");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode toFwkScolarite_ScolMaquetteHoraireCode() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode)storedValueForKey("toFwkScolarite_ScolMaquetteHoraireCode");
	}

	public void setToFwkScolarite_ScolMaquetteHoraireCodeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode oldValue = toFwkScolarite_ScolMaquetteHoraireCode();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteHoraireCode");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteHoraireCode");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireMode toFwkScolarite_ScolMaquetteHoraireMode() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireMode)storedValueForKey("toFwkScolarite_ScolMaquetteHoraireMode");
	}

	public void setToFwkScolarite_ScolMaquetteHoraireModeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireMode value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireMode oldValue = toFwkScolarite_ScolMaquetteHoraireMode();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteHoraireMode");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteHoraireMode");
		}
	}
  
	public NSArray toFwkScolarite_ScolGroupeObjets() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolGroupeObjets");
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier) {
		return toFwkScolarite_ScolGroupeObjets(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolGroupeObjets(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet.TO_FWK_SCOLARITE__SCOL_MAQUETTE_AP_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolGroupeObjets();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
	}

	public void removeFromToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet createToFwkScolarite_ScolGroupeObjetsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolGroupeObjet");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolGroupeObjets");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet) eo;
	}

	public void deleteToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolGroupeObjetsRelationships() {
		Enumeration objects = toFwkScolarite_ScolGroupeObjets().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolGroupeObjetsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteChargesAps() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteChargesAps");
	}

	public NSArray toFwkScolarite_ScolMaquetteChargesAps(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteChargesAps(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteChargesAps(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteChargesAps(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteChargesAps(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp.TO_FWK_SCOLARITE__SCOL_MAQUETTE_AP_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteChargesAps();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteChargesApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteChargesAps");
	}

	public void removeFromToFwkScolarite_ScolMaquetteChargesApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteChargesAps");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp createToFwkScolarite_ScolMaquetteChargesApsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteChargesAp");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteChargesAps");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteChargesApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteChargesAps");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteChargesApsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteChargesAps().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteChargesApsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteChargesAp)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteRepartitionAps() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionAps");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionAps(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionAps(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionAps(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionAps(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionAps(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp.TO_FWK_SCOLARITE__SCOL_MAQUETTE_AP_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionAps();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionAps");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionAps");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp createToFwkScolarite_ScolMaquetteRepartitionApsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionAp");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionAps");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionApsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionAps");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionApsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionAps().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionApsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteAp.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteAp.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteAp)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteAp fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteAp fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteAp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteAp)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteAp fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteAp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteAp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteAp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteAp)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteAp fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteAp fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteAp eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteAp ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteAp createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteAp.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteAp.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteAp object = (EOScolMaquetteAp) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteAp localInstanceOfObject(EOEditingContext ec, EOScolMaquetteAp object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteAp " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteAp) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
