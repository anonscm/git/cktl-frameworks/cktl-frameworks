/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.metier.eos;

import org.cocktail.scolaritefwk.serveur.exception.ScolFormationSpecialisationException;

import com.webobjects.foundation.NSValidation;

public class EOScolFormationSpecialisation extends _EOScolFormationSpecialisation {

	// Valeurs possibles pour les actions
	public static final String ACTION_INSERT = "I";
	public static final String ACTION_UPDATE = "U";
	private String actionType;

	public EOScolFormationSpecialisation() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.setActionType(ACTION_INSERT);
		this.validateObjectMetier();
		this.validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.setActionType(ACTION_UPDATE);
		this.validateObjectMetier();
		this.validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		this.validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele a partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		// validate for Insert
		if (this.isActionInsert() == true) {
			// On valide l'objet lui-meme
			this.validateFormationDiplomeSpecialisationForInsert();
			// On valide les dependances
			toFwkScolarite_ScolFormationDiplome().validateForInsert();
		}
		// validate for Update
		if (this.isActionUpdate() == true) {
			// On valide l'objet lui-meme
			this.validateFormationDiplomeSpecialisationForUpdate();
			// On valide les dependances
			toFwkScolarite_ScolFormationDiplome().validateForUpdate();
		}

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public void validateFormationDiplomeSpecialisationForInsert() throws ScolFormationSpecialisationException {
		// champs obligatoires
		if (toFwkScolarite_ScolFormationDiplome() == null) {
			throw new ScolFormationSpecialisationException("La formation n'a pas de diplome (scolFormationDiplome)");
		}
		// champs obligatoires conditionnels
		// verifications
		// particularites, actions
		toFwkScolarite_ScolFormationDiplome().setActionType(actionType());
	}

	public void validateFormationDiplomeSpecialisationForUpdate() throws ScolFormationSpecialisationException {
		// champs obligatoires
		if (fspnKey() == null) {
			throw new ScolFormationSpecialisationException("Une formation doit etre selectionnee");
		}
		if (toFwkScolarite_ScolFormationDiplome() == null) {
			throw new ScolFormationSpecialisationException("La formation n'a pas de diplome (scolFormationDiplome)");
		}
		// champs obligatoires conditionnels
		// verifications
		// particularites, actions
		toFwkScolarite_ScolFormationDiplome().setActionType(actionType());
	}

	/*
	 * Gestion du type d'action effectuee : AJOUTER MODIFIER
	 */
	public String actionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public boolean isActionInsert() {
		return this.actionType() != null && this.actionType().equals(EOScolFormationSpecialisation.ACTION_INSERT);
	}

	public boolean isActionUpdate() {
		return this.actionType() != null && this.actionType().equals(EOScolFormationSpecialisation.ACTION_UPDATE);
	}

	/**
	 * Retourne le libelle abrege du diplome.
	 * 
	 * @return String
	 */
	public String libelleDiplomeAbrege(Integer idiplAnneeSuivie) {
		StringBuffer sb = new StringBuffer();
		EOScolFormationDiplome diplome = toFwkScolarite_ScolFormationDiplome();
		sb = sb.append(diplome.filiere().abreviation());
		sb = sb.append(" ");
		if (idiplAnneeSuivie != null) {
			sb = sb.append(idiplAnneeSuivie);
			sb = sb.append(" ");
		}
		sb = sb.append(diplome.fdipAbreviation());
		if (fspnLibelle() != null) {
			sb = sb.append(" ");
			sb = sb.append(fspnLibelle());
		}
		return sb.toString();
	}

	/**
	 * Retourne le libelle complet du diplome.
	 * 
	 * @return String
	 */
	public String libelleDiplomeComplet(Integer idiplAnneeSuivie) {
		StringBuffer sb = new StringBuffer();
		EOScolFormationDiplome diplome = toFwkScolarite_ScolFormationDiplome();
		sb = sb.append(diplome.filiere().abreviation());
		sb = sb.append(" ");
		if (idiplAnneeSuivie != null) {
			sb = sb.append(idiplAnneeSuivie);
			sb = sb.append(" ");
		}
		sb = sb.append(diplome.fdipLibelle());
		if (fspnLibelle() != null) {
			sb = sb.append(" ");
			sb = sb.append(fspnLibelle());
		}
		return sb.toString();
	}

}
