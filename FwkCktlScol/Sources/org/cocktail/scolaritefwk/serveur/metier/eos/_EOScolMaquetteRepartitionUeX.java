/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteRepartitionUeX.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteRepartitionUeX extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteRepartitionUeX";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_REPARTITION_UE_X";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mruxKey";

	public static final ERXKey<Integer> MRUX_BONIFIABLE = new ERXKey<Integer>("mruxBonifiable");
	public static final ERXKey<java.math.BigDecimal> MRUX_COEFFICIENT = new ERXKey<java.math.BigDecimal>("mruxCoefficient");
	public static final ERXKey<Integer> MRUX_COMPTABILISABLE = new ERXKey<Integer>("mruxComptabilisable");
	public static final ERXKey<java.math.BigDecimal> MRUX_NOTE_BASE = new ERXKey<java.math.BigDecimal>("mruxNoteBase");
	public static final ERXKey<java.math.BigDecimal> MRUX_NOTE_ELIMINATION = new ERXKey<java.math.BigDecimal>("mruxNoteElimination");
	public static final ERXKey<java.math.BigDecimal> MRUX_NOTE_OBTENTION = new ERXKey<java.math.BigDecimal>("mruxNoteObtention");
	public static final ERXKey<Integer> MRUX_ORDRE = new ERXKey<Integer>("mruxOrdre");

	public static final String MRUX_BONIFIABLE_KEY = "mruxBonifiable";
	public static final String MRUX_COEFFICIENT_KEY = "mruxCoefficient";
	public static final String MRUX_COMPTABILISABLE_KEY = "mruxComptabilisable";
	public static final String MRUX_NOTE_BASE_KEY = "mruxNoteBase";
	public static final String MRUX_NOTE_ELIMINATION_KEY = "mruxNoteElimination";
	public static final String MRUX_NOTE_OBTENTION_KEY = "mruxNoteObtention";
	public static final String MRUX_ORDRE_KEY = "mruxOrdre";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MRUX_KEY_KEY = "mruxKey";
	public static final String MSEM_KEY_KEY = "msemKey";
	public static final String MUX_KEY_KEY = "muxKey";

	// Colkeys
	public static final String MRUX_BONIFIABLE_COLKEY = "MRUX_BONIFIABLE";
	public static final String MRUX_COEFFICIENT_COLKEY = "MRUX_COEFFICIENT";
	public static final String MRUX_COMPTABILISABLE_COLKEY = "MRUX_COMPTABILISABLE";
	public static final String MRUX_NOTE_BASE_COLKEY = "MRUX_NOTE_BASE";
	public static final String MRUX_NOTE_ELIMINATION_COLKEY = "MRUX_NOTE_ELIMINATION";
	public static final String MRUX_NOTE_OBTENTION_COLKEY = "MRUX_NOTE_OBTENTION";
	public static final String MRUX_ORDRE_COLKEY = "MRUX_ORDRE";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MRUX_KEY_COLKEY = "MRUX_KEY";
	public static final String MSEM_KEY_COLKEY = "MSEM_KEY";
	public static final String MUX_KEY_COLKEY = "MUX_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre> TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre>("toFwkScolarite_ScolMaquetteSemestre");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeX> TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_X = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeX>("toFwkScolarite_ScolMaquetteUeX");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY = "toFwkScolarite_ScolMaquetteSemestre";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_X_KEY = "toFwkScolarite_ScolMaquetteUeX";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteRepartitionUeX with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mruxBonifiable
	 * @param mruxCoefficient
	 * @param mruxComptabilisable
	 * @param mruxNoteBase
	 * @param mruxNoteObtention
	 * @param mruxOrdre
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteSemestre
	 * @param toFwkScolarite_ScolMaquetteUeX
	 * @return EOScolMaquetteRepartitionUeX
	 */
	public static EOScolMaquetteRepartitionUeX create(EOEditingContext editingContext, Integer mruxBonifiable, java.math.BigDecimal mruxCoefficient, Integer mruxComptabilisable, java.math.BigDecimal mruxNoteBase, java.math.BigDecimal mruxNoteObtention, Integer mruxOrdre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeX toFwkScolarite_ScolMaquetteUeX) {
		EOScolMaquetteRepartitionUeX eo = (EOScolMaquetteRepartitionUeX) createAndInsertInstance(editingContext);
		eo.setMruxBonifiable(mruxBonifiable);
		eo.setMruxCoefficient(mruxCoefficient);
		eo.setMruxComptabilisable(mruxComptabilisable);
		eo.setMruxNoteBase(mruxNoteBase);
		eo.setMruxNoteObtention(mruxNoteObtention);
		eo.setMruxOrdre(mruxOrdre);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteSemestreRelationship(toFwkScolarite_ScolMaquetteSemestre);
		eo.setToFwkScolarite_ScolMaquetteUeXRelationship(toFwkScolarite_ScolMaquetteUeX);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteRepartitionUeX.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteRepartitionUeX
	 */
	public static EOScolMaquetteRepartitionUeX create(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionUeX eo = (EOScolMaquetteRepartitionUeX) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteRepartitionUeX localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionUeX localInstance = (EOScolMaquetteRepartitionUeX) localInstanceOfObject(editingContext, (EOScolMaquetteRepartitionUeX) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteRepartitionUeX localInstanceIn(EOEditingContext editingContext, EOScolMaquetteRepartitionUeX eo) {
		EOScolMaquetteRepartitionUeX localInstance = (eo == null) ? null : (EOScolMaquetteRepartitionUeX) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mruxBonifiable() {
		return (Integer) storedValueForKey("mruxBonifiable");
	}

	public void setMruxBonifiable(Integer value) {
		takeStoredValueForKey(value, "mruxBonifiable");
	}
	public java.math.BigDecimal mruxCoefficient() {
		return (java.math.BigDecimal) storedValueForKey("mruxCoefficient");
	}

	public void setMruxCoefficient(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mruxCoefficient");
	}
	public Integer mruxComptabilisable() {
		return (Integer) storedValueForKey("mruxComptabilisable");
	}

	public void setMruxComptabilisable(Integer value) {
		takeStoredValueForKey(value, "mruxComptabilisable");
	}
	public java.math.BigDecimal mruxNoteBase() {
		return (java.math.BigDecimal) storedValueForKey("mruxNoteBase");
	}

	public void setMruxNoteBase(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mruxNoteBase");
	}
	public java.math.BigDecimal mruxNoteElimination() {
		return (java.math.BigDecimal) storedValueForKey("mruxNoteElimination");
	}

	public void setMruxNoteElimination(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mruxNoteElimination");
	}
	public java.math.BigDecimal mruxNoteObtention() {
		return (java.math.BigDecimal) storedValueForKey("mruxNoteObtention");
	}

	public void setMruxNoteObtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mruxNoteObtention");
	}
	public Integer mruxOrdre() {
		return (Integer) storedValueForKey("mruxOrdre");
	}

	public void setMruxOrdre(Integer value) {
		takeStoredValueForKey(value, "mruxOrdre");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre)storedValueForKey("toFwkScolarite_ScolMaquetteSemestre");
	}

	public void setToFwkScolarite_ScolMaquetteSemestreRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre oldValue = toFwkScolarite_ScolMaquetteSemestre();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteSemestre");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteSemestre");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeX toFwkScolarite_ScolMaquetteUeX() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeX)storedValueForKey("toFwkScolarite_ScolMaquetteUeX");
	}

	public void setToFwkScolarite_ScolMaquetteUeXRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeX value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeX oldValue = toFwkScolarite_ScolMaquetteUeX();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteUeX");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteUeX");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteRepartitionUeX.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteRepartitionUeX.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteRepartitionUeX)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteRepartitionUeX fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteRepartitionUeX fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteRepartitionUeX eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteRepartitionUeX)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionUeX fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionUeX fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionUeX fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteRepartitionUeX eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteRepartitionUeX)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionUeX fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionUeX fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteRepartitionUeX eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteRepartitionUeX ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteRepartitionUeX createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteRepartitionUeX.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteRepartitionUeX.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteRepartitionUeX object = (EOScolMaquetteRepartitionUeX) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteRepartitionUeX localInstanceOfObject(EOEditingContext ec, EOScolMaquetteRepartitionUeX object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteRepartitionUeX " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteRepartitionUeX) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
