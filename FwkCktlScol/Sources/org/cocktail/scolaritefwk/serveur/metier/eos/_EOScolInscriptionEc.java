/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolInscriptionEc.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolInscriptionEc extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolInscriptionEc";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_INSCRIPTION_EC";

	//Attributes

	public static final ERXKey<Integer> FANN_KEY = new ERXKey<Integer>("fannKey");
	public static final ERXKey<Long> IMREC_ABSENCE1 = new ERXKey<Long>("imrecAbsence1");
	public static final ERXKey<Long> IMREC_ABSENCE2 = new ERXKey<Long>("imrecAbsence2");
	public static final ERXKey<java.math.BigDecimal> IMREC_COEFFICIENT = new ERXKey<java.math.BigDecimal>("imrecCoefficient");
	public static final ERXKey<Integer> IMREC_DISPENSE = new ERXKey<Integer>("imrecDispense");
	public static final ERXKey<Long> IMREC_ETAT = new ERXKey<Long>("imrecEtat");
	public static final ERXKey<Long> IMREC_MENTION1 = new ERXKey<Long>("imrecMention1");
	public static final ERXKey<Long> IMREC_MENTION2 = new ERXKey<Long>("imrecMention2");
	public static final ERXKey<java.math.BigDecimal> IMREC_NOTE_BASE = new ERXKey<java.math.BigDecimal>("imrecNoteBase");
	public static final ERXKey<String> IMREC_OBTENTION = new ERXKey<String>("imrecObtention");
	public static final ERXKey<java.math.BigDecimal> IMREC_POINT_JURY = new ERXKey<java.math.BigDecimal>("imrecPointJury");
	public static final ERXKey<java.math.BigDecimal> IMREC_POINTS1 = new ERXKey<java.math.BigDecimal>("imrecPoints1");
	public static final ERXKey<java.math.BigDecimal> IMREC_POINTS2 = new ERXKey<java.math.BigDecimal>("imrecPoints2");
	public static final ERXKey<java.math.BigDecimal> IMREC_PONDERATION = new ERXKey<java.math.BigDecimal>("imrecPonderation");
	public static final ERXKey<java.math.BigDecimal> IMREC_SESSION1 = new ERXKey<java.math.BigDecimal>("imrecSession1");
	public static final ERXKey<java.math.BigDecimal> IMREC_SESSION2 = new ERXKey<java.math.BigDecimal>("imrecSession2");

	public static final String FANN_KEY_KEY = "fannKey";
	public static final String IMREC_ABSENCE1_KEY = "imrecAbsence1";
	public static final String IMREC_ABSENCE2_KEY = "imrecAbsence2";
	public static final String IMREC_COEFFICIENT_KEY = "imrecCoefficient";
	public static final String IMREC_DISPENSE_KEY = "imrecDispense";
	public static final String IMREC_ETAT_KEY = "imrecEtat";
	public static final String IMREC_MENTION1_KEY = "imrecMention1";
	public static final String IMREC_MENTION2_KEY = "imrecMention2";
	public static final String IMREC_NOTE_BASE_KEY = "imrecNoteBase";
	public static final String IMREC_OBTENTION_KEY = "imrecObtention";
	public static final String IMREC_POINT_JURY_KEY = "imrecPointJury";
	public static final String IMREC_POINTS1_KEY = "imrecPoints1";
	public static final String IMREC_POINTS2_KEY = "imrecPoints2";
	public static final String IMREC_PONDERATION_KEY = "imrecPonderation";
	public static final String IMREC_SESSION1_KEY = "imrecSession1";
	public static final String IMREC_SESSION2_KEY = "imrecSession2";

	// Non visible attributes
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String IMREC_SEMESTRE_KEY = "imrecSemestre";
	public static final String MREC_KEY_KEY = "mrecKey";

	// Colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String IMREC_ABSENCE1_COLKEY = "IMREC_ABSENCE1";
	public static final String IMREC_ABSENCE2_COLKEY = "IMREC_ABSENCE2";
	public static final String IMREC_COEFFICIENT_COLKEY = "IMREC_COEFFICIENT";
	public static final String IMREC_DISPENSE_COLKEY = "IMREC_DISPENSE";
	public static final String IMREC_ETAT_COLKEY = "IMREC_ETAT";
	public static final String IMREC_MENTION1_COLKEY = "IMREC_MENTION1";
	public static final String IMREC_MENTION2_COLKEY = "IMREC_MENTION2";
	public static final String IMREC_NOTE_BASE_COLKEY = "IMREC_NOTE_BASE";
	public static final String IMREC_OBTENTION_COLKEY = "IMREC_OBTENTION";
	public static final String IMREC_POINT_JURY_COLKEY = "IMREC_POINT_JURY";
	public static final String IMREC_POINTS1_COLKEY = "IMREC_POINTS1";
	public static final String IMREC_POINTS2_COLKEY = "IMREC_POINTS2";
	public static final String IMREC_PONDERATION_COLKEY = "IMREC_PONDERATION";
	public static final String IMREC_SESSION1_COLKEY = "IMREC_SESSION1";
	public static final String IMREC_SESSION2_COLKEY = "IMREC_SESSION2";

	// Non visible colkeys
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String IMREC_SEMESTRE_COLKEY = "IMREC_SEMESTRE";
	public static final String MREC_KEY_COLKEY = "MREC_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant> TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ETUDIANT = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant>("toFwkScolarite_ScolInscriptionEtudiant");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_EC = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc>("toFwkScolarite_ScolMaquetteRepartitionEc");

	public static final String TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ETUDIANT_KEY = "toFwkScolarite_ScolInscriptionEtudiant";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_EC_KEY = "toFwkScolarite_ScolMaquetteRepartitionEc";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolInscriptionEc with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fannKey
	 * @param imrecAbsence1
	 * @param imrecAbsence2
	 * @param imrecDispense
	 * @param imrecEtat
	 * @param imrecMention1
	 * @param imrecMention2
	 * @param toFwkScolarite_ScolInscriptionEtudiant
	 * @param toFwkScolarite_ScolMaquetteRepartitionEc
	 * @return EOScolInscriptionEc
	 */
	public static EOScolInscriptionEc create(EOEditingContext editingContext, Integer fannKey, Long imrecAbsence1, Long imrecAbsence2, Integer imrecDispense, Long imrecEtat, Long imrecMention1, Long imrecMention2, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant toFwkScolarite_ScolInscriptionEtudiant, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc toFwkScolarite_ScolMaquetteRepartitionEc) {
		EOScolInscriptionEc eo = (EOScolInscriptionEc) createAndInsertInstance(editingContext);
		eo.setFannKey(fannKey);
		eo.setImrecAbsence1(imrecAbsence1);
		eo.setImrecAbsence2(imrecAbsence2);
		eo.setImrecDispense(imrecDispense);
		eo.setImrecEtat(imrecEtat);
		eo.setImrecMention1(imrecMention1);
		eo.setImrecMention2(imrecMention2);
		eo.setToFwkScolarite_ScolInscriptionEtudiantRelationship(toFwkScolarite_ScolInscriptionEtudiant);
		eo.setToFwkScolarite_ScolMaquetteRepartitionEcRelationship(toFwkScolarite_ScolMaquetteRepartitionEc);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolInscriptionEc.
	 *
	 * @param editingContext
	 * @return EOScolInscriptionEc
	 */
	public static EOScolInscriptionEc create(EOEditingContext editingContext) {
		EOScolInscriptionEc eo = (EOScolInscriptionEc) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolInscriptionEc localInstanceIn(EOEditingContext editingContext) {
		EOScolInscriptionEc localInstance = (EOScolInscriptionEc) localInstanceOfObject(editingContext, (EOScolInscriptionEc) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolInscriptionEc localInstanceIn(EOEditingContext editingContext, EOScolInscriptionEc eo) {
		EOScolInscriptionEc localInstance = (eo == null) ? null : (EOScolInscriptionEc) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer fannKey() {
		return (Integer) storedValueForKey("fannKey");
	}

	public void setFannKey(Integer value) {
		takeStoredValueForKey(value, "fannKey");
	}
	public Long imrecAbsence1() {
		return (Long) storedValueForKey("imrecAbsence1");
	}

	public void setImrecAbsence1(Long value) {
		takeStoredValueForKey(value, "imrecAbsence1");
	}
	public Long imrecAbsence2() {
		return (Long) storedValueForKey("imrecAbsence2");
	}

	public void setImrecAbsence2(Long value) {
		takeStoredValueForKey(value, "imrecAbsence2");
	}
	public java.math.BigDecimal imrecCoefficient() {
		return (java.math.BigDecimal) storedValueForKey("imrecCoefficient");
	}

	public void setImrecCoefficient(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrecCoefficient");
	}
	public Integer imrecDispense() {
		return (Integer) storedValueForKey("imrecDispense");
	}

	public void setImrecDispense(Integer value) {
		takeStoredValueForKey(value, "imrecDispense");
	}
	public Long imrecEtat() {
		return (Long) storedValueForKey("imrecEtat");
	}

	public void setImrecEtat(Long value) {
		takeStoredValueForKey(value, "imrecEtat");
	}
	public Long imrecMention1() {
		return (Long) storedValueForKey("imrecMention1");
	}

	public void setImrecMention1(Long value) {
		takeStoredValueForKey(value, "imrecMention1");
	}
	public Long imrecMention2() {
		return (Long) storedValueForKey("imrecMention2");
	}

	public void setImrecMention2(Long value) {
		takeStoredValueForKey(value, "imrecMention2");
	}
	public java.math.BigDecimal imrecNoteBase() {
		return (java.math.BigDecimal) storedValueForKey("imrecNoteBase");
	}

	public void setImrecNoteBase(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrecNoteBase");
	}
	public String imrecObtention() {
		return (String) storedValueForKey("imrecObtention");
	}

	public void setImrecObtention(String value) {
		takeStoredValueForKey(value, "imrecObtention");
	}
	public java.math.BigDecimal imrecPointJury() {
		return (java.math.BigDecimal) storedValueForKey("imrecPointJury");
	}

	public void setImrecPointJury(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrecPointJury");
	}
	public java.math.BigDecimal imrecPoints1() {
		return (java.math.BigDecimal) storedValueForKey("imrecPoints1");
	}

	public void setImrecPoints1(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrecPoints1");
	}
	public java.math.BigDecimal imrecPoints2() {
		return (java.math.BigDecimal) storedValueForKey("imrecPoints2");
	}

	public void setImrecPoints2(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrecPoints2");
	}
	public java.math.BigDecimal imrecPonderation() {
		return (java.math.BigDecimal) storedValueForKey("imrecPonderation");
	}

	public void setImrecPonderation(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrecPonderation");
	}
	public java.math.BigDecimal imrecSession1() {
		return (java.math.BigDecimal) storedValueForKey("imrecSession1");
	}

	public void setImrecSession1(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrecSession1");
	}
	public java.math.BigDecimal imrecSession2() {
		return (java.math.BigDecimal) storedValueForKey("imrecSession2");
	}

	public void setImrecSession2(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "imrecSession2");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant toFwkScolarite_ScolInscriptionEtudiant() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant)storedValueForKey("toFwkScolarite_ScolInscriptionEtudiant");
	}

	public void setToFwkScolarite_ScolInscriptionEtudiantRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant oldValue = toFwkScolarite_ScolInscriptionEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolInscriptionEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolInscriptionEtudiant");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc toFwkScolarite_ScolMaquetteRepartitionEc() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionEc");
	}

	public void setToFwkScolarite_ScolMaquetteRepartitionEcRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc oldValue = toFwkScolarite_ScolMaquetteRepartitionEc();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteRepartitionEc");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteRepartitionEc");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolInscriptionEc.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolInscriptionEc.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolInscriptionEc)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolInscriptionEc fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolInscriptionEc fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolInscriptionEc eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolInscriptionEc)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolInscriptionEc fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionEc fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionEc fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolInscriptionEc eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolInscriptionEc)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionEc fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionEc fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolInscriptionEc eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolInscriptionEc ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolInscriptionEc createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolInscriptionEc.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolInscriptionEc.ENTITY_NAME + "' !");
		}
		else {
			EOScolInscriptionEc object = (EOScolInscriptionEc) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolInscriptionEc localInstanceOfObject(EOEditingContext ec, EOScolInscriptionEc object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolInscriptionEc " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolInscriptionEc) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
