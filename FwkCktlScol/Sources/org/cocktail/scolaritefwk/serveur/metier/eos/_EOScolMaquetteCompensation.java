/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteCompensation.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteCompensation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteCompensation";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_COMPENSATION";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mcomKey";

	public static final ERXKey<String> MCOM_CODE = new ERXKey<String>("mcomCode");
	public static final ERXKey<Integer> MCOM_COMPTABILISABLE = new ERXKey<Integer>("mcomComptabilisable");
	public static final ERXKey<Integer> MCOM_FINAL = new ERXKey<Integer>("mcomFinal");
	public static final ERXKey<String> MCOM_FORMULE = new ERXKey<String>("mcomFormule");
	public static final ERXKey<Integer> MCOM_IMPRIMABLE = new ERXKey<Integer>("mcomImprimable");
	public static final ERXKey<Integer> MCOM_NIVEAU = new ERXKey<Integer>("mcomNiveau");
	public static final ERXKey<java.math.BigDecimal> MCOM_NOTE_BASE = new ERXKey<java.math.BigDecimal>("mcomNoteBase");
	public static final ERXKey<java.math.BigDecimal> MCOM_NOTE_OBTENTION = new ERXKey<java.math.BigDecimal>("mcomNoteObtention");

	public static final String MCOM_CODE_KEY = "mcomCode";
	public static final String MCOM_COMPTABILISABLE_KEY = "mcomComptabilisable";
	public static final String MCOM_FINAL_KEY = "mcomFinal";
	public static final String MCOM_FORMULE_KEY = "mcomFormule";
	public static final String MCOM_IMPRIMABLE_KEY = "mcomImprimable";
	public static final String MCOM_NIVEAU_KEY = "mcomNiveau";
	public static final String MCOM_NOTE_BASE_KEY = "mcomNoteBase";
	public static final String MCOM_NOTE_OBTENTION_KEY = "mcomNoteObtention";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MCOM_KEY_KEY = "mcomKey";
	public static final String MSEM_KEY_KEY = "msemKey";

	// Colkeys
	public static final String MCOM_CODE_COLKEY = "MCOM_CODE";
	public static final String MCOM_COMPTABILISABLE_COLKEY = "MCOM_COMPTABILISABLE";
	public static final String MCOM_FINAL_COLKEY = "MCOM_FINAL";
	public static final String MCOM_FORMULE_COLKEY = "MCOM_FORMULE";
	public static final String MCOM_IMPRIMABLE_COLKEY = "MCOM_IMPRIMABLE";
	public static final String MCOM_NIVEAU_COLKEY = "MCOM_NIVEAU";
	public static final String MCOM_NOTE_BASE_COLKEY = "MCOM_NOTE_BASE";
	public static final String MCOM_NOTE_OBTENTION_COLKEY = "MCOM_NOTE_OBTENTION";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MCOM_KEY_COLKEY = "MCOM_KEY";
	public static final String MSEM_KEY_COLKEY = "MSEM_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre> TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre>("toFwkScolarite_ScolMaquetteSemestre");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY = "toFwkScolarite_ScolMaquetteSemestre";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteCompensation with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mcomCode
	 * @param mcomComptabilisable
	 * @param mcomFinal
	 * @param mcomImprimable
	 * @param mcomNiveau
	 * @param mcomNoteBase
	 * @param mcomNoteObtention
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteSemestre
	 * @return EOScolMaquetteCompensation
	 */
	public static EOScolMaquetteCompensation create(EOEditingContext editingContext, String mcomCode, Integer mcomComptabilisable, Integer mcomFinal, Integer mcomImprimable, Integer mcomNiveau, java.math.BigDecimal mcomNoteBase, java.math.BigDecimal mcomNoteObtention, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre) {
		EOScolMaquetteCompensation eo = (EOScolMaquetteCompensation) createAndInsertInstance(editingContext);
		eo.setMcomCode(mcomCode);
		eo.setMcomComptabilisable(mcomComptabilisable);
		eo.setMcomFinal(mcomFinal);
		eo.setMcomImprimable(mcomImprimable);
		eo.setMcomNiveau(mcomNiveau);
		eo.setMcomNoteBase(mcomNoteBase);
		eo.setMcomNoteObtention(mcomNoteObtention);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteSemestreRelationship(toFwkScolarite_ScolMaquetteSemestre);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteCompensation.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteCompensation
	 */
	public static EOScolMaquetteCompensation create(EOEditingContext editingContext) {
		EOScolMaquetteCompensation eo = (EOScolMaquetteCompensation) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteCompensation localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteCompensation localInstance = (EOScolMaquetteCompensation) localInstanceOfObject(editingContext, (EOScolMaquetteCompensation) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteCompensation localInstanceIn(EOEditingContext editingContext, EOScolMaquetteCompensation eo) {
		EOScolMaquetteCompensation localInstance = (eo == null) ? null : (EOScolMaquetteCompensation) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String mcomCode() {
		return (String) storedValueForKey("mcomCode");
	}

	public void setMcomCode(String value) {
		takeStoredValueForKey(value, "mcomCode");
	}
	public Integer mcomComptabilisable() {
		return (Integer) storedValueForKey("mcomComptabilisable");
	}

	public void setMcomComptabilisable(Integer value) {
		takeStoredValueForKey(value, "mcomComptabilisable");
	}
	public Integer mcomFinal() {
		return (Integer) storedValueForKey("mcomFinal");
	}

	public void setMcomFinal(Integer value) {
		takeStoredValueForKey(value, "mcomFinal");
	}
	public String mcomFormule() {
		return (String) storedValueForKey("mcomFormule");
	}

	public void setMcomFormule(String value) {
		takeStoredValueForKey(value, "mcomFormule");
	}
	public Integer mcomImprimable() {
		return (Integer) storedValueForKey("mcomImprimable");
	}

	public void setMcomImprimable(Integer value) {
		takeStoredValueForKey(value, "mcomImprimable");
	}
	public Integer mcomNiveau() {
		return (Integer) storedValueForKey("mcomNiveau");
	}

	public void setMcomNiveau(Integer value) {
		takeStoredValueForKey(value, "mcomNiveau");
	}
	public java.math.BigDecimal mcomNoteBase() {
		return (java.math.BigDecimal) storedValueForKey("mcomNoteBase");
	}

	public void setMcomNoteBase(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mcomNoteBase");
	}
	public java.math.BigDecimal mcomNoteObtention() {
		return (java.math.BigDecimal) storedValueForKey("mcomNoteObtention");
	}

	public void setMcomNoteObtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mcomNoteObtention");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre)storedValueForKey("toFwkScolarite_ScolMaquetteSemestre");
	}

	public void setToFwkScolarite_ScolMaquetteSemestreRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre oldValue = toFwkScolarite_ScolMaquetteSemestre();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteSemestre");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteSemestre");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteCompensation.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteCompensation.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteCompensation)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteCompensation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteCompensation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteCompensation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteCompensation)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteCompensation fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteCompensation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteCompensation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteCompensation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteCompensation)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteCompensation fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteCompensation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteCompensation eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteCompensation ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteCompensation createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteCompensation.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteCompensation.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteCompensation object = (EOScolMaquetteCompensation) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteCompensation localInstanceOfObject(EOEditingContext ec, EOScolMaquetteCompensation object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteCompensation " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteCompensation) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
