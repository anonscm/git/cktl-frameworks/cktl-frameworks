/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolFormationTitre.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolFormationTitre extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolFormationTitre";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_FORMATION_TITRE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "ftitKey";

	public static final ERXKey<String> FGRA_CODE = new ERXKey<String>("fgraCode");
	public static final ERXKey<Integer> FHAB_NIVEAU = new ERXKey<Integer>("fhabNiveau");
	public static final ERXKey<String> FTIT_CODE = new ERXKey<String>("ftitCode");
	public static final ERXKey<String> FTIT_LIBELLE = new ERXKey<String>("ftitLibelle");
	public static final ERXKey<String> FTIT_TERME = new ERXKey<String>("ftitTerme");
	public static final ERXKey<String> FTIT_TYPE = new ERXKey<String>("ftitType");

	public static final String FGRA_CODE_KEY = "fgraCode";
	public static final String FHAB_NIVEAU_KEY = "fhabNiveau";
	public static final String FTIT_CODE_KEY = "ftitCode";
	public static final String FTIT_LIBELLE_KEY = "ftitLibelle";
	public static final String FTIT_TERME_KEY = "ftitTerme";
	public static final String FTIT_TYPE_KEY = "ftitType";

	// Non visible attributes
	public static final String FDIP_CODE_KEY = "fdipCode";
	public static final String FTIT_KEY_KEY = "ftitKey";

	// Colkeys
	public static final String FGRA_CODE_COLKEY = "FGRA_CODE";
	public static final String FHAB_NIVEAU_COLKEY = "FHAB_NIVEAU";
	public static final String FTIT_CODE_COLKEY = "FTIT_CODE";
	public static final String FTIT_LIBELLE_COLKEY = "FTIT_LIBELLE";
	public static final String FTIT_TERME_COLKEY = "FTIT_TERME";
	public static final String FTIT_TYPE_COLKEY = "FTIT_TYPE";

	// Non visible colkeys
	public static final String FDIP_CODE_COLKEY = "FDIP_CODE";
	public static final String FTIT_KEY_COLKEY = "FTIT_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome> TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome>("toFwkScolarite_ScolFormationDiplome");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere> TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere>("toFwkScolarite_ScolFormationFiliere");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade> TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade>("toFwkScolarite_ScolFormationGrade");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY = "toFwkScolarite_ScolFormationDiplome";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE_KEY = "toFwkScolarite_ScolFormationFiliere";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY = "toFwkScolarite_ScolFormationGrade";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolFormationTitre with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fgraCode
	 * @param fhabNiveau
	 * @param ftitCode
	 * @param ftitLibelle
	 * @param ftitType
	 * @param toFwkScolarite_ScolFormationDiplome
	 * @param toFwkScolarite_ScolFormationFiliere
	 * @param toFwkScolarite_ScolFormationGrade
	 * @return EOScolFormationTitre
	 */
	public static EOScolFormationTitre create(EOEditingContext editingContext, String fgraCode, Integer fhabNiveau, String ftitCode, String ftitLibelle, String ftitType, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toFwkScolarite_ScolFormationDiplome, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere toFwkScolarite_ScolFormationFiliere, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade toFwkScolarite_ScolFormationGrade) {
		EOScolFormationTitre eo = (EOScolFormationTitre) createAndInsertInstance(editingContext);
		eo.setFgraCode(fgraCode);
		eo.setFhabNiveau(fhabNiveau);
		eo.setFtitCode(ftitCode);
		eo.setFtitLibelle(ftitLibelle);
		eo.setFtitType(ftitType);
		eo.setToFwkScolarite_ScolFormationDiplomeRelationship(toFwkScolarite_ScolFormationDiplome);
		eo.setToFwkScolarite_ScolFormationFiliereRelationship(toFwkScolarite_ScolFormationFiliere);
		eo.setToFwkScolarite_ScolFormationGradeRelationship(toFwkScolarite_ScolFormationGrade);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolFormationTitre.
	 *
	 * @param editingContext
	 * @return EOScolFormationTitre
	 */
	public static EOScolFormationTitre create(EOEditingContext editingContext) {
		EOScolFormationTitre eo = (EOScolFormationTitre) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolFormationTitre localInstanceIn(EOEditingContext editingContext) {
		EOScolFormationTitre localInstance = (EOScolFormationTitre) localInstanceOfObject(editingContext, (EOScolFormationTitre) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolFormationTitre localInstanceIn(EOEditingContext editingContext, EOScolFormationTitre eo) {
		EOScolFormationTitre localInstance = (eo == null) ? null : (EOScolFormationTitre) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String fgraCode() {
		return (String) storedValueForKey("fgraCode");
	}

	public void setFgraCode(String value) {
		takeStoredValueForKey(value, "fgraCode");
	}
	public Integer fhabNiveau() {
		return (Integer) storedValueForKey("fhabNiveau");
	}

	public void setFhabNiveau(Integer value) {
		takeStoredValueForKey(value, "fhabNiveau");
	}
	public String ftitCode() {
		return (String) storedValueForKey("ftitCode");
	}

	public void setFtitCode(String value) {
		takeStoredValueForKey(value, "ftitCode");
	}
	public String ftitLibelle() {
		return (String) storedValueForKey("ftitLibelle");
	}

	public void setFtitLibelle(String value) {
		takeStoredValueForKey(value, "ftitLibelle");
	}
	public String ftitTerme() {
		return (String) storedValueForKey("ftitTerme");
	}

	public void setFtitTerme(String value) {
		takeStoredValueForKey(value, "ftitTerme");
	}
	public String ftitType() {
		return (String) storedValueForKey("ftitType");
	}

	public void setFtitType(String value) {
		takeStoredValueForKey(value, "ftitType");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toFwkScolarite_ScolFormationDiplome() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome)storedValueForKey("toFwkScolarite_ScolFormationDiplome");
	}

	public void setToFwkScolarite_ScolFormationDiplomeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome oldValue = toFwkScolarite_ScolFormationDiplome();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDiplome");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDiplome");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere toFwkScolarite_ScolFormationFiliere() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere)storedValueForKey("toFwkScolarite_ScolFormationFiliere");
	}

	public void setToFwkScolarite_ScolFormationFiliereRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere oldValue = toFwkScolarite_ScolFormationFiliere();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationFiliere");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationFiliere");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade toFwkScolarite_ScolFormationGrade() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade)storedValueForKey("toFwkScolarite_ScolFormationGrade");
	}

	public void setToFwkScolarite_ScolFormationGradeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade oldValue = toFwkScolarite_ScolFormationGrade();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationGrade");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationGrade");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolFormationTitre.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolFormationTitre.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolFormationTitre)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolFormationTitre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolFormationTitre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolFormationTitre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolFormationTitre)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolFormationTitre fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolFormationTitre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolFormationTitre)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationTitre fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationTitre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolFormationTitre eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolFormationTitre ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolFormationTitre createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolFormationTitre.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolFormationTitre.ENTITY_NAME + "' !");
		}
		else {
			EOScolFormationTitre object = (EOScolFormationTitre) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolFormationTitre localInstanceOfObject(EOEditingContext ec, EOScolFormationTitre object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolFormationTitre " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolFormationTitre) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
