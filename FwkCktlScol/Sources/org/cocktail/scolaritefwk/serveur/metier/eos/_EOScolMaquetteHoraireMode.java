/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteHoraireMode.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteHoraireMode extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteHoraireMode";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_HORAIRE_MODE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mhmoCode";

	public static final ERXKey<String> MHMO_ABREVIATION = new ERXKey<String>("mhmoAbreviation");
	public static final ERXKey<String> MHMO_LIBELLE = new ERXKey<String>("mhmoLibelle");
	public static final ERXKey<String> MHMO_PAYABLE = new ERXKey<String>("mhmoPayable");
	public static final ERXKey<Integer> MHMO_PRIORITE = new ERXKey<Integer>("mhmoPriorite");
	public static final ERXKey<String> MHMO_VALIDITE = new ERXKey<String>("mhmoValidite");

	public static final String MHMO_ABREVIATION_KEY = "mhmoAbreviation";
	public static final String MHMO_LIBELLE_KEY = "mhmoLibelle";
	public static final String MHMO_PAYABLE_KEY = "mhmoPayable";
	public static final String MHMO_PRIORITE_KEY = "mhmoPriorite";
	public static final String MHMO_VALIDITE_KEY = "mhmoValidite";

	// Non visible attributes
	public static final String MHMO_CODE_KEY = "mhmoCode";

	// Colkeys
	public static final String MHMO_ABREVIATION_COLKEY = "MHMO_ABREVIATION";
	public static final String MHMO_LIBELLE_COLKEY = "MHMO_LIBELLE";
	public static final String MHMO_PAYABLE_COLKEY = "MHMO_PAYABLE";
	public static final String MHMO_PRIORITE_COLKEY = "MHMO_PRIORITE";
	public static final String MHMO_VALIDITE_COLKEY = "MHMO_VALIDITE";

	// Non visible colkeys
	public static final String MHMO_CODE_COLKEY = "MHMO_CODE";

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteHoraireMode with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mhmoLibelle
	 * @param mhmoPayable
	 * @param mhmoPriorite
	 * @param mhmoValidite
	 * @return EOScolMaquetteHoraireMode
	 */
	public static EOScolMaquetteHoraireMode create(EOEditingContext editingContext, String mhmoLibelle, String mhmoPayable, Integer mhmoPriorite, String mhmoValidite) {
		EOScolMaquetteHoraireMode eo = (EOScolMaquetteHoraireMode) createAndInsertInstance(editingContext);
		eo.setMhmoLibelle(mhmoLibelle);
		eo.setMhmoPayable(mhmoPayable);
		eo.setMhmoPriorite(mhmoPriorite);
		eo.setMhmoValidite(mhmoValidite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteHoraireMode.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteHoraireMode
	 */
	public static EOScolMaquetteHoraireMode create(EOEditingContext editingContext) {
		EOScolMaquetteHoraireMode eo = (EOScolMaquetteHoraireMode) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteHoraireMode localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteHoraireMode localInstance = (EOScolMaquetteHoraireMode) localInstanceOfObject(editingContext, (EOScolMaquetteHoraireMode) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteHoraireMode localInstanceIn(EOEditingContext editingContext, EOScolMaquetteHoraireMode eo) {
		EOScolMaquetteHoraireMode localInstance = (eo == null) ? null : (EOScolMaquetteHoraireMode) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String mhmoAbreviation() {
		return (String) storedValueForKey("mhmoAbreviation");
	}

	public void setMhmoAbreviation(String value) {
		takeStoredValueForKey(value, "mhmoAbreviation");
	}
	public String mhmoLibelle() {
		return (String) storedValueForKey("mhmoLibelle");
	}

	public void setMhmoLibelle(String value) {
		takeStoredValueForKey(value, "mhmoLibelle");
	}
	public String mhmoPayable() {
		return (String) storedValueForKey("mhmoPayable");
	}

	public void setMhmoPayable(String value) {
		takeStoredValueForKey(value, "mhmoPayable");
	}
	public Integer mhmoPriorite() {
		return (Integer) storedValueForKey("mhmoPriorite");
	}

	public void setMhmoPriorite(Integer value) {
		takeStoredValueForKey(value, "mhmoPriorite");
	}
	public String mhmoValidite() {
		return (String) storedValueForKey("mhmoValidite");
	}

	public void setMhmoValidite(String value) {
		takeStoredValueForKey(value, "mhmoValidite");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteHoraireMode.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteHoraireMode.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteHoraireMode)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteHoraireMode fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteHoraireMode fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteHoraireMode eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteHoraireMode)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteHoraireMode fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteHoraireMode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteHoraireMode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteHoraireMode eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteHoraireMode)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteHoraireMode fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteHoraireMode fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteHoraireMode eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteHoraireMode ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteHoraireMode createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteHoraireMode.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteHoraireMode.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteHoraireMode object = (EOScolMaquetteHoraireMode) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteHoraireMode localInstanceOfObject(EOEditingContext ec, EOScolMaquetteHoraireMode object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteHoraireMode " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteHoraireMode) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
