/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolFormationSupplement.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolFormationSupplement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolFormationSupplement";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_FORMATION_SUPPLEMENT";

	//Attributes

	public static final ERXKey<Integer> FSUP21 = new ERXKey<Integer>("fsup21");
	public static final ERXKey<Integer> FSUP22 = new ERXKey<Integer>("fsup22");
	public static final ERXKey<Integer> FSUP23 = new ERXKey<Integer>("fsup23");
	public static final ERXKey<Integer> FSUP24 = new ERXKey<Integer>("fsup24");
	public static final ERXKey<Integer> FSUP25 = new ERXKey<Integer>("fsup25");
	public static final ERXKey<Integer> FSUP31 = new ERXKey<Integer>("fsup31");
	public static final ERXKey<Integer> FSUP32 = new ERXKey<Integer>("fsup32");
	public static final ERXKey<Integer> FSUP33 = new ERXKey<Integer>("fsup33");
	public static final ERXKey<Integer> FSUP41 = new ERXKey<Integer>("fsup41");
	public static final ERXKey<Integer> FSUP42 = new ERXKey<Integer>("fsup42");
	public static final ERXKey<Integer> FSUP43 = new ERXKey<Integer>("fsup43");
	public static final ERXKey<Integer> FSUP44 = new ERXKey<Integer>("fsup44");
	public static final ERXKey<Integer> FSUP44_A1 = new ERXKey<Integer>("fsup44A1");
	public static final ERXKey<Integer> FSUP44_A2 = new ERXKey<Integer>("fsup44A2");
	public static final ERXKey<Integer> FSUP44_B1 = new ERXKey<Integer>("fsup44B1");
	public static final ERXKey<Integer> FSUP44_B2 = new ERXKey<Integer>("fsup44B2");
	public static final ERXKey<Integer> FSUP44_C1 = new ERXKey<Integer>("fsup44C1");
	public static final ERXKey<Integer> FSUP44_C2 = new ERXKey<Integer>("fsup44C2");
	public static final ERXKey<Integer> FSUP44_D1 = new ERXKey<Integer>("fsup44D1");
	public static final ERXKey<Integer> FSUP44_D2 = new ERXKey<Integer>("fsup44D2");
	public static final ERXKey<Integer> FSUP44_E1 = new ERXKey<Integer>("fsup44E1");
	public static final ERXKey<Integer> FSUP44_E2 = new ERXKey<Integer>("fsup44E2");
	public static final ERXKey<Integer> FSUP45 = new ERXKey<Integer>("fsup45");
	public static final ERXKey<Integer> FSUP51 = new ERXKey<Integer>("fsup51");
	public static final ERXKey<Integer> FSUP52 = new ERXKey<Integer>("fsup52");
	public static final ERXKey<Integer> FSUP61 = new ERXKey<Integer>("fsup61");
	public static final ERXKey<Integer> FSUP62 = new ERXKey<Integer>("fsup62");
	public static final ERXKey<Integer> FSUP81 = new ERXKey<Integer>("fsup81");

	public static final String FSUP21_KEY = "fsup21";
	public static final String FSUP22_KEY = "fsup22";
	public static final String FSUP23_KEY = "fsup23";
	public static final String FSUP24_KEY = "fsup24";
	public static final String FSUP25_KEY = "fsup25";
	public static final String FSUP31_KEY = "fsup31";
	public static final String FSUP32_KEY = "fsup32";
	public static final String FSUP33_KEY = "fsup33";
	public static final String FSUP41_KEY = "fsup41";
	public static final String FSUP42_KEY = "fsup42";
	public static final String FSUP43_KEY = "fsup43";
	public static final String FSUP44_KEY = "fsup44";
	public static final String FSUP44_A1_KEY = "fsup44A1";
	public static final String FSUP44_A2_KEY = "fsup44A2";
	public static final String FSUP44_B1_KEY = "fsup44B1";
	public static final String FSUP44_B2_KEY = "fsup44B2";
	public static final String FSUP44_C1_KEY = "fsup44C1";
	public static final String FSUP44_C2_KEY = "fsup44C2";
	public static final String FSUP44_D1_KEY = "fsup44D1";
	public static final String FSUP44_D2_KEY = "fsup44D2";
	public static final String FSUP44_E1_KEY = "fsup44E1";
	public static final String FSUP44_E2_KEY = "fsup44E2";
	public static final String FSUP45_KEY = "fsup45";
	public static final String FSUP51_KEY = "fsup51";
	public static final String FSUP52_KEY = "fsup52";
	public static final String FSUP61_KEY = "fsup61";
	public static final String FSUP62_KEY = "fsup62";
	public static final String FSUP81_KEY = "fsup81";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String MPAR_KEY_KEY = "mparKey";

	// Colkeys
	public static final String FSUP21_COLKEY = "FSUP_2_1";
	public static final String FSUP22_COLKEY = "FSUP_2_2";
	public static final String FSUP23_COLKEY = "FSUP_2_3";
	public static final String FSUP24_COLKEY = "FSUP_2_4";
	public static final String FSUP25_COLKEY = "FSUP_2_5";
	public static final String FSUP31_COLKEY = "FSUP_3_1";
	public static final String FSUP32_COLKEY = "FSUP_3_2";
	public static final String FSUP33_COLKEY = "FSUP_3_3";
	public static final String FSUP41_COLKEY = "FSUP_4_1";
	public static final String FSUP42_COLKEY = "FSUP_4_2";
	public static final String FSUP43_COLKEY = "FSUP_4_3";
	public static final String FSUP44_COLKEY = "FSUP_4_4";
	public static final String FSUP44_A1_COLKEY = "FSUP_4_4_A1";
	public static final String FSUP44_A2_COLKEY = "FSUP_4_4_A2";
	public static final String FSUP44_B1_COLKEY = "FSUP_4_4_B1";
	public static final String FSUP44_B2_COLKEY = "FSUP_4_4_B2";
	public static final String FSUP44_C1_COLKEY = "FSUP_4_4_C1";
	public static final String FSUP44_C2_COLKEY = "FSUP_4_4_C2";
	public static final String FSUP44_D1_COLKEY = "FSUP_4_4_D1";
	public static final String FSUP44_D2_COLKEY = "FSUP_4_4_D2";
	public static final String FSUP44_E1_COLKEY = "FSUP_4_4_E1";
	public static final String FSUP44_E2_COLKEY = "FSUP_4_4_E2";
	public static final String FSUP45_COLKEY = "FSUP_4_5";
	public static final String FSUP51_COLKEY = "FSUP_5_1";
	public static final String FSUP52_COLKEY = "FSUP_5_2";
	public static final String FSUP61_COLKEY = "FSUP_6_1";
	public static final String FSUP62_COLKEY = "FSUP_6_2";
	public static final String FSUP81_COLKEY = "FSUP_8_1";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String MPAR_KEY_COLKEY = "MPAR_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours> TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours>("toFwkScolarite_ScolMaquetteParcours");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY = "toFwkScolarite_ScolMaquetteParcours";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolFormationSupplement with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fsup21
	 * @param fsup22
	 * @param fsup23
	 * @param fsup24
	 * @param fsup25
	 * @param fsup31
	 * @param fsup32
	 * @param fsup33
	 * @param fsup41
	 * @param fsup42
	 * @param fsup43
	 * @param fsup44
	 * @param fsup44A1
	 * @param fsup44A2
	 * @param fsup44B1
	 * @param fsup44B2
	 * @param fsup44C1
	 * @param fsup44C2
	 * @param fsup44D1
	 * @param fsup44D2
	 * @param fsup44E1
	 * @param fsup44E2
	 * @param fsup45
	 * @param fsup51
	 * @param fsup52
	 * @param fsup61
	 * @param fsup62
	 * @param fsup81
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @param toFwkScolarite_ScolMaquetteParcours
	 * @return EOScolFormationSupplement
	 */
	public static EOScolFormationSupplement create(EOEditingContext editingContext, Integer fsup21, Integer fsup22, Integer fsup23, Integer fsup24, Integer fsup25, Integer fsup31, Integer fsup32, Integer fsup33, Integer fsup41, Integer fsup42, Integer fsup43, Integer fsup44, Integer fsup44A1, Integer fsup44A2, Integer fsup44B1, Integer fsup44B2, Integer fsup44C1, Integer fsup44C2, Integer fsup44D1, Integer fsup44D2, Integer fsup44E1, Integer fsup44E2, Integer fsup45, Integer fsup51, Integer fsup52, Integer fsup61, Integer fsup62, Integer fsup81, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours) {
		EOScolFormationSupplement eo = (EOScolFormationSupplement) createAndInsertInstance(editingContext);
		eo.setFsup21(fsup21);
		eo.setFsup22(fsup22);
		eo.setFsup23(fsup23);
		eo.setFsup24(fsup24);
		eo.setFsup25(fsup25);
		eo.setFsup31(fsup31);
		eo.setFsup32(fsup32);
		eo.setFsup33(fsup33);
		eo.setFsup41(fsup41);
		eo.setFsup42(fsup42);
		eo.setFsup43(fsup43);
		eo.setFsup44(fsup44);
		eo.setFsup44A1(fsup44A1);
		eo.setFsup44A2(fsup44A2);
		eo.setFsup44B1(fsup44B1);
		eo.setFsup44B2(fsup44B2);
		eo.setFsup44C1(fsup44C1);
		eo.setFsup44C2(fsup44C2);
		eo.setFsup44D1(fsup44D1);
		eo.setFsup44D2(fsup44D2);
		eo.setFsup44E1(fsup44E1);
		eo.setFsup44E2(fsup44E2);
		eo.setFsup45(fsup45);
		eo.setFsup51(fsup51);
		eo.setFsup52(fsup52);
		eo.setFsup61(fsup61);
		eo.setFsup62(fsup62);
		eo.setFsup81(fsup81);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		eo.setToFwkScolarite_ScolMaquetteParcoursRelationship(toFwkScolarite_ScolMaquetteParcours);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolFormationSupplement.
	 *
	 * @param editingContext
	 * @return EOScolFormationSupplement
	 */
	public static EOScolFormationSupplement create(EOEditingContext editingContext) {
		EOScolFormationSupplement eo = (EOScolFormationSupplement) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolFormationSupplement localInstanceIn(EOEditingContext editingContext) {
		EOScolFormationSupplement localInstance = (EOScolFormationSupplement) localInstanceOfObject(editingContext, (EOScolFormationSupplement) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolFormationSupplement localInstanceIn(EOEditingContext editingContext, EOScolFormationSupplement eo) {
		EOScolFormationSupplement localInstance = (eo == null) ? null : (EOScolFormationSupplement) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer fsup21() {
		return (Integer) storedValueForKey("fsup21");
	}

	public void setFsup21(Integer value) {
		takeStoredValueForKey(value, "fsup21");
	}
	public Integer fsup22() {
		return (Integer) storedValueForKey("fsup22");
	}

	public void setFsup22(Integer value) {
		takeStoredValueForKey(value, "fsup22");
	}
	public Integer fsup23() {
		return (Integer) storedValueForKey("fsup23");
	}

	public void setFsup23(Integer value) {
		takeStoredValueForKey(value, "fsup23");
	}
	public Integer fsup24() {
		return (Integer) storedValueForKey("fsup24");
	}

	public void setFsup24(Integer value) {
		takeStoredValueForKey(value, "fsup24");
	}
	public Integer fsup25() {
		return (Integer) storedValueForKey("fsup25");
	}

	public void setFsup25(Integer value) {
		takeStoredValueForKey(value, "fsup25");
	}
	public Integer fsup31() {
		return (Integer) storedValueForKey("fsup31");
	}

	public void setFsup31(Integer value) {
		takeStoredValueForKey(value, "fsup31");
	}
	public Integer fsup32() {
		return (Integer) storedValueForKey("fsup32");
	}

	public void setFsup32(Integer value) {
		takeStoredValueForKey(value, "fsup32");
	}
	public Integer fsup33() {
		return (Integer) storedValueForKey("fsup33");
	}

	public void setFsup33(Integer value) {
		takeStoredValueForKey(value, "fsup33");
	}
	public Integer fsup41() {
		return (Integer) storedValueForKey("fsup41");
	}

	public void setFsup41(Integer value) {
		takeStoredValueForKey(value, "fsup41");
	}
	public Integer fsup42() {
		return (Integer) storedValueForKey("fsup42");
	}

	public void setFsup42(Integer value) {
		takeStoredValueForKey(value, "fsup42");
	}
	public Integer fsup43() {
		return (Integer) storedValueForKey("fsup43");
	}

	public void setFsup43(Integer value) {
		takeStoredValueForKey(value, "fsup43");
	}
	public Integer fsup44() {
		return (Integer) storedValueForKey("fsup44");
	}

	public void setFsup44(Integer value) {
		takeStoredValueForKey(value, "fsup44");
	}
	public Integer fsup44A1() {
		return (Integer) storedValueForKey("fsup44A1");
	}

	public void setFsup44A1(Integer value) {
		takeStoredValueForKey(value, "fsup44A1");
	}
	public Integer fsup44A2() {
		return (Integer) storedValueForKey("fsup44A2");
	}

	public void setFsup44A2(Integer value) {
		takeStoredValueForKey(value, "fsup44A2");
	}
	public Integer fsup44B1() {
		return (Integer) storedValueForKey("fsup44B1");
	}

	public void setFsup44B1(Integer value) {
		takeStoredValueForKey(value, "fsup44B1");
	}
	public Integer fsup44B2() {
		return (Integer) storedValueForKey("fsup44B2");
	}

	public void setFsup44B2(Integer value) {
		takeStoredValueForKey(value, "fsup44B2");
	}
	public Integer fsup44C1() {
		return (Integer) storedValueForKey("fsup44C1");
	}

	public void setFsup44C1(Integer value) {
		takeStoredValueForKey(value, "fsup44C1");
	}
	public Integer fsup44C2() {
		return (Integer) storedValueForKey("fsup44C2");
	}

	public void setFsup44C2(Integer value) {
		takeStoredValueForKey(value, "fsup44C2");
	}
	public Integer fsup44D1() {
		return (Integer) storedValueForKey("fsup44D1");
	}

	public void setFsup44D1(Integer value) {
		takeStoredValueForKey(value, "fsup44D1");
	}
	public Integer fsup44D2() {
		return (Integer) storedValueForKey("fsup44D2");
	}

	public void setFsup44D2(Integer value) {
		takeStoredValueForKey(value, "fsup44D2");
	}
	public Integer fsup44E1() {
		return (Integer) storedValueForKey("fsup44E1");
	}

	public void setFsup44E1(Integer value) {
		takeStoredValueForKey(value, "fsup44E1");
	}
	public Integer fsup44E2() {
		return (Integer) storedValueForKey("fsup44E2");
	}

	public void setFsup44E2(Integer value) {
		takeStoredValueForKey(value, "fsup44E2");
	}
	public Integer fsup45() {
		return (Integer) storedValueForKey("fsup45");
	}

	public void setFsup45(Integer value) {
		takeStoredValueForKey(value, "fsup45");
	}
	public Integer fsup51() {
		return (Integer) storedValueForKey("fsup51");
	}

	public void setFsup51(Integer value) {
		takeStoredValueForKey(value, "fsup51");
	}
	public Integer fsup52() {
		return (Integer) storedValueForKey("fsup52");
	}

	public void setFsup52(Integer value) {
		takeStoredValueForKey(value, "fsup52");
	}
	public Integer fsup61() {
		return (Integer) storedValueForKey("fsup61");
	}

	public void setFsup61(Integer value) {
		takeStoredValueForKey(value, "fsup61");
	}
	public Integer fsup62() {
		return (Integer) storedValueForKey("fsup62");
	}

	public void setFsup62(Integer value) {
		takeStoredValueForKey(value, "fsup62");
	}
	public Integer fsup81() {
		return (Integer) storedValueForKey("fsup81");
	}

	public void setFsup81(Integer value) {
		takeStoredValueForKey(value, "fsup81");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours)storedValueForKey("toFwkScolarite_ScolMaquetteParcours");
	}

	public void setToFwkScolarite_ScolMaquetteParcoursRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours oldValue = toFwkScolarite_ScolMaquetteParcours();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteParcours");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteParcours");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolFormationSupplement.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolFormationSupplement.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolFormationSupplement)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolFormationSupplement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolFormationSupplement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolFormationSupplement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolFormationSupplement)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolFormationSupplement fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationSupplement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationSupplement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolFormationSupplement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolFormationSupplement)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationSupplement fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationSupplement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolFormationSupplement eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolFormationSupplement ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolFormationSupplement createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolFormationSupplement.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolFormationSupplement.ENTITY_NAME + "' !");
		}
		else {
			EOScolFormationSupplement object = (EOScolFormationSupplement) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolFormationSupplement localInstanceOfObject(EOEditingContext ec, EOScolFormationSupplement object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolFormationSupplement " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolFormationSupplement) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
