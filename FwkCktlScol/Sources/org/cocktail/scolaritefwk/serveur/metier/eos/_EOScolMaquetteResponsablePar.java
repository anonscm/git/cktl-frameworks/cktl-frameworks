/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteResponsablePar.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteResponsablePar extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteResponsablePar";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_RESPONSABLE_PAR";

	//Attributes



	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MBPAR_TYPE_KEY = "mbparType";
	public static final String MPAR_KEY_KEY = "mparKey";
	public static final String PERS_ID_KEY = "persId";

	// Colkeys

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MBPAR_TYPE_COLKEY = "MBPAR_TYPE";
	public static final String MPAR_KEY_COLKEY = "MPAR_KEY";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_FWKPERS__INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toFwkpers_Individu");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteResponsabilite> TO_FWK_SCOLARITE__SCOL_CONSTANTE_RESPONSABILITE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteResponsabilite>("toFwkScolarite_ScolConstanteResponsabilite");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours> TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours>("toFwkScolarite_ScolMaquetteParcours");

	public static final String TO_FWKPERS__INDIVIDU_KEY = "toFwkpers_Individu";
	public static final String TO_FWK_SCOLARITE__SCOL_CONSTANTE_RESPONSABILITE_KEY = "toFwkScolarite_ScolConstanteResponsabilite";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY = "toFwkScolarite_ScolMaquetteParcours";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteResponsablePar with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param toFwkScolarite_ScolConstanteResponsabilite
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteParcours
	 * @return EOScolMaquetteResponsablePar
	 */
	public static EOScolMaquetteResponsablePar create(EOEditingContext editingContext, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteResponsabilite toFwkScolarite_ScolConstanteResponsabilite, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours) {
		EOScolMaquetteResponsablePar eo = (EOScolMaquetteResponsablePar) createAndInsertInstance(editingContext);
		eo.setToFwkScolarite_ScolConstanteResponsabiliteRelationship(toFwkScolarite_ScolConstanteResponsabilite);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteParcoursRelationship(toFwkScolarite_ScolMaquetteParcours);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteResponsablePar.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteResponsablePar
	 */
	public static EOScolMaquetteResponsablePar create(EOEditingContext editingContext) {
		EOScolMaquetteResponsablePar eo = (EOScolMaquetteResponsablePar) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteResponsablePar localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteResponsablePar localInstance = (EOScolMaquetteResponsablePar) localInstanceOfObject(editingContext, (EOScolMaquetteResponsablePar) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteResponsablePar localInstanceIn(EOEditingContext editingContext, EOScolMaquetteResponsablePar eo) {
		EOScolMaquetteResponsablePar localInstance = (eo == null) ? null : (EOScolMaquetteResponsablePar) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods


	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteResponsabilite toFwkScolarite_ScolConstanteResponsabilite() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteResponsabilite)storedValueForKey("toFwkScolarite_ScolConstanteResponsabilite");
	}

	public void setToFwkScolarite_ScolConstanteResponsabiliteRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteResponsabilite value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteResponsabilite oldValue = toFwkScolarite_ScolConstanteResponsabilite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolConstanteResponsabilite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolConstanteResponsabilite");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours)storedValueForKey("toFwkScolarite_ScolMaquetteParcours");
	}

	public void setToFwkScolarite_ScolMaquetteParcoursRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours oldValue = toFwkScolarite_ScolMaquetteParcours();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteParcours");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteParcours");
		}
	}
  
	public NSArray toFwkpers_Individu() {
		return (NSArray)storedValueForKey("toFwkpers_Individu");
	}

	public NSArray toFwkpers_Individu(EOQualifier qualifier) {
		return toFwkpers_Individu(qualifier, null);
	}

	public NSArray toFwkpers_Individu(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
				results = toFwkpers_Individu();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				return results;
	}
  
	public void addToToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkpers_Individu");
	}

	public void removeFromToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkpers_Individu");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createToFwkpers_IndividuRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkpers_Individu");
		return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
	}

	public void deleteToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkpers_Individu");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkpers_IndividuRelationships() {
		Enumeration objects = toFwkpers_Individu().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkpers_IndividuRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteResponsablePar.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteResponsablePar.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteResponsablePar)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteResponsablePar fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteResponsablePar fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteResponsablePar eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteResponsablePar)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteResponsablePar fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteResponsablePar fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteResponsablePar fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteResponsablePar eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteResponsablePar)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteResponsablePar fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteResponsablePar fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteResponsablePar eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteResponsablePar ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteResponsablePar createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteResponsablePar.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteResponsablePar.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteResponsablePar object = (EOScolMaquetteResponsablePar) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteResponsablePar localInstanceOfObject(EOEditingContext ec, EOScolMaquetteResponsablePar object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteResponsablePar " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteResponsablePar) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
