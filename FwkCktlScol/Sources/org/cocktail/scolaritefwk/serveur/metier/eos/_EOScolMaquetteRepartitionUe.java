/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteRepartitionUe.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteRepartitionUe extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteRepartitionUe";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_REPARTITION_UE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mrueKey";

	public static final ERXKey<Integer> MRUE_BONIFIABLE = new ERXKey<Integer>("mrueBonifiable");
	public static final ERXKey<java.math.BigDecimal> MRUE_COEFFICIENT = new ERXKey<java.math.BigDecimal>("mrueCoefficient");
	public static final ERXKey<Integer> MRUE_COMPTABILISABLE = new ERXKey<Integer>("mrueComptabilisable");
	public static final ERXKey<java.math.BigDecimal> MRUE_NOTE_BASE = new ERXKey<java.math.BigDecimal>("mrueNoteBase");
	public static final ERXKey<java.math.BigDecimal> MRUE_NOTE_ELIMINATION = new ERXKey<java.math.BigDecimal>("mrueNoteElimination");
	public static final ERXKey<java.math.BigDecimal> MRUE_NOTE_OBTENTION = new ERXKey<java.math.BigDecimal>("mrueNoteObtention");
	public static final ERXKey<Integer> MRUE_ORDRE = new ERXKey<Integer>("mrueOrdre");

	public static final String MRUE_BONIFIABLE_KEY = "mrueBonifiable";
	public static final String MRUE_COEFFICIENT_KEY = "mrueCoefficient";
	public static final String MRUE_COMPTABILISABLE_KEY = "mrueComptabilisable";
	public static final String MRUE_NOTE_BASE_KEY = "mrueNoteBase";
	public static final String MRUE_NOTE_ELIMINATION_KEY = "mrueNoteElimination";
	public static final String MRUE_NOTE_OBTENTION_KEY = "mrueNoteObtention";
	public static final String MRUE_ORDRE_KEY = "mrueOrdre";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MRUE_KEY_KEY = "mrueKey";
	public static final String MSEM_KEY_KEY = "msemKey";
	public static final String MUE_KEY_KEY = "mueKey";

	// Colkeys
	public static final String MRUE_BONIFIABLE_COLKEY = "MRUE_BONIFIABLE";
	public static final String MRUE_COEFFICIENT_COLKEY = "MRUE_COEFFICIENT";
	public static final String MRUE_COMPTABILISABLE_COLKEY = "MRUE_COMPTABILISABLE";
	public static final String MRUE_NOTE_BASE_COLKEY = "MRUE_NOTE_BASE";
	public static final String MRUE_NOTE_ELIMINATION_COLKEY = "MRUE_NOTE_ELIMINATION";
	public static final String MRUE_NOTE_OBTENTION_COLKEY = "MRUE_NOTE_OBTENTION";
	public static final String MRUE_ORDRE_COLKEY = "MRUE_ORDRE";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MRUE_KEY_COLKEY = "MRUE_KEY";
	public static final String MSEM_KEY_COLKEY = "MSEM_KEY";
	public static final String MUE_KEY_COLKEY = "MUE_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre> TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre>("toFwkScolarite_ScolMaquetteSemestre");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe> TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe>("toFwkScolarite_ScolMaquetteUe");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY = "toFwkScolarite_ScolMaquetteSemestre";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY = "toFwkScolarite_ScolMaquetteUe";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteRepartitionUe with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mrueBonifiable
	 * @param mrueCoefficient
	 * @param mrueComptabilisable
	 * @param mrueNoteBase
	 * @param mrueNoteObtention
	 * @param mrueOrdre
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteSemestre
	 * @param toFwkScolarite_ScolMaquetteUe
	 * @return EOScolMaquetteRepartitionUe
	 */
	public static EOScolMaquetteRepartitionUe create(EOEditingContext editingContext, Integer mrueBonifiable, java.math.BigDecimal mrueCoefficient, Integer mrueComptabilisable, java.math.BigDecimal mrueNoteBase, java.math.BigDecimal mrueNoteObtention, Integer mrueOrdre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe toFwkScolarite_ScolMaquetteUe) {
		EOScolMaquetteRepartitionUe eo = (EOScolMaquetteRepartitionUe) createAndInsertInstance(editingContext);
		eo.setMrueBonifiable(mrueBonifiable);
		eo.setMrueCoefficient(mrueCoefficient);
		eo.setMrueComptabilisable(mrueComptabilisable);
		eo.setMrueNoteBase(mrueNoteBase);
		eo.setMrueNoteObtention(mrueNoteObtention);
		eo.setMrueOrdre(mrueOrdre);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteSemestreRelationship(toFwkScolarite_ScolMaquetteSemestre);
		eo.setToFwkScolarite_ScolMaquetteUeRelationship(toFwkScolarite_ScolMaquetteUe);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteRepartitionUe.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteRepartitionUe
	 */
	public static EOScolMaquetteRepartitionUe create(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionUe eo = (EOScolMaquetteRepartitionUe) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteRepartitionUe localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionUe localInstance = (EOScolMaquetteRepartitionUe) localInstanceOfObject(editingContext, (EOScolMaquetteRepartitionUe) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteRepartitionUe localInstanceIn(EOEditingContext editingContext, EOScolMaquetteRepartitionUe eo) {
		EOScolMaquetteRepartitionUe localInstance = (eo == null) ? null : (EOScolMaquetteRepartitionUe) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mrueBonifiable() {
		return (Integer) storedValueForKey("mrueBonifiable");
	}

	public void setMrueBonifiable(Integer value) {
		takeStoredValueForKey(value, "mrueBonifiable");
	}
	public java.math.BigDecimal mrueCoefficient() {
		return (java.math.BigDecimal) storedValueForKey("mrueCoefficient");
	}

	public void setMrueCoefficient(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrueCoefficient");
	}
	public Integer mrueComptabilisable() {
		return (Integer) storedValueForKey("mrueComptabilisable");
	}

	public void setMrueComptabilisable(Integer value) {
		takeStoredValueForKey(value, "mrueComptabilisable");
	}
	public java.math.BigDecimal mrueNoteBase() {
		return (java.math.BigDecimal) storedValueForKey("mrueNoteBase");
	}

	public void setMrueNoteBase(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrueNoteBase");
	}
	public java.math.BigDecimal mrueNoteElimination() {
		return (java.math.BigDecimal) storedValueForKey("mrueNoteElimination");
	}

	public void setMrueNoteElimination(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrueNoteElimination");
	}
	public java.math.BigDecimal mrueNoteObtention() {
		return (java.math.BigDecimal) storedValueForKey("mrueNoteObtention");
	}

	public void setMrueNoteObtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrueNoteObtention");
	}
	public Integer mrueOrdre() {
		return (Integer) storedValueForKey("mrueOrdre");
	}

	public void setMrueOrdre(Integer value) {
		takeStoredValueForKey(value, "mrueOrdre");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre)storedValueForKey("toFwkScolarite_ScolMaquetteSemestre");
	}

	public void setToFwkScolarite_ScolMaquetteSemestreRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre oldValue = toFwkScolarite_ScolMaquetteSemestre();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteSemestre");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteSemestre");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe toFwkScolarite_ScolMaquetteUe() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe)storedValueForKey("toFwkScolarite_ScolMaquetteUe");
	}

	public void setToFwkScolarite_ScolMaquetteUeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe oldValue = toFwkScolarite_ScolMaquetteUe();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteUe");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteUe");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteRepartitionUe.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteRepartitionUe.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteRepartitionUe)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteRepartitionUe fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteRepartitionUe fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteRepartitionUe eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteRepartitionUe)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionUe fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionUe fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionUe fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteRepartitionUe eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteRepartitionUe)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionUe fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionUe fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteRepartitionUe eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteRepartitionUe ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteRepartitionUe createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteRepartitionUe.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteRepartitionUe.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteRepartitionUe object = (EOScolMaquetteRepartitionUe) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteRepartitionUe localInstanceOfObject(EOEditingContext ec, EOScolMaquetteRepartitionUe object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteRepartitionUe " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteRepartitionUe) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
