/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolConstanteSupplement.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolConstanteSupplement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolConstanteSupplement";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_CONSTANTE_SUPPLEMENT";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "fannKey";

	public static final ERXKey<Integer> CSUP11_TITRE = new ERXKey<Integer>("csup11Titre");
	public static final ERXKey<Integer> CSUP12_TITRE = new ERXKey<Integer>("csup12Titre");
	public static final ERXKey<Integer> CSUP13_TITRE = new ERXKey<Integer>("csup13Titre");
	public static final ERXKey<Integer> CSUP14_TITRE = new ERXKey<Integer>("csup14Titre");
	public static final ERXKey<Integer> CSUP1_TITRE = new ERXKey<Integer>("csup1Titre");
	public static final ERXKey<Integer> CSUP21_TITRE = new ERXKey<Integer>("csup21Titre");
	public static final ERXKey<Integer> CSUP22_TITRE = new ERXKey<Integer>("csup22Titre");
	public static final ERXKey<Integer> CSUP23_TITRE = new ERXKey<Integer>("csup23Titre");
	public static final ERXKey<Integer> CSUP24_TITRE = new ERXKey<Integer>("csup24Titre");
	public static final ERXKey<Integer> CSUP25_TITRE = new ERXKey<Integer>("csup25Titre");
	public static final ERXKey<Integer> CSUP2_TITRE = new ERXKey<Integer>("csup2Titre");
	public static final ERXKey<Integer> CSUP31_TITRE = new ERXKey<Integer>("csup31Titre");
	public static final ERXKey<Integer> CSUP32_TITRE = new ERXKey<Integer>("csup32Titre");
	public static final ERXKey<Integer> CSUP33_TITRE = new ERXKey<Integer>("csup33Titre");
	public static final ERXKey<Integer> CSUP3_TITRE = new ERXKey<Integer>("csup3Titre");
	public static final ERXKey<Integer> CSUP41_TITRE = new ERXKey<Integer>("csup41Titre");
	public static final ERXKey<Integer> CSUP42_TITRE = new ERXKey<Integer>("csup42Titre");
	public static final ERXKey<Integer> CSUP43_TITRE = new ERXKey<Integer>("csup43Titre");
	public static final ERXKey<Integer> CSUP440_TITRE = new ERXKey<Integer>("csup440Titre");
	public static final ERXKey<Integer> CSUP441_TITRE = new ERXKey<Integer>("csup441Titre");
	public static final ERXKey<Integer> CSUP442_TITRE = new ERXKey<Integer>("csup442Titre");
	public static final ERXKey<Integer> CSUP44_TITRE = new ERXKey<Integer>("csup44Titre");
	public static final ERXKey<Integer> CSUP45_TITRE = new ERXKey<Integer>("csup45Titre");
	public static final ERXKey<Integer> CSUP4_TITRE = new ERXKey<Integer>("csup4Titre");
	public static final ERXKey<Integer> CSUP51_TITRE = new ERXKey<Integer>("csup51Titre");
	public static final ERXKey<Integer> CSUP52_TITRE = new ERXKey<Integer>("csup52Titre");
	public static final ERXKey<Integer> CSUP5_TITRE = new ERXKey<Integer>("csup5Titre");
	public static final ERXKey<Integer> CSUP61_TITRE = new ERXKey<Integer>("csup61Titre");
	public static final ERXKey<Integer> CSUP62_TITRE = new ERXKey<Integer>("csup62Titre");
	public static final ERXKey<Integer> CSUP6_TITRE = new ERXKey<Integer>("csup6Titre");
	public static final ERXKey<Integer> CSUP71_TITRE = new ERXKey<Integer>("csup71Titre");
	public static final ERXKey<Integer> CSUP72_TITRE = new ERXKey<Integer>("csup72Titre");
	public static final ERXKey<Integer> CSUP73_TITRE = new ERXKey<Integer>("csup73Titre");
	public static final ERXKey<Integer> CSUP74_TITRE = new ERXKey<Integer>("csup74Titre");
	public static final ERXKey<Integer> CSUP7_TITRE = new ERXKey<Integer>("csup7Titre");
	public static final ERXKey<Integer> CSUP8_TITRE = new ERXKey<Integer>("csup8Titre");
	public static final ERXKey<Integer> CSUP_LIB_OPTION2 = new ERXKey<Integer>("csupLibOption2");
	public static final ERXKey<Integer> CSUP_LIB_OPTION3 = new ERXKey<Integer>("csupLibOption3");
	public static final ERXKey<Integer> CSUP_LIB_OPTION4 = new ERXKey<Integer>("csupLibOption4");
	public static final ERXKey<Integer> CSUP_OPTION1 = new ERXKey<Integer>("csupOption1");
	public static final ERXKey<Integer> CSUP_OPTION2 = new ERXKey<Integer>("csupOption2");
	public static final ERXKey<Integer> CSUP_OPTION3 = new ERXKey<Integer>("csupOption3");
	public static final ERXKey<Integer> CSUP_OPTION4 = new ERXKey<Integer>("csupOption4");
	public static final ERXKey<Integer> CSUP_SOUS_TITRE = new ERXKey<Integer>("csupSousTitre");
	public static final ERXKey<Integer> CSUP_TEXTE = new ERXKey<Integer>("csupTexte");
	public static final ERXKey<Integer> CSUP_TITRE = new ERXKey<Integer>("csupTitre");

	public static final String CSUP11_TITRE_KEY = "csup11Titre";
	public static final String CSUP12_TITRE_KEY = "csup12Titre";
	public static final String CSUP13_TITRE_KEY = "csup13Titre";
	public static final String CSUP14_TITRE_KEY = "csup14Titre";
	public static final String CSUP1_TITRE_KEY = "csup1Titre";
	public static final String CSUP21_TITRE_KEY = "csup21Titre";
	public static final String CSUP22_TITRE_KEY = "csup22Titre";
	public static final String CSUP23_TITRE_KEY = "csup23Titre";
	public static final String CSUP24_TITRE_KEY = "csup24Titre";
	public static final String CSUP25_TITRE_KEY = "csup25Titre";
	public static final String CSUP2_TITRE_KEY = "csup2Titre";
	public static final String CSUP31_TITRE_KEY = "csup31Titre";
	public static final String CSUP32_TITRE_KEY = "csup32Titre";
	public static final String CSUP33_TITRE_KEY = "csup33Titre";
	public static final String CSUP3_TITRE_KEY = "csup3Titre";
	public static final String CSUP41_TITRE_KEY = "csup41Titre";
	public static final String CSUP42_TITRE_KEY = "csup42Titre";
	public static final String CSUP43_TITRE_KEY = "csup43Titre";
	public static final String CSUP440_TITRE_KEY = "csup440Titre";
	public static final String CSUP441_TITRE_KEY = "csup441Titre";
	public static final String CSUP442_TITRE_KEY = "csup442Titre";
	public static final String CSUP44_TITRE_KEY = "csup44Titre";
	public static final String CSUP45_TITRE_KEY = "csup45Titre";
	public static final String CSUP4_TITRE_KEY = "csup4Titre";
	public static final String CSUP51_TITRE_KEY = "csup51Titre";
	public static final String CSUP52_TITRE_KEY = "csup52Titre";
	public static final String CSUP5_TITRE_KEY = "csup5Titre";
	public static final String CSUP61_TITRE_KEY = "csup61Titre";
	public static final String CSUP62_TITRE_KEY = "csup62Titre";
	public static final String CSUP6_TITRE_KEY = "csup6Titre";
	public static final String CSUP71_TITRE_KEY = "csup71Titre";
	public static final String CSUP72_TITRE_KEY = "csup72Titre";
	public static final String CSUP73_TITRE_KEY = "csup73Titre";
	public static final String CSUP74_TITRE_KEY = "csup74Titre";
	public static final String CSUP7_TITRE_KEY = "csup7Titre";
	public static final String CSUP8_TITRE_KEY = "csup8Titre";
	public static final String CSUP_LIB_OPTION2_KEY = "csupLibOption2";
	public static final String CSUP_LIB_OPTION3_KEY = "csupLibOption3";
	public static final String CSUP_LIB_OPTION4_KEY = "csupLibOption4";
	public static final String CSUP_OPTION1_KEY = "csupOption1";
	public static final String CSUP_OPTION2_KEY = "csupOption2";
	public static final String CSUP_OPTION3_KEY = "csupOption3";
	public static final String CSUP_OPTION4_KEY = "csupOption4";
	public static final String CSUP_SOUS_TITRE_KEY = "csupSousTitre";
	public static final String CSUP_TEXTE_KEY = "csupTexte";
	public static final String CSUP_TITRE_KEY = "csupTitre";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";

	// Colkeys
	public static final String CSUP11_TITRE_COLKEY = "CSUP_1_1_TITRE";
	public static final String CSUP12_TITRE_COLKEY = "CSUP_1_2_TITRE";
	public static final String CSUP13_TITRE_COLKEY = "CSUP_1_3_TITRE";
	public static final String CSUP14_TITRE_COLKEY = "CSUP_1_4_TITRE";
	public static final String CSUP1_TITRE_COLKEY = "CSUP_1_TITRE";
	public static final String CSUP21_TITRE_COLKEY = "CSUP_2_1_TITRE";
	public static final String CSUP22_TITRE_COLKEY = "CSUP_2_2_TITRE";
	public static final String CSUP23_TITRE_COLKEY = "CSUP_2_3_TITRE";
	public static final String CSUP24_TITRE_COLKEY = "CSUP_2_4_TITRE";
	public static final String CSUP25_TITRE_COLKEY = "CSUP_2_5_TITRE";
	public static final String CSUP2_TITRE_COLKEY = "CSUP_2_TITRE";
	public static final String CSUP31_TITRE_COLKEY = "CSUP_3_1_TITRE";
	public static final String CSUP32_TITRE_COLKEY = "CSUP_3_2_TITRE";
	public static final String CSUP33_TITRE_COLKEY = "CSUP_3_3_TITRE";
	public static final String CSUP3_TITRE_COLKEY = "CSUP_3_TITRE";
	public static final String CSUP41_TITRE_COLKEY = "CSUP_4_1_TITRE";
	public static final String CSUP42_TITRE_COLKEY = "CSUP_4_2_TITRE";
	public static final String CSUP43_TITRE_COLKEY = "CSUP_4_3_TITRE";
	public static final String CSUP440_TITRE_COLKEY = "CSUP_4_4_0_TITRE";
	public static final String CSUP441_TITRE_COLKEY = "CSUP_4_4_1_TITRE";
	public static final String CSUP442_TITRE_COLKEY = "CSUP_4_4_2_TITRE";
	public static final String CSUP44_TITRE_COLKEY = "CSUP_4_4_TITRE";
	public static final String CSUP45_TITRE_COLKEY = "CSUP_4_5_TITRE";
	public static final String CSUP4_TITRE_COLKEY = "CSUP_4_TITRE";
	public static final String CSUP51_TITRE_COLKEY = "CSUP_5_1_TITRE";
	public static final String CSUP52_TITRE_COLKEY = "CSUP_5_2_TITRE";
	public static final String CSUP5_TITRE_COLKEY = "CSUP_5_TITRE";
	public static final String CSUP61_TITRE_COLKEY = "CSUP_6_1_TITRE";
	public static final String CSUP62_TITRE_COLKEY = "CSUP_6_2_TITRE";
	public static final String CSUP6_TITRE_COLKEY = "CSUP_6_TITRE";
	public static final String CSUP71_TITRE_COLKEY = "CSUP_7_1_TITRE";
	public static final String CSUP72_TITRE_COLKEY = "CSUP_7_2_TITRE";
	public static final String CSUP73_TITRE_COLKEY = "CSUP_7_3_TITRE";
	public static final String CSUP74_TITRE_COLKEY = "CSUP_7_4_TITRE";
	public static final String CSUP7_TITRE_COLKEY = "CSUP_7_TITRE";
	public static final String CSUP8_TITRE_COLKEY = "CSUP_8_TITRE";
	public static final String CSUP_LIB_OPTION2_COLKEY = "CSUP_LIB_OPTION2";
	public static final String CSUP_LIB_OPTION3_COLKEY = "CSUP_LIB_OPTION3";
	public static final String CSUP_LIB_OPTION4_COLKEY = "CSUP_LIB_OPTION4";
	public static final String CSUP_OPTION1_COLKEY = "CSUP_OPTION1";
	public static final String CSUP_OPTION2_COLKEY = "CSUP_OPTION2";
	public static final String CSUP_OPTION3_COLKEY = "CSUP_OPTION3";
	public static final String CSUP_OPTION4_COLKEY = "CSUP_OPTION4";
	public static final String CSUP_SOUS_TITRE_COLKEY = "CSUP_SOUS_TITRE";
	public static final String CSUP_TEXTE_COLKEY = "CSUP_TEXTE";
	public static final String CSUP_TITRE_COLKEY = "CSUP_TITRE";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolConstanteSupplement with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param csup11Titre
	 * @param csup12Titre
	 * @param csup13Titre
	 * @param csup14Titre
	 * @param csup1Titre
	 * @param csup21Titre
	 * @param csup22Titre
	 * @param csup23Titre
	 * @param csup24Titre
	 * @param csup25Titre
	 * @param csup2Titre
	 * @param csup31Titre
	 * @param csup32Titre
	 * @param csup33Titre
	 * @param csup3Titre
	 * @param csup41Titre
	 * @param csup42Titre
	 * @param csup43Titre
	 * @param csup440Titre
	 * @param csup441Titre
	 * @param csup442Titre
	 * @param csup44Titre
	 * @param csup45Titre
	 * @param csup4Titre
	 * @param csup51Titre
	 * @param csup52Titre
	 * @param csup5Titre
	 * @param csup61Titre
	 * @param csup62Titre
	 * @param csup6Titre
	 * @param csup71Titre
	 * @param csup72Titre
	 * @param csup73Titre
	 * @param csup74Titre
	 * @param csup7Titre
	 * @param csup8Titre
	 * @param csupLibOption2
	 * @param csupLibOption3
	 * @param csupLibOption4
	 * @param csupOption1
	 * @param csupOption2
	 * @param csupOption3
	 * @param csupOption4
	 * @param csupSousTitre
	 * @param csupTexte
	 * @param csupTitre
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @return EOScolConstanteSupplement
	 */
	public static EOScolConstanteSupplement create(EOEditingContext editingContext, Integer csup11Titre, Integer csup12Titre, Integer csup13Titre, Integer csup14Titre, Integer csup1Titre, Integer csup21Titre, Integer csup22Titre, Integer csup23Titre, Integer csup24Titre, Integer csup25Titre, Integer csup2Titre, Integer csup31Titre, Integer csup32Titre, Integer csup33Titre, Integer csup3Titre, Integer csup41Titre, Integer csup42Titre, Integer csup43Titre, Integer csup440Titre, Integer csup441Titre, Integer csup442Titre, Integer csup44Titre, Integer csup45Titre, Integer csup4Titre, Integer csup51Titre, Integer csup52Titre, Integer csup5Titre, Integer csup61Titre, Integer csup62Titre, Integer csup6Titre, Integer csup71Titre, Integer csup72Titre, Integer csup73Titre, Integer csup74Titre, Integer csup7Titre, Integer csup8Titre, Integer csupLibOption2, Integer csupLibOption3, Integer csupLibOption4, Integer csupOption1, Integer csupOption2, Integer csupOption3, Integer csupOption4, Integer csupSousTitre, Integer csupTexte, Integer csupTitre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee) {
		EOScolConstanteSupplement eo = (EOScolConstanteSupplement) createAndInsertInstance(editingContext);
		eo.setCsup11Titre(csup11Titre);
		eo.setCsup12Titre(csup12Titre);
		eo.setCsup13Titre(csup13Titre);
		eo.setCsup14Titre(csup14Titre);
		eo.setCsup1Titre(csup1Titre);
		eo.setCsup21Titre(csup21Titre);
		eo.setCsup22Titre(csup22Titre);
		eo.setCsup23Titre(csup23Titre);
		eo.setCsup24Titre(csup24Titre);
		eo.setCsup25Titre(csup25Titre);
		eo.setCsup2Titre(csup2Titre);
		eo.setCsup31Titre(csup31Titre);
		eo.setCsup32Titre(csup32Titre);
		eo.setCsup33Titre(csup33Titre);
		eo.setCsup3Titre(csup3Titre);
		eo.setCsup41Titre(csup41Titre);
		eo.setCsup42Titre(csup42Titre);
		eo.setCsup43Titre(csup43Titre);
		eo.setCsup440Titre(csup440Titre);
		eo.setCsup441Titre(csup441Titre);
		eo.setCsup442Titre(csup442Titre);
		eo.setCsup44Titre(csup44Titre);
		eo.setCsup45Titre(csup45Titre);
		eo.setCsup4Titre(csup4Titre);
		eo.setCsup51Titre(csup51Titre);
		eo.setCsup52Titre(csup52Titre);
		eo.setCsup5Titre(csup5Titre);
		eo.setCsup61Titre(csup61Titre);
		eo.setCsup62Titre(csup62Titre);
		eo.setCsup6Titre(csup6Titre);
		eo.setCsup71Titre(csup71Titre);
		eo.setCsup72Titre(csup72Titre);
		eo.setCsup73Titre(csup73Titre);
		eo.setCsup74Titre(csup74Titre);
		eo.setCsup7Titre(csup7Titre);
		eo.setCsup8Titre(csup8Titre);
		eo.setCsupLibOption2(csupLibOption2);
		eo.setCsupLibOption3(csupLibOption3);
		eo.setCsupLibOption4(csupLibOption4);
		eo.setCsupOption1(csupOption1);
		eo.setCsupOption2(csupOption2);
		eo.setCsupOption3(csupOption3);
		eo.setCsupOption4(csupOption4);
		eo.setCsupSousTitre(csupSousTitre);
		eo.setCsupTexte(csupTexte);
		eo.setCsupTitre(csupTitre);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolConstanteSupplement.
	 *
	 * @param editingContext
	 * @return EOScolConstanteSupplement
	 */
	public static EOScolConstanteSupplement create(EOEditingContext editingContext) {
		EOScolConstanteSupplement eo = (EOScolConstanteSupplement) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolConstanteSupplement localInstanceIn(EOEditingContext editingContext) {
		EOScolConstanteSupplement localInstance = (EOScolConstanteSupplement) localInstanceOfObject(editingContext, (EOScolConstanteSupplement) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolConstanteSupplement localInstanceIn(EOEditingContext editingContext, EOScolConstanteSupplement eo) {
		EOScolConstanteSupplement localInstance = (eo == null) ? null : (EOScolConstanteSupplement) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer csup11Titre() {
		return (Integer) storedValueForKey("csup11Titre");
	}

	public void setCsup11Titre(Integer value) {
		takeStoredValueForKey(value, "csup11Titre");
	}
	public Integer csup12Titre() {
		return (Integer) storedValueForKey("csup12Titre");
	}

	public void setCsup12Titre(Integer value) {
		takeStoredValueForKey(value, "csup12Titre");
	}
	public Integer csup13Titre() {
		return (Integer) storedValueForKey("csup13Titre");
	}

	public void setCsup13Titre(Integer value) {
		takeStoredValueForKey(value, "csup13Titre");
	}
	public Integer csup14Titre() {
		return (Integer) storedValueForKey("csup14Titre");
	}

	public void setCsup14Titre(Integer value) {
		takeStoredValueForKey(value, "csup14Titre");
	}
	public Integer csup1Titre() {
		return (Integer) storedValueForKey("csup1Titre");
	}

	public void setCsup1Titre(Integer value) {
		takeStoredValueForKey(value, "csup1Titre");
	}
	public Integer csup21Titre() {
		return (Integer) storedValueForKey("csup21Titre");
	}

	public void setCsup21Titre(Integer value) {
		takeStoredValueForKey(value, "csup21Titre");
	}
	public Integer csup22Titre() {
		return (Integer) storedValueForKey("csup22Titre");
	}

	public void setCsup22Titre(Integer value) {
		takeStoredValueForKey(value, "csup22Titre");
	}
	public Integer csup23Titre() {
		return (Integer) storedValueForKey("csup23Titre");
	}

	public void setCsup23Titre(Integer value) {
		takeStoredValueForKey(value, "csup23Titre");
	}
	public Integer csup24Titre() {
		return (Integer) storedValueForKey("csup24Titre");
	}

	public void setCsup24Titre(Integer value) {
		takeStoredValueForKey(value, "csup24Titre");
	}
	public Integer csup25Titre() {
		return (Integer) storedValueForKey("csup25Titre");
	}

	public void setCsup25Titre(Integer value) {
		takeStoredValueForKey(value, "csup25Titre");
	}
	public Integer csup2Titre() {
		return (Integer) storedValueForKey("csup2Titre");
	}

	public void setCsup2Titre(Integer value) {
		takeStoredValueForKey(value, "csup2Titre");
	}
	public Integer csup31Titre() {
		return (Integer) storedValueForKey("csup31Titre");
	}

	public void setCsup31Titre(Integer value) {
		takeStoredValueForKey(value, "csup31Titre");
	}
	public Integer csup32Titre() {
		return (Integer) storedValueForKey("csup32Titre");
	}

	public void setCsup32Titre(Integer value) {
		takeStoredValueForKey(value, "csup32Titre");
	}
	public Integer csup33Titre() {
		return (Integer) storedValueForKey("csup33Titre");
	}

	public void setCsup33Titre(Integer value) {
		takeStoredValueForKey(value, "csup33Titre");
	}
	public Integer csup3Titre() {
		return (Integer) storedValueForKey("csup3Titre");
	}

	public void setCsup3Titre(Integer value) {
		takeStoredValueForKey(value, "csup3Titre");
	}
	public Integer csup41Titre() {
		return (Integer) storedValueForKey("csup41Titre");
	}

	public void setCsup41Titre(Integer value) {
		takeStoredValueForKey(value, "csup41Titre");
	}
	public Integer csup42Titre() {
		return (Integer) storedValueForKey("csup42Titre");
	}

	public void setCsup42Titre(Integer value) {
		takeStoredValueForKey(value, "csup42Titre");
	}
	public Integer csup43Titre() {
		return (Integer) storedValueForKey("csup43Titre");
	}

	public void setCsup43Titre(Integer value) {
		takeStoredValueForKey(value, "csup43Titre");
	}
	public Integer csup440Titre() {
		return (Integer) storedValueForKey("csup440Titre");
	}

	public void setCsup440Titre(Integer value) {
		takeStoredValueForKey(value, "csup440Titre");
	}
	public Integer csup441Titre() {
		return (Integer) storedValueForKey("csup441Titre");
	}

	public void setCsup441Titre(Integer value) {
		takeStoredValueForKey(value, "csup441Titre");
	}
	public Integer csup442Titre() {
		return (Integer) storedValueForKey("csup442Titre");
	}

	public void setCsup442Titre(Integer value) {
		takeStoredValueForKey(value, "csup442Titre");
	}
	public Integer csup44Titre() {
		return (Integer) storedValueForKey("csup44Titre");
	}

	public void setCsup44Titre(Integer value) {
		takeStoredValueForKey(value, "csup44Titre");
	}
	public Integer csup45Titre() {
		return (Integer) storedValueForKey("csup45Titre");
	}

	public void setCsup45Titre(Integer value) {
		takeStoredValueForKey(value, "csup45Titre");
	}
	public Integer csup4Titre() {
		return (Integer) storedValueForKey("csup4Titre");
	}

	public void setCsup4Titre(Integer value) {
		takeStoredValueForKey(value, "csup4Titre");
	}
	public Integer csup51Titre() {
		return (Integer) storedValueForKey("csup51Titre");
	}

	public void setCsup51Titre(Integer value) {
		takeStoredValueForKey(value, "csup51Titre");
	}
	public Integer csup52Titre() {
		return (Integer) storedValueForKey("csup52Titre");
	}

	public void setCsup52Titre(Integer value) {
		takeStoredValueForKey(value, "csup52Titre");
	}
	public Integer csup5Titre() {
		return (Integer) storedValueForKey("csup5Titre");
	}

	public void setCsup5Titre(Integer value) {
		takeStoredValueForKey(value, "csup5Titre");
	}
	public Integer csup61Titre() {
		return (Integer) storedValueForKey("csup61Titre");
	}

	public void setCsup61Titre(Integer value) {
		takeStoredValueForKey(value, "csup61Titre");
	}
	public Integer csup62Titre() {
		return (Integer) storedValueForKey("csup62Titre");
	}

	public void setCsup62Titre(Integer value) {
		takeStoredValueForKey(value, "csup62Titre");
	}
	public Integer csup6Titre() {
		return (Integer) storedValueForKey("csup6Titre");
	}

	public void setCsup6Titre(Integer value) {
		takeStoredValueForKey(value, "csup6Titre");
	}
	public Integer csup71Titre() {
		return (Integer) storedValueForKey("csup71Titre");
	}

	public void setCsup71Titre(Integer value) {
		takeStoredValueForKey(value, "csup71Titre");
	}
	public Integer csup72Titre() {
		return (Integer) storedValueForKey("csup72Titre");
	}

	public void setCsup72Titre(Integer value) {
		takeStoredValueForKey(value, "csup72Titre");
	}
	public Integer csup73Titre() {
		return (Integer) storedValueForKey("csup73Titre");
	}

	public void setCsup73Titre(Integer value) {
		takeStoredValueForKey(value, "csup73Titre");
	}
	public Integer csup74Titre() {
		return (Integer) storedValueForKey("csup74Titre");
	}

	public void setCsup74Titre(Integer value) {
		takeStoredValueForKey(value, "csup74Titre");
	}
	public Integer csup7Titre() {
		return (Integer) storedValueForKey("csup7Titre");
	}

	public void setCsup7Titre(Integer value) {
		takeStoredValueForKey(value, "csup7Titre");
	}
	public Integer csup8Titre() {
		return (Integer) storedValueForKey("csup8Titre");
	}

	public void setCsup8Titre(Integer value) {
		takeStoredValueForKey(value, "csup8Titre");
	}
	public Integer csupLibOption2() {
		return (Integer) storedValueForKey("csupLibOption2");
	}

	public void setCsupLibOption2(Integer value) {
		takeStoredValueForKey(value, "csupLibOption2");
	}
	public Integer csupLibOption3() {
		return (Integer) storedValueForKey("csupLibOption3");
	}

	public void setCsupLibOption3(Integer value) {
		takeStoredValueForKey(value, "csupLibOption3");
	}
	public Integer csupLibOption4() {
		return (Integer) storedValueForKey("csupLibOption4");
	}

	public void setCsupLibOption4(Integer value) {
		takeStoredValueForKey(value, "csupLibOption4");
	}
	public Integer csupOption1() {
		return (Integer) storedValueForKey("csupOption1");
	}

	public void setCsupOption1(Integer value) {
		takeStoredValueForKey(value, "csupOption1");
	}
	public Integer csupOption2() {
		return (Integer) storedValueForKey("csupOption2");
	}

	public void setCsupOption2(Integer value) {
		takeStoredValueForKey(value, "csupOption2");
	}
	public Integer csupOption3() {
		return (Integer) storedValueForKey("csupOption3");
	}

	public void setCsupOption3(Integer value) {
		takeStoredValueForKey(value, "csupOption3");
	}
	public Integer csupOption4() {
		return (Integer) storedValueForKey("csupOption4");
	}

	public void setCsupOption4(Integer value) {
		takeStoredValueForKey(value, "csupOption4");
	}
	public Integer csupSousTitre() {
		return (Integer) storedValueForKey("csupSousTitre");
	}

	public void setCsupSousTitre(Integer value) {
		takeStoredValueForKey(value, "csupSousTitre");
	}
	public Integer csupTexte() {
		return (Integer) storedValueForKey("csupTexte");
	}

	public void setCsupTexte(Integer value) {
		takeStoredValueForKey(value, "csupTexte");
	}
	public Integer csupTitre() {
		return (Integer) storedValueForKey("csupTitre");
	}

	public void setCsupTitre(Integer value) {
		takeStoredValueForKey(value, "csupTitre");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolConstanteSupplement.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolConstanteSupplement.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolConstanteSupplement)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolConstanteSupplement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolConstanteSupplement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolConstanteSupplement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolConstanteSupplement)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolConstanteSupplement fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolConstanteSupplement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolConstanteSupplement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolConstanteSupplement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolConstanteSupplement)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolConstanteSupplement fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolConstanteSupplement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolConstanteSupplement eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolConstanteSupplement ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolConstanteSupplement createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolConstanteSupplement.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolConstanteSupplement.ENTITY_NAME + "' !");
		}
		else {
			EOScolConstanteSupplement object = (EOScolConstanteSupplement) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolConstanteSupplement localInstanceOfObject(EOEditingContext ec, EOScolConstanteSupplement object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolConstanteSupplement " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolConstanteSupplement) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
