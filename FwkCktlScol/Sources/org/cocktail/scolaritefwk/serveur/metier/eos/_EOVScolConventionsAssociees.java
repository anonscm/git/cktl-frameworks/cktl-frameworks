/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVScolConventionsAssociees.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOVScolConventionsAssociees extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_VScolConventionsAssociees";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.V_SCOL_CONVENTIONS_ASSOCIEES";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "conOrdre";

	public static final ERXKey<String> CLOC_LIBELLE = new ERXKey<String>("clocLibelle");
	public static final ERXKey<String> CON_OBJET_COURT = new ERXKey<String>("conObjetCourt");
	public static final ERXKey<String> CON_REFERENCE = new ERXKey<String>("conReference");
	public static final ERXKey<String> CON_REFERENCE_EXTERNE = new ERXKey<String>("conReferenceExterne");
	public static final ERXKey<String> CON_TYPE = new ERXKey<String>("conType");
	public static final ERXKey<String> ETAB_CODE = new ERXKey<String>("etabCode");
	public static final ERXKey<Integer> EXE_ORDRE = new ERXKey<Integer>("exeOrdre");
	public static final ERXKey<Integer> FANN_KEY = new ERXKey<Integer>("fannKey");
	public static final ERXKey<Integer> FCON_KEY = new ERXKey<Integer>("fconKey");
	public static final ERXKey<String> FDIP_CODE = new ERXKey<String>("fdipCode");
	public static final ERXKey<Integer> FSPN_KEY = new ERXKey<Integer>("fspnKey");

	public static final String CLOC_LIBELLE_KEY = "clocLibelle";
	public static final String CON_OBJET_COURT_KEY = "conObjetCourt";
	public static final String CON_REFERENCE_KEY = "conReference";
	public static final String CON_REFERENCE_EXTERNE_KEY = "conReferenceExterne";
	public static final String CON_TYPE_KEY = "conType";
	public static final String ETAB_CODE_KEY = "etabCode";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FCON_KEY_KEY = "fconKey";
	public static final String FDIP_CODE_KEY = "fdipCode";
	public static final String FSPN_KEY_KEY = "fspnKey";

	// Non visible attributes
	public static final String CON_ORDRE_KEY = "conOrdre";

	// Colkeys
	public static final String CLOC_LIBELLE_COLKEY = "CLOC_LIBELLE";
	public static final String CON_OBJET_COURT_COLKEY = "CON_OBJET_COURT";
	public static final String CON_REFERENCE_COLKEY = "CON_REFERENCE";
	public static final String CON_REFERENCE_EXTERNE_COLKEY = "CON_REFERENCE_EXTERNE";
	public static final String CON_TYPE_COLKEY = "CON_TYPE";
	public static final String ETAB_CODE_COLKEY = "ETAB_CODE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FCON_KEY_COLKEY = "FCON_KEY";
	public static final String FDIP_CODE_COLKEY = "FDIP_CODE";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";

	// Non visible colkeys
	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOVScolConventionsAssociees with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fannKey
	 * @param fconKey
	 * @param fdipCode
	 * @param fspnKey
	 * @return EOVScolConventionsAssociees
	 */
	public static EOVScolConventionsAssociees create(EOEditingContext editingContext, Integer fannKey, Integer fconKey, String fdipCode, Integer fspnKey) {
		EOVScolConventionsAssociees eo = (EOVScolConventionsAssociees) createAndInsertInstance(editingContext);
		eo.setFannKey(fannKey);
		eo.setFconKey(fconKey);
		eo.setFdipCode(fdipCode);
		eo.setFspnKey(fspnKey);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVScolConventionsAssociees.
	 *
	 * @param editingContext
	 * @return EOVScolConventionsAssociees
	 */
	public static EOVScolConventionsAssociees create(EOEditingContext editingContext) {
		EOVScolConventionsAssociees eo = (EOVScolConventionsAssociees) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVScolConventionsAssociees localInstanceIn(EOEditingContext editingContext) {
		EOVScolConventionsAssociees localInstance = (EOVScolConventionsAssociees) localInstanceOfObject(editingContext, (EOVScolConventionsAssociees) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVScolConventionsAssociees localInstanceIn(EOEditingContext editingContext, EOVScolConventionsAssociees eo) {
		EOVScolConventionsAssociees localInstance = (eo == null) ? null : (EOVScolConventionsAssociees) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String clocLibelle() {
		return (String) storedValueForKey("clocLibelle");
	}

	public void setClocLibelle(String value) {
		takeStoredValueForKey(value, "clocLibelle");
	}
	public String conObjetCourt() {
		return (String) storedValueForKey("conObjetCourt");
	}

	public void setConObjetCourt(String value) {
		takeStoredValueForKey(value, "conObjetCourt");
	}
	public String conReference() {
		return (String) storedValueForKey("conReference");
	}

	public void setConReference(String value) {
		takeStoredValueForKey(value, "conReference");
	}
	public String conReferenceExterne() {
		return (String) storedValueForKey("conReferenceExterne");
	}

	public void setConReferenceExterne(String value) {
		takeStoredValueForKey(value, "conReferenceExterne");
	}
	public String conType() {
		return (String) storedValueForKey("conType");
	}

	public void setConType(String value) {
		takeStoredValueForKey(value, "conType");
	}
	public String etabCode() {
		return (String) storedValueForKey("etabCode");
	}

	public void setEtabCode(String value) {
		takeStoredValueForKey(value, "etabCode");
	}
	public Integer exeOrdre() {
		return (Integer) storedValueForKey("exeOrdre");
	}

	public void setExeOrdre(Integer value) {
		takeStoredValueForKey(value, "exeOrdre");
	}
	public Integer fannKey() {
		return (Integer) storedValueForKey("fannKey");
	}

	public void setFannKey(Integer value) {
		takeStoredValueForKey(value, "fannKey");
	}
	public Integer fconKey() {
		return (Integer) storedValueForKey("fconKey");
	}

	public void setFconKey(Integer value) {
		takeStoredValueForKey(value, "fconKey");
	}
	public String fdipCode() {
		return (String) storedValueForKey("fdipCode");
	}

	public void setFdipCode(String value) {
		takeStoredValueForKey(value, "fdipCode");
	}
	public Integer fspnKey() {
		return (Integer) storedValueForKey("fspnKey");
	}

	public void setFspnKey(Integer value) {
		takeStoredValueForKey(value, "fspnKey");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVScolConventionsAssociees.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVScolConventionsAssociees.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVScolConventionsAssociees)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVScolConventionsAssociees fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVScolConventionsAssociees fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVScolConventionsAssociees eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVScolConventionsAssociees)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVScolConventionsAssociees fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVScolConventionsAssociees fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVScolConventionsAssociees fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVScolConventionsAssociees eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVScolConventionsAssociees)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVScolConventionsAssociees fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVScolConventionsAssociees fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVScolConventionsAssociees eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVScolConventionsAssociees ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVScolConventionsAssociees createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVScolConventionsAssociees.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVScolConventionsAssociees.ENTITY_NAME + "' !");
		}
		else {
			EOVScolConventionsAssociees object = (EOVScolConventionsAssociees) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVScolConventionsAssociees localInstanceOfObject(EOEditingContext ec, EOVScolConventionsAssociees object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVScolConventionsAssociees " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVScolConventionsAssociees) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
