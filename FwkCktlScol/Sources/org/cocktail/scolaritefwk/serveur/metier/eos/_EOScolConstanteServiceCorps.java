/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolConstanteServiceCorps.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolConstanteServiceCorps extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolConstanteServiceCorps";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_CONSTANTE_SERVICE_CORPS";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "cscoKey";

	public static final ERXKey<String> C_CORPS = new ERXKey<String>("cCorps");
	public static final ERXKey<Integer> CSCO_DENOMINATEUR_IN = new ERXKey<Integer>("cscoDenominateurIn");
	public static final ERXKey<Integer> CSCO_DENOMINATEUR_OUT = new ERXKey<Integer>("cscoDenominateurOut");
	public static final ERXKey<Integer> CSCO_NUMERATEUR_IN = new ERXKey<Integer>("cscoNumerateurIn");
	public static final ERXKey<Integer> CSCO_NUMERATEUR_OUT = new ERXKey<Integer>("cscoNumerateurOut");
	public static final ERXKey<String> CSCO_TYPE = new ERXKey<String>("cscoType");

	public static final String C_CORPS_KEY = "cCorps";
	public static final String CSCO_DENOMINATEUR_IN_KEY = "cscoDenominateurIn";
	public static final String CSCO_DENOMINATEUR_OUT_KEY = "cscoDenominateurOut";
	public static final String CSCO_NUMERATEUR_IN_KEY = "cscoNumerateurIn";
	public static final String CSCO_NUMERATEUR_OUT_KEY = "cscoNumerateurOut";
	public static final String CSCO_TYPE_KEY = "cscoType";

	// Non visible attributes
	public static final String CSCO_KEY_KEY = "cscoKey";
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MHCO_CODE_KEY = "mhcoCode";

	// Colkeys
	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String CSCO_DENOMINATEUR_IN_COLKEY = "CSCO_DENOMINATEUR_IN";
	public static final String CSCO_DENOMINATEUR_OUT_COLKEY = "CSCO_DENOMINATEUR_OUT";
	public static final String CSCO_NUMERATEUR_IN_COLKEY = "CSCO_NUMERATEUR_IN";
	public static final String CSCO_NUMERATEUR_OUT_COLKEY = "CSCO_NUMERATEUR_OUT";
	public static final String CSCO_TYPE_COLKEY = "CSCO_TYPE";

	// Non visible colkeys
	public static final String CSCO_KEY_COLKEY = "CSCO_KEY";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MHCO_CODE_COLKEY = "MHCO_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode> TO_FWK_SCOLARITE__SCOL_MAQUETTE_HORAIRE_CODE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode>("toFwkScolarite_ScolMaquetteHoraireCode");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_HORAIRE_CODE_KEY = "toFwkScolarite_ScolMaquetteHoraireCode";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolConstanteServiceCorps with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cCorps
	 * @param cscoDenominateurIn
	 * @param cscoDenominateurOut
	 * @param cscoNumerateurIn
	 * @param cscoNumerateurOut
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteHoraireCode
	 * @return EOScolConstanteServiceCorps
	 */
	public static EOScolConstanteServiceCorps create(EOEditingContext editingContext, String cCorps, Integer cscoDenominateurIn, Integer cscoDenominateurOut, Integer cscoNumerateurIn, Integer cscoNumerateurOut, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode toFwkScolarite_ScolMaquetteHoraireCode) {
		EOScolConstanteServiceCorps eo = (EOScolConstanteServiceCorps) createAndInsertInstance(editingContext);
		eo.setCCorps(cCorps);
		eo.setCscoDenominateurIn(cscoDenominateurIn);
		eo.setCscoDenominateurOut(cscoDenominateurOut);
		eo.setCscoNumerateurIn(cscoNumerateurIn);
		eo.setCscoNumerateurOut(cscoNumerateurOut);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteHoraireCodeRelationship(toFwkScolarite_ScolMaquetteHoraireCode);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolConstanteServiceCorps.
	 *
	 * @param editingContext
	 * @return EOScolConstanteServiceCorps
	 */
	public static EOScolConstanteServiceCorps create(EOEditingContext editingContext) {
		EOScolConstanteServiceCorps eo = (EOScolConstanteServiceCorps) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolConstanteServiceCorps localInstanceIn(EOEditingContext editingContext) {
		EOScolConstanteServiceCorps localInstance = (EOScolConstanteServiceCorps) localInstanceOfObject(editingContext, (EOScolConstanteServiceCorps) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolConstanteServiceCorps localInstanceIn(EOEditingContext editingContext, EOScolConstanteServiceCorps eo) {
		EOScolConstanteServiceCorps localInstance = (eo == null) ? null : (EOScolConstanteServiceCorps) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cCorps() {
		return (String) storedValueForKey("cCorps");
	}

	public void setCCorps(String value) {
		takeStoredValueForKey(value, "cCorps");
	}
	public Integer cscoDenominateurIn() {
		return (Integer) storedValueForKey("cscoDenominateurIn");
	}

	public void setCscoDenominateurIn(Integer value) {
		takeStoredValueForKey(value, "cscoDenominateurIn");
	}
	public Integer cscoDenominateurOut() {
		return (Integer) storedValueForKey("cscoDenominateurOut");
	}

	public void setCscoDenominateurOut(Integer value) {
		takeStoredValueForKey(value, "cscoDenominateurOut");
	}
	public Integer cscoNumerateurIn() {
		return (Integer) storedValueForKey("cscoNumerateurIn");
	}

	public void setCscoNumerateurIn(Integer value) {
		takeStoredValueForKey(value, "cscoNumerateurIn");
	}
	public Integer cscoNumerateurOut() {
		return (Integer) storedValueForKey("cscoNumerateurOut");
	}

	public void setCscoNumerateurOut(Integer value) {
		takeStoredValueForKey(value, "cscoNumerateurOut");
	}
	public String cscoType() {
		return (String) storedValueForKey("cscoType");
	}

	public void setCscoType(String value) {
		takeStoredValueForKey(value, "cscoType");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode toFwkScolarite_ScolMaquetteHoraireCode() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode)storedValueForKey("toFwkScolarite_ScolMaquetteHoraireCode");
	}

	public void setToFwkScolarite_ScolMaquetteHoraireCodeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteHoraireCode oldValue = toFwkScolarite_ScolMaquetteHoraireCode();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteHoraireCode");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteHoraireCode");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolConstanteServiceCorps.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolConstanteServiceCorps.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolConstanteServiceCorps)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolConstanteServiceCorps fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolConstanteServiceCorps fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolConstanteServiceCorps eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolConstanteServiceCorps)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolConstanteServiceCorps fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolConstanteServiceCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolConstanteServiceCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolConstanteServiceCorps eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolConstanteServiceCorps)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolConstanteServiceCorps fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolConstanteServiceCorps fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolConstanteServiceCorps eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolConstanteServiceCorps ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolConstanteServiceCorps createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolConstanteServiceCorps.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolConstanteServiceCorps.ENTITY_NAME + "' !");
		}
		else {
			EOScolConstanteServiceCorps object = (EOScolConstanteServiceCorps) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolConstanteServiceCorps localInstanceOfObject(EOEditingContext ec, EOScolConstanteServiceCorps object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolConstanteServiceCorps " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolConstanteServiceCorps) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
