/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolDroitPreference.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolDroitPreference extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolDroitPreference";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_DROIT_PREFERENCE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "dlogKey";

	public static final ERXKey<String> DPRF_CODES = new ERXKey<String>("dprfCodes");
	public static final ERXKey<String> DPRF_COULEURS = new ERXKey<String>("dprfCouleurs");
	public static final ERXKey<Integer> DPRF_TABLEUR = new ERXKey<Integer>("dprfTableur");

	public static final String DPRF_CODES_KEY = "dprfCodes";
	public static final String DPRF_COULEURS_KEY = "dprfCouleurs";
	public static final String DPRF_TABLEUR_KEY = "dprfTableur";

	// Non visible attributes
	public static final String DLOG_KEY_KEY = "dlogKey";

	// Colkeys
	public static final String DPRF_CODES_COLKEY = "DPRF_CODES";
	public static final String DPRF_COULEURS_COLKEY = "DPRF_COULEURS";
	public static final String DPRF_TABLEUR_COLKEY = "DPRF_TABLEUR";

	// Non visible colkeys
	public static final String DLOG_KEY_COLKEY = "DLOG_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin> TO_FWK_SCOLARITE__SCOL_DROIT_LOGIN = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin>("toFwkScolarite_ScolDroitLogin");

	public static final String TO_FWK_SCOLARITE__SCOL_DROIT_LOGIN_KEY = "toFwkScolarite_ScolDroitLogin";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolDroitPreference with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dprfTableur
	 * @param toFwkScolarite_ScolDroitLogin
	 * @return EOScolDroitPreference
	 */
	public static EOScolDroitPreference create(EOEditingContext editingContext, Integer dprfTableur, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin toFwkScolarite_ScolDroitLogin) {
		EOScolDroitPreference eo = (EOScolDroitPreference) createAndInsertInstance(editingContext);
		eo.setDprfTableur(dprfTableur);
		eo.setToFwkScolarite_ScolDroitLoginRelationship(toFwkScolarite_ScolDroitLogin);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolDroitPreference.
	 *
	 * @param editingContext
	 * @return EOScolDroitPreference
	 */
	public static EOScolDroitPreference create(EOEditingContext editingContext) {
		EOScolDroitPreference eo = (EOScolDroitPreference) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolDroitPreference localInstanceIn(EOEditingContext editingContext) {
		EOScolDroitPreference localInstance = (EOScolDroitPreference) localInstanceOfObject(editingContext, (EOScolDroitPreference) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolDroitPreference localInstanceIn(EOEditingContext editingContext, EOScolDroitPreference eo) {
		EOScolDroitPreference localInstance = (eo == null) ? null : (EOScolDroitPreference) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String dprfCodes() {
		return (String) storedValueForKey("dprfCodes");
	}

	public void setDprfCodes(String value) {
		takeStoredValueForKey(value, "dprfCodes");
	}
	public String dprfCouleurs() {
		return (String) storedValueForKey("dprfCouleurs");
	}

	public void setDprfCouleurs(String value) {
		takeStoredValueForKey(value, "dprfCouleurs");
	}
	public Integer dprfTableur() {
		return (Integer) storedValueForKey("dprfTableur");
	}

	public void setDprfTableur(Integer value) {
		takeStoredValueForKey(value, "dprfTableur");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin toFwkScolarite_ScolDroitLogin() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin)storedValueForKey("toFwkScolarite_ScolDroitLogin");
	}

	public void setToFwkScolarite_ScolDroitLoginRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin oldValue = toFwkScolarite_ScolDroitLogin();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolDroitLogin");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolDroitLogin");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolDroitPreference.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolDroitPreference.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolDroitPreference)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolDroitPreference fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolDroitPreference fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolDroitPreference eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolDroitPreference)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolDroitPreference fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolDroitPreference fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolDroitPreference fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolDroitPreference eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolDroitPreference)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolDroitPreference fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolDroitPreference fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolDroitPreference eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolDroitPreference ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolDroitPreference createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolDroitPreference.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolDroitPreference.ENTITY_NAME + "' !");
		}
		else {
			EOScolDroitPreference object = (EOScolDroitPreference) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolDroitPreference localInstanceOfObject(EOEditingContext ec, EOScolDroitPreference object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolDroitPreference " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolDroitPreference) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
