/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteRepartitionSem.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteRepartitionSem extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteRepartitionSem";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_REPARTITION_SEM";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mrsemKey";

	public static final ERXKey<Integer> MRSEM_ETAT_CALCUL = new ERXKey<Integer>("mrsemEtatCalcul");
	public static final ERXKey<java.math.BigDecimal> MRSEM_MOYENNE1 = new ERXKey<java.math.BigDecimal>("mrsemMoyenne1");
	public static final ERXKey<java.math.BigDecimal> MRSEM_MOYENNE2 = new ERXKey<java.math.BigDecimal>("mrsemMoyenne2");

	public static final String MRSEM_ETAT_CALCUL_KEY = "mrsemEtatCalcul";
	public static final String MRSEM_MOYENNE1_KEY = "mrsemMoyenne1";
	public static final String MRSEM_MOYENNE2_KEY = "mrsemMoyenne2";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MPAR_KEY_KEY = "mparKey";
	public static final String MRSEM_KEY_KEY = "mrsemKey";
	public static final String MSEM_KEY_KEY = "msemKey";

	// Colkeys
	public static final String MRSEM_ETAT_CALCUL_COLKEY = "MRSEM_ETAT_CALCUL";
	public static final String MRSEM_MOYENNE1_COLKEY = "MRSEM_MOYENNE1";
	public static final String MRSEM_MOYENNE2_COLKEY = "MRSEM_MOYENNE2";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MPAR_KEY_COLKEY = "MPAR_KEY";
	public static final String MRSEM_KEY_COLKEY = "MRSEM_KEY";
	public static final String MSEM_KEY_COLKEY = "MSEM_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre> TO_FWK_SCOLARITE__SCOL_INSCRIPTION_SEMESTRES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre>("toFwkScolarite_ScolInscriptionSemestres");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours> TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours>("toFwkScolarite_ScolMaquetteParcours");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre> TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre>("toFwkScolarite_ScolMaquetteSemestre");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_INSCRIPTION_SEMESTRES_KEY = "toFwkScolarite_ScolInscriptionSemestres";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY = "toFwkScolarite_ScolMaquetteParcours";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY = "toFwkScolarite_ScolMaquetteSemestre";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteRepartitionSem with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mrsemEtatCalcul
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteParcours
	 * @param toFwkScolarite_ScolMaquetteSemestre
	 * @return EOScolMaquetteRepartitionSem
	 */
	public static EOScolMaquetteRepartitionSem create(EOEditingContext editingContext, Integer mrsemEtatCalcul, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre) {
		EOScolMaquetteRepartitionSem eo = (EOScolMaquetteRepartitionSem) createAndInsertInstance(editingContext);
		eo.setMrsemEtatCalcul(mrsemEtatCalcul);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteParcoursRelationship(toFwkScolarite_ScolMaquetteParcours);
		eo.setToFwkScolarite_ScolMaquetteSemestreRelationship(toFwkScolarite_ScolMaquetteSemestre);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteRepartitionSem.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteRepartitionSem
	 */
	public static EOScolMaquetteRepartitionSem create(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionSem eo = (EOScolMaquetteRepartitionSem) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteRepartitionSem localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionSem localInstance = (EOScolMaquetteRepartitionSem) localInstanceOfObject(editingContext, (EOScolMaquetteRepartitionSem) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteRepartitionSem localInstanceIn(EOEditingContext editingContext, EOScolMaquetteRepartitionSem eo) {
		EOScolMaquetteRepartitionSem localInstance = (eo == null) ? null : (EOScolMaquetteRepartitionSem) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mrsemEtatCalcul() {
		return (Integer) storedValueForKey("mrsemEtatCalcul");
	}

	public void setMrsemEtatCalcul(Integer value) {
		takeStoredValueForKey(value, "mrsemEtatCalcul");
	}
	public java.math.BigDecimal mrsemMoyenne1() {
		return (java.math.BigDecimal) storedValueForKey("mrsemMoyenne1");
	}

	public void setMrsemMoyenne1(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrsemMoyenne1");
	}
	public java.math.BigDecimal mrsemMoyenne2() {
		return (java.math.BigDecimal) storedValueForKey("mrsemMoyenne2");
	}

	public void setMrsemMoyenne2(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrsemMoyenne2");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours)storedValueForKey("toFwkScolarite_ScolMaquetteParcours");
	}

	public void setToFwkScolarite_ScolMaquetteParcoursRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours oldValue = toFwkScolarite_ScolMaquetteParcours();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteParcours");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteParcours");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre)storedValueForKey("toFwkScolarite_ScolMaquetteSemestre");
	}

	public void setToFwkScolarite_ScolMaquetteSemestreRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre oldValue = toFwkScolarite_ScolMaquetteSemestre();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteSemestre");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteSemestre");
		}
	}
  
	public NSArray toFwkScolarite_ScolInscriptionSemestres() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolInscriptionSemestres");
	}

	public NSArray toFwkScolarite_ScolInscriptionSemestres(EOQualifier qualifier) {
		return toFwkScolarite_ScolInscriptionSemestres(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolInscriptionSemestres(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolInscriptionSemestres(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolInscriptionSemestres(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre.TO_SCOL_MAQUETTE_REPARTITION_SEM_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolInscriptionSemestres();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolInscriptionSemestresRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionSemestres");
	}

	public void removeFromToFwkScolarite_ScolInscriptionSemestresRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionSemestres");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre createToFwkScolarite_ScolInscriptionSemestresRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolInscriptionSemestre");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolInscriptionSemestres");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre) eo;
	}

	public void deleteToFwkScolarite_ScolInscriptionSemestresRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionSemestres");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolInscriptionSemestresRelationships() {
		Enumeration objects = toFwkScolarite_ScolInscriptionSemestres().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolInscriptionSemestresRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteRepartitionSem.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteRepartitionSem.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteRepartitionSem)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteRepartitionSem fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteRepartitionSem fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteRepartitionSem eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteRepartitionSem)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionSem fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionSem fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionSem fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteRepartitionSem eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteRepartitionSem)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionSem fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionSem fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteRepartitionSem eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteRepartitionSem ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteRepartitionSem createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteRepartitionSem.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteRepartitionSem.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteRepartitionSem object = (EOScolMaquetteRepartitionSem) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteRepartitionSem localInstanceOfObject(EOEditingContext ec, EOScolMaquetteRepartitionSem object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteRepartitionSem " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteRepartitionSem) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
