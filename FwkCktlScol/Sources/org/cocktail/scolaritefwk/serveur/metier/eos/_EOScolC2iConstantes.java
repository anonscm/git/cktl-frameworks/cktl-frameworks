/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolC2iConstantes.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolC2iConstantes extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolC2iConstantes";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_C2I_CONSTANTES";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "c2icKey";

	public static final ERXKey<java.math.BigDecimal> C2IC_MOYENNE_OBTENTION = new ERXKey<java.math.BigDecimal>("c2icMoyenneObtention");
	public static final ERXKey<Integer> C2IC_NB_NIVEAU = new ERXKey<Integer>("c2icNbNiveau");
	public static final ERXKey<Integer> C2IC_NIVEAU1_CONTRAINTE = new ERXKey<Integer>("c2icNiveau1Contrainte");
	public static final ERXKey<java.math.BigDecimal> C2IC_NIVEAU1_OBTENTION = new ERXKey<java.math.BigDecimal>("c2icNiveau1Obtention");
	public static final ERXKey<Integer> C2IC_NIVEAU2_CONTRAINTE = new ERXKey<Integer>("c2icNiveau2Contrainte");
	public static final ERXKey<java.math.BigDecimal> C2IC_NIVEAU2_OBTENTION = new ERXKey<java.math.BigDecimal>("c2icNiveau2Obtention");
	public static final ERXKey<Integer> C2IC_NIVEAU3_CONTRAINTE = new ERXKey<Integer>("c2icNiveau3Contrainte");
	public static final ERXKey<java.math.BigDecimal> C2IC_NIVEAU3_OBTENTION = new ERXKey<java.math.BigDecimal>("c2icNiveau3Obtention");
	public static final ERXKey<Integer> C2IC_TYPE = new ERXKey<Integer>("c2icType");

	public static final String C2IC_MOYENNE_OBTENTION_KEY = "c2icMoyenneObtention";
	public static final String C2IC_NB_NIVEAU_KEY = "c2icNbNiveau";
	public static final String C2IC_NIVEAU1_CONTRAINTE_KEY = "c2icNiveau1Contrainte";
	public static final String C2IC_NIVEAU1_OBTENTION_KEY = "c2icNiveau1Obtention";
	public static final String C2IC_NIVEAU2_CONTRAINTE_KEY = "c2icNiveau2Contrainte";
	public static final String C2IC_NIVEAU2_OBTENTION_KEY = "c2icNiveau2Obtention";
	public static final String C2IC_NIVEAU3_CONTRAINTE_KEY = "c2icNiveau3Contrainte";
	public static final String C2IC_NIVEAU3_OBTENTION_KEY = "c2icNiveau3Obtention";
	public static final String C2IC_TYPE_KEY = "c2icType";

	// Non visible attributes
	public static final String C2IC_KEY_KEY = "c2icKey";
	public static final String C2IE_KEY_KEY = "c2ieKey";
	public static final String FANN_KEY_KEY = "fannKey";

	// Colkeys
	public static final String C2IC_MOYENNE_OBTENTION_COLKEY = "C2IC_MOYENNE_OBTENTION";
	public static final String C2IC_NB_NIVEAU_COLKEY = "C2IC_NB_NIVEAU";
	public static final String C2IC_NIVEAU1_CONTRAINTE_COLKEY = "C2IC_NIVEAU1_CONTRAINTE";
	public static final String C2IC_NIVEAU1_OBTENTION_COLKEY = "C2IC_NIVEAU1_OBTENTION";
	public static final String C2IC_NIVEAU2_CONTRAINTE_COLKEY = "C2IC_NIVEAU2_CONTRAINTE";
	public static final String C2IC_NIVEAU2_OBTENTION_COLKEY = "C2IC_NIVEAU2_OBTENTION";
	public static final String C2IC_NIVEAU3_CONTRAINTE_COLKEY = "C2IC_NIVEAU3_CONTRAINTE";
	public static final String C2IC_NIVEAU3_OBTENTION_COLKEY = "C2IC_NIVEAU3_OBTENTION";
	public static final String C2IC_TYPE_COLKEY = "C2IC_TYPE";

	// Non visible colkeys
	public static final String C2IC_KEY_COLKEY = "C2IC_KEY";
	public static final String C2IE_KEY_COLKEY = "C2IE_KEY";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete> TO_FWK_SCOLARITE__SCOL_C2I_ENTETE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete>("toFwkScolarite_ScolC2iEntete");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");

	public static final String TO_FWK_SCOLARITE__SCOL_C2I_ENTETE_KEY = "toFwkScolarite_ScolC2iEntete";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolC2iConstantes with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param c2icMoyenneObtention
	 * @param c2icNbNiveau
	 * @param c2icNiveau1Contrainte
	 * @param c2icNiveau1Obtention
	 * @param c2icNiveau2Contrainte
	 * @param c2icNiveau2Obtention
	 * @param c2icNiveau3Contrainte
	 * @param c2icNiveau3Obtention
	 * @param c2icType
	 * @param toFwkScolarite_ScolC2iEntete
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @return EOScolC2iConstantes
	 */
	public static EOScolC2iConstantes create(EOEditingContext editingContext, java.math.BigDecimal c2icMoyenneObtention, Integer c2icNbNiveau, Integer c2icNiveau1Contrainte, java.math.BigDecimal c2icNiveau1Obtention, Integer c2icNiveau2Contrainte, java.math.BigDecimal c2icNiveau2Obtention, Integer c2icNiveau3Contrainte, java.math.BigDecimal c2icNiveau3Obtention, Integer c2icType, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete toFwkScolarite_ScolC2iEntete, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee) {
		EOScolC2iConstantes eo = (EOScolC2iConstantes) createAndInsertInstance(editingContext);
		eo.setC2icMoyenneObtention(c2icMoyenneObtention);
		eo.setC2icNbNiveau(c2icNbNiveau);
		eo.setC2icNiveau1Contrainte(c2icNiveau1Contrainte);
		eo.setC2icNiveau1Obtention(c2icNiveau1Obtention);
		eo.setC2icNiveau2Contrainte(c2icNiveau2Contrainte);
		eo.setC2icNiveau2Obtention(c2icNiveau2Obtention);
		eo.setC2icNiveau3Contrainte(c2icNiveau3Contrainte);
		eo.setC2icNiveau3Obtention(c2icNiveau3Obtention);
		eo.setC2icType(c2icType);
		eo.setToFwkScolarite_ScolC2iEnteteRelationship(toFwkScolarite_ScolC2iEntete);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolC2iConstantes.
	 *
	 * @param editingContext
	 * @return EOScolC2iConstantes
	 */
	public static EOScolC2iConstantes create(EOEditingContext editingContext) {
		EOScolC2iConstantes eo = (EOScolC2iConstantes) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolC2iConstantes localInstanceIn(EOEditingContext editingContext) {
		EOScolC2iConstantes localInstance = (EOScolC2iConstantes) localInstanceOfObject(editingContext, (EOScolC2iConstantes) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolC2iConstantes localInstanceIn(EOEditingContext editingContext, EOScolC2iConstantes eo) {
		EOScolC2iConstantes localInstance = (eo == null) ? null : (EOScolC2iConstantes) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public java.math.BigDecimal c2icMoyenneObtention() {
		return (java.math.BigDecimal) storedValueForKey("c2icMoyenneObtention");
	}

	public void setC2icMoyenneObtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "c2icMoyenneObtention");
	}
	public Integer c2icNbNiveau() {
		return (Integer) storedValueForKey("c2icNbNiveau");
	}

	public void setC2icNbNiveau(Integer value) {
		takeStoredValueForKey(value, "c2icNbNiveau");
	}
	public Integer c2icNiveau1Contrainte() {
		return (Integer) storedValueForKey("c2icNiveau1Contrainte");
	}

	public void setC2icNiveau1Contrainte(Integer value) {
		takeStoredValueForKey(value, "c2icNiveau1Contrainte");
	}
	public java.math.BigDecimal c2icNiveau1Obtention() {
		return (java.math.BigDecimal) storedValueForKey("c2icNiveau1Obtention");
	}

	public void setC2icNiveau1Obtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "c2icNiveau1Obtention");
	}
	public Integer c2icNiveau2Contrainte() {
		return (Integer) storedValueForKey("c2icNiveau2Contrainte");
	}

	public void setC2icNiveau2Contrainte(Integer value) {
		takeStoredValueForKey(value, "c2icNiveau2Contrainte");
	}
	public java.math.BigDecimal c2icNiveau2Obtention() {
		return (java.math.BigDecimal) storedValueForKey("c2icNiveau2Obtention");
	}

	public void setC2icNiveau2Obtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "c2icNiveau2Obtention");
	}
	public Integer c2icNiveau3Contrainte() {
		return (Integer) storedValueForKey("c2icNiveau3Contrainte");
	}

	public void setC2icNiveau3Contrainte(Integer value) {
		takeStoredValueForKey(value, "c2icNiveau3Contrainte");
	}
	public java.math.BigDecimal c2icNiveau3Obtention() {
		return (java.math.BigDecimal) storedValueForKey("c2icNiveau3Obtention");
	}

	public void setC2icNiveau3Obtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "c2icNiveau3Obtention");
	}
	public Integer c2icType() {
		return (Integer) storedValueForKey("c2icType");
	}

	public void setC2icType(Integer value) {
		takeStoredValueForKey(value, "c2icType");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete toFwkScolarite_ScolC2iEntete() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete)storedValueForKey("toFwkScolarite_ScolC2iEntete");
	}

	public void setToFwkScolarite_ScolC2iEnteteRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete oldValue = toFwkScolarite_ScolC2iEntete();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolC2iEntete");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolC2iEntete");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolC2iConstantes.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolC2iConstantes.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolC2iConstantes)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolC2iConstantes fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolC2iConstantes fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolC2iConstantes eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolC2iConstantes)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolC2iConstantes fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolC2iConstantes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolC2iConstantes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolC2iConstantes eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolC2iConstantes)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolC2iConstantes fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolC2iConstantes fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolC2iConstantes eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolC2iConstantes ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolC2iConstantes createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolC2iConstantes.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolC2iConstantes.ENTITY_NAME + "' !");
		}
		else {
			EOScolC2iConstantes object = (EOScolC2iConstantes) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolC2iConstantes localInstanceOfObject(EOEditingContext ec, EOScolC2iConstantes object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolC2iConstantes " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolC2iConstantes) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
