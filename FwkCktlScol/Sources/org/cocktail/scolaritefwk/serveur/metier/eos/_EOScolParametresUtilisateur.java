/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolParametresUtilisateur.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolParametresUtilisateur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolParametresUtilisateur";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_PARAMETRES_UTILISATEUR";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "putiOrdre";

	public static final ERXKey<String> PUTI_APPLICATION = new ERXKey<String>("putiApplication");
	public static final ERXKey<NSTimestamp> PUTI_DEBUT = new ERXKey<NSTimestamp>("putiDebut");
	public static final ERXKey<NSTimestamp> PUTI_FIN = new ERXKey<NSTimestamp>("putiFin");
	public static final ERXKey<NSTimestamp> PUTI_HOSTNAME = new ERXKey<NSTimestamp>("putiHostname");
	public static final ERXKey<String> PUTI_LOGIN = new ERXKey<String>("putiLogin");

	public static final String PUTI_APPLICATION_KEY = "putiApplication";
	public static final String PUTI_DEBUT_KEY = "putiDebut";
	public static final String PUTI_FIN_KEY = "putiFin";
	public static final String PUTI_HOSTNAME_KEY = "putiHostname";
	public static final String PUTI_LOGIN_KEY = "putiLogin";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String PUTI_ORDRE_KEY = "putiOrdre";

	// Colkeys
	public static final String PUTI_APPLICATION_COLKEY = "PUTI_APPLICATION";
	public static final String PUTI_DEBUT_COLKEY = "PUTI_DEBUT";
	public static final String PUTI_FIN_COLKEY = "PUTI_FIN";
	public static final String PUTI_HOSTNAME_COLKEY = "PUTI_HOSTNAME";
	public static final String PUTI_LOGIN_COLKEY = "PUTI_LOGIN";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String PUTI_ORDRE_COLKEY = "PUTI_ORDRE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolParametresUtilisateur with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param putiApplication
	 * @param putiDebut
	 * @param putiHostname
	 * @param putiLogin
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @return EOScolParametresUtilisateur
	 */
	public static EOScolParametresUtilisateur create(EOEditingContext editingContext, String putiApplication, NSTimestamp putiDebut, NSTimestamp putiHostname, String putiLogin, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee) {
		EOScolParametresUtilisateur eo = (EOScolParametresUtilisateur) createAndInsertInstance(editingContext);
		eo.setPutiApplication(putiApplication);
		eo.setPutiDebut(putiDebut);
		eo.setPutiHostname(putiHostname);
		eo.setPutiLogin(putiLogin);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolParametresUtilisateur.
	 *
	 * @param editingContext
	 * @return EOScolParametresUtilisateur
	 */
	public static EOScolParametresUtilisateur create(EOEditingContext editingContext) {
		EOScolParametresUtilisateur eo = (EOScolParametresUtilisateur) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolParametresUtilisateur localInstanceIn(EOEditingContext editingContext) {
		EOScolParametresUtilisateur localInstance = (EOScolParametresUtilisateur) localInstanceOfObject(editingContext, (EOScolParametresUtilisateur) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolParametresUtilisateur localInstanceIn(EOEditingContext editingContext, EOScolParametresUtilisateur eo) {
		EOScolParametresUtilisateur localInstance = (eo == null) ? null : (EOScolParametresUtilisateur) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String putiApplication() {
		return (String) storedValueForKey("putiApplication");
	}

	public void setPutiApplication(String value) {
		takeStoredValueForKey(value, "putiApplication");
	}
	public NSTimestamp putiDebut() {
		return (NSTimestamp) storedValueForKey("putiDebut");
	}

	public void setPutiDebut(NSTimestamp value) {
		takeStoredValueForKey(value, "putiDebut");
	}
	public NSTimestamp putiFin() {
		return (NSTimestamp) storedValueForKey("putiFin");
	}

	public void setPutiFin(NSTimestamp value) {
		takeStoredValueForKey(value, "putiFin");
	}
	public NSTimestamp putiHostname() {
		return (NSTimestamp) storedValueForKey("putiHostname");
	}

	public void setPutiHostname(NSTimestamp value) {
		takeStoredValueForKey(value, "putiHostname");
	}
	public String putiLogin() {
		return (String) storedValueForKey("putiLogin");
	}

	public void setPutiLogin(String value) {
		takeStoredValueForKey(value, "putiLogin");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolParametresUtilisateur.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolParametresUtilisateur.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolParametresUtilisateur)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolParametresUtilisateur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolParametresUtilisateur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolParametresUtilisateur eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolParametresUtilisateur)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolParametresUtilisateur fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolParametresUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolParametresUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolParametresUtilisateur eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolParametresUtilisateur)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolParametresUtilisateur fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolParametresUtilisateur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolParametresUtilisateur eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolParametresUtilisateur ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolParametresUtilisateur createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolParametresUtilisateur.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolParametresUtilisateur.ENTITY_NAME + "' !");
		}
		else {
			EOScolParametresUtilisateur object = (EOScolParametresUtilisateur) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolParametresUtilisateur localInstanceOfObject(EOEditingContext ec, EOScolParametresUtilisateur object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolParametresUtilisateur " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolParametresUtilisateur) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
