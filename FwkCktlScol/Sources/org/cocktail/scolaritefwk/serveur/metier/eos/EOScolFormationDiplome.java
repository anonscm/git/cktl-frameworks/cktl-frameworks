/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.metier.eos;

import org.cocktail.scolaritefwk.serveur.exception.ScolFormationDiplomeException;
import org.cocktail.scolaritefwk.serveur.interfaces.IScolFormationDomaine;
import org.cocktail.scolaritefwk.serveur.interfaces.IScolFormationGrade;

import com.webobjects.foundation.NSValidation;

public class EOScolFormationDiplome extends _EOScolFormationDiplome {

	// Valeurs possibles pour les actions
	public static final String ACTION_INSERT = "I";
	public static final String ACTION_UPDATE = "U";
	private String actionType;
	// Valeurs possibles pour FDIP_TYPE
	public static final String FORMATION_CLASSIQUE = "C";
	public static final String FORMATION_LMD = "H";
	public static final String FORMATION_FICTIVE = "F";
	// Valeurs possibles pour FDIP_SEMESTRIALISATION
	public static final String ORGANISATION_SEMESTRIELLE = "O";
	public static final String ORGANISATION_ANNUELLE = "N";
	// Valeurs possibles pour FDIP_DELIBERATION
	public static final String DELIBERATION_SEMESTRIELLE = "O";
	public static final String DELIBERATION_ANNUELLE = "N";
	// Valeurs possibles pour FDIP_MONO_SEMESTRE
	public static final String REPARTITION_MONO_SEMESTRE = "O";
	public static final String REPARTITION_MULTI_SEMESTRE = "N";
	// Variables pour stocker les bornes des habilitations
	private Integer habilitationDepart = null;
	private Integer habilitationArrivee = null;
	// Valeurs possibles pour la creation du code diplome
	// true : doit etre saisi via l'interface
	// false : sera cree via la procedure
	private boolean codeDiplomeObligatoire;

	public EOScolFormationDiplome() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.setActionType(ACTION_INSERT);
		this.validateObjectMetier();
		this.validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.setActionType(ACTION_UPDATE);
		this.validateObjectMetier();
		this.validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		this.validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		// validate for Insert
		if (this.isActionInsert() == true) {
			// On valide l'objet lui-meme
			this.validateScolFormationDiplomeForInsert();
			// On valide les dependances
		}
		// validate for Update
		if (this.isActionUpdate() == true) {
			// On valide l'objet lui-meme
			this.validateScolFormationDiplomeForUpdate();
			// On valide les dependances
		}

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public void validateScolFormationDiplomeForInsert() throws ScolFormationDiplomeException {
		// champs obligatoires
		if (fdipType() == null) {
			throw new ScolFormationDiplomeException("Type du diplome obligatoire");
		}
		if ((toFwkScolarite_ScolFormationGrade() == null) && (toFwkScolarite_ScolFormationFiliere() == null)) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de grade/filiere");
		}
		if ((toFwkScolarite_ScolFormationDomaine() == null) && (toFwkScolarite_ScolFormationDepartement() == null)) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de domaine/departement");
		}
		if (toFwkScolarite_ScolFormationVocation() == null) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de vocation");
		}
		if (toFwkScolarix_VEtablissementScolarite() == null) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas d'etablissement");
		}
		if (toFwkScolarix_VComposanteScolarite() == null) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de composante");
		}
		if (toFwkScolarite_ScolFormationVocation().fvocCode() == null) {
			throw new ScolFormationDiplomeException("Vocation du diplome obligatoire");
		}
		if (fdipMention() == null) {
			throw new ScolFormationDiplomeException("Mention du diplome obligatoire");
		}
		if (fdipLibelle() == null) {
			throw new ScolFormationDiplomeException("Libelle du diplome obligatoire");
		}
		if (fdipAbreviation() == null) {
			throw new ScolFormationDiplomeException("Abreviation du diplome obligatoire");
		}
		if (fdipDepart() == null) {
			throw new ScolFormationDiplomeException("Niveau de depart du diplome obligatoire");
		}
		if (fdipArrivee() == null) {
			throw new ScolFormationDiplomeException("Niveau d'arrivee du diplome obligatoire");
		}
		if (toFwkScolarix_VEtablissementScolarite().cRne() == null) {
			throw new ScolFormationDiplomeException("Etablissement du diplome obligatoire");
		}
		// if (toFwkScolarix_VComposanteScolarite().cRne() == null) {
		// throw new ScolFormationDiplomeException("Composante du diplome obligatoire");
		// }
		if (fdipTypeDroit() == null) {
			throw new ScolFormationDiplomeException("Type des droits de scolarite du diplome obligatoire");
		}
		if (fdipSemestrialisation() == null) {
			throw new ScolFormationDiplomeException("Organisation du diplome obligatoire (semestrielle : Oui/Non)");
		}
		if (fdipDeliberation() == null) {
			throw new ScolFormationDiplomeException("Deliberation du diplome obligatoire (semestrielle : Oui/Non)");
		}
		if (fdipMonoSemestre() == null) {
			throw new ScolFormationDiplomeException("Duree du diplome obligatoire (mono semestre : Oui/Non)");
		}
		// champs obligatoires conditionnels
		if ((fdipType().equals(FORMATION_LMD) == true) || (fdipType().equals(FORMATION_FICTIVE) == true)) {
			if ((toFwkScolarite_ScolFormationGrade() == null) || (toFwkScolarite_ScolFormationGrade().fgraCode() == null)) {
				throw new ScolFormationDiplomeException("Grade obligatoire pour un diplome de type LMD / FICTIF");
			}
			if ((toFwkScolarite_ScolFormationDomaine() == null) || (toFwkScolarite_ScolFormationDomaine().fdomCode() == null)) {
				throw new ScolFormationDiplomeException("Domaine obligatoire pour un diplome de type LMD / FICTIF");
			}
			if (fdipSemestrialisation().equals(ORGANISATION_SEMESTRIELLE) == false) {
				throw new ScolFormationDiplomeException("L'organisation doit etre semestrielle pour un diplome de type LMD / FICTIF");
			}
			if (fdipDeliberation().equals(DELIBERATION_SEMESTRIELLE) == false) {
				throw new ScolFormationDiplomeException("La deliberation ne peut etre annuelle pour un diplome de type LMD / FICTIF");
			}
			if (fdipMonoSemestre().equals(REPARTITION_MULTI_SEMESTRE) == false) {
				throw new ScolFormationDiplomeException("La duree ne peut etre mono semestre pour un diplome de type LMD / FICTIF");
			}
		}
		if (fdipType().equals(FORMATION_CLASSIQUE) == true) {
			if ((toFwkScolarite_ScolFormationFiliere() == null) || (toFwkScolarite_ScolFormationFiliere().ffilCode() == null)) {
				throw new ScolFormationDiplomeException("Filiere obligatoire pour le diplome de type CLASSIQUE");
			}
			if ((toFwkScolarite_ScolFormationDepartement() == null) || (toFwkScolarite_ScolFormationDepartement().fdepCode() == null)) {
				throw new ScolFormationDiplomeException("Departement obligatoire pour le diplome de type CLASSIQUE");
			}
		}
		// verifications
		if ((fdipType().equals(FORMATION_LMD) == false) && (fdipType().equals(FORMATION_FICTIVE) == false) && (fdipType().equals(FORMATION_CLASSIQUE) == false)) {
			throw new ScolFormationDiplomeException("Le type associe est inconnu");
		}
		if ((fdipSemestrialisation().equals(ORGANISATION_ANNUELLE) == false) && (fdipSemestrialisation().equals(ORGANISATION_SEMESTRIELLE) == false)) {
			throw new ScolFormationDiplomeException("L'organisation associee est inconnue");
		}
		if ((fdipDeliberation().equals(DELIBERATION_ANNUELLE) == false) && (fdipDeliberation().equals(DELIBERATION_SEMESTRIELLE) == false)) {
			throw new ScolFormationDiplomeException("La deliberation associee est inconnue");
		}
		if ((fdipMonoSemestre().equals(REPARTITION_MONO_SEMESTRE) == false) && (fdipMonoSemestre().equals(REPARTITION_MULTI_SEMESTRE) == false)) {
			throw new ScolFormationDiplomeException("La duree associee est inconnue");
		}
		if (fdipDepart().intValue() < 0) {
			throw new ScolFormationDiplomeException("Niveau depart doit etre superieur a 1");
		}
		if (fdipArrivee().intValue() < 0) {
			throw new ScolFormationDiplomeException("Niveau arrivee doit etre superieur a 1");
		}
		if (fdipArrivee().intValue() < fdipDepart().intValue()) {
			throw new ScolFormationDiplomeException("Revoir coherence entre le niveau depart et niveau arrivee");
		}
		if (this.habilitationDepart() != null) {
			if (this.habilitationArrivee() == null) {
				throw new ScolFormationDiplomeException("Revoir coherence pour habilitation : debut renseigne et fin non renseignee");
			}
			if (this.habilitationDepart().intValue() < 1900) {
				throw new ScolFormationDiplomeException("Habilitation debut doit etre superieure a 1900");
			}
			if (this.habilitationDepart().intValue() > 3000) {
				throw new ScolFormationDiplomeException("Habilitation debut doit etre inferieure a 3000");
			}
		}
		if (this.habilitationArrivee() != null) {
			if (this.habilitationDepart() == null) {
				throw new ScolFormationDiplomeException("Revoir coherence pour habilitation : debut non renseigne et fin renseignee");
			}
			if (this.habilitationArrivee().intValue() < 1900) {
				throw new ScolFormationDiplomeException("Habilitation fin doit etre superieure a 1900");
			}
			if (this.habilitationArrivee().intValue() > 3000) {
				throw new ScolFormationDiplomeException("Habilitation fin doit etre inferieure a 3000");
			}
		}
		if (((this.habilitationDepart() != null) && (this.habilitationArrivee() != null))
				&& (this.habilitationArrivee().intValue() < this.habilitationDepart().intValue())) {
			throw new ScolFormationDiplomeException("Revoir coherence entre l'habilitation debut et l'habilitation fin");
		}
		// particularites, actions
		if (this.codeDiplomeObligatoire() == true) {
			if (fdipCode() == null) {
				throw new ScolFormationDiplomeException("Code du diplome obligatoire");
			}
		} else {
			if (fdipCode() != null) {
				this.setFdipCode(null);
			}
		}
	}

	public void validateScolFormationDiplomeForUpdate() throws ScolFormationDiplomeException {
		// champs obligatoires
		if (fdipCode() == null) {
			throw new ScolFormationDiplomeException("Code du diplome obligatoire");
		}
		if (fdipType() == null) {
			throw new ScolFormationDiplomeException("Type du diplome obligatoire");
		}
		if (toFwkScolarite_ScolFormationDiplomeSise() == null) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de code SISE");
		}
		if ((toFwkScolarite_ScolFormationGrade() == null) && (toFwkScolarite_ScolFormationFiliere() == null)) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de grade/filiere");
		}
		if ((toFwkScolarite_ScolFormationDomaine() == null) && (toFwkScolarite_ScolFormationDepartement() == null)) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de domaine/departement");
		}
		if (toFwkScolarite_ScolFormationVocation() == null) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de vocation");
		}
		if (toFwkScolarix_VEtablissementScolarite() == null) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas d'etablissement");
		}
		if (toFwkScolarix_VComposanteScolarite() == null) {
			throw new ScolFormationDiplomeException("Le diplome n'a pas de composante");
		}
		if (toFwkScolarite_ScolFormationDiplomeSise().fdipCodeSise() == null) {
			throw new ScolFormationDiplomeException("Code SISE du diplome obligatoire");
		}
		if (toFwkScolarite_ScolFormationVocation().fvocCode() == null) {
			throw new ScolFormationDiplomeException("Vocation du diplome obligatoire");
		}
		if (fdipMention() == null) {
			throw new ScolFormationDiplomeException("Mention du diplome obligatoire");
		}
		if (fdipLibelle() == null) {
			throw new ScolFormationDiplomeException("Libelle du diplome obligatoire");
		}
		if (fdipAbreviation() == null) {
			throw new ScolFormationDiplomeException("Abreviation du diplome obligatoire");
		}
		if (fdipDepart() == null) {
			throw new ScolFormationDiplomeException("Niveau de depart du diplome obligatoire");
		}
		if (fdipArrivee() == null) {
			throw new ScolFormationDiplomeException("Niveau d'arrivee du diplome obligatoire");
		}
		if (toFwkScolarix_VEtablissementScolarite().cRne() == null) {
			throw new ScolFormationDiplomeException("Etablissement du diplome obligatoire");
		}
		// if (toFwkScolarix_VComposanteScolarite().cRne() == null) {
		// throw new ScolFormationDiplomeException("Composante du diplome obligatoire");
		// }
		if (fdipTypeDroit() == null) {
			throw new ScolFormationDiplomeException("Type des droits de scolarite du diplome obligatoire");
		}
		if (fdipSemestrialisation() == null) {
			throw new ScolFormationDiplomeException("Organisation du diplome obligatoire (semestrielle : Oui/Non)");
		}
		if (fdipDeliberation() == null) {
			throw new ScolFormationDiplomeException("Deliberation du diplome obligatoire (semestrielle : Oui/Non)");
		}
		if (fdipMonoSemestre() == null) {
			throw new ScolFormationDiplomeException("Duree du diplome obligatoire (mono semestre : Oui/Non)");
		}
		// champs obligatoires conditionnels
		if ((fdipType().equals(FORMATION_LMD) == true) || (fdipType().equals(FORMATION_FICTIVE) == true)) {
			if ((toFwkScolarite_ScolFormationGrade() == null) || (toFwkScolarite_ScolFormationGrade().fgraCode() == null)) {
				throw new ScolFormationDiplomeException("Grade obligatoire pour un diplome de type LMD / FICTIF");
			}
			if ((toFwkScolarite_ScolFormationDomaine() == null) || (toFwkScolarite_ScolFormationDomaine().fdomCode() == null)) {
				throw new ScolFormationDiplomeException("Domaine obligatoire pour un diplome de type LMD / FICTIF");
			}
			if (fdipSemestrialisation().equals(ORGANISATION_SEMESTRIELLE) == false) {
				throw new ScolFormationDiplomeException("L'organisation doit etre semestrielle pour un diplome de type LMD / FICTIF");
			}
			if (fdipDeliberation().equals(DELIBERATION_SEMESTRIELLE) == false) {
				throw new ScolFormationDiplomeException("La deliberation ne peut etre annuelle pour un diplome de type LMD / FICTIF");
			}
			if (fdipMonoSemestre().equals(REPARTITION_MULTI_SEMESTRE) == false) {
				throw new ScolFormationDiplomeException("La duree ne peut etre mono semestre pour un diplome de type LMD / FICTIF");
			}
		}
		if (fdipType().equals(FORMATION_CLASSIQUE) == true) {
			if ((toFwkScolarite_ScolFormationFiliere() == null) || (toFwkScolarite_ScolFormationFiliere().ffilCode() == null)) {
				throw new ScolFormationDiplomeException("Filiere obligatoire pour le diplome de type CLASSIQUE");
			}
			if ((toFwkScolarite_ScolFormationDepartement() == null) || (toFwkScolarite_ScolFormationDepartement().fdepCode() == null)) {
				throw new ScolFormationDiplomeException("Departement obligatoire pour le diplome de type CLASSIQUE");
			}
		}
		// verifications
		if ((fdipType().equals(FORMATION_LMD) == false) && (fdipType().equals(FORMATION_FICTIVE) == false) && (fdipType().equals(FORMATION_CLASSIQUE) == false)) {
			throw new ScolFormationDiplomeException("Le type associe est inconnu");
		}
		if ((fdipSemestrialisation().equals(ORGANISATION_ANNUELLE) == false) && (fdipSemestrialisation().equals(ORGANISATION_SEMESTRIELLE) == false)) {
			throw new ScolFormationDiplomeException("L'organisation associee est inconnue");
		}
		if ((fdipDeliberation().equals(DELIBERATION_ANNUELLE) == false) && (fdipDeliberation().equals(DELIBERATION_SEMESTRIELLE) == false)) {
			throw new ScolFormationDiplomeException("La deliberation associee est inconnue");
		}
		if ((fdipMonoSemestre().equals(REPARTITION_MONO_SEMESTRE) == false) && (fdipMonoSemestre().equals(REPARTITION_MULTI_SEMESTRE) == false)) {
			throw new ScolFormationDiplomeException("La duree associee est inconnue");
		}
		if (fdipDepart().intValue() < 0) {
			throw new ScolFormationDiplomeException("Niveau depart doit etre superieur a 1");
		}
		if (fdipArrivee().intValue() < 0) {
			throw new ScolFormationDiplomeException("Niveau arrivee doit etre superieur a 1");
		}
		if (fdipArrivee().intValue() < fdipDepart().intValue()) {
			throw new ScolFormationDiplomeException("Revoir coherence entre le niveau depart et niveau arrivee");
		}
		if (this.habilitationDepart() != null) {
			if (this.habilitationArrivee() == null) {
				throw new ScolFormationDiplomeException("Revoir coherence pour habilitation : debut renseigne et fin non renseignee");
			}
			if (this.habilitationDepart().intValue() < 1900) {
				throw new ScolFormationDiplomeException("Habilitation debut doit etre superieure a 1900");
			}
			if (this.habilitationDepart().intValue() > 3000) {
				throw new ScolFormationDiplomeException("Habilitation debut doit etre inferieure a 3000");
			}
		}
		if (this.habilitationArrivee() != null) {
			if (this.habilitationDepart() == null) {
				throw new ScolFormationDiplomeException("Revoir coherence pour habilitation : debut non renseigne et fin renseignee");
			}
			if (this.habilitationArrivee().intValue() < 1900) {
				throw new ScolFormationDiplomeException("Habilitation fin doit etre superieure a 1900");
			}
			if (this.habilitationArrivee().intValue() > 3000) {
				throw new ScolFormationDiplomeException("Habilitation fin doit etre inferieure a 3000");
			}
		}
		if (((this.habilitationDepart() != null) && (this.habilitationArrivee() != null))
				&& (this.habilitationArrivee().intValue() < this.habilitationDepart().intValue())) {
			throw new ScolFormationDiplomeException("Revoir coherence entre l'habilitation debut et l'habilitation fin");
		}
		// particularites, actions
	}

	/*
	 * Gestion du type d'action effectuee : AJOUTER MODIFIER
	 */
	public String actionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public boolean isActionInsert() {
		return this.actionType() != null && this.actionType().equals(EOScolFormationSpecialisation.ACTION_INSERT);
	}

	public boolean isActionUpdate() {
		return this.actionType() != null && this.actionType().equals(EOScolFormationSpecialisation.ACTION_UPDATE);
	}

	/*
	 * Gestion de la creation d'un diplome avec obligation de renseigner le code diplme
	 */
	public boolean codeDiplomeObligatoire() {
		return codeDiplomeObligatoire;
	}

	public void setCodeDiplomeObligatoire(boolean value) {
		codeDiplomeObligatoire = value;
	}

	/*
	 * Gestion des attributs supplementaires concernant la periode d'habilitation de la formation Ils seront utilises
	 * lors d l'appel a la procedure
	 */
	public Integer habilitationDepart() {
		return habilitationDepart;
	}

	public void setHabilitationDepart(Integer habilitationDepart) {
		this.habilitationDepart = habilitationDepart;
	}

	public Integer habilitationArrivee() {
		return habilitationArrivee;
	}

	public void setHabilitationArrivee(Integer habilitationArrivee) {
		this.habilitationArrivee = habilitationArrivee;
	}

	/**
	 * Retourne la filiere/grade du diplome selon que c'est un diplome ancienne generation (grade) ou LMD (filiere)
	 * 
	 * @return un IScolFormationGrade
	 */
	public IScolFormationGrade filiere() {
		if (FORMATION_CLASSIQUE.equals(fdipType())) {
			return toFwkScolarite_ScolFormationFiliere();
		} else {
			return toFwkScolarite_ScolFormationGrade();
		}
	}

	/**
	 * Retourne le domaine/departement du diplome selon que c'est un diplome ancienne generation (domaine) ou LMD
	 * (departement)
	 * 
	 * @return un IScolFormationDomaine
	 */
	public IScolFormationDomaine departement() {
		if (FORMATION_CLASSIQUE.equals(fdipType())) {
			return toFwkScolarite_ScolFormationDepartement();
		} else {
			return toFwkScolarite_ScolFormationDomaine();
		}
	}
}
