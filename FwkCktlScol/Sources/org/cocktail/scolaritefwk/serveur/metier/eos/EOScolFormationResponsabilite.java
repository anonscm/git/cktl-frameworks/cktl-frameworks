/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.metier.eos;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOScolFormationResponsabilite extends _EOScolFormationResponsabilite {

	public final static String TO_ONE_INDIVIDU = "toOneIndividu";

	public static EOQualifier QUAL_RESPONSABLE = ERXQ.equals(TO_FWK_SCOLARITE__SCOL_CONSTANTE_RESPONSABILITE_KEY + "." + EOScolConstanteResponsabilite.CRES_LIBELLE_KEY,
			EOScolConstanteResponsabilite.CRES_LIBELLE_RESPONSABLE);
	public static EOQualifier QUAL_SECRETAIRE = ERXQ.equals(TO_FWK_SCOLARITE__SCOL_CONSTANTE_RESPONSABILITE_KEY + "." + EOScolConstanteResponsabilite.CRES_LIBELLE_KEY,
			EOScolConstanteResponsabilite.CRES_LIBELLE_SECRETAIRE);
	public static EOQualifier QUAL_ENSEIGNANT = ERXQ.equals(TO_FWK_SCOLARITE__SCOL_CONSTANTE_RESPONSABILITE_KEY + "." + EOScolConstanteResponsabilite.CRES_LIBELLE_KEY,
			EOScolConstanteResponsabilite.CRES_LIBELLE_ENSEIGNANT);
	
	public EOScolFormationResponsabilite() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();

		NSArray<EOScolFormationResponsabilite> tmp = fetchAll(this.editingContext(), EOQualifier.qualifierWithQualifierFormat(TO_FWKPERS__INDIVIDU_KEY
				+ "=%@ AND " + EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "=%@ AND "
				+ EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "=%@", new NSArray<EOEnterpriseObject>(new EOEnterpriseObject[] {
				this.toOneIndividu(), this.toFwkScolarite_ScolFormationSpecialisation(), this.toFwkScolarite_ScolFormationAnnee() })));

		if ((tmp != null) && (tmp.size() > 0)) {
			throw new NSValidation.ValidationException("Impossible. Il est deja connu pour ce DIPLOME !");
		}
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
		NSArray<EOScolFormationResponsabilite> tmp = fetchAll(this.editingContext(), EOQualifier.qualifierWithQualifierFormat(TO_FWKPERS__INDIVIDU_KEY
				+ "=%@ AND " + EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "=%@ AND "
				+ EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "=%@ AND "
				+ EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_CONSTANTE_RESPONSABILITE_KEY + "=%@", new NSArray<EOEnterpriseObject>(
				new EOEnterpriseObject[] { this.toOneIndividu(), this.toFwkScolarite_ScolFormationSpecialisation(), this.toFwkScolarite_ScolFormationAnnee(),
						this.toFwkScolarite_ScolConstanteResponsabilite() })));

		if ((tmp != null) && (tmp.size() > 0)) {
			throw new NSValidation.ValidationException("Impossible. Il est deja "+this.toFwkScolarite_ScolConstanteResponsabilite().cresLibelle()+" pour ce DIPLOME !");
		}
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appel̩e.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (this.toOneIndividu() == null) {
			throw new NSValidation.ValidationException("L'individu est obligatoire !!");
		}
		if (this.toFwkScolarite_ScolConstanteResponsabilite() == null) {
			throw new NSValidation.ValidationException("Le type de responsabilité est obligatoire !!");
		}
		if (this.toFwkScolarite_ScolFormationAnnee() == null) {
			throw new NSValidation.ValidationException("L'année est obligatoire !!");
		}

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public EOIndividu toOneIndividu() {
		if ((toFwkpers_Individu() != null) && (toFwkpers_Individu().size() > 0)) {
			return (EOIndividu) toFwkpers_Individu().lastObject();
		}
		return null;
	}

	public void setToOneIndividu(EOIndividu ind) {
		addToToFwkpers_IndividuRelationship(ind);
	}

}
