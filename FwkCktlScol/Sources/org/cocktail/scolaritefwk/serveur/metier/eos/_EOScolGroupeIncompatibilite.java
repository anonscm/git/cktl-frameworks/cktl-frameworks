/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolGroupeIncompatibilite.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolGroupeIncompatibilite extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolGroupeIncompatibilite";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_GROUPE_INCOMPATIBILITE";

	//Attributes



	// Non visible attributes
	public static final String GGRP_KEY1_KEY = "ggrpKey1";
	public static final String GGRP_KEY2_KEY = "ggrpKey2";

	// Colkeys

	// Non visible colkeys
	public static final String GGRP_KEY1_COLKEY = "GGRP_KEY_1";
	public static final String GGRP_KEY2_COLKEY = "GGRP_KEY_2";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp> TO_FWK_SCOLARITE__SCOL_GROUPE_GRP1 = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp>("toFwkScolarite_ScolGroupeGrp1");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp> TO_FWK_SCOLARITE__SCOL_GROUPE_GRP2 = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp>("toFwkScolarite_ScolGroupeGrp2");

	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_GRP1_KEY = "toFwkScolarite_ScolGroupeGrp1";
	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_GRP2_KEY = "toFwkScolarite_ScolGroupeGrp2";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolGroupeIncompatibilite with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param toFwkScolarite_ScolGroupeGrp1
	 * @param toFwkScolarite_ScolGroupeGrp2
	 * @return EOScolGroupeIncompatibilite
	 */
	public static EOScolGroupeIncompatibilite create(EOEditingContext editingContext, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp toFwkScolarite_ScolGroupeGrp1, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp toFwkScolarite_ScolGroupeGrp2) {
		EOScolGroupeIncompatibilite eo = (EOScolGroupeIncompatibilite) createAndInsertInstance(editingContext);
		eo.setToFwkScolarite_ScolGroupeGrp1Relationship(toFwkScolarite_ScolGroupeGrp1);
		eo.setToFwkScolarite_ScolGroupeGrp2Relationship(toFwkScolarite_ScolGroupeGrp2);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolGroupeIncompatibilite.
	 *
	 * @param editingContext
	 * @return EOScolGroupeIncompatibilite
	 */
	public static EOScolGroupeIncompatibilite create(EOEditingContext editingContext) {
		EOScolGroupeIncompatibilite eo = (EOScolGroupeIncompatibilite) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolGroupeIncompatibilite localInstanceIn(EOEditingContext editingContext) {
		EOScolGroupeIncompatibilite localInstance = (EOScolGroupeIncompatibilite) localInstanceOfObject(editingContext, (EOScolGroupeIncompatibilite) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolGroupeIncompatibilite localInstanceIn(EOEditingContext editingContext, EOScolGroupeIncompatibilite eo) {
		EOScolGroupeIncompatibilite localInstance = (eo == null) ? null : (EOScolGroupeIncompatibilite) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods


	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp toFwkScolarite_ScolGroupeGrp1() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp)storedValueForKey("toFwkScolarite_ScolGroupeGrp1");
	}

	public void setToFwkScolarite_ScolGroupeGrp1Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp oldValue = toFwkScolarite_ScolGroupeGrp1();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolGroupeGrp1");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolGroupeGrp1");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp toFwkScolarite_ScolGroupeGrp2() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp)storedValueForKey("toFwkScolarite_ScolGroupeGrp2");
	}

	public void setToFwkScolarite_ScolGroupeGrp2Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp oldValue = toFwkScolarite_ScolGroupeGrp2();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolGroupeGrp2");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolGroupeGrp2");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolGroupeIncompatibilite.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolGroupeIncompatibilite.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolGroupeIncompatibilite)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolGroupeIncompatibilite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolGroupeIncompatibilite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolGroupeIncompatibilite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolGroupeIncompatibilite)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolGroupeIncompatibilite fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolGroupeIncompatibilite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolGroupeIncompatibilite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolGroupeIncompatibilite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolGroupeIncompatibilite)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolGroupeIncompatibilite fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolGroupeIncompatibilite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolGroupeIncompatibilite eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolGroupeIncompatibilite ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolGroupeIncompatibilite createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolGroupeIncompatibilite.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolGroupeIncompatibilite.ENTITY_NAME + "' !");
		}
		else {
			EOScolGroupeIncompatibilite object = (EOScolGroupeIncompatibilite) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolGroupeIncompatibilite localInstanceOfObject(EOEditingContext ec, EOScolGroupeIncompatibilite object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolGroupeIncompatibilite " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolGroupeIncompatibilite) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
