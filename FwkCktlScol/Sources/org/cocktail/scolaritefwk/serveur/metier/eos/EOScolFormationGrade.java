/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.metier.eos;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.interfaces.IScolFormationGrade;

import com.webobjects.foundation.NSValidation;

public class EOScolFormationGrade extends _EOScolFormationGrade implements IScolFormationGrade {

	// Valeurs possibles pour FGRA_VALIDITE
	public static final String GRADE_VALIDE = "O";
	public static final String GRADE_INVALIDE = "N";

	public static final String TRI_KEY = "tri";

	public EOScolFormationGrade() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appel̩e.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public String code() {
		return fgraCode();
	}

	public String libelle() {
		return fgraLibelle();
	}

	public String abreviation() {
		return fgraAbreviation();
	}

	public Boolean validite() {
		return Boolean.valueOf(StringCtrl.toBool(fgraValidite()));
	}

	public boolean isValide() {
		return this.fgraValidite() != null && this.fgraValidite().equals(EOScolFormationGrade.GRADE_VALIDE);
	}

	public boolean isInvalide() {
		return this.fgraValidite() != null && this.fgraValidite().equals(EOScolFormationGrade.GRADE_INVALIDE);
	}

	public boolean isLmd() {
		return true;
	}
	
	public String tri() {
		if (code() == null) {
			return "";
		}
		if (code().equals("L")) {
			return "a";
		}
		if (code().equals("M")) {
			return "b";
		}
		if (code().equals("D")) {
			return "c";
		}
		return "d";
	}

}
