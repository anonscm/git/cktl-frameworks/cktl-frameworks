/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteUeX.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteUeX extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteUeX";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_UE_X";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "muxKey";

	public static final ERXKey<String> MUX_CODE = new ERXKey<String>("muxCode");
	public static final ERXKey<java.math.BigDecimal> MUX_HORAIRE_ETU = new ERXKey<java.math.BigDecimal>("muxHoraireEtu");
	public static final ERXKey<String> MUX_LIBELLE = new ERXKey<String>("muxLibelle");
	public static final ERXKey<java.math.BigDecimal> MUX_POINTS = new ERXKey<java.math.BigDecimal>("muxPoints");

	public static final String MUX_CODE_KEY = "muxCode";
	public static final String MUX_HORAIRE_ETU_KEY = "muxHoraireEtu";
	public static final String MUX_LIBELLE_KEY = "muxLibelle";
	public static final String MUX_POINTS_KEY = "muxPoints";

	// Non visible attributes
	public static final String CLOC_KEY_KEY = "clocKey";
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MUX_KEY_KEY = "muxKey";

	// Colkeys
	public static final String MUX_CODE_COLKEY = "MUX_CODE";
	public static final String MUX_HORAIRE_ETU_COLKEY = "MUX_HORAIRE_ETU";
	public static final String MUX_LIBELLE_COLKEY = "MUX_LIBELLE";
	public static final String MUX_POINTS_COLKEY = "MUX_POINTS";

	// Non visible colkeys
	public static final String CLOC_KEY_COLKEY = "CLOC_KEY";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MUX_KEY_COLKEY = "MUX_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation> TO_FWK_SCOLARITE__SCOL_CONSTANTE_LOCALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation>("toFwkScolarite_ScolConstanteLocalisation");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UE_XS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX>("toFwkScolarite_ScolMaquetteRepartitionUeXs");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues> TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_X_LANGUESS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues>("toFwkScolarite_ScolMaquetteUeXLanguess");

	public static final String TO_FWK_SCOLARITE__SCOL_CONSTANTE_LOCALISATION_KEY = "toFwkScolarite_ScolConstanteLocalisation";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UE_XS_KEY = "toFwkScolarite_ScolMaquetteRepartitionUeXs";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_X_LANGUESS_KEY = "toFwkScolarite_ScolMaquetteUeXLanguess";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteUeX with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param muxCode
	 * @param muxHoraireEtu
	 * @param muxPoints
	 * @param toFwkScolarite_ScolConstanteLocalisation
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @return EOScolMaquetteUeX
	 */
	public static EOScolMaquetteUeX create(EOEditingContext editingContext, String muxCode, java.math.BigDecimal muxHoraireEtu, java.math.BigDecimal muxPoints, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation toFwkScolarite_ScolConstanteLocalisation, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee) {
		EOScolMaquetteUeX eo = (EOScolMaquetteUeX) createAndInsertInstance(editingContext);
		eo.setMuxCode(muxCode);
		eo.setMuxHoraireEtu(muxHoraireEtu);
		eo.setMuxPoints(muxPoints);
		eo.setToFwkScolarite_ScolConstanteLocalisationRelationship(toFwkScolarite_ScolConstanteLocalisation);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteUeX.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteUeX
	 */
	public static EOScolMaquetteUeX create(EOEditingContext editingContext) {
		EOScolMaquetteUeX eo = (EOScolMaquetteUeX) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteUeX localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteUeX localInstance = (EOScolMaquetteUeX) localInstanceOfObject(editingContext, (EOScolMaquetteUeX) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteUeX localInstanceIn(EOEditingContext editingContext, EOScolMaquetteUeX eo) {
		EOScolMaquetteUeX localInstance = (eo == null) ? null : (EOScolMaquetteUeX) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String muxCode() {
		return (String) storedValueForKey("muxCode");
	}

	public void setMuxCode(String value) {
		takeStoredValueForKey(value, "muxCode");
	}
	public java.math.BigDecimal muxHoraireEtu() {
		return (java.math.BigDecimal) storedValueForKey("muxHoraireEtu");
	}

	public void setMuxHoraireEtu(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "muxHoraireEtu");
	}
	public String muxLibelle() {
		return (String) storedValueForKey("muxLibelle");
	}

	public void setMuxLibelle(String value) {
		takeStoredValueForKey(value, "muxLibelle");
	}
	public java.math.BigDecimal muxPoints() {
		return (java.math.BigDecimal) storedValueForKey("muxPoints");
	}

	public void setMuxPoints(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "muxPoints");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation toFwkScolarite_ScolConstanteLocalisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation)storedValueForKey("toFwkScolarite_ScolConstanteLocalisation");
	}

	public void setToFwkScolarite_ScolConstanteLocalisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation oldValue = toFwkScolarite_ScolConstanteLocalisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolConstanteLocalisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolConstanteLocalisation");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public NSArray toFwkScolarite_ScolMaquetteRepartitionUeXs() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionUeXs");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUeXs(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionUeXs(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUeXs(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionUeXs(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUeXs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_X_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionUeXs();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUeXs");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUeXs");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX createToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionUeX");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionUeXs");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUeXs");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionUeXsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionUeXs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteUeXLanguess() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteUeXLanguess");
	}

	public NSArray toFwkScolarite_ScolMaquetteUeXLanguess(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteUeXLanguess(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteUeXLanguess(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteUeXLanguess(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteUeXLanguess(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_X_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteUeXLanguess();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteUeXLanguessRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteUeXLanguess");
	}

	public void removeFromToFwkScolarite_ScolMaquetteUeXLanguessRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteUeXLanguess");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues createToFwkScolarite_ScolMaquetteUeXLanguessRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteUeXLangues");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteUeXLanguess");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteUeXLanguessRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteUeXLanguess");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteUeXLanguessRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteUeXLanguess().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteUeXLanguessRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUeXLangues)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteUeX.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteUeX.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteUeX)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteUeX fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteUeX fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteUeX eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteUeX)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteUeX fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteUeX fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteUeX fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteUeX eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteUeX)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteUeX fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteUeX fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteUeX eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteUeX ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteUeX createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteUeX.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteUeX.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteUeX object = (EOScolMaquetteUeX) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteUeX localInstanceOfObject(EOEditingContext ec, EOScolMaquetteUeX object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteUeX " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteUeX) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
