/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolArreteLigne.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolArreteLigne extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolArreteLigne";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_ARRETE_LIGNE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "aligKey";

	public static final ERXKey<Integer> ALIG_DEBUT = new ERXKey<Integer>("aligDebut");
	public static final ERXKey<String> ALIG_DOSSIER = new ERXKey<String>("aligDossier");
	public static final ERXKey<Integer> ALIG_DUREE = new ERXKey<Integer>("aligDuree");
	public static final ERXKey<Integer> ALIG_FIN = new ERXKey<Integer>("aligFin");

	public static final String ALIG_DEBUT_KEY = "aligDebut";
	public static final String ALIG_DOSSIER_KEY = "aligDossier";
	public static final String ALIG_DUREE_KEY = "aligDuree";
	public static final String ALIG_FIN_KEY = "aligFin";

	// Non visible attributes
	public static final String AENT_KEY_KEY = "aentKey";
	public static final String ALIG_KEY_KEY = "aligKey";
	public static final String FDIP_CODE_KEY = "fdipCode";

	// Colkeys
	public static final String ALIG_DEBUT_COLKEY = "ALIG_DEBUT";
	public static final String ALIG_DOSSIER_COLKEY = "ALIG_DOSSIER";
	public static final String ALIG_DUREE_COLKEY = "ALIG_DUREE";
	public static final String ALIG_FIN_COLKEY = "ALIG_FIN";

	// Non visible colkeys
	public static final String AENT_KEY_COLKEY = "AENT_KEY";
	public static final String ALIG_KEY_COLKEY = "ALIG_KEY";
	public static final String FDIP_CODE_COLKEY = "FDIP_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolArreteEntete> TO_FWK_SCOLARITE__SCOL_ARRETE_ENTETE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolArreteEntete>("toFwkScolarite_ScolArreteEntete");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome> TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome>("toFwkScolarite_ScolFormationDiplome");

	public static final String TO_FWK_SCOLARITE__SCOL_ARRETE_ENTETE_KEY = "toFwkScolarite_ScolArreteEntete";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY = "toFwkScolarite_ScolFormationDiplome";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolArreteLigne with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param aligDebut
	 * @param aligDossier
	 * @param aligDuree
	 * @param aligFin
	 * @param toFwkScolarite_ScolArreteEntete
	 * @param toFwkScolarite_ScolFormationDiplome
	 * @return EOScolArreteLigne
	 */
	public static EOScolArreteLigne create(EOEditingContext editingContext, Integer aligDebut, String aligDossier, Integer aligDuree, Integer aligFin, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolArreteEntete toFwkScolarite_ScolArreteEntete, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toFwkScolarite_ScolFormationDiplome) {
		EOScolArreteLigne eo = (EOScolArreteLigne) createAndInsertInstance(editingContext);
		eo.setAligDebut(aligDebut);
		eo.setAligDossier(aligDossier);
		eo.setAligDuree(aligDuree);
		eo.setAligFin(aligFin);
		eo.setToFwkScolarite_ScolArreteEnteteRelationship(toFwkScolarite_ScolArreteEntete);
		eo.setToFwkScolarite_ScolFormationDiplomeRelationship(toFwkScolarite_ScolFormationDiplome);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolArreteLigne.
	 *
	 * @param editingContext
	 * @return EOScolArreteLigne
	 */
	public static EOScolArreteLigne create(EOEditingContext editingContext) {
		EOScolArreteLigne eo = (EOScolArreteLigne) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolArreteLigne localInstanceIn(EOEditingContext editingContext) {
		EOScolArreteLigne localInstance = (EOScolArreteLigne) localInstanceOfObject(editingContext, (EOScolArreteLigne) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolArreteLigne localInstanceIn(EOEditingContext editingContext, EOScolArreteLigne eo) {
		EOScolArreteLigne localInstance = (eo == null) ? null : (EOScolArreteLigne) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer aligDebut() {
		return (Integer) storedValueForKey("aligDebut");
	}

	public void setAligDebut(Integer value) {
		takeStoredValueForKey(value, "aligDebut");
	}
	public String aligDossier() {
		return (String) storedValueForKey("aligDossier");
	}

	public void setAligDossier(String value) {
		takeStoredValueForKey(value, "aligDossier");
	}
	public Integer aligDuree() {
		return (Integer) storedValueForKey("aligDuree");
	}

	public void setAligDuree(Integer value) {
		takeStoredValueForKey(value, "aligDuree");
	}
	public Integer aligFin() {
		return (Integer) storedValueForKey("aligFin");
	}

	public void setAligFin(Integer value) {
		takeStoredValueForKey(value, "aligFin");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolArreteEntete toFwkScolarite_ScolArreteEntete() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolArreteEntete)storedValueForKey("toFwkScolarite_ScolArreteEntete");
	}

	public void setToFwkScolarite_ScolArreteEnteteRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolArreteEntete value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolArreteEntete oldValue = toFwkScolarite_ScolArreteEntete();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolArreteEntete");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolArreteEntete");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toFwkScolarite_ScolFormationDiplome() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome)storedValueForKey("toFwkScolarite_ScolFormationDiplome");
	}

	public void setToFwkScolarite_ScolFormationDiplomeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome oldValue = toFwkScolarite_ScolFormationDiplome();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDiplome");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDiplome");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolArreteLigne.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolArreteLigne.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolArreteLigne)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolArreteLigne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolArreteLigne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolArreteLigne eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolArreteLigne)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolArreteLigne fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolArreteLigne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolArreteLigne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolArreteLigne eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolArreteLigne)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolArreteLigne fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolArreteLigne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolArreteLigne eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolArreteLigne ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolArreteLigne createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolArreteLigne.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolArreteLigne.ENTITY_NAME + "' !");
		}
		else {
			EOScolArreteLigne object = (EOScolArreteLigne) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolArreteLigne localInstanceOfObject(EOEditingContext ec, EOScolArreteLigne object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolArreteLigne " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolArreteLigne) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
