/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteReportJournal.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteReportJournal extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteReportJournal";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_REPORT_JOURNAL";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mrjoKey";

	public static final ERXKey<Integer> MRJO_ANNEE_SUIVIE = new ERXKey<Integer>("mrjoAnneeSuivie");
	public static final ERXKey<Integer> MRJO_CLE = new ERXKey<Integer>("mrjoCle");
	public static final ERXKey<String> MRJO_MOTIF = new ERXKey<String>("mrjoMotif");
	public static final ERXKey<Integer> MRJO_SEMESTRE = new ERXKey<Integer>("mrjoSemestre");
	public static final ERXKey<String> MRJO_TYPE = new ERXKey<String>("mrjoType");

	public static final String MRJO_ANNEE_SUIVIE_KEY = "mrjoAnneeSuivie";
	public static final String MRJO_CLE_KEY = "mrjoCle";
	public static final String MRJO_MOTIF_KEY = "mrjoMotif";
	public static final String MRJO_SEMESTRE_KEY = "mrjoSemestre";
	public static final String MRJO_TYPE_KEY = "mrjoType";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String MRJO_KEY_KEY = "mrjoKey";

	// Colkeys
	public static final String MRJO_ANNEE_SUIVIE_COLKEY = "MRJO_ANNEE_SUIVIE";
	public static final String MRJO_CLE_COLKEY = "MRJO_CLE";
	public static final String MRJO_MOTIF_COLKEY = "MRJO_MOTIF";
	public static final String MRJO_SEMESTRE_COLKEY = "MRJO_SEMESTRE";
	public static final String MRJO_TYPE_COLKEY = "MRJO_TYPE";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String MRJO_KEY_COLKEY = "MRJO_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant> TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ETUDIANT = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant>("toFwkScolarite_ScolInscriptionEtudiant");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";
	public static final String TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ETUDIANT_KEY = "toFwkScolarite_ScolInscriptionEtudiant";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteReportJournal with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mrjoAnneeSuivie
	 * @param mrjoCle
	 * @param mrjoMotif
	 * @param mrjoSemestre
	 * @param mrjoType
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @param toFwkScolarite_ScolInscriptionEtudiant
	 * @return EOScolMaquetteReportJournal
	 */
	public static EOScolMaquetteReportJournal create(EOEditingContext editingContext, Integer mrjoAnneeSuivie, Integer mrjoCle, String mrjoMotif, Integer mrjoSemestre, String mrjoType, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant toFwkScolarite_ScolInscriptionEtudiant) {
		EOScolMaquetteReportJournal eo = (EOScolMaquetteReportJournal) createAndInsertInstance(editingContext);
		eo.setMrjoAnneeSuivie(mrjoAnneeSuivie);
		eo.setMrjoCle(mrjoCle);
		eo.setMrjoMotif(mrjoMotif);
		eo.setMrjoSemestre(mrjoSemestre);
		eo.setMrjoType(mrjoType);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		eo.setToFwkScolarite_ScolInscriptionEtudiantRelationship(toFwkScolarite_ScolInscriptionEtudiant);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteReportJournal.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteReportJournal
	 */
	public static EOScolMaquetteReportJournal create(EOEditingContext editingContext) {
		EOScolMaquetteReportJournal eo = (EOScolMaquetteReportJournal) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteReportJournal localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteReportJournal localInstance = (EOScolMaquetteReportJournal) localInstanceOfObject(editingContext, (EOScolMaquetteReportJournal) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteReportJournal localInstanceIn(EOEditingContext editingContext, EOScolMaquetteReportJournal eo) {
		EOScolMaquetteReportJournal localInstance = (eo == null) ? null : (EOScolMaquetteReportJournal) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mrjoAnneeSuivie() {
		return (Integer) storedValueForKey("mrjoAnneeSuivie");
	}

	public void setMrjoAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "mrjoAnneeSuivie");
	}
	public Integer mrjoCle() {
		return (Integer) storedValueForKey("mrjoCle");
	}

	public void setMrjoCle(Integer value) {
		takeStoredValueForKey(value, "mrjoCle");
	}
	public String mrjoMotif() {
		return (String) storedValueForKey("mrjoMotif");
	}

	public void setMrjoMotif(String value) {
		takeStoredValueForKey(value, "mrjoMotif");
	}
	public Integer mrjoSemestre() {
		return (Integer) storedValueForKey("mrjoSemestre");
	}

	public void setMrjoSemestre(Integer value) {
		takeStoredValueForKey(value, "mrjoSemestre");
	}
	public String mrjoType() {
		return (String) storedValueForKey("mrjoType");
	}

	public void setMrjoType(String value) {
		takeStoredValueForKey(value, "mrjoType");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant toFwkScolarite_ScolInscriptionEtudiant() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant)storedValueForKey("toFwkScolarite_ScolInscriptionEtudiant");
	}

	public void setToFwkScolarite_ScolInscriptionEtudiantRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant oldValue = toFwkScolarite_ScolInscriptionEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolInscriptionEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolInscriptionEtudiant");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteReportJournal.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteReportJournal.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteReportJournal)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteReportJournal fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteReportJournal fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteReportJournal eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteReportJournal)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteReportJournal fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteReportJournal fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteReportJournal fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteReportJournal eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteReportJournal)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteReportJournal fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteReportJournal fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteReportJournal eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteReportJournal ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteReportJournal createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteReportJournal.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteReportJournal.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteReportJournal object = (EOScolMaquetteReportJournal) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteReportJournal localInstanceOfObject(EOEditingContext ec, EOScolMaquetteReportJournal object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteReportJournal " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteReportJournal) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
