/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTexteMultilingue.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOTexteMultilingue extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_TexteMultilingue";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TEXTE_MULTILINGUE";

	//Attributes

	public static final ERXKey<Integer> TMU_ORDRE = new ERXKey<Integer>("tmuOrdre");
	public static final ERXKey<String> TMU_TEXTE = new ERXKey<String>("tmuTexte");

	public static final String TMU_ORDRE_KEY = "tmuOrdre";
	public static final String TMU_TEXTE_KEY = "tmuTexte";

	// Non visible attributes
	public static final String C_LANGUE_KEY = "cLangue";
	public static final String C_PAYS_KEY = "cPays";

	// Colkeys
	public static final String TMU_ORDRE_COLKEY = "TMU_ORDRE";
	public static final String TMU_TEXTE_COLKEY = "TMU_TEXTE";

	// Non visible colkeys
	public static final String C_LANGUE_COLKEY = "C_LANGUE";
	public static final String C_PAYS_COLKEY = "C_PAYS";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> TO_FWKPERS__LANGUE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>("toFwkpers_Langue");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_FWKPERS__PAYS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toFwkpers_Pays");

	public static final String TO_FWKPERS__LANGUE_KEY = "toFwkpers_Langue";
	public static final String TO_FWKPERS__PAYS_KEY = "toFwkpers_Pays";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOTexteMultilingue with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param tmuOrdre
	 * @param toFwkpers_Langue
	 * @param toFwkpers_Pays
	 * @return EOTexteMultilingue
	 */
	public static EOTexteMultilingue create(EOEditingContext editingContext, Integer tmuOrdre, org.cocktail.fwkcktlpersonne.common.metier.EOLangue toFwkpers_Langue, org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays) {
		EOTexteMultilingue eo = (EOTexteMultilingue) createAndInsertInstance(editingContext);
		eo.setTmuOrdre(tmuOrdre);
		eo.setToFwkpers_LangueRelationship(toFwkpers_Langue);
		eo.setToFwkpers_PaysRelationship(toFwkpers_Pays);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOTexteMultilingue.
	 *
	 * @param editingContext
	 * @return EOTexteMultilingue
	 */
	public static EOTexteMultilingue create(EOEditingContext editingContext) {
		EOTexteMultilingue eo = (EOTexteMultilingue) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOTexteMultilingue localInstanceIn(EOEditingContext editingContext) {
		EOTexteMultilingue localInstance = (EOTexteMultilingue) localInstanceOfObject(editingContext, (EOTexteMultilingue) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOTexteMultilingue localInstanceIn(EOEditingContext editingContext, EOTexteMultilingue eo) {
		EOTexteMultilingue localInstance = (eo == null) ? null : (EOTexteMultilingue) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer tmuOrdre() {
		return (Integer) storedValueForKey("tmuOrdre");
	}

	public void setTmuOrdre(Integer value) {
		takeStoredValueForKey(value, "tmuOrdre");
	}
	public String tmuTexte() {
		return (String) storedValueForKey("tmuTexte");
	}

	public void setTmuTexte(String value) {
		takeStoredValueForKey(value, "tmuTexte");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOLangue toFwkpers_Langue() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOLangue)storedValueForKey("toFwkpers_Langue");
	}

	public void setToFwkpers_LangueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOLangue value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOLangue oldValue = toFwkpers_Langue();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Langue");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Langue");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toFwkpers_Pays() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey("toFwkpers_Pays");
	}

	public void setToFwkpers_PaysRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toFwkpers_Pays();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Pays");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Pays");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOTexteMultilingue.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOTexteMultilingue.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOTexteMultilingue)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOTexteMultilingue fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTexteMultilingue fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOTexteMultilingue eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOTexteMultilingue)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOTexteMultilingue fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOTexteMultilingue fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOTexteMultilingue fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOTexteMultilingue eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOTexteMultilingue)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOTexteMultilingue fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOTexteMultilingue fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOTexteMultilingue eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOTexteMultilingue ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOTexteMultilingue createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOTexteMultilingue.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOTexteMultilingue.ENTITY_NAME + "' !");
		}
		else {
			EOTexteMultilingue object = (EOTexteMultilingue) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOTexteMultilingue localInstanceOfObject(EOEditingContext ec, EOTexteMultilingue object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOTexteMultilingue " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOTexteMultilingue) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
