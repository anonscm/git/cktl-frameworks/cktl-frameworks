/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolGroupeGrp.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolGroupeGrp extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolGroupeGrp";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_GROUPE_GRP";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "ggrpKey";

	public static final ERXKey<String> GGRP_CODE = new ERXKey<String>("ggrpCode");
	public static final ERXKey<Integer> GGRP_DATE_DEBUT = new ERXKey<Integer>("ggrpDateDebut");
	public static final ERXKey<Integer> GGRP_DATE_FIN = new ERXKey<Integer>("ggrpDateFin");
	public static final ERXKey<Integer> GGRP_INT_CAPACITE = new ERXKey<Integer>("ggrpIntCapacite");
	public static final ERXKey<String> GGRP_LIBELLE = new ERXKey<String>("ggrpLibelle");
	public static final ERXKey<Integer> GGRP_MAX_CAPACITE = new ERXKey<Integer>("ggrpMaxCapacite");
	public static final ERXKey<String> GGRP_MAX_TEMOIN = new ERXKey<String>("ggrpMaxTemoin");

	public static final String GGRP_CODE_KEY = "ggrpCode";
	public static final String GGRP_DATE_DEBUT_KEY = "ggrpDateDebut";
	public static final String GGRP_DATE_FIN_KEY = "ggrpDateFin";
	public static final String GGRP_INT_CAPACITE_KEY = "ggrpIntCapacite";
	public static final String GGRP_LIBELLE_KEY = "ggrpLibelle";
	public static final String GGRP_MAX_CAPACITE_KEY = "ggrpMaxCapacite";
	public static final String GGRP_MAX_TEMOIN_KEY = "ggrpMaxTemoin";

	// Non visible attributes
	public static final String GCOL_KEY_KEY = "gcolKey";
	public static final String GGRP_KEY_KEY = "ggrpKey";

	// Colkeys
	public static final String GGRP_CODE_COLKEY = "GGRP_CODE";
	public static final String GGRP_DATE_DEBUT_COLKEY = "GGRP_DATE_DEBUT";
	public static final String GGRP_DATE_FIN_COLKEY = "GGRP_DATE_FIN";
	public static final String GGRP_INT_CAPACITE_COLKEY = "GGRP_INT_CAPACITE";
	public static final String GGRP_LIBELLE_COLKEY = "GGRP_LIBELLE";
	public static final String GGRP_MAX_CAPACITE_COLKEY = "GGRP_MAX_CAPACITE";
	public static final String GGRP_MAX_TEMOIN_COLKEY = "GGRP_MAX_TEMOIN";

	// Non visible colkeys
	public static final String GCOL_KEY_COLKEY = "GCOL_KEY";
	public static final String GGRP_KEY_COLKEY = "GGRP_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeCollection> TO_FWK_SCOLARITE__SCOL_GROUPE_COLLECTION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeCollection>("toFwkScolarite_ScolGroupeCollection");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion> TO_FWK_SCOLARITE__SCOL_GROUPE_INCLUSIONS1 = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion>("toFwkScolarite_ScolGroupeInclusions1");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion> TO_FWK_SCOLARITE__SCOL_GROUPE_INCLUSIONS2 = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion>("toFwkScolarite_ScolGroupeInclusions2");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite> TO_FWK_SCOLARITE__SCOL_GROUPE_INCOMPATIBILITES1 = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite>("toFwkScolarite_ScolGroupeIncompatibilites1");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite> TO_FWK_SCOLARITE__SCOL_GROUPE_INCOMPATIBILITES2 = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite>("toFwkScolarite_ScolGroupeIncompatibilites2");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet> TO_FWK_SCOLARITE__SCOL_GROUPE_OBJETS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet>("toFwkScolarite_ScolGroupeObjets");

	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_COLLECTION_KEY = "toFwkScolarite_ScolGroupeCollection";
	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_INCLUSIONS1_KEY = "toFwkScolarite_ScolGroupeInclusions1";
	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_INCLUSIONS2_KEY = "toFwkScolarite_ScolGroupeInclusions2";
	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_INCOMPATIBILITES1_KEY = "toFwkScolarite_ScolGroupeIncompatibilites1";
	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_INCOMPATIBILITES2_KEY = "toFwkScolarite_ScolGroupeIncompatibilites2";
	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_OBJETS_KEY = "toFwkScolarite_ScolGroupeObjets";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolGroupeGrp with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param ggrpCode
	 * @param ggrpDateDebut
	 * @param ggrpMaxTemoin
	 * @param toFwkScolarite_ScolGroupeCollection
	 * @return EOScolGroupeGrp
	 */
	public static EOScolGroupeGrp create(EOEditingContext editingContext, String ggrpCode, Integer ggrpDateDebut, String ggrpMaxTemoin, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeCollection toFwkScolarite_ScolGroupeCollection) {
		EOScolGroupeGrp eo = (EOScolGroupeGrp) createAndInsertInstance(editingContext);
		eo.setGgrpCode(ggrpCode);
		eo.setGgrpDateDebut(ggrpDateDebut);
		eo.setGgrpMaxTemoin(ggrpMaxTemoin);
		eo.setToFwkScolarite_ScolGroupeCollectionRelationship(toFwkScolarite_ScolGroupeCollection);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolGroupeGrp.
	 *
	 * @param editingContext
	 * @return EOScolGroupeGrp
	 */
	public static EOScolGroupeGrp create(EOEditingContext editingContext) {
		EOScolGroupeGrp eo = (EOScolGroupeGrp) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolGroupeGrp localInstanceIn(EOEditingContext editingContext) {
		EOScolGroupeGrp localInstance = (EOScolGroupeGrp) localInstanceOfObject(editingContext, (EOScolGroupeGrp) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolGroupeGrp localInstanceIn(EOEditingContext editingContext, EOScolGroupeGrp eo) {
		EOScolGroupeGrp localInstance = (eo == null) ? null : (EOScolGroupeGrp) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String ggrpCode() {
		return (String) storedValueForKey("ggrpCode");
	}

	public void setGgrpCode(String value) {
		takeStoredValueForKey(value, "ggrpCode");
	}
	public Integer ggrpDateDebut() {
		return (Integer) storedValueForKey("ggrpDateDebut");
	}

	public void setGgrpDateDebut(Integer value) {
		takeStoredValueForKey(value, "ggrpDateDebut");
	}
	public Integer ggrpDateFin() {
		return (Integer) storedValueForKey("ggrpDateFin");
	}

	public void setGgrpDateFin(Integer value) {
		takeStoredValueForKey(value, "ggrpDateFin");
	}
	public Integer ggrpIntCapacite() {
		return (Integer) storedValueForKey("ggrpIntCapacite");
	}

	public void setGgrpIntCapacite(Integer value) {
		takeStoredValueForKey(value, "ggrpIntCapacite");
	}
	public String ggrpLibelle() {
		return (String) storedValueForKey("ggrpLibelle");
	}

	public void setGgrpLibelle(String value) {
		takeStoredValueForKey(value, "ggrpLibelle");
	}
	public Integer ggrpMaxCapacite() {
		return (Integer) storedValueForKey("ggrpMaxCapacite");
	}

	public void setGgrpMaxCapacite(Integer value) {
		takeStoredValueForKey(value, "ggrpMaxCapacite");
	}
	public String ggrpMaxTemoin() {
		return (String) storedValueForKey("ggrpMaxTemoin");
	}

	public void setGgrpMaxTemoin(String value) {
		takeStoredValueForKey(value, "ggrpMaxTemoin");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeCollection toFwkScolarite_ScolGroupeCollection() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeCollection)storedValueForKey("toFwkScolarite_ScolGroupeCollection");
	}

	public void setToFwkScolarite_ScolGroupeCollectionRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeCollection value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeCollection oldValue = toFwkScolarite_ScolGroupeCollection();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolGroupeCollection");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolGroupeCollection");
		}
	}
  
	public NSArray toFwkScolarite_ScolGroupeInclusions1() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolGroupeInclusions1");
	}

	public NSArray toFwkScolarite_ScolGroupeInclusions1(EOQualifier qualifier) {
		return toFwkScolarite_ScolGroupeInclusions1(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolGroupeInclusions1(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolGroupeInclusions1(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolGroupeInclusions1(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion.TO_FWK_SCOLARITE__SCOL_GROUPE_GRP1_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolGroupeInclusions1();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolGroupeInclusions1Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeInclusions1");
	}

	public void removeFromToFwkScolarite_ScolGroupeInclusions1Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeInclusions1");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion createToFwkScolarite_ScolGroupeInclusions1Relationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolGroupeInclusion");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolGroupeInclusions1");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion) eo;
	}

	public void deleteToFwkScolarite_ScolGroupeInclusions1Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeInclusions1");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolGroupeInclusions1Relationships() {
		Enumeration objects = toFwkScolarite_ScolGroupeInclusions1().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolGroupeInclusions1Relationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolGroupeInclusions2() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolGroupeInclusions2");
	}

	public NSArray toFwkScolarite_ScolGroupeInclusions2(EOQualifier qualifier) {
		return toFwkScolarite_ScolGroupeInclusions2(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolGroupeInclusions2(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolGroupeInclusions2(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolGroupeInclusions2(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion.TO_FWK_SCOLARITE__SCOL_GROUPE_GRP2_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolGroupeInclusions2();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolGroupeInclusions2Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeInclusions2");
	}

	public void removeFromToFwkScolarite_ScolGroupeInclusions2Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeInclusions2");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion createToFwkScolarite_ScolGroupeInclusions2Relationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolGroupeInclusion");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolGroupeInclusions2");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion) eo;
	}

	public void deleteToFwkScolarite_ScolGroupeInclusions2Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeInclusions2");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolGroupeInclusions2Relationships() {
		Enumeration objects = toFwkScolarite_ScolGroupeInclusions2().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolGroupeInclusions2Relationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeInclusion)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolGroupeIncompatibilites1() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolGroupeIncompatibilites1");
	}

	public NSArray toFwkScolarite_ScolGroupeIncompatibilites1(EOQualifier qualifier) {
		return toFwkScolarite_ScolGroupeIncompatibilites1(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolGroupeIncompatibilites1(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolGroupeIncompatibilites1(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolGroupeIncompatibilites1(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite.TO_FWK_SCOLARITE__SCOL_GROUPE_GRP1_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolGroupeIncompatibilites1();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolGroupeIncompatibilites1Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeIncompatibilites1");
	}

	public void removeFromToFwkScolarite_ScolGroupeIncompatibilites1Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeIncompatibilites1");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite createToFwkScolarite_ScolGroupeIncompatibilites1Relationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolGroupeIncompatibilite");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolGroupeIncompatibilites1");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite) eo;
	}

	public void deleteToFwkScolarite_ScolGroupeIncompatibilites1Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeIncompatibilites1");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolGroupeIncompatibilites1Relationships() {
		Enumeration objects = toFwkScolarite_ScolGroupeIncompatibilites1().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolGroupeIncompatibilites1Relationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolGroupeIncompatibilites2() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolGroupeIncompatibilites2");
	}

	public NSArray toFwkScolarite_ScolGroupeIncompatibilites2(EOQualifier qualifier) {
		return toFwkScolarite_ScolGroupeIncompatibilites2(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolGroupeIncompatibilites2(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolGroupeIncompatibilites2(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolGroupeIncompatibilites2(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite.TO_FWK_SCOLARITE__SCOL_GROUPE_GRP2_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolGroupeIncompatibilites2();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolGroupeIncompatibilites2Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeIncompatibilites2");
	}

	public void removeFromToFwkScolarite_ScolGroupeIncompatibilites2Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeIncompatibilites2");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite createToFwkScolarite_ScolGroupeIncompatibilites2Relationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolGroupeIncompatibilite");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolGroupeIncompatibilites2");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite) eo;
	}

	public void deleteToFwkScolarite_ScolGroupeIncompatibilites2Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeIncompatibilites2");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolGroupeIncompatibilites2Relationships() {
		Enumeration objects = toFwkScolarite_ScolGroupeIncompatibilites2().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolGroupeIncompatibilites2Relationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeIncompatibilite)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolGroupeObjets() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolGroupeObjets");
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier) {
		return toFwkScolarite_ScolGroupeObjets(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolGroupeObjets(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet.TO_FWK_SCOLARITE__SCOL_GROUPE_GRP_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolGroupeObjets();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
	}

	public void removeFromToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet createToFwkScolarite_ScolGroupeObjetsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolGroupeObjet");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolGroupeObjets");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet) eo;
	}

	public void deleteToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolGroupeObjetsRelationships() {
		Enumeration objects = toFwkScolarite_ScolGroupeObjets().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolGroupeObjetsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolGroupeGrp.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolGroupeGrp.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolGroupeGrp)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolGroupeGrp fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolGroupeGrp fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolGroupeGrp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolGroupeGrp)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolGroupeGrp fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolGroupeGrp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolGroupeGrp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolGroupeGrp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolGroupeGrp)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolGroupeGrp fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolGroupeGrp fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolGroupeGrp eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolGroupeGrp ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolGroupeGrp createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolGroupeGrp.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolGroupeGrp.ENTITY_NAME + "' !");
		}
		else {
			EOScolGroupeGrp object = (EOScolGroupeGrp) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolGroupeGrp localInstanceOfObject(EOEditingContext ec, EOScolGroupeGrp object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolGroupeGrp " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolGroupeGrp) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
