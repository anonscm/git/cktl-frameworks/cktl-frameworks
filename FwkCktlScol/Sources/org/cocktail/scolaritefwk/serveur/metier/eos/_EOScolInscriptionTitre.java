/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolInscriptionTitre.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolInscriptionTitre extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolInscriptionTitre";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_INSCRIPTION_TITRE";

	//Attributes

	public static final ERXKey<Integer> FANN_KEY = new ERXKey<Integer>("fannKey");
	public static final ERXKey<Integer> FTIT_KEY = new ERXKey<Integer>("ftitKey");
	public static final ERXKey<Integer> IDIPL_ANNEE_SUIVIE = new ERXKey<Integer>("idiplAnneeSuivie");
	public static final ERXKey<Integer> IDIPL_NUMERO = new ERXKey<Integer>("idiplNumero");
	public static final ERXKey<Integer> IFTIT_ETAT = new ERXKey<Integer>("iftitEtat");
	public static final ERXKey<java.math.BigDecimal> IFTIT_MOYENNE = new ERXKey<java.math.BigDecimal>("iftitMoyenne");

	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FTIT_KEY_KEY = "ftitKey";
	public static final String IDIPL_ANNEE_SUIVIE_KEY = "idiplAnneeSuivie";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String IFTIT_ETAT_KEY = "iftitEtat";
	public static final String IFTIT_MOYENNE_KEY = "iftitMoyenne";

	// Non visible attributes
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String IFTIT_MENTION_KEY = "iftitMention";
	public static final String RES_CODE_KEY = "resCode";

	// Colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FTIT_KEY_COLKEY = "FTIT_KEY";
	public static final String IDIPL_ANNEE_SUIVIE_COLKEY = "IDIPL_ANNEE_SUIVIE";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String IFTIT_ETAT_COLKEY = "IFTIT_ETAT";
	public static final String IFTIT_MOYENNE_COLKEY = "IFTIT_MOYENNE";

	// Non visible colkeys
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String IFTIT_MENTION_COLKEY = "IFTIT_MENTION";
	public static final String RES_CODE_COLKEY = "RES_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention> TO_FWK_SCOLARITE__SCOL_CONSTANTE_MENTION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention>("toFwkScolarite_ScolConstanteMention");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre> TO_FWK_SCOLARITE__SCOL_FORMATION_TITRE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre>("toFwkScolarite_ScolFormationTitre");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiant> TO_FWK_SCOLARIX__ETUDIANT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOEtudiant>("toFwkScolarix_Etudiant");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl> TO_FWK_SCOLARIX__INSC_DIPL = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOInscDipl>("toFwkScolarix_InscDipl");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOResultat> TO_FWK_SCOLARIX__RESULTAT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOResultat>("toFwkScolarix_Resultat");

	public static final String TO_FWK_SCOLARITE__SCOL_CONSTANTE_MENTION_KEY = "toFwkScolarite_ScolConstanteMention";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_TITRE_KEY = "toFwkScolarite_ScolFormationTitre";
	public static final String TO_FWK_SCOLARIX__ETUDIANT_KEY = "toFwkScolarix_Etudiant";
	public static final String TO_FWK_SCOLARIX__INSC_DIPL_KEY = "toFwkScolarix_InscDipl";
	public static final String TO_FWK_SCOLARIX__RESULTAT_KEY = "toFwkScolarix_Resultat";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolInscriptionTitre with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fannKey
	 * @param ftitKey
	 * @param idiplNumero
	 * @param iftitEtat
	 * @param toFwkScolarite_ScolConstanteMention
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationTitre
	 * @param toFwkScolarix_Etudiant
	 * @param toFwkScolarix_InscDipl
	 * @param toFwkScolarix_Resultat
	 * @return EOScolInscriptionTitre
	 */
	public static EOScolInscriptionTitre create(EOEditingContext editingContext, Integer fannKey, Integer ftitKey, Integer idiplNumero, Integer iftitEtat, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention toFwkScolarite_ScolConstanteMention, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre toFwkScolarite_ScolFormationTitre, org.cocktail.scolarix.serveur.metier.eos.EOEtudiant toFwkScolarix_Etudiant, org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toFwkScolarix_InscDipl, org.cocktail.scolarix.serveur.metier.eos.EOResultat toFwkScolarix_Resultat) {
		EOScolInscriptionTitre eo = (EOScolInscriptionTitre) createAndInsertInstance(editingContext);
		eo.setFannKey(fannKey);
		eo.setFtitKey(ftitKey);
		eo.setIdiplNumero(idiplNumero);
		eo.setIftitEtat(iftitEtat);
		eo.setToFwkScolarite_ScolConstanteMentionRelationship(toFwkScolarite_ScolConstanteMention);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationTitreRelationship(toFwkScolarite_ScolFormationTitre);
		eo.setToFwkScolarix_EtudiantRelationship(toFwkScolarix_Etudiant);
		eo.setToFwkScolarix_InscDiplRelationship(toFwkScolarix_InscDipl);
		eo.setToFwkScolarix_ResultatRelationship(toFwkScolarix_Resultat);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolInscriptionTitre.
	 *
	 * @param editingContext
	 * @return EOScolInscriptionTitre
	 */
	public static EOScolInscriptionTitre create(EOEditingContext editingContext) {
		EOScolInscriptionTitre eo = (EOScolInscriptionTitre) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolInscriptionTitre localInstanceIn(EOEditingContext editingContext) {
		EOScolInscriptionTitre localInstance = (EOScolInscriptionTitre) localInstanceOfObject(editingContext, (EOScolInscriptionTitre) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolInscriptionTitre localInstanceIn(EOEditingContext editingContext, EOScolInscriptionTitre eo) {
		EOScolInscriptionTitre localInstance = (eo == null) ? null : (EOScolInscriptionTitre) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer fannKey() {
		return (Integer) storedValueForKey("fannKey");
	}

	public void setFannKey(Integer value) {
		takeStoredValueForKey(value, "fannKey");
	}
	public Integer ftitKey() {
		return (Integer) storedValueForKey("ftitKey");
	}

	public void setFtitKey(Integer value) {
		takeStoredValueForKey(value, "ftitKey");
	}
	public Integer idiplAnneeSuivie() {
		return (Integer) storedValueForKey("idiplAnneeSuivie");
	}

	public void setIdiplAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "idiplAnneeSuivie");
	}
	public Integer idiplNumero() {
		return (Integer) storedValueForKey("idiplNumero");
	}

	public void setIdiplNumero(Integer value) {
		takeStoredValueForKey(value, "idiplNumero");
	}
	public Integer iftitEtat() {
		return (Integer) storedValueForKey("iftitEtat");
	}

	public void setIftitEtat(Integer value) {
		takeStoredValueForKey(value, "iftitEtat");
	}
	public java.math.BigDecimal iftitMoyenne() {
		return (java.math.BigDecimal) storedValueForKey("iftitMoyenne");
	}

	public void setIftitMoyenne(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "iftitMoyenne");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention toFwkScolarite_ScolConstanteMention() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention)storedValueForKey("toFwkScolarite_ScolConstanteMention");
	}

	public void setToFwkScolarite_ScolConstanteMentionRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention oldValue = toFwkScolarite_ScolConstanteMention();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolConstanteMention");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolConstanteMention");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre toFwkScolarite_ScolFormationTitre() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre)storedValueForKey("toFwkScolarite_ScolFormationTitre");
	}

	public void setToFwkScolarite_ScolFormationTitreRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationTitre oldValue = toFwkScolarite_ScolFormationTitre();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationTitre");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationTitre");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOEtudiant toFwkScolarix_Etudiant() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOEtudiant)storedValueForKey("toFwkScolarix_Etudiant");
	}

	public void setToFwkScolarix_EtudiantRelationship(org.cocktail.scolarix.serveur.metier.eos.EOEtudiant value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOEtudiant oldValue = toFwkScolarix_Etudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarix_Etudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarix_Etudiant");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOInscDipl toFwkScolarix_InscDipl() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOInscDipl)storedValueForKey("toFwkScolarix_InscDipl");
	}

	public void setToFwkScolarix_InscDiplRelationship(org.cocktail.scolarix.serveur.metier.eos.EOInscDipl value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOInscDipl oldValue = toFwkScolarix_InscDipl();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarix_InscDipl");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarix_InscDipl");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOResultat toFwkScolarix_Resultat() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOResultat)storedValueForKey("toFwkScolarix_Resultat");
	}

	public void setToFwkScolarix_ResultatRelationship(org.cocktail.scolarix.serveur.metier.eos.EOResultat value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOResultat oldValue = toFwkScolarix_Resultat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarix_Resultat");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarix_Resultat");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolInscriptionTitre.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolInscriptionTitre.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolInscriptionTitre)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolInscriptionTitre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolInscriptionTitre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolInscriptionTitre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolInscriptionTitre)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolInscriptionTitre fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolInscriptionTitre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolInscriptionTitre)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionTitre fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionTitre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolInscriptionTitre eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolInscriptionTitre ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolInscriptionTitre createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolInscriptionTitre.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolInscriptionTitre.ENTITY_NAME + "' !");
		}
		else {
			EOScolInscriptionTitre object = (EOScolInscriptionTitre) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolInscriptionTitre localInstanceOfObject(EOEditingContext ec, EOScolInscriptionTitre object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolInscriptionTitre " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolInscriptionTitre) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
