/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolC2iCompetence.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolC2iCompetence extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolC2iCompetence";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_C2I_COMPETENCE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "c2ioKey";

	public static final ERXKey<String> C2IO_CODE = new ERXKey<String>("c2ioCode");
	public static final ERXKey<String> C2IO_DESCRIPTIF = new ERXKey<String>("c2ioDescriptif");
	public static final ERXKey<String> C2IO_LIBELLE = new ERXKey<String>("c2ioLibelle");
	public static final ERXKey<String> C2IO_PERE = new ERXKey<String>("c2ioPere");
	public static final ERXKey<String> C2IO_TYPE = new ERXKey<String>("c2ioType");

	public static final String C2IO_CODE_KEY = "c2ioCode";
	public static final String C2IO_DESCRIPTIF_KEY = "c2ioDescriptif";
	public static final String C2IO_LIBELLE_KEY = "c2ioLibelle";
	public static final String C2IO_PERE_KEY = "c2ioPere";
	public static final String C2IO_TYPE_KEY = "c2ioType";

	// Non visible attributes
	public static final String C2IE_KEY_KEY = "c2ieKey";
	public static final String C2IO_KEY_KEY = "c2ioKey";
	public static final String FANN_KEY_KEY = "fannKey";

	// Colkeys
	public static final String C2IO_CODE_COLKEY = "C2IO_CODE";
	public static final String C2IO_DESCRIPTIF_COLKEY = "C2IO_DESCRIPTIF";
	public static final String C2IO_LIBELLE_COLKEY = "C2IO_LIBELLE";
	public static final String C2IO_PERE_COLKEY = "C2IO_PERE";
	public static final String C2IO_TYPE_COLKEY = "C2IO_TYPE";

	// Non visible colkeys
	public static final String C2IE_KEY_COLKEY = "C2IE_KEY";
	public static final String C2IO_KEY_COLKEY = "C2IO_KEY";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete> TO_FWK_SCOLARITE__SCOL_C2I_ENTETE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete>("toFwkScolarite_ScolC2iEntete");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence> TO_FWK_SCOLARITE__SCOL_C2I_REPART_COMPETENCES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence>("toFwkScolarite_ScolC2iRepartCompetences");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");

	public static final String TO_FWK_SCOLARITE__SCOL_C2I_ENTETE_KEY = "toFwkScolarite_ScolC2iEntete";
	public static final String TO_FWK_SCOLARITE__SCOL_C2I_REPART_COMPETENCES_KEY = "toFwkScolarite_ScolC2iRepartCompetences";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolC2iCompetence with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param c2ioCode
	 * @param c2ioLibelle
	 * @param c2ioType
	 * @param toFwkScolarite_ScolC2iEntete
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @return EOScolC2iCompetence
	 */
	public static EOScolC2iCompetence create(EOEditingContext editingContext, String c2ioCode, String c2ioLibelle, String c2ioType, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete toFwkScolarite_ScolC2iEntete, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee) {
		EOScolC2iCompetence eo = (EOScolC2iCompetence) createAndInsertInstance(editingContext);
		eo.setC2ioCode(c2ioCode);
		eo.setC2ioLibelle(c2ioLibelle);
		eo.setC2ioType(c2ioType);
		eo.setToFwkScolarite_ScolC2iEnteteRelationship(toFwkScolarite_ScolC2iEntete);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolC2iCompetence.
	 *
	 * @param editingContext
	 * @return EOScolC2iCompetence
	 */
	public static EOScolC2iCompetence create(EOEditingContext editingContext) {
		EOScolC2iCompetence eo = (EOScolC2iCompetence) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolC2iCompetence localInstanceIn(EOEditingContext editingContext) {
		EOScolC2iCompetence localInstance = (EOScolC2iCompetence) localInstanceOfObject(editingContext, (EOScolC2iCompetence) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolC2iCompetence localInstanceIn(EOEditingContext editingContext, EOScolC2iCompetence eo) {
		EOScolC2iCompetence localInstance = (eo == null) ? null : (EOScolC2iCompetence) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String c2ioCode() {
		return (String) storedValueForKey("c2ioCode");
	}

	public void setC2ioCode(String value) {
		takeStoredValueForKey(value, "c2ioCode");
	}
	public String c2ioDescriptif() {
		return (String) storedValueForKey("c2ioDescriptif");
	}

	public void setC2ioDescriptif(String value) {
		takeStoredValueForKey(value, "c2ioDescriptif");
	}
	public String c2ioLibelle() {
		return (String) storedValueForKey("c2ioLibelle");
	}

	public void setC2ioLibelle(String value) {
		takeStoredValueForKey(value, "c2ioLibelle");
	}
	public String c2ioPere() {
		return (String) storedValueForKey("c2ioPere");
	}

	public void setC2ioPere(String value) {
		takeStoredValueForKey(value, "c2ioPere");
	}
	public String c2ioType() {
		return (String) storedValueForKey("c2ioType");
	}

	public void setC2ioType(String value) {
		takeStoredValueForKey(value, "c2ioType");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete toFwkScolarite_ScolC2iEntete() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete)storedValueForKey("toFwkScolarite_ScolC2iEntete");
	}

	public void setToFwkScolarite_ScolC2iEnteteRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iEntete oldValue = toFwkScolarite_ScolC2iEntete();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolC2iEntete");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolC2iEntete");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public NSArray toFwkScolarite_ScolC2iRepartCompetences() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolC2iRepartCompetences");
	}

	public NSArray toFwkScolarite_ScolC2iRepartCompetences(EOQualifier qualifier) {
		return toFwkScolarite_ScolC2iRepartCompetences(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolC2iRepartCompetences(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolC2iRepartCompetences(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolC2iRepartCompetences(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence.TO_FWK_SCOLARITE__SCOL_C2I_COMPETENCE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolC2iRepartCompetences();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolC2iRepartCompetencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iRepartCompetences");
	}

	public void removeFromToFwkScolarite_ScolC2iRepartCompetencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iRepartCompetences");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence createToFwkScolarite_ScolC2iRepartCompetencesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolC2iRepartCompetence");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolC2iRepartCompetences");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence) eo;
	}

	public void deleteToFwkScolarite_ScolC2iRepartCompetencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iRepartCompetences");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolC2iRepartCompetencesRelationships() {
		Enumeration objects = toFwkScolarite_ScolC2iRepartCompetences().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolC2iRepartCompetencesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartCompetence)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolC2iCompetence.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolC2iCompetence.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolC2iCompetence)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolC2iCompetence fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolC2iCompetence fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolC2iCompetence eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolC2iCompetence)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolC2iCompetence fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolC2iCompetence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolC2iCompetence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolC2iCompetence eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolC2iCompetence)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolC2iCompetence fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolC2iCompetence fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolC2iCompetence eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolC2iCompetence ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolC2iCompetence createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolC2iCompetence.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolC2iCompetence.ENTITY_NAME + "' !");
		}
		else {
			EOScolC2iCompetence object = (EOScolC2iCompetence) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolC2iCompetence localInstanceOfObject(EOEditingContext ec, EOScolC2iCompetence object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolC2iCompetence " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolC2iCompetence) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
