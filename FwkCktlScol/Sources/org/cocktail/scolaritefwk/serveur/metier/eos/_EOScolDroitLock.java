/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolDroitLock.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolDroitLock extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolDroitLock";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_DROIT_LOCK";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "dlokKey";

	public static final ERXKey<String> DLOK_APPLICATION = new ERXKey<String>("dlokApplication");
	public static final ERXKey<String> DLOK_CLIENT = new ERXKey<String>("dlokClient");
	public static final ERXKey<NSTimestamp> DLOK_DEBUT = new ERXKey<NSTimestamp>("dlokDebut");
	public static final ERXKey<NSTimestamp> DLOK_FIN = new ERXKey<NSTimestamp>("dlokFin");
	public static final ERXKey<String> DLOK_HOSTNAME = new ERXKey<String>("dlokHostname");

	public static final String DLOK_APPLICATION_KEY = "dlokApplication";
	public static final String DLOK_CLIENT_KEY = "dlokClient";
	public static final String DLOK_DEBUT_KEY = "dlokDebut";
	public static final String DLOK_FIN_KEY = "dlokFin";
	public static final String DLOK_HOSTNAME_KEY = "dlokHostname";

	// Non visible attributes
	public static final String DLOG_KEY_KEY = "dlogKey";
	public static final String DLOK_KEY_KEY = "dlokKey";

	// Colkeys
	public static final String DLOK_APPLICATION_COLKEY = "DLOK_APPLICATION";
	public static final String DLOK_CLIENT_COLKEY = "DLOK_CLIENT";
	public static final String DLOK_DEBUT_COLKEY = "DLOK_DEBUT";
	public static final String DLOK_FIN_COLKEY = "DLOK_FIN";
	public static final String DLOK_HOSTNAME_COLKEY = "DLOK_HOSTNAME";

	// Non visible colkeys
	public static final String DLOG_KEY_COLKEY = "DLOG_KEY";
	public static final String DLOK_KEY_COLKEY = "DLOK_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin> TO_FWK_SCOLARITE__SCOL_DROIT_LOGIN = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin>("toFwkScolarite_ScolDroitLogin");

	public static final String TO_FWK_SCOLARITE__SCOL_DROIT_LOGIN_KEY = "toFwkScolarite_ScolDroitLogin";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolDroitLock with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dlokApplication
	 * @param dlokClient
	 * @param dlokHostname
	 * @param toFwkScolarite_ScolDroitLogin
	 * @return EOScolDroitLock
	 */
	public static EOScolDroitLock create(EOEditingContext editingContext, String dlokApplication, String dlokClient, String dlokHostname, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin toFwkScolarite_ScolDroitLogin) {
		EOScolDroitLock eo = (EOScolDroitLock) createAndInsertInstance(editingContext);
		eo.setDlokApplication(dlokApplication);
		eo.setDlokClient(dlokClient);
		eo.setDlokHostname(dlokHostname);
		eo.setToFwkScolarite_ScolDroitLoginRelationship(toFwkScolarite_ScolDroitLogin);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolDroitLock.
	 *
	 * @param editingContext
	 * @return EOScolDroitLock
	 */
	public static EOScolDroitLock create(EOEditingContext editingContext) {
		EOScolDroitLock eo = (EOScolDroitLock) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolDroitLock localInstanceIn(EOEditingContext editingContext) {
		EOScolDroitLock localInstance = (EOScolDroitLock) localInstanceOfObject(editingContext, (EOScolDroitLock) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolDroitLock localInstanceIn(EOEditingContext editingContext, EOScolDroitLock eo) {
		EOScolDroitLock localInstance = (eo == null) ? null : (EOScolDroitLock) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String dlokApplication() {
		return (String) storedValueForKey("dlokApplication");
	}

	public void setDlokApplication(String value) {
		takeStoredValueForKey(value, "dlokApplication");
	}
	public String dlokClient() {
		return (String) storedValueForKey("dlokClient");
	}

	public void setDlokClient(String value) {
		takeStoredValueForKey(value, "dlokClient");
	}
	public NSTimestamp dlokDebut() {
		return (NSTimestamp) storedValueForKey("dlokDebut");
	}

	public void setDlokDebut(NSTimestamp value) {
		takeStoredValueForKey(value, "dlokDebut");
	}
	public NSTimestamp dlokFin() {
		return (NSTimestamp) storedValueForKey("dlokFin");
	}

	public void setDlokFin(NSTimestamp value) {
		takeStoredValueForKey(value, "dlokFin");
	}
	public String dlokHostname() {
		return (String) storedValueForKey("dlokHostname");
	}

	public void setDlokHostname(String value) {
		takeStoredValueForKey(value, "dlokHostname");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin toFwkScolarite_ScolDroitLogin() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin)storedValueForKey("toFwkScolarite_ScolDroitLogin");
	}

	public void setToFwkScolarite_ScolDroitLoginRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin oldValue = toFwkScolarite_ScolDroitLogin();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolDroitLogin");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolDroitLogin");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolDroitLock.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolDroitLock.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolDroitLock)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolDroitLock fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolDroitLock fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolDroitLock eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolDroitLock)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolDroitLock fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolDroitLock fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolDroitLock fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolDroitLock eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolDroitLock)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolDroitLock fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolDroitLock fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolDroitLock eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolDroitLock ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolDroitLock createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolDroitLock.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolDroitLock.ENTITY_NAME + "' !");
		}
		else {
			EOScolDroitLock object = (EOScolDroitLock) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolDroitLock localInstanceOfObject(EOEditingContext ec, EOScolDroitLock object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolDroitLock " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolDroitLock) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
