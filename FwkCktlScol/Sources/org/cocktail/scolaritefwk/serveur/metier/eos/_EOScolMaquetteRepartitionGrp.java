/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteRepartitionGrp.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteRepartitionGrp extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteRepartitionGrp";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_REPARTITION_GRP";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mrgrpKey";

	public static final ERXKey<java.math.BigDecimal> MRGRP_GROUPE = new ERXKey<java.math.BigDecimal>("mrgrpGroupe");
	public static final ERXKey<Integer> MSEM_ORDRE = new ERXKey<Integer>("msemOrdre");

	public static final String MRGRP_GROUPE_KEY = "mrgrpGroupe";
	public static final String MSEM_ORDRE_KEY = "msemOrdre";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String MAP_KEY_KEY = "mapKey";
	public static final String MEC_KEY_KEY = "mecKey";
	public static final String MPAR_KEY_KEY = "mparKey";
	public static final String MRAP_KEY_KEY = "mrapKey";
	public static final String MREC_KEY_KEY = "mrecKey";
	public static final String MRGRP_KEY_KEY = "mrgrpKey";
	public static final String MRSEM_KEY_KEY = "mrsemKey";
	public static final String MRUE_KEY_KEY = "mrueKey";
	public static final String MSEM_KEY_KEY = "msemKey";
	public static final String MUE_KEY_KEY = "mueKey";

	// Colkeys
	public static final String MRGRP_GROUPE_COLKEY = "MRGRP_GROUPE";
	public static final String MSEM_ORDRE_COLKEY = "MSEM_ORDRE";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String MAP_KEY_COLKEY = "MAP_KEY";
	public static final String MEC_KEY_COLKEY = "MEC_KEY";
	public static final String MPAR_KEY_COLKEY = "MPAR_KEY";
	public static final String MRAP_KEY_COLKEY = "MRAP_KEY";
	public static final String MREC_KEY_COLKEY = "MREC_KEY";
	public static final String MRGRP_KEY_COLKEY = "MRGRP_KEY";
	public static final String MRSEM_KEY_COLKEY = "MRSEM_KEY";
	public static final String MRUE_KEY_COLKEY = "MRUE_KEY";
	public static final String MSEM_KEY_COLKEY = "MSEM_KEY";
	public static final String MUE_KEY_COLKEY = "MUE_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp> TO_FWK_SCOLARITE__SCOL_MAQUETTE_AP = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp>("toFwkScolarite_ScolMaquetteAp");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc> TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc>("toFwkScolarite_ScolMaquetteEc");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours> TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours>("toFwkScolarite_ScolMaquetteParcours");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_AP = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp>("toFwkScolarite_ScolMaquetteRepartitionAp");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_EC = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc>("toFwkScolarite_ScolMaquetteRepartitionEc");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEM = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem>("toFwkScolarite_ScolMaquetteRepartitionSem");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe>("toFwkScolarite_ScolMaquetteRepartitionUe");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre> TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre>("toFwkScolarite_ScolMaquetteSemestre");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe> TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe>("toFwkScolarite_ScolMaquetteUe");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_AP_KEY = "toFwkScolarite_ScolMaquetteAp";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY = "toFwkScolarite_ScolMaquetteEc";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY = "toFwkScolarite_ScolMaquetteParcours";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_AP_KEY = "toFwkScolarite_ScolMaquetteRepartitionAp";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_EC_KEY = "toFwkScolarite_ScolMaquetteRepartitionEc";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEM_KEY = "toFwkScolarite_ScolMaquetteRepartitionSem";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UE_KEY = "toFwkScolarite_ScolMaquetteRepartitionUe";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY = "toFwkScolarite_ScolMaquetteSemestre";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY = "toFwkScolarite_ScolMaquetteUe";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteRepartitionGrp with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mrgrpGroupe
	 * @param msemOrdre
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @param toFwkScolarite_ScolMaquetteAp
	 * @param toFwkScolarite_ScolMaquetteEc
	 * @param toFwkScolarite_ScolMaquetteParcours
	 * @param toFwkScolarite_ScolMaquetteRepartitionAp
	 * @param toFwkScolarite_ScolMaquetteRepartitionEc
	 * @param toFwkScolarite_ScolMaquetteRepartitionSem
	 * @param toFwkScolarite_ScolMaquetteRepartitionUe
	 * @param toFwkScolarite_ScolMaquetteSemestre
	 * @param toFwkScolarite_ScolMaquetteUe
	 * @return EOScolMaquetteRepartitionGrp
	 */
	public static EOScolMaquetteRepartitionGrp create(EOEditingContext editingContext, java.math.BigDecimal mrgrpGroupe, Integer msemOrdre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp toFwkScolarite_ScolMaquetteAp, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc toFwkScolarite_ScolMaquetteEc, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp toFwkScolarite_ScolMaquetteRepartitionAp, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc toFwkScolarite_ScolMaquetteRepartitionEc, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem toFwkScolarite_ScolMaquetteRepartitionSem, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe toFwkScolarite_ScolMaquetteRepartitionUe, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe toFwkScolarite_ScolMaquetteUe) {
		EOScolMaquetteRepartitionGrp eo = (EOScolMaquetteRepartitionGrp) createAndInsertInstance(editingContext);
		eo.setMrgrpGroupe(mrgrpGroupe);
		eo.setMsemOrdre(msemOrdre);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		eo.setToFwkScolarite_ScolMaquetteApRelationship(toFwkScolarite_ScolMaquetteAp);
		eo.setToFwkScolarite_ScolMaquetteEcRelationship(toFwkScolarite_ScolMaquetteEc);
		eo.setToFwkScolarite_ScolMaquetteParcoursRelationship(toFwkScolarite_ScolMaquetteParcours);
		eo.setToFwkScolarite_ScolMaquetteRepartitionApRelationship(toFwkScolarite_ScolMaquetteRepartitionAp);
		eo.setToFwkScolarite_ScolMaquetteRepartitionEcRelationship(toFwkScolarite_ScolMaquetteRepartitionEc);
		eo.setToFwkScolarite_ScolMaquetteRepartitionSemRelationship(toFwkScolarite_ScolMaquetteRepartitionSem);
		eo.setToFwkScolarite_ScolMaquetteRepartitionUeRelationship(toFwkScolarite_ScolMaquetteRepartitionUe);
		eo.setToFwkScolarite_ScolMaquetteSemestreRelationship(toFwkScolarite_ScolMaquetteSemestre);
		eo.setToFwkScolarite_ScolMaquetteUeRelationship(toFwkScolarite_ScolMaquetteUe);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteRepartitionGrp.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteRepartitionGrp
	 */
	public static EOScolMaquetteRepartitionGrp create(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionGrp eo = (EOScolMaquetteRepartitionGrp) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteRepartitionGrp localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteRepartitionGrp localInstance = (EOScolMaquetteRepartitionGrp) localInstanceOfObject(editingContext, (EOScolMaquetteRepartitionGrp) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteRepartitionGrp localInstanceIn(EOEditingContext editingContext, EOScolMaquetteRepartitionGrp eo) {
		EOScolMaquetteRepartitionGrp localInstance = (eo == null) ? null : (EOScolMaquetteRepartitionGrp) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public java.math.BigDecimal mrgrpGroupe() {
		return (java.math.BigDecimal) storedValueForKey("mrgrpGroupe");
	}

	public void setMrgrpGroupe(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mrgrpGroupe");
	}
	public Integer msemOrdre() {
		return (Integer) storedValueForKey("msemOrdre");
	}

	public void setMsemOrdre(Integer value) {
		takeStoredValueForKey(value, "msemOrdre");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp toFwkScolarite_ScolMaquetteAp() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp)storedValueForKey("toFwkScolarite_ScolMaquetteAp");
	}

	public void setToFwkScolarite_ScolMaquetteApRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp oldValue = toFwkScolarite_ScolMaquetteAp();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteAp");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteAp");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc toFwkScolarite_ScolMaquetteEc() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc)storedValueForKey("toFwkScolarite_ScolMaquetteEc");
	}

	public void setToFwkScolarite_ScolMaquetteEcRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc oldValue = toFwkScolarite_ScolMaquetteEc();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteEc");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteEc");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours)storedValueForKey("toFwkScolarite_ScolMaquetteParcours");
	}

	public void setToFwkScolarite_ScolMaquetteParcoursRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours oldValue = toFwkScolarite_ScolMaquetteParcours();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteParcours");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteParcours");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp toFwkScolarite_ScolMaquetteRepartitionAp() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionAp");
	}

	public void setToFwkScolarite_ScolMaquetteRepartitionApRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp oldValue = toFwkScolarite_ScolMaquetteRepartitionAp();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteRepartitionAp");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteRepartitionAp");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc toFwkScolarite_ScolMaquetteRepartitionEc() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionEc");
	}

	public void setToFwkScolarite_ScolMaquetteRepartitionEcRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc oldValue = toFwkScolarite_ScolMaquetteRepartitionEc();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteRepartitionEc");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteRepartitionEc");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem toFwkScolarite_ScolMaquetteRepartitionSem() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionSem");
	}

	public void setToFwkScolarite_ScolMaquetteRepartitionSemRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem oldValue = toFwkScolarite_ScolMaquetteRepartitionSem();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteRepartitionSem");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteRepartitionSem");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe toFwkScolarite_ScolMaquetteRepartitionUe() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionUe");
	}

	public void setToFwkScolarite_ScolMaquetteRepartitionUeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe oldValue = toFwkScolarite_ScolMaquetteRepartitionUe();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteRepartitionUe");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteRepartitionUe");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre toFwkScolarite_ScolMaquetteSemestre() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre)storedValueForKey("toFwkScolarite_ScolMaquetteSemestre");
	}

	public void setToFwkScolarite_ScolMaquetteSemestreRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre oldValue = toFwkScolarite_ScolMaquetteSemestre();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteSemestre");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteSemestre");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe toFwkScolarite_ScolMaquetteUe() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe)storedValueForKey("toFwkScolarite_ScolMaquetteUe");
	}

	public void setToFwkScolarite_ScolMaquetteUeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe oldValue = toFwkScolarite_ScolMaquetteUe();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteUe");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteUe");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteRepartitionGrp.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteRepartitionGrp.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteRepartitionGrp)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteRepartitionGrp fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteRepartitionGrp fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteRepartitionGrp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteRepartitionGrp)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionGrp fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionGrp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteRepartitionGrp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteRepartitionGrp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteRepartitionGrp)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionGrp fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteRepartitionGrp fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteRepartitionGrp eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteRepartitionGrp ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteRepartitionGrp createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteRepartitionGrp.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteRepartitionGrp.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteRepartitionGrp object = (EOScolMaquetteRepartitionGrp) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteRepartitionGrp localInstanceOfObject(EOEditingContext ec, EOScolMaquetteRepartitionGrp object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteRepartitionGrp " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteRepartitionGrp) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
