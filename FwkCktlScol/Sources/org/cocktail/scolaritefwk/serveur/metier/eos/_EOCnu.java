/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCnu.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOCnu extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_Cnu";
	public static final String ENTITY_TABLE_NAME = "GRHUM.CNU";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "noCnu";

	public static final ERXKey<String> C_SECTION_CNU = new ERXKey<String>("cSectionCnu");
	public static final ERXKey<String> C_SOUS_SECTION_CNU = new ERXKey<String>("cSousSectionCnu");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> GROUPE_CNU = new ERXKey<String>("groupeCnu");
	public static final ERXKey<String> LC_SECTION_CNU = new ERXKey<String>("lcSectionCnu");
	public static final ERXKey<String> LL_SECTION_CNU = new ERXKey<String>("llSectionCnu");
	public static final ERXKey<String> TEM_COMPOSEE = new ERXKey<String>("temComposee");
	public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");

	public static final String C_SECTION_CNU_KEY = "cSectionCnu";
	public static final String C_SOUS_SECTION_CNU_KEY = "cSousSectionCnu";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String GROUPE_CNU_KEY = "groupeCnu";
	public static final String LC_SECTION_CNU_KEY = "lcSectionCnu";
	public static final String LL_SECTION_CNU_KEY = "llSectionCnu";
	public static final String TEM_COMPOSEE_KEY = "temComposee";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Non visible attributes
	public static final String NO_CNU_KEY = "noCnu";

	// Colkeys
	public static final String C_SECTION_CNU_COLKEY = "C_SECTION_CNU";
	public static final String C_SOUS_SECTION_CNU_COLKEY = "C_SOUS_SECTION_CNU";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String GROUPE_CNU_COLKEY = "GROUPE_CNU";
	public static final String LC_SECTION_CNU_COLKEY = "LC_SECTION_CNU";
	public static final String LL_SECTION_CNU_COLKEY = "LL_SECTION_CNU";
	public static final String TEM_COMPOSEE_COLKEY = "TEM_COMPOSEE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	// Non visible colkeys
	public static final String NO_CNU_COLKEY = "NO_CNU";

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOCnu with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cSectionCnu
	 * @param dCreation
	 * @param dModification
	 * @param temValide
	 * @return EOCnu
	 */
	public static EOCnu create(EOEditingContext editingContext, String cSectionCnu, NSTimestamp dCreation, NSTimestamp dModification, String temValide) {
		EOCnu eo = (EOCnu) createAndInsertInstance(editingContext);
		eo.setCSectionCnu(cSectionCnu);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOCnu.
	 *
	 * @param editingContext
	 * @return EOCnu
	 */
	public static EOCnu create(EOEditingContext editingContext) {
		EOCnu eo = (EOCnu) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOCnu localInstanceIn(EOEditingContext editingContext) {
		EOCnu localInstance = (EOCnu) localInstanceOfObject(editingContext, (EOCnu) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOCnu localInstanceIn(EOEditingContext editingContext, EOCnu eo) {
		EOCnu localInstance = (eo == null) ? null : (EOCnu) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cSectionCnu() {
		return (String) storedValueForKey("cSectionCnu");
	}

	public void setCSectionCnu(String value) {
		takeStoredValueForKey(value, "cSectionCnu");
	}
	public String cSousSectionCnu() {
		return (String) storedValueForKey("cSousSectionCnu");
	}

	public void setCSousSectionCnu(String value) {
		takeStoredValueForKey(value, "cSousSectionCnu");
	}
	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}
	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	public String groupeCnu() {
		return (String) storedValueForKey("groupeCnu");
	}

	public void setGroupeCnu(String value) {
		takeStoredValueForKey(value, "groupeCnu");
	}
	public String lcSectionCnu() {
		return (String) storedValueForKey("lcSectionCnu");
	}

	public void setLcSectionCnu(String value) {
		takeStoredValueForKey(value, "lcSectionCnu");
	}
	public String llSectionCnu() {
		return (String) storedValueForKey("llSectionCnu");
	}

	public void setLlSectionCnu(String value) {
		takeStoredValueForKey(value, "llSectionCnu");
	}
	public String temComposee() {
		return (String) storedValueForKey("temComposee");
	}

	public void setTemComposee(String value) {
		takeStoredValueForKey(value, "temComposee");
	}
	public String temValide() {
		return (String) storedValueForKey("temValide");
	}

	public void setTemValide(String value) {
		takeStoredValueForKey(value, "temValide");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOCnu.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOCnu.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOCnu)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOCnu fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCnu fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOCnu eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOCnu)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOCnu fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOCnu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOCnu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOCnu eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOCnu)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOCnu fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOCnu fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOCnu eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOCnu ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOCnu createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOCnu.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOCnu.ENTITY_NAME + "' !");
		}
		else {
			EOCnu object = (EOCnu) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOCnu localInstanceOfObject(EOEditingContext ec, EOCnu object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOCnu " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOCnu) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
