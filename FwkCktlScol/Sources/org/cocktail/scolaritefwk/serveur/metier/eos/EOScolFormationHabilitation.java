/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.metier.eos;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.exception.ScolFormationHabilitationException;

import com.webobjects.foundation.NSValidation;

public class EOScolFormationHabilitation extends _EOScolFormationHabilitation {

	// Valeurs possibles pour FHAB_OUVERT
	public static final String  HABILITATION_FERMEE = "N";
	public static final String  HABILITATION_OUVERTE = "O";

    public EOScolFormationHabilitation() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
		// champs obligatoires
		if (toFwkScolarite_ScolFormationAnnee() == null) {
			throw new ScolFormationHabilitationException("L'habilitation n'a pas d'annee (scolFormationAnnee)");
		}
		if (toFwkScolarite_ScolFormationSpecialisation() == null) {
			throw new ScolFormationHabilitationException("L'habilitation n'a pas de formation (scolFormationSpecialisation)");
		}
		if (toFwkScolarite_ScolFormationAnnee().fannKey() == null) {
			throw new ScolFormationHabilitationException("Annee de l'habilitation obligatoire");
		}
		if (toFwkScolarite_ScolFormationSpecialisation().fspnKey() == null) {
			throw new ScolFormationHabilitationException("Formation de l'habilitation obligatoire");
		}
		if (fhabNiveau() == null) {
			throw new ScolFormationHabilitationException("Niveau de l'habilitation obligatoire");
		}
		if (fhabOuvert() == null) {
			throw new ScolFormationHabilitationException("Temoin d'ouverture de l'habilitation obligatoire");
		}		
		// champs obligatoires conditionnels
		// verifications
		if ((fhabOuvert().equals(HABILITATION_OUVERTE) == false) 
				&& (fhabOuvert().equals(HABILITATION_FERMEE) == false)) {
			throw new ScolFormationHabilitationException("Le temoin d'ouverture associe est inconnu");
		}
		if (fhabNiveau().intValue() <= 0) {
			throw new ScolFormationHabilitationException("Niveau de l'habilitation doit etre superieur a 0");
		}
		// particularites, actions
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

	/*
	 * Gestion des ouvertures d'habilitation 
	 */
	public Boolean validite() {
		return Boolean.valueOf(StringCtrl.toBool(fhabOuvert()));
	}

	public boolean isHabilitationOuverte() {
		return this.fhabOuvert() != null && this.fhabOuvert().equals(EOScolFormationHabilitation.HABILITATION_OUVERTE);
	}

	public boolean isHabilitationFermee() {
		return this.fhabOuvert() != null && this.fhabOuvert().equals(EOScolFormationHabilitation.HABILITATION_FERMEE);
	}

}
