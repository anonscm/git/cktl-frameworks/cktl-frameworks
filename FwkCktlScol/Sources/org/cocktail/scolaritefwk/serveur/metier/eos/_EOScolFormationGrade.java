/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolFormationGrade.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolFormationGrade extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolFormationGrade";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_FORMATION_GRADE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "fgraCode";

	public static final ERXKey<String> FGRA_ABREVIATION = new ERXKey<String>("fgraAbreviation");
	public static final ERXKey<String> FGRA_CODAGE = new ERXKey<String>("fgraCodage");
	public static final ERXKey<String> FGRA_CODE = new ERXKey<String>("fgraCode");
	public static final ERXKey<String> FGRA_LIBELLE = new ERXKey<String>("fgraLibelle");
	public static final ERXKey<String> FGRA_VALIDITE = new ERXKey<String>("fgraValidite");

	public static final String FGRA_ABREVIATION_KEY = "fgraAbreviation";
	public static final String FGRA_CODAGE_KEY = "fgraCodage";
	public static final String FGRA_CODE_KEY = "fgraCode";
	public static final String FGRA_LIBELLE_KEY = "fgraLibelle";
	public static final String FGRA_VALIDITE_KEY = "fgraValidite";

	// Non visible attributes

	// Colkeys
	public static final String FGRA_ABREVIATION_COLKEY = "FGRA_ABREVIATION";
	public static final String FGRA_CODAGE_COLKEY = "FGRA_CODAGE";
	public static final String FGRA_CODE_COLKEY = "FGRA_CODE";
	public static final String FGRA_LIBELLE_COLKEY = "FGRA_LIBELLE";
	public static final String FGRA_VALIDITE_COLKEY = "FGRA_VALIDITE";

	// Non visible colkeys

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome> TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOMES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome>("toFwkScolarite_ScolFormationDiplomes");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOMES_KEY = "toFwkScolarite_ScolFormationDiplomes";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolFormationGrade with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fgraCode
	 * @param fgraLibelle
	 * @param fgraValidite
	 * @return EOScolFormationGrade
	 */
	public static EOScolFormationGrade create(EOEditingContext editingContext, String fgraCode, String fgraLibelle, String fgraValidite) {
		EOScolFormationGrade eo = (EOScolFormationGrade) createAndInsertInstance(editingContext);
		eo.setFgraCode(fgraCode);
		eo.setFgraLibelle(fgraLibelle);
		eo.setFgraValidite(fgraValidite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolFormationGrade.
	 *
	 * @param editingContext
	 * @return EOScolFormationGrade
	 */
	public static EOScolFormationGrade create(EOEditingContext editingContext) {
		EOScolFormationGrade eo = (EOScolFormationGrade) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolFormationGrade localInstanceIn(EOEditingContext editingContext) {
		EOScolFormationGrade localInstance = (EOScolFormationGrade) localInstanceOfObject(editingContext, (EOScolFormationGrade) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolFormationGrade localInstanceIn(EOEditingContext editingContext, EOScolFormationGrade eo) {
		EOScolFormationGrade localInstance = (eo == null) ? null : (EOScolFormationGrade) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String fgraAbreviation() {
		return (String) storedValueForKey("fgraAbreviation");
	}

	public void setFgraAbreviation(String value) {
		takeStoredValueForKey(value, "fgraAbreviation");
	}
	public String fgraCodage() {
		return (String) storedValueForKey("fgraCodage");
	}

	public void setFgraCodage(String value) {
		takeStoredValueForKey(value, "fgraCodage");
	}
	public String fgraCode() {
		return (String) storedValueForKey("fgraCode");
	}

	public void setFgraCode(String value) {
		takeStoredValueForKey(value, "fgraCode");
	}
	public String fgraLibelle() {
		return (String) storedValueForKey("fgraLibelle");
	}

	public void setFgraLibelle(String value) {
		takeStoredValueForKey(value, "fgraLibelle");
	}
	public String fgraValidite() {
		return (String) storedValueForKey("fgraValidite");
	}

	public void setFgraValidite(String value) {
		takeStoredValueForKey(value, "fgraValidite");
	}

	public NSArray toFwkScolarite_ScolFormationDiplomes() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolFormationDiplomes");
	}

	public NSArray toFwkScolarite_ScolFormationDiplomes(EOQualifier qualifier) {
		return toFwkScolarite_ScolFormationDiplomes(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolFormationDiplomes(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolFormationDiplomes(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolFormationDiplomes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolFormationDiplomes();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolFormationDiplomesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDiplomes");
	}

	public void removeFromToFwkScolarite_ScolFormationDiplomesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDiplomes");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome createToFwkScolarite_ScolFormationDiplomesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolFormationDiplome");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolFormationDiplomes");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome) eo;
	}

	public void deleteToFwkScolarite_ScolFormationDiplomesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationDiplomes");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolFormationDiplomesRelationships() {
		Enumeration objects = toFwkScolarite_ScolFormationDiplomes().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolFormationDiplomesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolFormationGrade.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolFormationGrade.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolFormationGrade)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolFormationGrade fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolFormationGrade fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolFormationGrade eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolFormationGrade)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolFormationGrade fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationGrade fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationGrade fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolFormationGrade eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolFormationGrade)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationGrade fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationGrade fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolFormationGrade eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolFormationGrade ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolFormationGrade createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolFormationGrade.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolFormationGrade.ENTITY_NAME + "' !");
		}
		else {
			EOScolFormationGrade object = (EOScolFormationGrade) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolFormationGrade localInstanceOfObject(EOEditingContext ec, EOScolFormationGrade object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolFormationGrade " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolFormationGrade) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
