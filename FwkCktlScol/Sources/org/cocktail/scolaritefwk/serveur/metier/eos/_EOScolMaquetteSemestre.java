/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteSemestre.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteSemestre extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteSemestre";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_SEMESTRE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "msemKey";

	public static final ERXKey<Integer> MSEM_BONIFIABLE = new ERXKey<Integer>("msemBonifiable");
	public static final ERXKey<NSTimestamp> MSEM_DATE_DEBUT = new ERXKey<NSTimestamp>("msemDateDebut");
	public static final ERXKey<NSTimestamp> MSEM_DATE_FIN = new ERXKey<NSTimestamp>("msemDateFin");
	public static final ERXKey<java.math.BigDecimal> MSEM_HORAIRE_ETU = new ERXKey<java.math.BigDecimal>("msemHoraireEtu");
	public static final ERXKey<java.math.BigDecimal> MSEM_NOTE_BASE = new ERXKey<java.math.BigDecimal>("msemNoteBase");
	public static final ERXKey<java.math.BigDecimal> MSEM_NOTE_ELIMINATION = new ERXKey<java.math.BigDecimal>("msemNoteElimination");
	public static final ERXKey<java.math.BigDecimal> MSEM_NOTE_OBTENTION = new ERXKey<java.math.BigDecimal>("msemNoteObtention");
	public static final ERXKey<Integer> MSEM_ORDRE = new ERXKey<Integer>("msemOrdre");
	public static final ERXKey<java.math.BigDecimal> MSEM_POINTS = new ERXKey<java.math.BigDecimal>("msemPoints");
	public static final ERXKey<Integer> MSEM_POINTS_CONTRAINTE = new ERXKey<Integer>("msemPointsContrainte");
	public static final ERXKey<Integer> MSEM_SEMAINE_SESSION1 = new ERXKey<Integer>("msemSemaineSession1");
	public static final ERXKey<Integer> MSEM_SEMAINE_SESSION2 = new ERXKey<Integer>("msemSemaineSession2");

	public static final String MSEM_BONIFIABLE_KEY = "msemBonifiable";
	public static final String MSEM_DATE_DEBUT_KEY = "msemDateDebut";
	public static final String MSEM_DATE_FIN_KEY = "msemDateFin";
	public static final String MSEM_HORAIRE_ETU_KEY = "msemHoraireEtu";
	public static final String MSEM_NOTE_BASE_KEY = "msemNoteBase";
	public static final String MSEM_NOTE_ELIMINATION_KEY = "msemNoteElimination";
	public static final String MSEM_NOTE_OBTENTION_KEY = "msemNoteObtention";
	public static final String MSEM_ORDRE_KEY = "msemOrdre";
	public static final String MSEM_POINTS_KEY = "msemPoints";
	public static final String MSEM_POINTS_CONTRAINTE_KEY = "msemPointsContrainte";
	public static final String MSEM_SEMAINE_SESSION1_KEY = "msemSemaineSession1";
	public static final String MSEM_SEMAINE_SESSION2_KEY = "msemSemaineSession2";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MSEM_KEY_KEY = "msemKey";

	// Colkeys
	public static final String MSEM_BONIFIABLE_COLKEY = "MSEM_BONIFIABLE";
	public static final String MSEM_DATE_DEBUT_COLKEY = "MSEM_DATE_DEBUT";
	public static final String MSEM_DATE_FIN_COLKEY = "MSEM_DATE_FIN";
	public static final String MSEM_HORAIRE_ETU_COLKEY = "MSEM_HORAIRE_ETU";
	public static final String MSEM_NOTE_BASE_COLKEY = "MSEM_NOTE_BASE";
	public static final String MSEM_NOTE_ELIMINATION_COLKEY = "MSEM_NOTE_ELIMINATION";
	public static final String MSEM_NOTE_OBTENTION_COLKEY = "MSEM_NOTE_OBTENTION";
	public static final String MSEM_ORDRE_COLKEY = "MSEM_ORDRE";
	public static final String MSEM_POINTS_COLKEY = "MSEM_POINTS";
	public static final String MSEM_POINTS_CONTRAINTE_COLKEY = "MSEM_POINTS_CONTRAINTE";
	public static final String MSEM_SEMAINE_SESSION1_COLKEY = "MSEM_SEMAINE_SESSION1";
	public static final String MSEM_SEMAINE_SESSION2_COLKEY = "MSEM_SEMAINE_SESSION2";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MSEM_KEY_COLKEY = "MSEM_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet> TO_FWK_SCOLARITE__SCOL_GROUPE_OBJETS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet>("toFwkScolarite_ScolGroupeObjets");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation> TO_FWK_SCOLARITE__SCOL_MAQUETTE_COMPENSATIONS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation>("toFwkScolarite_ScolMaquetteCompensations");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem>("toFwkScolarite_ScolMaquetteRepartitionSems");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe>("toFwkScolarite_ScolMaquetteRepartitionUes");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UE_XS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX>("toFwkScolarite_ScolMaquetteRepartitionUeXs");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_OBJETS_KEY = "toFwkScolarite_ScolGroupeObjets";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_COMPENSATIONS_KEY = "toFwkScolarite_ScolMaquetteCompensations";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY = "toFwkScolarite_ScolMaquetteRepartitionSems";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY = "toFwkScolarite_ScolMaquetteRepartitionUes";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UE_XS_KEY = "toFwkScolarite_ScolMaquetteRepartitionUeXs";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteSemestre with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param msemBonifiable
	 * @param msemHoraireEtu
	 * @param msemNoteBase
	 * @param msemNoteObtention
	 * @param msemOrdre
	 * @param msemPoints
	 * @param msemPointsContrainte
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @return EOScolMaquetteSemestre
	 */
	public static EOScolMaquetteSemestre create(EOEditingContext editingContext, Integer msemBonifiable, java.math.BigDecimal msemHoraireEtu, java.math.BigDecimal msemNoteBase, java.math.BigDecimal msemNoteObtention, Integer msemOrdre, java.math.BigDecimal msemPoints, Integer msemPointsContrainte, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee) {
		EOScolMaquetteSemestre eo = (EOScolMaquetteSemestre) createAndInsertInstance(editingContext);
		eo.setMsemBonifiable(msemBonifiable);
		eo.setMsemHoraireEtu(msemHoraireEtu);
		eo.setMsemNoteBase(msemNoteBase);
		eo.setMsemNoteObtention(msemNoteObtention);
		eo.setMsemOrdre(msemOrdre);
		eo.setMsemPoints(msemPoints);
		eo.setMsemPointsContrainte(msemPointsContrainte);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteSemestre.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteSemestre
	 */
	public static EOScolMaquetteSemestre create(EOEditingContext editingContext) {
		EOScolMaquetteSemestre eo = (EOScolMaquetteSemestre) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteSemestre localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteSemestre localInstance = (EOScolMaquetteSemestre) localInstanceOfObject(editingContext, (EOScolMaquetteSemestre) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteSemestre localInstanceIn(EOEditingContext editingContext, EOScolMaquetteSemestre eo) {
		EOScolMaquetteSemestre localInstance = (eo == null) ? null : (EOScolMaquetteSemestre) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer msemBonifiable() {
		return (Integer) storedValueForKey("msemBonifiable");
	}

	public void setMsemBonifiable(Integer value) {
		takeStoredValueForKey(value, "msemBonifiable");
	}
	public NSTimestamp msemDateDebut() {
		return (NSTimestamp) storedValueForKey("msemDateDebut");
	}

	public void setMsemDateDebut(NSTimestamp value) {
		takeStoredValueForKey(value, "msemDateDebut");
	}
	public NSTimestamp msemDateFin() {
		return (NSTimestamp) storedValueForKey("msemDateFin");
	}

	public void setMsemDateFin(NSTimestamp value) {
		takeStoredValueForKey(value, "msemDateFin");
	}
	public java.math.BigDecimal msemHoraireEtu() {
		return (java.math.BigDecimal) storedValueForKey("msemHoraireEtu");
	}

	public void setMsemHoraireEtu(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "msemHoraireEtu");
	}
	public java.math.BigDecimal msemNoteBase() {
		return (java.math.BigDecimal) storedValueForKey("msemNoteBase");
	}

	public void setMsemNoteBase(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "msemNoteBase");
	}
	public java.math.BigDecimal msemNoteElimination() {
		return (java.math.BigDecimal) storedValueForKey("msemNoteElimination");
	}

	public void setMsemNoteElimination(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "msemNoteElimination");
	}
	public java.math.BigDecimal msemNoteObtention() {
		return (java.math.BigDecimal) storedValueForKey("msemNoteObtention");
	}

	public void setMsemNoteObtention(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "msemNoteObtention");
	}
	public Integer msemOrdre() {
		return (Integer) storedValueForKey("msemOrdre");
	}

	public void setMsemOrdre(Integer value) {
		takeStoredValueForKey(value, "msemOrdre");
	}
	public java.math.BigDecimal msemPoints() {
		return (java.math.BigDecimal) storedValueForKey("msemPoints");
	}

	public void setMsemPoints(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "msemPoints");
	}
	public Integer msemPointsContrainte() {
		return (Integer) storedValueForKey("msemPointsContrainte");
	}

	public void setMsemPointsContrainte(Integer value) {
		takeStoredValueForKey(value, "msemPointsContrainte");
	}
	public Integer msemSemaineSession1() {
		return (Integer) storedValueForKey("msemSemaineSession1");
	}

	public void setMsemSemaineSession1(Integer value) {
		takeStoredValueForKey(value, "msemSemaineSession1");
	}
	public Integer msemSemaineSession2() {
		return (Integer) storedValueForKey("msemSemaineSession2");
	}

	public void setMsemSemaineSession2(Integer value) {
		takeStoredValueForKey(value, "msemSemaineSession2");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public NSArray toFwkScolarite_ScolGroupeObjets() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolGroupeObjets");
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier) {
		return toFwkScolarite_ScolGroupeObjets(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolGroupeObjets(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolGroupeObjets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolGroupeObjets();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
	}

	public void removeFromToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet createToFwkScolarite_ScolGroupeObjetsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolGroupeObjet");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolGroupeObjets");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet) eo;
	}

	public void deleteToFwkScolarite_ScolGroupeObjetsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeObjets");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolGroupeObjetsRelationships() {
		Enumeration objects = toFwkScolarite_ScolGroupeObjets().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolGroupeObjetsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeObjet)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteCompensations() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteCompensations");
	}

	public NSArray toFwkScolarite_ScolMaquetteCompensations(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteCompensations(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteCompensations(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteCompensations(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteCompensations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteCompensations();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteCompensationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteCompensations");
	}

	public void removeFromToFwkScolarite_ScolMaquetteCompensationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteCompensations");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation createToFwkScolarite_ScolMaquetteCompensationsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteCompensation");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteCompensations");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteCompensationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteCompensations");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteCompensationsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteCompensations().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteCompensationsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteCompensation)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteRepartitionSems() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionSems");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionSems(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionSems(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionSems(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionSems(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionSems(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionSems();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionSemsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionSems");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionSemsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionSems");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem createToFwkScolarite_ScolMaquetteRepartitionSemsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionSem");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionSems");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionSemsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionSems");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionSemsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionSems().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionSemsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteRepartitionUes() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionUes");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUes(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionUes(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUes(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionUes(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionUes();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionUesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUes");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionUesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUes");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe createToFwkScolarite_ScolMaquetteRepartitionUesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionUe");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionUes");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionUesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUes");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionUesRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionUes().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionUesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteRepartitionUeXs() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionUeXs");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUeXs(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionUeXs(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUeXs(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionUeXs(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUeXs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionUeXs();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUeXs");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUeXs");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX createToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionUeX");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionUeXs");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUeXs");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionUeXsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionUeXs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionUeXsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUeX)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteSemestre.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteSemestre.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteSemestre)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteSemestre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteSemestre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteSemestre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteSemestre)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteSemestre fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteSemestre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteSemestre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteSemestre eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteSemestre)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteSemestre fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteSemestre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteSemestre eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteSemestre ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteSemestre createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteSemestre.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteSemestre.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteSemestre object = (EOScolMaquetteSemestre) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteSemestre localInstanceOfObject(EOEditingContext ec, EOScolMaquetteSemestre object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteSemestre " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteSemestre) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
