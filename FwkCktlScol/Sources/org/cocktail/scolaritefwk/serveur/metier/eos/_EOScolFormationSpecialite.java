/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolFormationSpecialite.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolFormationSpecialite extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolFormationSpecialite";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_FORMATION_SPECIALITE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "fspeCode";

	public static final ERXKey<String> FSPE_ABREVIATION = new ERXKey<String>("fspeAbreviation");
	public static final ERXKey<String> FSPE_CODE = new ERXKey<String>("fspeCode");
	public static final ERXKey<String> FSPE_LIBELLE = new ERXKey<String>("fspeLibelle");
	public static final ERXKey<String> FSPE_VALIDITE = new ERXKey<String>("fspeValidite");

	public static final String FSPE_ABREVIATION_KEY = "fspeAbreviation";
	public static final String FSPE_CODE_KEY = "fspeCode";
	public static final String FSPE_LIBELLE_KEY = "fspeLibelle";
	public static final String FSPE_VALIDITE_KEY = "fspeValidite";

	// Non visible attributes

	// Colkeys
	public static final String FSPE_ABREVIATION_COLKEY = "FSPE_ABREVIATION";
	public static final String FSPE_CODE_COLKEY = "FSPE_CODE";
	public static final String FSPE_LIBELLE_COLKEY = "FSPE_LIBELLE";
	public static final String FSPE_VALIDITE_COLKEY = "FSPE_VALIDITE";

	// Non visible colkeys

	// Relationships


	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolFormationSpecialite with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fspeCode
	 * @param fspeLibelle
	 * @param fspeValidite
	 * @return EOScolFormationSpecialite
	 */
	public static EOScolFormationSpecialite create(EOEditingContext editingContext, String fspeCode, String fspeLibelle, String fspeValidite) {
		EOScolFormationSpecialite eo = (EOScolFormationSpecialite) createAndInsertInstance(editingContext);
		eo.setFspeCode(fspeCode);
		eo.setFspeLibelle(fspeLibelle);
		eo.setFspeValidite(fspeValidite);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolFormationSpecialite.
	 *
	 * @param editingContext
	 * @return EOScolFormationSpecialite
	 */
	public static EOScolFormationSpecialite create(EOEditingContext editingContext) {
		EOScolFormationSpecialite eo = (EOScolFormationSpecialite) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolFormationSpecialite localInstanceIn(EOEditingContext editingContext) {
		EOScolFormationSpecialite localInstance = (EOScolFormationSpecialite) localInstanceOfObject(editingContext, (EOScolFormationSpecialite) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolFormationSpecialite localInstanceIn(EOEditingContext editingContext, EOScolFormationSpecialite eo) {
		EOScolFormationSpecialite localInstance = (eo == null) ? null : (EOScolFormationSpecialite) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String fspeAbreviation() {
		return (String) storedValueForKey("fspeAbreviation");
	}

	public void setFspeAbreviation(String value) {
		takeStoredValueForKey(value, "fspeAbreviation");
	}
	public String fspeCode() {
		return (String) storedValueForKey("fspeCode");
	}

	public void setFspeCode(String value) {
		takeStoredValueForKey(value, "fspeCode");
	}
	public String fspeLibelle() {
		return (String) storedValueForKey("fspeLibelle");
	}

	public void setFspeLibelle(String value) {
		takeStoredValueForKey(value, "fspeLibelle");
	}
	public String fspeValidite() {
		return (String) storedValueForKey("fspeValidite");
	}

	public void setFspeValidite(String value) {
		takeStoredValueForKey(value, "fspeValidite");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolFormationSpecialite.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolFormationSpecialite.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolFormationSpecialite)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolFormationSpecialite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolFormationSpecialite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolFormationSpecialite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolFormationSpecialite)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolFormationSpecialite fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationSpecialite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationSpecialite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolFormationSpecialite eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolFormationSpecialite)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationSpecialite fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationSpecialite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolFormationSpecialite eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolFormationSpecialite ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolFormationSpecialite createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolFormationSpecialite.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolFormationSpecialite.ENTITY_NAME + "' !");
		}
		else {
			EOScolFormationSpecialite object = (EOScolFormationSpecialite) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolFormationSpecialite localInstanceOfObject(EOEditingContext ec, EOScolFormationSpecialite object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolFormationSpecialite " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolFormationSpecialite) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
