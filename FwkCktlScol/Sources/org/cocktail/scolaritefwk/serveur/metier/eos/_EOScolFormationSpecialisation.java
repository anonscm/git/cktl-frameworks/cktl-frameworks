/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolFormationSpecialisation.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolFormationSpecialisation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolFormationSpecialisation";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_FORMATION_SPECIALISATION";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "fspnKey";

	public static final ERXKey<String> FSPN_ABREVIATION = new ERXKey<String>("fspnAbreviation");
	public static final ERXKey<Integer> FSPN_KEY = new ERXKey<Integer>("fspnKey");
	public static final ERXKey<String> FSPN_LIBELLE = new ERXKey<String>("fspnLibelle");

	public static final String FSPN_ABREVIATION_KEY = "fspnAbreviation";
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String FSPN_LIBELLE_KEY = "fspnLibelle";

	// Non visible attributes
	public static final String FDIP_CODE_KEY = "fdipCode";

	// Colkeys
	public static final String FSPN_ABREVIATION_COLKEY = "FSPN_ABREVIATION";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String FSPN_LIBELLE_COLKEY = "FSPN_LIBELLE";

	// Non visible colkeys
	public static final String FDIP_CODE_COLKEY = "FDIP_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome> TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome>("toFwkScolarite_ScolFormationDiplome");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange> TO_FWK_SCOLARITE__SCOL_FORMATION_ECHANGES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange>("toFwkScolarite_ScolFormationEchanges");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation> TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation>("toFwkScolarite_ScolFormationHabilitations");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours> TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURSS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours>("toFwkScolarite_ScolMaquetteParcourss");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY = "toFwkScolarite_ScolFormationDiplome";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ECHANGES_KEY = "toFwkScolarite_ScolFormationEchanges";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY = "toFwkScolarite_ScolFormationHabilitations";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURSS_KEY = "toFwkScolarite_ScolMaquetteParcourss";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolFormationSpecialisation with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fspnKey
	 * @param toFwkScolarite_ScolFormationDiplome
	 * @return EOScolFormationSpecialisation
	 */
	public static EOScolFormationSpecialisation create(EOEditingContext editingContext, Integer fspnKey, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toFwkScolarite_ScolFormationDiplome) {
		EOScolFormationSpecialisation eo = (EOScolFormationSpecialisation) createAndInsertInstance(editingContext);
		eo.setFspnKey(fspnKey);
		eo.setToFwkScolarite_ScolFormationDiplomeRelationship(toFwkScolarite_ScolFormationDiplome);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolFormationSpecialisation.
	 *
	 * @param editingContext
	 * @return EOScolFormationSpecialisation
	 */
	public static EOScolFormationSpecialisation create(EOEditingContext editingContext) {
		EOScolFormationSpecialisation eo = (EOScolFormationSpecialisation) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolFormationSpecialisation localInstanceIn(EOEditingContext editingContext) {
		EOScolFormationSpecialisation localInstance = (EOScolFormationSpecialisation) localInstanceOfObject(editingContext, (EOScolFormationSpecialisation) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolFormationSpecialisation localInstanceIn(EOEditingContext editingContext, EOScolFormationSpecialisation eo) {
		EOScolFormationSpecialisation localInstance = (eo == null) ? null : (EOScolFormationSpecialisation) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String fspnAbreviation() {
		return (String) storedValueForKey("fspnAbreviation");
	}

	public void setFspnAbreviation(String value) {
		takeStoredValueForKey(value, "fspnAbreviation");
	}
	public Integer fspnKey() {
		return (Integer) storedValueForKey("fspnKey");
	}

	public void setFspnKey(Integer value) {
		takeStoredValueForKey(value, "fspnKey");
	}
	public String fspnLibelle() {
		return (String) storedValueForKey("fspnLibelle");
	}

	public void setFspnLibelle(String value) {
		takeStoredValueForKey(value, "fspnLibelle");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toFwkScolarite_ScolFormationDiplome() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome)storedValueForKey("toFwkScolarite_ScolFormationDiplome");
	}

	public void setToFwkScolarite_ScolFormationDiplomeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome oldValue = toFwkScolarite_ScolFormationDiplome();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDiplome");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDiplome");
		}
	}
  
	public NSArray toFwkScolarite_ScolFormationEchanges() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolFormationEchanges");
	}

	public NSArray toFwkScolarite_ScolFormationEchanges(EOQualifier qualifier) {
		return toFwkScolarite_ScolFormationEchanges(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolFormationEchanges(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolFormationEchanges(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolFormationEchanges(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolFormationEchanges();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolFormationEchangesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationEchanges");
	}

	public void removeFromToFwkScolarite_ScolFormationEchangesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationEchanges");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange createToFwkScolarite_ScolFormationEchangesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolFormationEchange");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolFormationEchanges");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange) eo;
	}

	public void deleteToFwkScolarite_ScolFormationEchangesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationEchanges");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolFormationEchangesRelationships() {
		Enumeration objects = toFwkScolarite_ScolFormationEchanges().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolFormationEchangesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationEchange)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolFormationHabilitations() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolFormationHabilitations");
	}

	public NSArray toFwkScolarite_ScolFormationHabilitations(EOQualifier qualifier) {
		return toFwkScolarite_ScolFormationHabilitations(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolFormationHabilitations(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolFormationHabilitations(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolFormationHabilitations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolFormationHabilitations();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolFormationHabilitationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationHabilitations");
	}

	public void removeFromToFwkScolarite_ScolFormationHabilitationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationHabilitations");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation createToFwkScolarite_ScolFormationHabilitationsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolFormationHabilitation");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolFormationHabilitations");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation) eo;
	}

	public void deleteToFwkScolarite_ScolFormationHabilitationsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolFormationHabilitations");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolFormationHabilitationsRelationships() {
		Enumeration objects = toFwkScolarite_ScolFormationHabilitations().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolFormationHabilitationsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteParcourss() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteParcourss");
	}

	public NSArray toFwkScolarite_ScolMaquetteParcourss(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteParcourss(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteParcourss(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteParcourss(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteParcourss(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteParcourss();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteParcourssRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteParcourss");
	}

	public void removeFromToFwkScolarite_ScolMaquetteParcourssRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteParcourss");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours createToFwkScolarite_ScolMaquetteParcourssRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteParcours");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteParcourss");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteParcourssRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteParcourss");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteParcourssRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteParcourss().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteParcourssRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolFormationSpecialisation.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolFormationSpecialisation.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolFormationSpecialisation)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolFormationSpecialisation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolFormationSpecialisation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolFormationSpecialisation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolFormationSpecialisation)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolFormationSpecialisation fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationSpecialisation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationSpecialisation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolFormationSpecialisation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolFormationSpecialisation)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationSpecialisation fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationSpecialisation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolFormationSpecialisation eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolFormationSpecialisation ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolFormationSpecialisation createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolFormationSpecialisation.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolFormationSpecialisation.ENTITY_NAME + "' !");
		}
		else {
			EOScolFormationSpecialisation object = (EOScolFormationSpecialisation) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolFormationSpecialisation localInstanceOfObject(EOEditingContext ec, EOScolFormationSpecialisation object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolFormationSpecialisation " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolFormationSpecialisation) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
