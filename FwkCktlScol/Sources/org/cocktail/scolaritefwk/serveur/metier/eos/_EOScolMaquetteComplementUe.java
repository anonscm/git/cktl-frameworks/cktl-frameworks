/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteComplementUe.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteComplementUe extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteComplementUe";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_COMPLEMENT_UE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mueKey";

	public static final ERXKey<Integer> MCUE_ADMISSION_DESC = new ERXKey<Integer>("mcueAdmissionDesc");
	public static final ERXKey<Integer> MCUE_AIDES_ETUD = new ERXKey<Integer>("mcueAidesEtud");
	public static final ERXKey<Integer> MCUE_BESOINS_PARTICULIERS = new ERXKey<Integer>("mcueBesoinsParticuliers");
	public static final ERXKey<Integer> MCUE_BIBLIOGRAPHIE = new ERXKey<Integer>("mcueBibliographie");
	public static final ERXKey<Integer> MCUE_CONTENU = new ERXKey<Integer>("mcueContenu");
	public static final ERXKey<Integer> MCUE_EVALUATION = new ERXKey<Integer>("mcueEvaluation");
	public static final ERXKey<Integer> MCUE_OBJECTIFS = new ERXKey<Integer>("mcueObjectifs");
	public static final ERXKey<Integer> MCUE_ORGANISATION = new ERXKey<Integer>("mcueOrganisation");
	public static final ERXKey<Integer> MCUE_PREREQUIS = new ERXKey<Integer>("mcuePrerequis");
	public static final ERXKey<Integer> MCUE_PUBLIC_CIBLE = new ERXKey<Integer>("mcuePublicCible");
	public static final ERXKey<Integer> MCUE_RECOMMANDATIONS = new ERXKey<Integer>("mcueRecommandations");
	public static final ERXKey<String> MCUE_SEMESTRES = new ERXKey<String>("mcueSemestres");

	public static final String MCUE_ADMISSION_DESC_KEY = "mcueAdmissionDesc";
	public static final String MCUE_AIDES_ETUD_KEY = "mcueAidesEtud";
	public static final String MCUE_BESOINS_PARTICULIERS_KEY = "mcueBesoinsParticuliers";
	public static final String MCUE_BIBLIOGRAPHIE_KEY = "mcueBibliographie";
	public static final String MCUE_CONTENU_KEY = "mcueContenu";
	public static final String MCUE_EVALUATION_KEY = "mcueEvaluation";
	public static final String MCUE_OBJECTIFS_KEY = "mcueObjectifs";
	public static final String MCUE_ORGANISATION_KEY = "mcueOrganisation";
	public static final String MCUE_PREREQUIS_KEY = "mcuePrerequis";
	public static final String MCUE_PUBLIC_CIBLE_KEY = "mcuePublicCible";
	public static final String MCUE_RECOMMANDATIONS_KEY = "mcueRecommandations";
	public static final String MCUE_SEMESTRES_KEY = "mcueSemestres";

	// Non visible attributes
	public static final String CLOC_KEY_KEY = "clocKey";
	public static final String COMP_CODE_KEY = "compCode";
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MUE_KEY_KEY = "mueKey";

	// Colkeys
	public static final String MCUE_ADMISSION_DESC_COLKEY = "MCUE_ADMISSION_DESC";
	public static final String MCUE_AIDES_ETUD_COLKEY = "MCUE_AIDES_ETUD";
	public static final String MCUE_BESOINS_PARTICULIERS_COLKEY = "MCUE_BESOINS_PARTICULIERS";
	public static final String MCUE_BIBLIOGRAPHIE_COLKEY = "MCUE_BIBLIOGRAPHIE";
	public static final String MCUE_CONTENU_COLKEY = "MCUE_CONTENU";
	public static final String MCUE_EVALUATION_COLKEY = "MCUE_EVALUATION";
	public static final String MCUE_OBJECTIFS_COLKEY = "MCUE_OBJECTIFS";
	public static final String MCUE_ORGANISATION_COLKEY = "MCUE_ORGANISATION";
	public static final String MCUE_PREREQUIS_COLKEY = "MCUE_PREREQUIS";
	public static final String MCUE_PUBLIC_CIBLE_COLKEY = "MCUE_PUBLIC_CIBLE";
	public static final String MCUE_RECOMMANDATIONS_COLKEY = "MCUE_RECOMMANDATIONS";
	public static final String MCUE_SEMESTRES_COLKEY = "MCUE_SEMESTRES";

	// Non visible colkeys
	public static final String CLOC_KEY_COLKEY = "CLOC_KEY";
	public static final String COMP_CODE_COLKEY = "COMP_CODE";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MUE_KEY_COLKEY = "MUE_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation> TO_FWK_SCOLARITE__SCOL_CONSTANTE_LOCALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation>("toFwkScolarite_ScolConstanteLocalisation");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe> TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe>("toFwkScolarite_ScolMaquetteUe");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite> TO_FWK_SCOLARIX_V_COMPOSANTE_SCOLARITES = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite>("toFwkScolarix_VComposanteScolarites");

	public static final String TO_FWK_SCOLARITE__SCOL_CONSTANTE_LOCALISATION_KEY = "toFwkScolarite_ScolConstanteLocalisation";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY = "toFwkScolarite_ScolMaquetteUe";
	public static final String TO_FWK_SCOLARIX_V_COMPOSANTE_SCOLARITES_KEY = "toFwkScolarix_VComposanteScolarites";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteComplementUe with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mcueBibliographie
	 * @param mcueContenu
	 * @param mcueEvaluation
	 * @param mcueObjectifs
	 * @param mcueOrganisation
	 * @param mcuePrerequis
	 * @param mcueRecommandations
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteUe
	 * @return EOScolMaquetteComplementUe
	 */
	public static EOScolMaquetteComplementUe create(EOEditingContext editingContext, Integer mcueBibliographie, Integer mcueContenu, Integer mcueEvaluation, Integer mcueObjectifs, Integer mcueOrganisation, Integer mcuePrerequis, Integer mcueRecommandations, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe toFwkScolarite_ScolMaquetteUe) {
		EOScolMaquetteComplementUe eo = (EOScolMaquetteComplementUe) createAndInsertInstance(editingContext);
		eo.setMcueBibliographie(mcueBibliographie);
		eo.setMcueContenu(mcueContenu);
		eo.setMcueEvaluation(mcueEvaluation);
		eo.setMcueObjectifs(mcueObjectifs);
		eo.setMcueOrganisation(mcueOrganisation);
		eo.setMcuePrerequis(mcuePrerequis);
		eo.setMcueRecommandations(mcueRecommandations);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteUeRelationship(toFwkScolarite_ScolMaquetteUe);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteComplementUe.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteComplementUe
	 */
	public static EOScolMaquetteComplementUe create(EOEditingContext editingContext) {
		EOScolMaquetteComplementUe eo = (EOScolMaquetteComplementUe) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteComplementUe localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteComplementUe localInstance = (EOScolMaquetteComplementUe) localInstanceOfObject(editingContext, (EOScolMaquetteComplementUe) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteComplementUe localInstanceIn(EOEditingContext editingContext, EOScolMaquetteComplementUe eo) {
		EOScolMaquetteComplementUe localInstance = (eo == null) ? null : (EOScolMaquetteComplementUe) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mcueAdmissionDesc() {
		return (Integer) storedValueForKey("mcueAdmissionDesc");
	}

	public void setMcueAdmissionDesc(Integer value) {
		takeStoredValueForKey(value, "mcueAdmissionDesc");
	}
	public Integer mcueAidesEtud() {
		return (Integer) storedValueForKey("mcueAidesEtud");
	}

	public void setMcueAidesEtud(Integer value) {
		takeStoredValueForKey(value, "mcueAidesEtud");
	}
	public Integer mcueBesoinsParticuliers() {
		return (Integer) storedValueForKey("mcueBesoinsParticuliers");
	}

	public void setMcueBesoinsParticuliers(Integer value) {
		takeStoredValueForKey(value, "mcueBesoinsParticuliers");
	}
	public Integer mcueBibliographie() {
		return (Integer) storedValueForKey("mcueBibliographie");
	}

	public void setMcueBibliographie(Integer value) {
		takeStoredValueForKey(value, "mcueBibliographie");
	}
	public Integer mcueContenu() {
		return (Integer) storedValueForKey("mcueContenu");
	}

	public void setMcueContenu(Integer value) {
		takeStoredValueForKey(value, "mcueContenu");
	}
	public Integer mcueEvaluation() {
		return (Integer) storedValueForKey("mcueEvaluation");
	}

	public void setMcueEvaluation(Integer value) {
		takeStoredValueForKey(value, "mcueEvaluation");
	}
	public Integer mcueObjectifs() {
		return (Integer) storedValueForKey("mcueObjectifs");
	}

	public void setMcueObjectifs(Integer value) {
		takeStoredValueForKey(value, "mcueObjectifs");
	}
	public Integer mcueOrganisation() {
		return (Integer) storedValueForKey("mcueOrganisation");
	}

	public void setMcueOrganisation(Integer value) {
		takeStoredValueForKey(value, "mcueOrganisation");
	}
	public Integer mcuePrerequis() {
		return (Integer) storedValueForKey("mcuePrerequis");
	}

	public void setMcuePrerequis(Integer value) {
		takeStoredValueForKey(value, "mcuePrerequis");
	}
	public Integer mcuePublicCible() {
		return (Integer) storedValueForKey("mcuePublicCible");
	}

	public void setMcuePublicCible(Integer value) {
		takeStoredValueForKey(value, "mcuePublicCible");
	}
	public Integer mcueRecommandations() {
		return (Integer) storedValueForKey("mcueRecommandations");
	}

	public void setMcueRecommandations(Integer value) {
		takeStoredValueForKey(value, "mcueRecommandations");
	}
	public String mcueSemestres() {
		return (String) storedValueForKey("mcueSemestres");
	}

	public void setMcueSemestres(String value) {
		takeStoredValueForKey(value, "mcueSemestres");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation toFwkScolarite_ScolConstanteLocalisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation)storedValueForKey("toFwkScolarite_ScolConstanteLocalisation");
	}

	public void setToFwkScolarite_ScolConstanteLocalisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteLocalisation oldValue = toFwkScolarite_ScolConstanteLocalisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolConstanteLocalisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolConstanteLocalisation");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe toFwkScolarite_ScolMaquetteUe() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe)storedValueForKey("toFwkScolarite_ScolMaquetteUe");
	}

	public void setToFwkScolarite_ScolMaquetteUeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe oldValue = toFwkScolarite_ScolMaquetteUe();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteUe");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteUe");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite toFwkScolarix_VComposanteScolarites() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite)storedValueForKey("toFwkScolarix_VComposanteScolarites");
	}

	public void setToFwkScolarix_VComposanteScolaritesRelationship(org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite oldValue = toFwkScolarix_VComposanteScolarites();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarix_VComposanteScolarites");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarix_VComposanteScolarites");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteComplementUe.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteComplementUe.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteComplementUe)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteComplementUe fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteComplementUe fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteComplementUe eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteComplementUe)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteComplementUe fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteComplementUe fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteComplementUe fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteComplementUe eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteComplementUe)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteComplementUe fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteComplementUe fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteComplementUe eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteComplementUe ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteComplementUe createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteComplementUe.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteComplementUe.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteComplementUe object = (EOScolMaquetteComplementUe) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteComplementUe localInstanceOfObject(EOEditingContext ec, EOScolMaquetteComplementUe object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteComplementUe " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteComplementUe) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
