/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolDroitDiplome.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolDroitDiplome extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolDroitDiplome";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_DROIT_DIPLOME";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "ddipKey";

	public static final ERXKey<String> DDIP_BILAN = new ERXKey<String>("ddipBilan");
	public static final ERXKey<String> DDIP_CHARGES = new ERXKey<String>("ddipCharges");
	public static final ERXKey<String> DDIP_COMPLEMENTS = new ERXKey<String>("ddipComplements");
	public static final ERXKey<String> DDIP_EDT = new ERXKey<String>("ddipEdt");
	public static final ERXKey<String> DDIP_EXAMENS = new ERXKey<String>("ddipExamens");
	public static final ERXKey<String> DDIP_GROUPES = new ERXKey<String>("ddipGroupes");
	public static final ERXKey<String> DDIP_IPEDAGOGIQUES = new ERXKey<String>("ddipIpedagogiques");
	public static final ERXKey<String> DDIP_MAQUETTES = new ERXKey<String>("ddipMaquettes");
	public static final ERXKey<String> DDIP_STATISTIQUES = new ERXKey<String>("ddipStatistiques");

	public static final String DDIP_BILAN_KEY = "ddipBilan";
	public static final String DDIP_CHARGES_KEY = "ddipCharges";
	public static final String DDIP_COMPLEMENTS_KEY = "ddipComplements";
	public static final String DDIP_EDT_KEY = "ddipEdt";
	public static final String DDIP_EXAMENS_KEY = "ddipExamens";
	public static final String DDIP_GROUPES_KEY = "ddipGroupes";
	public static final String DDIP_IPEDAGOGIQUES_KEY = "ddipIpedagogiques";
	public static final String DDIP_MAQUETTES_KEY = "ddipMaquettes";
	public static final String DDIP_STATISTIQUES_KEY = "ddipStatistiques";

	// Non visible attributes
	public static final String DDIP_KEY_KEY = "ddipKey";
	public static final String DLOG_KEY_KEY = "dlogKey";
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FHAB_KEY_KEY = "fhabKey";

	// Colkeys
	public static final String DDIP_BILAN_COLKEY = "DDIP_BILAN";
	public static final String DDIP_CHARGES_COLKEY = "DDIP_CHARGES";
	public static final String DDIP_COMPLEMENTS_COLKEY = "DDIP_COMPLEMENTS";
	public static final String DDIP_EDT_COLKEY = "DDIP_EDT";
	public static final String DDIP_EXAMENS_COLKEY = "DDIP_EXAMENS";
	public static final String DDIP_GROUPES_COLKEY = "DDIP_GROUPES";
	public static final String DDIP_IPEDAGOGIQUES_COLKEY = "DDIP_IPEDAGOGIQUES";
	public static final String DDIP_MAQUETTES_COLKEY = "DDIP_MAQUETTES";
	public static final String DDIP_STATISTIQUES_COLKEY = "DDIP_STATISTIQUES";

	// Non visible colkeys
	public static final String DDIP_KEY_COLKEY = "DDIP_KEY";
	public static final String DLOG_KEY_COLKEY = "DLOG_KEY";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FHAB_KEY_COLKEY = "FHAB_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin> TO_FWK_SCOLARITE__SCOL_DROIT_LOGIN = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin>("toFwkScolarite_ScolDroitLogin");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation> TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation>("toFwkScolarite_ScolFormationHabilitation");

	public static final String TO_FWK_SCOLARITE__SCOL_DROIT_LOGIN_KEY = "toFwkScolarite_ScolDroitLogin";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATION_KEY = "toFwkScolarite_ScolFormationHabilitation";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolDroitDiplome with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param ddipBilan
	 * @param ddipCharges
	 * @param ddipComplements
	 * @param ddipEdt
	 * @param ddipExamens
	 * @param ddipGroupes
	 * @param ddipIpedagogiques
	 * @param ddipMaquettes
	 * @param ddipStatistiques
	 * @param toFwkScolarite_ScolDroitLogin
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationHabilitation
	 * @return EOScolDroitDiplome
	 */
	public static EOScolDroitDiplome create(EOEditingContext editingContext, String ddipBilan, String ddipCharges, String ddipComplements, String ddipEdt, String ddipExamens, String ddipGroupes, String ddipIpedagogiques, String ddipMaquettes, String ddipStatistiques, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin toFwkScolarite_ScolDroitLogin, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation toFwkScolarite_ScolFormationHabilitation) {
		EOScolDroitDiplome eo = (EOScolDroitDiplome) createAndInsertInstance(editingContext);
		eo.setDdipBilan(ddipBilan);
		eo.setDdipCharges(ddipCharges);
		eo.setDdipComplements(ddipComplements);
		eo.setDdipEdt(ddipEdt);
		eo.setDdipExamens(ddipExamens);
		eo.setDdipGroupes(ddipGroupes);
		eo.setDdipIpedagogiques(ddipIpedagogiques);
		eo.setDdipMaquettes(ddipMaquettes);
		eo.setDdipStatistiques(ddipStatistiques);
		eo.setToFwkScolarite_ScolDroitLoginRelationship(toFwkScolarite_ScolDroitLogin);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationHabilitationRelationship(toFwkScolarite_ScolFormationHabilitation);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolDroitDiplome.
	 *
	 * @param editingContext
	 * @return EOScolDroitDiplome
	 */
	public static EOScolDroitDiplome create(EOEditingContext editingContext) {
		EOScolDroitDiplome eo = (EOScolDroitDiplome) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolDroitDiplome localInstanceIn(EOEditingContext editingContext) {
		EOScolDroitDiplome localInstance = (EOScolDroitDiplome) localInstanceOfObject(editingContext, (EOScolDroitDiplome) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolDroitDiplome localInstanceIn(EOEditingContext editingContext, EOScolDroitDiplome eo) {
		EOScolDroitDiplome localInstance = (eo == null) ? null : (EOScolDroitDiplome) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String ddipBilan() {
		return (String) storedValueForKey("ddipBilan");
	}

	public void setDdipBilan(String value) {
		takeStoredValueForKey(value, "ddipBilan");
	}
	public String ddipCharges() {
		return (String) storedValueForKey("ddipCharges");
	}

	public void setDdipCharges(String value) {
		takeStoredValueForKey(value, "ddipCharges");
	}
	public String ddipComplements() {
		return (String) storedValueForKey("ddipComplements");
	}

	public void setDdipComplements(String value) {
		takeStoredValueForKey(value, "ddipComplements");
	}
	public String ddipEdt() {
		return (String) storedValueForKey("ddipEdt");
	}

	public void setDdipEdt(String value) {
		takeStoredValueForKey(value, "ddipEdt");
	}
	public String ddipExamens() {
		return (String) storedValueForKey("ddipExamens");
	}

	public void setDdipExamens(String value) {
		takeStoredValueForKey(value, "ddipExamens");
	}
	public String ddipGroupes() {
		return (String) storedValueForKey("ddipGroupes");
	}

	public void setDdipGroupes(String value) {
		takeStoredValueForKey(value, "ddipGroupes");
	}
	public String ddipIpedagogiques() {
		return (String) storedValueForKey("ddipIpedagogiques");
	}

	public void setDdipIpedagogiques(String value) {
		takeStoredValueForKey(value, "ddipIpedagogiques");
	}
	public String ddipMaquettes() {
		return (String) storedValueForKey("ddipMaquettes");
	}

	public void setDdipMaquettes(String value) {
		takeStoredValueForKey(value, "ddipMaquettes");
	}
	public String ddipStatistiques() {
		return (String) storedValueForKey("ddipStatistiques");
	}

	public void setDdipStatistiques(String value) {
		takeStoredValueForKey(value, "ddipStatistiques");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin toFwkScolarite_ScolDroitLogin() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin)storedValueForKey("toFwkScolarite_ScolDroitLogin");
	}

	public void setToFwkScolarite_ScolDroitLoginRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin oldValue = toFwkScolarite_ScolDroitLogin();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolDroitLogin");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolDroitLogin");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation toFwkScolarite_ScolFormationHabilitation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation)storedValueForKey("toFwkScolarite_ScolFormationHabilitation");
	}

	public void setToFwkScolarite_ScolFormationHabilitationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation oldValue = toFwkScolarite_ScolFormationHabilitation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationHabilitation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationHabilitation");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolDroitDiplome.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolDroitDiplome.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolDroitDiplome)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolDroitDiplome fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolDroitDiplome fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolDroitDiplome eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolDroitDiplome)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolDroitDiplome fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolDroitDiplome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolDroitDiplome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolDroitDiplome eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolDroitDiplome)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolDroitDiplome fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolDroitDiplome fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolDroitDiplome eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolDroitDiplome ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolDroitDiplome createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolDroitDiplome.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolDroitDiplome.ENTITY_NAME + "' !");
		}
		else {
			EOScolDroitDiplome object = (EOScolDroitDiplome) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolDroitDiplome localInstanceOfObject(EOEditingContext ec, EOScolDroitDiplome object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolDroitDiplome " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolDroitDiplome) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
