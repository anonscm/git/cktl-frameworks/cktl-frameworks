/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolC2iEntete.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolC2iEntete extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolC2iEntete";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_C2I_ENTETE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "c2ieKey";

	public static final ERXKey<String> C2IE_ABREVIATION = new ERXKey<String>("c2ieAbreviation");
	public static final ERXKey<String> C2IE_CODE = new ERXKey<String>("c2ieCode");
	public static final ERXKey<String> C2IE_LIBELLE = new ERXKey<String>("c2ieLibelle");
	public static final ERXKey<String> C2IE_NIVEAU = new ERXKey<String>("c2ieNiveau");

	public static final String C2IE_ABREVIATION_KEY = "c2ieAbreviation";
	public static final String C2IE_CODE_KEY = "c2ieCode";
	public static final String C2IE_LIBELLE_KEY = "c2ieLibelle";
	public static final String C2IE_NIVEAU_KEY = "c2ieNiveau";

	// Non visible attributes
	public static final String C2IE_KEY_KEY = "c2ieKey";

	// Colkeys
	public static final String C2IE_ABREVIATION_COLKEY = "C2IE_ABREVIATION";
	public static final String C2IE_CODE_COLKEY = "C2IE_CODE";
	public static final String C2IE_LIBELLE_COLKEY = "C2IE_LIBELLE";
	public static final String C2IE_NIVEAU_COLKEY = "C2IE_NIVEAU";

	// Non visible colkeys
	public static final String C2IE_KEY_COLKEY = "C2IE_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence> TO_FWK_SCOLARITE__SCOL_C2I_COMPETENCES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence>("toFwkScolarite_ScolC2iCompetences");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes> TO_FWK_SCOLARITE__SCOL_C2I_CONSTANTESS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes>("toFwkScolarite_ScolC2iConstantess");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury> TO_FWK_SCOLARITE__SCOL_C2I_JURYS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury>("toFwkScolarite_ScolC2iJurys");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc> TO_FWK_SCOLARITE__SCOL_C2I_REPART_ECS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc>("toFwkScolarite_ScolC2iRepartEcs");

	public static final String TO_FWK_SCOLARITE__SCOL_C2I_COMPETENCES_KEY = "toFwkScolarite_ScolC2iCompetences";
	public static final String TO_FWK_SCOLARITE__SCOL_C2I_CONSTANTESS_KEY = "toFwkScolarite_ScolC2iConstantess";
	public static final String TO_FWK_SCOLARITE__SCOL_C2I_JURYS_KEY = "toFwkScolarite_ScolC2iJurys";
	public static final String TO_FWK_SCOLARITE__SCOL_C2I_REPART_ECS_KEY = "toFwkScolarite_ScolC2iRepartEcs";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolC2iEntete with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param c2ieCode
	 * @param c2ieLibelle
	 * @param c2ieNiveau
	 * @return EOScolC2iEntete
	 */
	public static EOScolC2iEntete create(EOEditingContext editingContext, String c2ieCode, String c2ieLibelle, String c2ieNiveau) {
		EOScolC2iEntete eo = (EOScolC2iEntete) createAndInsertInstance(editingContext);
		eo.setC2ieCode(c2ieCode);
		eo.setC2ieLibelle(c2ieLibelle);
		eo.setC2ieNiveau(c2ieNiveau);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolC2iEntete.
	 *
	 * @param editingContext
	 * @return EOScolC2iEntete
	 */
	public static EOScolC2iEntete create(EOEditingContext editingContext) {
		EOScolC2iEntete eo = (EOScolC2iEntete) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolC2iEntete localInstanceIn(EOEditingContext editingContext) {
		EOScolC2iEntete localInstance = (EOScolC2iEntete) localInstanceOfObject(editingContext, (EOScolC2iEntete) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolC2iEntete localInstanceIn(EOEditingContext editingContext, EOScolC2iEntete eo) {
		EOScolC2iEntete localInstance = (eo == null) ? null : (EOScolC2iEntete) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String c2ieAbreviation() {
		return (String) storedValueForKey("c2ieAbreviation");
	}

	public void setC2ieAbreviation(String value) {
		takeStoredValueForKey(value, "c2ieAbreviation");
	}
	public String c2ieCode() {
		return (String) storedValueForKey("c2ieCode");
	}

	public void setC2ieCode(String value) {
		takeStoredValueForKey(value, "c2ieCode");
	}
	public String c2ieLibelle() {
		return (String) storedValueForKey("c2ieLibelle");
	}

	public void setC2ieLibelle(String value) {
		takeStoredValueForKey(value, "c2ieLibelle");
	}
	public String c2ieNiveau() {
		return (String) storedValueForKey("c2ieNiveau");
	}

	public void setC2ieNiveau(String value) {
		takeStoredValueForKey(value, "c2ieNiveau");
	}

	public NSArray toFwkScolarite_ScolC2iCompetences() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolC2iCompetences");
	}

	public NSArray toFwkScolarite_ScolC2iCompetences(EOQualifier qualifier) {
		return toFwkScolarite_ScolC2iCompetences(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolC2iCompetences(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolC2iCompetences(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolC2iCompetences(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence.TO_FWK_SCOLARITE__SCOL_C2I_ENTETE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolC2iCompetences();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolC2iCompetencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iCompetences");
	}

	public void removeFromToFwkScolarite_ScolC2iCompetencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iCompetences");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence createToFwkScolarite_ScolC2iCompetencesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolC2iCompetence");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolC2iCompetences");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence) eo;
	}

	public void deleteToFwkScolarite_ScolC2iCompetencesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iCompetences");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolC2iCompetencesRelationships() {
		Enumeration objects = toFwkScolarite_ScolC2iCompetences().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolC2iCompetencesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iCompetence)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolC2iConstantess() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolC2iConstantess");
	}

	public NSArray toFwkScolarite_ScolC2iConstantess(EOQualifier qualifier) {
		return toFwkScolarite_ScolC2iConstantess(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolC2iConstantess(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolC2iConstantess(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolC2iConstantess(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes.TO_FWK_SCOLARITE__SCOL_C2I_ENTETE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolC2iConstantess();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolC2iConstantessRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iConstantess");
	}

	public void removeFromToFwkScolarite_ScolC2iConstantessRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iConstantess");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes createToFwkScolarite_ScolC2iConstantessRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolC2iConstantes");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolC2iConstantess");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes) eo;
	}

	public void deleteToFwkScolarite_ScolC2iConstantessRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iConstantess");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolC2iConstantessRelationships() {
		Enumeration objects = toFwkScolarite_ScolC2iConstantess().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolC2iConstantessRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iConstantes)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolC2iJurys() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolC2iJurys");
	}

	public NSArray toFwkScolarite_ScolC2iJurys(EOQualifier qualifier) {
		return toFwkScolarite_ScolC2iJurys(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolC2iJurys(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolC2iJurys(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolC2iJurys(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury.TO_FWK_SCOLARITE__SCOL_C2I_ENTETE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolC2iJurys();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolC2iJurysRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iJurys");
	}

	public void removeFromToFwkScolarite_ScolC2iJurysRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iJurys");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury createToFwkScolarite_ScolC2iJurysRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolC2iJury");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolC2iJurys");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury) eo;
	}

	public void deleteToFwkScolarite_ScolC2iJurysRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iJurys");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolC2iJurysRelationships() {
		Enumeration objects = toFwkScolarite_ScolC2iJurys().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolC2iJurysRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iJury)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolC2iRepartEcs() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolC2iRepartEcs");
	}

	public NSArray toFwkScolarite_ScolC2iRepartEcs(EOQualifier qualifier) {
		return toFwkScolarite_ScolC2iRepartEcs(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolC2iRepartEcs(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolC2iRepartEcs(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolC2iRepartEcs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc.TO_FWK_SCOLARITE__SCOL_C2I_ENTETE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolC2iRepartEcs();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolC2iRepartEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iRepartEcs");
	}

	public void removeFromToFwkScolarite_ScolC2iRepartEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iRepartEcs");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc createToFwkScolarite_ScolC2iRepartEcsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolC2iRepartEc");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolC2iRepartEcs");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc) eo;
	}

	public void deleteToFwkScolarite_ScolC2iRepartEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolC2iRepartEcs");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolC2iRepartEcsRelationships() {
		Enumeration objects = toFwkScolarite_ScolC2iRepartEcs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolC2iRepartEcsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolC2iRepartEc)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolC2iEntete.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolC2iEntete.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolC2iEntete)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolC2iEntete fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolC2iEntete fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolC2iEntete eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolC2iEntete)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolC2iEntete fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolC2iEntete fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolC2iEntete fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolC2iEntete eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolC2iEntete)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolC2iEntete fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolC2iEntete fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolC2iEntete eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolC2iEntete ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolC2iEntete createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolC2iEntete.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolC2iEntete.ENTITY_NAME + "' !");
		}
		else {
			EOScolC2iEntete object = (EOScolC2iEntete) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolC2iEntete localInstanceOfObject(EOEditingContext ec, EOScolC2iEntete object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolC2iEntete " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolC2iEntete) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
