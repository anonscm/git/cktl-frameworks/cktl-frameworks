/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolInscriptionAp.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolInscriptionAp extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolInscriptionAp";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_INSCRIPTION_AP";

	//Attributes

	public static final ERXKey<Integer> FANN_KEY = new ERXKey<Integer>("fannKey");
	public static final ERXKey<Integer> IMRAP_DISPENSE = new ERXKey<Integer>("imrapDispense");
	public static final ERXKey<Integer> IMRAP_SEMESTRE = new ERXKey<Integer>("imrapSemestre");

	public static final String FANN_KEY_KEY = "fannKey";
	public static final String IMRAP_DISPENSE_KEY = "imrapDispense";
	public static final String IMRAP_SEMESTRE_KEY = "imrapSemestre";

	// Non visible attributes
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String MRAP_KEY_KEY = "mrapKey";

	// Colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String IMRAP_DISPENSE_COLKEY = "IMRAP_DISPENSE";
	public static final String IMRAP_SEMESTRE_COLKEY = "IMRAP_SEMESTRE";

	// Non visible colkeys
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String MRAP_KEY_COLKEY = "MRAP_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant> TO_SCOL_INSCRIPTION_ETUDIANT = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant>("toScolInscriptionEtudiant");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp> TO_SCOL_MAQUETTE_REPARTITION_AP = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp>("toScolMaquetteRepartitionAp");

	public static final String TO_SCOL_INSCRIPTION_ETUDIANT_KEY = "toScolInscriptionEtudiant";
	public static final String TO_SCOL_MAQUETTE_REPARTITION_AP_KEY = "toScolMaquetteRepartitionAp";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolInscriptionAp with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param fannKey
	 * @param imrapDispense
	 * @param imrapSemestre
	 * @param toScolInscriptionEtudiant
	 * @param toScolMaquetteRepartitionAp
	 * @return EOScolInscriptionAp
	 */
	public static EOScolInscriptionAp create(EOEditingContext editingContext, Integer fannKey, Integer imrapDispense, Integer imrapSemestre, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant toScolInscriptionEtudiant, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp toScolMaquetteRepartitionAp) {
		EOScolInscriptionAp eo = (EOScolInscriptionAp) createAndInsertInstance(editingContext);
		eo.setFannKey(fannKey);
		eo.setImrapDispense(imrapDispense);
		eo.setImrapSemestre(imrapSemestre);
		eo.setToScolInscriptionEtudiantRelationship(toScolInscriptionEtudiant);
		eo.setToScolMaquetteRepartitionApRelationship(toScolMaquetteRepartitionAp);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolInscriptionAp.
	 *
	 * @param editingContext
	 * @return EOScolInscriptionAp
	 */
	public static EOScolInscriptionAp create(EOEditingContext editingContext) {
		EOScolInscriptionAp eo = (EOScolInscriptionAp) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolInscriptionAp localInstanceIn(EOEditingContext editingContext) {
		EOScolInscriptionAp localInstance = (EOScolInscriptionAp) localInstanceOfObject(editingContext, (EOScolInscriptionAp) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolInscriptionAp localInstanceIn(EOEditingContext editingContext, EOScolInscriptionAp eo) {
		EOScolInscriptionAp localInstance = (eo == null) ? null : (EOScolInscriptionAp) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer fannKey() {
		return (Integer) storedValueForKey("fannKey");
	}

	public void setFannKey(Integer value) {
		takeStoredValueForKey(value, "fannKey");
	}
	public Integer imrapDispense() {
		return (Integer) storedValueForKey("imrapDispense");
	}

	public void setImrapDispense(Integer value) {
		takeStoredValueForKey(value, "imrapDispense");
	}
	public Integer imrapSemestre() {
		return (Integer) storedValueForKey("imrapSemestre");
	}

	public void setImrapSemestre(Integer value) {
		takeStoredValueForKey(value, "imrapSemestre");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant toScolInscriptionEtudiant() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant)storedValueForKey("toScolInscriptionEtudiant");
	}

	public void setToScolInscriptionEtudiantRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant oldValue = toScolInscriptionEtudiant();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolInscriptionEtudiant");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toScolInscriptionEtudiant");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp toScolMaquetteRepartitionAp() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp)storedValueForKey("toScolMaquetteRepartitionAp");
	}

	public void setToScolMaquetteRepartitionApRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp oldValue = toScolMaquetteRepartitionAp();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolMaquetteRepartitionAp");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toScolMaquetteRepartitionAp");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolInscriptionAp.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolInscriptionAp.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolInscriptionAp)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolInscriptionAp fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolInscriptionAp fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolInscriptionAp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolInscriptionAp)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolInscriptionAp fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionAp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionAp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolInscriptionAp eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolInscriptionAp)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionAp fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionAp fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolInscriptionAp eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolInscriptionAp ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolInscriptionAp createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolInscriptionAp.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolInscriptionAp.ENTITY_NAME + "' !");
		}
		else {
			EOScolInscriptionAp object = (EOScolInscriptionAp) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolInscriptionAp localInstanceOfObject(EOEditingContext ec, EOScolInscriptionAp object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolInscriptionAp " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolInscriptionAp) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
