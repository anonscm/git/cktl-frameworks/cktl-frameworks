/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolFormationComplementHabi.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolFormationComplementHabi extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolFormationComplementHabi";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_FORMATION_COMPLEMENT_HABI";

	//Attributes

	public static final ERXKey<Integer> FCOM_DAC_AIDES = new ERXKey<Integer>("fcomDacAides");
	public static final ERXKey<Integer> FCOM_DAC_AUTOFORMATION = new ERXKey<Integer>("fcomDacAutoformation");
	public static final ERXKey<Integer> FCOM_DAC_DESCRIPTIF = new ERXKey<Integer>("fcomDacDescriptif");
	public static final ERXKey<Integer> FCOM_DAC_DISPOSITIF = new ERXKey<Integer>("fcomDacDispositif");
	public static final ERXKey<Integer> FCOM_DAC_FOAD = new ERXKey<Integer>("fcomDacFoad");
	public static final ERXKey<Integer> FCOM_DAC_LANGUES = new ERXKey<Integer>("fcomDacLangues");
	public static final ERXKey<Integer> FCOM_DAC_PUBLICS = new ERXKey<Integer>("fcomDacPublics");
	public static final ERXKey<Integer> FCOM_DAC_TIC_TICE = new ERXKey<Integer>("fcomDacTicTice");
	public static final ERXKey<Integer> FCOM_DEV_DESCRIPTIF = new ERXKey<Integer>("fcomDevDescriptif");
	public static final ERXKey<Integer> FCOM_DEV_DISPOSITIFS = new ERXKey<Integer>("fcomDevDispositifs");
	public static final ERXKey<Integer> FCOM_FOR_DESCRIPTIF = new ERXKey<Integer>("fcomForDescriptif");
	public static final ERXKey<Integer> FCOM_IMP_DESCRIPTIF = new ERXKey<Integer>("fcomImpDescriptif");
	public static final ERXKey<Integer> FCOM_INS_AUTRES = new ERXKey<Integer>("fcomInsAutres");
	public static final ERXKey<Integer> FCOM_INS_CDD_EXTERNE = new ERXKey<Integer>("fcomInsCddExterne");
	public static final ERXKey<Integer> FCOM_INS_CDD_INTERNE = new ERXKey<Integer>("fcomInsCddInterne");
	public static final ERXKey<Integer> FCOM_INS_CDI_EXTERNE = new ERXKey<Integer>("fcomInsCdiExterne");
	public static final ERXKey<Integer> FCOM_INS_CDI_INTERNE = new ERXKey<Integer>("fcomInsCdiInterne");
	public static final ERXKey<Integer> FCOM_INS_DESCRIPTIF = new ERXKey<Integer>("fcomInsDescriptif");
	public static final ERXKey<Integer> FCOM_INS_POURSUITE_ETUDES = new ERXKey<Integer>("fcomInsPoursuiteEtudes");
	public static final ERXKey<Integer> FCOM_INS_RECHERCHE_EMPLOI = new ERXKey<Integer>("fcomInsRechercheEmploi");
	public static final ERXKey<Integer> FCOM_OBJ_ACTIVITES = new ERXKey<Integer>("fcomObjActivites");
	public static final ERXKey<Integer> FCOM_OBJ_COMPETENCES = new ERXKey<Integer>("fcomObjCompetences");
	public static final ERXKey<Integer> FCOM_OBJ_CONNAISSANCES = new ERXKey<Integer>("fcomObjConnaissances");
	public static final ERXKey<Integer> FCOM_OBJ_METIERS = new ERXKey<Integer>("fcomObjMetiers");
	public static final ERXKey<Integer> FCOM_OBJ_NIVEAU_EMPLOI = new ERXKey<Integer>("fcomObjNiveauEmploi");
	public static final ERXKey<Integer> FCOM_OBJ_POURSUITES = new ERXKey<Integer>("fcomObjPoursuites");
	public static final ERXKey<Integer> FCOM_OBJ_SITUATION = new ERXKey<Integer>("fcomObjSituation");
	public static final ERXKey<Integer> FCOM_ORG_DESCRIPTIF = new ERXKey<Integer>("fcomOrgDescriptif");
	public static final ERXKey<Integer> FCOM_ORG_EVALUATION = new ERXKey<Integer>("fcomOrgEvaluation");
	public static final ERXKey<Integer> FCOM_ORG_PASSERELLES = new ERXKey<Integer>("fcomOrgPasserelles");
	public static final ERXKey<Integer> FCOM_ORG_PROGRESSION = new ERXKey<Integer>("fcomOrgProgression");
	public static final ERXKey<Integer> FCOM_ORG_REGLEMENT = new ERXKey<Integer>("fcomOrgReglement");
	public static final ERXKey<Integer> FCOM_OUV_DESCRIPTIF = new ERXKey<Integer>("fcomOuvDescriptif");
	public static final ERXKey<Integer> FCOM_POL_ORIGINE = new ERXKey<Integer>("fcomPolOrigine");
	public static final ERXKey<Integer> FCOM_POL_PREREQUIS = new ERXKey<Integer>("fcomPolPrerequis");
	public static final ERXKey<Integer> FCOM_POL_RECOMMANDATIONS = new ERXKey<Integer>("fcomPolRecommandations");
	public static final ERXKey<Integer> FCOM_POL_RECRUTEMENT = new ERXKey<Integer>("fcomPolRecrutement");
	public static final ERXKey<Integer> FCOM_POS_DESCRIPTIF = new ERXKey<Integer>("fcomPosDescriptif");

	public static final String FCOM_DAC_AIDES_KEY = "fcomDacAides";
	public static final String FCOM_DAC_AUTOFORMATION_KEY = "fcomDacAutoformation";
	public static final String FCOM_DAC_DESCRIPTIF_KEY = "fcomDacDescriptif";
	public static final String FCOM_DAC_DISPOSITIF_KEY = "fcomDacDispositif";
	public static final String FCOM_DAC_FOAD_KEY = "fcomDacFoad";
	public static final String FCOM_DAC_LANGUES_KEY = "fcomDacLangues";
	public static final String FCOM_DAC_PUBLICS_KEY = "fcomDacPublics";
	public static final String FCOM_DAC_TIC_TICE_KEY = "fcomDacTicTice";
	public static final String FCOM_DEV_DESCRIPTIF_KEY = "fcomDevDescriptif";
	public static final String FCOM_DEV_DISPOSITIFS_KEY = "fcomDevDispositifs";
	public static final String FCOM_FOR_DESCRIPTIF_KEY = "fcomForDescriptif";
	public static final String FCOM_IMP_DESCRIPTIF_KEY = "fcomImpDescriptif";
	public static final String FCOM_INS_AUTRES_KEY = "fcomInsAutres";
	public static final String FCOM_INS_CDD_EXTERNE_KEY = "fcomInsCddExterne";
	public static final String FCOM_INS_CDD_INTERNE_KEY = "fcomInsCddInterne";
	public static final String FCOM_INS_CDI_EXTERNE_KEY = "fcomInsCdiExterne";
	public static final String FCOM_INS_CDI_INTERNE_KEY = "fcomInsCdiInterne";
	public static final String FCOM_INS_DESCRIPTIF_KEY = "fcomInsDescriptif";
	public static final String FCOM_INS_POURSUITE_ETUDES_KEY = "fcomInsPoursuiteEtudes";
	public static final String FCOM_INS_RECHERCHE_EMPLOI_KEY = "fcomInsRechercheEmploi";
	public static final String FCOM_OBJ_ACTIVITES_KEY = "fcomObjActivites";
	public static final String FCOM_OBJ_COMPETENCES_KEY = "fcomObjCompetences";
	public static final String FCOM_OBJ_CONNAISSANCES_KEY = "fcomObjConnaissances";
	public static final String FCOM_OBJ_METIERS_KEY = "fcomObjMetiers";
	public static final String FCOM_OBJ_NIVEAU_EMPLOI_KEY = "fcomObjNiveauEmploi";
	public static final String FCOM_OBJ_POURSUITES_KEY = "fcomObjPoursuites";
	public static final String FCOM_OBJ_SITUATION_KEY = "fcomObjSituation";
	public static final String FCOM_ORG_DESCRIPTIF_KEY = "fcomOrgDescriptif";
	public static final String FCOM_ORG_EVALUATION_KEY = "fcomOrgEvaluation";
	public static final String FCOM_ORG_PASSERELLES_KEY = "fcomOrgPasserelles";
	public static final String FCOM_ORG_PROGRESSION_KEY = "fcomOrgProgression";
	public static final String FCOM_ORG_REGLEMENT_KEY = "fcomOrgReglement";
	public static final String FCOM_OUV_DESCRIPTIF_KEY = "fcomOuvDescriptif";
	public static final String FCOM_POL_ORIGINE_KEY = "fcomPolOrigine";
	public static final String FCOM_POL_PREREQUIS_KEY = "fcomPolPrerequis";
	public static final String FCOM_POL_RECOMMANDATIONS_KEY = "fcomPolRecommandations";
	public static final String FCOM_POL_RECRUTEMENT_KEY = "fcomPolRecrutement";
	public static final String FCOM_POS_DESCRIPTIF_KEY = "fcomPosDescriptif";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FSPN_KEY_KEY = "fspnKey";

	// Colkeys
	public static final String FCOM_DAC_AIDES_COLKEY = "FCOM_DAC_AIDES";
	public static final String FCOM_DAC_AUTOFORMATION_COLKEY = "FCOM_DAC_AUTOFORMATION";
	public static final String FCOM_DAC_DESCRIPTIF_COLKEY = "FCOM_DAC_DESCRIPTIF";
	public static final String FCOM_DAC_DISPOSITIF_COLKEY = "FCOM_DAC_DISPOSITIF";
	public static final String FCOM_DAC_FOAD_COLKEY = "FCOM_DAC_FOAD";
	public static final String FCOM_DAC_LANGUES_COLKEY = "FCOM_DAC_LANGUES";
	public static final String FCOM_DAC_PUBLICS_COLKEY = "FCOM_DAC_PUBLICS";
	public static final String FCOM_DAC_TIC_TICE_COLKEY = "FCOM_DAC_TIC_TICE";
	public static final String FCOM_DEV_DESCRIPTIF_COLKEY = "FCOM_DEV_DESCRIPTIF";
	public static final String FCOM_DEV_DISPOSITIFS_COLKEY = "FCOM_DEV_DISPOSITIFS";
	public static final String FCOM_FOR_DESCRIPTIF_COLKEY = "FCOM_FOR_DESCRIPTIF";
	public static final String FCOM_IMP_DESCRIPTIF_COLKEY = "FCOM_IMP_DESCRIPTIF";
	public static final String FCOM_INS_AUTRES_COLKEY = "FCOM_INS_AUTRES";
	public static final String FCOM_INS_CDD_EXTERNE_COLKEY = "FCOM_INS_CDD_EXTERNE";
	public static final String FCOM_INS_CDD_INTERNE_COLKEY = "FCOM_INS_CDD_INTERNE";
	public static final String FCOM_INS_CDI_EXTERNE_COLKEY = "FCOM_INS_CDI_EXTERNE";
	public static final String FCOM_INS_CDI_INTERNE_COLKEY = "FCOM_INS_CDI_INTERNE";
	public static final String FCOM_INS_DESCRIPTIF_COLKEY = "FCOM_INS_DESCRIPTIF";
	public static final String FCOM_INS_POURSUITE_ETUDES_COLKEY = "FCOM_INS_POURSUITE_ETUDES";
	public static final String FCOM_INS_RECHERCHE_EMPLOI_COLKEY = "FCOM_INS_RECHERCHE_EMPLOI";
	public static final String FCOM_OBJ_ACTIVITES_COLKEY = "FCOM_OBJ_ACTIVITES";
	public static final String FCOM_OBJ_COMPETENCES_COLKEY = "FCOM_OBJ_COMPETENCES";
	public static final String FCOM_OBJ_CONNAISSANCES_COLKEY = "FCOM_OBJ_CONNAISSANCES";
	public static final String FCOM_OBJ_METIERS_COLKEY = "FCOM_OBJ_METIERS";
	public static final String FCOM_OBJ_NIVEAU_EMPLOI_COLKEY = "FCOM_OBJ_NIVEAU_EMPLOI";
	public static final String FCOM_OBJ_POURSUITES_COLKEY = "FCOM_OBJ_POURSUITES";
	public static final String FCOM_OBJ_SITUATION_COLKEY = "FCOM_OBJ_SITUATION";
	public static final String FCOM_ORG_DESCRIPTIF_COLKEY = "FCOM_ORG_DESCRIPTIF";
	public static final String FCOM_ORG_EVALUATION_COLKEY = "FCOM_ORG_EVALUATION";
	public static final String FCOM_ORG_PASSERELLES_COLKEY = "FCOM_ORG_PASSERELLES";
	public static final String FCOM_ORG_PROGRESSION_COLKEY = "FCOM_ORG_PROGRESSION";
	public static final String FCOM_ORG_REGLEMENT_COLKEY = "FCOM_ORG_REGLEMENT";
	public static final String FCOM_OUV_DESCRIPTIF_COLKEY = "FCOM_OUV_DESCRIPTIF";
	public static final String FCOM_POL_ORIGINE_COLKEY = "FCOM_POL_ORIGINE";
	public static final String FCOM_POL_PREREQUIS_COLKEY = "FCOM_POL_PREREQUIS";
	public static final String FCOM_POL_RECOMMANDATIONS_COLKEY = "FCOM_POL_RECOMMANDATIONS";
	public static final String FCOM_POL_RECRUTEMENT_COLKEY = "FCOM_POL_RECRUTEMENT";
	public static final String FCOM_POS_DESCRIPTIF_COLKEY = "FCOM_POS_DESCRIPTIF";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolFormationComplementHabi with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @return EOScolFormationComplementHabi
	 */
	public static EOScolFormationComplementHabi create(EOEditingContext editingContext, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation) {
		EOScolFormationComplementHabi eo = (EOScolFormationComplementHabi) createAndInsertInstance(editingContext);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolFormationComplementHabi.
	 *
	 * @param editingContext
	 * @return EOScolFormationComplementHabi
	 */
	public static EOScolFormationComplementHabi create(EOEditingContext editingContext) {
		EOScolFormationComplementHabi eo = (EOScolFormationComplementHabi) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolFormationComplementHabi localInstanceIn(EOEditingContext editingContext) {
		EOScolFormationComplementHabi localInstance = (EOScolFormationComplementHabi) localInstanceOfObject(editingContext, (EOScolFormationComplementHabi) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolFormationComplementHabi localInstanceIn(EOEditingContext editingContext, EOScolFormationComplementHabi eo) {
		EOScolFormationComplementHabi localInstance = (eo == null) ? null : (EOScolFormationComplementHabi) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer fcomDacAides() {
		return (Integer) storedValueForKey("fcomDacAides");
	}

	public void setFcomDacAides(Integer value) {
		takeStoredValueForKey(value, "fcomDacAides");
	}
	public Integer fcomDacAutoformation() {
		return (Integer) storedValueForKey("fcomDacAutoformation");
	}

	public void setFcomDacAutoformation(Integer value) {
		takeStoredValueForKey(value, "fcomDacAutoformation");
	}
	public Integer fcomDacDescriptif() {
		return (Integer) storedValueForKey("fcomDacDescriptif");
	}

	public void setFcomDacDescriptif(Integer value) {
		takeStoredValueForKey(value, "fcomDacDescriptif");
	}
	public Integer fcomDacDispositif() {
		return (Integer) storedValueForKey("fcomDacDispositif");
	}

	public void setFcomDacDispositif(Integer value) {
		takeStoredValueForKey(value, "fcomDacDispositif");
	}
	public Integer fcomDacFoad() {
		return (Integer) storedValueForKey("fcomDacFoad");
	}

	public void setFcomDacFoad(Integer value) {
		takeStoredValueForKey(value, "fcomDacFoad");
	}
	public Integer fcomDacLangues() {
		return (Integer) storedValueForKey("fcomDacLangues");
	}

	public void setFcomDacLangues(Integer value) {
		takeStoredValueForKey(value, "fcomDacLangues");
	}
	public Integer fcomDacPublics() {
		return (Integer) storedValueForKey("fcomDacPublics");
	}

	public void setFcomDacPublics(Integer value) {
		takeStoredValueForKey(value, "fcomDacPublics");
	}
	public Integer fcomDacTicTice() {
		return (Integer) storedValueForKey("fcomDacTicTice");
	}

	public void setFcomDacTicTice(Integer value) {
		takeStoredValueForKey(value, "fcomDacTicTice");
	}
	public Integer fcomDevDescriptif() {
		return (Integer) storedValueForKey("fcomDevDescriptif");
	}

	public void setFcomDevDescriptif(Integer value) {
		takeStoredValueForKey(value, "fcomDevDescriptif");
	}
	public Integer fcomDevDispositifs() {
		return (Integer) storedValueForKey("fcomDevDispositifs");
	}

	public void setFcomDevDispositifs(Integer value) {
		takeStoredValueForKey(value, "fcomDevDispositifs");
	}
	public Integer fcomForDescriptif() {
		return (Integer) storedValueForKey("fcomForDescriptif");
	}

	public void setFcomForDescriptif(Integer value) {
		takeStoredValueForKey(value, "fcomForDescriptif");
	}
	public Integer fcomImpDescriptif() {
		return (Integer) storedValueForKey("fcomImpDescriptif");
	}

	public void setFcomImpDescriptif(Integer value) {
		takeStoredValueForKey(value, "fcomImpDescriptif");
	}
	public Integer fcomInsAutres() {
		return (Integer) storedValueForKey("fcomInsAutres");
	}

	public void setFcomInsAutres(Integer value) {
		takeStoredValueForKey(value, "fcomInsAutres");
	}
	public Integer fcomInsCddExterne() {
		return (Integer) storedValueForKey("fcomInsCddExterne");
	}

	public void setFcomInsCddExterne(Integer value) {
		takeStoredValueForKey(value, "fcomInsCddExterne");
	}
	public Integer fcomInsCddInterne() {
		return (Integer) storedValueForKey("fcomInsCddInterne");
	}

	public void setFcomInsCddInterne(Integer value) {
		takeStoredValueForKey(value, "fcomInsCddInterne");
	}
	public Integer fcomInsCdiExterne() {
		return (Integer) storedValueForKey("fcomInsCdiExterne");
	}

	public void setFcomInsCdiExterne(Integer value) {
		takeStoredValueForKey(value, "fcomInsCdiExterne");
	}
	public Integer fcomInsCdiInterne() {
		return (Integer) storedValueForKey("fcomInsCdiInterne");
	}

	public void setFcomInsCdiInterne(Integer value) {
		takeStoredValueForKey(value, "fcomInsCdiInterne");
	}
	public Integer fcomInsDescriptif() {
		return (Integer) storedValueForKey("fcomInsDescriptif");
	}

	public void setFcomInsDescriptif(Integer value) {
		takeStoredValueForKey(value, "fcomInsDescriptif");
	}
	public Integer fcomInsPoursuiteEtudes() {
		return (Integer) storedValueForKey("fcomInsPoursuiteEtudes");
	}

	public void setFcomInsPoursuiteEtudes(Integer value) {
		takeStoredValueForKey(value, "fcomInsPoursuiteEtudes");
	}
	public Integer fcomInsRechercheEmploi() {
		return (Integer) storedValueForKey("fcomInsRechercheEmploi");
	}

	public void setFcomInsRechercheEmploi(Integer value) {
		takeStoredValueForKey(value, "fcomInsRechercheEmploi");
	}
	public Integer fcomObjActivites() {
		return (Integer) storedValueForKey("fcomObjActivites");
	}

	public void setFcomObjActivites(Integer value) {
		takeStoredValueForKey(value, "fcomObjActivites");
	}
	public Integer fcomObjCompetences() {
		return (Integer) storedValueForKey("fcomObjCompetences");
	}

	public void setFcomObjCompetences(Integer value) {
		takeStoredValueForKey(value, "fcomObjCompetences");
	}
	public Integer fcomObjConnaissances() {
		return (Integer) storedValueForKey("fcomObjConnaissances");
	}

	public void setFcomObjConnaissances(Integer value) {
		takeStoredValueForKey(value, "fcomObjConnaissances");
	}
	public Integer fcomObjMetiers() {
		return (Integer) storedValueForKey("fcomObjMetiers");
	}

	public void setFcomObjMetiers(Integer value) {
		takeStoredValueForKey(value, "fcomObjMetiers");
	}
	public Integer fcomObjNiveauEmploi() {
		return (Integer) storedValueForKey("fcomObjNiveauEmploi");
	}

	public void setFcomObjNiveauEmploi(Integer value) {
		takeStoredValueForKey(value, "fcomObjNiveauEmploi");
	}
	public Integer fcomObjPoursuites() {
		return (Integer) storedValueForKey("fcomObjPoursuites");
	}

	public void setFcomObjPoursuites(Integer value) {
		takeStoredValueForKey(value, "fcomObjPoursuites");
	}
	public Integer fcomObjSituation() {
		return (Integer) storedValueForKey("fcomObjSituation");
	}

	public void setFcomObjSituation(Integer value) {
		takeStoredValueForKey(value, "fcomObjSituation");
	}
	public Integer fcomOrgDescriptif() {
		return (Integer) storedValueForKey("fcomOrgDescriptif");
	}

	public void setFcomOrgDescriptif(Integer value) {
		takeStoredValueForKey(value, "fcomOrgDescriptif");
	}
	public Integer fcomOrgEvaluation() {
		return (Integer) storedValueForKey("fcomOrgEvaluation");
	}

	public void setFcomOrgEvaluation(Integer value) {
		takeStoredValueForKey(value, "fcomOrgEvaluation");
	}
	public Integer fcomOrgPasserelles() {
		return (Integer) storedValueForKey("fcomOrgPasserelles");
	}

	public void setFcomOrgPasserelles(Integer value) {
		takeStoredValueForKey(value, "fcomOrgPasserelles");
	}
	public Integer fcomOrgProgression() {
		return (Integer) storedValueForKey("fcomOrgProgression");
	}

	public void setFcomOrgProgression(Integer value) {
		takeStoredValueForKey(value, "fcomOrgProgression");
	}
	public Integer fcomOrgReglement() {
		return (Integer) storedValueForKey("fcomOrgReglement");
	}

	public void setFcomOrgReglement(Integer value) {
		takeStoredValueForKey(value, "fcomOrgReglement");
	}
	public Integer fcomOuvDescriptif() {
		return (Integer) storedValueForKey("fcomOuvDescriptif");
	}

	public void setFcomOuvDescriptif(Integer value) {
		takeStoredValueForKey(value, "fcomOuvDescriptif");
	}
	public Integer fcomPolOrigine() {
		return (Integer) storedValueForKey("fcomPolOrigine");
	}

	public void setFcomPolOrigine(Integer value) {
		takeStoredValueForKey(value, "fcomPolOrigine");
	}
	public Integer fcomPolPrerequis() {
		return (Integer) storedValueForKey("fcomPolPrerequis");
	}

	public void setFcomPolPrerequis(Integer value) {
		takeStoredValueForKey(value, "fcomPolPrerequis");
	}
	public Integer fcomPolRecommandations() {
		return (Integer) storedValueForKey("fcomPolRecommandations");
	}

	public void setFcomPolRecommandations(Integer value) {
		takeStoredValueForKey(value, "fcomPolRecommandations");
	}
	public Integer fcomPolRecrutement() {
		return (Integer) storedValueForKey("fcomPolRecrutement");
	}

	public void setFcomPolRecrutement(Integer value) {
		takeStoredValueForKey(value, "fcomPolRecrutement");
	}
	public Integer fcomPosDescriptif() {
		return (Integer) storedValueForKey("fcomPosDescriptif");
	}

	public void setFcomPosDescriptif(Integer value) {
		takeStoredValueForKey(value, "fcomPosDescriptif");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolFormationComplementHabi.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolFormationComplementHabi.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolFormationComplementHabi)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolFormationComplementHabi fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolFormationComplementHabi fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolFormationComplementHabi eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolFormationComplementHabi)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolFormationComplementHabi fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationComplementHabi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolFormationComplementHabi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolFormationComplementHabi eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolFormationComplementHabi)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationComplementHabi fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolFormationComplementHabi fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolFormationComplementHabi eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolFormationComplementHabi ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolFormationComplementHabi createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolFormationComplementHabi.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolFormationComplementHabi.ENTITY_NAME + "' !");
		}
		else {
			EOScolFormationComplementHabi object = (EOScolFormationComplementHabi) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolFormationComplementHabi localInstanceOfObject(EOEditingContext ec, EOScolFormationComplementHabi object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolFormationComplementHabi " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolFormationComplementHabi) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
