/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolGroupeCollection.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolGroupeCollection extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolGroupeCollection";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_GROUPE_COLLECTION";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "gcolKey";

	public static final ERXKey<Integer> GCOL_DATE_DEBUT = new ERXKey<Integer>("gcolDateDebut");
	public static final ERXKey<Integer> GCOL_DATE_FIN = new ERXKey<Integer>("gcolDateFin");
	public static final ERXKey<String> GCOL_LIBELLE = new ERXKey<String>("gcolLibelle");

	public static final String GCOL_DATE_DEBUT_KEY = "gcolDateDebut";
	public static final String GCOL_DATE_FIN_KEY = "gcolDateFin";
	public static final String GCOL_LIBELLE_KEY = "gcolLibelle";

	// Non visible attributes
	public static final String GCOL_KEY_KEY = "gcolKey";

	// Colkeys
	public static final String GCOL_DATE_DEBUT_COLKEY = "GCOL_DATE_DEBUT";
	public static final String GCOL_DATE_FIN_COLKEY = "GCOL_DATE_FIN";
	public static final String GCOL_LIBELLE_COLKEY = "GCOL_LIBELLE";

	// Non visible colkeys
	public static final String GCOL_KEY_COLKEY = "GCOL_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp> TO_FWK_SCOLARITE__SCOL_GROUPE_GRPS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp>("toFwkScolarite_ScolGroupeGrps");

	public static final String TO_FWK_SCOLARITE__SCOL_GROUPE_GRPS_KEY = "toFwkScolarite_ScolGroupeGrps";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolGroupeCollection with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param gcolDateDebut
	 * @param gcolLibelle
	 * @return EOScolGroupeCollection
	 */
	public static EOScolGroupeCollection create(EOEditingContext editingContext, Integer gcolDateDebut, String gcolLibelle) {
		EOScolGroupeCollection eo = (EOScolGroupeCollection) createAndInsertInstance(editingContext);
		eo.setGcolDateDebut(gcolDateDebut);
		eo.setGcolLibelle(gcolLibelle);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolGroupeCollection.
	 *
	 * @param editingContext
	 * @return EOScolGroupeCollection
	 */
	public static EOScolGroupeCollection create(EOEditingContext editingContext) {
		EOScolGroupeCollection eo = (EOScolGroupeCollection) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolGroupeCollection localInstanceIn(EOEditingContext editingContext) {
		EOScolGroupeCollection localInstance = (EOScolGroupeCollection) localInstanceOfObject(editingContext, (EOScolGroupeCollection) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolGroupeCollection localInstanceIn(EOEditingContext editingContext, EOScolGroupeCollection eo) {
		EOScolGroupeCollection localInstance = (eo == null) ? null : (EOScolGroupeCollection) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer gcolDateDebut() {
		return (Integer) storedValueForKey("gcolDateDebut");
	}

	public void setGcolDateDebut(Integer value) {
		takeStoredValueForKey(value, "gcolDateDebut");
	}
	public Integer gcolDateFin() {
		return (Integer) storedValueForKey("gcolDateFin");
	}

	public void setGcolDateFin(Integer value) {
		takeStoredValueForKey(value, "gcolDateFin");
	}
	public String gcolLibelle() {
		return (String) storedValueForKey("gcolLibelle");
	}

	public void setGcolLibelle(String value) {
		takeStoredValueForKey(value, "gcolLibelle");
	}

	public NSArray toFwkScolarite_ScolGroupeGrps() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolGroupeGrps");
	}

	public NSArray toFwkScolarite_ScolGroupeGrps(EOQualifier qualifier) {
		return toFwkScolarite_ScolGroupeGrps(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolGroupeGrps(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolGroupeGrps(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolGroupeGrps(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp.TO_FWK_SCOLARITE__SCOL_GROUPE_COLLECTION_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolGroupeGrps();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolGroupeGrpsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeGrps");
	}

	public void removeFromToFwkScolarite_ScolGroupeGrpsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeGrps");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp createToFwkScolarite_ScolGroupeGrpsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolGroupeGrp");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolGroupeGrps");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp) eo;
	}

	public void deleteToFwkScolarite_ScolGroupeGrpsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolGroupeGrps");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolGroupeGrpsRelationships() {
		Enumeration objects = toFwkScolarite_ScolGroupeGrps().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolGroupeGrpsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolGroupeGrp)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolGroupeCollection.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolGroupeCollection.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolGroupeCollection)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolGroupeCollection fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolGroupeCollection fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolGroupeCollection eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolGroupeCollection)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolGroupeCollection fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolGroupeCollection fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolGroupeCollection fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolGroupeCollection eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolGroupeCollection)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolGroupeCollection fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolGroupeCollection fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolGroupeCollection eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolGroupeCollection ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolGroupeCollection createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolGroupeCollection.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolGroupeCollection.ENTITY_NAME + "' !");
		}
		else {
			EOScolGroupeCollection object = (EOScolGroupeCollection) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolGroupeCollection localInstanceOfObject(EOEditingContext ec, EOScolGroupeCollection object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolGroupeCollection " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolGroupeCollection) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
