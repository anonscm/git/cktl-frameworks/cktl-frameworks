/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolInscriptionEtudiant.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolInscriptionEtudiant extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolInscriptionEtudiant";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_INSCRIPTION_ETUDIANT";

	//Attributes

	public static final ERXKey<String> ADR_NOM = new ERXKey<String>("adrNom");
	public static final ERXKey<String> ADR_PRENOM = new ERXKey<String>("adrPrenom");
	public static final ERXKey<String> ADR_PRENOM2 = new ERXKey<String>("adrPrenom2");
	public static final ERXKey<Integer> CPT_ORDRE = new ERXKey<Integer>("cptOrdre");
	public static final ERXKey<String> ETUD_CODE_INE = new ERXKey<String>("etudCodeIne");
	public static final ERXKey<String> ETUD_NOM_MARITAL = new ERXKey<String>("etudNomMarital");
	public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
	public static final ERXKey<String> FDIP_ABREVIATION = new ERXKey<String>("fdipAbreviation");
	public static final ERXKey<String> FDIP_MENTION = new ERXKey<String>("fdipMention");
	public static final ERXKey<String> FDIP_SPECIALITE = new ERXKey<String>("fdipSpecialite");
	public static final ERXKey<String> FSPN_LIBELLE = new ERXKey<String>("fspnLibelle");
	public static final ERXKey<Integer> HIST_NUMERO = new ERXKey<Integer>("histNumero");
	public static final ERXKey<Integer> IDIPL_ANNEE_SUIVIE = new ERXKey<Integer>("idiplAnneeSuivie");
	public static final ERXKey<NSTimestamp> IDIPL_DATE_DEMISSION = new ERXKey<NSTimestamp>("idiplDateDemission");
	public static final ERXKey<NSTimestamp> IDIPL_DATE_INSC = new ERXKey<NSTimestamp>("idiplDateInsc");
	public static final ERXKey<Integer> IDIPL_NUMERO = new ERXKey<Integer>("idiplNumero");
	public static final ERXKey<String> IDIPL_PASSAGE_CONDITIONNEL = new ERXKey<String>("idiplPassageConditionnel");
	public static final ERXKey<String> IDIPL_REDOUBLANT = new ERXKey<String>("idiplRedoublant");
	public static final ERXKey<java.math.BigDecimal> IETUD_BASE = new ERXKey<java.math.BigDecimal>("ietudBase");
	public static final ERXKey<Integer> IETUD_KEY = new ERXKey<Integer>("ietudKey");
	public static final ERXKey<java.math.BigDecimal> IETUD_MOYENNE1 = new ERXKey<java.math.BigDecimal>("ietudMoyenne1");
	public static final ERXKey<java.math.BigDecimal> IETUD_MOYENNE2 = new ERXKey<java.math.BigDecimal>("ietudMoyenne2");
	public static final ERXKey<java.math.BigDecimal> IETUD_PONDERATION = new ERXKey<java.math.BigDecimal>("ietudPonderation");
	public static final ERXKey<java.math.BigDecimal> IETUD_POURCENTAGE1 = new ERXKey<java.math.BigDecimal>("ietudPourcentage1");
	public static final ERXKey<java.math.BigDecimal> IETUD_POURCENTAGE2 = new ERXKey<java.math.BigDecimal>("ietudPourcentage2");
	public static final ERXKey<Integer> IETUD_RANG = new ERXKey<Integer>("ietudRang");
	public static final ERXKey<Integer> MPAR_KEY = new ERXKey<Integer>("mparKey");
	public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");

	public static final String ADR_NOM_KEY = "adrNom";
	public static final String ADR_PRENOM_KEY = "adrPrenom";
	public static final String ADR_PRENOM2_KEY = "adrPrenom2";
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String ETUD_CODE_INE_KEY = "etudCodeIne";
	public static final String ETUD_NOM_MARITAL_KEY = "etudNomMarital";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String FDIP_ABREVIATION_KEY = "fdipAbreviation";
	public static final String FDIP_MENTION_KEY = "fdipMention";
	public static final String FDIP_SPECIALITE_KEY = "fdipSpecialite";
	public static final String FSPN_LIBELLE_KEY = "fspnLibelle";
	public static final String HIST_NUMERO_KEY = "histNumero";
	public static final String IDIPL_ANNEE_SUIVIE_KEY = "idiplAnneeSuivie";
	public static final String IDIPL_DATE_DEMISSION_KEY = "idiplDateDemission";
	public static final String IDIPL_DATE_INSC_KEY = "idiplDateInsc";
	public static final String IDIPL_NUMERO_KEY = "idiplNumero";
	public static final String IDIPL_PASSAGE_CONDITIONNEL_KEY = "idiplPassageConditionnel";
	public static final String IDIPL_REDOUBLANT_KEY = "idiplRedoublant";
	public static final String IETUD_BASE_KEY = "ietudBase";
	public static final String IETUD_KEY_KEY = "ietudKey";
	public static final String IETUD_MOYENNE1_KEY = "ietudMoyenne1";
	public static final String IETUD_MOYENNE2_KEY = "ietudMoyenne2";
	public static final String IETUD_PONDERATION_KEY = "ietudPonderation";
	public static final String IETUD_POURCENTAGE1_KEY = "ietudPourcentage1";
	public static final String IETUD_POURCENTAGE2_KEY = "ietudPourcentage2";
	public static final String IETUD_RANG_KEY = "ietudRang";
	public static final String MPAR_KEY_KEY = "mparKey";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PERS_ID_KEY = "persId";

	// Non visible attributes
	public static final String ADR_CIVILITE_KEY = "adrCivilite";
	public static final String ADR_ORDRE_PARENT_KEY = "adrOrdreParent";
	public static final String ADR_ORDRE_SCOL_KEY = "adrOrdreScol";
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FDIP_CODE_KEY = "fdipCode";
	public static final String FDOM_CODE_KEY = "fdomCode";
	public static final String FGRA_CODE_KEY = "fgraCode";
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String IDIPL_TYPE_INSCRIPTION_KEY = "idiplTypeInscription";
	public static final String IETUD_ETAT_KEY = "ietudEtat";
	public static final String IETUD_MENTION1_KEY = "ietudMention1";
	public static final String IETUD_MENTION2_KEY = "ietudMention2";
	public static final String RES_CODE_KEY = "resCode";

	// Colkeys
	public static final String ADR_NOM_COLKEY = "ADR_NOM";
	public static final String ADR_PRENOM_COLKEY = "ADR_PRENOM";
	public static final String ADR_PRENOM2_COLKEY = "ADR_PRENOM2";
	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String ETUD_CODE_INE_COLKEY = "ETUD_CODE_INE";
	public static final String ETUD_NOM_MARITAL_COLKEY = "ETUD_NOM_MARITAL";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String FDIP_ABREVIATION_COLKEY = "FDIP_ABREVIATION";
	public static final String FDIP_MENTION_COLKEY = "FDIP_MENTION";
	public static final String FDIP_SPECIALITE_COLKEY = "FDIP_SPECIALITE";
	public static final String FSPN_LIBELLE_COLKEY = "FSPN_LIBELLE";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String IDIPL_ANNEE_SUIVIE_COLKEY = "IDIPL_ANNEE_SUIVIE";
	public static final String IDIPL_DATE_DEMISSION_COLKEY = "IDIPL_DATE_DEMISSION";
	public static final String IDIPL_DATE_INSC_COLKEY = "IDIPL_DATE_INSC";
	public static final String IDIPL_NUMERO_COLKEY = "IDIPL_NUMERO";
	public static final String IDIPL_PASSAGE_CONDITIONNEL_COLKEY = "IDIPL_PASSAGE_CONDITIONNEL";
	public static final String IDIPL_REDOUBLANT_COLKEY = "IDIPL_REDOUBLANT";
	public static final String IETUD_BASE_COLKEY = "IETUD_BASE";
	public static final String IETUD_KEY_COLKEY = "IETUD_KEY";
	public static final String IETUD_MOYENNE1_COLKEY = "IETUD_MOYENNE1";
	public static final String IETUD_MOYENNE2_COLKEY = "IETUD_MOYENNE2";
	public static final String IETUD_PONDERATION_COLKEY = "IETUD_PONDERATION";
	public static final String IETUD_POURCENTAGE1_COLKEY = "IETUD_POURCENTAGE1";
	public static final String IETUD_POURCENTAGE2_COLKEY = "IETUD_POURCENTAGE2";
	public static final String IETUD_RANG_COLKEY = "IETUD_RANG";
	public static final String MPAR_KEY_COLKEY = "MPAR_KEY";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	// Non visible colkeys
	public static final String ADR_CIVILITE_COLKEY = "ADR_CIVILITE";
	public static final String ADR_ORDRE_PARENT_COLKEY = "ADR_ORDRE_PARENT";
	public static final String ADR_ORDRE_SCOL_COLKEY = "ADR_ORDRE_SCOL";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FDIP_CODE_COLKEY = "FDIP_CODE";
	public static final String FDOM_CODE_COLKEY = "FDOM_CODE";
	public static final String FGRA_CODE_COLKEY = "FGRA_CODE";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String IDIPL_TYPE_INSCRIPTION_COLKEY = "IDIPL_TYPE_INSCRIPTION";
	public static final String IETUD_ETAT_COLKEY = "IETUD_ETAT";
	public static final String IETUD_MENTION1_COLKEY = "IETUD_MENTION1";
	public static final String IETUD_MENTION2_COLKEY = "IETUD_MENTION2";
	public static final String RES_CODE_COLKEY = "RES_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_FWKPERS__ADRESSE_PARENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toFwkpers_AdresseParent");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_FWKPERS__ADRESSE_SCOL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toFwkpers_AdresseScol");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite> TO_FWKPERS__CIVILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite>("toFwkpers_Civilite");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteEtat> TO_FWK_SCOLARITE__SCOL_CONSTANTE_ETAT = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteEtat>("toFwkScolarite_ScolConstanteEtat");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention> TO_FWK_SCOLARITE__SCOL_CONSTANTE_MENTION1 = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention>("toFwkScolarite_ScolConstanteMention1");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention> TO_FWK_SCOLARITE__SCOL_CONSTANTE_MENTION2 = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention>("toFwkScolarite_ScolConstanteMention2");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement> TO_FWK_SCOLARITE__SCOL_FORMATION_DEPARTEMENT = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement>("toFwkScolarite_ScolFormationDepartement");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome> TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome>("toFwkScolarite_ScolFormationDiplome");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine> TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine>("toFwkScolarite_ScolFormationDomaine");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere> TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere>("toFwkScolarite_ScolFormationFiliere");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade> TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade>("toFwkScolarite_ScolFormationGrade");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation> TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation>("toFwkScolarite_ScolFormationSpecialisation");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc> TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ECS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc>("toFwkScolarite_ScolInscriptionEcs");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre> TO_FWK_SCOLARITE__SCOL_INSCRIPTION_SEMESTRES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre>("toFwkScolarite_ScolInscriptionSemestres");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOResultat> TO_FWK_SCOLARIX__RESULTAT = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOResultat>("toFwkScolarix_Resultat");

	public static final String TO_FWKPERS__ADRESSE_PARENT_KEY = "toFwkpers_AdresseParent";
	public static final String TO_FWKPERS__ADRESSE_SCOL_KEY = "toFwkpers_AdresseScol";
	public static final String TO_FWKPERS__CIVILITE_KEY = "toFwkpers_Civilite";
	public static final String TO_FWK_SCOLARITE__SCOL_CONSTANTE_ETAT_KEY = "toFwkScolarite_ScolConstanteEtat";
	public static final String TO_FWK_SCOLARITE__SCOL_CONSTANTE_MENTION1_KEY = "toFwkScolarite_ScolConstanteMention1";
	public static final String TO_FWK_SCOLARITE__SCOL_CONSTANTE_MENTION2_KEY = "toFwkScolarite_ScolConstanteMention2";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DEPARTEMENT_KEY = "toFwkScolarite_ScolFormationDepartement";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY = "toFwkScolarite_ScolFormationDiplome";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY = "toFwkScolarite_ScolFormationDomaine";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE_KEY = "toFwkScolarite_ScolFormationFiliere";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY = "toFwkScolarite_ScolFormationGrade";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY = "toFwkScolarite_ScolFormationSpecialisation";
	public static final String TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ECS_KEY = "toFwkScolarite_ScolInscriptionEcs";
	public static final String TO_FWK_SCOLARITE__SCOL_INSCRIPTION_SEMESTRES_KEY = "toFwkScolarite_ScolInscriptionSemestres";
	public static final String TO_FWK_SCOLARIX__RESULTAT_KEY = "toFwkScolarix_Resultat";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolInscriptionEtudiant with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param adrNom
	 * @param adrPrenom
	 * @param etudNumero
	 * @param fdipAbreviation
	 * @param fdipMention
	 * @param histNumero
	 * @param idiplAnneeSuivie
	 * @param idiplNumero
	 * @param idiplPassageConditionnel
	 * @param idiplRedoublant
	 * @param ietudPonderation
	 * @param ietudPourcentage1
	 * @param ietudPourcentage2
	 * @param noIndividu
	 * @param persId
	 * @param toFwkScolarite_ScolConstanteEtat
	 * @param toFwkScolarite_ScolConstanteMention1
	 * @param toFwkScolarite_ScolConstanteMention2
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationDepartement
	 * @param toFwkScolarite_ScolFormationDiplome
	 * @param toFwkScolarite_ScolFormationDomaine
	 * @param toFwkScolarite_ScolFormationFiliere
	 * @param toFwkScolarite_ScolFormationGrade
	 * @param toFwkScolarite_ScolFormationSpecialisation
	 * @return EOScolInscriptionEtudiant
	 */
	public static EOScolInscriptionEtudiant create(EOEditingContext editingContext, String adrNom, String adrPrenom, Integer etudNumero, String fdipAbreviation, String fdipMention, Integer histNumero, Integer idiplAnneeSuivie, Integer idiplNumero, String idiplPassageConditionnel, String idiplRedoublant, java.math.BigDecimal ietudPonderation, java.math.BigDecimal ietudPourcentage1, java.math.BigDecimal ietudPourcentage2, Integer noIndividu, Integer persId, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteEtat toFwkScolarite_ScolConstanteEtat, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention toFwkScolarite_ScolConstanteMention1, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention toFwkScolarite_ScolConstanteMention2, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement toFwkScolarite_ScolFormationDepartement, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toFwkScolarite_ScolFormationDiplome, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine toFwkScolarite_ScolFormationDomaine, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere toFwkScolarite_ScolFormationFiliere, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade toFwkScolarite_ScolFormationGrade, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation) {
		EOScolInscriptionEtudiant eo = (EOScolInscriptionEtudiant) createAndInsertInstance(editingContext);
		eo.setAdrNom(adrNom);
		eo.setAdrPrenom(adrPrenom);
		eo.setEtudNumero(etudNumero);
		eo.setFdipAbreviation(fdipAbreviation);
		eo.setFdipMention(fdipMention);
		eo.setHistNumero(histNumero);
		eo.setIdiplAnneeSuivie(idiplAnneeSuivie);
		eo.setIdiplNumero(idiplNumero);
		eo.setIdiplPassageConditionnel(idiplPassageConditionnel);
		eo.setIdiplRedoublant(idiplRedoublant);
		eo.setIetudPonderation(ietudPonderation);
		eo.setIetudPourcentage1(ietudPourcentage1);
		eo.setIetudPourcentage2(ietudPourcentage2);
		eo.setNoIndividu(noIndividu);
		eo.setPersId(persId);
		eo.setToFwkScolarite_ScolConstanteEtatRelationship(toFwkScolarite_ScolConstanteEtat);
		eo.setToFwkScolarite_ScolConstanteMention1Relationship(toFwkScolarite_ScolConstanteMention1);
		eo.setToFwkScolarite_ScolConstanteMention2Relationship(toFwkScolarite_ScolConstanteMention2);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationDepartementRelationship(toFwkScolarite_ScolFormationDepartement);
		eo.setToFwkScolarite_ScolFormationDiplomeRelationship(toFwkScolarite_ScolFormationDiplome);
		eo.setToFwkScolarite_ScolFormationDomaineRelationship(toFwkScolarite_ScolFormationDomaine);
		eo.setToFwkScolarite_ScolFormationFiliereRelationship(toFwkScolarite_ScolFormationFiliere);
		eo.setToFwkScolarite_ScolFormationGradeRelationship(toFwkScolarite_ScolFormationGrade);
		eo.setToFwkScolarite_ScolFormationSpecialisationRelationship(toFwkScolarite_ScolFormationSpecialisation);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolInscriptionEtudiant.
	 *
	 * @param editingContext
	 * @return EOScolInscriptionEtudiant
	 */
	public static EOScolInscriptionEtudiant create(EOEditingContext editingContext) {
		EOScolInscriptionEtudiant eo = (EOScolInscriptionEtudiant) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolInscriptionEtudiant localInstanceIn(EOEditingContext editingContext) {
		EOScolInscriptionEtudiant localInstance = (EOScolInscriptionEtudiant) localInstanceOfObject(editingContext, (EOScolInscriptionEtudiant) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolInscriptionEtudiant localInstanceIn(EOEditingContext editingContext, EOScolInscriptionEtudiant eo) {
		EOScolInscriptionEtudiant localInstance = (eo == null) ? null : (EOScolInscriptionEtudiant) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String adrNom() {
		return (String) storedValueForKey("adrNom");
	}

	public void setAdrNom(String value) {
		takeStoredValueForKey(value, "adrNom");
	}
	public String adrPrenom() {
		return (String) storedValueForKey("adrPrenom");
	}

	public void setAdrPrenom(String value) {
		takeStoredValueForKey(value, "adrPrenom");
	}
	public String adrPrenom2() {
		return (String) storedValueForKey("adrPrenom2");
	}

	public void setAdrPrenom2(String value) {
		takeStoredValueForKey(value, "adrPrenom2");
	}
	public Integer cptOrdre() {
		return (Integer) storedValueForKey("cptOrdre");
	}

	public void setCptOrdre(Integer value) {
		takeStoredValueForKey(value, "cptOrdre");
	}
	public String etudCodeIne() {
		return (String) storedValueForKey("etudCodeIne");
	}

	public void setEtudCodeIne(String value) {
		takeStoredValueForKey(value, "etudCodeIne");
	}
	public String etudNomMarital() {
		return (String) storedValueForKey("etudNomMarital");
	}

	public void setEtudNomMarital(String value) {
		takeStoredValueForKey(value, "etudNomMarital");
	}
	public Integer etudNumero() {
		return (Integer) storedValueForKey("etudNumero");
	}

	public void setEtudNumero(Integer value) {
		takeStoredValueForKey(value, "etudNumero");
	}
	public String fdipAbreviation() {
		return (String) storedValueForKey("fdipAbreviation");
	}

	public void setFdipAbreviation(String value) {
		takeStoredValueForKey(value, "fdipAbreviation");
	}
	public String fdipMention() {
		return (String) storedValueForKey("fdipMention");
	}

	public void setFdipMention(String value) {
		takeStoredValueForKey(value, "fdipMention");
	}
	public String fdipSpecialite() {
		return (String) storedValueForKey("fdipSpecialite");
	}

	public void setFdipSpecialite(String value) {
		takeStoredValueForKey(value, "fdipSpecialite");
	}
	public String fspnLibelle() {
		return (String) storedValueForKey("fspnLibelle");
	}

	public void setFspnLibelle(String value) {
		takeStoredValueForKey(value, "fspnLibelle");
	}
	public Integer histNumero() {
		return (Integer) storedValueForKey("histNumero");
	}

	public void setHistNumero(Integer value) {
		takeStoredValueForKey(value, "histNumero");
	}
	public Integer idiplAnneeSuivie() {
		return (Integer) storedValueForKey("idiplAnneeSuivie");
	}

	public void setIdiplAnneeSuivie(Integer value) {
		takeStoredValueForKey(value, "idiplAnneeSuivie");
	}
	public NSTimestamp idiplDateDemission() {
		return (NSTimestamp) storedValueForKey("idiplDateDemission");
	}

	public void setIdiplDateDemission(NSTimestamp value) {
		takeStoredValueForKey(value, "idiplDateDemission");
	}
	public NSTimestamp idiplDateInsc() {
		return (NSTimestamp) storedValueForKey("idiplDateInsc");
	}

	public void setIdiplDateInsc(NSTimestamp value) {
		takeStoredValueForKey(value, "idiplDateInsc");
	}
	public Integer idiplNumero() {
		return (Integer) storedValueForKey("idiplNumero");
	}

	public void setIdiplNumero(Integer value) {
		takeStoredValueForKey(value, "idiplNumero");
	}
	public String idiplPassageConditionnel() {
		return (String) storedValueForKey("idiplPassageConditionnel");
	}

	public void setIdiplPassageConditionnel(String value) {
		takeStoredValueForKey(value, "idiplPassageConditionnel");
	}
	public String idiplRedoublant() {
		return (String) storedValueForKey("idiplRedoublant");
	}

	public void setIdiplRedoublant(String value) {
		takeStoredValueForKey(value, "idiplRedoublant");
	}
	public java.math.BigDecimal ietudBase() {
		return (java.math.BigDecimal) storedValueForKey("ietudBase");
	}

	public void setIetudBase(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "ietudBase");
	}
	public Integer ietudKey() {
		return (Integer) storedValueForKey("ietudKey");
	}

	public void setIetudKey(Integer value) {
		takeStoredValueForKey(value, "ietudKey");
	}
	public java.math.BigDecimal ietudMoyenne1() {
		return (java.math.BigDecimal) storedValueForKey("ietudMoyenne1");
	}

	public void setIetudMoyenne1(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "ietudMoyenne1");
	}
	public java.math.BigDecimal ietudMoyenne2() {
		return (java.math.BigDecimal) storedValueForKey("ietudMoyenne2");
	}

	public void setIetudMoyenne2(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "ietudMoyenne2");
	}
	public java.math.BigDecimal ietudPonderation() {
		return (java.math.BigDecimal) storedValueForKey("ietudPonderation");
	}

	public void setIetudPonderation(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "ietudPonderation");
	}
	public java.math.BigDecimal ietudPourcentage1() {
		return (java.math.BigDecimal) storedValueForKey("ietudPourcentage1");
	}

	public void setIetudPourcentage1(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "ietudPourcentage1");
	}
	public java.math.BigDecimal ietudPourcentage2() {
		return (java.math.BigDecimal) storedValueForKey("ietudPourcentage2");
	}

	public void setIetudPourcentage2(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "ietudPourcentage2");
	}
	public Integer ietudRang() {
		return (Integer) storedValueForKey("ietudRang");
	}

	public void setIetudRang(Integer value) {
		takeStoredValueForKey(value, "ietudRang");
	}
	public Integer mparKey() {
		return (Integer) storedValueForKey("mparKey");
	}

	public void setMparKey(Integer value) {
		takeStoredValueForKey(value, "mparKey");
	}
	public Integer noIndividu() {
		return (Integer) storedValueForKey("noIndividu");
	}

	public void setNoIndividu(Integer value) {
		takeStoredValueForKey(value, "noIndividu");
	}
	public Integer persId() {
		return (Integer) storedValueForKey("persId");
	}

	public void setPersId(Integer value) {
		takeStoredValueForKey(value, "persId");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toFwkpers_AdresseParent() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey("toFwkpers_AdresseParent");
	}

	public void setToFwkpers_AdresseParentRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toFwkpers_AdresseParent();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_AdresseParent");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_AdresseParent");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toFwkpers_AdresseScol() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey("toFwkpers_AdresseScol");
	}

	public void setToFwkpers_AdresseScolRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toFwkpers_AdresseScol();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_AdresseScol");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_AdresseScol");
		}
	}
  
	public org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toFwkpers_Civilite() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOCivilite)storedValueForKey("toFwkpers_Civilite");
	}

	public void setToFwkpers_CiviliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCivilite value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOCivilite oldValue = toFwkpers_Civilite();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Civilite");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Civilite");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteEtat toFwkScolarite_ScolConstanteEtat() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteEtat)storedValueForKey("toFwkScolarite_ScolConstanteEtat");
	}

	public void setToFwkScolarite_ScolConstanteEtatRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteEtat value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteEtat oldValue = toFwkScolarite_ScolConstanteEtat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolConstanteEtat");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolConstanteEtat");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention toFwkScolarite_ScolConstanteMention1() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention)storedValueForKey("toFwkScolarite_ScolConstanteMention1");
	}

	public void setToFwkScolarite_ScolConstanteMention1Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention oldValue = toFwkScolarite_ScolConstanteMention1();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolConstanteMention1");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolConstanteMention1");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention toFwkScolarite_ScolConstanteMention2() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention)storedValueForKey("toFwkScolarite_ScolConstanteMention2");
	}

	public void setToFwkScolarite_ScolConstanteMention2Relationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolConstanteMention oldValue = toFwkScolarite_ScolConstanteMention2();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolConstanteMention2");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolConstanteMention2");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement toFwkScolarite_ScolFormationDepartement() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement)storedValueForKey("toFwkScolarite_ScolFormationDepartement");
	}

	public void setToFwkScolarite_ScolFormationDepartementRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement oldValue = toFwkScolarite_ScolFormationDepartement();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDepartement");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDepartement");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome toFwkScolarite_ScolFormationDiplome() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome)storedValueForKey("toFwkScolarite_ScolFormationDiplome");
	}

	public void setToFwkScolarite_ScolFormationDiplomeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome oldValue = toFwkScolarite_ScolFormationDiplome();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDiplome");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDiplome");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine toFwkScolarite_ScolFormationDomaine() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine)storedValueForKey("toFwkScolarite_ScolFormationDomaine");
	}

	public void setToFwkScolarite_ScolFormationDomaineRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine oldValue = toFwkScolarite_ScolFormationDomaine();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDomaine");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDomaine");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere toFwkScolarite_ScolFormationFiliere() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere)storedValueForKey("toFwkScolarite_ScolFormationFiliere");
	}

	public void setToFwkScolarite_ScolFormationFiliereRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere oldValue = toFwkScolarite_ScolFormationFiliere();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationFiliere");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationFiliere");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade toFwkScolarite_ScolFormationGrade() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade)storedValueForKey("toFwkScolarite_ScolFormationGrade");
	}

	public void setToFwkScolarite_ScolFormationGradeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade oldValue = toFwkScolarite_ScolFormationGrade();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationGrade");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationGrade");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation toFwkScolarite_ScolFormationSpecialisation() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation)storedValueForKey("toFwkScolarite_ScolFormationSpecialisation");
	}

	public void setToFwkScolarite_ScolFormationSpecialisationRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation oldValue = toFwkScolarite_ScolFormationSpecialisation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationSpecialisation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationSpecialisation");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOResultat toFwkScolarix_Resultat() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOResultat)storedValueForKey("toFwkScolarix_Resultat");
	}

	public void setToFwkScolarix_ResultatRelationship(org.cocktail.scolarix.serveur.metier.eos.EOResultat value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOResultat oldValue = toFwkScolarix_Resultat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarix_Resultat");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarix_Resultat");
		}
	}
  
	public NSArray toFwkScolarite_ScolInscriptionEcs() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolInscriptionEcs");
	}

	public NSArray toFwkScolarite_ScolInscriptionEcs(EOQualifier qualifier) {
		return toFwkScolarite_ScolInscriptionEcs(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolInscriptionEcs(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolInscriptionEcs(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolInscriptionEcs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc.TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolInscriptionEcs();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolInscriptionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionEcs");
	}

	public void removeFromToFwkScolarite_ScolInscriptionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionEcs");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc createToFwkScolarite_ScolInscriptionEcsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolInscriptionEc");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolInscriptionEcs");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc) eo;
	}

	public void deleteToFwkScolarite_ScolInscriptionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionEcs");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolInscriptionEcsRelationships() {
		Enumeration objects = toFwkScolarite_ScolInscriptionEcs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolInscriptionEcsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolInscriptionSemestres() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolInscriptionSemestres");
	}

	public NSArray toFwkScolarite_ScolInscriptionSemestres(EOQualifier qualifier) {
		return toFwkScolarite_ScolInscriptionSemestres(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolInscriptionSemestres(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolInscriptionSemestres(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolInscriptionSemestres(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre.TO_SCOL_INSCRIPTION_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolInscriptionSemestres();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolInscriptionSemestresRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionSemestres");
	}

	public void removeFromToFwkScolarite_ScolInscriptionSemestresRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionSemestres");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre createToFwkScolarite_ScolInscriptionSemestresRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolInscriptionSemestre");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolInscriptionSemestres");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre) eo;
	}

	public void deleteToFwkScolarite_ScolInscriptionSemestresRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolInscriptionSemestres");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolInscriptionSemestresRelationships() {
		Enumeration objects = toFwkScolarite_ScolInscriptionSemestres().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolInscriptionSemestresRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolInscriptionEtudiant.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolInscriptionEtudiant.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolInscriptionEtudiant)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolInscriptionEtudiant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolInscriptionEtudiant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolInscriptionEtudiant eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolInscriptionEtudiant)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolInscriptionEtudiant fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolInscriptionEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolInscriptionEtudiant eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolInscriptionEtudiant)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionEtudiant fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolInscriptionEtudiant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolInscriptionEtudiant eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolInscriptionEtudiant ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolInscriptionEtudiant createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolInscriptionEtudiant.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolInscriptionEtudiant.ENTITY_NAME + "' !");
		}
		else {
			EOScolInscriptionEtudiant object = (EOScolInscriptionEtudiant) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolInscriptionEtudiant localInstanceOfObject(EOEditingContext ec, EOScolInscriptionEtudiant object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolInscriptionEtudiant " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolInscriptionEtudiant) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
