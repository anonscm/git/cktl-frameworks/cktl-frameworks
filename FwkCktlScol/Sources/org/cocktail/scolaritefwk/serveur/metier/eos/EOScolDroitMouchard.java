/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.metier.eos;

import org.cocktail.scolaritefwk.serveur.exception.ScolFormationSpecialisationException;

import com.webobjects.foundation.NSValidation;

public class EOScolDroitMouchard extends _EOScolDroitMouchard {

	// Valeurs possibles pour les operations
	public static final String  OPERATION_AJOUTER    = "AJOUTER";
	public static final String  OPERATION_ANONYMER   = "ANONYMER";
	public static final String  OPERATION_DISPENSER  = "DISPENSER";
	public static final String  OPERATION_DUPLIQUER  = "DUPLIQUER";
	public static final String  OPERATION_ETIQUETER  = "ETIQUETTES";
	public static final String  OPERATION_IMPORTER   = "IMPORTER";
	public static final String  OPERATION_INTEGRER   = "INTEGRER";
	public static final String  OPERATION_MODIFIER   = "MODIFIER";
	public static final String  OPERATION_RAFRAICHIR = "RAFRAICHIR";
	public static final String  OPERATION_RESERVER   = "RESERVER";
	public static final String  OPERATION_SAISIR     = "SAISIR";
	public static final String  OPERATION_SUPPRIMER  = "SUPPRIMER";
	public static final String  OPERATION_VALIDER    = "VALIDER";

    public EOScolDroitMouchard() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
		// champs obligatoires
		if (toFwkScolarite_ScolDroitLogin() == null) {
			throw new ScolFormationSpecialisationException("L'utilisateur n'est pas connu (scolDroitLogin)");
		}
		// champs obligatoires conditionnels
		// verifications
		// particularites, actions
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
