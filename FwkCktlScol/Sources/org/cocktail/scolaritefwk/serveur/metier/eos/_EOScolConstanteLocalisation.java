/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolConstanteLocalisation.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolConstanteLocalisation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolConstanteLocalisation";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_CONSTANTE_LOCALISATION";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "clocKey";

	public static final ERXKey<String> CLOC_LIBELLE = new ERXKey<String>("clocLibelle");
	public static final ERXKey<String> CLOC_STATUT = new ERXKey<String>("clocStatut");
	public static final ERXKey<String> ETAB_CODE = new ERXKey<String>("etabCode");

	public static final String CLOC_LIBELLE_KEY = "clocLibelle";
	public static final String CLOC_STATUT_KEY = "clocStatut";
	public static final String ETAB_CODE_KEY = "etabCode";

	// Non visible attributes
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String CLOC_KEY_KEY = "clocKey";
	public static final String TETAB_CODE_KEY = "tetabCode";

	// Colkeys
	public static final String CLOC_LIBELLE_COLKEY = "CLOC_LIBELLE";
	public static final String CLOC_STATUT_COLKEY = "CLOC_STATUT";
	public static final String ETAB_CODE_COLKEY = "ETAB_CODE";

	// Non visible colkeys
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String CLOC_KEY_COLKEY = "CLOC_KEY";
	public static final String TETAB_CODE_COLKEY = "TETAB_CODE";

	// Relationships
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_FWKPERS__ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toFwkpers_Adresse");
	public static final ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeEtablissementUlr> TO_FWK_SCOLARIX__TYPE_ETABLISSEMENT_ULR = new ERXKey<org.cocktail.scolarix.serveur.metier.eos.EOTypeEtablissementUlr>("toFwkScolarix_TypeEtablissementUlr");

	public static final String TO_FWKPERS__ADRESSE_KEY = "toFwkpers_Adresse";
	public static final String TO_FWK_SCOLARIX__TYPE_ETABLISSEMENT_ULR_KEY = "toFwkScolarix_TypeEtablissementUlr";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolConstanteLocalisation with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param toFwkpers_Adresse
	 * @param toFwkScolarix_TypeEtablissementUlr
	 * @return EOScolConstanteLocalisation
	 */
	public static EOScolConstanteLocalisation create(EOEditingContext editingContext, org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toFwkpers_Adresse, org.cocktail.scolarix.serveur.metier.eos.EOTypeEtablissementUlr toFwkScolarix_TypeEtablissementUlr) {
		EOScolConstanteLocalisation eo = (EOScolConstanteLocalisation) createAndInsertInstance(editingContext);
		eo.setToFwkpers_AdresseRelationship(toFwkpers_Adresse);
		eo.setToFwkScolarix_TypeEtablissementUlrRelationship(toFwkScolarix_TypeEtablissementUlr);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolConstanteLocalisation.
	 *
	 * @param editingContext
	 * @return EOScolConstanteLocalisation
	 */
	public static EOScolConstanteLocalisation create(EOEditingContext editingContext) {
		EOScolConstanteLocalisation eo = (EOScolConstanteLocalisation) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolConstanteLocalisation localInstanceIn(EOEditingContext editingContext) {
		EOScolConstanteLocalisation localInstance = (EOScolConstanteLocalisation) localInstanceOfObject(editingContext, (EOScolConstanteLocalisation) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolConstanteLocalisation localInstanceIn(EOEditingContext editingContext, EOScolConstanteLocalisation eo) {
		EOScolConstanteLocalisation localInstance = (eo == null) ? null : (EOScolConstanteLocalisation) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String clocLibelle() {
		return (String) storedValueForKey("clocLibelle");
	}

	public void setClocLibelle(String value) {
		takeStoredValueForKey(value, "clocLibelle");
	}
	public String clocStatut() {
		return (String) storedValueForKey("clocStatut");
	}

	public void setClocStatut(String value) {
		takeStoredValueForKey(value, "clocStatut");
	}
	public String etabCode() {
		return (String) storedValueForKey("etabCode");
	}

	public void setEtabCode(String value) {
		takeStoredValueForKey(value, "etabCode");
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toFwkpers_Adresse() {
		return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey("toFwkpers_Adresse");
	}

	public void setToFwkpers_AdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
		if (value == null) {
			org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toFwkpers_Adresse();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Adresse");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Adresse");
		}
	}
  
	public org.cocktail.scolarix.serveur.metier.eos.EOTypeEtablissementUlr toFwkScolarix_TypeEtablissementUlr() {
		return (org.cocktail.scolarix.serveur.metier.eos.EOTypeEtablissementUlr)storedValueForKey("toFwkScolarix_TypeEtablissementUlr");
	}

	public void setToFwkScolarix_TypeEtablissementUlrRelationship(org.cocktail.scolarix.serveur.metier.eos.EOTypeEtablissementUlr value) {
		if (value == null) {
			org.cocktail.scolarix.serveur.metier.eos.EOTypeEtablissementUlr oldValue = toFwkScolarix_TypeEtablissementUlr();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarix_TypeEtablissementUlr");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarix_TypeEtablissementUlr");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolConstanteLocalisation.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolConstanteLocalisation.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolConstanteLocalisation)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolConstanteLocalisation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolConstanteLocalisation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolConstanteLocalisation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolConstanteLocalisation)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolConstanteLocalisation fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolConstanteLocalisation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolConstanteLocalisation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolConstanteLocalisation eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolConstanteLocalisation)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolConstanteLocalisation fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolConstanteLocalisation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolConstanteLocalisation eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolConstanteLocalisation ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolConstanteLocalisation createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolConstanteLocalisation.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolConstanteLocalisation.ENTITY_NAME + "' !");
		}
		else {
			EOScolConstanteLocalisation object = (EOScolConstanteLocalisation) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolConstanteLocalisation localInstanceOfObject(EOEditingContext ec, EOScolConstanteLocalisation object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolConstanteLocalisation " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolConstanteLocalisation) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
