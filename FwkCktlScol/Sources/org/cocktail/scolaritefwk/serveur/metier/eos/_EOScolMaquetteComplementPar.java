/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteComplementPar.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteComplementPar extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteComplementPar";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_COMPLEMENT_PAR";

	//Attributes

	public static final ERXKey<Integer> MPAR_ADMISSION_DESC = new ERXKey<Integer>("mparAdmissionDesc");
	public static final ERXKey<Integer> MPAR_BESOINS_PARTICULIERS = new ERXKey<Integer>("mparBesoinsParticuliers");
	public static final ERXKey<Integer> MPAR_CONTENU = new ERXKey<Integer>("mparContenu");
	public static final ERXKey<Integer> MPAR_DEBOUCHES = new ERXKey<Integer>("mparDebouches");
	public static final ERXKey<Integer> MPAR_OBJECTIFS = new ERXKey<Integer>("mparObjectifs");
	public static final ERXKey<Integer> MPAR_POURSUITES = new ERXKey<Integer>("mparPoursuites");
	public static final ERXKey<Integer> MPAR_PREREQUIS = new ERXKey<Integer>("mparPrerequis");
	public static final ERXKey<Integer> MPAR_PUBLIC_CIBLE = new ERXKey<Integer>("mparPublicCible");
	public static final ERXKey<Integer> MPAR_QUALIF_DESC = new ERXKey<Integer>("mparQualifDesc");
	public static final ERXKey<Integer> MPAR_RECOMMANDATIONS = new ERXKey<Integer>("mparRecommandations");
	public static final ERXKey<Integer> MPAR_REGLEMENT = new ERXKey<Integer>("mparReglement");

	public static final String MPAR_ADMISSION_DESC_KEY = "mparAdmissionDesc";
	public static final String MPAR_BESOINS_PARTICULIERS_KEY = "mparBesoinsParticuliers";
	public static final String MPAR_CONTENU_KEY = "mparContenu";
	public static final String MPAR_DEBOUCHES_KEY = "mparDebouches";
	public static final String MPAR_OBJECTIFS_KEY = "mparObjectifs";
	public static final String MPAR_POURSUITES_KEY = "mparPoursuites";
	public static final String MPAR_PREREQUIS_KEY = "mparPrerequis";
	public static final String MPAR_PUBLIC_CIBLE_KEY = "mparPublicCible";
	public static final String MPAR_QUALIF_DESC_KEY = "mparQualifDesc";
	public static final String MPAR_RECOMMANDATIONS_KEY = "mparRecommandations";
	public static final String MPAR_REGLEMENT_KEY = "mparReglement";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String MPAR_KEY_KEY = "mparKey";

	// Colkeys
	public static final String MPAR_ADMISSION_DESC_COLKEY = "MPAR_ADMISSION_DESC";
	public static final String MPAR_BESOINS_PARTICULIERS_COLKEY = "MPAR_BESOINS_PARTICULIERS";
	public static final String MPAR_CONTENU_COLKEY = "MPAR_CONTENU";
	public static final String MPAR_DEBOUCHES_COLKEY = "MPAR_DEBOUCHES";
	public static final String MPAR_OBJECTIFS_COLKEY = "MPAR_OBJECTIFS";
	public static final String MPAR_POURSUITES_COLKEY = "MPAR_POURSUITES";
	public static final String MPAR_PREREQUIS_COLKEY = "MPAR_PREREQUIS";
	public static final String MPAR_PUBLIC_CIBLE_COLKEY = "MPAR_PUBLIC_CIBLE";
	public static final String MPAR_QUALIF_DESC_COLKEY = "MPAR_QUALIF_DESC";
	public static final String MPAR_RECOMMANDATIONS_COLKEY = "MPAR_RECOMMANDATIONS";
	public static final String MPAR_REGLEMENT_COLKEY = "MPAR_REGLEMENT";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String MPAR_KEY_COLKEY = "MPAR_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours> TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours>("toFwkScolarite_ScolMaquetteParcours");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY = "toFwkScolarite_ScolMaquetteParcours";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteComplementPar with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mparContenu
	 * @param mparDebouches
	 * @param mparObjectifs
	 * @param mparPoursuites
	 * @param mparPrerequis
	 * @param mparQualifDesc
	 * @param mparRecommandations
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolMaquetteParcours
	 * @return EOScolMaquetteComplementPar
	 */
	public static EOScolMaquetteComplementPar create(EOEditingContext editingContext, Integer mparContenu, Integer mparDebouches, Integer mparObjectifs, Integer mparPoursuites, Integer mparPrerequis, Integer mparQualifDesc, Integer mparRecommandations, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours) {
		EOScolMaquetteComplementPar eo = (EOScolMaquetteComplementPar) createAndInsertInstance(editingContext);
		eo.setMparContenu(mparContenu);
		eo.setMparDebouches(mparDebouches);
		eo.setMparObjectifs(mparObjectifs);
		eo.setMparPoursuites(mparPoursuites);
		eo.setMparPrerequis(mparPrerequis);
		eo.setMparQualifDesc(mparQualifDesc);
		eo.setMparRecommandations(mparRecommandations);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolMaquetteParcoursRelationship(toFwkScolarite_ScolMaquetteParcours);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteComplementPar.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteComplementPar
	 */
	public static EOScolMaquetteComplementPar create(EOEditingContext editingContext) {
		EOScolMaquetteComplementPar eo = (EOScolMaquetteComplementPar) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteComplementPar localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteComplementPar localInstance = (EOScolMaquetteComplementPar) localInstanceOfObject(editingContext, (EOScolMaquetteComplementPar) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteComplementPar localInstanceIn(EOEditingContext editingContext, EOScolMaquetteComplementPar eo) {
		EOScolMaquetteComplementPar localInstance = (eo == null) ? null : (EOScolMaquetteComplementPar) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public Integer mparAdmissionDesc() {
		return (Integer) storedValueForKey("mparAdmissionDesc");
	}

	public void setMparAdmissionDesc(Integer value) {
		takeStoredValueForKey(value, "mparAdmissionDesc");
	}
	public Integer mparBesoinsParticuliers() {
		return (Integer) storedValueForKey("mparBesoinsParticuliers");
	}

	public void setMparBesoinsParticuliers(Integer value) {
		takeStoredValueForKey(value, "mparBesoinsParticuliers");
	}
	public Integer mparContenu() {
		return (Integer) storedValueForKey("mparContenu");
	}

	public void setMparContenu(Integer value) {
		takeStoredValueForKey(value, "mparContenu");
	}
	public Integer mparDebouches() {
		return (Integer) storedValueForKey("mparDebouches");
	}

	public void setMparDebouches(Integer value) {
		takeStoredValueForKey(value, "mparDebouches");
	}
	public Integer mparObjectifs() {
		return (Integer) storedValueForKey("mparObjectifs");
	}

	public void setMparObjectifs(Integer value) {
		takeStoredValueForKey(value, "mparObjectifs");
	}
	public Integer mparPoursuites() {
		return (Integer) storedValueForKey("mparPoursuites");
	}

	public void setMparPoursuites(Integer value) {
		takeStoredValueForKey(value, "mparPoursuites");
	}
	public Integer mparPrerequis() {
		return (Integer) storedValueForKey("mparPrerequis");
	}

	public void setMparPrerequis(Integer value) {
		takeStoredValueForKey(value, "mparPrerequis");
	}
	public Integer mparPublicCible() {
		return (Integer) storedValueForKey("mparPublicCible");
	}

	public void setMparPublicCible(Integer value) {
		takeStoredValueForKey(value, "mparPublicCible");
	}
	public Integer mparQualifDesc() {
		return (Integer) storedValueForKey("mparQualifDesc");
	}

	public void setMparQualifDesc(Integer value) {
		takeStoredValueForKey(value, "mparQualifDesc");
	}
	public Integer mparRecommandations() {
		return (Integer) storedValueForKey("mparRecommandations");
	}

	public void setMparRecommandations(Integer value) {
		takeStoredValueForKey(value, "mparRecommandations");
	}
	public Integer mparReglement() {
		return (Integer) storedValueForKey("mparReglement");
	}

	public void setMparReglement(Integer value) {
		takeStoredValueForKey(value, "mparReglement");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours toFwkScolarite_ScolMaquetteParcours() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours)storedValueForKey("toFwkScolarite_ScolMaquetteParcours");
	}

	public void setToFwkScolarite_ScolMaquetteParcoursRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours oldValue = toFwkScolarite_ScolMaquetteParcours();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolMaquetteParcours");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolMaquetteParcours");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteComplementPar.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteComplementPar.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteComplementPar)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteComplementPar fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteComplementPar fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteComplementPar eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteComplementPar)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteComplementPar fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteComplementPar fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteComplementPar fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteComplementPar eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteComplementPar)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteComplementPar fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteComplementPar fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteComplementPar eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteComplementPar ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteComplementPar createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteComplementPar.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteComplementPar.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteComplementPar object = (EOScolMaquetteComplementPar) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteComplementPar localInstanceOfObject(EOEditingContext ec, EOScolMaquetteComplementPar object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteComplementPar " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteComplementPar) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
