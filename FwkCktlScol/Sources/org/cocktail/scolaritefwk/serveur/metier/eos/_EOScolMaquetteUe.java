/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOScolMaquetteUe.java instead.
package org.cocktail.scolaritefwk.serveur.metier.eos;


import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXKey;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import er.extensions.eof.ERXGenericRecord;

@SuppressWarnings("all")
public abstract class _EOScolMaquetteUe extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkScolarite_ScolMaquetteUe";
	public static final String ENTITY_TABLE_NAME = "SCOLARITE.SCOL_MAQUETTE_UE";

	//Attributes
	public static final String ENTITY_PRIMARY_KEY = "mueKey";

	public static final ERXKey<String> MUE_CODE = new ERXKey<String>("mueCode");
	public static final ERXKey<java.math.BigDecimal> MUE_HORAIRE_ETU = new ERXKey<java.math.BigDecimal>("mueHoraireEtu");
	public static final ERXKey<String> MUE_LIBELLE = new ERXKey<String>("mueLibelle");
	public static final ERXKey<java.math.BigDecimal> MUE_POINTS = new ERXKey<java.math.BigDecimal>("muePoints");

	public static final String MUE_CODE_KEY = "mueCode";
	public static final String MUE_HORAIRE_ETU_KEY = "mueHoraireEtu";
	public static final String MUE_LIBELLE_KEY = "mueLibelle";
	public static final String MUE_POINTS_KEY = "muePoints";

	// Non visible attributes
	public static final String FANN_KEY_KEY = "fannKey";
	public static final String FDOM_CODE_KEY = "fdomCode";
	public static final String MUE_KEY_KEY = "mueKey";

	// Colkeys
	public static final String MUE_CODE_COLKEY = "MUE_CODE";
	public static final String MUE_HORAIRE_ETU_COLKEY = "MUE_HORAIRE_ETU";
	public static final String MUE_LIBELLE_COLKEY = "MUE_LIBELLE";
	public static final String MUE_POINTS_COLKEY = "MUE_POINTS";

	// Non visible colkeys
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FDOM_CODE_COLKEY = "FDOM_CODE";
	public static final String MUE_KEY_COLKEY = "MUE_KEY";

	// Relationships
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee> TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee>("toFwkScolarite_ScolFormationAnnee");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine> TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine>("toFwkScolarite_ScolFormationDomaine");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc>("toFwkScolarite_ScolMaquetteRepartitionEcs");
	public static final ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe> TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES = new ERXKey<org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe>("toFwkScolarite_ScolMaquetteRepartitionUes");

	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY = "toFwkScolarite_ScolFormationAnnee";
	public static final String TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY = "toFwkScolarite_ScolFormationDomaine";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY = "toFwkScolarite_ScolMaquetteRepartitionEcs";
	public static final String TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY = "toFwkScolarite_ScolMaquetteRepartitionUes";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOScolMaquetteUe with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param mueCode
	 * @param mueHoraireEtu
	 * @param mueLibelle
	 * @param muePoints
	 * @param toFwkScolarite_ScolFormationAnnee
	 * @param toFwkScolarite_ScolFormationDomaine
	 * @return EOScolMaquetteUe
	 */
	public static EOScolMaquetteUe create(EOEditingContext editingContext, String mueCode, java.math.BigDecimal mueHoraireEtu, String mueLibelle, java.math.BigDecimal muePoints, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee, org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine toFwkScolarite_ScolFormationDomaine) {
		EOScolMaquetteUe eo = (EOScolMaquetteUe) createAndInsertInstance(editingContext);
		eo.setMueCode(mueCode);
		eo.setMueHoraireEtu(mueHoraireEtu);
		eo.setMueLibelle(mueLibelle);
		eo.setMuePoints(muePoints);
		eo.setToFwkScolarite_ScolFormationAnneeRelationship(toFwkScolarite_ScolFormationAnnee);
		eo.setToFwkScolarite_ScolFormationDomaineRelationship(toFwkScolarite_ScolFormationDomaine);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOScolMaquetteUe.
	 *
	 * @param editingContext
	 * @return EOScolMaquetteUe
	 */
	public static EOScolMaquetteUe create(EOEditingContext editingContext) {
		EOScolMaquetteUe eo = (EOScolMaquetteUe) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOScolMaquetteUe localInstanceIn(EOEditingContext editingContext) {
		EOScolMaquetteUe localInstance = (EOScolMaquetteUe) localInstanceOfObject(editingContext, (EOScolMaquetteUe) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOScolMaquetteUe localInstanceIn(EOEditingContext editingContext, EOScolMaquetteUe eo) {
		EOScolMaquetteUe localInstance = (eo == null) ? null : (EOScolMaquetteUe) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String mueCode() {
		return (String) storedValueForKey("mueCode");
	}

	public void setMueCode(String value) {
		takeStoredValueForKey(value, "mueCode");
	}
	public java.math.BigDecimal mueHoraireEtu() {
		return (java.math.BigDecimal) storedValueForKey("mueHoraireEtu");
	}

	public void setMueHoraireEtu(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "mueHoraireEtu");
	}
	public String mueLibelle() {
		return (String) storedValueForKey("mueLibelle");
	}

	public void setMueLibelle(String value) {
		takeStoredValueForKey(value, "mueLibelle");
	}
	public java.math.BigDecimal muePoints() {
		return (java.math.BigDecimal) storedValueForKey("muePoints");
	}

	public void setMuePoints(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "muePoints");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee toFwkScolarite_ScolFormationAnnee() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee)storedValueForKey("toFwkScolarite_ScolFormationAnnee");
	}

	public void setToFwkScolarite_ScolFormationAnneeRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee oldValue = toFwkScolarite_ScolFormationAnnee();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationAnnee");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationAnnee");
		}
	}
  
	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine toFwkScolarite_ScolFormationDomaine() {
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine)storedValueForKey("toFwkScolarite_ScolFormationDomaine");
	}

	public void setToFwkScolarite_ScolFormationDomaineRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine value) {
		if (value == null) {
			org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine oldValue = toFwkScolarite_ScolFormationDomaine();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkScolarite_ScolFormationDomaine");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "toFwkScolarite_ScolFormationDomaine");
		}
	}
  
	public NSArray toFwkScolarite_ScolMaquetteRepartitionEcs() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionEcs");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionEcs(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionEcs(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionEcs(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionEcs(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionEcs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionEcs();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionEcs");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionEcs");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc createToFwkScolarite_ScolMaquetteRepartitionEcsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionEc");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionEcs");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionEcsRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionEcs");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionEcsRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionEcs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionEcsRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc)objects.nextElement());
		}
	}
	public NSArray toFwkScolarite_ScolMaquetteRepartitionUes() {
		return (NSArray)storedValueForKey("toFwkScolarite_ScolMaquetteRepartitionUes");
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUes(EOQualifier qualifier) {
		return toFwkScolarite_ScolMaquetteRepartitionUes(qualifier, null, false);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUes(EOQualifier qualifier, boolean fetch) {
		return toFwkScolarite_ScolMaquetteRepartitionUes(qualifier, null, fetch);
	}

	public NSArray toFwkScolarite_ScolMaquetteRepartitionUes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = toFwkScolarite_ScolMaquetteRepartitionUes();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToToFwkScolarite_ScolMaquetteRepartitionUesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUes");
	}

	public void removeFromToFwkScolarite_ScolMaquetteRepartitionUesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUes");
	}

	public org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe createToFwkScolarite_ScolMaquetteRepartitionUesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkScolarite_ScolMaquetteRepartitionUe");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkScolarite_ScolMaquetteRepartitionUes");
		return (org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe) eo;
	}

	public void deleteToFwkScolarite_ScolMaquetteRepartitionUesRelationship(org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkScolarite_ScolMaquetteRepartitionUes");
				editingContext().deleteObject(object);
			}

	public void deleteAllToFwkScolarite_ScolMaquetteRepartitionUesRelationships() {
		Enumeration objects = toFwkScolarite_ScolMaquetteRepartitionUes().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToFwkScolarite_ScolMaquetteRepartitionUesRelationship((org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOScolMaquetteUe.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOScolMaquetteUe.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOScolMaquetteUe)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOScolMaquetteUe fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOScolMaquetteUe fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOScolMaquetteUe eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOScolMaquetteUe)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOScolMaquetteUe fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteUe fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOScolMaquetteUe fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOScolMaquetteUe eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOScolMaquetteUe)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteUe fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOScolMaquetteUe fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOScolMaquetteUe eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOScolMaquetteUe ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOScolMaquetteUe createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOScolMaquetteUe.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOScolMaquetteUe.ENTITY_NAME + "' !");
		}
		else {
			EOScolMaquetteUe object = (EOScolMaquetteUe) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOScolMaquetteUe localInstanceOfObject(EOEditingContext ec, EOScolMaquetteUe object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOScolMaquetteUe " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOScolMaquetteUe) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
