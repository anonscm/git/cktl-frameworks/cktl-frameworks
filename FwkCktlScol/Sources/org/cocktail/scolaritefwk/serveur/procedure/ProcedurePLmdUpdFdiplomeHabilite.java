/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolaritefwk.serveur.exception.ScolariteFwkException;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedurePLmdUpdFdiplomeHabilite {

	private static final String PROCEDURE_NAME = "FwkScolarite_pLmdUpdFdiplomeHabilite";

	/**
	 * Appele la procedure de modification d'une formation.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param specialisation
	 *            EOScolFormationSpecialisation qui sera enregistree
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOScolFormationSpecialisation specialisation) throws NSValidation.ValidationException {
		if (specialisation == null) {
			throw new ScolariteFwkException("Formation a enregistrer NULL !");
		}
		boolean ok;
		try {
			specialisation.validateForInsert();
			ok = dataBus.executeProcedure(ProcedurePLmdUpdFdiplomeHabilite.PROCEDURE_NAME, ProcedurePLmdUpdFdiplomeHabilite.construireDictionnaireEnregistrer(dataBus.editingContext(), specialisation));
			if (ok && specialisation.fspnKey() == null) {
				specialisation.setFspnKey((Integer) dataBus.executedProcResult().valueForKey("0220fspnkey"));
			}
		} catch (ScolariteFwkException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ScolariteFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la formation.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param specialisation
	 *            EOScolFormationSpecialisation pour lequel on construit le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaireEnregistrer(EOEditingContext ec, EOScolFormationSpecialisation specialisation) {
		NSMutableDictionary dico = new NSMutableDictionary();
		if (specialisation == null) {
			return dico;
		}

		// recuperation du diplome...
		EOScolFormationDiplome diplome = specialisation.toFwkScolarite_ScolFormationDiplome();
		if (diplome == null) {
			throw new ScolariteFwkException("ProcedurePLmdUpdFdiplomeHabilite : Impossible de retrouver un diplome a la formation associee !");
		}

		// recuperation du diplome Sise...
		EOScolFormationDiplomeSise diplomeSise = diplome.toFwkScolarite_ScolFormationDiplomeSise();
		if (diplomeSise == null) {
			throw new ScolariteFwkException("ProcedurePLmdUpdFdiplomeHabilite : Impossible de retrouver un diplome Sise a la formation associee !");
		}

		// Construction du dico...
		dico.takeValueForKey(diplome.fdipCode(), "0010fdipcode");
		
		if (diplome.toFwkScolarite_ScolFormationDiplomeSise() != null) {
			dico.takeValueForKey(diplome.toFwkScolarite_ScolFormationDiplomeSise().fdipCodeSise(), "0020fdipcodesise");
		}
		if (diplome.toFwkScolarite_ScolFormationGrade() != null) {
			dico.takeValueForKey(diplome.toFwkScolarite_ScolFormationGrade().code(), "0030fgracode");
		}
		if (diplome.toFwkScolarite_ScolFormationDomaine() != null) {
			dico.takeValueForKey(diplome.toFwkScolarite_ScolFormationDomaine().code(), "0040fdomcode");
		}
		if (diplome.toFwkScolarite_ScolFormationVocation() != null) {
			dico.takeValueForKey(diplome.toFwkScolarite_ScolFormationVocation().fvocCode(), "0050fvoccode");
		}

		dico.takeValueForKey(diplome.fdipMention(), "0060fdipmention");
		dico.takeValueForKey(diplome.fdipSpecialite(), "0070fdipspecialite");
		dico.takeValueForKey(diplome.fdipLibelle(), "0080fdiplibelle");
		dico.takeValueForKey(diplome.fdipAbreviation(), "0090fdipabreviation");
		dico.takeValueForKey(diplome.habilitationDepart(), "0100fdipdepart");
		dico.takeValueForKey(diplome.habilitationArrivee(), "0110fdiparrivee");

		if (diplome.toFwkScolarix_VEtablissementScolarite() != null) {
			dico.takeValueForKey(diplome.toFwkScolarix_VEtablissementScolarite().cRne(), "0120etabcode");
		}
		if (diplome.toFwkScolarix_VComposanteScolarite() != null) {
			dico.takeValueForKey(EOUtilities.primaryKeyForObject(ec, diplome.toFwkScolarix_VComposanteScolarite()).objectForKey(EOVComposanteScolarite.C_RNE_KEY), "0130compcode");		
		}
			
		dico.takeValueForKey("", "0140edoccode");
		dico.takeValueForKey(diplome.fdipModele(), "0150fdipmodele");
		dico.takeValueForKey(diplome.fdipTypeDroit(), "0160fdiptypedroit");
		dico.takeValueForKey("", "0170sremocode");
		dico.takeValueForKey(diplome.fdipType(), "0180fdiptype");
		dico.takeValueForKey(diplome.fdipSemestrialisation(), "0190fdipsemestrialisation");
		dico.takeValueForKey(diplome.fdipDeliberation(), "0200fdipdeliberation");
		dico.takeValueForKey(diplome.fdipMonoSemestre(), "0210fdipmonosemestre");
		dico.takeValueForKey(specialisation.fspnKey(), "0220fspnkey");
		dico.takeValueForKey(specialisation.fspnLibelle(), "0230fspnlibelle");
		dico.takeValueForKey(specialisation.fspnAbreviation(), "0240fspnabreviation");
		dico.takeValueForKey(diplome.habilitationDepart(), "0250fanndebut");
		dico.takeValueForKey(diplome.habilitationArrivee(), "0260fannfin");

		return dico;
	}
}