package org.cocktail.scolaritefwk.serveur.procedure;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureDelDossierAnnee {
	private static final String PROCEDURE_NAME = "FwkScolarix_delDossierAnnee";

	/**
	 * Appele la procedure de suppression d'un INSC_DIPL.
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param inscDipl
	 *            EOInscDipl qui sera supprimé
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOEtudiant etudiant, Integer histAnnee) throws NSValidation.ValidationException {
		if (etudiant == null) {
			throw new ScolarixFwkException("InscDipl à supprimer NULL !");
		}
		boolean ok;
		try {
			etudiant.validateForDelete();
			ok = dataBus.executeProcedure(ProcedureDelDossierAnnee.PROCEDURE_NAME,
					ProcedureDelDossierAnnee.construireDictionnaire(dataBus.editingContext(), etudiant, histAnnee));
		}
		catch (ScolarixFwkException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new ScolarixFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir du inscDipl.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param inscDipl
	 *            EOInscDipl pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary<String, Object> construireDictionnaire(EOEditingContext ec, EOEtudiant etudiant, Integer histAnnee) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
		if (etudiant == null) {
			return dico;
		}

		dico.takeValueForKey(etudiant.etudNumero(), "0010etudnumero");
		dico.takeValueForKey(histAnnee, "0020histannee");

		return dico;
	}
}
