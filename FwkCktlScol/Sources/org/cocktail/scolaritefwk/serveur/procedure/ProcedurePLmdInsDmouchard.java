/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolaritefwk.serveur.exception.ScolariteFwkException;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitMouchard;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedurePLmdInsDmouchard {

	private static final String PROCEDURE_NAME = "FwkScolarite_pLmdInsDmouchard";

	/**
	 * Appele la procedure de creation d'un "mouchard".
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param mouchard
	 *            EOScolDroitMouchard qui sera enregistre
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOScolDroitMouchard mouchard) throws NSValidation.ValidationException {
		if (mouchard == null) {
			throw new ScolariteFwkException("Mouchard a enregistrer NULL !");
		}
		boolean ok;
		try {
			mouchard.validateForSave();
			ok = dataBus.executeProcedure(ProcedurePLmdInsDmouchard.PROCEDURE_NAME, ProcedurePLmdInsDmouchard.construireDictionnaireEnregistrer(dataBus.editingContext(), mouchard));
		} catch (ScolariteFwkException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ScolariteFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la formation.
	 * 
	 * @param ec
	 *            EOEditingContext dans lequel on travaille
	 * @param mouchard
	 *            EOScolDroitMouchard pour lequel on construit le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaireEnregistrer(EOEditingContext ec, EOScolDroitMouchard mouchard) {
		NSMutableDictionary dico = new NSMutableDictionary();
		if (mouchard == null) {
			return dico;
		}

		// recuperation de l'utilisateur...
		EOScolDroitLogin login = mouchard.toFwkScolarite_ScolDroitLogin();
		if (login == null) {
			throw new ScolariteFwkException("ProcedurePLmdInsDmouchard : Impossible de retrouver l'utilisateur associe !");
		}

		// Construction du dico...
		dico.takeValueForKey(null, "0010dmoukey");
		dico.takeValueForKey(EOUtilities.primaryKeyForObject(ec, login).objectForKey(EOScolDroitLogin.DLOG_KEY), "0020dlogkey");		
		dico.takeValueForKey(mouchard.dmouApplication(), "0030dmouapplication");
		dico.takeValueForKey(mouchard.dmouOperation(), "0040dmouoperation");
		dico.takeValueForKey(mouchard.dmouTable(), "0050dmoutable");
		dico.takeValueForKey(mouchard.dmouEnregistrement(), "0060dmouenregistrement");
		dico.takeValueForKey(mouchard.dmouQuand(), "0070dmouquand");

		return dico;
	}
}