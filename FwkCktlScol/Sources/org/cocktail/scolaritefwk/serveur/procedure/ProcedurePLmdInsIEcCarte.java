package org.cocktail.scolaritefwk.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolaritefwk.serveur.exception.ScolariteFwkException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedurePLmdInsIEcCarte {
	private static final String PROCEDURE_NAME = "FwkScolarite_pLmdInsIEcCarte";
	
	
	public static boolean enregistrer(_CktlBasicDataBus dataBus, int idiplNumero, int mrueKey, int mrecKey, int msemOrdre, int imrecDispense, int fannKey ) throws NSValidation.ValidationException {
		boolean ok;
		try {
			ok = dataBus.executeProcedure(ProcedurePLmdInsIEcCarte.PROCEDURE_NAME, ProcedurePLmdInsIEcCarte.construireDictionnaireEnregistrer(dataBus.editingContext(), idiplNumero, mrueKey, mrecKey, msemOrdre, imrecDispense, fannKey ));
		} catch (ScolariteFwkException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ScolariteFwkException(e.getMessage());
		}
		return ok;
	}
	
	private static NSDictionary construireDictionnaireEnregistrer(EOEditingContext ec,int idiplNumero, int mrueKey, int mrecKey, int msemOrdre, int imrecDispense, int fannKey) {
		NSMutableDictionary dico = new NSMutableDictionary();

		// Construction du dico...
		if (idiplNumero ==0) {
			throw new ScolariteFwkException("ProcedurePLmdInsIEc : Diplome null !");
		}
		dico.takeValueForKey(idiplNumero, "010idiplnumero");
		dico.takeValueForKey(mrueKey, "020mruekey");
		if (mrecKey ==0) {
			throw new ScolariteFwkException("ProcedurePLmdInsIEc : mrecKey null !");
		}
		dico.takeValueForKey(mrecKey, "030mreckey");
		dico.takeValueForKey(msemOrdre, "040msemordre");
		dico.takeValueForKey(imrecDispense, "050imrecdispense");
		dico.takeValueForKey(fannKey, "060fannkey");
		return dico;
	}
}
