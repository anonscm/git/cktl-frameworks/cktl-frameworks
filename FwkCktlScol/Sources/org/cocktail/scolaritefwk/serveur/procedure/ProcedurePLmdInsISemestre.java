package org.cocktail.scolaritefwk.serveur.procedure;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.scolaritefwk.serveur.exception.ScolariteFwkException;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolDroitLogin;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedurePLmdInsISemestre {
	private static final String PROCEDURE_NAME = "FwkScolarite_pLmdInsISemestre";

	/**
	 * Appel de la procedure stockee FwkScolarite_pLmdInsISemestre
	 * @param dataBus
	 * @param idiplNumero
	 * @param mrsemKey
	 * @param msemOrdre
	 * @param fannKey
	 * @param etat
	 * @param utilisateur
	 * @param applicationName
	 * @return
	 * @throws NSValidation.ValidationException
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, int idiplNumero, int mrsemKey, int msemOrdre, int fannKey, int etat, EOScolDroitLogin utilisateur, String applicationName) throws NSValidation.ValidationException {
		boolean ok;
		try {
			ok = dataBus.executeProcedure(ProcedurePLmdInsISemestre.PROCEDURE_NAME, ProcedurePLmdInsISemestre.construireDictionnaireEnregistrer(dataBus.editingContext(), idiplNumero, mrsemKey, msemOrdre, fannKey, etat));
		} catch (ScolariteFwkException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ScolariteFwkException(e.getMessage());
		}
		return ok;
	}

	/**
	 * Constructiuon des parametres d'entree
	 * @param ec
	 * @param idiplNumero
	 * @param mrsemKey
	 * @param msemOrdre
	 * @param fannKey
	 * @param etat
	 * @return
	 */
	private static NSDictionary construireDictionnaireEnregistrer(EOEditingContext ec, int idiplNumero, int mrsemKey, int msemOrdre, int fannKey, int etat) {
		NSMutableDictionary dico = new NSMutableDictionary();

		// Construction du dico...
		dico.takeValueForKey(idiplNumero, "010idiplnumero");
		dico.takeValueForKey(mrsemKey, "020mrsemkey");
		dico.takeValueForKey(msemOrdre, "030msemordre");
		dico.takeValueForKey(fannKey, "040fannkey");
		dico.takeValueForKey(etat, "050etat");
		return dico;
	}
}
