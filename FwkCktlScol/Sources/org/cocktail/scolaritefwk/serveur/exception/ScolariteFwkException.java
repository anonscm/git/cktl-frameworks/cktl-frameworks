/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.exception;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation._NSStringUtilities;

public class ScolariteFwkException extends RuntimeException {

	private String message;
	private boolean bloquant;

	public ScolariteFwkException() {
		super();
		bloquant = true;
	}

	public ScolariteFwkException(String s) {
		super();
		this.setMessage(s);
		bloquant = true;
	}

	public ScolariteFwkException(String s, boolean yn) {
		super();
		this.setMessage(s);
		bloquant = yn;
	}

	public boolean isBloquant() {
		return bloquant;
	}

	/**
	 * Indique si l'exception est une exception grave ou si il s'agit d'une exception d'information
	 */
	public boolean isInformatif() {
		boolean isInformatif = false;
		String message = getMessageFormatte();
		if (message != null) {
			int index = message.indexOf("ORA-");
			if (index > -1) {
				index = message.indexOf("ORA-20001:");
				if (index > -1) {
					// Exception Oracle renvoyee par les procedures
					// Message informatif pour l'utilisateur
					isInformatif = true;
				} else {
					// Exception Oracle renvoyee par les procedures
					// Exception "non controlee" par les procedures
					isInformatif = false;
				}
			} else {
				// Erreur de validation
				// Message informatif pour l'utilisateur
				isInformatif = true;
			}
		}
		return isInformatif;
	}

	/**
	 * Renvoie un message reformatte si possible
	 */
	public String getMessageFormatte() {
		String message = getMessage();
		if (message != null) {
			message = StringCtrl.replace(message, "'", "\\'");
			message = message.replaceAll("\n", " ");
			message = message.replaceAll("\"", "''");

			int index = message.indexOf("ORA-20001:");
			if (index > -1) {
				message = message.substring(index + 10);
				index = message.indexOf("ORA-");
				if (index > -1) {
					message = message.substring(0, index);
				}
			} else {
				// Pour des raisons historiques, certains messages d'erreurs en provenance de la base 
				index = message.indexOf("ORA-20016:");
				if (index > -1) {
					message = message.substring(index + 10);
					index = message.indexOf("ORA-");
					if (index > -1) {
						message = message.substring(0, index);
					}
				}
			}
		}
		return message;
	}

	public String getMessageHTML() {
		String message = getMessage();
		if (message != null) {
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "é", "&eacute;");
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "ê", "&ecirc;");
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "è", "&egrave;");
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "à", "&agrave;");
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "\n", "<br/>");
		}
		return message;
	}
	public String getMessageJS() {
		String message = getMessage();
		if (message != null) {
/*			message = _NSStringUtilities.replaceAllInstancesOfString(message, "é", "&eacute;");
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "ê", "&ecirc;");
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "è", "&egrave;");
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "à", "&agrave;");
			message = _NSStringUtilities.replaceAllInstancesOfString(message, "\n", "<br/>");
*/			message = _NSStringUtilities.replaceAllInstancesOfString(message, "'", "\\'");
		}
		return message;
	}
	/**
	 * renvoie une cause message pour l exception
	 * 
	 * @return String
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * affecte une cause message pour l exception
	 * 
	 * @param string
	 */
	public void setMessage(String string) {
		message = string;
	}
}
