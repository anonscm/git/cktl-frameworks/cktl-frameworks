/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.factory;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplomeSise;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryScolFormationDiplome extends Factory {

	public FactoryScolFormationDiplome() {
		super();
	}

	public FactoryScolFormationDiplome(boolean withLog) {
		super(withLog);
	}

	public static EOScolFormationDiplome create(EOEditingContext ec) {
		EOScolFormationDiplome newScolFormationDiplome = (EOScolFormationDiplome) Factory.instanceForEntity(ec, EOScolFormationDiplome.ENTITY_NAME);
		ec.insertObject(newScolFormationDiplome);
		
		EOScolFormationDiplomeSise newScolFormationDiplomeSise = FactoryScolFormationDiplomeSise.create(ec);
		newScolFormationDiplome.setToFwkScolarite_ScolFormationDiplomeSiseRelationship(newScolFormationDiplomeSise);
		
		newScolFormationDiplome.setToFwkScolarite_ScolFormationGradeRelationship(null);
		newScolFormationDiplome.setToFwkScolarite_ScolFormationFiliereRelationship(null);
		
		newScolFormationDiplome.setToFwkScolarite_ScolFormationDomaineRelationship(null);
		newScolFormationDiplome.setToFwkScolarite_ScolFormationDepartementRelationship(null);
		
		newScolFormationDiplome.setToFwkScolarite_ScolFormationVocationRelationship(null);
		newScolFormationDiplome.setToFwkScolarix_VEtablissementScolariteRelationship(null);
		newScolFormationDiplome.setToFwkScolarix_VComposanteScolariteRelationship(null);
		
		return newScolFormationDiplome;
	}

	/**
	 * Creation d'un nouvel EOScolFormationDiplome de type "LMD".<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @return un EOScolFormationDiplome.
	 */
	public static EOScolFormationDiplome createWithDefaultValuesLmd(EOEditingContext ec) {
		EOScolFormationDiplome newScolFormationDiplome = create(ec);
		
		newScolFormationDiplome.setFdipType(EOScolFormationDiplome.FORMATION_LMD);
		newScolFormationDiplome.setFdipSemestrialisation(EOScolFormationDiplome.ORGANISATION_SEMESTRIELLE);
		newScolFormationDiplome.setFdipDeliberation(EOScolFormationDiplome.DELIBERATION_SEMESTRIELLE);
		newScolFormationDiplome.setFdipMonoSemestre(EOScolFormationDiplome.REPARTITION_MULTI_SEMESTRE);
		
		return newScolFormationDiplome;
	}

	/**
	 * Creation d'un nouvel EOScolFormationDiplome de type "FICTIF".<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @return un EOScolFormationDiplome.
	 */
	public static EOScolFormationDiplome createWithDefaultValuesFictif(EOEditingContext ec) {
		EOScolFormationDiplome newScolFormationDiplome = create(ec);
		
		newScolFormationDiplome.setFdipType(EOScolFormationDiplome.FORMATION_FICTIVE);
		newScolFormationDiplome.setFdipSemestrialisation(EOScolFormationDiplome.ORGANISATION_SEMESTRIELLE);
		newScolFormationDiplome.setFdipDeliberation(EOScolFormationDiplome.DELIBERATION_SEMESTRIELLE);
		newScolFormationDiplome.setFdipMonoSemestre(EOScolFormationDiplome.REPARTITION_MULTI_SEMESTRE);
		
		return newScolFormationDiplome;
	}

	/**
	 * Creation d'un nouvel EOScolFormationDiplome de type "CLASSIQUE".<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @return un EOScolFormationDiplome.
	 */
	public static EOScolFormationDiplome createWithDefaultValuesClassique(EOEditingContext ec) {
		EOScolFormationDiplome newScolFormationDiplome = create(ec);
		
		newScolFormationDiplome.setFdipType(EOScolFormationDiplome.FORMATION_CLASSIQUE);
		newScolFormationDiplome.setFdipSemestrialisation(EOScolFormationDiplome.ORGANISATION_ANNUELLE);
		newScolFormationDiplome.setFdipDeliberation(EOScolFormationDiplome.DELIBERATION_ANNUELLE);
		newScolFormationDiplome.setFdipMonoSemestre(EOScolFormationDiplome.REPARTITION_MULTI_SEMESTRE);
		
		return newScolFormationDiplome;
	}

}
