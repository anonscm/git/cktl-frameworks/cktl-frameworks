/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.factory;

import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FactoryScolFormationHabilitation extends Factory {

	public FactoryScolFormationHabilitation() {
		super();
	}

	public FactoryScolFormationHabilitation(boolean withLog) {
		super(withLog);
	}

	private static EOScolFormationHabilitation create(EOEditingContext ec) {
		EOScolFormationHabilitation newScolFormationHabilitation = (EOScolFormationHabilitation) Factory.instanceForEntity(ec, EOScolFormationHabilitation.ENTITY_NAME);
		ec.insertObject(newScolFormationHabilitation);
		
		newScolFormationHabilitation.setFhabOuvert(EOScolFormationHabilitation.HABILITATION_FERMEE);
		newScolFormationHabilitation.setToFwkScolarite_ScolFormationAnneeRelationship(null);
		newScolFormationHabilitation.setToFwkScolarite_ScolFormationSpecialisationRelationship(null);
	
		return newScolFormationHabilitation;
	}

	/**
	 * Creation d'un EOScolFormationHabilitation "non ouvert" 
	 * pour un niveau du diplôme défini en paramètre 
	 * et pour l'année universitaire définie.<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param specialisation
	 *            OBLIGATOIRE : de type EOScolFormationSpecialisation
	 * @param annee
	 *            OBLIGATOIRE : de type EOScolFormationAnnee
	 * @param niveau
	 *            OBLIGATOIRE : niveau de la formation
	 * @return un EOScolFormationHabilitation
	 */
	private static EOScolFormationHabilitation createWithDefaultValues(EOEditingContext ec, EOScolFormationSpecialisation specialisation, EOScolFormationAnnee annee, Integer niveau) {
		EOScolFormationHabilitation newScolFormationHabilitation = null;
		
		if ((specialisation != null) && (specialisation.toFwkScolarite_ScolFormationDiplome() != null)
				&& (specialisation.toFwkScolarite_ScolFormationDiplome().fdipDepart() != null)
				&& (specialisation.toFwkScolarite_ScolFormationDiplome().fdipArrivee() != null)) {
			if ((niveau.intValue() < specialisation.toFwkScolarite_ScolFormationDiplome().fdipDepart().intValue()) 
					|| (niveau.intValue() > specialisation.toFwkScolarite_ScolFormationDiplome().fdipArrivee().intValue())) {
				return newScolFormationHabilitation;
			}
		}
		
		if (annee != null) {
			newScolFormationHabilitation = create(ec);
			newScolFormationHabilitation.setFhabNiveau(niveau);
			newScolFormationHabilitation.setToFwkScolarite_ScolFormationAnneeRelationship(annee);
			if (specialisation != null) {
				newScolFormationHabilitation.setToFwkScolarite_ScolFormationSpecialisationRelationship(specialisation);
			}
		}

		return newScolFormationHabilitation;
	}

	/**
	 * Creation d'un EOScolFormationHabilitation "ouvert" 
	 * pour un niveau du diplôme défini en paramètre 
	 * et pour l'année universitaire définie.<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param specialisation
	 *            OBLIGATOIRE : de type EOScolFormationSpecialisation
	 * @param annee
	 *            OBLIGATOIRE : de type EOScolFormationAnnee
	 * @param niveau
	 *            OBLIGATOIRE : niveau de la formation
	 * @return un EOScolFormationHabilitation
	 */
	private static EOScolFormationHabilitation createWithDefaultValuesForOuverture(EOEditingContext ec, EOScolFormationSpecialisation specialisation, EOScolFormationAnnee annee, Integer niveau) {
		EOScolFormationHabilitation newScolFormationHabilitation = createWithDefaultValues(ec, specialisation, annee, niveau);
		
		newScolFormationHabilitation.setFhabOuvert(EOScolFormationHabilitation.HABILITATION_OUVERTE);

		return newScolFormationHabilitation;
	}

	/**
	 * Creation d'EOScolFormationHabilitation "non ouverts" 
	 * pour tous les niveaux du diplôme associé 
	 * et pour l'année universitaire définie.<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param specialisation
	 *            OBLIGATOIRE : de type EOScolFormationSpecialisation
	 * @param annee
	 *            OBLIGATOIRE : de type EOScolFormationAnnee
	 */
	private static void createWithDefaultValuesForHabilitationsMultiples(EOEditingContext ec, EOScolFormationSpecialisation specialisation, EOScolFormationAnnee annee) {
		if ((specialisation != null) && (specialisation.toFwkScolarite_ScolFormationDiplome() != null)) {
			if ((specialisation.toFwkScolarite_ScolFormationDiplome().fdipDepart() != null) && (specialisation.toFwkScolarite_ScolFormationDiplome().fdipArrivee() != null)) {
				EOScolFormationHabilitation newScolFormationHabilitation = null;
				int debut = specialisation.toFwkScolarite_ScolFormationDiplome().fdipDepart().intValue();
				int fin = specialisation.toFwkScolarite_ScolFormationDiplome().fdipArrivee().intValue();

				for (int i=debut; i<=fin; i++) {
					// Vérifier que l'objet n'existe pas déjà
					NSMutableArray qualifiers = new NSMutableArray();
					qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.FSPN_KEY_KEY, EOQualifier.QualifierOperatorEqual, specialisation.fspnKey()));
					qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.FHAB_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, new Integer(i)));
					qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, annee));
					NSArray habilitations = specialisation.toFwkScolarite_ScolFormationHabilitations(new EOAndQualifier(qualifiers));
					
					if ((habilitations != null) && (habilitations.count() == 0)) {
						newScolFormationHabilitation = createWithDefaultValues(ec, specialisation, annee, i);
						specialisation.addToToFwkScolarite_ScolFormationHabilitationsRelationship(newScolFormationHabilitation);
					}
				}
			}
		}
		
	}

	/**
	 * Creation d'EOScolFormationHabilitation "ouverts" 
	 * pour tous les niveaux du diplôme associé 
	 * et pour l'année universitaire définie.<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param specialisation
	 *            OBLIGATOIRE : de type EOScolFormationSpecialisation
	 * @param annee
	 *            OBLIGATOIRE : de type EOScolFormationAnnee
	 */
	private static void createWithDefaultValuesForHabilitationsOuvertesMultiples(EOEditingContext ec, EOScolFormationSpecialisation specialisation, EOScolFormationAnnee annee) {
		if ((specialisation != null) && (specialisation.toFwkScolarite_ScolFormationDiplome() != null)) {
			if ((specialisation.toFwkScolarite_ScolFormationDiplome().fdipDepart() != null) && (specialisation.toFwkScolarite_ScolFormationDiplome().fdipArrivee() != null)) {
				EOScolFormationHabilitation newScolFormationHabilitation = null;
				int debut = specialisation.toFwkScolarite_ScolFormationDiplome().fdipDepart().intValue();
				int fin = specialisation.toFwkScolarite_ScolFormationDiplome().fdipArrivee().intValue();

				for (int i=debut; i<=fin; i++) {
					// Vérifier que l'objet n'existe pas déjà
					NSMutableArray qualifiers = new NSMutableArray();
					qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.FSPN_KEY_KEY, EOQualifier.QualifierOperatorEqual, specialisation.fspnKey()));
					qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.FHAB_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, new Integer(i)));
					qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, annee));
					NSArray habilitations = specialisation.toFwkScolarite_ScolFormationHabilitations(new EOAndQualifier(qualifiers));
					
					if ((habilitations != null) && (habilitations.count() == 0)) {
						newScolFormationHabilitation = createWithDefaultValuesForOuverture(ec, specialisation, annee, i);
						specialisation.addToToFwkScolarite_ScolFormationHabilitationsRelationship(newScolFormationHabilitation);
					}
				}
			}
		}
	}

	/**
	 * Creation des EOScolFormationHabilitation "non ouverts" 
	 * pour tous les niveaux du diplôme associé 
	 * et pour la tranche d'années universitaires définie en paramètre.<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param specialisation
	 *            OBLIGATOIRE : de type EOScolFormationSpecialisation
	 * @param anneeDebut
	 *            OBLIGATOIRE : annee universitaire de debut d'habilitation
	 * @param anneeFin
	 *            OBLIGATOIRE : annee universitaire de fin d'habilitation 
	 */
	public static void createWithDefaultValuesForHabilitationsMultiples(EOEditingContext ec, EOScolFormationSpecialisation specialisation, Integer anneeDebut, Integer anneeFin) {
		if ((anneeDebut != null) && (anneeFin != null) && (anneeDebut <= anneeFin)) {
			for (int i=anneeDebut; i<=anneeFin; i++) {
				EOScolFormationAnnee newScolFormationAnnee = FinderScolFormationAnnee.getScolFormationAnnee(ec, i);
				createWithDefaultValuesForHabilitationsMultiples(ec, specialisation, newScolFormationAnnee);
			}
		}
	}

	/**
	 * Creation des EOScolFormationHabilitation "ouverts" 
	 * pour tous les niveaux du diplôme associé 
	 * et pour la tranche d'années universitaires définie en paramètre.<BR>
	 * 
	 * @param ec
	 *            OBLIGATOIRE
	 * @param specialisation
	 *            OBLIGATOIRE : de type EOScolFormationSpecialisation
	 * @param anneeDebut
	 *            OBLIGATOIRE : annee universitaire de debut d'habilitation
	 * @param anneeFin
	 *            OBLIGATOIRE : annee universitaire de fin d'habilitation 
	 */
	public static void createWithDefaultValuesForHabilitationsOuvertesMultiples(EOEditingContext ec, EOScolFormationSpecialisation specialisation, Integer anneeDebut, Integer anneeFin) {
		if ((anneeDebut != null) && (anneeFin != null) && (anneeDebut <= anneeFin)) {
			for (int i=anneeDebut; i<=anneeFin; i++) {
				EOScolFormationAnnee newScolFormationAnnee = FinderScolFormationAnnee.getScolFormationAnnee(ec, i);
				createWithDefaultValuesForHabilitationsOuvertesMultiples(ec, specialisation, newScolFormationAnnee);
			}
		}
	}

}
