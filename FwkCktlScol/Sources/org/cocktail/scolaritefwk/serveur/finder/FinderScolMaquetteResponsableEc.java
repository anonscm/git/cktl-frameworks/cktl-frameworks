/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteResponsableEc;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * @author Julien BLANDINEAU <jblandin@univ-lr.fr>
 * 
 */
public class FinderScolMaquetteResponsableEc extends Finder {

	/**
	 * @param edc
	 *            L'EditingContext
	 * @param persId
	 *            Le PERS_ID de l'individu
	 * @param anneeScol
	 *            L'année scolaire
	 * @return La liste des {@link EOScolMaquetteEc} pour lesquels l'individu est responsable ('R')
	 * @deprecated Utiliser
	 *             {@link FinderScolResponsabilites#getEcByResponsable(EOEditingContext, Integer, Integer, String)}
	 */
	public static NSArray<EOScolMaquetteEc> getEcForResponsable(EOEditingContext edc, Integer persId, Integer anneeScol) {
		EOQualifier qualifier = ERXQ.and(ERXQ.equals(EOScolMaquetteResponsableEc.FANN_KEY_KEY, anneeScol),
				ERXQ.equals(EOScolMaquetteResponsableEc.PERS_ID_KEY, persId),
				EOScolMaquetteResponsableEc.QUAL_RESPONSABLE);
		NSArray<EOScolMaquetteResponsableEc> listeMrec = EOScolMaquetteResponsableEc.fetchAll(edc, qualifier);
		NSArray<EOScolMaquetteEc> listeEc = (NSArray<EOScolMaquetteEc>) listeMrec
				.valueForKey(EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY);
		return ERXArrayUtilities.sortedArraySortedWithKey(listeEc, EOScolMaquetteEc.MEC_CODE_KEY);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @param persId
	 *            Le PERS_ID de l'individu
	 * @param anneeScol
	 *            L'année scolaire
	 * @return La liste des {@link EOScolMaquetteEc} pour lesquels l'individu est secrétaire ('S'), et non responsable
	 * @deprecated Utiliser
	 *             {@link FinderScolResponsabilites#getEcByResponsable(EOEditingContext, Integer, Integer, String)}
	 */
	public static NSArray<EOScolMaquetteEc> getEcForSecretaire(EOEditingContext edc, Integer persId, Integer anneeScol) {
		EOQualifier qualifier = ERXQ.and(ERXQ.equals(EOScolMaquetteResponsableEc.FANN_KEY_KEY, anneeScol),
				ERXQ.equals(EOScolMaquetteResponsableEc.PERS_ID_KEY, persId),
				EOScolMaquetteResponsableEc.QUAL_SECRETAIRE, ERXQ.not(EOScolMaquetteResponsableEc.QUAL_RESPONSABLE));
		NSArray<EOScolMaquetteResponsableEc> listeMrec = EOScolMaquetteResponsableEc.fetchAll(edc, qualifier);
		NSArray<EOScolMaquetteEc> listeEc = (NSArray<EOScolMaquetteEc>) listeMrec
				.valueForKey(EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY);
		return ERXArrayUtilities.sortedArraySortedWithKey(listeEc, EOScolMaquetteEc.MEC_CODE_KEY);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @param persId
	 *            Le PERS_ID de l'individu
	 * @param anneeScol
	 *            L'année scolaire
	 * @return La liste des {@link EOScolMaquetteEc} pour lesquels l'individu est enseignant ('E'), et non responsable
	 *         ni secrétaire
	 * @deprecated Utiliser
	 *             {@link FinderScolResponsabilites#getEcByResponsable(EOEditingContext, Integer, Integer, String)}
	 */
	public static NSArray<EOScolMaquetteEc> getEcForEnseignant(EOEditingContext edc, Integer persId, Integer anneeScol) {
		EOQualifier qualifier = ERXQ.and(ERXQ.equals(EOScolMaquetteResponsableEc.FANN_KEY_KEY, anneeScol),
				ERXQ.equals(EOScolMaquetteResponsableEc.PERS_ID_KEY, persId),
				EOScolMaquetteResponsableEc.QUAL_ENSEIGNANT, ERXQ.not(EOScolMaquetteResponsableEc.QUAL_RESPONSABLE),
				ERXQ.not(EOScolMaquetteResponsableEc.QUAL_SECRETAIRE));
		NSArray<EOScolMaquetteResponsableEc> listeMrec = EOScolMaquetteResponsableEc.fetchAll(edc, qualifier);
		NSArray<EOScolMaquetteEc> listeEc = (NSArray<EOScolMaquetteEc>) listeMrec
				.valueForKey(EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY);
		return ERXArrayUtilities.sortedArraySortedWithKey(listeEc, EOScolMaquetteEc.MEC_CODE_KEY);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @param persId
	 *            Le PERS_ID de l'individu
	 * @param anneeScol
	 *            L'année scolaire
	 * @return La liste des {@link EOScolMaquetteEc} pour lesquels l'individu est enseignant ('E'), le responsable ou le
	 *         secrétaire
	 * @deprecated Utiliser
	 *             {@link FinderScolResponsabilites#getEcByResponsable(EOEditingContext, Integer, Integer, String)}
	 */
	public static NSArray<EOScolMaquetteEc> getEcForAllResponsabilites(EOEditingContext edc, Integer persId,
			Integer anneeScol) {
		EOQualifier qualifier = ERXQ.and(ERXQ.equals(EOScolMaquetteResponsableEc.FANN_KEY_KEY, anneeScol),
				ERXQ.equals(EOScolMaquetteResponsableEc.PERS_ID_KEY, persId));
		NSArray<EOScolMaquetteResponsableEc> listeMrec = EOScolMaquetteResponsableEc.fetchAll(edc, qualifier);
		NSArray<EOScolMaquetteEc> listeMec = (NSArray<EOScolMaquetteEc>) listeMrec
				.valueForKey(EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY);
		NSArray<EOScolMaquetteEc> listeEc = ERXArrayUtilities.distinct(listeMec);
		return ERXArrayUtilities.sortedArraySortedWithKey(listeEc, EOScolMaquetteEc.MEC_CODE_KEY);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param anneeScol
	 *            L'année scolaire
	 * @param typeResponsabilite
	 *            Le type de responsabilité
	 * @see {@link FinderScolResponsabilites#TYPE_ENSEIGNANT}, {@link FinderScolResponsabilites#TYPE_RESPONSABLE},
	 *      {@link FinderScolResponsabilites#TYPE_SECRETAIRE}
	 * @return
	 */
	public static NSArray<EOScolMaquetteEc> getEcForResponsabilite(EOEditingContext edc, Integer persId,
			Integer anneeScol, String typeResponsabilite, EOQualifier addQual) {
		EOQualifier qualifier = ERXQ.and(ERXQ.equals(EOScolMaquetteResponsableEc.FANN_KEY_KEY, anneeScol),
				ERXQ.equals(EOScolMaquetteResponsableEc.PERS_ID_KEY, persId), getQualType(typeResponsabilite));
		NSArray<EOScolMaquetteResponsableEc> listeMrec = EOScolMaquetteResponsableEc.fetchAll(edc, qualifier);
		NSArray<EOScolMaquetteEc> listeEc = (NSArray<EOScolMaquetteEc>) listeMrec
				.valueForKey(EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY);
		if (addQual != null) {
			listeEc = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(listeEc, addQual);
		}
		return ERXArrayUtilities.sortedArraySortedWithKey(listeEc, EOScolMaquetteEc.MEC_CODE_KEY);
	}

	private static EOQualifier getQualType(String type) {
		if (FinderScolResponsabilites.TYPE_RESPONSABLE.equals(type)) {
			return EOScolMaquetteResponsableEc.QUAL_RESPONSABLE;
		} else if (FinderScolResponsabilites.TYPE_SECRETAIRE.equals(type)) {
			return EOScolMaquetteResponsableEc.QUAL_SECRETAIRE;
		} else if (FinderScolResponsabilites.TYPE_ENSEIGNANT.equals(type)) {
			return EOScolMaquetteResponsableEc.QUAL_ENSEIGNANT;
		} else {
			return null;
		}
	}
}
