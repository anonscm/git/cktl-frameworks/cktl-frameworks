/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderScolFormationFiliere extends Finder {

	/**
	 * Recherche d'une filiere precise<BR>
	 * 
	 * @param ec
	 * @param ffilCode
	 * @param ffilLibelle
	 * 
	 * @return un EOScolFormationFiliere
	 */
	public static EOScolFormationFiliere getScolFormationFiliere(EOEditingContext ec, String ffilCode, String ffilLibelle) {
		EOScolFormationFiliere scolFormationFiliere = null;
		if (ffilCode != null || ffilLibelle != null) {
			NSMutableArray qualifiers = new NSMutableArray();
			if (ffilCode != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationFiliere.FFIL_CODE_KEY, EOQualifier.QualifierOperatorEqual, ffilCode));
			}
			if (ffilLibelle != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationFiliere.FFIL_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
						ffilLibelle));
			}
			scolFormationFiliere = EOScolFormationFiliere.fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		return scolFormationFiliere;
	}

	/**
	 * Recherche des filieres par un filtre : where ffilCode = filtre or ffilLibelle caseInsensitiveLike *filtre*
	 * Convention : si l'on veut tout obtenir le filtre sera egal à "*"
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationFiliere, sinon NULL
	 */
	public static NSArray getScolFormationFilieresFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationFilieres = null;
		if (!StringCtrl.isEmpty(filtre)) {
			EOSortOrdering codeOrdering = EOSortOrdering.sortOrderingWithKey(EOScolFormationFiliere.FFIL_CODE_KEY, EOSortOrdering.CompareAscending);
			NSArray sortOrderings = new NSArray(new EOSortOrdering[] { codeOrdering });

			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOScolFormationFiliere.FFIL_CODE_KEY, EOQualifier.QualifierOperatorEqual, filtre);
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EOScolFormationFiliere.FFIL_LIBELLE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtre + "*");
			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
			EOOrQualifier qualifier = new EOOrQualifier(qualifiers);

			scolFormationFilieres = EOScolFormationFiliere.fetchAll(ec, qualifier, sortOrderings);
		}
		return scolFormationFilieres;
	}

	/**
	 * Recherche des filieres valides par un filtre : where ffilCode = filtre or ffilLibelle caseInsensitiveLike
	 * *filtre* Convention : si l'on veut tout obtenir le filtre sera egal à "*"
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationFiliere, sinon NULL
	 */
	public static NSArray getScolFormationFilieresValidesFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationFilieres = getScolFormationFilieresFiltre(ec, filtre);

		EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationFiliere.FFIL_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual,
				EOScolFormationFiliere.FILIERE_VALIDE);
		scolFormationFilieres = EOQualifier.filteredArrayWithQualifier(scolFormationFilieres, qualifier);

		return scolFormationFilieres;
	}

}
