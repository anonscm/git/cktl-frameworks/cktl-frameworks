/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteResponsablePar;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public abstract class FinderScolMaquetteParcours {

	public static NSArray<EOScolMaquetteSemestre> getSemestresForParcoursAndYear(EOEditingContext ec,
			EOScolMaquetteParcours parcours, EOScolFormationAnnee number) {
		return getSemestresForParcoursAndYear(ec, parcours, number, null);
	}

	/**
	 * @param ec
	 *            Un EditingContext
	 * @param parcours
	 *            Un {@link EOScolMaquetteParcours}
	 * @param annee
	 *            Une {@link EOScolFormationAnnee}
	 * @param addQual
	 *            Un qualifier supplémentaire
	 * @return La liste des semestres pour un parcours et une année donnés
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOScolMaquetteSemestre> getSemestresForParcoursAndYear(EOEditingContext ec,
			EOScolMaquetteParcours parcours, EOScolFormationAnnee annee, EOQualifier addQual) {

		if ((ec == null) || (parcours == null) || (annee == null)) {
			return null;
		}

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
				+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY + " = %@";
		tbKeys.add(parcours);

		conditionStr += " AND " + EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
				+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(annee);
		// CktlLog.log(" - "+conditionStr);

		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(conditionStr, tbKeys);
		EOQualifier qual = null;
		if (addQual != null) {
			qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {qualIni, addQual}));
		} else {
			qual = qualIni;
		}

		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolMaquetteSemestre.ENTITY_NAME, qual,
				new NSArray<EOSortOrdering>(new EOSortOrdering(EOScolMaquetteSemestre.MSEM_ORDRE_KEY,
						EOSortOrdering.CompareAscending)));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		return ec.objectsWithFetchSpecification(fetchSpec);

	}

	/**
	 * @param mec
	 *            Un {@link EOScolMaquetteEc}
	 * @return Liste des parcours où se trouve un EC donné
	 */
	public static NSArray<EOScolMaquetteParcours> getParcoursForEc(EOScolMaquetteEc mec) {
		String qualStr = EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
				+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY + "."
				+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY + "."
				+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY + "."
				+ EOScolMaquetteUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY + "."
				+ EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY;
		return EOScolMaquetteParcours.fetchAll(mec.editingContext(), ERXQ.equals(qualStr, mec));
	}

	/**
	 * @param mpar
	 *            Un {@link EOScolMaquetteParcours}
	 * @param type
	 *            Le type de responsabilité
	 * @param fannKey
	 *            Une année
	 * @return La liste des {@link EOScolMaquetteResponsablePar} d'un parcours et du type donné
	 */
	public static NSArray<EOScolMaquetteResponsablePar> getResponsablesParcours(EOScolMaquetteParcours mpar,
			String type, Integer fannKey) {
		EOQualifier qualMpar = ERXQ.equals(EOScolMaquetteResponsablePar.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY,
				mpar);
		EOQualifier qualFann = ERXQ.equals(EOScolMaquetteResponsablePar.FANN_KEY_KEY, fannKey);
		EOQualifier qual = ERXQ.and(qualMpar, getQualType(type), qualFann);
		return EOScolMaquetteResponsablePar.fetchAll(mpar.editingContext(), qual);
	}

	/**
	 * @param ie
	 *            Une {@link EOScolInscriptionEtudiant}
	 * @return La liste des parcours associés à une inscription
	 */
	public static NSArray<EOScolMaquetteParcours> getParcoursForInscriptionEtudiant(EOScolInscriptionEtudiant ie) {
		String qualStr = EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
				+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_INSCRIPTION_SEMESTRES_KEY + "."
				+ EOScolInscriptionSemestre.TO_SCOL_INSCRIPTION_ETUDIANT_KEY;
		EOQualifier qual = ERXQ.equals(qualStr, ie);
		return EOScolMaquetteParcours.fetchAll(ie.editingContext(), qual);
	}

	/**
	 * @param edc
	 *            Un EditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param typeResponsabilite
	 *            Le type de responsabilité
	 * @param fannKey
	 *            L'annéd
	 * @return La liste des parcours pour lesquels l'individu a une responsabilité du type donné
	 */
	public static NSArray<EOScolMaquetteParcours> getParcoursForResponsable(EOEditingContext edc, Integer persId,
			String typeResponsabilite, Integer fannKey) {
		EOQualifier qualPersId = ERXQ.equals(EOScolMaquetteResponsablePar.TO_FWKPERS__INDIVIDU_KEY + "."
				+ IPersonne.PERSID_KEY, persId);
		EOQualifier qualFannKey = ERXQ.equals(EOScolMaquetteResponsablePar.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ "." + EOScolFormationAnnee.FANN_KEY_KEY, fannKey);

		EOQualifier qual = ERXQ.and(qualPersId, getQualType(typeResponsabilite), qualFannKey);

		NSArray<EOScolMaquetteResponsablePar> resp = EOScolMaquetteResponsablePar.fetchAll(edc, qual);
		NSMutableArray<EOScolMaquetteParcours> listeParcours = new NSMutableArray<EOScolMaquetteParcours>();
		for (EOScolMaquetteResponsablePar responsablePar : resp) {
			listeParcours.addObject(responsablePar.toFwkScolarite_ScolMaquetteParcours());
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeParcours));
	}

	private static EOQualifier getQualType(String type) {
		if (FinderScolResponsabilites.TYPE_RESPONSABLE.equals(type)) {
			return EOScolMaquetteResponsablePar.QUAL_RESPONSABLE;
		} else if (FinderScolResponsabilites.TYPE_SECRETAIRE.equals(type)) {
			return EOScolMaquetteResponsablePar.QUAL_SECRETAIRE;
		} else if (FinderScolResponsabilites.TYPE_ENSEIGNANT.equals(type)) {
			return EOScolMaquetteResponsablePar.QUAL_ENSEIGNANT;
		} else {
			return null;
		}
	}
}
