/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderScolFormationGrade extends Finder {

	/**
	 * Recherche d'un grade precis<BR>
	 * 
	 * @param ec
	 * @param fgraCode
	 * @param fgraLibelle
	 * 
	 * @return un EOScolFormationGrade
	 */
	public static EOScolFormationGrade getScolFormationGrade(EOEditingContext ec, String fgraCode, String fgraLibelle) {
		EOScolFormationGrade scolFormationGrade = null;
		if (fgraCode != null || fgraLibelle != null) {
			NSMutableArray qualifiers = new NSMutableArray();
			if (fgraCode != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationGrade.FGRA_CODE_KEY, EOQualifier.QualifierOperatorEqual, fgraCode));
			}
			if (fgraLibelle != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationGrade.FGRA_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
						fgraLibelle));
			}
			scolFormationGrade = EOScolFormationGrade.fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		return scolFormationGrade;
	}

	/**
	 * Recherche des grades par un filtre : where fgraCode = filtre or fgraLibelle caseInsensitiveLike *filtre*
	 * Convention : si l'on veut tout obtenir le filtre sera egal à "*"
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationGrade, sinon NULL
	 */
	public static NSArray getScolFormationGradesFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationGrades = null;
		if (!StringCtrl.isEmpty(filtre)) {
			EOSortOrdering codeOrdering = EOSortOrdering.sortOrderingWithKey(EOScolFormationGrade.FGRA_CODAGE_KEY, EOSortOrdering.CompareAscending);
			NSArray sortOrderings = new NSArray(new EOSortOrdering[] { codeOrdering });

			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOScolFormationGrade.FGRA_CODE_KEY, EOQualifier.QualifierOperatorEqual, filtre);
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EOScolFormationGrade.FGRA_LIBELLE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtre + "*");
			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
			EOOrQualifier qualifier = new EOOrQualifier(qualifiers);

			scolFormationGrades = EOScolFormationGrade.fetchAll(ec, qualifier, sortOrderings);
		}
		return scolFormationGrades;
	}

	/**
	 * Recherche des grades valides par un filtre : where fgraCode = filtre or fgraLibelle caseInsensitiveLike *filtre*
	 * Convention : si l'on veut tout obtenir le filtre sera egal à "*"
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationGrade, sinon NULL
	 */
	public static NSArray getScolFormationGradesValidesFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationGrades = getScolFormationGradesFiltre(ec, filtre);

		EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationGrade.FGRA_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual,
				EOScolFormationGrade.GRADE_VALIDE);
		scolFormationGrades = EOQualifier.filteredArrayWithQualifier(scolFormationGrades, qualifier);

		return scolFormationGrades;
	}

	/**
	 * Recherche la liste des grades contenant des diplomes, pour un domaine et une annee
	 * 
	 * @param ec
	 * @param domaine
	 * @param number
	 * @return
	 */

	public static NSArray<EOScolFormationGrade> getGradesForDomaineAndYear(EOEditingContext ec, EOScolFormationDomaine domaine, EOScolFormationAnnee number) {
		return getGradesForDomaineAndYear(ec, domaine, number, null);
	}

	public static NSArray<EOScolFormationGrade> getGradesForDomaineAndYear(EOEditingContext ec, EOScolFormationDomaine domaine, EOScolFormationAnnee number,
			EOQualifier addQual) {

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolFormationGrade.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOMES_KEY + "."
				+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY + " = %@";
		tbKeys.add(domaine);
		conditionStr += " AND " + EOScolFormationGrade.FGRA_VALIDITE_KEY + " = %s";
		tbKeys.add("O");

		conditionStr += " AND " + EOScolFormationGrade.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOMES_KEY + "."
				+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY + "."
				+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(number);
		// CktlLog.log(" - "+conditionStr);

		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(conditionStr, tbKeys);
		EOQualifier qual = null;
		if (addQual != null)
			qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qualIni, addQual }));
		else
			qual = qualIni;

		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationGrade.ENTITY_NAME, qual, new NSArray(new Object[] { new EOSortOrdering(
				EOScolFormationGrade.FGRA_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		NSMutableArray<EOScolFormationGrade> retour = new NSMutableArray<EOScolFormationGrade>();
		NSArray<EOScolFormationGrade> tmp = ec.objectsWithFetchSpecification(fetchSpec);
		for (EOScolFormationGrade grdOccur : tmp) {
			if (grdOccur.toFwkScolarite_ScolFormationDiplomes().size() > 0)
				retour.addObject(grdOccur);
		}
		return retour;
	}

}
