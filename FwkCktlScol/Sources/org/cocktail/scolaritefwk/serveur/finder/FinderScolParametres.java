/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.exception.ParametreException;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolParametres;
import org.cocktail.scolarix.serveur.metier.eos.EOGarnucheParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public final class FinderScolParametres extends Finder {

	/**
	 * Recherche la valeur par defaut pour un type STRING.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return la valeur par defaut (String)
	 */
	public static final String getDefaultString(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_DEFAULT_STRING);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolDefaultStringManquant);
		}
		return parametre;
	}

	/**
	 * Recherche la valeur par defaut pour un type NUMBER.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return la valeur par defaut (Integer)
	 */
	public static final Integer getDefaultNumber(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_DEFAULT_NUMBER);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolDefaultNumberManquant);
		}
		try {
			Integer valeurDefaut = new Integer(parametre);
			return valeurDefaut;
		} catch (NumberFormatException e) {
			throw new ParametreException(ParametreException.scolDefaultNumberIncoherent);
		}
	}

	/**
	 * Recherche si on est en annee civile ou non.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout
	 * @return boolean true si en annee civile, false si en annee universitaire
	 */
	public static final boolean getAnneeCivile(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_CONSTANTE_ANNEE_CIVILE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolConstanteAnneeCivileManquant);
		}
		return StringCtrl.toBool(parametre);
	}

	/**
	 * Recherche l'annee d'exploitation du USER SCOLARITE.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return l'annee d'exploitation (Integer)
	 */
	public static final Integer getAnneeExploitation(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_ANNEE_EXPLOITATION);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolAnneeExploitationManquant);
		}
		try {
			Integer anneeExploitation = new Integer(parametre);
			return anneeExploitation;
		} catch (NumberFormatException e) {
			throw new ParametreException(ParametreException.scolAnneeExploitationIncoherent);
		}
	}

	/**
	 * Recherche le pays d'exploitation par defaut.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return le pays d'exploitation (String)
	 */
	public static final String getConstantePays(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_CONSTANTE_PAYS);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolConstantePaysManquant);
		}
		return parametre;
	}

	/**
	 * Recherche la langue d'exploitation par defaut.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return la langue d'exploitation (String)
	 */
	public static final String getConstanteLangue(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_CONSTANTE_LANGUE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolConstanteLangueManquant);
		}
		return parametre;
	}

	/**
	 * Recherche le semestre maximal d'enseignement de l'etablissement.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return le semestre maximal (Integer)
	 */
	public static final Integer getConstanteSemestre(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_CONSTANTE_SEMESTRE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolConstanteSemestreManquant);
		}
		try {
			Integer anneeExploitation = new Integer(parametre);
			return anneeExploitation;
		} catch (NumberFormatException e) {
			throw new ParametreException(ParametreException.scolConstanteSemestreIncoherent);
		}
	}

	/**
	 * Recherche le separateur pour la construction d'un code E.C./U.E..
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return le separateur (String)
	 */
	public static final String getMaquetteSeparateur(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_MAQUETTE_SEPARATEUR);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolMaquetteSeparateurManquant);
		}
		return parametre;
	}

	/**
	 * Recherche le libelle de l'etablissement d'exploitation.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return le libelle de l'etablissement (String)
	 */
	public static final String getUniversiteLibelle(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_UNIVERSITE_LIBELLE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolUniversiteLibelleManquant);
		}
		return parametre;
	}

	/**
	 * Recherche la ville de l'etablissement d'exploitation.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return la ville de l'etablissement (String)
	 */
	public static final String getUniversiteVille(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_UNIVERSITE_VILLE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolUniversiteVilleManquant);
		}
		return parametre;
	}

	/**
	 * Recherche l'identite du responsable de l'etablissement d'exploitation.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return l'identite du responsable de l'etablissement (String)
	 */
	public static final String getUniversiteIdentite(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_UNIVERSITE_IDENTITE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolUniversiteIdentiteManquant);
		}
		return parametre;
	}

	/**
	 * Recherche le titre du responsable de l'etablissement d'exploitation.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return le titre du responsable de l'etablissement (String)
	 */
	public static final String getUniversiteTitre(EOEditingContext ec) {
		String parametre = getParam(ec, EOScolParametres.SCOL_UNIVERSITE_TITRE);
		if (parametre == null) {
			throw new ParametreException(ParametreException.scolUniversiteTitreManquant);
		}
		return parametre;
	}

	/**
	 * Recherche un parametre de type STRING.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return la valeur du parametre (String)
	 */
	public static final String getStringParametre(EOEditingContext ec, String key) {
		String parametre = null;
		parametre = getScolParam(ec, key);
		if (parametre == null) {
			parametre = getDefaultString(ec);
		}
		return parametre;
	}

	/**
	 * Recherche un parametre de type NUMBER.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans une meme table ou s'il n'est pas defini du tout ou
	 *             s'il n'est pas correct
	 * @return la valeur du parametre (Integer)
	 */
	public static final Integer getNumberParametre(EOEditingContext ec, String key) {
		String parametre = null;
		parametre = getScolParam(ec, key);
		if (parametre == null) {
			return getDefaultNumber(ec);
		}
		try {
			Integer numberValue = new Integer(parametre);
			return numberValue;
		} catch (NumberFormatException e) {
			throw new ParametreException(ParametreException.scolDefaultNumberIncoherent);
		}
	}

	/**
	 * Recherche d'un parametre par sa cle, toutes tables de parametres confondues<BR>
	 * L'ordre de priorite de la recherche est important.<BR>
	 * Ici SCOLARITE.SCOL_PARAMETRES, GARNUCHE.GARNUCHE_PARAMETRES, GRHUM.GRHUM_PARAMETRES. Si non trouve, retourne
	 * null.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un String qui va bien, sinon null
	 * @throws ParametreException
	 *             Si la cle est trouvee plusieurs fois dans une meme table
	 */
	public static final String getParam(EOEditingContext ec, String key) {
		String parametre = null;
		parametre = getScolParam(ec, key);
		if (parametre == null) {
			parametre = getGarnucheParam(ec, key);
			if (parametre == null) {
				parametre = getGrhumParam(ec, key);
				if (parametre == null) {
					parametre = getGrhumParam(ec, key);
				}
			}
		}
		return parametre;
	}

	/**
	 * Recherche d'un parametre OBLIGATOIRE par sa cle, toutes tables de parametres confondues<BR>
	 * L'ordre de priorite de la recherche est important.<BR>
	 * Ici SCOLARITE.SCOL_PARAMETRES, GARNUCHE.GARNUCHE_PARAMETRES, GRHUM.GRHUM_PARAMETRES. Si non trouve, Exception !
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un String qui va bien, sinon une exception si non trouve
	 * @throws ParametreException
	 *             Si la cle est trouvee plusieurs fois dans une meme table ou si aucune cle n'est trouvee
	 * @see FinderScolParametres#getParam(EOEditingContext, String)
	 */
	public static final String getRequiredParam(EOEditingContext ec, String key) {
		String parametre = getParam(ec, key);
		if (parametre == null) {
			throw new ParametreException("Il faut definir le parametre " + key);
		}
		return parametre;
	}

	/**
	 * Recherche de parametres par la cle, toutes tables de parametres confondues<BR>
	 * L'ordre de priorite de la recherche est important.<BR>
	 * Ici SCOLARITE.SCOL_PARAMETRES, GARNUCHE.GARNUCHE_PARAMETRES, GRHUM.GRHUM_PARAMETRES.
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un tableau de String par ordre de recherche dans les tables
	 */
	public static final NSArray getParams(EOEditingContext ec, String key) {
		NSMutableArray params = new NSMutableArray();
		params.addObjectsFromArray(getScolParams(ec, key));
		params.addObjectsFromArray(getGarnucheParams(ec, key));
		params.addObjectsFromArray(getGrhumParams(ec, key));
		return params;
	}

	/**
	 * Recherche d'un parametre par sa cle dans ScolParametres<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans la table
	 * @return un String
	 */
	private static final String getScolParam(EOEditingContext ec, String key) {
		NSArray params = getScolParams(ec, key);
		if (params == null || params.count() == 0) {
			return null;
		}
		if (params.count() > 1) {
			throw new ParametreException("Le parametre " + key + " est defini plusieurs fois dans la table de parametres ("
					+ EOScolParametres.ENTITY_TABLE_NAME + "), il ne devrait y etre qu'une seule fois");
		}
		return (String) params.lastObject();
	}

	/**
	 * Recherche la liste des parametres correspondants a une cle dans ScolParametres <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un NSArray de String
	 */
	private static final NSArray getScolParams(EOEditingContext ec, String key) {
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOScolParametres.PARAM_KEY_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, key);
		NSArray params = EOScolParametres.fetchAll(ec, qualifier);
		if (params == null || params.count() == 0) {
			return null;
		}
		return (NSArray) params.valueForKey(EOScolParametres.PARAM_VALUE_KEY);
	}

	/**
	 * Recherche d'un parametre par sa cle dans GarnucheParametres<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans la table
	 * @return un String
	 */
	private static final String getGarnucheParam(EOEditingContext ec, String key) {
		NSArray params = getGarnucheParams(ec, key);
		if (params == null || params.count() == 0) {
			return null;
		}
		if (params.count() > 1) {
			throw new ParametreException("Le parametre " + key + " est defini plusieurs fois dans la table de parametres ("
					+ EOGarnucheParametres.ENTITY_TABLE_NAME + "), il ne devrait y etre qu'une seule fois");
		}
		return (String) params.lastObject();
	}

	/**
	 * Recherche la liste des parametres correspondants a une cle dans GarnucheParametres <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un NSArray de String
	 */
	private static final NSArray getGarnucheParams(EOEditingContext ec, String key) {
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOGarnucheParametres.PARAM_KEY_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, key);
		NSArray params = EOGarnucheParametres.fetchAll(ec, qualifier);
		if (params == null || params.count() == 0) {
			return null;
		}
		return (NSArray) params.valueForKey(EOGarnucheParametres.PARAM_VALUE_KEY);
	}

	/**
	 * Recherche d'un parametre par sa cle dans GrhumParametres<BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @throws ParametreException
	 *             Si le parametre est defini plusieurs fois dans la table
	 * @return un String
	 */
	private static final String getGrhumParam(EOEditingContext ec, String key) {
		NSArray params = getGrhumParams(ec, key);
		if (params == null || params.count() == 0) {
			return null;
		}
		if (params.count() > 1) {
			throw new ParametreException("Le parametre " + key + " est defini plusieurs fois dans la table de parametres ("
					+ EOGrhumParametres.ENTITY_TABLE_NAME + "), il ne devrait y etre qu'une seule fois");
		}
		return (String) params.lastObject();
	}

	/**
	 * Recherche la liste des parametres correspondants a une cle dans GrhumParametres <BR>
	 * 
	 * @param ec
	 *            editingContext dans lequel se fait le fetch
	 * @param key
	 *            Cle du parametre
	 * @return un NSArray de String
	 */
	private static final NSArray getGrhumParams(EOEditingContext ec, String key) {
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOGrhumParametres.PARAM_KEY_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, key);
		NSArray params = EOGrhumParametres.fetchAll(ec, qualifier, null);
		if (params == null || params.count() == 0) {
			return null;
		}
		return (NSArray) params.valueForKey(EOGrhumParametres.PARAM_VALUE_KEY);
	}

}
