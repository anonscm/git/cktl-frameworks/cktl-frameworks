/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteResponsableUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public abstract class FinderScolMaquetteUe {

	/**
	 * @param ec
	 *            Un EditingContext
	 * @param ue
	 *            Une {@link EOScolMaquetteUe}
	 * @param annee
	 *            Une {@link EOScolFormationAnnee}
	 * @return La liste des EC liés à une EU et une année données
	 */
	public static NSArray<EOScolMaquetteEc> getEcForUeAndYear(EOEditingContext ec, EOScolMaquetteUe ue,
			EOScolFormationAnnee number) {
		return getEcForUeAndYear(ec, ue, number, null);
	}

	/**
	 * @param ec
	 *            Un EditingContext
	 * @param ue
	 *            Une {@link EOScolMaquetteUe}
	 * @param annee
	 *            Une {@link EOScolFormationAnnee}
	 * @param addQual
	 *            Un qualifier
	 * @return La liste des EC liés à une EU et une année données
	 */
	public static NSArray<EOScolMaquetteEc> getEcForUeAndYear(EOEditingContext ec, EOScolMaquetteUe ue,
			EOScolFormationAnnee annee, EOQualifier addQual) {

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY + "."
				+ EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY + " = %@";
		tbKeys.add(ue);

		conditionStr += " AND " + EOScolMaquetteEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(annee);
		// CktlLog.log(" - "+conditionStr);
		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(conditionStr, tbKeys);
		EOQualifier qual = null;
		if (addQual != null) {
			qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {qualIni, addQual}));
		} else {
			qual = qualIni;
		}
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolMaquetteEc.ENTITY_NAME, qual, new NSArray(
				new Object[] {new EOSortOrdering(EOScolMaquetteEc.MEC_LIBELLE_KEY, EOSortOrdering.CompareAscending)}));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * @param edc
	 *            Un EditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @param typeResponsabilite
	 *            Le type de responsabilité
	 * @return La liste des UE dont l'individu est responsable
	 */
	public static NSArray<EOScolMaquetteUe> getUeForResponsable(EOEditingContext edc, Integer persId, Integer fannKey,
			String typeResponsabilite) {
		EOQualifier qualPersId = ERXQ.equals(EOScolMaquetteResponsableUe.TO_FWKPERS__INDIVIDU_KEY + "."
				+ IPersonne.PERSID_KEY, persId);
		EOQualifier qualFannKey = ERXQ.equals(EOScolMaquetteResponsableUe.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY
				+ "." + EOScolFormationAnnee.FANN_KEY_KEY, fannKey);

		EOQualifier qual = ERXQ.and(qualPersId, getQualType(typeResponsabilite), qualFannKey);

		NSArray<EOScolMaquetteResponsableUe> resp = EOScolMaquetteResponsableUe.fetchAll(edc, qual);
		NSMutableArray<EOScolMaquetteUe> listeUe = new NSMutableArray<EOScolMaquetteUe>();
		for (EOScolMaquetteResponsableUe responsableUe : resp) {
			listeUe.addObject(responsableUe.toFwkScolarite_ScolMaquetteUe());
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeUe));
	}

	private static EOQualifier getQualType(String type) {
		if (FinderScolResponsabilites.TYPE_RESPONSABLE.equals(type)) {
			return EOScolMaquetteResponsableUe.QUAL_RESPONSABLE;
		} else if (FinderScolResponsabilites.TYPE_SECRETAIRE.equals(type)) {
			return EOScolMaquetteResponsableUe.QUAL_SECRETAIRE;
		} else if (FinderScolResponsabilites.TYPE_ENSEIGNANT.equals(type)) {
			return EOScolMaquetteResponsableUe.QUAL_ENSEIGNANT;
		} else {
			return null;
		}
	}
}
