/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * @author Julien BLANDINEAU <jblandin@univ-lr.fr>
 * 
 */
public class FinderScolFormationResponsabilite extends Finder {
	/**
	 * @param specialisation
	 * @param anneeScol
	 * @return La liste des responsabilité pour une spécialisation
	 */
	public static NSArray<EOScolFormationResponsabilite> fetchBySpecialisation(EOScolFormationSpecialisation specialisation, Integer anneeScol) {
		return fetchBySpecialisationAndType(specialisation, anneeScol, null);
	}

	/**
	 * @param specialisation
	 * @param anneeScol
	 * @param typeResponsabilite
	 * @return La liste des responsabilités, filtrée selon le type, pour une
	 *         spécialisation
	 */
	public static NSArray<EOScolFormationResponsabilite> fetchBySpecialisationAndType(EOScolFormationSpecialisation specialisation, Integer anneeScol, String typeResponsabilite) {
		EOQualifier qualSpecialisation = ERXQ.equals(EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY, specialisation);
		EOQualifier qualAnnee = ERXQ.equals(EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "." + EOScolFormationAnnee.FANN_KEY_KEY, anneeScol);
		EOQualifier qualType = getQualType(typeResponsabilite);
		EOQualifier qualifier = ERXQ.and(qualSpecialisation, qualAnnee, qualType);
		return EOScolFormationResponsabilite.fetchAll(specialisation.editingContext(), qualifier);
	}
	
	/**
	 * @param edc L'EditingContext
	 * @param persId Le <code>PERS_ID</code> du responsable
	 * @param fannKey L'année
	 * @param type Le type de responsabilité
	 * @return La liste des {@link EOScolFormationSpecialisation} dont l'utilisateur est responsable;
	 */
	public static NSArray<EOScolFormationSpecialisation> getSpecialisationsByResponsable(EOEditingContext edc, Integer persId, Integer fannKey, String type) {
		EOQualifier qualPersId = ERXQ.equals(EOScolFormationResponsabilite.PERS_ID_KEY, persId);
		EOQualifier qualType = getQualType(type);
		EOQualifier qualAnnee = ERXQ.equals(
				EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "." + 
				EOScolFormationAnnee.FANN_KEY_KEY, fannKey);
		EOQualifier qual = ERXQ.and(qualPersId, qualType, qualAnnee);
		NSArray<EOScolFormationResponsabilite> responsabilites = EOScolFormationResponsabilite.fetchAll(edc, qual);
		NSMutableArray<EOScolFormationSpecialisation> listeSpe = new NSMutableArray<EOScolFormationSpecialisation>();
		for (EOScolFormationResponsabilite responsabilite : responsabilites) {
			listeSpe.addObject(responsabilite.toFwkScolarite_ScolFormationSpecialisation());
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(listeSpe);
	}
		
	private static EOQualifier getQualType(String type) {
		if (FinderScolResponsabilites.TYPE_RESPONSABLE.equals(type)) {
			return EOScolFormationResponsabilite.QUAL_RESPONSABLE;
		} else if (FinderScolResponsabilites.TYPE_SECRETAIRE.equals(type)) {
			return EOScolFormationResponsabilite.QUAL_SECRETAIRE;			
		} else if (FinderScolResponsabilites.TYPE_ENSEIGNANT.equals(type)) {
			return EOScolFormationResponsabilite.QUAL_ENSEIGNANT;
		} else {
			return null;
		}
	}
}
