/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationResponsabilite;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteResponsableEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteResponsablePar;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteResponsableUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FinderScolResponsabilites extends Finder {

	public static final String TYPE_RESPONSABLE = "R";
	public static final String TYPE_SECRETAIRE = "S";
	public static final String TYPE_ENSEIGNANT = "E";

	private static FinderScolResponsabilites instance;

	protected FinderScolResponsabilites() {

	}

	public static FinderScolResponsabilites getInstance() {
		if (instance == null) {
			instance = new FinderScolResponsabilites();
		}
		return instance;
	}

	/**
	 * @param etudiant
	 *            Un {@link IEtudiant}
	 * @param mec
	 *            Un {@link EOScolMaquetteEc}
	 * @param fannKey
	 *            L'année
	 * @param typeResponsabilite
	 *            Le type de responsabilité
	 * @return La liste des responsables
	 */
	public NSArray<IPersonne> getResponsablesByEtudiantAndEc(IEtudiant etudiant, EOScolMaquetteEc mec, Integer fannKey,
			String typeResponsabilite) {
		NSMutableArray<IPersonne> listeResp = new NSMutableArray<IPersonne>();

		if (mec != null) {
			listeResp.addObjectsFromArray(getResponsablesEcByEc(mec, typeResponsabilite));
			listeResp.addObjectsFromArray(getResponsablesUeByEc(mec, typeResponsabilite));
		}
		if (etudiant != null) {
			listeResp.addObjectsFromArray(getResponsablesParcoursByEtudiant(etudiant, fannKey, typeResponsabilite));
			listeResp.addObjectsFromArray(getResponsablesFormationByEtudiant(etudiant, fannKey, typeResponsabilite));
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(listeResp);
	}

	/**
	 * @param edc
	 *            L'EditingContext
	 * @param persId
	 *            Le <code>PERS_ID</code> de l'individu
	 * @param fannKey
	 *            L'année
	 * @param typeResponsabilite
	 *            Le type de responsabilité
	 * @return La liste des EC dont l'individu est responsable
	 * @see {@link FinderScolResponsabilites#TYPE_RESPONSABLE}, {@link FinderScolResponsabilites#TYPE_SECRETAIRE},
	 *      {@link FinderScolResponsabilites#TYPE_ENSEIGNANT}
	 */
	public NSArray<EOScolMaquetteEc> getEcByResponsable(EOEditingContext edc, Integer persId, Integer fannKey,
			String typeResponsabilite) {
		NSMutableArray<EOScolMaquetteEc> listeEc = new NSMutableArray<EOScolMaquetteEc>();

		listeEc.addObjectsFromArray(FinderScolMaquetteResponsableEc.getEcForResponsabilite(edc, persId, fannKey,
				typeResponsabilite, FinderScolMaquetteEc.QUAL_EC_STAGE));
		listeEc.addObjectsFromArray(getEcByResponsableUe(edc, persId, fannKey, typeResponsabilite));
		listeEc.addObjectsFromArray(getEcByResponsableParcours(edc, persId, fannKey, typeResponsabilite));
		listeEc.addObjectsFromArray(getEcByResponsableFormation(edc, persId, fannKey, typeResponsabilite));

		return ERXArrayUtilities.sortedArraySortedWithKey(ERXArrayUtilities.arrayWithoutDuplicates(listeEc),
				EOScolMaquetteEc.MEC_CODE_KEY);
	}

	/**
	 * @param mec
	 * @param typeResponsabilite
	 * @return La liste des responsables d'un EC donné
	 */
	private NSArray<IPersonne> getResponsablesEcByEc(EOScolMaquetteEc mec, String typeResponsabilite) {
		NSMutableArray<IPersonne> listeResp = new NSMutableArray<IPersonne>();
		if (mec != null) {
			EOQualifier qualType = null;
			if (TYPE_RESPONSABLE.equals(typeResponsabilite)) {
				qualType = EOScolMaquetteResponsableEc.QUAL_RESPONSABLE;
			} else if (TYPE_SECRETAIRE.equals(typeResponsabilite)) {
				qualType = EOScolMaquetteResponsableEc.QUAL_SECRETAIRE;
			}
			NSArray<EOScolMaquetteResponsableEc> arrResp = FinderScolMaquetteEc.getEquipeByTypeForEc(mec, qualType);
			for (EOScolMaquetteResponsableEc responsableEc : arrResp) {
				listeResp.addObject(responsableEc.toPersonne());
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeResp));
	}

	/**
	 * @param mec
	 * @param typeResponsabilite
	 * @return La liste des responsables des UE associées à l'EC donnée
	 */
	private NSArray<IPersonne> getResponsablesUeByEc(EOScolMaquetteEc mec, String typeResponsabilite) {
		NSMutableArray<IPersonne> listeResp = new NSMutableArray<IPersonne>();

		NSArray<EOScolMaquetteRepartitionEc> arrMrec = mec.toFwkScolarite_ScolMaquetteRepartitionEcs();
		for (EOScolMaquetteRepartitionEc mrec : arrMrec) {
			EOScolMaquetteUe ue = mrec.toFwkScolarite_ScolMaquetteUe();
			EOQualifier qual = ERXQ.equals(EOScolMaquetteResponsableUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY, ue);
			if (TYPE_RESPONSABLE.equals(typeResponsabilite) || TYPE_SECRETAIRE.equals(typeResponsabilite)) {
				qual = ERXQ.and(qual, ERXQ.equals(EOScolMaquetteResponsableUe.MBUE_TYPE_KEY, typeResponsabilite));
			}

			NSArray<EOScolMaquetteResponsableUe> arrResp = EOScolMaquetteResponsableUe.fetchAll(mec.editingContext(),
					qual);
			for (EOScolMaquetteResponsableUe responsableUe : arrResp) {
				listeResp.addObject(responsableUe.toPersonne());
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeResp));
	}

	/**
	 * @param etudiant
	 * @param fannKey
	 * @return
	 */
	private NSArray<EOScolFormationSpecialisation> getListeScolFormationSpecialisationByEtudiant(IEtudiant etudiant,
			Integer fannKey) {
		NSMutableArray<EOScolFormationSpecialisation> listeFspn = new NSMutableArray<EOScolFormationSpecialisation>();
		EOHistorique histo = etudiant.historique(fannKey);

		if (histo != null) {
			NSArray<EOInscDipl> arrInscDipls = histo.toInscDipls();
			for (EOInscDipl inscDipl : arrInscDipls) {
				listeFspn.addObject(inscDipl.toFwkScolarite_ScolFormationSpecialisation());
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(listeFspn);
	}

	private NSArray<EOScolMaquetteParcours> getListeScolMaquetteParcoursByEtudiant(IEtudiant etudiant, Integer fannKey) {
		NSMutableArray<EOScolMaquetteParcours> listeMpar = new NSMutableArray<EOScolMaquetteParcours>();
		EOHistorique histo = etudiant.historique(fannKey);

		if (histo != null) {
			NSArray<EOInscDipl> arrInscDipls = histo.toInscDipls();
			NSMutableArray<EOScolInscriptionEtudiant> listeInscriptionsEtudiant = new NSMutableArray<EOScolInscriptionEtudiant>();
			for (EOInscDipl inscDipl : arrInscDipls) {
				EOQualifier qualIDipl = ERXQ.equals(EOScolInscriptionEtudiant.IDIPL_NUMERO_KEY, inscDipl.idiplNumero());
				EOQualifier qualFannKey = ERXQ.equals(EOScolInscriptionEtudiant.FANN_KEY_KEY, fannKey);
				EOQualifier qual = ERXQ.and(qualIDipl, qualFannKey);
				listeInscriptionsEtudiant.addObjectsFromArray(EOScolInscriptionEtudiant.fetchAll(etudiant.edc(), qual));
			}
			for (EOScolInscriptionEtudiant ie : listeInscriptionsEtudiant) {
				listeMpar.addObjectsFromArray(FinderScolMaquetteParcours.getParcoursForInscriptionEtudiant(ie));
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(listeMpar);
	}

	/**
	 * @param etudiant
	 * @param fannKey
	 * @param typeResponsabilite
	 * @return La liste des responsables de parcours d'un étudiant
	 */
	private NSArray<IPersonne> getResponsablesParcoursByEtudiant(IEtudiant etudiant, Integer fannKey,
			String typeResponsabilite) {
		NSMutableArray<IPersonne> listeResp = new NSMutableArray<IPersonne>();

		NSArray<EOScolMaquetteParcours> arrMpar = getListeScolMaquetteParcoursByEtudiant(etudiant, fannKey);
		for (EOScolMaquetteParcours mpar : arrMpar) {
			NSArray<EOScolMaquetteResponsablePar> arrResp = FinderScolMaquetteParcours.getResponsablesParcours(mpar,
					typeResponsabilite, fannKey);
			for (EOScolMaquetteResponsablePar responsablePar : arrResp) {
				listeResp.addObject(responsablePar.toPersonne());
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeResp));
	}

	/**
	 * @param mec
	 * @param fannKey
	 * @param typeResponsabilite
	 * @return La liste des responsables de parcours associés à un EC
	 */
	private NSArray<IPersonne> getResponsablesParcoursByEc(EOScolMaquetteEc mec, Integer fannKey,
			String typeResponsabilite) {
		NSMutableArray<IPersonne> listeResp = new NSMutableArray<IPersonne>();

		if (mec != null) {
			EOQualifier qual = ERXQ.equals(EOScolMaquetteResponsablePar.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY
					+ "." + EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
					+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY + "."
					+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY + "."
					+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY + "."
					+ EOScolMaquetteUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY + "."
					+ EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY, mec);
			if (fannKey == null) {
				fannKey = mec.toFwkScolarite_ScolFormationAnnee().fannKey();
			}
			EOQualifier qualFannKey = ERXQ.equals(
					EOScolMaquetteResponsablePar.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
							+ EOScolFormationAnnee.FANN_KEY_KEY, fannKey);
			qual = ERXQ.and(qual, qualFannKey);

			if (typeResponsabilite != null) {
				if (TYPE_RESPONSABLE.equals(typeResponsabilite)) {
					qual = ERXQ.and(qual, EOScolMaquetteResponsablePar.QUAL_RESPONSABLE);
				} else if (TYPE_SECRETAIRE.equals(typeResponsabilite)) {
					qual = ERXQ.and(qual, EOScolMaquetteResponsablePar.QUAL_SECRETAIRE);
				}
			}

			NSArray<EOScolMaquetteResponsablePar> arrResp = EOScolMaquetteResponsablePar.fetchAll(mec.editingContext(),
					qual);
			for (EOScolMaquetteResponsablePar responsablePar : arrResp) {
				listeResp.addObject(responsablePar.toPersonne());
			}
		}

		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeResp));
	}

	/**
	 * @param mec
	 * @param fannKey
	 * @param typeResponsabilite
	 * @return La liste des responsables de formations associées à un EC
	 */
	private NSArray<IPersonne> getResponsablesFormationByEc(EOScolMaquetteEc mec, Integer fannKey,
			String typeResponsabilite) {
		NSMutableArray<IPersonne> listeResp = new NSMutableArray<IPersonne>();

		if (mec != null) {
			EOQualifier qual = ERXQ.equals(
					EOScolFormationResponsabilite.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURSS_KEY + "."
							+ EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
							+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY + "."
							+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY + "."
							+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY + "."
							+ EOScolMaquetteUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY + "."
							+ EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY, mec);
			if (fannKey == null) {
				fannKey = mec.toFwkScolarite_ScolFormationAnnee().fannKey();
			}
			EOQualifier qualFannKey = ERXQ.equals(
					EOScolMaquetteResponsablePar.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + "."
							+ EOScolFormationAnnee.FANN_KEY_KEY, fannKey);
			qual = ERXQ.and(qual, qualFannKey);

			if (typeResponsabilite != null) {
				if (TYPE_RESPONSABLE.equals(typeResponsabilite)) {
					qual = ERXQ.and(qual, EOScolFormationResponsabilite.QUAL_RESPONSABLE);
				} else if (TYPE_SECRETAIRE.equals(typeResponsabilite)) {
					qual = ERXQ.and(qual, EOScolFormationResponsabilite.QUAL_SECRETAIRE);
				}
			}
			NSArray<EOScolFormationResponsabilite> arrResp = EOScolFormationResponsabilite.fetchAll(
					mec.editingContext(), qual);
			for (EOScolFormationResponsabilite responsableFor : arrResp) {
				listeResp.addObject(responsableFor.toOneIndividu());
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeResp));
	}

	/**
	 * @param edc
	 * @param persId
	 * @param fannKey
	 * @param typeResponsabilite
	 * @return La liste des EC associés à un responsable d'UE
	 */
	private NSArray<EOScolMaquetteEc> getEcByResponsableUe(EOEditingContext edc, Integer persId, Integer fannKey,
			String typeResponsabilite) {
		NSMutableArray<EOScolMaquetteEc> listeEc = new NSMutableArray<EOScolMaquetteEc>();

		if (persId != null) {
			EOScolFormationAnnee year = FinderScolFormationAnnee.getScolFormationAnnee(edc, fannKey);
			NSArray<EOScolMaquetteUe> listeUe = FinderScolMaquetteUe.getUeForResponsable(edc, persId, fannKey,
					typeResponsabilite);
			for (EOScolMaquetteUe ue : listeUe) {
				listeEc.addObjectsFromArray(FinderScolMaquetteUe.getEcForUeAndYear(edc, ue, year,
						FinderScolMaquetteEc.QUAL_EC_STAGE));
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeEc));
	}

	/**
	 * @param edc
	 * @param persId
	 * @param fannKey
	 * @param typeResponsabilite
	 * @return La liste des EC associés à un responsable de parcours
	 */
	private NSArray<EOScolMaquetteEc> getEcByResponsableParcours(EOEditingContext edc, Integer persId, Integer fannKey,
			String typeResponsabilite) {
		NSMutableArray<EOScolMaquetteEc> listeEc = new NSMutableArray<EOScolMaquetteEc>();

		if (persId != null) {
			EOScolFormationAnnee year = FinderScolFormationAnnee.getScolFormationAnnee(edc, fannKey);
			NSArray<EOScolMaquetteParcours> listePar = FinderScolMaquetteParcours.getParcoursForResponsable(edc,
					persId, typeResponsabilite, fannKey);
			for (EOScolMaquetteParcours parcours : listePar) {
				NSArray<EOScolMaquetteEc> ecs = FinderScolMaquetteEc.getEcForParcoursAndLibelle(edc, parcours, null,
						year);
				listeEc.addObjectsFromArray(ERXArrayUtilities.filteredArrayWithQualifierEvaluation(ecs,
						FinderScolMaquetteEc.QUAL_EC_STAGE));
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeEc));
	}

	/**
	 * @param edc
	 * @param persId
	 * @param fannKey
	 * @param typeResponsabilite
	 * @return La liste des EC associés à un responsable de formation
	 */
	private NSArray<EOScolMaquetteEc> getEcByResponsableFormation(EOEditingContext edc, Integer persId,
			Integer fannKey, String typeResponsabilite) {
		NSMutableArray<EOScolMaquetteEc> listeEc = new NSMutableArray<EOScolMaquetteEc>();

		if (persId != null) {
			EOScolFormationAnnee year = FinderScolFormationAnnee.getScolFormationAnnee(edc, fannKey);

			NSArray<EOScolFormationSpecialisation> listeSpe = FinderScolFormationResponsabilite
					.getSpecialisationsByResponsable(edc, persId, fannKey, typeResponsabilite);
			NSMutableArray<EOScolMaquetteParcours> listePar = new NSMutableArray<EOScolMaquetteParcours>();
			for (EOScolFormationSpecialisation specialisation : listeSpe) {
				listePar.addObjectsFromArray(specialisation.toFwkScolarite_ScolMaquetteParcourss());
			}
			listePar = ERXArrayUtilities.arrayWithoutDuplicates(listePar).mutableClone();
			for (EOScolMaquetteParcours parcours : listePar) {
				NSArray<EOScolMaquetteEc> ecs = FinderScolMaquetteEc.getEcForParcoursAndLibelle(edc, parcours, null,
						year);
				listeEc.addObjectsFromArray(ERXArrayUtilities.filteredArrayWithQualifierEvaluation(ecs,
						FinderScolMaquetteEc.QUAL_EC_STAGE));
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeEc));
	}

	/**
	 * @param etudiant
	 * @param fannKey
	 * @param typeResponsabilite
	 * @return La liste des responsables de formation pour un étudiant
	 */
	private NSArray<IPersonne> getResponsablesFormationByEtudiant(IEtudiant etudiant, Integer fannKey,
			String typeResponsabilite) {
		NSMutableArray<IPersonne> listeResp = new NSMutableArray<IPersonne>();

		NSArray<EOScolFormationSpecialisation> listeFspn = getListeScolFormationSpecialisationByEtudiant(etudiant,
				fannKey);
		for (EOScolFormationSpecialisation fspn : listeFspn) {

			NSArray<EOScolFormationResponsabilite> arrResp = FinderScolFormationResponsabilite
					.fetchBySpecialisationAndType(fspn, fannKey, typeResponsabilite);
			for (EOScolFormationResponsabilite responsablePar : arrResp) {
				listeResp.addObject(responsablePar.toOneIndividu());
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXArrayUtilities.removeNullValues(listeResp));
	}
}
