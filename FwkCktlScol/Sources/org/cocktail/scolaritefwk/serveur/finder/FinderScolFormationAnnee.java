/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderScolFormationAnnee extends Finder {

	/**
	 * Recherche une annee precise<BR>
	 * 
	 * @param ec
	 * @param fannDebut
	 * 
	 * @return EOScolFormationAnnee
	 */
	public static EOScolFormationAnnee getScolFormationAnnee(EOEditingContext ec, Integer fannDebut) {
		EOScolFormationAnnee scolFormationAnnee = null;

		if (ec != null && fannDebut != null) {
			EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationAnnee.FANN_DEBUT_KEY, EOQualifier.QualifierOperatorEqual, fannDebut);
			scolFormationAnnee = EOScolFormationAnnee.fetchByQualifier(ec, qualifier);
		}

		return scolFormationAnnee;
	}

	/**
	 * Recherche une annee precise<BR>
	 * 
	 * @param ec
	 * @param fannKey
	 * 
	 * @return EOScolFormationAnnee
	 */
	public static EOScolFormationAnnee getScolFormationAnneeByKey(EOEditingContext ec, Integer fannKey) {
		EOScolFormationAnnee scolFormationAnnee = null;

		if (ec != null && fannKey != null) {
			EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationAnnee.FANN_KEY_KEY, EOQualifier.QualifierOperatorEqual, fannKey);
			scolFormationAnnee = EOScolFormationAnnee.fetchByQualifier(ec, qualifier);
		}

		return scolFormationAnnee;
	}

	/**
	 * Recherche l'annee courante<BR>
	 * 
	 * @param ec
	 * 
	 * @return EOScolFormationAnnee
	 */
	public static EOScolFormationAnnee getScolFormationAnneeCourante(EOEditingContext ec) {
		EOScolFormationAnnee scolFormationAnnee = null;

		if (ec != null) {
			EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationAnnee.FANN_COURANTE_KEY, EOQualifier.QualifierOperatorEqual,
					EOScolFormationAnnee.ANNEE_COURANTE);
			scolFormationAnnee = EOScolFormationAnnee.fetchByQualifier(ec, qualifier);
		}

		return scolFormationAnnee;
	}

	/**
	 * Recherche la liste des annees declarees<BR>
	 * 
	 * @param ec
	 * 
	 * @return NSArray<EOScolFormationAnnee>
	 */
	public static NSArray<EOScolFormationAnnee> getLstAnnees(EOEditingContext ec) {
		NSMutableArray<EOScolFormationAnnee> annees = new NSMutableArray<EOScolFormationAnnee>();

		if (ec != null) {
			annees = (NSMutableArray<EOScolFormationAnnee>) EOUtilities.objectsForEntityNamed(ec, EOScolFormationAnnee.ENTITY_NAME).mutableClone();
			EOSortOrdering.sortArrayUsingKeyOrderArray(annees, new NSArray<EOSortOrdering>(new EOSortOrdering(EOScolFormationAnnee.FANN_KEY_KEY,
					EOSortOrdering.CompareCaseInsensitiveAscending)));
		}

		return annees.immutableClone();
	}

}
