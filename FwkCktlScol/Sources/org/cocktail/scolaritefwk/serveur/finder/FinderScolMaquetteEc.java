/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionEtudiant;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionAp;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteResponsableEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;
import org.cocktail.scolarix.serveur.finder.FinderEtudiant;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.eof.qualifiers.ERXInQualifier;
import er.extensions.qualifiers.ERXAndQualifier;

public abstract class FinderScolMaquetteEc {

	public static final EOSortOrdering SORT_MEC_LIBELLE_ASC = new EOSortOrdering(EOScolMaquetteEc.MEC_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);

	public static final EOQualifier QUAL_EC_STAGE = ERXQ.like(EOScolMaquetteEc.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE_KEY + "." + EOScolFormationDiscipline.FDSC_CODE_KEY,
			"STAG*");
	/**
	 * retourne la liste des AP pour un Ec donnee
	 * 
	 * @param ec
	 * @param ecu
	 * @param number
	 * @return
	 */
	public static NSArray<EOScolMaquetteAp> getApForEcAndYear(EOEditingContext ec, EOScolMaquetteEc ecu, EOScolFormationAnnee number) {
		return getApForEcAndYear(ec, ecu, number, null);
	}

	/**
	 * retourne la liste des AP pour un Ec donnee avec un qualifier en parametre
	 * 
	 * @param ec
	 * @param ecu
	 * @param number
	 * @param addQual
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOScolMaquetteAp> getApForEcAndYear(EOEditingContext ec, EOScolMaquetteEc ecu, EOScolFormationAnnee number, EOQualifier addQual) {

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteAp.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_APS_KEY + "." + EOScolMaquetteRepartitionAp.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY
				+ " = %@";
		tbKeys.add(ecu);

		conditionStr += " AND " + EOScolMaquetteAp.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(number);
		// CktlLog.log(" - "+conditionStr);
		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(conditionStr, tbKeys);
		EOQualifier qual = null;
		if (addQual != null) {
			qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qualIni, addQual }));
		} else {
			qual = qualIni;
		}
		@SuppressWarnings("rawtypes")
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolMaquetteAp.ENTITY_NAME, qual, new NSArray(new Object[] { new EOSortOrdering(
				EOScolMaquetteAp.MAP_LIBELLE_KEY, EOSortOrdering.CompareAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<EOScolMaquetteAp> retour = ec.objectsWithFetchSpecification(fetchSpec);
		return retour;

	}

	/**
	 * Retourne l'équipe d'un EC donnee
	 * 
	 * @param edCo
	 * @param ec
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOScolMaquetteResponsableEc> getEquipeForEc(EOEditingContext edCo, EOScolMaquetteEc ec) {
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY + " = %@";
		tbKeys.add(ec);

		conditionStr += " AND " + EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(ec.toFwkScolarite_ScolFormationAnnee());
		// CktlLog.log(" - "+conditionStr);
		@SuppressWarnings("rawtypes")
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolMaquetteResponsableEc.ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(conditionStr, tbKeys),
		// new NSArray(new Object[] { new EOSortOrdering(
		// EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_CONSTANTE_RESPONSABILITE_KEY
		// , EOSortOrdering.CompareAscending) })
				null

		);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<EOScolMaquetteResponsableEc> retour = edCo.objectsWithFetchSpecification(fetchSpec);
		return retour;
	}

	/**
	 * @param ec
	 * @param qualifier
	 * @return Les personnes responsables de l'EC, en filtrant avec le qualifier
	 */
	public static NSArray<EOScolMaquetteResponsableEc> getEquipeByTypeForEc(EOScolMaquetteEc ec, EOQualifier qualifier) {
		NSArray<EOScolMaquetteResponsableEc> liste = getEquipeForEc(ec.editingContext(), ec);
		return ERXQ.filtered(liste, qualifier);
	}

	/**
	 * @param ec
	 * @return Les responsables ('R')
	 */
	public static NSArray<EOScolMaquetteResponsableEc> getEquipeResponsableForEc(EOScolMaquetteEc ec) {
		return getEquipeByTypeForEc(ec, EOScolMaquetteResponsableEc.QUAL_RESPONSABLE);
	}

	/**
	 * @param ec
	 * @return Les secrétaitres ('S')
	 */
	public static NSArray<EOScolMaquetteResponsableEc> getEquipeSecretaireForEc(EOScolMaquetteEc ec) {
		return getEquipeByTypeForEc(ec, EOScolMaquetteResponsableEc.QUAL_SECRETAIRE);
	}

	/**
	 * @param ec
	 * @return Les enseignants ('E')
	 */
	public static NSArray<EOScolMaquetteResponsableEc> getEquipeEnseignantForEc(EOScolMaquetteEc ec) {
		return getEquipeByTypeForEc(ec, EOScolMaquetteResponsableEc.QUAL_ENSEIGNANT);
	}

	/** retourne les EC du parcours en filtrant en fonction du libelle */
	public static NSArray<EOScolMaquetteEc> getEcForParcoursAndLibelle(EOEditingContext ec, EOScolMaquetteParcours parcours, String libelleEc, EOScolFormationAnnee year) {

		String parcoursKey = EOScolMaquetteEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY + "." + EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY + "."
				+ EOScolMaquetteUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY + "." + EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY + "."
				+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "." + EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY;

		NSMutableArray<EOQualifier> array = new NSMutableArray<EOQualifier>();
		array.add(new EOKeyValueQualifier(EOScolMaquetteEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, year));
		if (parcours != null) {
//			System.out.println("Parcours non null");
			array.add(new EOKeyValueQualifier(parcoursKey, EOQualifier.QualifierOperatorEqual, parcours));
		}
//		System.out.println("libelleEc=" + libelleEc);
		if (libelleEc != null && !"".equals(libelleEc.trim())) {
			array.add(new EOKeyValueQualifier(EOScolMaquetteEc.MEC_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, libelleEc));
		}
		if (array.count() == 0) {
			return NSArray.EmptyArray;
		} else {

			EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolMaquetteEc.ENTITY_NAME, new EOAndQualifier(array), new NSArray<EOSortOrdering>(SORT_MEC_LIBELLE_ASC));
			fetchSpec.setUsesDistinct(true);
			fetchSpec.setIsDeep(true);
			return ec.objectsWithFetchSpecification(fetchSpec);
		}
	}

	/** retourne la liste des EC pour un domaine complet pour une année */
	public static NSArray<EOScolMaquetteEc> getEcForDomaineAndYear(EOEditingContext ec, EOScolFormationDomaine dom, EOScolFormationAnnee year) {
		NSArray<EOScolFormationDiplome> diplomes = FinderScolFormationDiplome.getDiplomesForDomaineAndYear(ec, dom, year);
		NSMutableArray<EOScolMaquetteEc> listeEc = new NSMutableArray<EOScolMaquetteEc>();
		for (EOScolFormationDiplome dipl : diplomes) {
			listeEc.addObjectsFromArray(getEcForDiplome(ec, dipl, year));
		}
		return listeEc;
	}

	/** retourne la liste des EC pour un diplome complet */
	public static NSArray<EOScolMaquetteEc> getEcForDiplome(EOEditingContext ec, EOScolFormationDiplome dipl, EOScolFormationAnnee annee) {
		@SuppressWarnings("unchecked")
		NSArray<EOScolFormationSpecialisation> specs = dipl.toFwkScolarite_ScolFormationSpecialisations();

		NSMutableArray<EOScolMaquetteEc> listeEc = new NSMutableArray<EOScolMaquetteEc>();

		for (EOScolFormationSpecialisation spec : specs) {
			listeEc.addObjectsFromArray(getEcForSpecialisation(ec, spec, annee));
		}
		return listeEc;
	}

	/** retourne la liste des EC pour une specialisation */
	public static NSArray<EOScolMaquetteEc> getEcForSpecialisation(EOEditingContext ec, EOScolFormationSpecialisation spec, EOScolFormationAnnee annee) {
		@SuppressWarnings("unchecked")
		NSArray<EOScolMaquetteParcours> parcours = spec.toFwkScolarite_ScolMaquetteParcourss();

		NSMutableArray<EOScolMaquetteEc> listeEc = new NSMutableArray<EOScolMaquetteEc>();

		for (EOScolMaquetteParcours par : parcours) {
			listeEc.addObjectsFromArray(getEcForParcoursAndLibelle(ec, par, null, annee));
		}

		return listeEc;
	}

	/**
	 * @param ec
	 * @return Le qualifier permettant d'avoir les étudiants inscrits à l'EC
	 */
	private static EOQualifier getEtudiantsForEcQualifier(EOScolMaquetteEc ec) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		// L'Ec
		qualifiers.add(ERXQ.equals(EOScolInscriptionEtudiant.TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ECS_KEY + "."
				+ EOScolInscriptionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_EC_KEY + "." + EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY, ec));
		// L'année
		qualifiers.add(ERXQ.equals(EOScolInscriptionEtudiant.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, ec.toFwkScolarite_ScolFormationAnnee()));
		// Démission
		qualifiers.add(new ERXInQualifier(EOScolInscriptionEtudiant.RES_CODE_KEY, new NSArray<String>("D", "E", "Z", "H", "1")).not().or(
				ERXQ.isNull(EOScolInscriptionEtudiant.RES_CODE_KEY)));
		// Dispense
		qualifiers.add(new ERXInQualifier(EOScolInscriptionEtudiant.TO_FWK_SCOLARITE__SCOL_INSCRIPTION_ECS_KEY + "." + EOScolInscriptionEc.IMREC_DISPENSE_KEY,
				new NSArray<Integer>(0, 2, 11, 10, 20)));

		return new ERXAndQualifier(qualifiers);
	}

	/**
	 * @param edc
	 * @param ec
	 * @return La liste des étudiants inscrits à l'EC
	 */
	public static NSArray<EOScolInscriptionEtudiant> getEtudiantsForEc(EOEditingContext edc, EOScolMaquetteEc ec) {
		EOQualifier qualifier = getEtudiantsForEcQualifier(ec);
		ERXSortOrderings sortOrder = ERXS.ascs(EOScolInscriptionEtudiant.ADR_NOM_KEY, EOScolInscriptionEtudiant.ADR_PRENOM_KEY);

		ERXFetchSpecification<EOScolInscriptionEtudiant> fetchSpec = new ERXFetchSpecification(EOScolInscriptionEtudiant.ENTITY_NAME, qualifier, sortOrder);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<EOScolInscriptionEtudiant> retour = edc.objectsWithFetchSpecification(fetchSpec);

		return retour;
	}

	/**
	 * @param edc
	 * @param mecKey
	 * @param year
	 * @return Le CPT_ORDRE des étudiants inscrits à l'EC
	 */
	public static NSArray<Integer> getRawCptEtudiantsForMecKey(EOEditingContext edc, Integer mecKey, EOScolFormationAnnee year) {

		EOScolMaquetteEc mec = EOScolMaquetteEc.fetchByQualifier(edc,
				ERXQ.equals(EOScolMaquetteEc.MEC_KEY_KEY, mecKey).and(ERXQ.equals(EOScolMaquetteEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, year)));
		EOQualifier qualifier = getEtudiantsForEcQualifier(mec);

		ERXFetchSpecification<EOScolInscriptionEtudiant> fetchSpec = new ERXFetchSpecification(EOScolInscriptionEtudiant.ENTITY_NAME, qualifier, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		fetchSpec.setRawRowKeyPaths(new NSArray<String>(EOScolInscriptionEtudiant.CPT_ORDRE_KEY));
		NSArray<Object> arr = edc.objectsWithFetchSpecification(fetchSpec);
		NSArray<Integer> result = (NSArray<Integer>) arr.valueForKey(EOScolInscriptionEtudiant.CPT_ORDRE_KEY);
		return result;
	}

	/**
	 * @param ec
	 * @param qualifier
	 *            Qualifier à appliquer (peut être null)
	 * @return La liste des PERS_ID de l'équipe associée à l'EC
	 */
	public static NSArray<Integer> getRawEquipeForEc(EOScolMaquetteEc ec, EOQualifier qualifier) {
		EOQualifier final_qualifier = ERXQ.equals(EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY, ec)
				.and(ERXQ.equals(EOScolMaquetteResponsableEc.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, ec.toFwkScolarite_ScolFormationAnnee())).and(qualifier);

		ERXFetchSpecification<EOScolMaquetteResponsableEc> fetchSpec = new ERXFetchSpecification<EOScolMaquetteResponsableEc>(EOScolMaquetteResponsableEc.ENTITY_NAME,
				final_qualifier, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		fetchSpec.setRawRowKeyPaths(new NSArray<String>(EOScolMaquetteResponsableEc.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY));
		NSArray<Object> arr = ec.editingContext().objectsWithFetchSpecification(fetchSpec);
		NSArray<Integer> result = (NSArray<Integer>) arr.valueForKey(EOScolMaquetteResponsableEc.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY);

		return result;
	}

	public static NSArray<EOScolMaquetteEc> getEcForEtudiant(EOEditingContext edc, Integer etudNumero, EOScolFormationAnnee anneeScol, EOQualifier qualifier) {
		IEtudiant etudiant = FinderEtudiant.getEtudiant(edc, etudNumero, null, null);
		NSMutableArray<EOScolMaquetteEc> listeMec = new NSMutableArray<EOScolMaquetteEc>();

		// Inscriptions
		EOHistorique histo = etudiant.historique(anneeScol.fannKey());
		if (histo == null) {
			return NSArray.EmptyArray;
		}
		NSArray<EOInscDipl> listeInscDipl = histo.inscriptionsValides();
		for (EOInscDipl currentInscDipl : listeInscDipl) {
			EOScolInscriptionEtudiant uneScolInscEtu = EOScolInscriptionEtudiant.fetchFirstRequiredByKeyValue(edc, EOScolInscriptionEtudiant.IDIPL_NUMERO_KEY,
					currentInscDipl.idiplNumero());
			NSArray<EOScolMaquetteEc> tmpListeMec = (NSArray<EOScolMaquetteEc>) uneScolInscEtu.toFwkScolarite_ScolInscriptionEcs().valueForKey(
					EOScolInscriptionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_EC_KEY + "."
							+ EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY);

			listeMec.addObjectsFromArray(tmpListeMec);
		}
		ERXQ.filter(listeMec, qualifier);
		return listeMec;
	}
}
