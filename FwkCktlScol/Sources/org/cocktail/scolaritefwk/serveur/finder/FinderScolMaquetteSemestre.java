/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderScolMaquetteSemestre extends Finder {

	/**
	 * Recherche les semestres d'une année universitaire<BR>
	 * 
	 * @param ec
	 * @param scolFormationAnnee
	 * 
	 * @return NSArray<EOScolMaquetteSemestre>
	 */
	public static NSArray<EOScolMaquetteSemestre> getSemestresFromYear(EOEditingContext ec, EOScolFormationAnnee scolFormationAnnee) {
		NSArray<EOScolMaquetteSemestre> semestres = new NSArray<EOScolMaquetteSemestre>();

		if (ec != null && scolFormationAnnee != null) {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(new EOKeyValueQualifier(EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOQualifier.QualifierOperatorEqual,
					scolFormationAnnee));

			semestres = (NSArray<EOScolMaquetteSemestre>) EOScolMaquetteSemestre.fetchAll(ec, new EOAndQualifier(qualifiers), new NSArray<Object>(new Object[] {
					new EOSortOrdering(EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOSortOrdering.CompareAscending),
					new EOSortOrdering(EOScolMaquetteSemestre.MSEM_ORDRE_KEY, EOSortOrdering.CompareAscending) }), true);
		}

		return semestres;
	}

	/**
	 * Recherche les semestres d'un parcours d'une année universitaire<BR>
	 * 
	 * @param ec
	 * @param scolMaquetteParcours
	 * @param scolFormationAnnee
	 * 
	 * @return NSArray<EOScolMaquetteSemestre>
	 */
	public static NSArray<EOScolMaquetteSemestre> getSemestreFromParcoursAndYear(EOEditingContext ec, EOScolMaquetteParcours scolMaquetteParcours,
			EOScolFormationAnnee scolFormationAnnee) {
		NSArray<EOScolMaquetteSemestre> semestres = new NSArray<EOScolMaquetteSemestre>();

		if (ec != null && scolMaquetteParcours != null && scolFormationAnnee != null) {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(new EOKeyValueQualifier(EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
					+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY, EOQualifier.QualifierOperatorEqual, scolMaquetteParcours));
			qualifiers.addObject(new EOKeyValueQualifier(EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOQualifier.QualifierOperatorEqual,
					scolFormationAnnee));

			semestres = (NSArray<EOScolMaquetteSemestre>) EOScolMaquetteSemestre.fetchAll(ec, new EOAndQualifier(qualifiers), new NSArray<Object>(new Object[] {
					new EOSortOrdering(EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOSortOrdering.CompareAscending),
					new EOSortOrdering(EOScolMaquetteSemestre.MSEM_ORDRE_KEY, EOSortOrdering.CompareAscending) }), true);
		}

		return semestres;
	}

	/**
	 * Recherche les U.E. rattachées à un semestre d'une année universitaire<BR>
	 * 
	 * @param ec
	 * @param scolMaquetteSemestre
	 * @param scolFormationAnnee
	 * 
	 * @return NSArray<EOScolMaquetteUe>
	 */
	public static NSArray<EOScolMaquetteUe> getUeForSemestreAndYear(EOEditingContext ec, EOScolMaquetteSemestre scolMaquetteSemestre,
			EOScolFormationAnnee scolFormationAnnee) {
		return getUeForSemestreAndYear(ec, scolMaquetteSemestre, scolFormationAnnee, null);
	}

	/**
	 * Recherche les U.E. rattachées à un semestre d'une année universitaire + un qualifier auxiliaire<BR>
	 * 
	 * @param ec
	 * @param scolMaquetteSemestre
	 * @param scolFormationAnnee
	 * @param addQual
	 * 
	 * @return NSArray<EOScolMaquetteUe>
	 */
	public static NSArray<EOScolMaquetteUe> getUeForSemestreAndYear(EOEditingContext ec, EOScolMaquetteSemestre scolMaquetteSemestre,
			EOScolFormationAnnee scolFormationAnnee, EOQualifier addQual) {
		NSArray<EOScolMaquetteUe> ues = new NSArray<EOScolMaquetteUe>();

		if (ec != null && scolMaquetteSemestre != null && scolFormationAnnee != null) {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(new EOKeyValueQualifier(EOScolMaquetteUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY + "."
					+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY, EOQualifier.QualifierOperatorEqual, scolMaquetteSemestre));
			qualifiers.addObject(new EOKeyValueQualifier(EOScolMaquetteUe.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, EOQualifier.QualifierOperatorEqual,
					scolFormationAnnee));

			if (addQual != null) {
				qualifiers.addObject(addQual);
			}

			ues = (NSArray<EOScolMaquetteUe>) EOScolMaquetteUe.fetchAll(ec, new EOAndQualifier(qualifiers), new NSArray<Object>(
					new Object[] { new EOSortOrdering(EOScolMaquetteUe.MUE_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending) }), true);
		}

		return ues;
	}

}
