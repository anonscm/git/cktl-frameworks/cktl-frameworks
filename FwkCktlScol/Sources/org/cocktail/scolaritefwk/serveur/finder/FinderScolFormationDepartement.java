/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDepartement;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderScolFormationDepartement extends Finder {

	/**
	 * Recherche un departement pedagogique precis<BR>
	 * 
	 * @param ec
	 * @param fdepCode
	 * @param fdepLibelle
	 * 
	 * @return EOScolFormationDepartement
	 */
	public static EOScolFormationDepartement getScolFormationDepartement(EOEditingContext ec, String fdepCode, String fdepLibelle) {
		EOScolFormationDepartement scolFormationDepartement = null;

		if (ec != null && (fdepCode != null || fdepLibelle != null)) {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			if (fdepCode != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationDepartement.FDEP_CODE_KEY, EOQualifier.QualifierOperatorEqual, fdepCode));
			}
			if (fdepLibelle != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationDepartement.FDEP_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
						fdepLibelle));
			}
			scolFormationDepartement = EOScolFormationDepartement.fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}

		return scolFormationDepartement;
	}

	/**
	 * Recherche les departements pedagogiques par un filtre<BR>
	 * where fdepCode = filtre or fdepLibelle caseInsensitiveLike *filtre*<BR>
	 * Convention : si l'on veut tout obtenir, le filtre sera egal a "*"<BR>
	 * 
	 * @param ec
	 * @param filtre
	 * 
	 * @return NSArray<EOScolFormationDepartement>
	 */
	public static NSArray<EOScolFormationDepartement> getScolFormationDepartementsFiltre(EOEditingContext ec, String filtre) {
		NSArray<EOScolFormationDepartement> departements = null;

		if (ec != null && !StringCtrl.isEmpty(filtre)) {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationDepartement.FDEP_CODE_KEY, EOQualifier.QualifierOperatorEqual, filtre));
			qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationDepartement.FDEP_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"
					+ filtre + "*"));

			departements = (NSArray<EOScolFormationDepartement>) EOScolFormationDepartement.fetchAll(ec, new EOOrQualifier(qualifiers), new NSArray<Object>(
					new Object[] { new EOSortOrdering(EOScolFormationDepartement.FDEP_CODE_KEY, EOSortOrdering.CompareAscending) }));
		}

		return departements;
	}

	/**
	 * Recherche les departements pedagogiques valides par un filtre<BR>
	 * where fdepCode = filtre or fdepLibelle caseInsensitiveLike *filtre*<BR>
	 * Convention : si l'on veut tout obtenir, le filtre sera egal a "*"<BR>
	 * 
	 * @param ec
	 * @param filtre
	 * 
	 * @return NSArray<EOScolFormationDepartement>
	 */
	public static NSArray<EOScolFormationDepartement> getScolFormationDepartementsValidesFiltre(EOEditingContext ec, String filtre) {
		NSArray<EOScolFormationDepartement> departements = getScolFormationDepartementsFiltre(ec, filtre);

		if (ec != null && !StringCtrl.isEmpty(filtre)) {
			EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationDepartement.FDEP_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual,
					EOScolFormationDepartement.DEPARTEMENT_VALIDE);
			departements = (NSArray<EOScolFormationDepartement>) EOQualifier.filteredArrayWithQualifier(departements, qualifier);
		}

		return departements;
	}

}
