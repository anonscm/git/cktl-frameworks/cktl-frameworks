/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class FinderScolFormationDomaine extends Finder {

	/**
	 * Recherche d'un domaine precis<BR>
	 * 
	 * @param ec
	 * @param fdomCode
	 * @param fdomLibelle
	 * 
	 * @return un EOScolFormationDomaine
	 */
	public static EOScolFormationDomaine getScolFormationDomaine(EOEditingContext ec, String fdomCode, String fdomLibelle) {
		EOScolFormationDomaine scolFormationDomaine = null;
		if (fdomCode != null || fdomLibelle != null) {
			NSMutableArray qualifiers = new NSMutableArray();
			if (fdomCode != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationDomaine.FDOM_CODE_KEY, EOQualifier.QualifierOperatorEqual, fdomCode));
			}
			if (fdomLibelle != null) {
				qualifiers.addObject(new EOKeyValueQualifier(EOScolFormationDomaine.FDOM_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
						fdomLibelle));
			}
			scolFormationDomaine = EOScolFormationDomaine.fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		return scolFormationDomaine;
	}

	/**
	 * Recherche des domaines par un filtre : where fdomCode = filtre or fdomLibelle caseInsensitiveLike *filtre*
	 * Convention : si l'on veut tout obtenir le filtre sera egal à "*"
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationDomaines = null;
		if (!StringCtrl.isEmpty(filtre)) {
			EOSortOrdering codeOrdering1 = EOSortOrdering.sortOrderingWithKey(EOScolFormationDomaine.FDOM_TYPE_KEY, EOSortOrdering.CompareAscending);
			EOSortOrdering codeOrdering2 = EOSortOrdering.sortOrderingWithKey(EOScolFormationDomaine.FDOM_CODE_KEY, EOSortOrdering.CompareAscending);
			NSArray sortOrderings = new NSArray(new EOSortOrdering[] { codeOrdering1, codeOrdering2 });

			EOKeyValueQualifier codeQualifier = new EOKeyValueQualifier(EOScolFormationDomaine.FDOM_CODE_KEY, EOQualifier.QualifierOperatorEqual, filtre);
			EOKeyValueQualifier libelleQualifier = new EOKeyValueQualifier(EOScolFormationDomaine.FDOM_LIBELLE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtre + "*");
			NSArray qualifiers = new NSArray(new EOQualifier[] { codeQualifier, libelleQualifier });
			EOOrQualifier qualifier = new EOOrQualifier(qualifiers);

			scolFormationDomaines = EOScolFormationDomaine.fetchAll(ec, qualifier, sortOrderings);
		}
		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines valides par un filtre : where fdomCode = filtre or fdomLibelle caseInsensitiveLike
	 * *filtre* Convention : si l'on veut tout obtenir le filtre sera egal à "*"
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesValidesFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationDomaines = getScolFormationDomainesFiltre(ec, filtre);

		EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationDomaine.FDOM_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual,
				EOScolFormationDomaine.DOMAINE_VALIDE);
		scolFormationDomaines = EOQualifier.filteredArrayWithQualifier(scolFormationDomaines, qualifier);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines par des filtres : where fdomCode = filtre or fdomLibelle caseInsensitiveLike *filtre* and
	 * fdomType = domaineType
	 * 
	 * @param ec
	 * @param filtre
	 * @param domaineType
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesFiltres(EOEditingContext ec, String filtre, String domaineType) {
		NSArray scolFormationDomaines = null;
		scolFormationDomaines = getScolFormationDomainesFiltre(ec, filtre);

		if ((!StringCtrl.isEmpty(domaineType)) && (scolFormationDomaines != null && scolFormationDomaines.count() > 0)) {
			EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationDomaine.FDOM_TYPE_KEY, EOQualifier.QualifierOperatorEqual, domaineType);

			scolFormationDomaines = EOQualifier.filteredArrayWithQualifier(scolFormationDomaines, qualifier);
		}
		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines valides par des filtres : where fdomCode = filtre or fdomLibelle caseInsensitiveLike
	 * *filtre* and fdomType = domaineType
	 * 
	 * @param ec
	 * @param filtre
	 * @param domaineType
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesValidesFiltres(EOEditingContext ec, String filtre, String domaineType) {
		NSArray scolFormationDomaines = getScolFormationDomainesFiltres(ec, filtre, domaineType);

		EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationDomaine.FDOM_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual,
				EOScolFormationDomaine.DOMAINE_VALIDE);
		scolFormationDomaines = EOQualifier.filteredArrayWithQualifier(scolFormationDomaines, qualifier);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines de type "DOMAINE_TYPE_DIPLOME" avec un filtre : where fdomCode = filtre or fdomLibelle
	 * caseInsensitiveLike *filtre* and fdomType = DOMAINE_TYPE_DIPLOME
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesDeTypeDiplomeAvecFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationDomaines = getScolFormationDomainesFiltres(ec, filtre, EOScolFormationDomaine.DOMAINE_TYPE_DIPLOME);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines valides de type "DOMAINE_TYPE_DIPLOME" avec un filtre : where fdomCode = filtre or
	 * fdomLibelle caseInsensitiveLike *filtre* and fdomType = DOMAINE_TYPE_DIPLOME
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesValidesDeTypeDiplomeAvecFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationDomaines = getScolFormationDomainesValidesFiltres(ec, filtre, EOScolFormationDomaine.DOMAINE_TYPE_DIPLOME);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines de type "DOMAINE_TYPE_UE" avec un filtre : where fdomCode = filtre or fdomLibelle
	 * caseInsensitiveLike *filtre* and fdomType = DOMAINE_TYPE_UE
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesDeTypeUeAvecFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationDomaines = getScolFormationDomainesFiltres(ec, filtre, EOScolFormationDomaine.DOMAINE_TYPE_UE);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines valides de type "DOMAINE_TYPE_UE" avec un filtre : where fdomCode = filtre or fdomLibelle
	 * caseInsensitiveLike *filtre* and fdomType = DOMAINE_TYPE_UE
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesValidesDeTypeUeAvecFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationDomaines = getScolFormationDomainesValidesFiltres(ec, filtre, EOScolFormationDomaine.DOMAINE_TYPE_UE);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines de type "DOMAINE_TYPE_COMPOSANTE" avec un filtre : where fdomCode = filtre or fdomLibelle
	 * caseInsensitiveLike *filtre* and fdomType = DOMAINE_TYPE_COMPOSANTE
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesDeTypeComposanteAvecFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationDomaines = getScolFormationDomainesFiltres(ec, filtre, EOScolFormationDomaine.DOMAINE_TYPE_COMPOSANTE);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines valides de type "DOMAINE_TYPE_COMPOSANTE" avec un filtre : where fdomCode = filtre or
	 * fdomLibelle caseInsensitiveLike *filtre* and fdomType = DOMAINE_TYPE_COMPOSANTE
	 * 
	 * @param ec
	 * @param filtre
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesValidesDeTypeComposanteAvecFiltre(EOEditingContext ec, String filtre) {
		NSArray scolFormationDomaines = getScolFormationDomainesValidesFiltres(ec, filtre, EOScolFormationDomaine.DOMAINE_TYPE_COMPOSANTE);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines par des filtres : where fdomCode = filtre or fdomLibelle caseInsensitiveLike *filtre* and
	 * fdomType in "bindings"
	 * 
	 * @param ec
	 * @param filtre
	 * @param domaineType
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesFiltres(EOEditingContext ec, String filtre, NSMutableDictionary bindings) {
		NSArray scolFormationDomaines = null;
		scolFormationDomaines = getScolFormationDomainesFiltre(ec, filtre);

		EOOrQualifier qualScolFormationDomaines = null;
		NSMutableArray qualifiers = new NSMutableArray();
		Enumeration<String> enumBindings = bindings.keyEnumerator();
		while (enumBindings.hasMoreElements()) {
			String key = enumBindings.nextElement();
			EOKeyValueQualifier qualifier = new EOKeyValueQualifier(key, EOQualifier.QualifierOperatorEqual, bindings.valueForKey(key));
			qualifiers.addObject(qualifier);
		}
		qualScolFormationDomaines = new EOOrQualifier(qualifiers);
		scolFormationDomaines = EOQualifier.filteredArrayWithQualifier(scolFormationDomaines, qualScolFormationDomaines);

		return scolFormationDomaines;
	}

	/**
	 * Recherche des domaines valides par des filtres : where fdomCode = filtre or fdomLibelle caseInsensitiveLike
	 * *filtre* and fdomType in "bindings"
	 * 
	 * @param ec
	 * @param filtre
	 * @param domaineType
	 * @return un NSArray contenant des EOScolFormationDomaine, sinon NULL
	 */
	public static NSArray getScolFormationDomainesValidesFiltres(EOEditingContext ec, String filtre, NSMutableDictionary bindings) {
		NSArray scolFormationDomaines = getScolFormationDomainesFiltres(ec, filtre, bindings);

		EOQualifier qualifier = new EOKeyValueQualifier(EOScolFormationDomaine.FDOM_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual,
				EOScolFormationDomaine.DOMAINE_VALIDE);
		scolFormationDomaines = EOQualifier.filteredArrayWithQualifier(scolFormationDomaines, qualifier);

		return scolFormationDomaines;
	}

	/**
	 * Recherche la liste des diplômes pour un domaine, un grade et une annee
	 * 
	 * @param ec
	 * @param domaine
	 * @param grade
	 * @param number
	 * @return NS Array de EOScolFormationDiplome
	 */
	public static NSArray<EOScolFormationDiplome> getDiplomesForDomaineGradeAndYear(EOEditingContext ec, EOScolFormationDomaine domaine,
			EOScolFormationGrade grade, EOScolFormationAnnee number) {

		NSMutableArray<EOGenericRecord> tbKeys = new NSMutableArray<EOGenericRecord>();
		String conditionStr = EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY + " = %@";
		tbKeys.add(domaine);

		conditionStr += " AND " + EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY + " = %@";
		tbKeys.add(grade);

		conditionStr += " AND " + EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY + "."
				+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(number);
		// CktlLog.log(" - "+conditionStr);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationDiplome.ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(conditionStr,
				tbKeys), new NSArray(new Object[] {
		/*
		 * new EOSortOrdering( EOScolFormationDiplome.SCOL_FORMATION_SPECIALISATIONS_KEY + "." +
		 * EOScolFormationSpecialisation.SCOL_FORMATION_HABILITATIONS_KEY + "." +
		 * EOScolFormationHabilitation.FHAB_NIVEAU_KEY, EOSortOrdering.CompareAscending),
		 */
		new EOSortOrdering(EOScolFormationDiplome.FDIP_ABREVIATION_KEY, EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * Recherche la liste des disciplines pour un domaine, un grade et une annee
	 * 
	 * @param ec
	 * @param domaine
	 * @param grade
	 * @param number
	 * @return
	 */

	public static NSArray<EOScolFormationDiscipline> getDisciplinesForDomaineGradeAndYear(EOEditingContext ec, EOScolFormationDomaine domaine,
			EOScolFormationGrade grade, EOScolFormationAnnee number) {
		return getDisciplinesForDomaineGradeAndYear(ec, domaine, grade, number, null);
	}

	public static NSArray<EOScolFormationDiscipline> getDisciplinesForDomaineGradeAndYear(EOEditingContext ec, EOScolFormationDomaine domaine,
			EOScolFormationGrade grade, EOScolFormationAnnee number, EOQualifier addQual) {

		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOScolFormationDiscipline.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES_KEY + "."
				+ EOScolFormationDisciplinaire.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
				+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY + " = %@";
		tbKeys.add(domaine);

		if (grade != null) {
			conditionStr += " AND " + EOScolFormationDiscipline.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES_KEY + "."
					+ EOScolFormationDisciplinaire.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
					+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY + " = %@";
			tbKeys.add(grade);
		}

		conditionStr += " AND " + EOScolFormationDiscipline.FDSC_VALIDITE_KEY + " = %@";
		tbKeys.add("O");

		conditionStr += " AND " + EOScolFormationDiscipline.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES_KEY + "."
				+ EOScolFormationDisciplinaire.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
				+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY + "."
				+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(number);
		// CktlLog.log(" - "+conditionStr);

		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(conditionStr, tbKeys);
		EOQualifier qual = null;
		if (addQual != null) {
			qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qualIni, addQual }));
		} else {
			qual = qualIni;
		}

	
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationDiscipline.ENTITY_NAME, qual, new NSArray(new Object[] { new EOSortOrdering(
				EOScolFormationDiscipline.FDSC_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * Recherche la liste des disciplines pour un domaine et une annee
	 * 
	 * @param ec
	 * @param domaine
	 * @param number
	 * @return
	 */
	public static NSArray<EOScolFormationDiscipline> getDisciplinesForDomaineAndYear(EOEditingContext ec, EOScolFormationDomaine domaine,
			EOScolFormationAnnee number) {
		return getDisciplinesForDomaineGradeAndYear(ec, domaine, null, number);
	}

	/**
	 * Recherche la liste des domaines pour un EC et une annee
	 * 
	 * @param ec
	 * @param annee
	 * @return
	 */
	public static NSMutableArray<EOScolFormationDomaine> getDomainesForEcAndYear(EOScolMaquetteEc ec, EOScolFormationAnnee annee) {
		return ((NSMutableArray<NSMutableArray>) ec.valueForKeyPath(
		/*
		 * EOScolMaquetteAp.SCOL_MAQUETTE_REPARTITION_APS_KEY+"."+ EOScolMaquetteRepartitionAp.SCOL_MAQUETTE_EC_KEY+"."+
		 */
		EOScolMaquetteEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY + "." + EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY + "."
				+ EOScolMaquetteUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY + "."
				+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY + "."
				+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
				+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURS_KEY + "."
				+ EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
				+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY))
		// .lastObject())
				.lastObject();

	}

}
