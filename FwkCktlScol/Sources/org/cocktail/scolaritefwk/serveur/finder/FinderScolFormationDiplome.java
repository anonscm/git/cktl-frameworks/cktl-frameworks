/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2012 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.scolaritefwk.serveur.finder;

import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAnnee;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDisciplinaire;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiscipline;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDomaine;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteParcours;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionEc;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionSem;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteRepartitionUe;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteSemestre;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolMaquetteUe;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderScolFormationDiplome extends Finder {

	/**
	 * Recherche les diplomes d'une discipline d'une annee universitaire<BR>
	 * 
	 * @param ec
	 * @param scolFormationDiscipline
	 * @param scolFormationAnnee
	 * 
	 * @return NSArray<EOScolFormationDiplome>
	 */
	public static NSArray<EOScolFormationDiplome> getDiplomesForDisciplineAndYear(EOEditingContext ec, EOScolFormationDiscipline scolFormationDiscipline,
			EOScolFormationAnnee scolFormationAnnee) {

		if (ec != null && scolFormationDiscipline != null && scolFormationAnnee != null) {
			NSMutableArray<EOGenericRecord> tbKeys = new NSMutableArray<EOGenericRecord>();

			String conditionStr = EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES_KEY + "."
					+ EOScolFormationDisciplinaire.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE_KEY + " = %@";
			tbKeys.add(scolFormationDiscipline);

			conditionStr += " AND " + EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
					+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY + "."
					+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
			tbKeys.add(scolFormationAnnee);
			// CktlLog.log(" - "+conditionStr);
			NSArray<EOSortOrdering> sort = new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					new EOSortOrdering(EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY + "." + EOScolFormationGrade.FGRA_ABREVIATION_KEY,
							EOSortOrdering.CompareAscending),
					new EOSortOrdering(EOScolFormationDiplome.FDIP_ABREVIATION_KEY, EOSortOrdering.CompareCaseInsensitiveAscending) });
			EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationDiplome.ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(
					conditionStr, tbKeys), null);
			fetchSpec.setUsesDistinct(true);
			fetchSpec.setIsDeep(true);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(ec.objectsWithFetchSpecification(fetchSpec), sort);
		}

		return null;

	}

	/**
	 * retourne la liste des diplomes pour une discipline, un grade et une annee donnee
	 * 
	 * @param ec
	 * @param disc
	 * @param grade
	 * @param number
	 * @return
	 */
	public static NSArray<EOScolFormationDiplome> getDiplomesForDisciplineGradeAndYear(EOEditingContext ec, EOScolFormationDiscipline eoObject,
			EOScolFormationGrade eoObject2, EOScolFormationAnnee selectedYear) {

		return getDiplomesForDisciplineGradeAndYear(ec, eoObject, eoObject2, selectedYear, null);
	}

	public static NSArray<EOScolFormationDiplome> getDiplomesForDisciplineGradeAndYear(EOEditingContext ec, EOScolFormationDiscipline disc,
			EOScolFormationGrade grade, EOScolFormationAnnee number, EOQualifier addQual) {

		if (ec == null)
			return null;
		if (disc == null)
			return null;
		if (grade == null)
			return null;
		NSMutableArray<EOGenericRecord> tbKeys = new NSMutableArray<EOGenericRecord>();
		String conditionStr = EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINAIRES_KEY + "."
				+ EOScolFormationDisciplinaire.TO_FWK_SCOLARITE__SCOL_FORMATION_DISCIPLINE_KEY + " = %@";
		tbKeys.add(disc);

		conditionStr += " AND " + EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY + "."
				+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(number);

		conditionStr += " AND " + EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY + " = %@";
		tbKeys.add(grade);

		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(conditionStr, tbKeys);
		EOQualifier qual = null;
		if (addQual != null)
			qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] { qualIni, addQual }));
		else
			qual = qualIni;

		// CktlLog.log(" - "+conditionStr);
		NSArray<EOSortOrdering> sort = new NSArray<EOSortOrdering>(new EOSortOrdering[] { new EOSortOrdering(EOScolFormationDiplome.FDIP_ABREVIATION_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending) });
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationDiplome.ENTITY_NAME, qual, null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(ec.objectsWithFetchSpecification(fetchSpec), sort);

	}

	/**
	 * retourne la liste des diplomes pour un domaine,un grade et une annee donnee
	 * 
	 * @param ec
	 * @param grade
	 * @param number
	 * @return
	 */
	public static NSArray<EOScolFormationDiplome> getDiplomesForDomaineGradeAndYear(EOEditingContext ec, EOScolFormationDomaine domaine,
			EOScolFormationGrade grade, EOScolFormationAnnee number) {
		if (ec == null)
			return null;
		if (number == null)
			return null;
		if (grade == null)
			return null;
		if (domaine == null)
			return null;
		NSMutableArray<EOGenericRecord> tbKeys = new NSMutableArray<EOGenericRecord>();

		String conditionStr = EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY + "."
				+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@";
		tbKeys.add(number);

		conditionStr += " AND " + EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY + " = %@";
		tbKeys.add(grade);

		conditionStr += " AND " + EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY + " = %@";
		tbKeys.add(domaine);

		// CktlLog.log(" - "+conditionStr);
		NSArray<EOSortOrdering> sort = new NSArray<EOSortOrdering>(new EOSortOrdering[] { new EOSortOrdering(EOScolFormationDiplome.FDIP_ABREVIATION_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending) });
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationDiplome.ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(conditionStr,
				tbKeys), null);
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(ec.objectsWithFetchSpecification(fetchSpec), sort);

	}

	/**
	 * retourne la liste des diplomes pour un domaine et une annee donnee
	 * 
	 * @param ec
	 * @param domaine
	 * @param year
	 * @return
	 */
	public static NSArray<EOScolFormationDiplome> getDiplomesForDomaineAndYear(EOEditingContext ec, EOScolFormationDomaine selectedDomaine,
			EOScolFormationAnnee year) {

		if (ec == null)
			return null;
		if (year == null)
			return null;
		if (selectedDomaine == null)
			return null;
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY + "."
				+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@ " + " AND "
				+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_DOMAINE_KEY + " = %@", new NSArray<Object>(new Object[] { year, selectedDomaine }));

		NSArray<EOSortOrdering> sort = new NSArray<EOSortOrdering>(new EOSortOrdering[] { new EOSortOrdering(EOScolFormationDiplome.FDIP_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending) });
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationDiplome.ENTITY_NAME, qual, sort);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * retourne la liste des diplomes pour un EC et une annee donnee
	 * 
	 * @param edCo
	 * @param ec
	 * @param year
	 * @return
	 */
	public static NSArray<EOScolFormationDiplome> getDiplomesForEcAndYear(EOEditingContext edCo, EOScolMaquetteEc ec, EOScolFormationAnnee year) {
		if (ec == null)
			return null;
		if (year == null)
			return null;
		if (edCo == null)
			return null;

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_MAQUETTE_PARCOURSS_KEY + "."
				+ EOScolMaquetteParcours.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_SEMS_KEY + "."
				+ EOScolMaquetteRepartitionSem.TO_FWK_SCOLARITE__SCOL_MAQUETTE_SEMESTRE_KEY + "."
				+ EOScolMaquetteSemestre.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_UES_KEY + "."
				+ EOScolMaquetteRepartitionUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_UE_KEY + "."
				+ EOScolMaquetteUe.TO_FWK_SCOLARITE__SCOL_MAQUETTE_REPARTITION_ECS_KEY + "."
				+ EOScolMaquetteRepartitionEc.TO_FWK_SCOLARITE__SCOL_MAQUETTE_EC_KEY + " = %@ " + " AND "
				+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATIONS_KEY + "."
				+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_HABILITATIONS_KEY + "."
				+ EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY + " = %@ ", new NSArray<Object>(new Object[] { ec, year }));

		NSArray<EOSortOrdering> sort = new NSArray<EOSortOrdering>(new EOSortOrdering[] { new EOSortOrdering(EOScolFormationDiplome.FDIP_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending) });
		EOFetchSpecification fetchSpec = new EOFetchSpecification(EOScolFormationDiplome.ENTITY_NAME, qual, sort);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(true);
		return edCo.objectsWithFetchSpecification(fetchSpec);
	}

}
