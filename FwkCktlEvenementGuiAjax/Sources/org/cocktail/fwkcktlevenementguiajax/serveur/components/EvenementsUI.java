/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenementguiajax.serveur.components;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenementguiajax.serveur.components.EvenementForm.PersonnesTbvHelper;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation._NSDelegate;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

/**
 * 
 * Composant de gestion des évènements.
 * L'utilisateur de ce composant doit passer un displayGroup récupérant les évènements (via une table de jointure
 * vraisemblablement). Une partie de la gestion des évènements est donc laissée à la charge de l'utilisateur de ce 
 * composant via un deleguée à implémenter et un displayGroup à fournir.
 * 
 * @binding displayGroup dg renvoyant un tableau de {@link EOEvenement}
 * @binding utilisateur l'utilisateur en cours de type {@link EOPersonne}
 * @binding editingContext (optionnel) editingContext parent éventuel sur lequel sera crée un nested ec pour l'édition.
 *          Si null, un nouvel ec sera crée (enregistrant directement en bdd).
 * @binding delegate (optionnel) objet déléguée ayant une ou plusieurs méthodes de l'interface {@link Delegate}.
 * @binding personnesTbvHelper (optionnel) objet de type {@link PersonnesTbvHelper} à utiliser que dans le cas où 
 *          l'on veut personnaliser les colonnes de la tableView des personnes concernées par l'évènement.
 *          
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class EvenementsUI extends EvenementsComponent {

    private static final long serialVersionUID = -1233995223913479704L;
    private static final Logger LOG = Logger.getLogger(EvenementsUI.class);
    private static final String BINDING_DISPLAY_GROUP = "displayGroup";
    private static final String BINDING_UTILISATEUR = "utilisateur";
    private static final String BINDING_EDITING_CONTEXT = "editingContext";
    private static final String BINDING_DELEGATE = "delegate";
    private static final String BINDING_WRAPPER = "wrapper";
    public static final String BINDING_PERSONNES_TBV_HELPER = "personnesTbvHelper";

    private static final String EVENEMENT_KEY = "currentEvenement";
    private static final String DATE_PREVUE_KEY = EOEvenement.DATE_PREVUE_KEY;
    private static final String MOTIF_KEY = EOEvenement.OBJET_KEY;
    private static final String NATURE_KEY = EOEvenement.TYPE_KEY;

    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private EOEvenement currentEvenement;
    private EOEvenement selectedEvenement;
    private EOEditingContext ecForEdition;
    private EOPersonne utilisateur;

    
    private _NSDelegate delegate;
    
    public EvenementsUI(WOContext context) {
        super(context);
    }
    
    private EOEditingContext parentEc() {
        return (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
    }
    
    private EOEditingContext ecForEdition() {
        if (ecForEdition == null) {
            if (parentEc() != null)
                ecForEdition = ERXEC.newEditingContext(parentEc());
            else
                ecForEdition = ERXEC.newEditingContext();
        }
        return ecForEdition;
    }
    
    private EOEvenement selectedEvenementDg() {
        return (EOEvenement) getDisplayGroup().selectedObject();
    }
    
    public EOPersonne utilisateur() {
        if (utilisateur == null) {
            utilisateur = (EOPersonne)valueForBinding(BINDING_UTILISATEUR);
        }
        return utilisateur;
    }
    
    private boolean saveChanges(EOEditingContext ec) {
        boolean success = true;
        try {
            if (ec.hasChanges()) {
                ec.saveChanges();
            }
        } catch (NSValidation.ValidationException e) {
            success = false;
            LOG.warn(e.getMessage(), e);
            session().addSimpleErrorMessage("Erreur de validation", e.getMessage());
        } catch (Exception e) {
            success = false;
            ec.revert();
            LOG.error(e.getMessage(), e);
            throw new NSForwardException(e);
        }
        return success;
    }
    
    public WOActionResults ajouter() {
        ecForEdition().revert();
        EOEvenement event = FwkCktlEvenementUtil.creerNouvelEvenement(
                utilisateur().localInstanceIn(ecForEdition()), ecForEdition());
        setSelectedEvenement(event);
        if (delegate().respondsTo(Delegate.evenementInserted))
            delegate().perform(Delegate.evenementInserted, event);
        return null;
    }
    
    public WOActionResults editer() {
        ecForEdition().revert();
        setSelectedEvenement(selectedEvenementDg().localInstanceIn(ecForEdition()));
        return null;
    }
    
    public WOActionResults enregistrer() {
        boolean isNewEvent = ecForEdition().insertedObjects().containsObject(getSelectedEvenement());
        // Lorsqu'on maj un evenement, il faut aussi recalculer sa frequence cron standard eventuelle
        // peut pas mettre ça niveau métier pour l'instant, car plus tard possibilité de créer des frequences cron
        // custom
        if (!isNewEvent && getSelectedEvenement().lastFrequenceCron() != null) {
            FwkCktlEvenementUtil.recalculerFrequenceCronStandard(getSelectedEvenement().lastFrequenceCron());
        }
        // On renseigne ici la personne ayant modifié l'evt
        getSelectedEvenement().setPersidModif(utilisateur().persId());
        boolean hasSaved = saveChanges(ecForEdition());
        isNewEvent = hasSaved && isNewEvent;
        if (hasSaved) {
            CktlAjaxWindow.close(context(), eventDetailDiagId());
            if (isNewEvent && delegate().respondsTo(Delegate.evenementCreated)) {
                delegate().perform(Delegate.evenementCreated, getSelectedEvenement());
            }
        }
        return null;
    }
    
    public WOActionResults annuler() {
        ecForEdition().revert();
        setSelectedEvenement(null);
        CktlAjaxWindow.close(context(), eventDetailDiagId());
        return null;
    }
    
    public WOActionResults supprimer() {
        // FIX si jamais l'event est déjà sélectionné sans avoir cliqué dessus 
        setSelectedEvenement(selectedEvenementDg().localInstanceIn(ecForEdition()));
        EOEditingContext ec = getSelectedEvenement().editingContext();
        try {
            if (delegate().respondsTo(Delegate.deleteEvenement)) {
                delegate().perform(Delegate.deleteEvenement, getSelectedEvenement());
            } 
            saveChanges(ec);
            setSelectedEvenement(null);
        } catch (NSValidation.ValidationException e) {
            LOG.warn(e.getMessage(), e);
            session().addSimpleErrorMessage("Erreur de validation", e.getMessage());
        }
        return null;
    }
    
    public WOActionResults openEventDetailDiag() {
        EvenementFormPage detailPage = pageWithName(EvenementFormPage.class);
        detailPage.setUtilisateur(utilisateur());
        detailPage.setSelectedEvenement(getSelectedEvenement());
        detailPage.setWrapper(getWrapper());
        detailPage.setParent(this);
        detailPage.setPersonnesTbvHelper(getPersonnesTbvHelper());
        return detailPage;
    }
    
    public String tbvId() {
        return "TBV_" + componentId();
    }
    
    public String containerId() {
        return "Cont_" + componentId();
    }
    
    public String eventDetailDiagId() {
        return "EvtDetailDiag_" + componentId();
    }
    
    public NSArray<CktlAjaxTableViewColumn> getColonnes() {
        if (colonnes == null) {
            NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
            // Colonne Date
            CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
            col.setLibelle("Date");
            col.setOrderKeyPath(DATE_PREVUE_KEY);
            String keyPath = ERXQ.keyPath(EVENEMENT_KEY, DATE_PREVUE_KEY);
            CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            ass.setDateformat("%d/%m/%Y");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Motif
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Motif");
            col.setOrderKeyPath(MOTIF_KEY);
            keyPath = ERXQ.keyPath(EVENEMENT_KEY, MOTIF_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Lieu
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Nature");
            col.setOrderKeyPath(NATURE_KEY);
            keyPath = ERXQ.keyPath(EVENEMENT_KEY, NATURE_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            colonnes = colTmp.immutableClone();
        }
        return colonnes;
    }
    
    public ERXDisplayGroup getDisplayGroup() {
        return (ERXDisplayGroup)valueForBinding(BINDING_DISPLAY_GROUP);
    }
    
    private String getWrapper() {
        return stringValueForBinding(BINDING_WRAPPER);
    }
    
    private _NSDelegate delegate() {
        if (delegate == null) {
            if (hasBinding(BINDING_DELEGATE))
                delegate = new _NSDelegate(Delegate.class, valueForBinding(BINDING_DELEGATE));
            else
                delegate = new _NSDelegate(Delegate.class, defaultDelegate());
        }
        return delegate;
    }
    
    public PersonnesTbvHelper getPersonnesTbvHelper() {
        return (PersonnesTbvHelper)valueForBinding(BINDING_PERSONNES_TBV_HELPER);
    }
    
    public EOEvenement getCurrentEvenement() {
        return currentEvenement;
    }
    
    public void setCurrentEvenement(EOEvenement currentEvenement) {
        this.currentEvenement = currentEvenement;
    }
    
    public EOEvenement getSelectedEvenement() {
        return selectedEvenement;
    }
    
    public void setSelectedEvenement(EOEvenement selectedEvenement) {
        this.selectedEvenement = selectedEvenement;
    }
    
    /**
     * Délégué par défaut. Supprime l'évènement, ce n'est pas ce qui est voulu la plupart 
     * du temps : on doit aussi supprimer les items dépendant de cet évènement (jointure item->évènement)
     */
    private Delegate defaultDelegate() {
        return new Delegate() {
            public void evenementCreated(EOEvenement evenement) {
                // RIEN
            }

            public void deleteEvenement(EOEvenement evenement) {
                EOEditingContext ec = evenement.editingContext();
                EOEvenement.supprimerEvenement(getSelectedEvenement(), ec);
            }

            public void evenementInserted(EOEvenement evenement) {
                // RIEN
            }

			public Boolean peutModifierEvenements() {
				return true;
			}
        };
    }
    
    /**
     * 
     * Delegué à implémenter pour exécuter du code avant la manipulation d'un évènement.
     *
     */
    public static interface Delegate {
        public final static String evenementCreated = "evenementCreated";
        public final static String evenementInserted = "evenementInserted";
        public final static String deleteEvenement = "deleteEvenement";
        public final static String peutModifierEvenements = "peutModifierEvenements";

        /**
         * Appelé lorsqu'un évènement a été crée
         * @param evenement crée
         */
        public void evenementCreated(EOEvenement evenement);
        
        /**
         * Appelé lorsqu'un évènement a été ajouté (click sur le bouton +)
         * @param evenement inséré
         */
        public void evenementInserted(EOEvenement evenement);
        
        /**
         * Appelé lorsqu'un évènement doit être supprimé
         * C'est au délégué à s'assurer de la suppresion de l'évènement.
         * @param evenement l'évènement à supprimer
         */
        public void deleteEvenement(EOEvenement evenement);
        
        /**
         * Appel pour determiner si l'on peut ajouter un évènement
         */
        public Boolean peutModifierEvenements();
    }
    public Boolean peutAjouterEvenement() {
    	return peutModifierEvenements();
    }

	private Boolean peutModifierEvenements() {
		Boolean peutModifierEvenements = false;
        if (delegate().respondsTo(Delegate.peutModifierEvenements)) {
        	peutModifierEvenements = (Boolean) delegate().perform(Delegate.peutModifierEvenements);
        } 
        return peutModifierEvenements;
	}
	
    public Boolean peutEditerEvenement() {
    	return getDisplayGroup() != null && getDisplayGroup().selectedObject() != null && peutModifierEvenements();
    }
    
    public Boolean peutSupprimerEvenement() {
    	return getDisplayGroup() != null && getDisplayGroup().selectedObject() != null && peutModifierEvenements();
    }
    
}