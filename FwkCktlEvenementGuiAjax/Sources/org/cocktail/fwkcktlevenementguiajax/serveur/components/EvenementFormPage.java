/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenementguiajax.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenementguiajax.serveur.components.EvenementForm.PersonnesTbvHelper;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * 
 * Page encapsulant le composant EvenementForm.
 * Cette page est destiné à être ouverte dans une modal window.<br/>
 * 
 * Les champs suivants sont à renseigner :
 * <ul>
 *  <li>{@link EvenementFormPage#selectedEvenement} l'évènement à éditer</li>
 *  <li>{@link EvenementFormPage#utilisateur} l'utilisateur en cours</li>
 *  <li>{@link EvenementFormPage#wrapper} le nom du composant Wrapper à utiliser dans cette page</li>
 *  <li>{@link EvenementFormPage#parent} le composant parent ayant instancié cette page (pour l'instant 
 *   de type {@link EvenementsUI}).</li>
 *  <li>{@link EvenementFormPage#personnesTbvHelper} objet de type {@link PersonnesTbvHelper} à
 *   utiliser que dans le cas où l'on veut personnaliser les colonnes de la tableView des personnes concernées par l'évènement.</li>
 * </ul>
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class EvenementFormPage extends EvenementsComponent {

    private static final long serialVersionUID = -3064076512860754744L;
    private EOEvenement selectedEvenement;
    private EOPersonne  utilisateur;
    private String wrapper;
    private EvenementsUI parent;
    private PersonnesTbvHelper personnesTbvHelper;
    
    public EvenementFormPage(WOContext context) {
        super(context);
    }
    
    public WOActionResults enregistrer() {
        return parent.enregistrer();
    }
    
    public WOActionResults annuler() {
        
        return parent.annuler();
    }
    
    public String detailContId() {
        return "EvtDetail_" + componentId();
    }
    
    public EOEvenement getSelectedEvenement() {
        return selectedEvenement;
    }
    
    public void setSelectedEvenement(EOEvenement selectedEvenement) {
        this.selectedEvenement = selectedEvenement;
    }
    
    public EOPersonne getUtilisateur() {
        return utilisateur;
    }
    
    public void setUtilisateur(EOPersonne utilisateur) {
        this.utilisateur = utilisateur;
    }
    
    public void setWrapper(String wrapper) {
        this.wrapper = wrapper;
    }
    
    public String getWrapper() {
        return wrapper;
    }
    
    public void setParent(EvenementsUI parent) {
        this.parent = parent;
    }
    
    public PersonnesTbvHelper getPersonnesTbvHelper() {
        return personnesTbvHelper;
    }
    
    public void setPersonnesTbvHelper(PersonnesTbvHelper personnesTbvHelper) {
        this.personnesTbvHelper = personnesTbvHelper;
    }
    
}