/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenementguiajax.serveur.components;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron;
import org.cocktail.fwkcktlevenement.serveur.modele.EORepetition;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

/**
 * 
 * Composant d'édition des fréquences de répétition d'un évènement.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class FrequenceForm extends EvenementsComponent {

    private static final long serialVersionUID = 6883524694876225915L;
    public static final String BINDING_EVENEMENT = "evenement";
    public static final String BINDING_READ_ONLY = "readOnly";
    
    private EORepetition currentRepetition;
    private EORepetition selectedRepetition;
    
    public FrequenceForm(WOContext context) {
        super(context);
    }
    
    @Override
    public void sleep() {
        currentRepetition = null;
        selectedRepetition = null;
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
        EOFrequenceCron freq = (EOFrequenceCron) evenement().frequencesCrons().lastObject();
        if (freq != null && freq.repetition() != null)
            setSelectedRepetition(freq.repetition());
        super.appendToResponse(response, context);
    }
    
    public WOActionResults ajouterFrequence() {
        // Si une fréquence existe on la supprime
        FwkCktlEvenementUtil.supprimerFrequences(evenement());
        // On crée une nouvelle fréquence avec la repetition donnée si on est dans le cas de rep != JAMAIS
        if (!selectedRepetition().isAucune()) {
            try {
                FwkCktlEvenementUtil.ajouterFrequenceCronStandard(evenement(), selectedRepetition(), null, evenement().editingContext());
            } catch (ExceptionOperationImpossible e) {
                throw new NSForwardException(e);
            }
        }
        return null;
    }
    
    public EOEvenement evenement() {
        return (EOEvenement)valueForBinding(BINDING_EVENEMENT);
    }
    
    public Boolean isReadOnly() {
    	return valueForBooleanBinding(BINDING_READ_ONLY, false);
    }
    
    public EORepetition selectedRepetition() {
        return selectedRepetition;
    }
    
    public void setSelectedRepetition(EORepetition repetition)  {
        this.selectedRepetition = repetition;
    }
    
    public NSArray<EORepetition> repetitions() {
        return EORepetition.getRepetitionsSansAvance();
    }
    
    public EORepetition getCurrentRepetition() {
        return currentRepetition;
    }
    
    public void setCurrentRepetition(EORepetition currentRepetition) {
        this.currentRepetition = currentRepetition;
    }
    
}