/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenementguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.appserver.ERXWOContext;
import er.extensions.components.ERXComponent;
import er.extensions.eof.ERXEC;

/**
 * 
 * Classe de base pour les composants évènement. 
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class EvenementsComponent extends ERXComponent {

    private static final long serialVersionUID = -788308780827112422L;
    public static final String BINDING_EDC = "editingContext";
    public static final String BINDING_WANT_RESET = "wantReset";

    private String componentId;
    private EOEditingContext editingContext;

    public EvenementsComponent(WOContext context) {
        super(context);
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public String componentId() {
        if (componentId == null)
            componentId = ERXWOContext.safeIdentifierName(context(), true);
        return componentId;
    }
    
    public CocktailAjaxSession session() {
        return (CocktailAjaxSession)super.session();
    }
    
    public EOEditingContext edc() {
        if (hasBinding(BINDING_EDC) && valueForBinding(BINDING_EDC) != null) {
            if (editingContext == null || wantReset()) {
                editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_EDC));
                // FIX du BUG No snapshot....
                editingContext.setRetainsRegisteredObjects(true);
                setWantReset(false);
            }
            return editingContext;
        } else {
            throw new IllegalArgumentException("le binding editingContext est obligatoire");
        }
    }
    
    public boolean wantReset() {
        return booleanValueForBinding(BINDING_WANT_RESET, false);
    }
    
    public void setWantReset(boolean value) {
        if (hasBinding(BINDING_WANT_RESET) && canSetValueForBinding(BINDING_WANT_RESET))
            setValueForBinding(false, BINDING_WANT_RESET);
    }
    
}
