/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenementguiajax.serveur.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities.UniteTemps;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementEtat;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementTypeCat;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTache;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache;
import org.cocktail.fwkcktlevenementguiajax.serveur.components.EvenementForm.PersonnesTbvHelper;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.localization.ERXLocalizer;

/**
 * 
 * Formulaire d'édition d'un évènement.
 * C'est le composant parent utilisant ce composant qui est chargé de son état.
 * 
 * @see EvenementsUI
 * 
 * @binding evenement {@link EOEvenement} l'évènement à éditer
 * @binding utilisateur {@link EOPersonne} l'utilisateur de l'application
 * @binding personnesTbvHelper (optionnel) objet de type {@link PersonnesTbvHelper} à utiliser que dans le cas où 
 *          l'on veut personnaliser les colonnes de la tableView des personnes concernées par l'évènement.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class EvenementForm extends EvenementsComponent {

    private static final long serialVersionUID = 4787375302924668986L;
    public static final String BINDING_EVENEMENT = "evenement";
    public static final String BINDING_UTILISATEUR = "utilisateur";
    public static final String BINDING_PERSONNES_TBV_HELPER = "personnesTbvHelper";
    public static final String BINDING_READ_ONLY = "readOnly";

    private EOEvenementType currentType;
    private EOEvenementTypeCat currentTypeCat;
    private EOEvenementTypeCat selectedTypeCat;
    private EOEvenementEtat currentEtat;
    private EOTypeTache currentTypeTache;
    private EOTache currentTache;
    private IPersonne currentResponsableJuridique;
    private ERXDisplayGroup personnesDisplayGroup;
    private ERXDisplayGroup personnesSrchDisplayGroup;
    private ERXDisplayGroup tachesDisplayGroup;
    private NSMutableArray<CktlAjaxTableViewColumn> colonnesTaches;
    private NSArray<TypeOffset> typeOffset = new NSArray<TypeOffset>(TypeOffset.Avant, TypeOffset.Apres);
    private NSArray<IPersonne> responsablesJuridique;
    private TypeOffset currentTypeOffset;
    private boolean resetPersonneSrch;
    private IPersonne selectedPersonne;
    private UniteTemps currentUnite;
    private NSMutableDictionary<String, Object> newTache = new NSMutableDictionary<String, Object>();
    
    private PersonnesTbvHelper personnesTbvHelper;
    
    public EvenementForm(WOContext context) {
        super(context);
    }

    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
        // Sélection de la catégorie de type d'évènements
        if (getSelectedTypeCat() == null) {
            if (!isNewEvent()) {
                setSelectedTypeCat(getEvenement().type().evenementTypeCat());
            } else {
                setSelectedTypeCat(EOEvenementTypeCat.getTypeCategorieDefault().localInstanceIn(editingContext()));
            }
        }
        super.appendToResponse(response, context);
    }
    
    @Override
    public void sleep() {
        //personnesDisplayGroup = null;
        tachesDisplayGroup = null;
        super.sleep();
    }
    
    public String personneSrchDiagId() {
        return "PersSrch_" + componentId();
    }
    
    public String personnesContId() {
        return "PersCont_" + componentId();
    }
    
    public String tachesContId() {
        return "TachesCont_" + componentId();
    }
    
    public String tachesDiagId() {
        return "TachesDiag_" + componentId();
    }
    
    public String tachesTbvId() {
        return "TachesTbv_" + componentId();
    }
    
    public String tabsId() {
        return "Tabs_" + componentId();
    }
    
    public String generalTabId() {
        return "GeneralTab_" + componentId();
    }
    
    public String personnesTabId() {
        return "PersonnesTab_" + componentId();
    }
    
    public String personnesTbvId() {
        return "PersonnesTbv_" + componentId();
    }
    
    public WOActionResults openPersonneSrch() {
        setResetPersonneSrch(true);
        return null;
    }
    
    public WOActionResults openAjoutTache() {
        // Par défaut 1j avant l'evt
        newTache.setObjectForKey(Integer.valueOf(1), "offset");
        newTache.setObjectForKey(DateUtilities.Jours, "unite");
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public WOActionResults selectionner() {
        getEvenement().addToPersonnes(getPersonnesSrchDisplayGroup().selectedObjects());
        refreshPersonnesDisplayGroup();
        //personnesDisplayGroup = null;
        CktlAjaxWindow.close(context(), personneSrchDiagId());
        return null;
    }
    
    public WOActionResults ajouterTache() {
        EOTypeTache type = (EOTypeTache) newTache.objectForKey("type");
        try {
            if (newTache.objectForKey("offset") == null)
                throw new ValidationException("Vous devez renseigner un délais pour cette tâche");
            int offset = ((BigDecimal)newTache.objectForKey("offset")).intValue();
            int unite = ((UniteTemps)newTache.objectForKey("unite")).getCode();
            TypeOffset typeOffset = (TypeOffset)newTache.objectForKey("typeOffset");
            int triggerOffsetInMinutes = DateUtilities.toMinutes(offset, unite);
            if (TypeOffset.Avant.equals(typeOffset))
                triggerOffsetInMinutes = -triggerOffsetInMinutes;

            EOTache.creerTacheAlerteMail(getEvenement(), triggerOffsetInMinutes, EOTache.ETAT_NON_PROGRAMMEE, utilisateur(), editingContext());
            CktlAjaxWindow.close(context(), tachesDiagId());
        } catch (ValidationException e) {
            session().addSimpleErrorMessage("Erreur lors de la création d'une tache", e.getMessage());
        }
        tachesDisplayGroup = null;
        return null;
    }
    
    public WOActionResults supprimerPersonne() {
        IPersonne personne = (IPersonne)getPersonnesDisplayGroup().selectedObject();
        getEvenement().removeFromPersonnes(personne);
        refreshPersonnesDisplayGroup();
        //personnesDisplayGroup = null;
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public WOActionResults supprimerTache() {
        NSArray<EOTache> taches = getTachesDisplayGroup().selectedObjects().immutableClone();
        for (EOTache tache : taches) {
            getEvenement().removeFromTachesRelationship(tache);
            tache.delete();
        }
        tachesDisplayGroup = null;
        return null;
    }
    
    public WOActionResults changerTypeCategorie() {
        getEvenement().deleteAllEvenementExtJuridiquesRelationships();
        if (isSelectedTypeCatJuridique()) {
            EOEvenementExtJuridique evtExt = EOEvenementExtJuridique.create(editingContext());
            getEvenement().setEvenementExtJuridique(evtExt);
        } 
        return null;
    }
    
    public WOActionResults changerType() {
    	getEvenement().setTypeRelationship(getSelectedType());
    	return null;
    }
    
    public EOEditingContext editingContext() {
        return getEvenement().editingContext();
    }
    
    public EOPersonne utilisateur() {
        return (EOPersonne)valueForBinding(BINDING_UTILISATEUR);
    }
    
    public ERXDisplayGroup getPersonnesSrchDisplayGroup() {
        if (personnesSrchDisplayGroup == null) {
            personnesSrchDisplayGroup = new ERXDisplayGroup();
            personnesSrchDisplayGroup.setSelectsFirstObjectAfterFetch(false);
        }
        return personnesSrchDisplayGroup;
    }
    
    public ERXDisplayGroup getTachesDisplayGroup() {
        if (tachesDisplayGroup == null) {
            tachesDisplayGroup = new ERXDisplayGroup();
            tachesDisplayGroup.setObjectArray(getEvenement().taches());
        }
        return tachesDisplayGroup;
    }
    
    public NSArray<CktlAjaxTableViewColumn> getColonnesTaches() {
        if (colonnesTaches == null) {
            colonnesTaches = new NSMutableArray<CktlAjaxTableViewColumn>();
            CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
            col1.setOrderKeyPath("labelForTache");
            String libelle = localizer().localizedStringForKey("tbv.taches.libelle");
            col1.setLibelle(libelle);
            CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation("labelForTache", "");
            col1.setAssociations(ass1);
            colonnesTaches.addObject(col1);
        }
        return colonnesTaches;
    }
    
    public String labelForTache() {
        return getCurrentTache().type().libelleCourt() + " " + getCurrentTache().triggerOffsetInMinutesToString() + " " +
               "le début de l'évènement";
    }
    
    public PersonnesTbvHelper getPersonnesTbvHelper() {
        if (personnesTbvHelper == null) {
            PersonnesTbvHelper fromParent = (PersonnesTbvHelper)valueForBinding(BINDING_PERSONNES_TBV_HELPER);
            if (fromParent != null) {
                personnesTbvHelper = fromParent;
            } else {
                personnesTbvHelper = new PersonnesTbvHelper();
            }
        }
        return personnesTbvHelper;
    }
    
    public boolean isNewEvent() {
        EOEditingContext ec = getEvenement().editingContext();
        return ec == null || ec.insertedObjects().containsObject(getEvenement());
    }
    
    public EOEvenement getEvenement() {
        return (EOEvenement)valueForBinding(BINDING_EVENEMENT);
    }
    
    public Boolean isReadOnly() {
    	return valueForBooleanBinding(BINDING_READ_ONLY, false);
    }
    
    public NSArray<EOEvenementType> types() {
        NSArray<EOEvenementType> types = EOEvenementType.getTypesEvenement();
        if (getSelectedTypeCat() != null)
            types = ERXQ.filtered(types, ERXQ.equals(EOEvenementType.EVENEMENT_TYPE_CAT_KEY, getSelectedTypeCat()));
        return ERXEOControlUtilities.localInstancesOfObjects(editingContext(), types);
    }
    
    public NSArray<EOEvenementTypeCat> typeCategories() {
        return ERXEOControlUtilities.localInstancesOfObjects(editingContext(), EOEvenementTypeCat.getTypeCategories());
    }
    
    public boolean isSelectedTypeCatJuridique() {
        return getSelectedTypeCat() != null && EOEvenementTypeCat.ID_INTERNE_JURIDIQUE.equals(getSelectedTypeCat().idInterne());
    }
    
    public EOEvenementTypeCat getCurrentTypeCat() {
        return currentTypeCat;
    }
    
    public void setCurrentTypeCat(EOEvenementTypeCat currentTypeCat) {
        this.currentTypeCat = currentTypeCat;
    }
    
    public EOEvenementType getCurrentType() {
        return currentType;
    }
    
    public void setCurrentType(EOEvenementType currentType) {
        this.currentType = currentType;
    }
    
    public EOEvenementType getSelectedType() {
        return getEvenement().type();
    }
    
    public void setSelectedType(EOEvenementType selectedType) {
    	getEvenement().setTypeRelationship(selectedType);
    }
    
    public NSArray<EOEvenementEtat> etats() {
        return EOEvenementEtat.getEtatsEvenement();
    }
    
    public EOEvenementEtat getCurrentEtat() {
        return currentEtat;
    }
    
    public void setCurrentEtat(EOEvenementEtat currentEtat) {
        this.currentEtat = currentEtat;
    }
    
    public boolean isResetPersonneSrch() {
        return resetPersonneSrch;
    }
    
    public void setResetPersonneSrch(boolean resetPersonneSrch) {
        this.resetPersonneSrch = resetPersonneSrch;
    }
    
    public IPersonne getSelectedPersonne() {
        return selectedPersonne;
    }
    
    public void setSelectedPersonne(IPersonne selectedPersonne) {
        this.selectedPersonne = selectedPersonne;
    }
    
    public NSArray<EOTypeTache> typesTache() {
        return EOTypeTache.getTypesTache();
    }
    
    public EOTypeTache getCurrentTypeTache() {
        return currentTypeTache;
    }
    
    public void setCurrentTypeTache(EOTypeTache currentTypeTache) {
        this.currentTypeTache = currentTypeTache;
    }
    
    public NSMutableDictionary<String, Object> getNewTache() {
        return newTache;
    }
    
    public NSArray<UniteTemps> getUnites() {
        return DateUtilities.Unites;
    }
    
    public EOTache getCurrentTache() {
        return currentTache;
    }
    
    public void setCurrentTache(EOTache currentTache) {
        this.currentTache = currentTache;
    }
    
    public UniteTemps getCurrentUnite() {
        return currentUnite;
    }
    
    public void setCurrentUnite(UniteTemps currentUnite) {
        this.currentUnite = currentUnite;
    }
    
    public NSArray<TypeOffset> getTypeOffset() {
        return typeOffset;
    }
    
    public TypeOffset getCurrentTypeOffset() {
        return currentTypeOffset;
    }
    
    public void setCurrentTypeOffset(TypeOffset currentTypeOffset) {
        this.currentTypeOffset = currentTypeOffset;
    }
    
    public EOEvenementTypeCat getSelectedTypeCat() {
        return selectedTypeCat;
    }
    
    public void setSelectedTypeCat(EOEvenementTypeCat selectedTypeCat) {
        this.selectedTypeCat = selectedTypeCat;
    }
    
  
    public String getAvisFav() {
        return EOEvenementExtJuridique.AVIS_FAV;
    }
    
    public String getAvisDefav() {
        return EOEvenementExtJuridique.AVIS_DEFAV;
    }
    
    public Boolean isFavorable() {
    	return getEvenement().evenementExtJuridique().evtAvis().equals(getAvisFav());
    }
    
    public ERXDisplayGroup getPersonnesDisplayGroup() {
        if (personnesDisplayGroup == null) {
            personnesDisplayGroup = new ERXDisplayGroup();
            personnesDisplayGroup.setObjectArray(getEvenement().personnes());
        }
        return personnesDisplayGroup;
    }
    
    public NSArray<IPersonne> responsablesJuridique() {
        if (responsablesJuridique == null) {
            responsablesJuridique = EOEvenementExtJuridique.fetchResponsablesJuridiquesPossibles(editingContext());
        }
        return responsablesJuridique;
    }
    
    public IPersonne getCurrentResponsableJuridique() {
        return currentResponsableJuridique;
    }
    
    public void setCurrentResponsableJuridique(IPersonne currentResponsableJuridique) {
        this.currentResponsableJuridique = currentResponsableJuridique;
    }
    
    private void refreshPersonnesDisplayGroup() {
        getPersonnesDisplayGroup().setObjectArray(getEvenement().personnes());
    }
    
    public static class TypeOffset {
        
        public final static TypeOffset Avant = new TypeOffset("-", ERXLocalizer.defaultLocalizer().localizedStringForKey("offset.avant"));
        public final static TypeOffset Apres = new TypeOffset("+", ERXLocalizer.defaultLocalizer().localizedStringForKey("offset.apres"));
        
        String code;
        String label;
        
        public TypeOffset(String code, String label) {
            super();
            this.code = code;
            this.label = label;
        }

        public String getCode() {
            return code;
        }
        
        public String getLabel() {
            return label;
        }
        
    }
    
    public static class PersonnesTbvHelper {
        
        protected static final String HelperKey = "personnesTbvHelper.";
        protected static final String CurrentPersonneKey = HelperKey + "currentPersonne.";
        private IPersonne currentPersonne;
        protected NSMutableArray<CktlAjaxTableViewColumn> colonnesPersonnes;
        
        public IPersonne getCurrentPersonne() {
            return currentPersonne;
        }
        
        public void setCurrentPersonne(IPersonne currentPersonne) {
            this.currentPersonne = currentPersonne;
        }
     
        public NSArray<CktlAjaxTableViewColumn> getColonnesPersonnes() {
            if (colonnesPersonnes == null) {
                String numKey = CurrentPersonneKey + IPersonne.NUMERO_KEY;
                String nomPrenomKey = CurrentPersonneKey + IPersonne.NOM_PRENOM_AFFICHAGE_KEY;
                colonnesPersonnes = new NSMutableArray<CktlAjaxTableViewColumn>();
                CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
                col1.setOrderKeyPath(IPersonne.NUMERO_KEY);
                String num = ERXLocalizer.defaultLocalizer().localizedStringForKey("tbv.numero");
                col1.setLibelle(num);
                CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(numKey, "");
                col1.setAssociations(ass1);
                colonnesPersonnes.addObject(col1);
                CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
                col2.setOrderKeyPath(IPersonne.NOM_PRENOM_AFFICHAGE_KEY);
                String nom = ERXLocalizer.defaultLocalizer().localizedStringForKey("tbv.nom");
                col2.setLibelle(nom);
                CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(nomPrenomKey, "");
                col2.setAssociations(ass2);
                colonnesPersonnes.addObject(col2);
            }
            return colonnesPersonnes;
        }
        
    }
    
}